$(document).on("change",".form-check-input[type='checkbox']",function (e) {
	$dataid = $(this).attr("id");
	if ($('.form-check-input[name="'+$dataid+'"').is(':checked')) {
        $(".active_inactive_sub[dataid='"+$dataid+"']").removeClass('graycolor');
        $(".active_inactive_sub[dataid='"+$dataid+"']").addClass('greencolor');
        $(".active_inactive_sub[dataid='"+$dataid+"']").text('Active');
    } else {
        $(".active_inactive_sub[dataid='"+$dataid+"']").removeClass('greencolor');
        $(".active_inactive_sub[dataid='"+$dataid+"']").addClass('graycolor');
        $(".active_inactive_sub[dataid='"+$dataid+"']").text('Inactive');
    }
});

$(document).on("click","#wraplikeselect",function (e) {

	if ($(".connectwartists").is(":visible") == false) {
		$(".connectwartists").show();
	} else {
		$(".connectwartists").hide();
	}
});

$(document).on("click",".liconartists",function (e) {
	$value = $(this).attr("value");
	$("#id_request_type").val($value);
	$(".dropdownarrow").val($(this).attr("data-text"));
	$(".connectwartists").hide();
});