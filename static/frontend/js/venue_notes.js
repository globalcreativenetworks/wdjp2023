//  Functions for Adding and updating Event Notes


function validate_new_note(event){
    var text = $(event).find('#id_note').val().trim()
    if (text === ""){
        return false
    }
    else{
        return true
    }
}

function show_user_note_modal(note_id,note_text,venue_name){
    $("#note_key").val(note_id)
    $("#note_text").val(note_text)
    $(".note-title").text(`Your Notes on ${venue_name} `)
    $("#id_user_notes").modal("show");
}

$("#id_note_form").submit(function(event){
    event.preventDefault();
    var text = $($(event)[0]["target"]).find("#note_text").val().trim() 

    if (text === ""){
        $("#id_user_notes").modal("hide");
    }
    else{
        pk = $("#note_key").val()
        $.ajax({
            type: "POST",
            async:false,
            url: "/events/venue-note/0/".replace('0',pk),
            data: $(this).serialize(),
            success: function(response) {
                $('.user_note_div').html(response);
                $("#id_user_notes").modal("hide");
        }
    })
    }
})

function show_new_user_note_modal(venue_name){
    $(".note-title").text(`Your Notes on ${venue_name} `)
    $("#id_note").val("")
    $("#id_new_user_notes").modal("show");
}

$("#id_new_note_form").submit(function(event){
    event.preventDefault();
    var text = $($(event)[0]["target"]).find("#id_note").val().trim() 

    if (text === ""){
        $("#id_new_user_notes").modal("hide");
    }
    else{

        $.ajax({
            type: "POST",
            url: "/events/venue-note/0/",
            async:false,
            data: $(this).serialize(),
            success: function(response) {
                $('.user_note_div').html(response);
                $("#id_new_user_notes").modal("hide");
            }
        })
    }
})

function delete_modal_open(note_id){
    $("#dlt-temp-id").val(note_id)
    $("#dlt-modal").modal('show')   
}

$("#delete_note").click(function(){
    url = "/events/delete-venue-note/0/".replace('0', $("#dlt-temp-id").val())
    $.ajax({
            url : url,
            type : "GET",
            success: function(response) {
            $('.user_note_div').html(response);
            $("#dlt-modal").modal('hide')
            },
            error: function(data) {
                console.log(data);
            }
        });
    })