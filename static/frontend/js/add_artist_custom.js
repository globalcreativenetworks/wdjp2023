$(document).on('click', '.reset-btn', function() {
    $('#id_url').val('');
});

$(document).on('change', '#id_has_official_account', function() {
    if (this.checked) {
        $('#id_url').val("");
        $('#id_is_verified_link').prop('checked', false);
        $('#id_submit').addClass('hidden');
        $('#id_submit_move_on').removeClass('hidden');
    } else {
        $('#id_submit_move_on').addClass('hidden');
        $('#id_submit').removeClass('hidden');
    }
});