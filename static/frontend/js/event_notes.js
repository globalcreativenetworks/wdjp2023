//  Functions for Adding and updating Event Notes

function show_user_note_modal(note_id,note_text,event_name){
    $("#note_key").val(note_id)
    $("#note_text").val(note_text)
    $(".note-title").text(`Your Notes on ${event_name} Event`)
    $("#id_user_notes").modal("show");
}

$("#id_note_form").submit(function(event){
    event.preventDefault();
    pk = $("#note_key").val()
    $.ajax({
        type: "POST",
        url: "/events/event-note/0/".replace('0',pk),
        data: $(this).serialize(),
        success: function(response) {
            $('.user_note_div').html(response);
            $("#id_user_notes").modal("hide");
   }
})
})

function show_new_user_note_modal(event_name){
    $(".note-title").text(`Your Notes on ${event_name} Event`)
    $("#id_note").val("")
    $("#id_new_user_notes").modal("show");
}

$("#id_new_note_form").submit(function(event){
    event.preventDefault();
    $.ajax({
        type: "POST",
        url: "/events/event-note/0/",
        data: $(this).serialize(),
        success: function(response) {
            $('.user_note_div').html(response);
            $("#id_new_user_notes").modal("hide");
   }
})
})

function delete_modal_open(note_id){
    $("#dlt-temp-id").val(note_id)
    $("#dlt-modal").modal('show')   
}

$("#delete_note").click(function(){
        url = "/events/delete-event-note/0/".replace('0', $("#dlt-temp-id").val())
        $.ajax({
              url : url,
              type : "GET",
              success: function(response) {
                $('.user_note_div').html(response);
                $("#dlt-modal").modal('hide')
              },
              error: function(data) {
                  console.log(data);
             }
          });
    })