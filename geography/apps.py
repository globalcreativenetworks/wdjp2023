from __future__ import unicode_literals

from django.apps import AppConfig
from watson import search as watson



class GeographyConfig(AppConfig):
    name = 'geography'
    def ready(self):
        Country = self.get_model("Country",)
        City = self.get_model("City",)
        watson.register(Country)
        watson.register(City)
        # from . import signals
