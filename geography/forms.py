from __future__ import unicode_literals

from django import forms
from profiles.models import UserVenueContact


class UserVenueContactForm(forms.ModelForm):
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required' ,'placeholder': 'Email', 'type': 'email'}))
    class Meta:
        model = UserVenueContact

        fields = ('name', 'last_name', 'email', 'facebook_link', 'facebook_event_link', 'notes' )

    def __init__(self, *args, **kwargs):
        super(UserVenueContactForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({'class': 'py-form-control', 'required': 'required', 'cols': '100', 'rows': '5', 'placeholder': ' First Name'})
        self.fields['last_name'].widget.attrs.update({'class': 'py-form-control', 'required': 'required', 'cols': '100', 'rows': '5', 'placeholder': 'Last Name'})
        self.fields['facebook_link'].widget.attrs.update({'class': 'py-form-control', 'placeholder': 'facebook Personal Link' })
        self.fields['facebook_event_link'].widget.attrs.update({'class': 'py-form-control', 'placeholder': 'facebook Event Link' })
        self.fields['notes'].widget.attrs.update({'class': 'form-control', 'cols': '25', 'rows': '5', 'placeholder': 'Notes' })
