from __future__ import unicode_literals

from django.db import models

# Create your models here.
from django.db import models

from django.contrib import admin


class Territories(models.Model):
    short_code = models.CharField(max_length=20, primary_key=True)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "territories"
        verbose_name_plural = "Territories"


class Continent(models.Model):
    code = models.CharField(max_length=2, primary_key=True)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "continents"
        verbose_name_plural = "Continents"


class Country(models.Model):
    code = models.CharField(max_length=2, primary_key=True, verbose_name="Two-letter country code (ISO 3166-1 alpha-2)")
    name = models.CharField(max_length=255, verbose_name="English country name")
    full_name = models.CharField(max_length=255, verbose_name="Full English country name")
    iso3 = models.CharField(max_length=3, verbose_name="Three-letter country code (ISO 3166-1 alpha-3)")
    number = models.IntegerField(default=0, verbose_name="Three-digit country number (ISO 3166-1 numeric)")
    continent_code = models.ForeignKey(Continent,on_delete=models.CASCADE, db_column='continent_code')
    territory_code = models.ForeignKey(Territories, on_delete=models.CASCADE, db_column='territory_code')

    def __str__(self):
        return self.name

    @classmethod
    def get_classname(cls):
        return cls.__name__


    class Meta:
        db_table = "countries"
        verbose_name_plural = "Countries"


class City(models.Model):
    pk_city = models.AutoField(primary_key=True)
    city = models.CharField(max_length=33, default='')
    city_ascii = models.CharField(max_length=39, default='')
    lat = models.DecimalField(max_digits=12, decimal_places=9)
    lng = models.DecimalField(max_digits=12, decimal_places=9)
    iso2 = models.CharField(max_length=3, default='')
    iso3 = models.CharField(max_length=3, default='')
    continent_code = models.ForeignKey(Continent, verbose_name='code of continent', db_column='continent_code')

    def __str__(self):
        return self.city

    @classmethod
    def get_classname(cls):
        return cls.__name__

    class Meta:
        db_table = "cities"
        verbose_name_plural = "Cities"
