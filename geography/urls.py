from django.conf.urls import url

from geography import views

urlpatterns = [
   url(r'^$', views.territory_search, name="territory_search"),
   url(r'^territory_map/(?P<territory>\d+)/$', views.territory_map_frame, name='territory_map_frame'),
   url(r'^maps/$', views.territory_map, name="territory_map"),
   url(r'^add-or-remove-favorite-venue/$', views.favorite_venue, name='add_or_remove_favorite_venue'),
   url(r'^add-or-remove-shortlist-venue/$', views.add_remove_shortlist_venue, name='add_or_remove_shortlist_venue'),
   url(r'^my-favorite-venue/$', views.my_favorite_venue, name='my_favorite_venue'),
   url(r'^my-favorite-venue-xls/$', views.my_favorite_venue_xls, name='my_favorite_venue_xls'),
   url(r'^artist-country-analysis/$', views.artist_country_analysis, name='artist_country_analysis'),
   url(r'^venue-contacts/(?P<venue_id>\d+)/$', views.venue_contacts, name='venue_contacts'),
   url(r'^delete-contact/(?P<contact_id>\d+)/$', views.delete_contact, name='delete_contact'),
   url(r'^territory/$', views.territory, name="territory"),
   ]
