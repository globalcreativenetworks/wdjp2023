from django.contrib import admin

# Register your models here.
from .models import Territories, Continent, Country, City


class TerritoriesAdmin(admin.ModelAdmin):
    list_display = ("name", "short_code")
    ordering = ('name',)


class ContinentAdmin(admin.ModelAdmin):
    list_display = ("code", "name")
    ordering = ('name',)


class CountryAdmin(admin.ModelAdmin):
    empty_value_display = '-empty-'
    list_display = ("code", "name", "full_name", "continent_code")
    ordering = ('name',)
    list_filter = ['continent_code']


class CityAdmin(admin.ModelAdmin):
    empty_value_display = '-empty-'
    list_display = ("city", "city_ascii", "lat", "lng", "continent_code")
    ordering = ('city',)
    list_filter = ['continent_code']
    search_fields = ['city']


admin.site.register(Territories, TerritoriesAdmin)
admin.site.register(Continent, ContinentAdmin)
admin.site.register(Country, CountryAdmin)
admin.site.register(City, CityAdmin)