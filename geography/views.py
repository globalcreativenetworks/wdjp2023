import json
import xlwt
from xlwt import Workbook
from colorhash import ColorHash
from datetime import timedelta
from datetime import datetime as dt
from datetime import datetime

from django.shortcuts import render
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.db.models import Count, Sum


from artist.models import Genre, Artist

from geography.models import Country, City, Territories
from django.db.models import Q
from events.models import DjVenues, DjMaster
from artist.views import filters, get_similar_artist
from accounts.decorators import *
from scrape.models import ScrapePhoto
from profiles.views import save_recent_search
from social_data.models import FacebookLike
from accounts.utils import SendEmail
from news.utils import get_artist_country_news
from profiles.models import ProfilesProfileFavoriteVenues, UserVenueContact
from .forms import UserVenueContactForm

from events.views import get_artist_fb_likes_of_a_country, get_comparison_data, get_todays_like, add_venue, remove_venue


def get_artists_events_in_a_country(country, artists_ids, upcoming_event=True,
                                    start=0, limit=2):
    venues = DjVenues.objects.filter(Q(country__icontains=country))
    venuesids = [item.venueid for item in venues]

    dj_master_objects = DjMaster.objects.filter(
        venueid__in=venuesids,
        artistid__in=artists_ids,
    )
    # .group_by('eventid__date', 'venueid__country', 'artistid', 'eventid__eventName', 'venueid__venueid',
    #            'eventid__evenueName', 'venueid__city')

    if upcoming_event:
        dj_master_objects = dj_master_objects.filter(eventid__date__gt=dt.today()).\
            order_by('eventid__date').distinct()
    else:
        dj_master_objects = dj_master_objects.filter(eventid__date__lt=dt.today()).\
            order_by('-eventid__date').distinct()

    return dj_master_objects[start:start + limit]


def events(place, country=1, limit=None):
    if country == 1:
        venues = DjVenues.objects.filter(Q(country=place))
    elif country == 0:
        venues = DjVenues.objects.filter(Q(city=place))
    else:
        venues = DjVenues.objects.filter(country__in=place)
    venuesids = [item.venueid for item in venues]
    if limit:
        up_events = DjMaster.objects.filter(venueid__in=venuesids).filter(
            eventid__date__gte=dt.today()).order_by('eventid__date')[:limit]
        year_ago = dt.today() - timedelta(730)
        past_events = DjMaster.objects.filter(venueid__in=venuesids).filter(eventid__date__lt=dt.today()).exclude(
            eventid__date__lt=year_ago, venueid__venuelatitude=None).exclude(venueid__venuelatitude="0.000000000").order_by('-eventid__date')[:limit]

    else:
        year_ago = dt.today() - timedelta(730)
        past_events = DjMaster.objects.filter(venueid__in=venuesids).filter(
            eventid__date__lt=dt.today()).exclude(eventid__date__lt=year_ago).order_by('-eventid__date')
        up_events = DjMaster.objects.filter(venueid__in=venuesids).filter(
            eventid__date__gte=dt.today()).order_by('eventid__date')

    return up_events, past_events


@login_required
@check_email_validated
@check_plan('Trial', 'Promoter', 'Artist/DJ', 'Agent')
def artist_country_analysis(request):
    context = {}
    event_load_limit = 10
    countries = Country.objects.all().order_by("name")
    pro_obj = Profile.objects.get(user=request.user)

    country = request.GET.get("country", None)
    artist_name = request.GET.get("artist_name_1", None)
    competitors = request.GET.getlist("competitor", [])
    showing_comp = False
    competitor_list = []

    if artist_name and country:
        artist_obj = Artist.objects.filter(artistName=artist_name).first()
        if artist_obj:
            # Get Similar Artist
            if competitors:
                for comp in competitors:
                    try:
                        competitor_list.append(int(comp))
                    except Exception:
                        pass

            if competitor_list:
                showing_comp = True
                similar_artist = Artist.objects.filter(
                    artistID__in=competitor_list)
            else:
                similar_artist = get_similar_artist(artist_obj, limit=4)

            all_artist_ids = []
            for sm in similar_artist:
                all_artist_ids.append(sm.artistID)
            all_artist_ids.append(artist_obj.artistID)

            if request.is_ajax() and request.GET.get('start', ''):
                start = request.GET.get('start', '')
                action = request.GET.get('action', '')
                start = int(start)
                if action == 'upcoming':
                    allevents = get_artists_events_in_a_country(country, all_artist_ids,
                                                                upcoming_event=True, start=start,
                                                                limit=event_load_limit)
                else:
                    allevents = get_artists_events_in_a_country(country, all_artist_ids,
                                                                upcoming_event=False, start=start,
                                                                limit=event_load_limit)

                context = {
                    "events": allevents,
                    "pro_obj": pro_obj
                }

                template = render_to_string("geography/get_events.html", context,
                                            context_instance=RequestContext(request))

                start = int(start) + event_load_limit

                if (allevents):
                    status = "True"  # No More events
                else:
                    status = "False"  # More events
                response = {
                    "status": status,
                    "start": start,
                    "data": template,
                }
                return HttpResponse(json.dumps(response), content_type="applicaton/json")

            # Get Artist and Similar Artist Event in Country
            artist_past_events = get_artists_events_in_a_country(country, all_artist_ids,
                                                                 upcoming_event=False,
                                                                 limit=event_load_limit)

            artist_future_events = get_artists_events_in_a_country(country, all_artist_ids,
                                                                   upcoming_event=True,
                                                                   limit=event_load_limit)

            # Artist Country News
            artist_country_news = get_artist_country_news(
                all_artist_ids, country, multiple_artists=True)

            # Country Object
            country_object = Country.objects.filter(
                name__icontains=country
            ).first()

            compare_artist = {}
            up_events, past_events = events(country)
            for artist in similar_artist:
                compare_artist.update(get_comparison_data(past_events, artist))
            compare_artist.update(get_comparison_data(past_events, artist_obj))

            context.update({"artist_number_of_event": compare_artist})

            country_fb_likes = {}
            for artist in similar_artist:
                country_fb_likes.update(get_todays_like(artist, country))
            country_fb_likes.update(get_todays_like(artist_obj, country))

            context.update({'country_fb_likes': country_fb_likes, })

            # Get Facebook Stats
            likes_data = {}
            for sm_artist in similar_artist:
                likes_data.update(get_artist_fb_likes_of_a_country(
                    sm_artist, country_name=country, months=12))
            likes_data.update(get_artist_fb_likes_of_a_country(
                artist_obj, country_name=country, months=12))

            color_code = {}
            for key in likes_data.keys():
                color_code[key] = ColorHash(key).hex

            context.update({
                "similar_artist": similar_artist,
                "nationality": country,
                "artist_name": artist_name,
                "artist_obj": artist_obj,
                'likes_data': likes_data,
                'color_code': color_code,
                "artist_country_news": artist_country_news,
                "past_events": artist_past_events,
                "upcoming_events": artist_future_events,
                "country_object": country_object,
                "showing_comp": showing_comp,
                "show_loader": True,
            })

    context.update({
        "countries": countries,
        "start": event_load_limit,
    })

    return render(request, "geography/artist_country_analysis.html", context)


@login_required
@check_email_validated
# @check_plan('Trial', 'Basic', 'Premium')
@check_plan('Trial', 'Promoter', 'Artist/DJ', 'Agent')
def territory_search(request):
    events_limit = 10
    countries = Country.objects.all().order_by("name")
    territories = territory_name = Territories.objects.all()
    pro_obj = Profile.objects.get(user=request.user)
    # Facebook likes In Country
    facebook_like_data = {}
    chart_text = ""
    tab_label = ""
    color_code = {}

    if request.is_ajax() and request.GET.get('ctry'):
        country = request.GET.get('ctry')
        cities = City.objects.filter(iso2=country).order_by("city")
        context = {
            "cities": cities}
        template = render_to_string(
            "geography/get_cities.html", context, context_instance=RequestContext(request))
        response = {"data": template
                    }
        return HttpResponse(json.dumps(response), content_type="application/json")

    # Genre Country Relation Data
    genre_countries = []
    top_countries = []
    genre_countries_titles = {}
    genre_countries_pie = []
    genre_countries_title = ""
    genre_cities = []
    genre_cities_title = ""
    top_country_artist = []
    top_country_artist_title = ""
    dj_countries_facebook_like_data = []
    dj_countries_facebook_like_title = ""
    all_genre = Genre.objects.all()
    for genre in all_genre:
        genre_countries.append({
            "values": [],
            "text": str(genre.genreName)
        })
    if request.GET.get('country'):
        selected_country_name = countries.filter(
            Q(code=request.GET.get('country'))).get().name

        # Facebook Popularity Graph For the Dj and Agent User
        if pro_obj.user_types == "Artists/DJs" and pro_obj.dj_artist:
            chart_text = "You and Your Similar Artist Stats"
            tab_label = "Your Facebook Stats in {}".format(
                selected_country_name)
            facebook_like_data.update(get_artist_fb_likes_of_a_country(
                pro_obj.dj_artist, selected_country_name))
            similar_artists = get_similar_artist(pro_obj.dj_artist, limit=5)
            for similar_artist in similar_artists:
                facebook_like_data.update(get_artist_fb_likes_of_a_country(
                    similar_artist, selected_country_name))

        if pro_obj.user_types == "Agent":
            chart_text = "Your Artists Facebook Stats in {}".format(
                selected_country_name)
            tab_label = "Your Artists Facebook Stats in {}".format(
                selected_country_name)

            for managed_artist in pro_obj.profile_managed_djs.all():
                facebook_like_data.update(get_artist_fb_likes_of_a_country(
                    managed_artist, selected_country_name))

        color_code = {}
        # rand_color = randomcolor.RandomColor()
        for key in facebook_like_data.keys():
            color_code[key] = ColorHash(key).hex

        genre_countries_titles['title'] = selected_country_name + \
            " > Top Ten Cities Events by Genre"

        genre_countries_titles['scaleX'] = "Cities"
        genre_countries_titles['stacked'] = "true"
        top_cities_name = DjVenues.objects.filter(country=selected_country_name).exclude(
            city__exact='').values_list('city', flat=True).annotate(e_count=Count('city')).order_by('-e_count')[:10]
        top_cities_name = list(top_cities_name)
        top_cities_name.sort()
        for city_name in top_cities_name:
            top_countries.append(str(city_name))
            venuesids = DjVenues.objects.filter(
                country=selected_country_name, city=city_name).values_list('venueid', flat=True)
            obj_dj_master = DjMaster.objects.filter(venueid__in=venuesids).values(
                'artistid__genre__genreName').annotate(g_count=Count('artistid__genre__genreName'))
            for genre_country in genre_countries:
                flag = True
                for dj_master in obj_dj_master:
                    if dj_master['artistid__genre__genreName'] == genre_country['text']:
                        genre_country['values'].append(dj_master['g_count'])
                        flag = False
                if flag == True:
                    genre_country['values'].append(0)

        genre_countries_title = str(selected_country_name).upper(
        ) + " > NUMBER OF EVENTS BY GENRE AS A PERCENTAGE"
        venuesids = DjVenues.objects.filter(
            country=selected_country_name).values_list('venueid', flat=True)
        obj_dj_master = DjMaster.objects.filter(venueid__in=venuesids).values(
            'artistid__genre__genreName').annotate(g_count=Count('artistid__genre__genreName'))[:5]
        for dj_master in obj_dj_master:
            genre_countries_pie.append({
                "text": str(dj_master['artistid__genre__genreName']),
                "values": [int(dj_master['g_count'])],
            })

        top_artist_countries = DjMaster.objects.filter(venueid__country=selected_country_name).values(
            'artistid').annotate(a_count=Count('artistid')).order_by('-a_count')[:10]

        if request.GET.get('genre'):
            genre_id = int(request.GET.get('genre'))
            genre_obj = Genre.objects.filter(genreID=genre_id).get()
            top_country_artist_title = str(selected_country_name).upper(
                ) + " > TOP TEN " + genre_obj.genreName.upper() + " ARTIST"
        else:
            top_country_artist_title = str(selected_country_name).upper(
            ) + " > TOP TEN ARTIST BY NUMBER OF EVENTS"

        for atrist in top_artist_countries:
            try:
                artist_object = Artist.objects.get(artistID=atrist['artistid'])
            except:
                pass
            top_country_artist.append({
                "values": [int(atrist['a_count'])],
                "text": str(artist_object.artistName)
            })

        if request.GET.get('city'):
            selected_country_name = countries.filter(
                Q(code=request.GET.get('country'))).get().name
            selected_city_name = City.objects.filter(Q(iso2=request.GET.get(
                'country'))).filter(Q(pk_city=request.GET.get('city'))).get().city
            genre_cities_title = str(selected_country_name).upper(
            ) + " > " + str(selected_city_name).upper() + " > TOP FIVE GENRES EVENTS"
            venuesids = DjVenues.objects.filter(
                country=selected_country_name, city=selected_city_name).values_list('venueid', flat=True)
            obj_dj_master = DjMaster.objects.filter(venueid__in=venuesids).values(
                'artistid__genre__genreName').annotate(g_count=Count('artistid__genre__genreName'))[:5]
            for dj_master in obj_dj_master:
                genre_cities.append({
                    "text": str(dj_master['artistid__genre__genreName']),
                    "values": [int(dj_master['g_count'])],
                })
    else:
        # genre_countries_titles['title'] = "Top Ten Countries Events by Genre"
        # genre_countries_titles['scaleX'] = "Countries"
        # genre_countries_titles['stacked'] = "true"
        # top_countries_name = DjVenues.objects.all().exclude(country__exact='').values_list('country',flat=True).annotate(e_count=Count('country')).order_by('-e_count')[:10]
        # top_countries_name = list(top_countries_name)
        # top_countries_name.sort()
        # for country_name in top_countries_name:
        #     top_countries.append(str(country_name))
        #     obj_dj_master = DjMaster.objects.filter(venueid__country=country_name).values('artistid__genre__genreName').annotate(g_count=Count('artistid__genre__genreName'))
        #     for genre_country in genre_countries:
        #         flag = True
        #         for dj_master in obj_dj_master:
        #             if dj_master['artistid__genre__genreName'] == genre_country['text']:
        #                 genre_country['values'].append(dj_master['g_count'])
        #                 flag = False
        #         if flag == True:
        #             genre_country['values'].append(0)
        genre_countries_titles, top_countries = get_top_10_country_events_by_genre(
            genre_countries)
    if request.GET.get('country') and request.GET.get('genre'):
        genre_id = int(request.GET.get('genre'))
        genreName = Genre.objects.filter(genreID=genre_id).get().genreName
        dj_countries_facebook_like_title = str(selected_country_name).upper(
        ) + " -> "+str(genreName).upper() + " - TOP 10 FACEBOOK DJ RANKING"
        created_at = FacebookLike.objects.filter(artist_id__genre__genreID=request.GET.get('genre'),
                                                 country_name=selected_country_name).count()
        if created_at:
            created_at = FacebookLike.objects.filter(artist_id__genre__genreID=request.GET.get('genre'), country_name=selected_country_name)\
                .order_by('-created_at')[0].created_at
            facebook_likes = FacebookLike.objects.filter(artist_id__genre__genreID=request.GET.get('genre'), country_name=selected_country_name, created_at=created_at).values(
                'artist_id__artistName').annotate(like_counts=Sum('like_count')).order_by('-like_counts')[:10]
            for facebook_like in facebook_likes:
                dj_countries_facebook_like_data.append({
                    "text": str(facebook_like['artist_id__artistName']),
                    "values": [int(facebook_like['like_counts'])],
                })
    elif request.GET.get('country'):
        dj_countries_facebook_like_title = str(
            selected_country_name).upper() + " - TOP 10 FACEBOOK DJ RANKING"
        created_at = FacebookLike.objects.filter(
            country_name=selected_country_name).count()
        if created_at:
            created_at = FacebookLike.objects.filter(country_name=selected_country_name).\
                order_by('-created_at')[0].created_at
            facebook_likes = FacebookLike.objects.filter(country_name=selected_country_name, created_at=created_at).values(
                'artist_id__artistName').annotate(like_counts=Sum('like_count')).order_by('-like_counts')[:10]
            for facebook_like in facebook_likes:
                dj_countries_facebook_like_data.append({
                    "text": str(facebook_like['artist_id__artistName']),
                    "values": [int(facebook_like['like_counts'])],
                })
    # elif request.GET.get('genre'):
    #     genre_id = int(request.GET.get('genre'))
    #     genreName = Genre.objects.filter(genreID=genre_id).get().genreName
    #     dj_countries_facebook_like_title = str(genreName).upper() + " - WORLDS TOP 10 FACEBOOK DJ RANKING"
    #     created_at = FacebookLike.objects.filter(artist_id__genre__genreID=\
    #         request.GET.get('genre')).count()
    #     if created_at:
    #         created_at = FacebookLike.objects.filter(artist_id__genre__genreID=\
    #             request.GET.get('genre')).order_by('-created_at')[0].created_at
    #         facebook_likes = FacebookLike.objects.filter(artist_id__genre__genreID=request.GET.get('genre'),created_at=created_at).values('artist_id__artistName').annotate(like_counts=Sum('like_count')).order_by('-like_counts')[:10]
    #         for facebook_like in facebook_likes:
    #             dj_countries_facebook_like_data.append({
    #                 "text": str(facebook_like['artist_id__artistName']),
    #                 "values": [int(facebook_like['like_counts'])],
    #             })
    else:
        dj_countries_facebook_like_data, dj_countries_facebook_like_title = get_top_10_facebook_djs(
            request)

    message = 'YOUR SEARCH RESULTS FOR '
    context = {}
    if request.GET.get('what'):
        what = request.GET.get('what')

        if (request.GET.get('entries')):
            entries = int(request.GET.get('entries'))
            if ((0 < entries < 30) or (30 < entries < 60) or (60 < entries < 120) or (120 < entries)):
                entries = 30
            events_limit = entries
        else:
            entries = 30
        if (request.GET.get('genre')):
            genre = int(request.GET.get('genre'))
            name = Genre.objects.filter(genreID=genre).get()
            message += ' > FROM ' + name.genreName + ' GENRE'
        else:
            genre = 0
        if (request.GET.get('budget')):
            budget = int(request.GET.get('budget'))
            name = "$" * budget
            if budget == 0:
                name = 'NOT SPECIFIED'
            message += ' > AT -' + name + '- BUDGET'
        else:
            budget = 0

        if request.GET.get('country'):
            country = request.GET.get('country')
            country_name = countries.filter(Q(code=country)).get().name
            message += ' COUNTRY > ' + country_name
            up_events = events(country_name)[0]
            past_events = events(country_name)[1]
            if request.GET.get('city'):
                cities = City.objects.filter(Q(iso2=country))
                city = request.GET.get('city')
                city_name = cities.filter(Q(pk_city=city)).get().city
                message += ' AND CITY > ' + city_name
                up_events = events(city_name, 0)[0]
                past_events = events(city_name, 0)[1]

            if request.GET.get('artist_name', ''):
                artist_name = request.GET.get('artist_name', '')
                message += ' AND ARTIST > ' + artist_name
                if request.GET.get('id', ''):
                    artist_id_get = request.GET.get('id', '')
                    up_events = up_events.filter(artistid=artist_id_get)
                    past_events = past_events.filter(artistid=artist_id_get)
                else:
                    up_events = up_events.filter(
                        artistid__artistName__icontains=artist_name)
                    past_events = past_events.filter(
                        artistid__artistName__icontains=artist_name)

        elif request.GET.get('territory'):
            territory = request.GET.get('territory')
            territory_name = territory_name.filter(
                Q(short_code=territory)).get().name
            message += ' TERRITORY > ' + territory_name
            territor = countries.filter(Q(territory_code=territory))
            territoriu = [item.name for item in territor]
            up_events = events(territoriu, 2)[0]
            past_events = events(territoriu, 2)[1]

        else:
            welcome = 1
            response = 1
            context = {
                "countries": countries,
                "territories": territories,
                "welcome": welcome,
                "response": response,
                'genres': filters()[0],
                'budget': filters()[1],
                'popularity': filters()[2],
                "genre_countries": genre_countries,
                "top_countries": top_countries,
                "genre_countries_titles": genre_countries_titles,
                "genre_cities": genre_cities,
                "genre_cities_title": genre_cities_title,
                "top_country_artist": top_country_artist,
                "top_country_artist_title": top_country_artist_title,
                "genre_countries_title": genre_countries_title,
                "genre_countries_pie": genre_countries_pie,
                "dj_countries_facebook_like_title": dj_countries_facebook_like_title,
                "dj_countries_facebook_like_data": dj_countries_facebook_like_data,
                'color_code': color_code,
                "show_loader": True,
            }

            return render(request, 'geography/territory.html', context)
        # filter upcoming events
        if up_events:
            if budget:
                up_events = up_events.filter(artistid__artistBudget=budget)
            if genre:
                up_events = up_events.filter(artistid__genre__genreID=genre)
        # filter past events
        if past_events:
            if budget:
                past_events = past_events.filter(artistid__artistBudget=budget)
            if genre:
                past_events = past_events.filter(
                    artistid__genre__genreID=genre)

            # if popularity:
            #     artists = up_events.values_list('artistid',flat=True)
            #     print artists
            #     artists = sort_by_popularity(artists, name.sourceName.lower())
            #     up_events = up_events.filter(artistid__in = artists)
            # if shuffle == 1:
            #     up_events = up_events.order_by('?')

        if request.is_ajax() and request.GET.get('start', ''):
            start = request.GET.get('start', '')
            action = request.GET.get('action', '')
            if action == 'upcoming':
                start = int(start)
                allevents = up_events[start:(start + events_limit)]
                all_events = event_unique_artist_event_date(list(allevents))
                context = {
                    "events": all_events,
                    "pro_obj": pro_obj
                }

                template = render_to_string(
                    "geography/get_events.html", context, context_instance=RequestContext(request))

                start = int(start) + events_limit
                if (len(allevents) < events_limit):
                    status = "False"  # No More events
                else:
                    status = "True"  # More events
                response = {
                    "status": status,
                    "start": start,
                    "data": template,
                }
                return HttpResponse(json.dumps(response), content_type="applicaton/json")

            else:
                start = int(start)
                allevents = past_events[start:(start + events_limit)]
                all_events = event_unique_artist_event_date(list(allevents))
                context = {
                    "events": all_events,
                    "pro_obj": pro_obj
                }

                template = render_to_string(
                    "geography/get_events.html", context, context_instance=RequestContext(request))

                start = int(start) + events_limit
                if (len(allevents) < events_limit):
                    status = "False"  # No More events
                else:
                    status = "True"  # More events
                response = {
                    "status": status,
                    "start": start,
                    "data": template,
                }
                return HttpResponse(json.dumps(response), content_type="applicaton/json")

        past_events = event_unique_artist_event_date(
            list(past_events[0:events_limit]))
        up_events = event_unique_artist_event_date(
            list(up_events[0:events_limit]))
        context = {
            "countries": countries,
            "territories": territories,
            'genres': filters()[0],
            'budget': filters()[1],
            "upcoming_events": up_events,
            "old_events": past_events,
            "message": message,
            "start": events_limit,
            "what": what,
            "pro_obj": pro_obj,
            "genre_countries": genre_countries,
            "top_countries": top_countries,
            "genre_countries_titles": genre_countries_titles,
            "genre_cities": genre_cities,
            "genre_cities_title": genre_cities_title,
            "top_country_artist": top_country_artist,
            "top_country_artist_title": top_country_artist_title,
            "genre_countries_title": genre_countries_title,
            "genre_countries_pie": genre_countries_pie,
            "dj_countries_facebook_like_title": dj_countries_facebook_like_title,
            "dj_countries_facebook_like_data": dj_countries_facebook_like_data,
            "facebook_like_data": facebook_like_data,
            'color_code': color_code,
            "show_loader": True,
        }

        if request.GET.get('country'):
            context.update({
                "country": country,
                "show_filter": True
            })
            context.update({"map": country_name})
            if request.GET.get('city'):
                context.update({"city": city})
            if request.GET.get('id') and request.GET.get('artist_name'):
                context.update({
                    'artist_name': artist_name,
                    'id': artist_id_get
                })
        elif request.GET.get('territory'):
            context.update({"territory": territory})
            context.update({"map": territory_name})
    else:
        welcome = 1
        context = {
            "countries": countries,
            "territories": territories,
            "welcome": welcome,
            'genres': filters()[0],
            'budget': filters()[1],
            'popularity': filters()[2],
            "genre_countries": genre_countries,
            "top_countries": top_countries,
            "genre_countries_titles": genre_countries_titles,
            "genre_cities": genre_cities,
            "genre_cities_title": genre_cities_title,
            "top_country_artist": top_country_artist,
            "top_country_artist_title": top_country_artist_title,
            "genre_countries_title": genre_countries_title,
            "genre_countries_pie": genre_countries_pie,
            "dj_countries_facebook_like_title": dj_countries_facebook_like_title,
            "dj_countries_facebook_like_data": dj_countries_facebook_like_data,
            "facebook_like_data": facebook_like_data,
            "color_code": color_code,
        }

    # This block save the user searches
    if request.GET.get('country') or request.GET.get('territory'):
        search_type = 'TerritorySearch'
        recentsearch = save_recent_search(request, search_type, message)

    context["chart_text"] = chart_text
    context["tab_label"] = tab_label
    return render(request, 'geography/territory.html', context)


def get_top_10_country_events_by_genre(genre_countries):

    genre_countries_titles = {}
    top_countries = []
    genre_countries_titles['title'] = "Top Ten Countries Events by Genre"
    genre_countries_titles['scaleX'] = "Countries"
    genre_countries_titles['stacked'] = "true"
    top_countries_name = DjVenues.objects.all().exclude(country__exact='').values_list(
        'country', flat=True).annotate(e_count=Count('country')).order_by('-e_count')[:10]
    top_countries_name = list(top_countries_name)
    top_countries_name.sort()
    for country_name in top_countries_name:
        top_countries.append(str(country_name))
        obj_dj_master = DjMaster.objects.filter(venueid__country=country_name).values(
            'artistid__genre__genreName').annotate(g_count=Count('artistid__genre__genreName'))
        for genre_country in genre_countries:
            flag = True
            for dj_master in obj_dj_master:
                if dj_master['artistid__genre__genreName'] == genre_country['text']:
                    genre_country['values'].append(dj_master['g_count'])
                    flag = False
            if flag == True:
                genre_country['values'].append(0)
    return genre_countries_titles, top_countries


def get_top_10_facebook_djs(request):

    dj_countries_facebook_like_title = ""
    dj_countries_facebook_like_data = []
    if request.GET.get('genre'):
        genre_id = int(request.GET.get('genre'))
        genreName = Genre.objects.filter(genreID=genre_id).get().genreName
        dj_countries_facebook_like_title = str(
            genreName).upper() + " - WORLDS TOP 10 FACEBOOK DJ RANKING"
        created_at = FacebookLike.objects.filter(
            artist_id__genre__genreID=request.GET.get('genre')).count()
        if created_at:
            created_at = FacebookLike.objects.filter(artist_id__genre__genreID=request.GET.get(
                'genre')).order_by('-created_at')[0].created_at
            facebook_likes = FacebookLike.objects.filter(artist_id__genre__genreID=request.GET.get('genre'), created_at=created_at).values(
                'artist_id__artistName').annotate(like_counts=Sum('like_count')).order_by('-like_counts')[:10]
            for facebook_like in facebook_likes:
                dj_countries_facebook_like_data.append({
                    "text": str(facebook_like['artist_id__artistName']),
                    "values": [int(facebook_like['like_counts'])],
                })

    else:
        created_at = FacebookLike.objects.all().count()
        dj_countries_facebook_like_title = "WORLDS TOP 10 FACEBOOK DJ RANKING"
        if created_at:
            created_at = FacebookLike.objects.all().order_by(
                '-created_at')[0].created_at
            facebook_likes = FacebookLike.objects.filter(created_at=created_at).values(
                'artist_id__artistName').annotate(like_counts=Sum('like_count')).order_by('-like_counts')[:10]
            for facebook_like in facebook_likes:
                dj_countries_facebook_like_data.append({
                    "text": str(facebook_like['artist_id__artistName']),
                    "values": [int(facebook_like['like_counts'])],
                })
    return dj_countries_facebook_like_data, dj_countries_facebook_like_title


def event_unique_artist_event_date(events):
    seen = []
    new_events = []
    for x in range(len(events)):
        try:
            if (events[x].artistid.artistID, str(events[x].eventid.date)) not in seen:
                seen.append((events[x].artistid.artistID,
                            str(events[x].eventid.date)))
                new_events.append(events[x])
        except:
            continue

    return new_events


@login_required
@check_email_validated
# @check_plan('Trial', 'Basic', 'Premium')
@check_plan('Trial', 'Promoter', 'Artist/DJ', 'Agent')
def territory(request):
    events_limit = 10
    context = {}
    countries = Country.objects.all().order_by("name")
    territories = territory_name = Territories.objects.all()
    pro_obj = Profile.objects.get(user=request.user)
    up_events = None
    past_events = None
    message = 'YOUR SEARCH RESULTS FOR'
    all_genre = Genre.objects.all()
    genre_countries = []
    genre_countries_titles = {}
    for genre in all_genre:
        genre_countries.append({
            "values": [],
            "text": str(genre.genreName)
        })
    dj_countries_facebook_like_data, dj_countries_facebook_like_title = get_top_10_facebook_djs(
        request)
    genre_countries_titles, top_countries = get_top_10_country_events_by_genre(
        genre_countries)

    if request.GET.get('territory_name', None):
        territory = request.GET.get('territory_name', None)
        territory_name = territory_name.filter(
            Q(short_code=territory)).get().name
        message += ' TERRITORY > ' + territory_name
        territor = countries.filter(Q(territory_code=territory))
        territoriu = [item.name for item in territor]
        up_events, past_events = events(territoriu, 2)
        # past_events = events(territoriu, 2)[1]

        if request.GET.get('artist_name', ''):
            artist_name = request.GET.get('artist_name', '')
            message += ' AND ARTIST > ' + artist_name
            if request.GET.get('id', ''):
                artist_id_get = request.GET.get('id', '')
                up_events = up_events.filter(artistid=artist_id_get)
                past_events = past_events.filter(artistid=artist_id_get)
                context.update({
                    'id': artist_id_get,
                })
            else:
                up_events = up_events.filter(
                    artistid__artistName__icontains=artist_name)
                past_events = past_events.filter(
                    artistid__artistName__icontains=artist_name)
            context.update({
                'artist_name': artist_name,
            })
        # past_events = [x for x in past_events if [(x.artistid, x.eventid.date)] not in seen and seen.append(tuple((x.artistid, x.eventid.date)))]

        context.update({
            "upcoming_events": up_events,
            "old_events": past_events,
            "territory": territory,
            "map": territory_name,
            "show_loader": True,
        })

    if (request.GET.get('entries')):
        entries = int(request.GET.get('entries'))
        if ((0 < entries < 30) or (30 < entries < 60) or (60 < entries < 120) or (120 < entries)):
            entries = 30
        events_limit = entries
    else:
        entries = 30

    if (request.GET.get('genre')):
        genre = int(request.GET.get('genre'))
        name = Genre.objects.filter(genreID=genre).get()
        message += ' > FROM ' + name.genreName + ' GENRE'
    else:
        genre = 0

    if (request.GET.get('budget')):
        budget = int(request.GET.get('budget'))
        name = "$" * budget
        if budget == 0:
            name = 'NOT SPECIFIED'
        message += ' > AT -' + name + '- BUDGET'
    else:
        budget = 0

    if up_events:
        if budget:
            up_events = up_events.filter(artistid__artistBudget=budget)
        if genre:
            up_events = up_events.filter(artistid__genre__genreID=genre)
    # filter past events

    if past_events:
        if budget:
            past_events = past_events.filter(artistid__artistBudget=budget)
        if genre:
            past_events = past_events.filter(artistid__genre__genreID=genre)

        if request.is_ajax() and request.GET.get('start', ''):
            start = request.GET.get('start', '')
            action = request.GET.get('action', '')
            if action == 'upcoming':
                start = int(start)
                allevents = up_events[start:(start + events_limit)]
                all_events = event_unique_artist_event_date(list(allevents))
                context = {
                    "events": all_events,
                    "pro_obj": pro_obj
                }

                template = render_to_string(
                    "geography/get_events.html", context, context_instance=RequestContext(request))

                start = int(start) + events_limit
                if (len(allevents) < events_limit):
                    status = "False"  # No More events
                else:
                    status = "True"  # More events
                response = {
                    "status": status,
                    "start": start,
                    "data": template,
                }
                return HttpResponse(json.dumps(response), content_type="applicaton/json")

            else:
                start = int(start)
                allevents = past_events[start:(start + events_limit)]
                all_events = event_unique_artist_event_date(list(allevents))
                context = {
                    "events": all_events,
                    "pro_obj": pro_obj
                }

                template = render_to_string(
                    "geography/get_events.html", context, context_instance=RequestContext(request))

                start = int(start) + events_limit
                if (len(allevents) < events_limit):
                    status = "False"  # No More events
                else:
                    status = "True"  # More events
                response = {
                    "status": status,
                    "start": start,
                    "data": template,
                }

                return HttpResponse(json.dumps(response), content_type="applicaton/json")

        if not request.is_ajax() and not request.GET.get('start', ''):
            past_events = event_unique_artist_event_date(
                list(past_events[0:events_limit]))
            up_events = event_unique_artist_event_date(
                list(up_events[0:events_limit]))

    context.update({
        "message": message,
        "territories": territories,
        'genres': filters()[0],
        'budget': filters()[1],
        "start": events_limit,
        "dj_countries_facebook_like_title": dj_countries_facebook_like_title,
        "dj_countries_facebook_like_data": dj_countries_facebook_like_data,
        "genre_countries_titles": genre_countries_titles,
        "top_countries": top_countries,
        "genre_countries": genre_countries
    })

    return render(request, "geography/territory_search.html", context)


@login_required
@check_email_validated
#@check_plan('Trial','Basic','Premium')
@check_plan('Trial', 'Artist/DJ', 'Promoter', 'Agent')
def territory_map(request):

    country = request.GET.get('country')
    city = request.GET.get('city')
    territory = request.GET.get('territory')
    if country:
        country_name = Country.objects.filter(Q(code=country)).get().name
        up_events, past_events = events(country_name, limit=50)
        #past_events = events(country_name)[1]
        if city:
            cities = City.objects.filter(Q(iso2=country))
            city = request.GET.get('city')
            city_name = cities.filter(Q(pk_city=city)).get().city
            up_events, past_events = events(city_name, 0, limit=50)
            #past_events = events(city_name, 0)[1]
    elif territory:
        territory = request.GET.get('territory')
        territor = Country.objects.filter(Q(territory_code=territory))
        territoriu = [item.name for item in territor]
        up_events, past_events = events(territoriu, 2, limit=50)
        #past_events = events(territoriu, 2)[1]
    if country or territory:
        data_list = []
        up_events = list(up_events)
        for event in past_events:
            up_events.append(event)
        all_events = []
        for event in up_events:

            artist_info = {}
            try:
                artist_info['id'] = event.artistid.artistID
            except:
                continue
            try:
                sourceImage = event.artistid.artistPhoto.url
            except Exception as e:
                sourceImage = None
            if sourceImage == None:
                try:
                    sourceImage = ScrapePhoto.objects.get(
                        artistID=event.artistid.artistID).flipImage
                    sourceImage = "/media%s" % (sourceImage)
                except Exception as e:
                    sourceImage = ""

            artist_info['artist'] = event.artistid.artistName
            artist_info['artist_image'] = sourceImage
            e = {}
            e['eventID'] = event.eventid.eventID
            e['eventName'] = event.eventid.eventName
            e['date'] = str(event.eventid.date)
            e['startTime'] = event.eventid.startTime
            e['endTime'] = event.eventid.endTime
            if event.eventid.date < datetime.now().date():
                e['eventType'] = "past"
            else:
                e['eventType'] = "future"
            v = {}
            v['venueID'] = str(event.venueid.venueid)
            v['venueName'] = event.venueid.venuename
            v['venueAddress'] = event.venueid.venueaddress
            if event.venueid.venuelatitude == 0E-9:
                v['venueLatitude'] = "0.000000000"
            else:
                v['venueLatitude'] = str(event.venueid.venuelatitude)
            if event.venueid.venuelongitude == 0E-9:
                v['venueLongitude'] = "0.000000000"
            else:
                v['venueLongitude'] = str(event.venueid.venuelongitude)
            v['city'] = event.venueid.city
            v['country'] = event.venueid.country
            event_dict = {}
            event_dict['artistDetail'] = artist_info
            event_dict['eventDetail'] = e
            event_dict['venueDetail'] = v
            all_events.append(event_dict)
        data = {}
        data['events'] = all_events
        data_list.append(data)
    else:
        data_list = []

    # print 'DTAA:',data_list
    return HttpResponse(json.dumps(data_list))


@login_required
@check_email_validated
#@check_plan('Trial','Basic','Premium')
@check_plan('Trial', 'Artist/DJ', 'Promoter', 'Agent')
def territory_map_frame(request, territory, city):
    if len(territory) == 2:
        country = territory
        country_name = Country.objects.filter(Q(code=country)).get().name
        up_events = events(country_name)[0]
        past_events = events(country_name)[1]
        if city:
            cities = City.objects.filter(Q(iso2=country))
            city_name = cities.filter(Q(pk_city=city)).get().city
            up_events = events(city_name, 0)[0]

        context = {
            'upcoming_events': up_events,
            'territory': country,
            'city': city,
        }
    if len(territory) == 3:
        terr = territory
        territoriu = Country.objects.filter(Q(territory_code=terr))
        territor = [item.name for item in territoriu]
        up_events = events(territor, 2)[0]
        context = {
            'upcoming_events': up_events,
            'territory': terr,
            'city': '',
        }

    return render(request, 'geography/territory_map.html', context)


@login_required
@check_email_validated
# @check_plan('Trial','Premium')
@check_plan('Trial', 'Artist/DJ', 'Agent')
def favorite_venue(request):
    response = {"status": False, "errors": []}

    if request.is_ajax() and request.POST['action'] == 'create_favorite_venue':
        venue_id = request.POST['id']
        venue = DjVenues.objects.get(venueid=venue_id)
        profile = Profile.objects.get(user=request.user)
        profile.favorite_venues.add(venue)
        profile.favorite_venues.all()
        profile.save()

    if request.is_ajax() and request.POST['action'] == 'remove_favorite_venue':
        venue_id = request.POST['id']
        venue = DjVenues.objects.get(venueid=venue_id)
        profile = Profile.objects.get(user=request.user)
        profile_fav_venue = ProfilesProfileFavoriteVenues.objects.filter(
            profile=profile, djvenues=venue).first()
        profile_fav_venue.uservenuecontact_set.all().delete()
        profile.favorite_venues.remove(venue)
        profile.save()

    response["status"] = True
    response["id"] = venue_id
    response["venue"] = venue.venuename
    return HttpResponse(json.dumps(response))


@login_required
@check_email_validated
@check_plan('Trial', 'Artist/DJ', 'Agent')
def add_remove_shortlist_venue(request):
    response = {"status": False, "errors": []}

    if request.is_ajax() and request.POST['action'] == 'create_shortlist_venue':
        venue_id = request.POST['id']
        venue = DjVenues.objects.get(venueid=venue_id)
        artist_id = request.POST.get('artist_id')
        add_venue(request, venue, artist_id)

    if request.is_ajax() and request.POST['action'] == 'remove_shortlist_venue':
        venue_id = request.POST['id']
        venue = DjVenues.objects.get(venueid=venue_id)
        artist_id = request.POST.get('artist_id')
        remove_venue(request, venue, artist_id)

    artist_id = request.POST.get('artist_id')
    artist = Artist.objects.get(artistID=request.POST.get('artist_id'))
    all_venues = []
    if artist.shortlistedvenue_set.all().first():
        all_venues = artist.shortlistedvenue_set.all().first().venueids.all()

    response["all_venues"] = len(all_venues)
    response["status"] = True
    response["id"] = venue_id
    response["venue"] = venue.venuename
    return HttpResponse(json.dumps(response))


@login_required
@check_email_validated
# @check_plan('Trial','Premium')
@check_plan('Trial', 'Agent', 'Artist/DJ')
def my_favorite_venue(request):
    order_by = request.GET.get("order_by", "country")
    events_limit = 10
    pro_obj = Profile.objects.get(user=request.user)
    if pro_obj.user_types == "Promoter" and not request.user.is_superuser:
        return redirect("/")

    all_venues_object = list(pro_obj.favorite_venues.all().order_by(order_by))
    artist_performing_on_venue = []
    now_date = datetime.now().date()

    for venue in all_venues_object:
        artists_id = DjMaster.objects.filter(venueid=venue).filter(
            eventid__date__lt=now_date).order_by('-eventid__date').values_list('artistid', flat=True)[:5]
        artists = []
        for id in artists_id:
            try:
                temp_artist = Artist.objects.get(artistID=int(id))
                if temp_artist not in artists:
                    artists.append(temp_artist)
            except Exception:
                pass

        artist_performing_on_venue.append(artists)

    all_venues = zip(all_venues_object, artist_performing_on_venue)

    if request.is_ajax() and request.method == "POST":
        start = request.POST.get('start')
        all_venues = all_venues[: int(start) + events_limit]
        user_email = request.user.email
        if user_email:
            msg = SendEmail(request)
            msg.send_by_template([user_email], "geography/email/fav_venues_email.html",
                                 context={
                'all_venues': all_venues,
                "site_url": settings.SITE_URL,
            },
                subject="Favorite Venues Details - Wheredjsplay.com"
            )
            response = {
                "status": True,
            }
            return HttpResponse(json.dumps(response), content_type="applicaton/json")

    if request.is_ajax() and request.GET.get('start', ''):
        start = request.GET.get('start', '')
        action = request.GET.get('action', '')
        if action == 'upcoming':
            start = int(start)
            allvenues = all_venues[start:(start + events_limit)]

            context = {
                "allvenues": allvenues,
                "pro_obj": pro_obj,
            }

            template = render_to_string(
                "geography/get_favorite_venues.html", context, context_instance=RequestContext(request))

            start = int(start) + events_limit
            if (len(allvenues) < events_limit):
                status = "False"  # No More events
            else:
                status = "True"  # More events
            response = {
                "status": status,
                "start": start,
                "data": template,
            }
            return HttpResponse(json.dumps(response), content_type="applicaton/json")

    context = {
        'all_venues': all_venues[0:events_limit],
        "pro_obj": pro_obj,
        "start": events_limit
    }

    return render(request, 'geography/favorite_venues.html', context)


def my_favorite_venue_xls(request):

    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="fav_venues.xls"'

    wb = Workbook()
    sheet1 = wb.add_sheet('sheet 1')

    user = request.user
    pro_obj = user.profile_set.all()
    profile = pro_obj[0]

    all_venues_object = list(profile.favorite_venues.all().order_by("country"))

    columns = [
        (u"Venue Name", 8000),
        (u"Address", 8000),
        (u"Country", 8000),
        (u"City", 8000),
        (u"Artist Who Performed", 8000),
        # (u"Contacts",10000)
        (u"Contact 1 First Name ", 10000),
        (u"Contact 1 Email", 10000),
        (u"Contact 2 First Name ", 10000),
        (u"Contact 2 Email", 10000),
    ]

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    row_num = 0

    for col_num in xrange(len(columns)):
        sheet1.write(row_num, col_num, columns[col_num][0], font_style)
        sheet1.col(col_num).width = columns[col_num][1]

    now_date = datetime.now().date()

    artist_performing_on_venue = []

    for venue in all_venues_object:
        artists_id = DjMaster.objects.filter(venueid=venue).filter(eventid__date__lt=now_date).\
            order_by('-eventid__date').values_list('artistid', flat=True)[:5]
        artists = []
        for id in artists_id:
            try:
                temp_artist = Artist.objects.get(artistID=int(id))
                artists.append(temp_artist)
            except Exception:
                pass

        artist_performing_on_venue.append(artists)

    all_venues = zip(all_venues_object, artist_performing_on_venue)

    for venue in all_venues:
        xls_contact = []
        all_contacts = []

        artistname_list = []

        for artist in venue[1]:

            artistname_list.append(artist.artistName)
            artist_name = ",".join(artistname_list)

        row_num += 1
        row = [
            venue[0].venuename,
            venue[0].venueaddress,
            venue[0].country,
            venue[0].city,
            # artist_name
        ]

        if artist_name:
            row.append(artist_name)

        fav_venue_obj = ProfilesProfileFavoriteVenues.objects.filter(
            profile=profile, djvenues=venue[0]).first()
        contact_obj = fav_venue_obj.uservenuecontact_set.all()

        for contact in contact_obj:
            contact_detail = {
                "first_name": contact.name,
                "last_name": contact.last_name,
                "email": contact.email,
                "facebook_personal_Link ": contact.facebook_link,
                "facebook_event_Link": contact.facebook_event_link,
                "notes": contact.notes
            }
            all_contacts.append(contact_detail)

        # for contact in all_contacts:
        #     xls_contact.append("( first name = ")
        #     xls_contact.append(contact["first_name"])
        #     xls_contact.append(", email = ")
        #     xls_contact.append(contact["email"])
        #     xls_contact.append("),")
        #
        # row.append(xls_contact)

        for contact in all_contacts[:2]:
            # xls_contact.append(contact["first_name"])
            # xls_contact.append(contact["email"])

            row.append(contact["first_name"])
            row.append(contact["email"])

        for col_num in xrange(len(row)):
            sheet1.write(row_num, col_num, row[col_num])

    wb.save(response)
    return response


@login_required
@check_email_validated
def venue_contacts(request, venue_id):
    context = {}
    user = request.user

    loged_in_profile = Profile.objects.get(user=user)

    djvenue = DjVenues.objects.filter(venueid=venue_id)

    profile_venue = ProfilesProfileFavoriteVenues.objects.filter(
        djvenues=djvenue[0], profile=loged_in_profile)

    venue_contacts = profile_venue[0].uservenuecontact_set.all()

    context.update({
        "djvenue": djvenue[0],
        "venue_id": venue_id,
        "venue_contacts": venue_contacts
    })

    if request.method == "POST":
        form = UserVenueContactForm(request.POST or None)
        if form.is_valid():
            contact = UserVenueContact()
            contact.name = form.cleaned_data['name']
            contact.last_name = form.cleaned_data['last_name']
            contact.email = form.cleaned_data['email']
            contact.facebook_link = form.cleaned_data['facebook_link']
            contact.facebook_event_link = form.cleaned_data['facebook_event_link']
            contact.notes = form.cleaned_data['notes']
            contact.user_fav_venues = profile_venue[0]
            contact.save()
    else:
        form = UserVenueContactForm()

    context.update({
        "form": form
    })

    return render(request, "geography/venue_contacts.html", context)


def delete_contact(request, contact_id):

    contact = UserVenueContact.objects.get(id=contact_id)
    contact.delete()

    return redirect(request.META.get('HTTP_REFERER'))
