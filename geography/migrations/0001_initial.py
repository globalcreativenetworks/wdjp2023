# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-01-18 10:52
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('pk_city', models.AutoField(primary_key=True, serialize=False)),
                ('city', models.CharField(default='', max_length=33)),
                ('city_ascii', models.CharField(default='', max_length=39)),
                ('lat', models.DecimalField(decimal_places=9, max_digits=12)),
                ('lng', models.DecimalField(decimal_places=9, max_digits=12)),
                ('iso2', models.CharField(default='', max_length=3)),
                ('iso3', models.CharField(default='', max_length=3)),
            ],
            options={
                'db_table': 'cities',
                'verbose_name_plural': 'Cities',
            },
        ),
        migrations.CreateModel(
            name='Continent',
            fields=[
                ('code', models.CharField(max_length=2, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
            ],
            options={
                'db_table': 'continents',
                'verbose_name_plural': 'Continents',
            },
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('code', models.CharField(max_length=2, primary_key=True, serialize=False, verbose_name='Two-letter country code (ISO 3166-1 alpha-2)')),
                ('name', models.CharField(max_length=255, verbose_name='English country name')),
                ('full_name', models.CharField(max_length=255, verbose_name='Full English country name')),
                ('iso3', models.CharField(max_length=3, verbose_name='Three-letter country code (ISO 3166-1 alpha-3)')),
                ('number', models.IntegerField(default=0, verbose_name='Three-digit country number (ISO 3166-1 numeric)')),
                ('continent_code', models.ForeignKey(db_column='continent_code', on_delete=django.db.models.deletion.CASCADE, to='geography.Continent')),
            ],
            options={
                'db_table': 'countries',
                'verbose_name_plural': 'Countries',
            },
        ),
        migrations.CreateModel(
            name='Territories',
            fields=[
                ('short_code', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
            ],
            options={
                'db_table': 'territories',
                'verbose_name_plural': 'Territories',
            },
        ),
        migrations.AddField(
            model_name='country',
            name='territory_code',
            field=models.ForeignKey(db_column='territory_code', on_delete=django.db.models.deletion.CASCADE, to='geography.Territories'),
        ),
        migrations.AddField(
            model_name='city',
            name='continent_code',
            field=models.ForeignKey(db_column='continent_code', on_delete=django.db.models.deletion.CASCADE, to='geography.Continent', verbose_name='code of continent'),
        ),
    ]
