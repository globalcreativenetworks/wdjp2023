from __future__ import unicode_literals

from django.db import models

from redactor.fields import RedactorField
from multiselectfield import MultiSelectField

from accounts.constants import USER_TYPES, EMAIL_CONDITIONS

# Create your models here.

class MarketingPages(models.Model):
    user_type = models.CharField(max_length=100, choices=USER_TYPES)
    login_stage = models.PositiveIntegerField()
    slider_order = models.PositiveIntegerField(default=1, verbose_name='slide no')
    page_title = models.CharField(max_length=200)
    internal_page_name = models.CharField(max_length=50, null=True, blank=True)
    content = RedactorField(verbose_name=u'add text, image, video. here size should be smaller then 100 kB')

    def __unicode__(self):
        return "login-stage - {} , slider_order {}".format(self.login_stage, self.slider_order)

class PublicMarketingPages(models.Model):
    user_type = models.CharField(max_length=100, choices=USER_TYPES)
    slide_number = models.PositiveIntegerField(default=1, verbose_name='slide no')
    page_title = models.CharField(max_length=200)
    Internal_title = models.CharField(max_length=50, null=True, blank=True)
    count = models.PositiveIntegerField(default=0)
    content = RedactorField(verbose_name=u'add text, image, video. here size should be smaller then 100 kB')

    def __unicode__(self):
        return "User Type - {} , Slide Number {}".format(self.user_type, self.slide_number)


class MarketingEmails(models.Model):
    email_type = models.CharField(max_length=120)
    user_type = MultiSelectField(choices=USER_TYPES, max_length=100)
    email_content = RedactorField(verbose_name=u'add text, image, video. here size should be smaller then 100 kB')
    email_subject = models.CharField(max_length=200)
    email_conditions = MultiSelectField(choices=EMAIL_CONDITIONS, max_length=100, max_choices=5, null=True, blank=True)
    dj_stage = models.IntegerField(null=True, blank=True)

    def __unicode__(self):
        return "Email Type - {} ".format(self.email_type)
