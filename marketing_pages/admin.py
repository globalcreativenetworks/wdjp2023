from django.contrib import admin

# Register your models here.

from django import forms
from redactor.widgets import RedactorEditor
from .models import MarketingPages
from profiles.models import Profile
from .models import MarketingPages, PublicMarketingPages, MarketingEmails


class MarketingPagesAdminForm(forms.ModelForm):
    class Meta:
        model = MarketingPages
        widgets = {
           'short_text': RedactorEditor(),
        }
        fields = '__all__'

class MarketingPagesAdmin(admin.ModelAdmin):
    form = MarketingPagesAdminForm
    list_display = ('user_type', 'login_stage', 'slider_order','internal_page_name')+ ('slide_link',)
    ordering = ('slider_order',)
    list_filter = ('user_type',)

    def slide_link(self, request):
        self_id = request.id

        slide = MarketingPages.objects.filter(id=self_id)
        if slide[0]:
            return '<a target="_blank" href="/marketing/{}/{}/{}/">Slide Link</a>'.format(slide[0].user_type, slide[0].login_stage, slide[0].slider_order)
        else:
            "No slide"

    slide_link.allow_tags = True
    slide_link.short_description = 'Slide Link'

class PublicMarketingPagesAdminForm(forms.ModelForm):
    class Meta:
        model = PublicMarketingPages
        widgets = {
           'short_text': RedactorEditor(),
        }
        fields = '__all__'



class PublicMarketingPagesAdmin(admin.ModelAdmin):
    form = PublicMarketingPagesAdminForm
    list_display = ('user_type', 'slide_number', 'page_title','Internal_title', 'count')+ ('slide_link',)
    ordering = ('slide_number',)
    list_filter = ('user_type',)

    def slide_link(self, request):
        self_id = request.id

        slide = PublicMarketingPages.objects.filter(id=self_id)
        if slide[0]:
            return '<a target="_blank" href="/marketing-campaign/{}/{}/">Slide Link</a>'.format(slide[0].user_type, slide[0].slide_number)
        else:
            "No slide"

    slide_link.allow_tags = True
    slide_link.short_description = 'Slide Link'


class MarketingEmailAdminForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(MarketingEmailAdminForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            self.fields['email_type'].widget.attrs['readonly'] = True

    class Meta:
        model = MarketingEmails
        widgets = {
           'short_text': RedactorEditor(),
        }
        fields = '__all__'

class MarketingEmailsAdmin(admin.ModelAdmin):
    form = MarketingEmailAdminForm
    # readonly_fields=('email_type',)
    list_display = ('email_type', 'user_type','email_subject', 'email_conditions')
    # ordering = ('slide_number',)
    # list_filter = ('user_type',)

    # def slide_link(self, request):
    #     self_id = request.id
    #
    #     slide = PublicMarketingPages.objects.filter(id=self_id)
    #     if slide[0]:
    #         return '<a target="_blank" href="/marketing-campaign/{}/{}/">Slide Link</a>'.format(slide[0].user_type, slide[0].slide_number)
    #     else:
    #         "No slide"
    #
    # slide_link.allow_tags = True
    # slide_link.short_description = 'Slide Link'

admin.site.register(MarketingPages, MarketingPagesAdmin)

admin.site.register(PublicMarketingPages, PublicMarketingPagesAdmin)

admin.site.register(MarketingEmails, MarketingEmailsAdmin)
