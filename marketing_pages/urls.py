from django.conf.urls import url


from marketing_pages import views

urlpatterns = [
    url(r'^marketing/$', views.marketing_dashboard , name="marketing_dashboard"),
    url(r'^marketing/(?P<user_type>.+)/(?P<login_stage>\d+)/(?P<slider_order>\d+)/$', views.marketing_pages, name="marketing_pages"),
    url(r'^marketing-campaign/(?P<user_type>.+)/(?P<slide_number>\d+)/$', views.marketing_campaign, name="marketing_campaign"),
    url(r'^internal-emails/$', views.internal_emails , name="internal_emails"),

   ]
