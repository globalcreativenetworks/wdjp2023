from __future__ import unicode_literals

from django.apps import AppConfig


class MarketingPagesConfig(AppConfig):
    name = 'marketing_pages'
