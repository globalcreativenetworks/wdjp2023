import sys, os
import json
import datetime
import pytz

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from django.conf import settings
from django.template.loader import render_to_string

from accounts.utils import SendEmail
from marketing_pages.models import MarketingEmails
from accounts.constants import user_types_list


class Command(BaseCommand):
    help = "Run Marketing Email Process"

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        all_email = MarketingEmails.objects.all()

        users = User.objects.filter(id__in=[11, 155, 91])
        # users = User.objects.all()

        now = datetime.datetime.now()
        utc=pytz.UTC
        utc_now = utc.localize(now)

    def send_email_for_not_loged(users, user_types_list, email, day):
        for user in users:
            profile = user.profile_set.all()[0]
            user_type = profile.user_types
            user_login_count = profile.login_count
            content = email.email_content
            for u_type in user_types_list:
                if u_type in email.user_type :
                    if user_type == u_type :
                        # <img src="/media/uploads/wdjp_user_survey.png" data-image="wdjp_user_survey.png">
                        if utc_now - user.last_login >= datetime.timedelta(day):
                            site_url = settings.SITE_URL
                            content = content = email.email_content
                            content = content.replace("{username}", user.username)
                            content = content.replace('src="', 'src="'+site_url+'')
                            if email.dj_stage:
                                if email.dj_stage == user_login_count:
                                    msg = SendEmail()
                                    msg.send_by_template(settings.DEFAULT_ADMIN_EMAILS, "marketing_pages/not_loged_5.html",
                                        context={
                                            "content":content,
                                            "site_url": settings.SITE_URL
                                            # "content":content.replace("{username}", user.username),
                                        },
                                        subject = email.email_subject
                                    )
                            else:
                                msg = SendEmail()
                                msg.send_by_template(settings.DEFAULT_ADMIN_EMAILS, "marketing_pages/not_loged_5.html",
                                    context={
                                        "content":content,
                                        "site_url": settings.SITE_URL
                                    },
                                    subject = email.email_subject
                                )

    for email in all_email:

        if email.email_type == "dj_stage_1_not_logged_5" :
            send_email_for_not_loged(users, user_types_list, email, day=5)
            print "dj_stage_1_not_logged_5 ++++++++++++++++++++++++++++++++++++++ "

        if email.email_type == "dj_stage_2_not_logged_5" :
            send_email_for_not_loged(users, user_types_list, email, day=5)

        if email.email_type == "dj_stage_3_not_logged_5" :
            send_email_for_not_loged(users, user_types_list, email, day=5)
