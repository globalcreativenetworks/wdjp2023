# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2018-02-10 14:23
from __future__ import unicode_literals

from django.db import migrations, models
import multiselectfield.db.fields
import redactor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('marketing_pages', '0003_auto_20171117_1244'),
    ]

    operations = [
        migrations.CreateModel(
            name='MarketingEmails',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email_type', models.CharField(max_length=120)),
                ('user_type', multiselectfield.db.fields.MultiSelectField(choices=[(b'Promoter', b'Promoter'), (b'Agent', b'Agent'), (b'Artists/DJs', b'Artists/DJs'), (b'PressFanRecord', b'PressFanRecord'), (b'Companies', b'Companies'), (b'Management', b'Management')], max_length=62)),
                ('email_content', redactor.fields.RedactorField(verbose_name='add text, image, video. here size should be smaller then 100 kB')),
                ('email_subject', models.CharField(max_length=200)),
                ('email_conditions', multiselectfield.db.fields.MultiSelectField(choices=[(b'2_5', b'2_5'), (b'3_5', b'3_5')], max_length=7)),
            ],
        ),
    ]
