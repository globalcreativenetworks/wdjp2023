from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import get_object_or_404

from accounts.decorators import check_email_validated
from marketing_pages.models import MarketingPages, PublicMarketingPages

@login_required
@check_email_validated
def marketing_dashboard(request):

    next = request.GET.get('next', '')

    profile = request.user.profile_set.first()
    login_count = profile.login_count
    user_type = profile.user_types

    marketing_page = MarketingPages.objects.filter(
        user_type=user_type,
        login_stage=login_count,
    ).order_by('slider_order')

    if marketing_page:
        marketing_page = marketing_page[0]
        return redirect(reverse('marketing_pages',
            args = (
                user_type,
                int(login_count),
                int(marketing_page.slider_order),
            )
        ))
    else:
        if login_count == 1:
            # if no slides for a User Type in Backend
            return redirect('introduction')

        if next:
            # Redirect User to next Page
            return redirect(next)

        return redirect(reverse('dashboard'))


@login_required
@check_email_validated
def marketing_pages(request, user_type, login_stage, slider_order):

    marketing_page = get_object_or_404(MarketingPages,
        user_type=user_type,
        login_stage=login_stage,
        slider_order=slider_order,
        )

    # marketing_page = MarketingPages.objects.get(
    #     user_type=user_type,
    #     login_stage=login_stage,
    #     slider_order=slider_order,
    # )
    #

    other_link = MarketingPages.objects.filter(
        user_type=user_type,
        login_stage=login_stage,
    ).order_by('slider_order')

    next_link = other_link.filter(slider_order__gt=slider_order).order_by('slider_order')
    if next_link.exists():
        next_link = next_link[0]
    else:
        next_link = None

    previous_link = other_link.filter(slider_order__lt=slider_order).order_by('-slider_order')
    if previous_link.exists():
        previous_link = previous_link[0]
    else:
        previous_link = None

    all_marketing_pages_id = other_link.values_list('id', flat=True)

    return render(request, "marketing_pages/marketing_page.html", {
        "marketing_page": marketing_page,
        "next_link": next_link,
        "previous_link": previous_link,
        "all_marketing_pages_id": all_marketing_pages_id,
    })


def marketing_campaign(request, user_type, slide_number):
    public_marketing_page = get_object_or_404(PublicMarketingPages,
        user_type=user_type,
        slide_number=slide_number,
        )

    public_marketing_page.count += 1
    public_marketing_page.save()

    other_link = PublicMarketingPages.objects.filter(
        user_type=user_type,
    ).order_by('slide_number')

    next_link = other_link.filter(slide_number__gt=slide_number).order_by('slide_number')
    if next_link.exists():
        next_link = next_link[0]
    else:
        next_link = None

    previous_link = other_link.filter(slide_number__lt=slide_number).order_by('-slide_number')
    if previous_link.exists():
        previous_link = previous_link[0]
    else:
        previous_link = None

    all_public_marketing_pages_id = other_link.values_list('id', flat=True)

    return render(request, "marketing_pages/public_marketing_page.html", {
        "public_marketing_page": public_marketing_page,
        "next_link": next_link,
        "previous_link": previous_link,
        "all_public_marketing_pages_ids": all_public_marketing_pages_id,
    })


def internal_emails(request):
    context = {}
    return render(request, "marketing_pages/_base_internal_email.html", context)
