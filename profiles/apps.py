from __future__ import unicode_literals

from django.apps import AppConfig


class ProfileAppConfig(AppConfig):
    name = 'profiles'

    def ready(self):
        from profiles import signals
