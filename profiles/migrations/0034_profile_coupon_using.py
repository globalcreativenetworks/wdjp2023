# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-11-13 12:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0033_profile_nickname'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='coupon_using',
            field=models.BooleanField(default=False),
        ),
    ]
