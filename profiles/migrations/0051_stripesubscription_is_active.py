# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2019-09-20 13:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0050_auto_20190920_0708'),
    ]

    operations = [
        migrations.AddField(
            model_name='stripesubscription',
            name='is_active',
            field=models.BooleanField(default=True),
        ),
    ]
