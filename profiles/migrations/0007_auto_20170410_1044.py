# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-04-10 10:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0006_profile_favorite_venues'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='facebook_url',
            field=models.CharField(blank=True, max_length=300, null=True),
        ),
        migrations.AddField(
            model_name='profile',
            name='user_types',
            field=models.CharField(blank=True, max_length=300, null=True),
        ),
    ]
