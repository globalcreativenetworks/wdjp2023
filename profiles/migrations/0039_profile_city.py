# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2019-09-09 21:14
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('geography', '0001_initial'),
        ('profiles', '0038_auto_20190907_1728'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='city',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='geography.City'),
        ),
    ]
