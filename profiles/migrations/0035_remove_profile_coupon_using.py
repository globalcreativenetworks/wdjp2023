# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-11-13 13:02
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0034_profile_coupon_using'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='coupon_using',
        ),
    ]
