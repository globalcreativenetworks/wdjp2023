# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-02-10 12:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0002_wpmyevents_invited_users'),
        ('profiles', '0005_auto_20170201_1022'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='favorite_venues',
            field=models.ManyToManyField(blank=True, null=True, to='events.DjVenues'),
        ),
    ]
