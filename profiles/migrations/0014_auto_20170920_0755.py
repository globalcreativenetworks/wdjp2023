# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-09-20 07:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0013_coupon'),
    ]

    operations = [
        migrations.AddField(
            model_name='coupon',
            name='coupon_name',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='coupon',
            name='coupon_code',
            field=models.CharField(blank=True, max_length=15, null=True),
        ),
        migrations.AlterField(
            model_name='coupon',
            name='coupon_time',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
