from django import forms
from django.contrib.auth.models import User
from django.forms import FileInput, Textarea
from .models import Profile, EMAIL_FREQUENCY_CHOICES, ProfileConnections
from ckeditor.widgets import CKEditorWidget
from accounts.constants import USER_TYPES


class FrequencyForm(forms.Form):
    email_frequency = forms.MultipleChoiceField(required=False, widget=forms.CheckboxSelectMultiple, choices=EMAIL_FREQUENCY_CHOICES,)


class ProfileForm(forms.ModelForm):
    signature = forms.CharField(widget=CKEditorWidget())
    # first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control','required':'required'}))
    # last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control','required':'required'}))
    # email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'py-form-control', 'required':'required', 'disabled':'disabled'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'py-form-control', 'required':'required'}))
    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'py-form-control', 'required':'required'}))

    class Meta:
        model = Profile
        # fields = ('image', 'country', 'state', 'artists_you_worked_with', 'artist_you_love',
        #     'signature', 'is_verified', 'company_organization', 'company_website', 'signatory_name',
        #     'mobile_no', 'contract_signatory_name', 'contact_no', 'address')
        fields = ('image', 'signature',)

        widgets = {
            'image': FileInput(),
        }

    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        self.fields['image'].widget.attrs.update({'class': 'py-form-control'})
        # self.fields['state'].widget.attrs.update({'class': 'py-form-control', 'required': 'required'})
        # self.fields['artists_you_worked_with'].widget.attrs.update({'class': 'py-form-control',  'cols': '100', 'rows': '5', 'required':'required'})
        # self.fields['artist_you_love'].widget.attrs.update({'class': 'py-form-control',  'cols': '100', 'rows': '5', 'required':'required'})
        self.fields['signature'].widget.attrs.update({'class': 'richtexteditor', 'cols': '40', 'rows': '5', 'required':'required'})
        # self.fields['is_verified'].widget.attrs.update({'class': 'py-form-control', 'required': 'required'})
        # self.fields['company_organization'].widget.attrs.update({'class': 'py-form-control', 'required': 'required'})
        # self.fields['company_website'].widget.attrs.update({'class': 'py-form-control', 'required': 'required'})
        # self.fields['signatory_name'].widget.attrs.update({'class': 'py-form-control', 'required': 'required'})
        # self.fields['mobile_no'].widget.attrs.update({'class': 'py-form-control', 'required': 'required'})
        # self.fields['contract_signatory_name'].widget.attrs.update({'class': 'py-form-control', 'required': 'required'})
        # self.fields['contact_no'].widget.attrs.update({'class': 'py-form-control', 'required': 'required'})
        # self.fields['country'].widget.attrs.update({'class': 'py-form-control', 'required': 'required'})
        # self.fields['address'].widget.attrs.update({'class': 'py-form-control', 'cols': '100', 'rows': '5', 'required': 'required'})
        # self.fields['email_frequency'].widget.attrs.update({'class': 'py-form-control', 'required':'required'})

    def clean_email(self):
        try:
            existing_user = User.objects.get(email__iexact=self.cleaned_data['email'])
            if existing_user:
                self._errors["email"] = self.error_class(["An account already exists under this email address."])
        except User.MultipleObjectsReturned:
            self._errors["email"] = self.error_class(["An account already exists under this email address."])
        except:
            pass
        return self.cleaned_data['email']

    def clean_confirm_password(self):
        cleaned_data = super(ProfileForm, self).clean()
        password = cleaned_data.get("password", "")
        confirm_password = cleaned_data.get("confirm_password", "")
        if password and confirm_password and not password == confirm_password:
            raise forms.ValidationError("Password did not mactch. Please retry.")
        return cleaned_data


class EditProfileForm(forms.ModelForm):
    signature = forms.CharField(required=False, widget=CKEditorWidget())
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control', 'required':'required'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control', 'required':'required'}))
    nick_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control', 'required':'required'}))
    #user_types = forms.ChoiceField(choices=USER_TYPES,widget=forms.TextInput(attrs={'class': 'py-form-control'}))
    # country = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control', 'required':'required'}))
    # email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'py-form-control', 'required':'required'}))
    # email_frequency = forms.MultipleChoiceField(required=False, widget=forms.CheckboxSelectMultiple, choices=EMAIL_FREQUENCY_CHOICES,)
    

    class Meta:
        model = Profile
        # fields = ('image', 'cnick_nameountry', 'state', 'artists_you_worked_with', 'artist_you_love',
        #     'signature', 'is_verified', 'company_organization','company_website', 'signatory_name',
        #     'mobile_no', 'contract_signatory_name', 'contact_no', 'address')
        fields = ('signature', 'nick_name',)

    def __init__(self, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)
        # self.fields['image'].widget.attrs.update({'class': 'py-form-control'})
        self.fields['signature'].widget.attrs.update({'class': 'form-control richtexteditor', 'cols': '55', 'rows': '5','required': False})
        #self.fields['user_types'].widget.attrs({'class': 'py-form-control', 'required':'required'})
        self.fields['nick_name'].widget.attrs.update({'class': 'py-form-control', 'required':'required'})
        # self.fields['state'].widget.attrs.update({'class': 'py-form-control', 'required': 'required'})
        # self.fields['artists_you_worked_with'].widget.attrs.update({'class': 'py-form-control',  'cols': '100', 'rows': '5', 'required':'required'})
        # self.fields['artist_you_love'].widget.attrs.update({'class': 'py-form-control',  'cols': '100', 'rows': '5', 'required':'required'})
        # self.fields['is_verified'].widget.attrs.update({'class': 'py-form-control', 'required': 'required'})
        # self.fields['company_organization'].widget.attrs.update({'class': 'py-form-control', 'required': 'required'})
        # self.fields['company_website'].widget.attrs.update({'class': 'py-form-control', 'required': 'required'})
        # self.fields['signatory_name'].widget.attrs.update({'class': 'py-form-control', 'required': 'required'})
        # self.fields['mobile_no'].widget.attrs.update({'class': 'py-form-control', 'required': 'required'})
        # self.fields['contract_signatory_name'].widget.attrs.update({'class': 'py-form-control', 'required': 'required'})
        # self.fields['contact_no'].widget.attrs.update({'class': 'py-form-control', 'required': 'required'})
        # self.fields['address'].widget.attrs.update({'class': 'py-form-control', 'cols': '100', 'rows': '5', 'required': 'required'})
        # self.fields['email_frequency'].widget.attrs.update({'class': 'py-form-control', 'required':'required'})
      


class CustomProfileAdminForm(forms.ModelForm):
    class Meta:
        model = Profile
        exclude = ("country_intrested_in",)

    def clean(self):
        user = self.cleaned_data.get('user')
        if self.instance:
            current_profile = Profile.objects.filter(user=user)
            if current_profile and current_profile[0] != self.instance:
                raise forms.ValidationError("A Profile Already Exist With this User")
        else:
            if Profile.objects.filter(user=user).exists():
                raise forms.ValidationError("User with this Profile Already Exist")
        return self.cleaned_data


class ProfileImageForm(forms.ModelForm):
    # IMAGE FIELDS FOR CROPPING
    x = forms.FloatField(widget=forms.HiddenInput())
    y = forms.FloatField(widget=forms.HiddenInput())
    width = forms.FloatField(widget=forms.HiddenInput())
    height = forms.FloatField(widget=forms.HiddenInput())
    
    class Meta:
        model = Profile
        fields = ('image','x', 'y', 'width', 'height')
        widgets = {
            'image': FileInput(),
        }

    def __init__(self, *args, **kwargs):
        super(ProfileImageForm, self).__init__(*args, **kwargs)

        # FIELDS FOR IMAGE CROP FUNCTIONALITY
        self.fields['image'].widget.attrs.update({'class': 'py-form-control', 'required': False})
        self.fields['x'].required = False
        self.fields['y'].required = False
        self.fields['width'].required = False
        self.fields['height'].required = False

class AddConnectionForm(forms.ModelForm):

    class Meta:
        model = ProfileConnections
        fields = ('connected_as', 'other_option','user_info','file1','file2')
        widgets = {
            'file1': FileInput(),
            'file2': FileInput(),
            'connected_as':forms.RadioSelect()
        }
    
    def __init__(self, *args, **kwargs):
        self.created = kwargs.get('instance', None)
        self.connection_type = kwargs.pop('connection_type', None)
        super(AddConnectionForm, self).__init__(*args, **kwargs)


        if self.created:
            self.fields['file1'].widget.attrs.update({'class': 'py-form-control'})
            self.fields['file2'].widget.attrs.update({'class': 'py-form-control'})    
        else:
            self.fields['file1'].widget.attrs.update({'class': 'py-form-control','required':'required', 'data-required-error':"Please upload an image in jpg/png format."})
            self.fields['file2'].widget.attrs.update({'class': 'py-form-control', 'required':'required', 'data-required-error':"Please upload an image in jpg/png format."})

        self.fields['other_option'].widget.attrs.update({'class': 'py-form-control'})
        self.fields['user_info'].widget.attrs.update({'class': 'py-form-control', 'required':'required', 'data-required-error':"Please provide contact details"})

        if self.connection_type == "Venue":
            self.fields['connected_as'].choices = ProfileConnections.VENUE_CONNECTION_TYPES
            self.fields['connected_as'].widget.attrs.update({'required':'required'})
            self.fields['connected_as'].initial = 'Owner'
        elif self.connection_type == "Event":
            self.fields['connected_as'].choices = ProfileConnections.EVENT_CONNECTION_TYPES
            self.fields['connected_as'].widget.attrs.update({'required':'required'})
            self.fields['connected_as'].initial = 'Owner'
        else:
            self.fields['connected_as'].choices = ProfileConnections.ARTIST_CONNECTION_TYPES
            self.fields['connected_as'].widget.attrs.update({'required':'required'})
            self.fields['connected_as'].initial = 'Manager'

class AddProfileConnectionApprovalForm(forms.ModelForm):
    approved = forms.CharField(widget=forms.HiddenInput())

    class Meta:
        model = ProfileConnections
        fields = ('comments','approved',)
    
    def __init__(self, *args, **kwargs):
        super(AddProfileConnectionApprovalForm, self).__init__(*args, **kwargs)
        self.fields['comments'].widget.attrs.update({'class': 'py-form-control','cols': '100', 'rows': '5'})
        self.fields['comments'].label = ''