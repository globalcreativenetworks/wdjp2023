from django.conf.urls import url
from profiles import views
from artist import views as artist_views

urlpatterns = [
   url(r'^my-search/$', views.my_recent_search, name="my_recent_search"),
   url(r'^load-more-search/$', views.load_more_search, name='load_more_search'),
   url(r'^signup/$', views.signup, name="signup"),
   url(r'^edit-details/$', views.edit_details, name="edit_details"),
   url(r'^user/(?P<user_id>\d+)/$', views.profile_show, name='profile_show'),
   url(r'^managed-djs/$', views.profile_managed_djs, name="profile_managed_djs"),
   url(r'^added-djs/$', views.added_djs, name="added_djs"),
   url(r'^profile-trackers/$', views.profile_trackers, name="profile_trackers"),
   url(r'^add-tracker/$', views.add_tracker, name="add_tracker"),
   url(r'^edit-tracker/(?P<tracker_id>\d+)/$', views.edit_tracker, name='edit_tracker'),
   url(r'^delete-tracker/(?P<tracker_id>\d+)/$', views.delete_tracker, name='delete_tracker'),
   url(r'^tracker-stats/(?P<tracker_id>\d+)/$', views.tracker_stats, name='tracker_stats'),
   url(r'^tracker-facebook-stats/(?P<tracker_id>\d+)/$', views.tracker_fb_stats, name='tracker_fb_stats'),
   url(r'^tracker-youtube-stats/(?P<tracker_id>\d+)/$', views.tracker_youtube_stats, name='tracker_youtube_stats'),
   url(r'^tracker-twitter-stats/(?P<tracker_id>\d+)/$', views.tracker_twitter_stats, name='tracker_twitter_stats'),
   url(r'^tracker-spotify-stats/(?P<tracker_id>\d+)/$', views.tracker_spotify_stats, name='tracker_spotify_stats'),
   url(r'^tracker-instagram-stats/(?P<tracker_id>\d+)/$', views.tracker_instagram_stats, name='tracker_instagram_stats'),
   url(r'^tracker-events/(?P<tracker_id>\d+)/$', views.tracker_events, name='tracker_events'),
   url(r'^add-connection-info/(?P<conn_type>[-\w]+)/(?P<type_id>\d+)/$', views.add_connection_info, name='add_connection_info'),
   url(r'^add-connection-form/(?P<conn_type>[-\w]+)/(?P<type_id>\d+)/$', views.add_connection_form, name='add_connection_form'),
   url(r'^add-connection-request/(?P<connection_id>\d+)/$', views.add_connection_approval, name='add_connection_approval'),
   url(r'^get-cities/$', views.get_cities, name='get_cities'),
   url(r'^update-profile-image/$', views.update_image, name='update_image'),
   url(r'^managed-artists/$', artist_views.profile_managed_artists, name='profile_managed_artists'),
   url(r'^managed-artist/$', artist_views.profile_managed_artist, name='profile_managed_artist'),
   

   
   
]
