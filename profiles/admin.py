from django.contrib import admin
from .models import Profile, RecentSearch, Subscription, NewsLetter, Coupon, UserVenueContact, ProfileTracker, CompareDjsProfile, Plan, AdminSettings, StripeSubscription, WebhookHistory, Coupon, CardDetails, ProfileConnections
from .models import UserType, Testimonial, TrustedBy
from .forms import CustomProfileAdminForm
from accounts.models import UserFrom


class ProfileAdmin(admin.ModelAdmin):
    def source(self, obj):
        user_source = UserFrom.objects.filter(user=obj.user).first()
        if user_source:
            return user_source.source

    form = CustomProfileAdminForm
    # list_display = ['user', 'state', 'country', 'artist_you_love', 'artists_you_worked_with', 'source',]
    list_display = ['user', 'state', 'country', 'source', 'user_types', 'plan', 'is_email_validated', 'is_trial_done']
    filter_horizontal = ['favorite_artist']
    exclude = ['favorite_artist', 'favorite_venues','favorite_events']

    search_fields = ['user__first_name', 'user__last_name']


# class CompareDjsProfileAdmin(admin.ModelAdmin):
# 	list_display = ['user',]
# 	search_fields = ['user',]


class RecentSearchAdmin(admin.ModelAdmin):
    list_display = ['profile', 'search_type', 'link', 'created']
    search_fields = ['search_type']


class SubscriptionAdmin(admin.ModelAdmin):
    list_display = ['user', 'start_time', 'end_time', 'plan']
    search_fields = ['user__username']


class NewsLetterAdmin(admin.ModelAdmin):
    list_display = ['sender','created','modified']
    search_fields = ['sender']


class PlanAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ['title', 'amount', 'stripe_plan_id']
        return []

    def has_delete_permission(self, request, obj=None):
        return True


class UserTypeAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return True


class TestimonialAdmin(admin.ModelAdmin):
    list_display = ['title','name','company']
    search_fields = ['title', 'name']


class TrustedByAdmin(admin.ModelAdmin):
    list_display = ['company']
    search_fields = ['company']


class SettingsAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class CouponAdmin(admin.ModelAdmin):
    exclude = ['coupon_time', 'stripe_coupon_code']
    
    def has_update_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class ProfileConnectionsAdmin(admin.ModelAdmin):
    list_display = ['requested_by', 'connection_type', 'status', 'comments', 'email_status']
    readonly_fields =['connection_type', 'artist', 'event', 'venue']
    exclude = ['modified_at']


admin.site.register(Profile, ProfileAdmin)
admin.site.register(RecentSearch, RecentSearchAdmin)
admin.site.register(Subscription, SubscriptionAdmin)
admin.site.register(NewsLetter,NewsLetterAdmin)
admin.site.register(UserVenueContact)
admin.site.register(ProfileTracker)
admin.site.register(Plan, PlanAdmin)
admin.site.register(UserType, UserTypeAdmin)
admin.site.register(Testimonial, TestimonialAdmin)
admin.site.register(TrustedBy, TrustedByAdmin)
admin.site.register(StripeSubscription)
admin.site.register(AdminSettings, SettingsAdmin)
admin.site.register(Coupon, CouponAdmin)
admin.site.register(WebhookHistory)
admin.site.register(CardDetails)
admin.site.register(ProfileConnections, ProfileConnectionsAdmin)
# admin.site.register(CompareDjsProfile, CompareDjsProfileAdmin)


# Register your models here.
