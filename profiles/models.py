from __future__ import unicode_literals
import django
import jsonfield
from decimal import Decimal
from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator

User._meta.get_field('email')._unique = True
from artist.models import Artist, Genre
from events.models import *
from geography.models import Country, City
from accounts.constants import USER_TYPES, COUPON_DURATION_TYPES
from ckeditor.fields import RichTextField
from accounts.payment_api import StripeAPIView


SEARCH_TYPE = (
    ('MasterSearch', 'MasterSearch'),
    ('FlightShareSearch', 'FlightShareSearch'),
    ('SomeOneMusicYouLove', 'SomeOneMusicYouLove'),
    ('DateSearch', 'DateSearch'),
    ('TerritorySearch', 'TerritorySearch'),
)

PLAN_TYPE = (
    ('Beta', 'Beta'),
    ('Trial', 'Trial'),
    ('Basic', 'Basic'),
    ('Premium', 'Premium'),
    ('Artist/DJ', 'Artist/DJ'),
    ('Promoter', 'Promoter'),
    ('Agent', 'Agent'),
)


EMAIL_FREQUENCY_CHOICES = (
    ('Monday', 'Monday'),
    ('Tuesday', 'Tuesday'),
    ('Wednesday', 'Wednesday'),
    ('Thursday', 'Thursday'),
    ('Friday', 'Friday'),
    ('Saturday', 'Saturday'),
    ('Sunday', 'Sunday'),
)

CONNECTION_TYPE = (
    ('Artist', 'Artist'),
    ('Event', 'Event'),
    ('Venue', 'Venue'),
)

BUTTON_STATE = (
    ('',            'enabled'),
    ('disabled',    'disabled')
)

class ProfilesProfileAddedDjs(models.Model):
    profile = models.ForeignKey('Profile', models.DO_NOTHING)
    artist = models.ForeignKey(Artist, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'profiles_profile_added_djs'
        unique_together = (('profile', 'artist'),)


class ProfilesProfileProfileManagedDjs(models.Model):
    profile = models.ForeignKey('Profile', models.DO_NOTHING)
    artist = models.ForeignKey(Artist, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'profiles_profile_profile_managed_djs'
        unique_together = (('profile', 'artist'),)


class ProfilesProfileFavoriteVenues(models.Model):
    profile = models.ForeignKey('Profile', models.DO_NOTHING)
    djvenues = models.ForeignKey(DjVenues, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'profiles_profile_favorite_venues'
        unique_together = (('profile', 'djvenues'),)

class UserVenueContact(models.Model):
    name = models.CharField(max_length=50, null=True, blank=True)
    last_name = models.CharField(max_length=50, null=True, blank=True)
    email = models.EmailField(blank=True, null= True)
    facebook_link = models.CharField(max_length=250, blank=True, null=True)
    facebook_event_link = models.CharField(max_length=250, blank=True, null=True)
    notes = models.TextField(null=True, blank=True)
    user_fav_venues = models.ForeignKey(ProfilesProfileFavoriteVenues, null=True, blank=True)

    def __unicode__(self):
        return u'%s' % (self.first_name)


class Coupon(models.Model):
    coupon_name = models.CharField(max_length=100, null=True, blank=True)
    coupon_code = models.CharField(max_length=15, null=True, blank=True, unique=True)
    coupon_time = models.IntegerField(null=True, blank=True, verbose_name="Enter the No. of Days for Coupon")
    is_live = models.BooleanField(default=True)
    stripe_coupon_code = models.CharField(max_length=100, null=True, blank=True)
    amount_off = models.IntegerField(null=True, blank=True,help_text="Example: If you type 10 and actual price is 50 USD it means the user will have to pay 50 USD - 10 USD i.e 40 USD")
    duration = models.CharField(max_length=50, choices=COUPON_DURATION_TYPES, null=True, blank=True, help_text="Note: Forever and repeating means the discount will be given on every billing cycle and once means it is a one time discount.")
  
    def __unicode__(self):
        return u'%s' % (self.coupon_name)

    def save(self, *args, **kwargs):
        stripe_obj = StripeAPIView()
        coupon = stripe_obj.create_coupon(duration=self.duration, amount_off=self.amount_off * 100, coupon_code=self.coupon_code, coupon_name=self.coupon_name)
        if coupon["status"] == 200:
            self.stripe_coupon_code = coupon["message"]["id"]
            super(Coupon, self).save(*args, **kwargs)


class Plan(models.Model):
    """
    This model class is intended for storing Website membership plans
    which can be found on HomePage. It has a detailed description of
    what a plan offers.
    """
    amount = models.DecimalField(decimal_places=2, max_digits=5, validators=[MinValueValidator(Decimal('0.01'))], default=0)
    title = models.CharField(max_length=100)
    stripe_plan_id = models.CharField(max_length=100, null=True, blank=True)
    user_type = models.CharField(max_length=300, choices=USER_TYPES, null=True, blank=True)
    description = models.TextField(help_text="Add plan description and what it includes row by row. You can use HTML to"
                                             " customize.", null=True)
    button_title = models.CharField(max_length=20, default="Sign up")
    button_state = models.CharField(max_length=10, choices=BUTTON_STATE, null= True, blank=True)
    button_url = models.CharField(max_length=100, default="#")
    plan_color = models.CharField(max_length=10, default='red', help_text="Add HTML color name or color code in HEX e.g: #000000")

    def __unicode__(self):
        return u'%s' % (self.title)


class UserType(models.Model):
    """
    This model class is intended for storing Users Types
    which can be found on HomePage and can be editable
    It should keep data about what a user can do and how
    the website helps it
    """
    icon = models.CharField(max_length=100, default="")
    title = models.CharField(max_length=100)
    description = models.TextField(help_text="Add user features description row by row. You can use HTML to"
                                             " customize.", null=True)
    button_title = models.CharField(max_length=40, default="Change text here")
    button_state = models.CharField(max_length=10, choices=BUTTON_STATE, null=True, blank=True)
    button_url = models.CharField(max_length=100, default="#")
    user_color = models.CharField(max_length=10, default='red',
                                  help_text="Add HTML color name or color code in HEX e.g: #000000")
    use_second_button = models.BooleanField(default=False)
    second_button_title = models.CharField(max_length=20, default="Change text here", null=True, blank=True)
    second_button_state = models.CharField(max_length=10, choices=BUTTON_STATE, null=True, blank=True)
    second_button_url = models.CharField(max_length=100, default="#", null=True, blank=True)

    def __unicode__(self):
        return u'%s' % (self.title)


class Testimonial(models.Model):
    """
    This model class is intended for storing Users Testimonials
    which can be found on HomePage
    """
    title = models.CharField(max_length=100)
    image = models.ImageField(upload_to='uploads/', blank=True, null=True)
    message = models.TextField(null=True)
    name = models.CharField(max_length=100)
    company = models.CharField(max_length=100)

    def __unicode__(self):
        return u'%s' % self.title


class TrustedBy(models.Model):
    """
    This model class aims to store companies/users well-known
    as a marketing tool for selling plans
    """

    company = models.CharField(max_length=100)
    logo = models.ImageField(upload_to='uploads/', blank=True, null=True)

    def __unicode__(self):
        return u'%s' % self.company


class AdminSettings(models.Model):
    free_trial = models.PositiveIntegerField(default=30)


class WebhookHistory(models.Model):
    event_name = models.CharField(max_length=500, null=True, blank=True)
    data = jsonfield.JSONField()

    def __str__(self):
        return self.event_name


class StripeSubscription(models.Model):
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    is_refund = models.BooleanField(default=False)
    refund_date = models.DateTimeField(null=True, blank=True )
    from_coupon = models.ForeignKey(Coupon, null=True, blank=True)
    stripe_subscription_id = models.CharField(max_length=100, null=True, blank=True)
    is_active = models.BooleanField(default=True)


class Profile(models.Model):

    @property
    def all_added_artists(self):
        return ProfilesProfileAddedDjs.objects.filter(profile=self)

    @property
    def all_managed_artists(self):
        return ProfilesProfileProfileManagedDjs.objects.filter(profile=self)

    user = models.ForeignKey(User, null=True, blank=True)
    dj_artist = models.OneToOneField(Artist, related_name="artist_dj", null=True, blank=True)
    image = models.ImageField(upload_to='userphotos/', blank=True, null=True)
    favorite_artist = models.ManyToManyField(Artist, null=True, blank=True)
    favorite_venues = models.ManyToManyField(DjVenues, null=True, blank=True)
    favorite_events = models.ManyToManyField(Event, null=True, blank=True)
    country = models.ForeignKey(Country, null=True,on_delete=django.db.models.deletion.DO_NOTHING)
    city = models.ForeignKey(City, null=True,on_delete=django.db.models.deletion.DO_NOTHING)
    state = models.CharField(max_length=50, null=True, blank=True)
    artists_you_worked_with = models.TextField(null=True, blank=True)
    artist_you_love = models.TextField(null=True, blank=True)
    # signature = models.TextField(null=True, blank=True)
    signature =  RichTextField(verbose_name='signature', null=True, blank=True)
    is_verified = models.BooleanField(default=False)
    company_organization = models.CharField(max_length=100, null=True, blank=True)
    company_website = models.CharField(max_length=100, null=True, blank=True)
    signatory_name = models.CharField(max_length=150, null=True, blank=True)
    mobile_no = models.CharField(max_length=20, null=True, blank=True)
    contract_signatory_name = models.CharField(max_length=100, null=True, blank=True)
    contact_no = models.CharField(max_length=20, null=True, blank=True)
    address = models.TextField(null=True, blank=True)
    is_premium = models.BooleanField(default=False)
    is_trial_done = models.BooleanField(default=False)
    is_trial_end_time = models.DateTimeField(null=True, blank=True)
    plan = models.CharField(max_length=255, choices=PLAN_TYPE, null=True, blank=True)
    stripe_plan = models.ForeignKey(Plan, null=True, blank=True)
    stripe_customer_id = models.CharField(max_length=100, null=True, blank=True)
    subscription = models.ForeignKey(StripeSubscription, null=True, blank=True)
    is_subscription_failed = models.BooleanField(default=False)
    facebook_url = models.CharField(max_length=300, null=True, blank=True)
    user_types = models.CharField(max_length=300, choices=USER_TYPES, null=True, blank=True)
    country_intrested_in = models.CharField(max_length=50, null=True, blank=True)
    email_frequency = models.CharField(max_length=100, default='Daily')
    login_count = models.IntegerField(default=0)
    genres = models.ManyToManyField(Genre, null=True, blank=True)
    shortlist_artist = models.ManyToManyField(Artist, related_name="shortlist_artist" ,null=True, blank=True)
    profile_managed_djs = models.ManyToManyField(Artist, related_name="profile_managed_djs", null=True, blank=True)
    added_djs = models.ManyToManyField(Artist, related_name="added_djs", null=True, blank=True)
    is_email_validated = models.BooleanField(default=False)
    nickname = models.CharField(max_length=50, null=True, blank=True)

    def __unicode__(self):
        return u'%s' % (self.user)

    def value(self):
        return dict(self.EMAIL_FREQUENCY_CHOICES).get(self.email_frequency)


class CompareDjsProfile(models.Model):
    user = models.ForeignKey(User, null=True, blank=True)
    user_types = models.CharField(max_length=300, choices=USER_TYPES, null=True, blank=True)
    login_count = models.IntegerField(default=0)

    def __unicode__(self):
        return u'%s' % (self.user)



class RecentSearch(models.Model):
    search_type = models.CharField(max_length=100, choices=SEARCH_TYPE)
    message = models.CharField(max_length=300, null=True, blank=True)
    link = models.CharField(max_length=300, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    profile = models.ForeignKey(Profile)


class Subscription(models.Model):
    user = models.ForeignKey(User)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    customer_id = models.CharField(max_length=50)
    plan = models.CharField(max_length=255, choices=PLAN_TYPE)
    is_refund = models.BooleanField(default=False)
    refund_date = models.DateTimeField(null=True, blank=True )
    from_coupon = models.BooleanField(default=False)

    def __str__(self):
        return "%s %s"%(self.user, self.end_time)


class NewsLetter(models.Model):
    sender = models.ForeignKey(Profile)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)


class ProfileTracker(models.Model):
    profile = models.ForeignKey(Profile)
    is_active = models.BooleanField(default=False)
    artist = models.ManyToManyField(Artist)
    tracker_name = models.CharField(max_length=100)

    def __unicode__(self):
        return u'%s' % (self.tracker_name)


class CardDetails(models.Model):
    card_number = models.CharField(max_length=4, blank=True)
    user = models.ForeignKey(Profile)
    holder_name = models.CharField(max_length=500)
    card_type = models.CharField(max_length=100)
    expiry_month = models.PositiveIntegerField()
    expiry_year = models.PositiveIntegerField()
    is_primary = models.BooleanField(default=False)
    stripe_card_id = models.CharField(max_length=500)

    def __str__(self):
        return self.holder_name

class ProfileConnections(models.Model):
    requested_by = models.ForeignKey(User)
    handled_by = models.ForeignKey(User, null=True, blank=True, related_name="connection_handled_by")
    artist = models.ForeignKey(Artist, related_name='artist_connection', blank=True, null=True)
    event = models.ForeignKey(Event, related_name='event_connection', blank=True, null=True)
    venue = models.ForeignKey(DjVenues, related_name='venue_connection', blank=True, null=True)
    connection_type = models.CharField(max_length=20, choices=CONNECTION_TYPE)
    StatusChoice = ((0, 'Pending'),(1, 'Approved'),(2, 'Rejected'))
    status = models.IntegerField(default=0, choices=StatusChoice)
    created_at  = models.DateTimeField(editable=False)
    modified_at = models.DateTimeField()
    comments = models.CharField(max_length=500, null=True, blank=True)
    EmailStatus = ((0, 'Not Sent'),(1, 'Sent'))
    email_status = models.IntegerField(default=0, choices=EmailStatus)
    ARTIST_CONNECTION_TYPES = (
        ('Manager','Manager'),
        ('Agent','Agent'),
        ('Fan','Fan'),
        ('Event Promoter','Event Promoter'),
        ('Tour Manager','Tour Manager'),
        ('Press Agent','Press Agent'),
        ('Other','Other')
    )
    VENUE_CONNECTION_TYPES  = (
        ('Owner','Owner'),
        ('Booker','Booker'),
        ('Staff','Staff'),
        ('Venue Promoter','Venue Promoter'),
        ('Press Agent','Press Agent'),
        ('Other','Other')
    )
    EVENT_CONNECTION_TYPES  = (
        ('Owner','Owner'),
        ('Booker','Booker'),
        ('Staff','Staff'),
        ('Event Promoter','Event Promoter'),
        ('Press Agent','Press Agent'),
        ('Other','Other')
    )
    CONNECTION_TYPES = (
        ('Manager','Manager'),
        ('Agent','Agent'),
        ('Fan','Fan'),
        ('Owner','Owner'),
        ('Booker','Booker'),
        ('Staff','Staff'),
        ('Event Promoter','Event Promoter'),
        ('Venue Promoter','Venue Promoter'),
        ('Tour Manager','Tour Manager'),
        ('Press Agent','Press Agent'),
        ('Other','Other')
    )
    connected_as = models.CharField(max_length=50, choices=CONNECTION_TYPES , blank=False, default='Manager')
    other_option = models.TextField(null=True, blank=True)
    user_info = models.TextField()
    file1 = models.ImageField(upload_to='uploads/', blank=True, null=True)
    file2 = models.ImageField(upload_to='uploads/', blank=True, null=True)


    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.status = 0
            self.email_status = 0
            self.created_at = timezone.now()

        self.modified_at = timezone.now()

        return super(ProfileConnections, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Connection'
        verbose_name_plural = 'Connections'
    
    def __str__(self):
        return "{} - Status: {} ".format(self.requested_by.username, self.status)

