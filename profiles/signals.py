import os, random, string
from django.db.models.signals import pre_save, post_save
from django.contrib.auth.models import User
from django.utils import timezone
from django.conf import settings

from profiles.models import Profile, Subscription, Coupon
from accounts.utils import SendEmail
from accounts.models import UserFrom, Applications


def create_sample_password():
    length = 13
    chars = string.ascii_letters + string.digits + '!@#$%^&*()'
    random.seed = (os.urandom(1024))

    return ''.join(random.choice(chars) for i in range(length))


def send_confirmation_email_user(sender, instance, **kwargs):
    if instance.pk:
        try:
            old = instance.__class__.objects.get(pk=instance.pk)
        except:
            old = None
        if old and not old.is_active == instance.is_active:
            if instance.is_active:
                password = create_sample_password()
                instance.set_password(password)

                #temp code until beta tester are using the website
                profiles = Profile.objects.filter(user=instance)
                if profiles:
                    profile = profiles[0]
                    profile.is_verified = True
                    profile.is_trial_end_time = timezone.now() + timezone.timedelta(days=15)
                    profile.save()
                    if profile.plan == "Beta":
                        profile.is_trial_end_time = timezone.now() + timezone.timedelta(days=15)
                    if profile.plan == "Basic":
                        coupon = Coupon.objects.get(id=1)
                        time = coupon.coupon_time
                        subscription = Subscription()
                        subscription.user = instance
                        subscription.start_time = timezone.now()
                        subscription.end_time = timezone.now() + timezone.timedelta(days=time)
                        subscription.plan = "Basic"
                        subscription.save()

                else:
                    profile = None

                email = SendEmail()
                email.send_by_template([instance.email], "profiles/email/approved_email.html",
                    context={
                        'user': instance,
                        'login_url': settings.SITE_URL + '/accounts/login-user/',
                        'profile': profile,
                        'password': password,
                        'site_url': settings.SITE_URL,
                    },
                    subject = "Your Invitation Is Approved Wheredjsplay.com"
                )


def create_profile_obj(sender, instance, *args, **kwargs):

    user_from = UserFrom.objects.filter(user=instance).first()

    application_obj = Applications.objects.filter(application_name="MusicRiver").first()

    if user_from:
        userfrom_all = user_from.user_from.all()
    else:
        userfrom_all = False

    if userfrom_all:
        if application_obj not in userfrom_all:
            if instance.pk:
                profile_objs = Profile.objects.get_or_create(user=instance)

                # if not profile_objs:
                #     obj = Profile.objects.create(user=instance)
                #     obj.is_verified = True
                #     obj.country = "India"
                #     obj.save()

            application = Applications.objects.filter(application_name="WhereDjsPlay").first()

            user, created = UserFrom.objects.get_or_create(user=instance)
            if created:
                user.user_from.add(application)
                user.source = "Wheredjsplay"
                user.save()



# pre_save.connect(send_confirmation_email_user, sender=User)


post_save.connect(create_profile_obj, sender=User)
