import requests
from django.contrib.gis.geoip import GeoIP
from django.contrib.auth import logout
from django.shortcuts import render_to_response, redirect, render
from django.core.urlresolvers import reverse

from accounts.models import UserFrom, Applications
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
import json


# from django.utils.deprecation import MiddlewareMixin


class ErrorFormatterMiddleware(object):

    def process_response(self, request, response):

        if response.status_code == 400 or response.status_code == 500:

            error_dict = {
                'status_code': response.status_code,
                'status_description': '',
                'status': False,
                'error': None
            }

            try:
                if 'detail' in response.data or 'error' in response.data or 'non_field_errors' in response.data:
                    non_field_errors = ''

                    if 'detail' in response.data:
                        non_field_errors = force_text(response.data['detail']) if not type(response.data['detail']) == list else force_text(response.data['detail'][0]) 

                    elif 'error' in response.data:
                        non_field_errors =  force_text(response.data['error']) if not type(response.data['error']) == list else force_text(response.data['error'][0]) 

                    elif 'non_field_errors' in response.data:
                        non_field_errors = force_text(response.data['non_field_errors']) if not type(response.data['non_field_errors']) == list else force_text(response.data['non_field_errors'][0]) 
                
                    error_dict.update({'error': non_field_errors})

                else:
                    field = {}
                    for k in response.data:
                        if not (k == 'detail') and not (k == 'error') and not (k == 'non_field_errors'):
                            field[k] = response.data[k]
                        if field:
                            error_dict['error'] = field
            except:
                pass

            if response.status_code == 400:
                status_description = "Bad Request"
            elif response.status_code == 500:
                status_description = "Internal Server Error"
            error_dict["status_description"] = status_description

            try:
                a = response.data
                a.update({
                    "code": response.data["status"]["code"],
                    "message": response.data["status"]["message"]["non_field_errors"][0],                   
                    "status_code": response.status_code
                })

            except:
                try:
                    message =  response.data["status"]["message"]
                    message_text = ""

                    for key, value in message.iteritems() :
                        field_name = str(value[0]).replace('This field', str(key))
                        message_text = message_text + field_name
                        message_text = message_text.replace(".", ",")

                    a.update({
                        "code": response.data["status"]["code"],
                        "message" : message_text,
                        "status_code": response.status_code
                        })
                except :
                    a = {}


            if a == {}:
                a.update({
                    "message" : "Input Value is missing",
                    "code": "Error"
                })

            response.content = json.dumps(a)

        return response




class CheckLoggedInUser(object):
    def process_response(self, request, response):
        try:
            if request.user.is_authenticated() and not request.user.is_superuser :
                user = request.user
                # compare_djs_application = Applications.objects.get(application_name="CompareDjs")
                wdjp_application = Applications.objects.get(application_name="WhereDjsPlay")
                user_from = UserFrom.objects.filter(user=user).first()
                if user_from:
                    user_applications = user_from.user_from.all()
                    if wdjp_application not in user_applications:
                        logout(request)
                        return redirect("/")
                    # if user_from.user_from == application :
                    #     logout(request)
                    #     return redirect("/")
                    else:
                        return response
            else:
                return response
        except:
            return response


class UserLocationCookieMiddleware(object):
    def process_response(self, request, response):

        if 'user_session_lat' in request.COOKIES and 'user_session_long' in request.COOKIES:
            user_lat = float(request.COOKIES.get('user_session_lat'))
            user_long = float(request.COOKIES.get('user_session_long'))
        else:
            user_lat = 51.509865  # Default Coordinates
            user_long = -0.118092

        geoip = GeoIP()
        if 'user_session_lat' not in request.COOKIES and 'user_session_long' not in request.COOKIES:
            ip = get_client_ip(request)
            if ip and ip != "127.0.0.1":
                try:
                    user_lat, user_long = geoip.lat_lon(ip)
                except:
                    pass

        try:
            if 'user_session_lat' not in request.COOKIES:
                response.set_cookie('user_session_lat', user_lat)
            if 'user_session_long' not in request.COOKIES:
                response.set_cookie('user_session_long', user_long)
        except Exception:
            pass

        try:
            if not request.COOKIES.get('user_session_city', ''):
                url = "https://maps.googleapis.com/maps/api/geocode/json?latlng={},{}&sensor=false&result_type=locality&key=AIzaSyA7xY6JgrR-hAOLs1ZmZU9vCoFNb_YQ7qI".format(user_lat, user_long)
                data = requests.get(url).json()
                city = data['results'][0]['formatted_address'].split(",")[0]
                response.set_cookie('user_session_city', city)
        except Exception:
            response.set_cookie('user_session_city', "")

        return response


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip
