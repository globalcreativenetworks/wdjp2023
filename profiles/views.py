# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
import mailchimp
from colorhash import ColorHash
from django.db.models import Q
from django.views.generic.edit import View
from django.contrib.auth.mixins import LoginRequiredMixin
from django.template.loader import render_to_string
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.conf import settings
from django.shortcuts import render, render_to_response, redirect
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse, Http404, HttpResponseRedirect, JsonResponse
from django.core.urlresolvers import reverse
from datetime import datetime as dt

from events.models import DjVenues, Event, WpMyEvents
from profiles.models import Profile, ProfileTracker, Subscription, Event, RecentSearch, ProfileConnections
from accounts.utils import SendEmail, create_username
from profiles.forms import ProfileForm, EditProfileForm, ProfileImageForm, AddConnectionForm, AddProfileConnectionApprovalForm
from accounts.decorators import *
from geography.models import Country, City
from events.utils import get_artist_fb_likes, get_artist_youtube_likes, get_artist_twitter_followers,\
                        get_artist_spotify_followers, get_artist_instagram_followers, get_artist_fb_likes_difference,\
                        get_artist_youtube_likes_difference, get_artist_twitter_followers_difference,\
                        get_artist_spotify_followers_difference, get_artist_instagram_followers_difference
from artist.models import Artist, ArtistGenre

# FOR CROPPING IMAGE
from PIL import Image
from StringIO import StringIO
from django.core.files import File
from django.core.files.base import ContentFile

def crop_image(request,form,profile):
    img_io = StringIO()
    x = form.cleaned_data.get('x')
    y = form.cleaned_data.get('y')
    w = form.cleaned_data.get('width')
    h = form.cleaned_data.get('height')

    img = Image.open(profile.image)
    cropped_image = img.crop((x, y, w+x, h+y))
    cropped_image.save(img_io, format='JPEG')
    img_content = ContentFile(img_io.getvalue(),profile.image.path)
    profile.image = img_content
    profile.save()

# CROP IMAGE END

def signup(request):
    if request.user.is_authenticated():
        return redirect(reverse('artists'))

    form = ProfileForm(request.POST or None, request.FILES)
    countries = Country.objects.all().order_by("name")
    image_form = ProfileImageForm(request.POST or None, request.FILES)


    if request.method == "POST":
        if form.is_valid():
            user = User.objects.create_user(
                username=create_username('%s %s' % (form.cleaned_data["first_name"], form.cleaned_data["last_name"])),
                email=form.cleaned_data['email'],
                password=form.cleaned_data['password'],
                is_active = False,
            )
            user.first_name = form.cleaned_data['first_name']
            user.last_name = form.cleaned_data['last_name']
            user.save()
            profile = Profile.objects.get_or_create(user=user)
            profile.country = form.cleaned_data['country'],
            profile.state = form.cleaned_data['state'],
            profile.artists_you_worked_with = form.cleaned_data['artists_you_worked_with'],
            profile.company_organization = form.cleaned_data['company_organization'],
            profile.contract_signatory_name = form.cleaned_data['contract_signatory_name'],
            profile.artist_you_love = form.cleaned_data['artist_you_love'],
            profile.signature = form.cleaned_data['signature'],
            profile.is_verified = form.cleaned_data['is_verified'],
            profile.company_website = form.cleaned_data['company_website'],
            profile.signatory_name = form.cleaned_data['signatory_name'],
            profile.mobile_no = form.cleaned_data['mobile_no'],
            profile.contact_no = form.cleaned_data['contact_no'],
            profile.address = form.cleaned_data['address'],
            profile.email_frequency = form.cleaned_data['email_frequency'],


            if request.FILES:
                profile.image = request.FILES.get("image")
            profile.save()

            email = SendEmail(request)
            email.send_by_template([user.email], "profiles/email/sign_up_thanks.html",
                context={
                },
                subject = "Welcome to Wheredjsplay.com"
            )

            admin_email = SendEmail(request)
            email.send_by_template(settings.DEFAULT_ADMIN_EMAILS, "profiles/email/admin_notify.html",
                context={
                    'profile': profile,
                    'profile_url': settings.SITE_URL + '/admin/profiles/profile/{}/change/'.format(profile.id),
                    'user_url': settings.SITE_URL + '/admin/auth/user/{}/change/'.format(user.id),
                    "form":form,
                    "image_form": image_form,
                },
                subject = "New User Applied For the Invitation"
            )

            list = mailchimp.utils.get_connection().get_list_by_id(settings.MAILCHIMP_FREE_LIST)
            list.subscribe(user.email, {'EMAIL': user.email, 'FNAME': user.first_name, 'LNAME': user.last_name})

            return redirect(reverse('login_user'))

    else:
        form = ProfileForm()

    return render_to_response("profiles/signup.html", {
        "form": form,
        "countries": countries,
    }, context_instance=RequestContext(request))


@login_required
@check_email_validated
def get_cities(request):
    profile_obj = Profile.objects.get(user=request.user)
    cities = City.objects.all().order_by('city')
    if 'ctry' in request.GET:
        selected_country_name = Country.objects.filter(Q(code=request.GET.get('ctry')))
        profile_obj.country = selected_country_name[0]
        profile_obj.city = None
        profile_obj.save()
        cities = City.objects.filter(iso2=request.GET.get('ctry')).order_by('city')
    
    context = {
        "cities":cities,
        "profile_obj":profile_obj,
    }
    return render_to_response("profiles/get_cities.html", context)

class UpdateProfileImage(LoginRequiredMixin,View):
    
    def post(self, request):
        profile_obj = Profile.objects.get(user=request.user)
        form_image = ProfileImageForm(request.POST or None, request.FILES, instance=profile_obj)
        if form_image.is_valid():
            obj = form_image.save(commit=False)
            x_point = form_image.cleaned_data.get('x',None)
            if str(x_point) == "0.0":
                x_point = True

            if request.FILES and x_point:
                obj.image = request.FILES.get("image")
                obj.save()
                crop_image(request,form_image,obj)
                messages.add_message(request, messages.SUCCESS, "Image Updated")
            else:
                messages.add_message(request, messages.INFO, "Image Update Failed")
        return redirect(reverse('edit_details'))

update_image = UpdateProfileImage.as_view()

@login_required
@check_email_validated
def edit_details(request):
    profile_obj = Profile.objects.get(user=request.user)
    if 'city' in request.POST:
        selected_city_name = City.objects.filter(Q(city_ascii=request.POST.get('city'))).first()
        profile_obj.city = selected_city_name
        profile_obj.save()
    # if 'ctry' in request.GET:
    #     selected_country_name = Country.objects.filter(Q(code=request.GET.get('ctry')))
    #     profile_obj.country = selected_country_name[0]
    #     profile_obj.save()
    #     pass
    form = EditProfileForm(request.POST or None, request.FILES, instance=profile_obj)
    form_image = ProfileImageForm(instance=profile_obj)
    # countries = Country.objects.all().order_by("name")

    social_auth_facebook = request.user.social_auth.filter(provider='facebook')
    social_auth_google = request.user.social_auth.filter(provider='google-oauth2')
    # subscriptions = Subscription.objects.filter(user = profile_obj.user).order_by("-end_time")
    subscriptions = profile_obj.subscription
    artists = Artist.objects.all()
    all_user_shortlist_artist = profile_obj.shortlist_artist.all()
    countries = Country.objects.all().order_by("name")
    cities = City.objects.all().order_by("city")
    if profile_obj.country:
        cities = City.objects.filter(iso2=profile_obj.country.code).order_by("city")
    
    if request.method == "POST":

        shortlist_artist_id = request.POST.getlist('shortlisted_artists')

        for artist in profile_obj.shortlist_artist.all():
            profile_obj.shortlist_artist.remove(artist)

        if shortlist_artist_id:
            shortlist_artist_id = [int(id) for id in shortlist_artist_id]
            for id in shortlist_artist_id:
                profile_obj.shortlist_artist.add(Artist.objects.get(artistID=id))
                profile_obj.save()

        # form = EditProfileForm(request.POST or None, request.FILES, instance=profile_obj)
        if form.is_valid():
            cd = form.cleaned_data
            obj = form.save(commit=False)

            #obj.user_types = form.cleaned_data['user_types']
            obj.nickname = form.cleaned_data['nick_name']
            obj.user.first_name = form.cleaned_data['first_name']
            obj.user.last_name = form.cleaned_data['last_name']
            obj.user.save()
            obj.save()
            messages.add_message(request, messages.SUCCESS, "Profile Data Updated")
        
        return render_to_response("profiles/signup.html", {
            "form": form,
            "profile_obj":profile_obj,
            # "countries": countries,
            "form_image": form_image,
            "social_auth_facebook": social_auth_facebook,
            "social_auth_google": social_auth_google,
            "user_subscriptions": subscriptions,
            "countries": countries,
            "cities": cities,
            "artists":artists,
            "all_user_shortlist_artist":all_user_shortlist_artist,
            }, context_instance=RequestContext(request))

    else:

        form = EditProfileForm(instance=profile_obj, initial={
            'first_name': profile_obj.user.first_name,
            'last_name': profile_obj.user.last_name,
            'nick_name': profile_obj.nickname,
            #'user_types': profile_obj.user_types,
        })

        return render_to_response("profiles/signup.html", {
            "form": form,
            "profile_obj":profile_obj,
            # "countries": countries,
            "form_image": form_image,
            "social_auth_facebook": social_auth_facebook,
            "social_auth_google": social_auth_google,
            "user_subscriptions": subscriptions,
            "countries": countries,
            "cities": cities,
            "artists":artists,
            "all_user_shortlist_artist":all_user_shortlist_artist,
            }, context_instance=RequestContext(request))


def save_recent_search(request, search_type, message):
    profile = Profile.objects.get(user=request.user)
    link = request.get_full_path()
    recentsearch = RecentSearch.objects.get_or_create(
        search_type=search_type,
        message=message,
        link=link,
        profile=profile
    )
    return recentsearch


@login_required
@check_email_validated
# @check_plan('Trial','Basic','Premium')
@check_plan('Trial','Artist/DJ','Promoter', 'Agent')
def my_recent_search(request):
    if request.method == "POST":
        remove_all = request.POST.get('remove_all', '')
        if remove_all:
            RecentSearch.objects.filter(profile__user=request.user).delete()

    all_search = RecentSearch.objects.filter(profile__user=request.user).order_by('-created')
    if len(all_search) > 6:
        all_search = all_search[:6]
        more = True
    else:
        more = False

    context = {
        "start": 6,
        "more": more,
        "all_search": all_search,
    }
    return render(request, "profiles/my_search.html", context)


@login_required
@check_email_validated
@check_plan('Trial','Artist/DJ','Promoter', 'Agent')
# @check_plan('Trial','Basic','Premium')
def load_more_search(request):

    all_search = RecentSearch.objects.filter(profile__user=request.user).order_by('-created')

    start = int(request.GET.get('start'))

    if len(all_search) > start+6:
        all_search = all_search[start:start+6]
        more = True
    else:
        all_search = all_search[start:]
        more = False

    context = {"all_search": all_search}

    template = render_to_string("profiles/load_more_search.html", context ,context_instance=RequestContext(request))

    response = {"start": start+6, "more": more, "data": template}

    return HttpResponse(json.dumps(response), content_type="applicaton/json")


@login_required
@check_email_validated
def profile_managed_djs(request):
    profile = Profile.objects.get(user=request.user)
    if profile.user_types == "Promoter" or profile.user_types == "Agent" or request.user.is_superuser :
        managed_djs = profile.all_managed_artists.all()
        context = {"profile":profile, "managed_djs":managed_djs}
        return render(request, "profiles/profile_added_djs.html", context)
    else:
        return redirect("/")


@login_required
@check_email_validated
def added_djs(request):
    profile = Profile.objects.get(user=request.user)
    added_djs = profile.all_added_artists.all()

    context = {
            "add_dj":True,
            "managed_djs":added_djs,
        }
    return render(request, "profiles/profile_added_djs.html", context)


@login_required
@check_email_validated
@check_plan('Trial', 'Artist/DJ', 'Promoter', 'Agent')
def profile_trackers(request):
    context = {}
    user = request.user
    profile = Profile.objects.filter(user=user).first()
    context.update({
        "profile":profile,
    })
    profile_tracker = profile.profiletracker_set.all()
    context.update({
        "profile_tracker":profile_tracker
    })

    return render(request, "profiles/profile_trackers.html", context)

@login_required
# @check_plan('Trial','Basic','Premium')
@check_plan('Trial', 'Promoter', 'Artist/DJ', 'Agent')
@check_email_validated
def profile_show(request, user_id):
    pro_obj = None
    if request.user.is_authenticated():
        try:
            pro_obj = Profile.objects.get(user_id=user_id)
        except Profile.DoesNotExist:
            raise Http404()
    
    artist_connections = ProfileConnections.objects.filter(requested_by=pro_obj.user, artist__isnull=False)
    venue_connections = ProfileConnections.objects.filter(requested_by=pro_obj.user, venue__isnull=False)
    event_connections = ProfileConnections.objects.filter(requested_by=pro_obj.user, event__isnull=False)
    context = {}

    context.update({
        'pro_obj': pro_obj,
        "site_url": settings.SITE_URL,
        "artist_connections":artist_connections,
        "venue_connections":venue_connections,
        "event_connections":event_connections,
    })

    return render(request, 'profiles/show_profile.html', context)


@login_required
@check_email_validated
def delete_tracker(request, tracker_id):
    context = {}
    user = request.user
    profile = Profile.objects.filter(user=user).first()
    profile_tracker  = ProfileTracker.objects.get(id=tracker_id)
    profile_tracker.delete()
    messages.add_message(request, messages.SUCCESS, " Group of Artist has been Deleted ")


    return redirect(reverse("profile_trackers"))


@login_required
@check_email_validated
def add_tracker(request):
    context = {}
    user = request.user
    profile = Profile.objects.filter(user=user).first()
    artists = Artist.objects.all()

    context.update({
        "artists":artists,
        "profile":profile,
    })

    if request.method == "POST" :
        tracker_artist_ids = request.POST.getlist("tracker_artists", None)
        tracker_name = request.POST.get("tracker_name", None)
        # active = request.POST.get("active", None)
        profile_tracker = ProfileTracker()
        profile_tracker.profile = profile
        profile_tracker.tracker_name = tracker_name
        all_tracker = profile.profiletracker_set.all()
        # all_tracker = profile.profiletracker_set.filter(is_active=False)

        # active tracker functionality , need to be deleted in future

        # all_tracker_length = len(all_tracker)
        #
        # if all_tracker_length == 0:
        #     active = "True"
        # if active == "True":
        #     profile_tracker.is_active = True
        #     all_tracker = all_tracker.exclude(id=profile_tracker.id)
        #     if all_tracker:
        #         for tracker in all_tracker:
        #             tracker.is_active = False
        #             tracker.save()
        # else:
        #     profile_tracker.is_active = False

        profile_tracker.save()

        if tracker_artist_ids:
            tracker_artist_id = [int(id) for id in tracker_artist_ids]
            for id in tracker_artist_ids:
                profile_tracker.artist.add(Artist.objects.get(artistID=id))
        # profile_tracker.save()

        return redirect(reverse("profile_trackers"))

    return render(request, "profiles/add_tracker.html", context)


@login_required
@check_email_validated
def edit_tracker(request, tracker_id):
    context = {}
    user = request.user
    profile = Profile.objects.filter(user=user).first()
    artists = Artist.objects.all()

    profile_tracker = ProfileTracker.objects.filter(id=tracker_id).first()

    profile_tracker_artists = profile_tracker.artist.all()

    context.update({
        "profile_tracker":profile_tracker,
        "profile_tracker_artists":profile_tracker_artists,
        "artists":artists,
        "profile":profile,
    })

    if request.method == "POST" :
        tracker_artist_ids = request.POST.getlist("tracker_artists", None)
        tracker_name = request.POST.get("tracker_name", None)
        active = request.POST.get("active", None)

        if profile_tracker:
            profile_tracker.profile = profile
            profile_tracker.tracker_name = tracker_name
            # all_tracker = profile.profiletracker_set.filter(is_active=False)
            all_tracker = profile.profiletracker_set.all()
            all_tracker_length = len(all_tracker)
            if all_tracker_length == 1:
                active = "True"
            if active == "True" :
                profile_tracker.is_active = True
                all_tracker = all_tracker.exclude(id=profile_tracker.id)
                for tracker in all_tracker:
                    tracker.is_active = False
                    tracker.save()
            else:
                if active == "False":
                    all_tracker = all_tracker.exclude(id=profile_tracker.id).values_list("is_active", flat=True)
                    if True in all_tracker:
                        profile_tracker.is_active = False
                    else:
                        profile_tracker.is_active = True

            profile_tracker.save()

            for artist in profile_tracker.artist.all():
                profile_tracker.artist.remove(artist)

            if tracker_artist_ids:
                tracker_artist_id = [int(id) for id in tracker_artist_ids]
                for id in tracker_artist_ids:
                    profile_tracker.artist.add(Artist.objects.get(artistID=id))
                    profile_tracker.save()

            return redirect(reverse("profile_trackers"))

    return render(request, "profiles/add_tracker.html", context)


@login_required
@check_email_validated
def tracker_stats(request, tracker_id):
    context = {}
    profile_tracker = ProfileTracker.objects.filter(id=tracker_id).first()
    tracker_artist = profile_tracker.artist.all()

    likes_data = {}
    youtube_likes_data = {}
    twitter_followers_data = {}
    spotify_followers_data = {}

    for artist in tracker_artist:
        likes_data.update(get_artist_fb_likes(artist))
        youtube_likes_data.update(get_artist_youtube_likes(artist))
        twitter_followers_data.update(get_artist_twitter_followers(artist))
        spotify_followers_data.update(get_artist_spotify_followers(artist))

    context.update({
        "profile_tracker":profile_tracker,
        "likes_data":likes_data,
        "youtube_likes_data":youtube_likes_data,
        "twitter_followers_data":twitter_followers_data,
        "spotify_followers_data":spotify_followers_data
    })

    return render(request, "profiles/tracker_stats.html", context)

from events.models import DjMaster


@login_required
@check_email_validated
def tracker_events(request, tracker_id):
    context = {}
    event_load_limit = 10
    user = request.user
    pro_obj = Profile.objects.get(user=user)
    profile_tracker = ProfileTracker.objects.filter(id=tracker_id).first()
    start = 0
    limit = 2

    if profile_tracker:
        context.update({
            "profile_tracker":profile_tracker
        })
        artists = profile_tracker.artist.all()

        artists_ids = []

        for artsit in artists:
            artists_ids.append(artsit.artistID)

        dj_master_objects = DjMaster.objects.filter(
            artistid__in=artists_ids,
        )

        dj_master_objects_upcoming = dj_master_objects.filter(eventid__date__gt=dt.today()).\
            order_by('eventid__date').distinct()[start:start + limit]

        dj_master_objects_past = dj_master_objects.filter(eventid__date__lt=dt.today()).\
            order_by('-eventid__date').distinct()[start:start + limit]

        context.update({
            "upcoming_events":dj_master_objects_upcoming,
            "former_events": dj_master_objects_past,
            "artists":artists
        })

    if request.is_ajax() and request.GET.get('start', ''):
        start = request.GET.get('start', '')
        action = request.GET.get('action', '')
        start = int(start)
        if action == 'upcoming':

            allevents = dj_master_objects.filter(eventid__date__gt=dt.today()).\
                order_by('eventid__date').distinct()[start:start + limit]
        else:
            allevents = dj_master_objects.filter(eventid__date__lt=dt.today()).\
                order_by('-eventid__date').distinct()[start:start + limit]

        context = {
            "events": allevents,
            "pro_obj": pro_obj,
        }

        template = render_to_string("geography/get_events.html", context,
                                    context_instance=RequestContext(request))

        start = int(start) + event_load_limit

        if (allevents):
            status = "True"  # No More events
        else:
            status = "False"  # More events
        response = {
            "status": status,
            "start": start,
            "data": template,
        }
        return HttpResponse(json.dumps(response), content_type="applicaton/json")

    context.update({
        "start": event_load_limit,
        "tracker_events": True,
        "tracker_id": tracker_id,
        "pro_obj": pro_obj,
    })

    return render(request, "profiles/tracker_events.html", context)


@login_required
@check_email_validated
def tracker_fb_stats(request, tracker_id):
    context = {}
    profile_tracker = ProfileTracker.objects.filter(id=tracker_id).first()
    tracker_artist = profile_tracker.artist.all()
    facebook_likes_data = {}
    context.update({
        "artists": tracker_artist,
        "profile_tracker":profile_tracker,
    })

    if tracker_artist:
        for artist in tracker_artist:
            try:
                facebook_likes_data.update(get_artist_fb_likes_difference(artist))
            except:
                pass

        # color_code = {}
        # for key in facebook_likes_data.keys():
        #     color_code[key] = ColorHash(key).hex

        context.update({
            "fb_stats":True,
            "likes_data": facebook_likes_data,
            # "color_code": color_code,
        })

    return render(request, "profiles/tracker_fb_stats.html", context)


@login_required
@check_email_validated
def tracker_youtube_stats(request, tracker_id):
    context = {}
    profile_tracker = ProfileTracker.objects.filter(id=tracker_id).first()
    tracker_artist = profile_tracker.artist.all()
    youtube_likes_data = {}
    context.update({
        "artists": tracker_artist,
        "profile_tracker":profile_tracker,
    })

    if tracker_artist:
        for artist in tracker_artist:
            try:
                youtube_likes_data.update(get_artist_youtube_likes_difference(artist))
            except:
                pass
        context.update({
            "youtube_stats":True,
            "youtube_likes_data": youtube_likes_data
        })

    return render(request, "profiles/tracker_youtube_stats.html", context)


@login_required
@check_email_validated
def tracker_twitter_stats(request, tracker_id):
    context = {}
    profile_tracker = ProfileTracker.objects.filter(id=tracker_id).first()
    tracker_artist = profile_tracker.artist.all()
    twitter_followers_data = {}

    context.update({
        "artists": tracker_artist,
        "profile_tracker":profile_tracker,
    })

    if tracker_artist:
        for artist in tracker_artist:
            try:
                twitter_followers_data.update(get_artist_twitter_followers_difference(artist))
            except:
                pass

        context.update({
            "twitter_stats":True,
            "twitter_followers_data": twitter_followers_data
        })

    return render(request, "profiles/tracker_twitter_stats.html", context)


@login_required
@check_email_validated
def tracker_spotify_stats(request, tracker_id):
    context = {}
    profile_tracker = ProfileTracker.objects.filter(id=tracker_id).first()
    tracker_artist = profile_tracker.artist.all()
    spotify_followers_data = {}

    context.update({
        "artists": tracker_artist,
        "profile_tracker":profile_tracker,
    })

    if tracker_artist:
        for artist in tracker_artist:
            try:
                spotify_followers_data.update(get_artist_spotify_followers_difference(artist))
            except:
                pass

        context.update({
            "spotify_stats":True,
            "spotify_followers_data": spotify_followers_data
        })

    return render(request, "profiles/tracker_spotify_stats.html", context)


@login_required
@check_email_validated
def tracker_instagram_stats(request, tracker_id):
    context = {}
    profile_tracker = ProfileTracker.objects.filter(id=tracker_id).first()
    tracker_artist = profile_tracker.artist.all()
    instagram_followers_data = {}

    context.update({
        "artists": tracker_artist,
        "profile_tracker":profile_tracker,
    })

    if tracker_artist:
        for artist in tracker_artist:
            try:
                instagram_followers_data.update(get_artist_instagram_followers_difference(artist))

            except:
                pass

        context.update({
            "instagram_stats":True,
            "instagram_followers_data": instagram_followers_data
        })

    return render(request, "profiles/tracker_instagram_stats.html", context)


@method_decorator(check_plan('Artist/DJ', 'Promoter', 'Agent'), name='dispatch')
class AddConnectionInfo(LoginRequiredMixin, View):

    def get(self, request, conn_type, type_id):
        connection_type_obj = None
        context = {
            "connection_type": conn_type
        }
        if conn_type == "Artist":
            try:
                connection_type_obj = Artist.allartistobjects.get(artistID=type_id)
            except Artist.DoesNotExist:
                raise Http404()
            context.update({
                'connection_type_obj': connection_type_obj,
            })

            connection_obj = ProfileConnections.objects.filter(
                requested_by=request.user,
                artist=connection_type_obj).first()

            if connection_obj:
                messages.add_message(request, messages.ERROR, "Connection Request Already Sent..!")
                return redirect(reverse('artist_detail', kwargs={'artist_id': connection_type_obj.artistID }))

            return render(request, 'profiles/add_connection_start.html', context)
        
        elif conn_type == "Venue":
            try:
                connection_type_obj = DjVenues.objects.get(venueid=type_id)
            except DjVenues.DoesNotExist:
                raise Http404()

            context.update({
                'connection_type_obj': connection_type_obj,
            })

            connection_obj = ProfileConnections.objects.filter(
                requested_by=request.user,
                venue=connection_type_obj).first()
            
            if connection_obj:
                messages.add_message(request, messages.ERROR, "Connection Request Already Sent..!")
                return redirect(reverse('venue_detail', kwargs={'venue_id': connection_type_obj.venueid }))
            return render(request, 'profiles/add_connection_start.html', context)
        
        elif conn_type == "Event":
            try:
                connection_type_obj = Event.objects.get(eventID=type_id)
            except Event.DoesNotExist:
                raise Http404()

            context.update({
                'connection_type_obj': connection_type_obj,
            })

            connection_obj = ProfileConnections.objects.filter(
                requested_by=request.user,
                event=connection_type_obj).first()
            
            if connection_obj:
                messages.add_message(request, messages.ERROR, "Connection Request Already Sent..!")
                return redirect(reverse('event_show', kwargs={'event_id': connection_type_obj.eventID }))
            return render(request, 'profiles/add_connection_start.html', context)
        
        else:
            message.add_message(request, messages.ERROR,"Something went wrong")
            return redirect(reverse('dashboard'))

add_connection_info = AddConnectionInfo.as_view()


@method_decorator(check_plan('Artist/DJ', 'Promoter', 'Agent'), name='dispatch')
class AddConnectionFormView(LoginRequiredMixin, View):

    def get(self, request, conn_type, type_id):
        profile_obj = Profile.objects.get(user=request.user)
        connection_type_obj = None
        form = AddConnectionForm()
        context = {
            "connection_type": conn_type,
            "profile_obj":profile_obj,
        }
        # For Artist Connection Type
        if conn_type == "Artist":
            try:
                connection_type_obj = Artist.allartistobjects.get(artistID=type_id)
            except Artist.DoesNotExist:
                raise Http404()
            connection_obj = ProfileConnections.objects.filter(requested_by=profile_obj.user, artist=connection_type_obj).first()
            
            connections_count = ProfileConnections.objects.filter(artist=connection_type_obj).count()
            if connections_count > 5:
                messages.add_message(request, messages.ERROR, "Connections Limit Exceeded for this artist !")
                return redirect(reverse('artist_detail', kwargs={'artist_id': connection_type_obj.artistID }))


            if connection_obj:
                if connection_obj.status == 1:
                    messages.add_message(request, messages.SUCCESS, "Artist Connection already approved..!")
                    return redirect(reverse('artist_detail', kwargs={'artist_id': connection_type_obj.artistID }))
                
                context.update({
                    "form":AddConnectionForm(instance=connection_obj),
                })
            else:
                context.update({
                    "form":AddConnectionForm()
                })

            context.update({
                'connection_type_obj': connection_type_obj,
            })
            return render(request, 'profiles/add_connection_form.html', context)
        
        # For Venue Connection type
        elif conn_type == "Venue":
            try:
                connection_type_obj = DjVenues.objects.get(venueid=type_id)
            except DjVenues.DoesNotExist:
                raise Http404()
            
            connection_obj = ProfileConnections.objects.filter(requested_by=profile_obj.user, venue=connection_type_obj).first()
            connections_count = ProfileConnections.objects.filter(venue=connection_type_obj).count()
            
            if connections_count > 5:
                messages.add_message(request, messages.ERROR, "Connections Limit Exceeded for this venue !")
                return redirect(reverse('venue_detail', kwargs={'venue_id': connection_type_obj.venueid }))

            if connection_obj:
                if connection_obj.status == 1:
                    messages.add_message(request, messages.SUCCESS, "Venue Connection already approved..!")
                    return redirect(reverse('venue_detail', kwargs={'venue_id': connection_type_obj.venueid }))
                
                context.update({
                    "form":AddConnectionForm(instance=connection_obj, connection_type="Venue"),
                })
            else:
                context.update({
                    "form":AddConnectionForm(connection_type="Venue")
                })

            context.update({
                'connection_type_obj': connection_type_obj,
            })
            return render(request, 'profiles/add_connection_form.html', context)
        
        # For Event Connection type
        elif conn_type == "Event":
            try:
                connection_type_obj = Event.objects.get(eventID=type_id)
            except Event.DoesNotExist:
                raise Http404()
            
            connection_obj = ProfileConnections.objects.filter(requested_by=profile_obj.user, event=connection_type_obj).first()
            connections_count = ProfileConnections.objects.filter(event=connection_type_obj).count()
            
            if connections_count > 5:
                messages.add_message(request, messages.ERROR, "Connections Limit Exceeded for this event !")
                return redirect(reverse('event_show', kwargs={'event_id': connection_type_obj.eventID }))

            if connection_obj:
                if connection_obj.status == 1:
                    messages.add_message(request, messages.SUCCESS, "Event Connection already approved..!")
                    return redirect(reverse('event_show', kwargs={'event_id': connection_type_obj.eventID }))
                
                context.update({
                    "form":AddConnectionForm(instance=connection_obj, connection_type="Event"),
                })
            else:
                context.update({
                    "form":AddConnectionForm(connection_type="Event")
                })

            context.update({
                'connection_type_obj': connection_type_obj,
            })
            return render(request, 'profiles/add_connection_form.html', context)
        
        else:
            message.add_message(request, messages.ERROR,"Something went wrong")
            return redirect(reverse('dashboard'))

    
    def post(self, request, conn_type, type_id):
        profile_obj = Profile.objects.get(user=request.user)
        connection_type_obj = None
        
        context = {
            "connection_type": conn_type,
            "profile_obj":profile_obj,
            }
        
        #  For Artist Connection
        if conn_type == "Artist":
            form = AddConnectionForm(request.POST or None, request.FILES)

            try:
                connection_type_obj = Artist.allartistobjects.get(artistID=type_id)
            except Artist.DoesNotExist:
                raise Http404()
            
            context.update({
                'connection_type_obj': connection_type_obj,
            })
            
            connection_obj = ProfileConnections.objects.filter(requested_by=profile_obj.user, artist=connection_type_obj).first()
            
            if connection_obj:
                form = AddConnectionForm(request.POST or None, request.FILES, instance=connection_obj)

            if form.is_valid():
                form.instance.requested_by = profile_obj.user
                form.instance.artist = connection_type_obj
                form.instance.connection_type = conn_type
                connection_request_obj = form.save()

                # Send email to User
                msg = SendEmail()
                msg.send_by_template([request.user.email, ], "profiles/connection_request_thanks.html",
                                    context={
                                        "connection_request_obj": connection_request_obj,
                                    },
                                    subject="Artist Connection Request Submitted."
                                    )
                # Send email to Admins
                admins = User.objects.filter(is_superuser=True)
                admin_emails = [admin.email for admin in admins]
                msg = SendEmail()
                msg.send_by_template(admin_emails, "profiles/artist_connection_request.html",
                                        context={
                                            "connection_request_obj":connection_request_obj,
                                            "site_url": settings.SITE_URL,
                                        },
                                        subject="Artist Connection Request"
                                        )
                connection_request_obj.email_status = 1
                connection_request_obj.status = 0
                connection_request_obj.save()

                return render(request, 'profiles/add_connection_finish.html', context)
                
            else:
                context = {
                    "form":form
                }
                return render(request, 'profiles/add_connection_form.html', context)

        #  For Venue Connection
        elif conn_type == "Venue":
            try:
                connection_type_obj = DjVenues.objects.get(venueid=type_id)
            except DjVenues.DoesNotExist:
                raise Http404()
            
            context.update({
                'connection_type_obj': connection_type_obj,
            })
            
            connection_obj = ProfileConnections.objects.filter(requested_by=profile_obj.user, venue=connection_type_obj).first()
            
            if connection_obj:
                form = AddConnectionForm(request.POST or None, request.FILES, instance=connection_obj, connection_type = "Venue")
            else:
                form = AddConnectionForm(request.POST or None, request.FILES, connection_type = "Venue")

            if form.is_valid():
                form.instance.requested_by = profile_obj.user
                form.instance.venue = connection_type_obj
                form.instance.connection_type = conn_type
                connection_request_obj = form.save()

                # Send email to User
                msg = SendEmail()
                msg.send_by_template([request.user.email, ], "profiles/connection_request_thanks.html",
                                    context={
                                        "connection_request_obj": connection_request_obj,
                                    },
                                    subject="Venue Connection Request Submitted."
                                    )
                # Send email to Admins
                admins = User.objects.filter(is_superuser=True)
                admin_emails = [admin.email for admin in admins]
                msg = SendEmail()
                msg.send_by_template(admin_emails, "profiles/venue_connection_request.html",
                                        context={
                                            "connection_request_obj":connection_request_obj,
                                            "site_url": settings.SITE_URL,
                                        },
                                        subject="Venue Connection Request"
                                        )
                connection_request_obj.email_status = 1
                connection_request_obj.status = 0
                connection_request_obj.save()

                return render(request, 'profiles/add_connection_finish.html', context)
                
            else:
                form = AddConnectionForm(request.POST or None, connection_type = "Venue")
                context = {
                    "form":form
                }
                return render(request, 'profiles/add_connection_form.html', context)
        
        #  For Event Connection
        elif conn_type == "Event":
            try:
                connection_type_obj = Event.objects.get(eventID=type_id)
            except Event.DoesNotExist:
                raise Http404()
            
            context.update({
                'connection_type_obj': connection_type_obj,
            })
            
            connection_obj = ProfileConnections.objects.filter(requested_by=profile_obj.user, event=connection_type_obj).first()
            
            if connection_obj:
                form = AddConnectionForm(request.POST or None, request.FILES, instance=connection_obj, connection_type = "Event")
            else:
                form = AddConnectionForm(request.POST or None, request.FILES, connection_type = "Event")

            if form.is_valid():
                form.instance.requested_by = profile_obj.user
                form.instance.event = connection_type_obj
                form.instance.connection_type = conn_type
                connection_request_obj = form.save()

                # Send email to User
                msg = SendEmail()
                msg.send_by_template([request.user.email, ], "profiles/connection_request_thanks.html",
                                    context={
                                        "connection_request_obj": connection_request_obj,
                                    },
                                    subject="Event Connection Request Submitted."
                                    )
                # Send email to Admins
                admins = User.objects.filter(is_superuser=True)
                admin_emails = [admin.email for admin in admins]
                msg = SendEmail()
                msg.send_by_template(admin_emails, "profiles/event_connection_request.html",
                                        context={
                                            "connection_request_obj":connection_request_obj,
                                            "site_url": settings.SITE_URL,
                                        },
                                        subject="Event Connection Request"
                                        )
                connection_request_obj.email_status = 1
                connection_request_obj.status = 0
                connection_request_obj.save()

                return render(request, 'profiles/add_connection_finish.html', context)
                
            else:
                form = AddConnectionForm(request.POST or None, connection_type = "Event")
                context = {
                    "form":form
                }
                return render(request, 'profiles/add_connection_form.html', context)

add_connection_form = AddConnectionFormView.as_view()


@method_decorator(check_plan('Artist/DJ', 'Promoter', 'Agent'), name='dispatch')
class AddConnectionApproval(LoginRequiredMixin, View):

    def get(self, request, connection_id):
        if request.user.is_superuser:
            context = {}
            try:
                connection = ProfileConnections.objects.get(id=connection_id)
            except ProfileConnections.DoesNotExist:
                raise Http404()

            form = AddProfileConnectionApprovalForm()
            profile_obj = connection.requested_by.profile_set.first()
            context.update({
                "connection":connection,
                "pro_obj":profile_obj,
                "form":form,
            })
            return render(request, 'profiles/show_connection_request_form.html', context)
        else:
            messages.add_message(request, messages.ERROR, "You do not have the access to visit this page.")
            return redirect(reverse('dashboard'))

    def post(self, request, connection_id):
        if request.user.is_superuser:
            context = {}
            try:
                connection_obj = ProfileConnections.objects.get(id=connection_id)
            except ProfileConnections.DoesNotExist:
                raise Http404()
            
            form = AddProfileConnectionApprovalForm(request.POST or None)
            status = form.data["approved"]
            
            connection_obj.comments = form.data["comments"]
            connection_obj.handled_by = request.user
            connection_obj.save()
            
            if connection_obj.connection_type == "Artist":
                subject = "Your Request to add %s as a connection is Rejected"%(connection_obj.artist.artistName)
    
                if status == "approved":
                    connection_obj.status = 1
                    connection_obj.save()
                    subject = "Your request to add %s as a connection is Approved"%(connection_obj.artist.artistName)
                    messages.add_message(request, messages.SUCCESS, "%s %s Successfully."%(connection_obj.artist.artistName, status))

            elif connection_obj.connection_type == "Venue":
                subject = "Your Request to add %s as a connection is Rejected"%(connection_obj.venue.venuename)

                if status == "approved":
                    connection_obj.status = 1
                    connection_obj.save()
                    subject = "Your request to add %s as a connection is Approved"%(connection_obj.venue.venuename)            
                    messages.add_message(request, messages.SUCCESS, "%s %s Successfully."%(connection_obj.venue.venuename, status))

            elif connection_obj.connection_type == "Event":
                subject = "Your Request to add %s as a connection is Rejected"%(connection_obj.event.eventName)

                if status == "approved":
                    connection_obj.status = 1
                    connection_obj.save()
                    subject = "Your request to add %s as a connection is Approved"%(connection_obj.event.eventName)            
                    messages.add_message(request, messages.SUCCESS, "%s %s Successfully."%(connection_obj.event.eventName, status))
            
            msg = SendEmail()
            msg.send_by_template([connection_obj.requested_by.email, ], "profiles/add_connection_response_email.html",
                context={
                    "site_url": settings.SITE_URL,
                    "connection_obj": connection_obj,
                },
                subject=subject
                )
            return redirect(reverse('dashboard'))
        else:
            messages.add_message(request, messages.ERROR, "You do not have the access to visit this page.")
            return redirect(reverse('dashboard'))

add_connection_approval = AddConnectionApproval.as_view()
