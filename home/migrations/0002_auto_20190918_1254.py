# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2019-09-18 12:54
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='comparedjfaq',
            options={'verbose_name': 'WDJP Faq', 'verbose_name_plural': 'WDJP Faqs'},
        ),
    ]
