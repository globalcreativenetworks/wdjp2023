from django.conf.urls import url

from . import views

urlpatterns = [
   url(r'^$', views.index, name='index'),
   url(r'about/$', views.about, name='about'),
   url(r'contact/$', views.contact, name='contact'),
   url(r'mobile/$', views.mobile, name='mobile'),
   url(r'current-location-map-api/$', views.user_location_map, name='user_location_map'),
   url(r'near-by-map/$', views.nearby_event_map, name='nearby_event_map'),
   url(r'faq/$', views.faq, name='faq'),
   url(r'faq/available/$', views.faq_avail, name='faq_avail'),
   url(r'faq/compare/$', views.faq_compare, name='faq_compare'),
   url(r'faq/global-event/$', views.faq_global_event, name='faq_global_event'),
   url(r'faq/events/$', views.faq_events, name='faq_events'),
   url(r'set_user_location/$', views.set_user_location, name='set_user_location'),
   url(r'demo-video/lesson-101/$', views.demo_video, name='demo_video'),
   url(r'help/$', views.help, name='help'),
   url(r'help/available-artist/$', views.help_available_artist, name='help_available_artist'),
   url(r'help/compare-artist/$', views.help_compare_artist, name='help_compare_artist'),
   url(r'help/global-event/$', views.help_global_event, name='help_global_event'),
   url(r'help/create-event/$', views.help_create_event, name='help_create_event'),
   url(r'load-more-news/$', views.load_more_news, name='load_more_news'),
   url(r'privacy-policy/$', views.privacy_policy, name='privacy_policy'),
   url(r'terms/$', views.terms, name='terms'),
   url(r'^promoter/$', views.promoter_link, name='promoter_link'),
   url(r'^dj/$', views.dj_link, name='dj_link'),
   url(r'^agent/$', views.agent_link, name='agent_link'),

   ]
