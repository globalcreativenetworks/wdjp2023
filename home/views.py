import json
import requests
from datetime import datetime

from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.template.loader import render_to_string
from django.db.models import F
from social_data.models import FacebookData
from django.template import RequestContext
from django.http import HttpResponse
from django.http import JsonResponse
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout

from accounts.decorators import check_email_validated
from events.models import DjMaster

from news.models import NewsSource, News
from artist.models import Artist, Genre
from django.contrib.gis.geoip import GeoIP
from home.models import CompareDjFaq
from profiles.models import Plan, UserType, Testimonial, TrustedBy


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def index(request):
    # if not request.user.is_authenticated() and request.user_agent.is_mobile:
    #     return redirect(reverse('mobile'))
    if request.user.is_authenticated():
        return redirect(reverse('dashboard'))

    user_plans = Plan.objects.all()
    user_types = UserType.objects.all()
    testimonials = Testimonial.objects.all()
    trustedBys = TrustedBy.objects.all()

    return render(request, 'home/index.html', {
        'plans': user_plans,
        'user_types': user_types,
        'testimonials': testimonials,
        'trustedBys': trustedBys
    })
# Create your views here.


def about(request):
    return render(request, 'home/about.html', {})


def contact(request):
    return render(request, 'home/contact.html', {})
@login_required
@check_email_validated
def faq(request):
    if request.user.is_authenticated():
        faq = CompareDjFaq.objects.all()
        return render(request, 'home/faq.html', {"all_faq":faq})
    else:
        return redirect("/")

def nearby_event_map(request):
    context = {}
    return render(request, 'home/near_by_map.html', context)


def user_location_map(request):
    data_list = []
    up_events = get_user_near_event(request)[1]
    all_events = []
    for event in up_events:

        artist_info = {}
        try:
            artist_info['id'] = event.artistid.artistID
        except:
            continue
        try:
            sourceImage = event.artistid.artistPhoto.url
        except Exception as e:
            sourceImage = None
        if sourceImage == None:
            try:
                sourceImage = ScrapePhoto.objects.get(artistID=event.artistid.artistID).flipImage
                sourceImage = "/media%s"%(sourceImage)
            except Exception as e:
                sourceImage = ""

        artist_info['artist'] = event.artistid.artistName
        artist_info['artist_image'] = sourceImage
        e = {}
        e['eventID'] = event.eventid.eventID
        e['eventName'] = event.eventid.eventName
        e['date'] = str(event.eventid.date)
        e['startTime'] = event.eventid.startTime
        print event.eventid.startTime
        e['endTime'] = event.eventid.endTime
        if event.eventid.date < datetime.now().date():
            e['eventType'] = "past"
        else:
            e['eventType'] = "future"
        v = {}
        v['venueID'] = str(event.venueid.venueid)
        v['venueName'] = event.venueid.venuename
        v['venueAddress'] = event.venueid.venueaddress
        if event.venueid.venuelatitude == 0E-9:
            v['venueLatitude'] = "0.000000000"
        else:
            v['venueLatitude'] = str(event.venueid.venuelatitude)
        if event.venueid.venuelongitude == 0E-9:
            v['venueLongitude'] = "0.000000000"
        else:
            v['venueLongitude'] = str(event.venueid.venuelongitude)
        v['city'] = event.venueid.city
        v['country'] = event.venueid.country
        event_dict = {}
        event_dict['artistDetail'] = artist_info
        event_dict['eventDetail'] = e
        event_dict['venueDetail'] = v
        all_events.append(event_dict)
    data = {}
    data['events'] = all_events
    data_list.append(data)

    return HttpResponse(json.dumps(data_list))


def get_artist_most_recent_event(artist, event_date, is_future_event=True):
    if event_date and artist:
        if is_future_event:
                query = """
                SELECT DISTINCT * FROM dj_Master
                            JOIN dj_Events ON dj_Events.eventID = dj_Master.eventID
                            JOIN dj_Venues ON dj_Venues.venueID = dj_Master.venueID
                            Where dj_Events.date > "{}" AND dj_Master.artistID = {}
                            ORDER BY dj_Events.date ASC LIMIT 1;
                """.format(event_date.strftime('%Y-%m-%d'), artist.artistID)
        else:
            query = """
            SELECT DISTINCT * FROM dj_Master
                        JOIN dj_Events ON dj_Events.eventID = dj_Master.eventID
                        JOIN dj_Venues ON dj_Venues.venueID = dj_Master.venueID
                        Where dj_Events.date < "{}" AND dj_Master.artistID = {}
                        ORDER BY dj_Events.date DESC LIMIT 1;
            """.format(event_date.strftime('%Y-%m-%d'), artist.artistID)

        artist_event = DjMaster.objects.raw(query)
        try:
            return artist_event[0]
        except Exception as e:
            print e
            return None
    else:
        return None


def get_artist_event_near_by_to_city(artist, lat, longitude, is_future_event=True):
    user_lat = lat
    user_long = longitude
    distance = 300

    if is_future_event:
        query = """
        SELECT DISTINCT *, (6367*acos(cos(radians({}))
                    *cos(radians(venuelatitude))*cos(radians(venuelongitude)-radians({}))
                    +sin(radians({}))*sin(radians(venuelatitude)))) AS distance FROM dj_Master
                    JOIN dj_Events ON dj_Events.eventID = dj_Master.eventID
                    JOIN dj_Venues ON dj_Venues.venueID = dj_Master.venueID
                    Where dj_Events.date >= CURDATE() AND dj_Master.artistID = {}
                    HAVING distance < {} ORDER BY dj_Events.date ASC LIMIT 1;
        """.format(float(user_lat), float(user_long), float(user_lat), artist.artistID, distance)
    else:
        query = """
        SELECT DISTINCT *, (6367*acos(cos(radians({}))
                    *cos(radians(venuelatitude))*cos(radians(venuelongitude)-radians({}))
                    +sin(radians({}))*sin(radians(venuelatitude)))) AS distance FROM dj_Master
                    JOIN dj_Events ON dj_Events.eventID = dj_Master.eventID
                    JOIN dj_Venues ON dj_Venues.venueID = dj_Master.venueID
                    Where dj_Events.date < CURDATE() AND dj_Master.artistID = {}
                    HAVING distance < {} ORDER BY dj_Events.date DESC LIMIT 1;
        """.format(float(user_lat), float(user_long), float(user_lat), artist.artistID, distance)


    city_event = DjMaster.objects.raw(query)

    try:
        return city_event[0]
    except:
        return None


def get_user_near_event(request):
    # user_lat = 51.5074 #Default Coordinates
    # user_long = -0.118092

    user_lat = request.COOKIES.get('user_session_lat', '')
    user_long =  request.COOKIES.get('user_session_long', '')

    #
    # user_lat_temp = request.COOKIES.get('user_session_lat', '')
    # user_long_temp =  request.COOKIES.get('user_session_long', '')
    # geoip = GeoIP()
    # if not user_lat_temp and not user_long_temp:
    #     ip = get_client_ip(request)
    #     if ip and ip != "127.0.0.1":
    #         user_lat_temp, user_long_temp = geoip.lat_lon(ip)
    #
    # if user_lat_temp and user_long_temp:
    #     user_lat = user_lat_temp
    #     user_long = user_long_temp

    all_event_near_by = []
    distance = 150
    # distance = 15000

    query = """
    SELECT DISTINCT *, (6367*acos(cos(radians({}))
                *cos(radians(venuelatitude))*cos(radians(venuelongitude)-radians({}))
                +sin(radians({}))*sin(radians(venuelatitude)))) AS distance FROM dj_Master JOIN dj_Events ON dj_Events.eventID = dj_Master.eventID JOIN dj_Venues ON dj_Venues.venueID = dj_Master.venueID Where dj_Events.date >= CURDATE() HAVING distance < {} ORDER BY dj_Events.date ASC LIMIT 50;
    """.format(float(user_lat), float(user_long), float(user_lat), distance)

    all_test_event = DjMaster.objects.raw(query)
    # unique_event_id = []
    # for event in all_test_event:
    #     all_event_near_by.append(event)
    #     if len(unique_event_id) >= 15:
    #         break
    #     else:
    #         if event.eventid.eventID not in unique_event_id:
    #             unique_event_id.append(event.eventid.eventID)

    event_data = {}
    for event in all_test_event:
        if not event_data.get(event.eventid.eventID, ''):
            event_data[event.eventid.eventID] = {}
            event_data[event.eventid.eventID]["event_data"] = event.eventid
            event_data[event.eventid.eventID]["venue_data"] = event.venueid
            try:
                event_data[event.eventid.eventID]["artist_performing"] = [event.artistid]
            except:
                pass
        else:
            try:
                event_data[event.eventid.eventID]["artist_performing"].append(event.artistid)
            except:
                pass

    event_list = []
    for key,value in event_data.iteritems():
        event_list.append(value)

    event_list = sorted(event_list, key=lambda k: k['event_data'].date)

    for event in event_list:
        try:
            if len(event["artist_performing"]) > 1:
                top_artist = FacebookData.objects.filter(artist__in=event["artist_performing"]).annotate(count=F('fanCount')+0).order_by('-count')
                if top_artist:
                    event["top_artist"] = top_artist[0]
            elif event["artist_performing"]:
                event["top_artist"] = event["artist_performing"][0]
            else:
                pass
        except:
            pass

    return event_list, all_event_near_by

# def get_news():
#     news = News.objects.all().order_by('-publish_date')
#     return news


def mobile(request):
    news_limit = 15
    event_list = get_user_near_event(request)[0]

    edm_genre = Genre.objects.get(genreName="EDM")
    edm_artist = Artist.objects.filter(genre=edm_genre)
    edm_news = News.objects.filter(artists__in=edm_artist).order_by('-publish_date').distinct()[:15]

    techno_genre = Genre.objects.get(genreName="Techno")
    techno_artist = Artist.objects.filter(genre=techno_genre)
    news_source = NewsSource.objects.filter(sourceName="Mtv")
    techno_news = News.objects.filter(artists__in=techno_artist).exclude(source__in=news_source).order_by('-publish_date').distinct()

    if len(techno_news) > 15:
        techno_news = techno_news[:15]
        techno_more = True
    else:
        techno_more = False

    if len(edm_news) > 15:
        edm_news = edm_news[:15]
        more = True
    else:
        more = False

    return render(request, 'home/mobile.html', {
        'event_list': event_list,
        'edm_news':edm_news,
        'start': 15,
        'techno_start':15,
        'more': more,
        'techno_more':techno_more,
        'techno_news':techno_news,

    })


def load_more_news(request):
    if request.is_ajax() and request.POST.get('edm') == "edm":
        start = int(request.POST.get('start'))
        edm_genre = Genre.objects.get(genreName="EDM")
        edm_artist = Artist.objects.filter(genre=edm_genre)
        edm_news = News.objects.filter(artists__in=edm_artist).order_by('-publish_date').distinct()

        if len(edm_news) > start+15:
            edm_news = edm_news[start:start+15]
            more = True
        else:
            edm_news = edm_news[start:]
            more = False

        context = {"edm_news": edm_news, "start":start+15}

        template = render_to_string("home/edm_news.html", context, context_instance=RequestContext(request))

        response = {"start": start+15, "more": more, "data": template}

        return HttpResponse(json.dumps(response), content_type="applicaton/json")

    if request.is_ajax() and request.POST.get('techno') == "techno" :
        techno_start = int(request.POST.get('techno_start'))
        techno_genre = Genre.objects.get(genreName="Techno")
        techno_artist = Artist.objects.filter(genre=techno_genre)
        news_source = NewsSource.objects.filter(sourceName="Mtv")
        techno_news = News.objects.filter(artists__in=techno_artist).exclude(source__in=news_source).order_by('-publish_date').distinct()

        if len(techno_news) > techno_start+15:
            techno_news = techno_news[techno_start:techno_start+15]
            techno_more = True
        else:
            techno_news = techno_news[techno_start:]
            techno_more = False

        context = {"techno_news": techno_news, "techno_start":techno_start+15}

        template = render_to_string("home/techno_news.html", context, context_instance=RequestContext(request))

        response = {"techno_start": techno_start+15, "techno_more": techno_more, "data": template}

        return HttpResponse(json.dumps(response), content_type="applicaton/json")


def set_user_location(request):

    if request.method == "POST":
        geolat = request.POST.get("latitude", "")
        geolong = request.POST.get("longitude", "")
        redirect_url = request.POST.get("redirect_url", "")

        response = redirect(redirect_url)
        if geolat and geolong:
            response.set_cookie('user_session_lat', geolat)
            response.set_cookie('user_session_long', geolong)
            url = "https://maps.googleapis.com/maps/api/geocode/json?latlng={},{}&sensor=false&result_type=political&key=AIzaSyCJTSEiRDKSHOsV5jbnxrISJXoopUUlhgw".format(geolat, geolong)
            data = requests.get(url).json()
            if data['results']:
                city = data['results'][0]['formatted_address']
                response.set_cookie('user_session_city', city)
        return response

    else:
        response = redirect("index")
        return response


def promoter_link(request):
    return redirect("https://wheresdjsplay.agilecrm.com/landing/5678508399394816")


def dj_link(request):
    return redirect("https://wheresdjsplay.agilecrm.com/landing/5680975992324096")


def agent_link(request):
    return redirect("https://wheresdjsplay.agilecrm.com/landing/5738940401188864")


def demo_video(request):
    return render(request, 'home/demo_video.html', {})


def help(request):
    return render(request, 'home/help.html', {})


def help_available_artist(request):
    return render(request, 'home/help_available.html', {})


def help_compare_artist(request):
    return render(request, 'home/help_comapare.html', {})


def help_global_event(request):
    return render(request, 'home/help_global_event.html', {})


def help_create_event(request):
    return render(request, 'home/help_create_event.html', {})


def faq_avail(request):
    return render(request, 'home/faq_avail.html', {})


def faq_compare(request):
    return render(request, 'home/faq_compare.html', {})


def faq_global_event(request):
    return render(request, 'home/faq_global_events.html', {})


def faq_events(request):
    return render(request, 'home/faq_events.html', {})


def privacy_policy(request):
    return render(request, 'home/privacy_policy.html', {})


def terms(request):
    return render(request, 'home/terms.html', {})

