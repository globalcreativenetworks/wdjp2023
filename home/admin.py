from django.contrib import admin

from .models import CompareDjFaq

class CompareDjFaqAdmin(admin.ModelAdmin):
    list_display = ("question", "answer")

    # formfield_overrides = {
    #     models.CharField: {'widget': TextInput(attrs={'size': '80'})},
    # }

admin.site.register(CompareDjFaq, CompareDjFaqAdmin)
