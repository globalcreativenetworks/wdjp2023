# coding: utf-8
from django.core.management.base import BaseCommand, CommandError
from home.models import CompareDjFaq

faqs_list = [{
    'question':"""What is Wheredjsplay ?""",
    'answer':"""WDJP Is a system that allows promoters, djs and agents to get better information about global talent and make bookings more quickly and efficiently than ever before. We use original searches within our dataset of artists and events to produce easy to understand info about djs. So WDJP shows when they are they might be free- are they close to you, how popular they are. This will provide active promoters with a competitive advantage in their decision making That is why it is initially B2B software and a chargeable service."""
}, {
    'question':"""Where does the date info come from ?""",
    'answer': """We gather information from major portals including Facebook and have a research team in India who research the data and update it on a weekly basis."""
}, {
    'question':"""I want to join WDJP in beta ?""",
    'answer':"""We’ll give you two weeks free access and then it’s 45 USD per month. We are looking for a limited number of active users initially. Please request an invite here.- <a href="/accounts/signup/" > Click Here </a> Please note that at the beta stage WDJP is not open to all promoters and agents so if your invitation is refused do not take offense . When we move out of beta it will probably be open to all and you can re-apply at that stage."""
},{
    'question':"""What do the buttons on the side of the events do ?""",
    'answer':"""They allow you to search for the event parameter on Facebook, google or twitter. This is helpful if you want to find an event promoter, source event artwork or other info online."""
},{
    'question':""" What do the buttons on the side of the artist do ? """,
    'answer':""" ‘Add to favorites’ allows you always to prioritise certain artists in searches. So if you add the fifty acts you most want to book and then click down when you search and prioritise favorites this will ensure that you see these first.<br> ‘Booking request’ allows you to send event info to yourself with the agent’s details thus considerably  speeding up the booking process and giving you records of which acts you have tried to book. ‘Email dates’ allows you to send a snapshot."""
}, {
    'question': """How does Free dates work""",
    'answer': """We look at the online listings that are currently accessible and analyse data, showing what weekends are not booked; we then enter that info into a calendar we send you."""
}, {
    'question': """I tried to book a DJ who seemed to be shown Potentially Available, I emailed the agent and they were booked ?""",
    'answer': """We can’t really help you here as many deals are often in progress and not yet announced online, so we can’t promise you can book a DJ simply because they are not showing dates online."""
}, {
    'question': """It says Potentially Available but I checked On Facebook and they were booked ? Why is that ? You are out of date!""",
    'answer': """We update once a week so it’s always possible that talent has added dates in the interim. That’s why we provide the check buttons for you to use. We will speed up the data updating process over time."""
}, {
    'question': """Why can’t I email Djs directly through Where DJs Play ?""",
    'answer': """We send you the agent’s email address and the necessary details for you to email directly yourself"""
}, {
    'question': """How can you represent so much great talent?""",
    'answer': """We don’t represent artists and we never will.- WDJP is simply a collation and presentation of publicly available information to help promoters, djs and agents make better decisions."""
}, {
    'question': """I love the artists you represent how can I join ?""",
    'answer': """We don’t represent artists and we never will. We are an information service to help promoters and bookers make good decisions, not a booking agency"""
}, {
    'question': """ ‘Similar artist’ displays some weird names that aren’t like the selected artist """,
    'answer': """Sorry we are fixing this the programe is still in the beta stage"""
}, {
    'question': """There’s an artist that I don’t think is an artist from that genre included in the genre group""",
    'answer': """The genre tags are intended to be inclusive and therefore artists will appear in multiple genres and budget selections"""
}, {
    'question': """I want my DJs to be included in your system""",
    'answer': """Please email us, but note that we have selected the talent based on standards of global popularity and do not intend to be a listing for “all djs” so please don’t assume that we will add everyone."""
}, {
    'question': """Why can’t I see all the listing on google maps functionality ?""",
    'answer': """We are working to improve this and it will get better over time,"""
}, {
    'question': """How dare you represent my Dj on your site ?""",
    'answer': """We are sorry you that you don’t like us having your DJ’s details on the site.  But please note we are only collating publicly available information about DJs.   We always direct booking and other requests to registered managements for those reasons.  The searches for information that we carry out will hopefully result in more accurate communications coming to you from the people that you want to hear from.  <br> <br> However if you are upset about this, then please also note the following.  Joining WDJP is currently an invitation-only process for members who make bookings, not at this time accessible for the general public. And finally of course if you wish us to remove any DJ managed by yourselves we will.  Just ask."""
}]

class Command(BaseCommand):
    def handle(self, *args, **options):
        try:
            for faq in faqs_list:
                wdjp_faq = CompareDjFaq(**faq)
                wdjp_faq.save()
        except Exception as del_error:
            print(del_error)
