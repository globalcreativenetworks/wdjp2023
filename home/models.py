from __future__ import unicode_literals

from django.db import models


class CompareDjFaq(models.Model):
    question = models.TextField()
    answer = models.TextField()

    class Meta:
        verbose_name = 'WDJP Faq'
        verbose_name_plural = 'WDJP Faqs'

    def __str__(self):
        return u"{}".format(self.question)
