#!/usr/bin/python


def is_divisible(number, divisor):
    return True if number % divisor == 0 else False

"""
The function will iterate over given range and print labels based on the divisor list given as input.

start: Integer value containing start index. The value will be inclusive.
end: Integer value containing last index to be check. The value will be inclusive.
check_values: Dictionary containing key as divisor and value as label to be printed in case any number is divisible by key.

The function will print number and string generated based on divisibility.
"""


def version_1(start=10, end=40, check_values={3:"Foo", 5:"Bar"}):
    for index in range(start, end+1):
        divisible_list = [key for key in check_values.keys() if is_divisible(index, key)]
        print_string = ''
        for item in divisible_list:
            print_string += check_values.get(item)
        if print_string:
            print ("%d. %s" % (index, print_string))


"""
The function will iterate over given range and print labels based on the divisor list given as input.

start: Integer value containing start index. The value will be inclusive.
end: Integer value containing last index to be check. The value will be inclusive.
check_values: Dictionary containing key as divisor and value as label to be printed in case any number is divisible by key.

The function will print number and string generated based on divisibility.

NOTE: In version there is some change in requirements and now we want to print the string label
        in reverse if it is divisible by all given keys.
"""


def version_2(start=10, end=40, check_values={3:"Foo", 5:"Bar"}):
    for index in range(start, end+1):
        divisible_list = [key for key in check_values.keys() if is_divisible(index, key)]
        print_string = ''
        for item in divisible_list:
            print_string += check_values.get(item)
        if print_string:
            print ("%d. %s" % (index, print_string[::-1] if len(check_values.keys()) == len(divisible_list) else print_string))

if __name__ == '__main__':
    version_1()
    version_2()
