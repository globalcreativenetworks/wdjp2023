from stem.control import Controller
from stem import Signal
import urllib2
import socks
import socket
import time


### Setup steps http://sacharya.com/crawling-anonymously-with-tor-in-python/
class Proxy(object):
    def __init__(self, controlport = 9051, passphrase = "Iasi-100"):
        self.passphrase = passphrase
        self.control_port = controlport
        socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, "127.0.0.1", 9050, True)
        socket.socket = socks.socksocket


    def reconnect(self):
        socks.setdefaultproxy()
        with Controller.from_port(port=self.control_port) as controller:
            controller.authenticate(password=self.passphrase)
            socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, "127.0.0.1", 9050, True)
            socket.socket = socks.socksocket
            if controller.is_newnym_available():
                controller.signal(Signal.NEWNYM)
                time.sleep(0.5)

    def set_proxy(self):
        proxy_support = urllib2.ProxyHandler({"http" : "127.0.0.1:8118", "https" : "127.0.0.1:8118"})
        opener = urllib2.build_opener(proxy_support)
        opener.addheaders = [('User-agent', 'Mozilla/5.0')]
        urllib2.install_opener(opener)

    def renew(self):
        self.reconnect()
        # self.set_proxy()
