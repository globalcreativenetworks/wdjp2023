#!/usr/bin/python
import os
import logging
from logging.handlers import RotatingFileHandler
LOG_NAME = "Parser"


def get_loglevel(loglevel_str):
    loglevel_str = loglevel_str.lower()
    loglevel = {"debug" : logging.DEBUG,
                "info" : logging.INFO,
                "warning" : logging.WARNING,
                "warn" : logging.WARNING,
                "error" : logging.ERROR,
                "fatal" : logging.FATAL,
                "critical" : logging.CRITICAL
                }
    if loglevel_str not in loglevel.keys():
        loglevel_str = "warning" 
        
    return loglevel.get(loglevel_str)


def setup_logging(logdir=None, scrnlog=True, txtlog=True, loglevel=logging.INFO):
    logdir = os.path.abspath(logdir)

    if not os.path.exists(logdir):
        os.mkdir(logdir)

    log = logging.getLogger(LOG_NAME)
    log.setLevel(loglevel)

    log_formatter = logging.Formatter("%(asctime)s - %(levelname)s %(process)d :: %(message)s")

    if txtlog:
        txt_handler = RotatingFileHandler(os.path.join(logdir, "import.log"), mode='a', 
                                          maxBytes=1024*1024*3, backupCount=20)
        #txt_handler.doRollover()
        txt_handler.setFormatter(log_formatter)
        log.addHandler(txt_handler)

    if scrnlog:
        console_handler = logging.StreamHandler()
        console_handler.setFormatter(log_formatter)
        log.addHandler(console_handler)
    
    log.info("Logger initialized.")
    
    return log
