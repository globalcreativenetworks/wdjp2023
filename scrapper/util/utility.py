#!/usr/bin/python
import os
import re
from urlparse import urlparse
from os.path import splitext
from PIL import Image


def add_http(url, is_secure=False):
    http_str = ""
    if None == re.match("^https?://",url,re.IGNORECASE):
        http_str = "https://" if is_secure else "http://"
    
    return "%s%s" % (http_str, url)


def get_path(file_path, subdir):
    head, tail = os.path.split(file_path)
    thumb_dir = os.path.join(head, str(subdir))
    if not os.path.exists(thumb_dir):
        os.makedirs(thumb_dir, 0777)

    return os.path.join(thumb_dir, tail)


def convert_image_to_monochrome(image_path, subdir='mono'):
    outfile = get_path(image_path, subdir)
    image_file = Image.open(image_path)
    image_file = image_file.convert('L')
    image_file.save(outfile)

    return outfile


def resize_image(image_path, resize_list):
    for width, height in resize_list:
        outfile = get_path(image_path, str(width))
        im = Image.open(image_path)
        im_width, im_height = im.size
        if width == 128 or im_width >= width or im_height >= height:
            im.thumbnail((width, height), Image.ANTIALIAS)
        im.save(outfile)

    return outfile


def flip_image(image_path, subdir='flip'):
    outfile = get_path(image_path, subdir)
    img = Image.open(image_path).transpose(Image.FLIP_LEFT_RIGHT)
    img.save(outfile, "JPEG")

    return outfile


def get_ext(url):
    """Return the filename extension from url, or ''."""
    parsed = urlparse(url)
    root, ext = splitext(parsed.path)
    return ext  # or ext[1:] if you don't want the leading '.'