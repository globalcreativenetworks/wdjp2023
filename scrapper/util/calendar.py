#!/usr/bin/python
import time
import datetime
def format_year(year):
    if year and len(year) > 0:
        if len(year) == 2:
            year = "20%s" % year
    return year


def get_numberic_month(month):
    months = {
              'JAN' : '01',
              'FEB' : '02',
              'MAR' : '03',
              'APR' : '04',
              'MAY' : '05',
              'JUN' : '06',
              'JUL' : '07',
              'AUG' : '08',
              'SEP' : '09',
              'OCT' : '10',
              'NOV' : '11',
              'DEC' : '12'
              }
    
    if month is not None:
        month = month.upper()
        if month in months.keys():
            return months[month]
    return ""


def get_current_timestamp():
    ts = time.time()
    timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
    return timestamp
