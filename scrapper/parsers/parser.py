#!/usr/bin/python
import os
import re
import logging
from scrapper.logger import LOG_NAME
from scrapper.util import (add_http, get_ext, resize_image, convert_image_to_monochrome, flip_image)
logger = logging.getLogger(LOG_NAME)


class Parser(object):
    artist_photo_dir = "artistphotos"
    media_dir = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "media")

    def __init__(self, artist, source_name):
        self.artist = artist
        self.source_name = source_name

    def scrape_similar_artist(self):
        return []

    def get_artist_image_name(self, url):
        return "%s%s" % (self.artist.artistID, get_ext(url))

    @staticmethod
    def prepare_artist_image_url(source_base_url, image_url):
        artist_image_url = image_url if re.match("^https?://",image_url, re.IGNORECASE) \
            else source_base_url + "/" + image_url
        return add_http(artist_image_url)

    @staticmethod
    def get_artist_image_path():
        artist_image_path = os.path.join(Parser.media_dir, Parser.artist_photo_dir)
        return artist_image_path

    def process_image(self, image_url):
        processed_data = {}
        file_name = self.get_artist_image_name(image_url)
        original_image = Parser.download_image(image_url, file_name)
        resize_list = [(50, 50), (128, 128), (160, 160), (470, 394), (800, 533), (640, 427), (350, 233)]
        resize_image(original_image, resize_list)
        monochrome_image = convert_image_to_monochrome(original_image)
        flipped_image = flip_image(original_image)

        processed_data['sourceImage'] = image_url
        processed_data['originalImage'] = original_image.replace(Parser.media_dir, '')
        processed_data['monoImage'] = monochrome_image.replace(Parser.media_dir, '')
        processed_data['flipImage'] = flipped_image.replace(Parser.media_dir, '')
        return processed_data

    @staticmethod
    def download_image(image_url, image_file_name):
        import requests
        import shutil
        artist_image_path = Parser.get_artist_image_path()
        if not os.path.exists(artist_image_path):
            os.makedirs(artist_image_path, 0777)
        image_path = os.path.join(artist_image_path, image_file_name)
        logger.info("Downloading image from %s to %s" %
                    (image_url, image_path))
        response = requests.get(add_http(image_url), stream=True)
        if response.status_code == 200:
            with open(image_path, 'wb') as image_file:
                response.raw.decode_content = True
                shutil.copyfileobj(response.raw, image_file)
            logger.info("File %s downloaded successfully." % image_path)

            return image_path
        raise Exception("Failed to Download image (%s) with error code: %s" %
                        (image_url, response.status_code))

    def validate(self):
        if self.source_name not in self.artist.artistSources.keys():
            raise Exception("%s source is not set for %s" %
                            (self.source_name, self.artist.artistName))

        url = self.artist.artistSources[self.source_name]['url']

        if url is None or len(url) <= 0:
            raise Exception("Blank url set for Artist: %s, Source: %s" %
                            (self.artist.artistName, self.source_name))
        self.artist.artistSources[self.source_name]['url'] = add_http(url)


class ParsedItem(object):
    def __init__(self):
        self.PK = 0
        self.eventID = 0
        self.eventName = ""
        self.venueID = ""
        self.venueName = ""
        self.venueAddress = ""
        self.latitude = 0
        self.longitude = 0
        self.date = ""
        self.startTime = ""
        self.endTime = ""
        self.city = ""
        self.country = ""
        self.artistID = 0
        self.sourceID = 0

    def print_info(self, extra_info = ""):
        logger.info("Parsed Event Info(%s):: EventID: %s, EventName: %s, VenueID: %s, VenueName: %s, VenueAddress: %s, "
                    "Latitude: %s,longitude: %s,Date: %s, StartTime: %s, EndTime: %s, City: %s, Country: %s" %
                    (
                         extra_info,
                         self.eventID,
                         self.eventName,
                         self.venueID,
                         self.venueName,
                         self.venueAddress,
                         self.latitude,
                         self.longitude,
                         self.date,
                         self.startTime,
                         self.endTime,
                         self.city,
                         self.country
                     ))
