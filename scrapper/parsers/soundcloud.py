#!/usr/bin/python
import logging
import scrapper.requests , urllib
from scrapper.fuzzywuzzy import fuzz
from scrapper.fuzzywuzzy import process
from array import array
import difflib
from time import sleep
from bs4 import BeautifulSoup
from scrapper.parsers import Parser, ParsedItem, Processor
from scrapper.parsers.source import SourceType
from scrapper.logger import LOG_NAME

logger = logging.getLogger(LOG_NAME)


class SoundcloudParser(Parser):
    source_base_url = "https://www.bandsintown.com"
    def __init__(self, artist):
        Parser.__init__(self, artist, SourceType.Soundcloud)
        logger.info("Creating %s instance. Artist Name: %s, Source: %s" % 
                    (self.__class__.__name__, self.artist.artistName, self.source_name))
    
    def scrape(self):
        return []


class SoundcloudProcessor (Processor):
    def __init__(self): 
        Processor.__init__(self, SourceType.Soundcloud)
        logger.info("Creating %s instance. Source: %s" % 
                    (self.__class__.__name__, self.source_name))
    
    def process(self, database, sourceID, artistID):
        pass