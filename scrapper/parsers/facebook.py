#!/usr/bin/python
import logging
import scrapper.requests , urllib
from scrapper.fuzzywuzzy import fuzz
from scrapper.fuzzywuzzy import process
from array import array
from unidecode import unidecode
import difflib
import time
from time import sleep
from bs4 import BeautifulSoup
from scrapper.parsers import Parser, ParsedItem, Processor
from scrapper.parsers.source import SourceType
from scrapper.logger import LOG_NAME
from social_data.facebook import FacebookAPI
from social_data.facebook import scrap_facebook_data
logger = logging.getLogger(LOG_NAME)


class FacebookParser (Parser):

    def __init__(self, artist):
        Parser.__init__(self, artist, SourceType.Facebook)
        logger.info("Creating %s instance. Artist Name: %s, Source: %s" %
                    (self.__class__.__name__, self.artist.artistName, self.source_name))

    def scrape(self):
        self.validate()
        facebookevent = FacebookAPI()
        facebookevent.set_pageid_from_artist(self.artist.artistID)
        api_data = facebookevent.get_details()
        if api_data.has_key('error'):
            raise Exception("Artist Facebook Url is wrong")
            return []
        scrap_facebook_data(api_data, artists=[self.artist.artistID])
        if 'events' in api_data:
            events = api_data['events']['data']
        else:
            events = []
        parsed_items = []
        for event in events:
            try:
                parsed_item = ParsedItem()
                parsed_item.artistID = self.artist.artistID
                parsed_item.sourceID = self.artist.artistSources[self.source_name]['id']
                parsed_item.eventName = unidecode(event['name'])
                parsed_item.eventID = event['id']
                parsed_item.startTime = time.strftime('%H-%M', time.strptime(event['start_time'][:-5], '%Y-%m-%dT%H:%M:%S'))
                parsed_item.date = time.strftime('%Y-%m-%d', time.strptime(event['start_time'][:-5], '%Y-%m-%dT%H:%M:%S'))
                if event.has_key('end_time'):
                    parsed_item.endTime = time.strftime('%H-%M', time.strptime(event['end_time'][:-5], '%Y-%m-%dT%H:%M:%S'))
                if event.has_key('place'):
                    parsed_item.venueName = event['place']['name']
                    if event['place'].has_key('id'):
                        parsed_item.venueID = event['place']['id']
                    if event['place']['location'].has_key('street'):
                        parsed_item.venueAddress = event['place']['name'] + "," + event['place']['location']['street']
                    else:
                        parsed_item.venueAddress = event['place']['name']
                    if event['place']['location'].has_key('city'):
                        parsed_item.city = event['place']['location']['city']
                    else:
                        parsed_item.city = ""
                    if event['place']['location'].has_key('country'):
                        parsed_item.country = event['place']['location']['country']
                    else:
                        parsed_item.country = ""
                    if event['place']['location'].has_key('latitude'):
                        parsed_item.latitude = event['place']['location']['latitude']
                    if event['place']['location'].has_key('longitude'):
                        parsed_item.longitude = event['place']['location']['longitude']
                else:
                    raise Exception("Event do not have enough data")

                parsed_item.print_info(extra_info="%s-%s" % (self.artist.artistName, self.source_name))
                parsed_items.append(parsed_item)
            except Exception as ex:
                logger.exception(ex)
                continue

        logger.info("FacebookParser:: Scrapping Ended for Artist Name: %s, Source: %s, url: %s" %
                    (self.artist.artistName,
                     self.source_name, self.artist.artistSources[self.source_name]))
        return parsed_items


class FacebookProcessor (Processor):
    def __init__(self):
        Processor.__init__(self, SourceType.Facebook)
        logger.info("Creating %s instance. Source: %s" %
                    (self.__class__.__name__, self.source_name))

    def process(self, database, sourceID, artistID):
        try:  # open text file with bandsintown profiles

            db = database.con
            cursor = db.cursor(buffered=True)
            print "PR0CESS FACEBOOK"
            sleep(2)
            try:
                query = "SELECT PK, eventID, eventName, venueID, venueName, venueAddress, date, startTime, city, country, artistID, sourceID FROM dj_parsed_data where sourceID = '%s' and artistID = '%s'" % (sourceID, artistID)  # where eventID not in (Select bandsintownID from dj_Events where bandsintownID > 0) "
                cursor.execute(query)
            except Exception:
                print "No data from Facebook for this artistID = '%s'"%(artistID)
                return
            bands = cursor.fetchall()
            query2 = "SELECT * FROM dj_Venues"
            cursor.execute(query2)
            venues = cursor.fetchall()
            query3 = "SELECT * FROM dj_Events"
            cursor.execute(query3)
            events = cursor.fetchall()
            comun = array('H')
            i = 0

            # (%s)
            # comun1=', '.join(map(lambda x: '%s',comun))
            # query=query % ('%s',comun1)

            for row in bands:

                pk = row[0]
                eventID = row[1]
                eventName = row[2]
                venueID = row[3]
                venueName = row[4]
                venueAddress = row[5]
                date = row[6]
                start = row[7]
                city = row[8]
                country = row[9]
                artistID = row[10]
                sourceID = row[11]
                if "\\" in eventName:
                    eventName = eventName.replace("\\", "")
                if "\'" in eventName:
                    eventName = eventName.replace("\'", "\\'")
                elif "'" in eventName:
                    eventName = eventName.replace("'", "\'")
                if "\\" in venueName:
                    venueName = venueName.replace("\\", "")
                if "\'" in venueName:
                    venueName = venueName.replace("\'", "\\'")
                elif "'" in venueName:
                    venueName = venueName.replace("'", "\'")
                print pk
                query = "Select venueID,venueFacebook,venueName from dj_Venues where venueFacebook like '" + str(
                    venueID) + "'"
                cursor.execute(query)

                if cursor.rowcount > 0:

                    result = cursor.fetchone()
                    VID = result[0]
                    vname = result[2]
                    print VID, vname, eventID, pk
                    print "EXISTENT VENUE"
                    if "\\" in vname:
                        vname = vname.replace("\\", "")
                    if "\'" in vname:
                        vname = vname.replace("\'", "\\'")
                    elif "'" in vname:
                        vname = vname.replace("'", "\'")
                    query = "Select facebookID,eventID from dj_Events where facebookID like '" + str(
                        eventID) + "'"
                    cursor.execute(query)

                    if cursor.rowcount > 0:
                        print "EXISTENT EVENT"
                        comun.append(pk)
                        EID = cursor.fetchone()[1]
                        # query = "Select * from dj_Master where artistID like '" + str(
                        #     artistID) + "' and eventID in (Select eventID from dj_Events where date like '" + str(
                        #     date) + "')"  # like'"+str(EID)+"' and venueID like '"+str(VID)+"'"

                        query = "Select * from dj_Master where artistID like '" + str(
                            artistID) + "' and eventID like'"+str(EID)+"' and venueID like '"+str(VID)+"'"
                        cursor.execute(query)
                        if cursor.rowcount == 0:
                            print "Insert to master"
                            query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
                            variable = (artistID, EID, VID)
                            cursor.execute(query, variable)
                            db.commit()
                    else:  # the event is not in DB with this id and we try to match it with the existing one and find a possible match

                        for row in events:
                            eventName1 = row[1]
                            venueEv = row[2]
                            bandsintownID = row[3]
                            residentID = row[4]
                            gigatoolsID = row[5]
                            date1 = row[6]
                            start1 = row[7]
                            end1 = row[8]
                            facebookID = row[9]
                            if "\\" in eventName1:
                                eventName1 = eventName1.replace("\\", "")
                            if "\'" in eventName1:
                                eventName1 = eventName1.replace("\'", "\\'")
                            elif "'" in eventName1:
                                eventName1 = eventName1.replace("'", "\'")
                            if vname == venueEv:
                                if date == date1:  # if match found update it with bands ID
                                    print eventName + " || " + eventName1 + " = EVENTS ON " + str(date) + "=" + str(
                                        date1)
                                    query = "Select eventID from dj_Events where facebookID like '" + str(
                                        eventID) + "'"
                                    cursor.execute(query)
                                    if cursor.rowcount > 0:
                                        print "ALREADY"
                                        EID = cursor.fetchone()[0]
                                        print pk
                                        try:
                                            pk = ' '.join(map(str, pk))
                                            print pk, type(pk)
                                            pk = int(pk)
                                            print pk, type(pk)
                                        except:
                                            print "except"
                                            pk = float(pk)
                                            pk = int(pk)

                                        comun.append(pk)
                                    else:
                                        print "UPDATE EVENTS"
                                        query = "Update dj_Events set facebookID=%s,sourceFacebook=%s where eventName like '" + eventName1.encode(
                                            'utf-8') + "' and evenueName like '" + vname.encode(
                                            'utf-8') + "' and date like '" + str(date1) + "'"
                                        variable = (eventID, sourceID)
                                        cursor.execute(query, variable)
                                        db.commit()
                                        comun.append(pk)
                                        query = "Select eventID from dj_Events where eventName like '" + eventName1.encode(
                                            'utf-8') + "' and date like '" + str(date1) + "'"

                                        cursor.execute(query)

                                        pk = cursor.fetchone()
                                        EID = pk[0]

                                    # query = "Select * from dj_Master where artistID like '" + str(
                                    #     artistID) + "' and eventID in (Select eventID from dj_Events where date like '" + str(
                                    #     date1) + "')"  # like '"+str(EID)+"' and venueID like '"+str(VID)+"'"

                                    query = "Select * from dj_Master where artistID like '" + str(
                                        artistID) + "' and eventID like '" + str(
                                        EID) + "' and venueID like '" + str(VID) + "'"

                                    cursor.execute(query)
                                    if cursor.rowcount == 0:
                                        print "Insert to master"
                                        query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
                                        variable = (artistID, EID, VID)
                                        cursor.execute(query, variable)
                                        db.commit()
                                else:  # check again to see if was inserted on last iteration in events matching and if exists go to next
                                    query = "Select facebookID,eventID from dj_Events where facebookID like '" + str(
                                        eventID) + "'"
                                    cursor.execute(query)
                                    if cursor.rowcount > 0:
                                        print "DUPLICATE EVENT"
                                        EID = cursor.fetchone()[1]
                                    else:  # the event is completely new and we insert it
                                        print "CREATING EVENT"
                                        query = "INSERT IGNORE INTO dj_Events(eventName,evenueName,facebookID,date,startTime,sourceFacebook) VALUES (%s,%s,%s,%s,%s,%s) ON DUPLICATE KEY UPDATE eventName=eventName,evenueName=evenueName,facebookID=facebookID,date=date,startTime=startTime,sourceFacebook=sourceFacebook"
                                        variable = (eventName, vname, eventID, date, start, sourceID)
                                        cursor.execute(query, variable)
                                        db.commit()
                                        query = "Select eventID from dj_Events where facebookID like '" + str(
                                            eventID) + "' and date like '" + str(date) + "'"
                                        print pk
                                        comun.append(pk)
                                        cursor.execute(query)
                                        if cursor.rowcount > 0:
                                            EID = cursor.fetchone()[0]

                                    query = "Select * from dj_Master where artistID like '" + str(
                                        artistID) + "' and eventID like '" + str(EID) + "' and venueID like '" + str(VID) + "'"


                                    # query = "Select * from dj_Master where artistID like '" + str(
                                    #     artistID) + "' and eventID in (Select eventID from dj_Events where date like '" + str(
                                    #     date) + "')"  # eventID like '"+str(EID)+"' and venueID like '"+str(VID)+"'"

                                    cursor.execute(query)
                                    if cursor.rowcount == 0:
                                        print "Insert to master"
                                        query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
                                        variable = (artistID, EID, VID)
                                        cursor.execute(query, variable)
                                        db.commit()
                                        EID = 0
                    VID = 0
                else:
                    for row in venues:
                        venueID1 = row[0]
                        venueName1 = row[1]
                        venueAddress1 = row[2]
                        venueBands = row[5]
                        venueResident = row[6]
                        venueGigatools = row[7]

                        city1 = row[9]
                        country1 = row[10]
                        venueFacebook = row[11]
                        VN = fuzz.token_set_ratio(venueName, venueName1)
                        CT = fuzz.token_set_ratio(country, country1)
                        if venueAddress == '' or venueAddress1 == '':
                            string = 1
                        else:
                            AD = fuzz.token_set_ratio(venueAddress, venueAddress1)
                            string = 2
                        # print "Ev1:%s"%event+"-Ev2:%s"%event1+"=%s" %ptr
                        if VN >= 95:
                            if string == 1:
                                if CT >= 90:
                                    print venueName + " || " + venueName1 + "=" + str(VN) + " ARE THE SAME VENUES"
                                    address = venueAddress1
                                    region = country1
                                    venue = venueName1
                                    if "\\" in venue:
                                        venue = venue.replace("\\", "")
                                    if "\'" in venue:
                                        venue = venue.replace("\'", "\\'")
                                    elif "'" in venue:
                                        venue = venue.replace("'", "\'")
                                    city1 = city
                                    comun.append(pk)
                                    query = "Select venueID from dj_Venues where venueFacebook like '" + str(
                                        venueID) + "' and country like '" + region + "' LOCK IN SHARE MODE"
                                    cursor.execute(query)
                                    comun.append(pk)
                                    if cursor.rowcount > 0:
                                        print "dont update"
                                        VID = cursor.fetchone()[0]
                                    else:
                                        print "Update"
                                        query = "Update dj_Venues set venueFacebook=%s,city=%s where venueName like '" + venue.encode(
                                            'utf-8') + "' and country like '" + region + "'"
                                        variable = (venueID, city1)
                                        cursor.execute(query, variable)
                                        db.commit()
                                        query = "Select venueID from dj_Venues where venueName like '" + venue.encode(
                                            'utf-8') + "' or venueName like '" + venueName.encode(
                                            'utf-8') + "' and country like '" + region + "'"
                                        cursor.execute(query)
                                        VID = cursor.fetchone()[0]
                                    for row in events:
                                        eventName1 = row[1]
                                        venueEv = row[2]
                                        bandsintownID = row[3]
                                        residentID = row[4]
                                        gigatoolsID = row[5]
                                        date1 = row[6]
                                        start1 = row[7]
                                        end1 = row[8]
                                        facebookID = row[9]
                                        if "\\" in eventName1:
                                            eventName1 = eventName1.replace("\\", "")
                                        if "\'" in eventName1:
                                            eventName1 = eventName1.replace("\'", "\\'")
                                        elif "'" in eventName1:
                                            eventName1 = eventName1.replace("'", "\'")
                                        if "\\" in eventName:
                                            eventName = eventName.replace("\\", "")
                                        if "\'" in eventName:
                                            eventName = eventName.replace("\'", "\\'")
                                        elif "'" in eventName:
                                            eventName = eventName.replace("'", "\'")
                                        if venue == venueEv:
                                            if date == date1:
                                                print eventName + " || " + eventName1 + " ARE THE SAME EVENTS ON " + str(
                                                    date) + "=" + str(date1)

                                                query = "Select eventID from dj_Events where facebookID like '" + str(
                                                    eventID) + "' and eventName like '" + eventName1.encode(
                                                    'utf-8') + "' or eventName like '" + eventName.encode(
                                                    'utf-8') + "' and evenueName like '" + venue.encode(
                                                    'utf-8') + "' and date like '" + str(date1) + "' LOCK IN SHARE MODE"
                                                cursor.execute(query)
                                                if cursor.rowcount > 0:
                                                    print "ALREADY"
                                                    EID = cursor.fetchone()[0]
                                                else:
                                                    print "UPDATE EVENTS"
                                                    query = "Update dj_Events set facebookID=%s,sourceFacebook=%s where eventName like '" + eventName1.encode(
                                                        'utf-8') + "' and evenueName like '" + venue.encode(
                                                        'utf-8') + "' and date like '" + str(date1) + "'"
                                                    variable = (eventID, sourceID)
                                                    cursor.execute(query, variable)
                                                    db.commit()
                                                    query = "Select eventID from dj_Events where eventName like '" + eventName1.encode(
                                                        'utf-8') + "' or eventName like '" + eventName.encode(
                                                        'utf-8') + "' and evenueName like '" + venue.encode(
                                                        'utf-8') + "' and date like '" + str(date1) + "'"
                                                    cursor.execute(query)
                                                    EID = cursor.fetchone()[0]
                                                # query = "Select * from dj_Master where artistID like '" + str(
                                                #     artistID) + "' and eventID in (Select eventID from dj_Events where date like '" + str(
                                                #     date1) + "')"  # eventID like '"+str(EID)+"' and venueID like '"+str(VID)+"'"

                                                query = "Select * from dj_Master where artistID like '" + str(
                                                    artistID) + "' and eventID like '" + str(
                                                    EID) + "' and venueID like '" + str(VID) + "'"

                                                cursor.execute(query)
                                                if cursor.rowcount == 0:
                                                    print "Insert to master"
                                                    query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
                                                    variable = (artistID, EID, VID)
                                                    cursor.execute(query, variable)
                                                    db.commit()
                                                EID = 0
                                    VID = 0

                            elif string == 2:
                                if CT >= 90 and AD >= 60:
                                    print venueName + " || " + venueName1 + "=" + str(VN) + " ARE THE SAME VENUES"
                                    address = venueAddress1
                                    region = country1
                                    venue = venueName1
                                    if "\\" in venue:
                                        venue = venue.replace("\\", "")
                                    if "\'" in venue:
                                        venue = venue.replace("\'", "\\'")
                                    elif "'" in venue:
                                        venue = venue.replace("'", "\'")

                                    city1 = city
                                    comun.append(pk)
                                    query = "Select venueID from dj_Venues where venueFacebook like '" + str(
                                        venueID) + "' and country like '" + region + "' LOCK IN SHARE MODE"
                                    cursor.execute(query)
                                    comun.append(pk)
                                    if cursor.rowcount > 0:
                                        print "dont update"
                                        VID = cursor.fetchone()[0]
                                    else:
                                        print "Update"
                                        query = "Update dj_Venues set venueFacebook=%s,city=%s where venueName like '" + venue + "' and country like '" + region + "'"
                                        variable = (venueID, city1)
                                        cursor.execute(query, variable)
                                        db.commit()
                                        query = "Select venueID from dj_Venues where venueName like '" + str(
                                            venue) + "' and country like '" + region + "'"
                                        cursor.execute(query)
                                        VID = cursor.fetchone()[0]
                                    for row in events:
                                        eventName1 = row[1]
                                        venueEv = row[2]
                                        bandsintownID = row[3]
                                        residentID = row[4]
                                        gigatoolsID = row[5]
                                        date1 = row[6]
                                        start1 = row[7]
                                        end1 = row[8]
                                        facebookID = row[9]
                                        if "\\" in eventName1:
                                            eventName1 = eventName1.replace("\\", "")
                                        if "\'" in eventName1:
                                            eventName1 = eventName1.replace("\'", "\\'")
                                        elif "'" in eventName1:
                                            eventName1 = eventName1.replace("'", "\'")
                                        if "\\" in eventName:
                                            eventName = eventName.replace("\\", "")
                                        if "\'" in eventName:
                                            eventName = eventName.replace("\'", "\\'")
                                        elif "'" in eventName:
                                            eventName = eventName.replace("'", "\'")
                                        if venue == venueEv:
                                            if date == date1:
                                                print eventName + " || " + eventName1 + " ARE THE SAME EVENTS ON " + str(
                                                    date) + "=" + str(date1)

                                                query = "Select eventID from dj_Events where facebookID like '" + str(
                                                    eventID) + "' and eventName like '" + eventName1.encode(
                                                    'utf-8') + "' or eventName like '" + eventName.encode(
                                                    'utf-8') + "' and evenueName like '" + venue.encode(
                                                    'utf-8') + "' and date like '" + str(date1) + "' LOCK IN SHARE MODE"
                                                cursor.execute(query)
                                                if cursor.rowcount > 0:
                                                    print "ALREADY"
                                                    EID = cursor.fetchone()[0]
                                                else:
                                                    print "UPDATE EVENTS"
                                                    query = "Update dj_Events set facebookID=%s,sourceFacebook=%s where eventName like '" + eventName1.encode(
                                                        'utf-8') + "' and evenueName like '" + venue.encode(
                                                        'utf-8') + "' and date like '" + str(date1) + "'"
                                                    variable = (eventID, sourceID)
                                                    cursor.execute(query, variable)
                                                    db.commit()
                                                    query = "Select eventID from dj_Events where eventName like '" + eventName1.encode(
                                                        'utf-8') + "' or facebookID like '" + str(
                                                        eventID) + "' and evenueName like '" + venue.encode(
                                                        'utf-8') + "' and date like '" + str(date1) + "'"
                                                    cursor.execute(query)
                                                    EID = cursor.fetchone()[0]
                                                query = "Select * from dj_Master where artistID like '" + str(
                                                    artistID) + "' and eventID in (Select eventID from dj_Events where date like '" + str(
                                                    date) + "')"  # eventID like '"+str(EID)+"' and venueID like '"+str(VID)+"'"

                                                query = "Select * from dj_Master where artistID like '" + str(
                                                    artistID) + "' and eventID like '" + str(
                                                    EID) + "' and venueID like '" + str(VID) + "'"

                                                cursor.execute(query)
                                                if cursor.rowcount == 0:
                                                    print "Insert to master"
                                                    query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
                                                    variable = (artistID, EID, VID)
                                                    cursor.execute(query, variable)
                                                    db.commit()
                                                EID = 0
                                    VID = 0
                i += 1

            myTuple = tuple(comun)
            print myTuple
            lenght_of_tuple = len(myTuple)
            try:
                if lenght_of_tuple > 1:
                    query = "SELECT PK, eventID, eventName, venueID, venueName, venueAddress, date, startTime, city, country, artistID, sourceID FROM dj_parsed_data where sourceID = '%s' and artistID = '%s' and PK not in %s" % (
                    sourceID, artistID, str(myTuple))
                elif lenght_of_tuple == 1:
                    query = "SELECT PK, eventID, eventName, venueID, venueName, venueAddress, date, startTime, city, country, artistID, sourceID FROM dj_parsed_data where sourceID = '%s' and artistID = '%s' and PK <> %s" % (
                    sourceID, artistID, str(myTuple[0]))
                else:
                    query = "SELECT PK, eventID, eventName, venueID, venueName, venueAddress, date, startTime, city, country, artistID, sourceID FROM dj_parsed_data where sourceID = '%s' and artistID = '%s'" % (
                    sourceID, artistID)
                print query
                cursor.execute(query)
            except Exception:
                print "No data from Facebook for this artistID = '%s'"%(artistID)
                return
            unic = cursor.fetchall()
            for row in unic:
                pk = row[0]
                eventID = row[1]
                eventName = row[2]
                venueID = row[3]
                venueName = row[4]
                venueAddress = row[5]
                date = row[6]
                start = row[7]
                city = row[8]
                country = row[9]
                artistID = row[10]
                sourceID = row[11]
                if "\\" in eventName:
                    eventName = eventName.replace("\\", "")
                '''if "\'" in eventName:
                    eventName=eventName.replace("\'","\\'")'''
                if "'" in eventName:
                    eventName = eventName.replace("'", "\'")
                if "\\" in venueName:
                    venueName = venueName.replace("\\", "")
                '''if "\'" in venueName:
                    venueName=venueName.replace("\'","\\'")'''
                if "'" in venueName:
                    venueName = venueName.replace("'", "\'")
                query = "Select venueID from dj_Venues where venueFacebook like '" + str(
                    venueID) + "' and country like '" + country + "'"
                cursor.execute(query)
                if cursor.rowcount > 0:
                    print "VENUE JUST INSERTED,DUPLICATE"
                    VID = cursor.fetchone()[0]
                else:
                    query = "INSERT INTO dj_Venues( venueName,venueAddress,venueFacebook,city,country) VALUES (%s,%s,%s,%s,%s)"
                    variable = (venueName, venueAddress, venueID, city, country)
                    cursor.execute(query, variable)
                    db.commit()
                    if "\\" in venueName:
                        venueName = venueName.replace("\\", "")
                    '''if "\'" in venueName:
                        venueName=venueName.replace("\'","\\'")'''
                    if "'" in venueName:
                        venueName = venueName.replace("'", "\'")
                    query = "Select venueID from dj_Venues where venueFacebook like '" + str(
                        venueID) + "' and country like '" + country + "'"
                    cursor.execute(query)
                    VID = cursor.fetchone()[0]

                query = "Select eventID from dj_Events where facebookID like '" + str(
                    eventID) + "' and date like '" + str(date) + "'"  # eventName like '"+eventName+"' and
                cursor.execute(query)
                if cursor.rowcount > 0:
                    print "EVENT JUST INSERTED,DUPLICATE"
                    EID = cursor.fetchone()[0]
                else:
                    # current_date_time = datetime.now()
                    query = "INSERT INTO dj_Events(eventName,evenueName,facebookID,date,startTime,sourceFacebook) VALUES (%s,%s,%s,%s,%s,%s)"
                    variable = (eventName, venueName, eventID, date, start, sourceID)
                    cursor.execute(query, variable)
                    db.commit()
                    query = "Select eventID from dj_Events where facebookID like '" + str(
                        eventID) + "' and date like '" + str(date) + "'"
                    cursor.execute(query)
                    EID = cursor.fetchone()[0]
                # query = "Select * from dj_Master where artistID like '" + str(
                #     artistID) + "' and eventID in (Select eventID from dj_Events where date like '" + str(
                #     date) + "')"  # eventID like '"+str(EID)+"' and venueID like '"+str(VID)+"'"
                query = "Select * from dj_Master where artistID like '" + str(
                    artistID) + "' and eventID like '" + str(
                    EID) + "' and venueID like '" + str(VID) + "'"

                cursor.execute(query)
                if cursor.rowcount == 0:
                    query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
                    variable = (artistID, EID, VID)
                    cursor.execute(query, variable)
                    db.commit()
                EID = 0
                VID = 0
            cursor.close()
        except Exception, ex:
            logger.exception(ex);
