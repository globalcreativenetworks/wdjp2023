#!/usr/bin/python
import logging
from scrapper.parsers import Parser, ParsedItem, Processor
from scrapper.parsers.source import SourceType
from scrapper.logger import LOG_NAME

from social_data.youtube import scrap_youtube_api
logger = logging.getLogger(LOG_NAME)


class YoutubeParser (Parser):


    def __init__(self, artist):
        Parser.__init__(self, artist, SourceType.Youtube)
        logger.info("Creating %s instance. Artist Name: %s, Source: %s" %
                    (self.__class__.__name__, self.artist.artistName, self.source_name))

    def scrape(self):
        self.validate()
        scrap_youtube_api([self.artist.artistID])
        logger.info("Twiiter API Data scrapped for the ArtistName %s, Source: %s" %
                    (self.artist.artistName, self.source_name))
        return []


class YoutubeProcessor (Processor):
    def __init__(self):
        Processor.__init__(self, SourceType.Youtube)
        logger.info("Creating %s instance. Source: %s" %
                    (self.__class__.__name__, self.source_name))

    def process(self, database, sourceID, artistID):
        pass
