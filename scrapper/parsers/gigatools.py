#!/usr/bin/python
import logging, requests
import scrapper.requests

from datetime import datetime, date

from operator import concat
from time import sleep
from bs4 import BeautifulSoup
from report.models import ReportGigatools
from report.utils import *
from scrapper.fuzzywuzzy import fuzz
from scrapper.fuzzywuzzy import process
from array import array
from scrapper.parsers import Parser, ParsedItem, Processor
from scrapper.parsers.source import SourceType
from scrapper.logger import LOG_NAME
from scrapper.util.calendar import format_year, get_numberic_month
import sys

from social_data.models import DataLog

logger = logging.getLogger(LOG_NAME)


class GigatoolsParser(Parser):
	source_base_url = "https://gigs.gigatools.com"
	today_date = date.today()

	def __init__(self, artist):
		Parser.__init__(self, artist, SourceType.Gigatools)
		logger.info("Creating %s instance. Artist Name: %s, Source: %s" %
					(self.__class__.__name__, self.artist.artistName, self.source_name))

	def scrape(self):
		self.validate()
		source_url = self.artist.artistSources[self.source_name]['url']

		logger.info("GigatoolsParser:: Scrapping initiated for Artist Name: %s, Source: %s, url: %s" %
					(self.artist.artistName,
					 self.source_name, source_url))

		source_request = requests.get(source_url)

		logger.info("GigatoolsParser:: Url: %s, Response content length: %s"
					% (source_url, len(source_request.content)))

		source_soup = BeautifulSoup(source_request.content, "html.parser")
		try:
			Name = source_soup.find("title")
			data = Name.text.replace('\'s Upcoming Gigs', ' ')
			print "DD: %s" % data
		except:
			pass
		parsed_items = []

		events = source_soup.find('div', attrs={'class': 'events'})
		month = ""
		count = 0
		for div in events.findAll('div'):
			try:
				if (div.get("class")[0] == "monthLeft"):
					mon = div.text
					month = mon.strip()[:3]
					year = mon.strip()[-2:]
					year = format_year(year)  # year as 2016 for i.e
					month = get_numberic_month(month)

				if (div.get("class")[0] == "number"):
					nbr = div.text.strip()
					nbr = nbr.zfill(2)
				if (div.get("class")[0] == "name"):
					eventname=div.text.replace(",","").strip()
					eventid=div.a['href'].split('/')[2]

					# eventname = str(div).strip().replace(",", ' ').split('name">')[1].split('</div>')[0].split('<br>')[0]
					# eventname = eventname.split('href="')[1]
					# eventname1 = "<a target=\"_blank\" href=\"" + "https://gigs.gigatools.com" + eventname  # Event name to store in DB
					# eventname2 = eventname.split('">')[1].split('</a>')[0]
					# eventid = eventname1.split('/')[4].split('">')[0]  # Event id to store in DB
					sleep(0.5)
				# while True:
				# try:
				if (div.get("class")[0] == "records"):
					venue = str(div).strip().split(',')[0].split('records">')[1].split('</div>')[0] + "</a>"
					venue = venue.split('href="')[1]
					# venue="<a target=\"_blank\" href=\""+"https://gigs.gigatools.com"+venue
					# Venue name to store in DB
					venueid = venue.split('/')[2].split('">')[0]  # VenueID to store in DB
					venue = venue.split('">')[1].split('</a>')[0]
					location = div.text.strip().split(',')[1]
					location = concat(location, ",")
					location = concat(location, div.text.strip().split(',')[2])
					city = location.split(',')[0]  # City to store in database
					country = location.split(',')[1]  # Country to store in database
					date = year + '-' + month + '-' + nbr  # date to store in DB

					try:
						if (div.get("class")[0] == "desc"):
							start_time = div.text.strip()
							if 'Showtime' in start_time:
								start_time = start_time.split('time:')[1].split('http')[0]
							else:
								start_time = ''
					except:
						pass

					# except:
					#	continue
					# break
					# print eventid
					# print eventname2
					# print venue
					# print venueid
					# print date
					# print city
					# print country
					# print start_time

					parsed_item = ParsedItem()
					parsed_item.artistID = self.artist.artistID
					parsed_item.sourceID = self.artist.artistSources[self.source_name]['id']
					parsed_item.eventID = eventid
					parsed_item.eventName = eventname
					parsed_item.venueID = venueid
					parsed_item.venueName = venue
					parsed_item.startTime = start_time
					parsed_item.date = date
					parsed_item.city = city
					parsed_item.country = country
					parsed_item.print_info(extra_info="%s-%s" % (self.artist.artistName, self.source_name))
					parsed_items.append(parsed_item)

					count += 1

				eventid = eventid
				eventname2 = ''
				venue = ''
				venueid = ''
				date = ''
				city = ''
				country = ''
				start_time = ''

			except:
				pass

		report = ReportGigatools(created=self.today_date,
								artist=self.artDBObject)
		report.title = "Artist: %s was scrapped" % self.artist.artistName
		report.description = "Succesful scrape Events : " + str(count)
		report.event = INFO
		report.resolution = NA
		report.save()
		gigatools_log, created = DataLog.objects.get_or_create(created=GigatoolsParser.today_date)
		if gigatools_log.events_gigatools:
			events_gigatools_count = int(gigatools_log.events_gigatools)
			events_gigatools_count += count
			gigatools_log.events_gigatools = events_gigatools_count
		else:
			gigatools_log.events_gigatools = count
		gigatools_log.save()

		logger.info("GigatoolsParser:: Scrapping Ended for Artist Name: %s, Source: %s, url: %s" %
					(self.artist.artistName,
					 self.source_name, self.artist.artistSources[self.source_name]))

		return parsed_items


class GigatoolsProcessor(Processor):
	def __init__(self):
		Processor.__init__(self, SourceType.Gigatools)
		logger.info("Creating %s instance. Source: %s" %
					(self.__class__.__name__, self.source_name))

	def process(self, database, sourceID, artistID):
		try:  # open text file with bandsintown profiles
			db = database.con
			cursor = db.cursor(buffered=True)
			print "PR0CESS GIGATOOLS"
			sleep(2)
			try:
				query = "SELECT PK, eventID, eventName, venueID, venueName, date, startTime, city, country, artistID, sourceID FROM dj_parsed_data where sourceID = '%s' and artistID ='%s'" % (sourceID, artistID)
				cursor.execute(query)
			except Exception:
				print "No data from Gigatools for this artistID = '%s'"%(artistID)
				return
			bands = cursor.fetchall()
			query2 = "SELECT * FROM dj_Venues"
			cursor.execute(query2)
			venues = cursor.fetchall()
			query3 = "SELECT * FROM dj_Events"
			cursor.execute(query3)
			events = cursor.fetchall()
			comun = array("i")
			i = 0

			# (%s)
			# comun1=', '.join(map(lambda x: '%s',comun))
			# query=query % ('%s',comun1)

			for row in bands:

				pk = row[0]
				eventID = row[1]
				eventName = row[2]
				venueID = row[3]
				venueName = row[4]
				date = row[5]
				start = row[6]
				city = row[7]
				country = row[8]
				artistID = row[9]
				sourceID = row[10]
				if "\\" in eventName:
					eventName = eventName.replace("\\", "")
				if "\'" in eventName:
					eventName = eventName.replace("\'", "\\'")
				elif "'" in eventName:
					eventName = eventName.replace("'", "\'")
				if "\\" in venueID:
					venueID = venueID.replace("\\", "")
				if "\'" in venueID:
					venueID = venueID.replace("\'", "\\'")
				elif "'" in venueID:
					venueID = venueID.replace("'", "\'")
				print pk
				query = "Select venueID,venueGigatools,venueName from dj_Venues where venueGigatools like '" + str(
					venueID) + "'"
				cursor.execute(query)
				if cursor.rowcount > 0:

					result = cursor.fetchone()
					VID = result[0]
					vname = result[2]
					print VID, vname, eventID
					print "EXISTENT VENUE"
					if "\\" in vname:
						vname = vname.replace("\\", "")
					if "\'" in vname:
						vname = vname.replace("\'", "\\'")
					elif "'" in vname:
						vname = vname.replace("'", "\'")
					query = "Select gigatoolsID,eventID from dj_Events where gigatoolsID like '" + str(eventID) + "'"
					cursor.execute(query)
					if cursor.rowcount > 0:
						print "EXISTENT EVENT"
						comun.append(pk)
						EID = cursor.fetchone()[1]
						query = "Select * from dj_Master where artistID like '" + str(
							artistID) + "' and eventID in (Select eventID from dj_Events where date like '" + str(
							date) + "')"  # eventID like '"+str(EID)+"' and venueID like '"+str(VID)+"'"
						cursor.execute(query)
						if cursor.rowcount == 0:
							print "Insert to master"
							query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
							variable = (artistID, EID, VID)
							cursor.execute(query, variable)
							db.commit()
					else:  # the event is not in DB with this id and we try to match it with the existing one and find a possible match
						for row in events:
							eventName1 = row[1]
							venueEv = row[2]
							bandsintownID = row[3]
							residentID = row[4]
							gigatoolsID = row[5]
							date1 = row[6]
							start1 = row[7]
							end1 = row[8]
							if "\\" in eventName1:
								eventName1 = eventName1.replace("\\", "")
							if "\'" in eventName1:
								eventName1 = eventName1.replace("\'", "\\'")
							elif "'" in eventName1:
								eventName1 = eventName1.replace("'", "\'")
							if vname == venueEv:
								if date == date1:  # if match found update it with gigatools ID
									print eventName + " || " + eventName1 + " = EVENTS ON " + str(date) + "=" + str(
										date1)
									query = "Select eventID from dj_Events where gigatoolsID like '" + str(
										eventID) + "' "
									cursor.execute(query)
									if cursor.rowcount > 0:
										print "ALREADY"
										EID = cursor.fetchone()[0]
										comun.append(pk)
									else:
										print "UPDATE EVENTS"

										query = "Update dj_Events set gigatoolsID=%s,sourceGigatools=%s where eventName like '" + eventName1.encode(
											'utf-8') + "' and evenueName like '" + vname.encode(
											'utf-8') + "' and date like '" + str(date1) + "'"
										variable = (eventID, sourceID)
										cursor.execute(query, variable)
										db.commit()

										query = "Select eventID from dj_Events where gigatoolsID like '" + str(
											eventID) + "' and date like '" + str(date1) + "' GROUP BY eventName DESC "

										cursor.execute(query)

										pk = cursor.fetchone()
										EID = pk[0]

									query = "Select * from dj_Master where artistID like '" + str(
										artistID) + "' and eventID in (Select eventID from dj_Events where date like '" + str(
										date) + "')"  # and eventID like '"+str(EID)+"' and venueID like '"+str(VID)+"'"

									cursor.execute(query)
									if cursor.rowcount == 0:
										print "Insert to master"
										query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
										variable = (artistID, EID, VID)
										cursor.execute(query, variable)
										db.commit()
								else:  # check again to see if was inserted on last iteration in events matching and if exists go to next
									query = "Select gigatoolsID,eventID from dj_Events where gigatoolsID like '" + str(
										eventID) + "'"
									cursor.execute(query)
									if cursor.rowcount > 0:
										print "DUPLICATE EVENT"
										EID = cursor.fetchone()[1]
									else:  # the event is completely new and we insert it
										print "CREATING EVENT"
										query = "INSERT IGNORE INTO dj_Events(eventName,evenueName,gigatoolsID,date,startTime,sourceGigatools) VALUES (%s,%s,%s,%s,%s,%s) ON DUPLICATE KEY UPDATE eventName=eventName,evenueName=evenueName,gigatoolsID=gigatoolsID,date=date,startTime=startTime,sourceGigatools=sourceGigatools"
										variable = (eventName, vname, eventID, date, start, sourceID)
										cursor.execute(query, variable)
										db.commit()
										query = "Select eventID from dj_Events where gigatoolsID like '" + str(
											eventID) + "' and date like '" + str(date) + "'"

										cursor.execute(query)
										if cursor.rowcount > 0:
											EID = cursor.fetchone()[0]
									query = "Select * from dj_Master where artistID like '" + str(
										artistID) + "' and eventID in (Select eventID from dj_Events where date like '" + str(
										date) + "')"  # and eventID like '"+str(EID)+"' and venueID like '"+str(VID)+"'"

									cursor.execute(query)
									if cursor.rowcount == 0:
										print "Insert to master"
										query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
										variable = (artistID, EID, VID)
										cursor.execute(query, variable)
										db.commit()
										EID = 0
					VID = 0
				else:
					for row in venues:
						venueID1 = row[0]
						venueName1 = row[1]
						venueAddress1 = row[2]
						venueBands = row[5]
						venueResident = row[6]
						venueGigatools = row[7]
						city1 = row[8]
						country1 = row[9]

						VN = fuzz.token_set_ratio(venueName, venueName1)
						CT = fuzz.token_set_ratio(country, country1)
						# print "Ev1:%s"%event+"-Ev2:%s"%event1+"=%s"%ptr
						if VN >= 95:
							if CT >= 90:
								print venueName + " || " + venueName1 + "=" + str(VN) + " ARE THE SAME VENUES"
								address = venueAddress1
								region = country1
								venue = venueName1
								if "\\" in venue:
									venue = venue.replace("\\", "")
								if "\'" in venue:
									venue = venue.replace("\'", "\\'")
								elif "'" in venue:
									venue = venue.replace("'", "\'")
								city1 = city
								comun.append(pk)

								query = "Select venueID from dj_Venues where venueName like '" + venue + "' and country like '" + region + "' LOCK IN SHARE MODE"
								cursor.execute(query)
								if cursor.rowcount > 0:
									print "dont update"
									VID = cursor.fetchone()[0]
								else:
									print "Update"
									query = "Update dj_Venues set venueGigatools=%s,city=%s where venueName like '" + str(
										venue) + "' and country like '" + region + "'"
									variable = (venueID, city1)
									cursor.execute(query, variable)
									db.commit()
									query = "Select venueID from dj_Venues where venueName like '" + str(
										venue) + "' or venueName like '" + str(
										venueName) + "' and country like '" + region + "'"
									cursor.execute(query)
									VID = cursor.fetchone()[0]
								for row in events:
									eventName1 = row[1]
									venueEv = row[2]
									bandsintownID = row[3]
									residentID = row[4]
									gigatoolsID = row[5]
									date1 = row[6]
									start1 = row[7]
									end1 = row[8]
									if "\\" in eventName1:
										eventName1 = eventName1.replace("\\", "")
									if "\'" in eventName1:
										eventName1 = eventName1.replace("\'", "\\'")
									elif "'" in eventName1:
										eventName1 = eventName1.replace("'", "\'")
									if "\\" in eventName:
										eventName = eventName.replace("\\", "")
									if "\'" in eventName:
										eventName = eventName.replace("\'", "\\'")
									elif "'" in eventName:
										eventName = eventName.replace("'", "\'")
									if venue == venueEv:
										if date == date1:
											print eventName + " || " + eventName1 + " ARE THE SAME EVENTS ON " + str(
												date) + "=" + str(date1)

											query = "Select eventID from dj_Events where gigatoolsID like '" + str(
												eventID) + "' and eventName like '" + eventName1.encode(
												'utf-8') + "' or eventName like '" + eventName.encode(
												'utf-8') + "' and evenueName like '" + venue.encode(
												'utf-8') + "' and date like '" + str(date1) + "' LOCK IN SHARE MODE"
											cursor.execute(query)
											if cursor.rowcount > 0:
												print "ALREADY"
												EID = cursor.fetchone()[0]
											else:
												print "UPDATE EVENTS"
												query = "Update dj_Events set gigatoolsID=%s,sourceGigatools=%s where eventName like '" + eventName1.encode(
													'utf-8') + "' and evenueName like '" + venue.encode(
													'utf-8') + "' and date like '" + str(date1) + "'"
												variable = (eventID, sourceID)
												cursor.execute(query, variable)
												db.commit()
												query = "Select eventID from dj_Events where gigatoolsID like '" + str(
													eventID) + "' and date like '" + str(date1) + "'"

												cursor.execute(query)
												EID = cursor.fetchone()[0]
											query = "Select * from dj_Master where artistID like '" + str(
												artistID) + "' and eventID in (Select eventID from dj_Events where date like '" + str(
												date) + "')"  # and eventID like '"+str(EID)+"' and venueID like '"+str(VID)+"'"
											cursor.execute(query)
											if cursor.rowcount == 0:
												print "Insert to master"
												query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
												variable = (artistID, EID, VID)
												cursor.execute(query, variable)
												db.commit()

											EID = 0
								VID = 0
				i += 1
			myTuple = tuple(comun)
			try:
				query = "SELECT PK, eventID, eventName, venueID, venueName, date, startTime, city, country, artistID, sourceID FROM dj_parsed_data where sourceID ='%s' and artistID = '%s' and PK not in %s" % (
				sourceID, artistID, str(myTuple))
				print query
				cursor.execute(query)
			except Exception:
				print "No data from Gigatools for this artistID = '%s'"%(artistID)
				return
			unic = cursor.fetchall()
			for row in unic:
				pk = row[0]
				eventID = row[1]
				eventName = row[2]
				venueID = row[3]
				venueName = row[4]
				date = row[5]
				start = row[6]
				city = row[7]
				country = row[8]
				artistID = row[9]
				sourceID = row[10]
				if "\\" in venueName:
					venueName = venueName.replace("\\", "")
				if "\'" in venueName:
					venueName = venueName.replace("\'", "\\'")
				elif "'" in venueName:
					venueName = venueName.replace("'", "\'")
				if "\\" in venueID:
					venueID = venueID.replace("\\", "")
				if "\'" in venueID:
					venueID = venueID.replace("\'", "\\'")
				elif "'" in venueID:
					venueID = venueID.replace("'", "\'")
				if "\\" in eventName:
					eventName = eventName.replace("\'", "")
				if "\'" in eventName:
					eventName = eventName.replace("\'", "\\'")
				elif "'" in eventName:
					eventName = eventName.replace("'", "\'")
				query = "Select venueID from dj_Venues where venueGigatools like '" + str(
					venueID) + "' and country like '" + str(country) + "'"
				cursor.execute(query)
				if cursor.rowcount > 0:
					print "VENUE JUST INSERTED,DUPLICATE"
					VID = cursor.fetchone()[0]
				else:
					query = "INSERT INTO dj_Venues( venueName,venueGigatools,city,country) VALUES (%s,%s,%s,%s)"
					variable = (venueName, venueID, city, country)
					cursor.execute(query, variable)
					db.commit()
					print str(venueID)
					query = "Select venueID from dj_Venues where venueGigatools like '" + str(
						venueID) + "' and country like '" + str(country) + "'"
					cursor.execute(query)
					VID = cursor.fetchone()[0]
				query = "Select eventID from dj_Events where gigatoolsID like '" + str(
					eventID) + "' and date like '" + str(date) + "'"  # eventName like '"+eventName+"' and
				cursor.execute(query)
				if cursor.rowcount > 0:
					print "EVENT JUST INSERTED,DUPLICATE"
					EID = cursor.fetchone()[0]
				else:
					# current_date_time = datetime.now()
					query = "INSERT INTO dj_Events(eventName,evenueName,gigatoolsID,date,startTime,sourceGigatools) VALUES (%s,%s,%s,%s,%s,%s)"
					variable = (eventName, venueName, eventID, date, start, sourceID)
					cursor.execute(query, variable)
					db.commit()
					query = "Select eventID from dj_Events where gigatoolsID like '" + str(
						eventID) + "' and date like '" + str(date) + "'"
					cursor.execute(query)
					EID = cursor.fetchone()[0]
				query = "Select * from dj_Master where artistID like '" + str(
					artistID) + "' and eventID in (Select eventID from dj_Events where date like '" + str(
					date) + "')"  # and eventID like '"+str(EID)+"' and venueID like '"+str(VID)+"'"
				cursor.execute(query)
				if cursor.rowcount == 0:
					print "Insert to master"
					query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
					variable = (artistID, EID, VID)
					cursor.execute(query, variable)
					db.commit()

			cursor.close()
		except Exception, ex:
			logger.exception(ex);
