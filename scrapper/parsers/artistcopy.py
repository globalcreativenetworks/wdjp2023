#!/usr/bin/python
import logging
from scrapper.logger import LOG_NAME
logger = logging.getLogger(LOG_NAME)


class ArtistCopy(object):
    def __init__(self):
        self.artistID = 0
        self.artistName = ""
        self.artistWebsite = ""
        self.artistPhoto = ""
        self.artistAgent = ""
        self.artistBudget = 0
        self.artistSources = {}
    
    def print_info(self):
        logger.info("Artist Details:: ArtistID: %s, Artistname:%s, Sources: %s" %
                    (self.artistID, self.artistName, self.artistSources))
