#!/usr/bin/python
import logging
from scrapper.parsers import Parser, ParsedItem, Processor
from scrapper.parsers.source import SourceType
from scrapper.logger import LOG_NAME

from social_data.instagram import scrap_instagram_data, scrape_artist_photo

logger = logging.getLogger(LOG_NAME)


class InstagramParser (Parser):

    def __init__(self, artist):
        Parser.__init__(self, artist, SourceType.Instagram)
        logger.info("Creating %s instance. Artist Name: %s, Source: %s" %
                    (self.__class__.__name__, self.artist.artistName, self.source_name))

    def scrape(self):
        self.validate()
        scrap_instagram_data([self.artist.artistID])
        logger.info("Data scrapped for the ArtistName %s, Source: %s" %
                    (self.artist.artistName, self.source_name))
        return []

    def scrape_artist_photo(self):
        self.validate()
        scrape_artist_photo([self.artist.artistID])
        logger.info("Photos scrapped for the ArtistName %s, Source: %s" %
                    (self.artist.artistName, self.source_name))

class InstagramProcessor (Processor):
    def __init__(self):
        Processor.__init__(self, SourceType.Instagram)
        logger.info("Creating %s instance. Source: %s" %
                    (self.__class__.__name__, self.source_name))

    def process(self, database, sourceID, artistID):
        pass
