#!/usr/bin/python
import logging, re, requests
import scrapper.requests, urllib

from datetime import date, datetime

from array import array
from operator import concat

from artist.models import Artist
from bs4 import BeautifulSoup
from report.models import ReportResident
from report.utils import *
from scrapper.fuzzywuzzy import fuzz
from scrapper.fuzzywuzzy import process
from scrapper.parsers import Parser, ParsedItem, Processor
from scrapper.parsers.source import SourceType
from scrapper.logger import LOG_NAME
from time import sleep

from social_data.models import DataLog

import sys

logger = logging.getLogger(LOG_NAME)


class ResidentAdviserParser(Parser):
    source_base_url = "http://www.residentadvisor.net"

    today_date = date.today()

    def __init__(self, artist):
        Parser.__init__(self, artist, SourceType.ResidentAdviser)
        self.artDBObject = Artist.objects.get(artistID=self.artist.artistID)

        logger.info("Creating %s instance. Artist Name: %s, Source: %s" %
                    (self.__class__.__name__, self.artist.artistName, self.source_name))

    def scrap_photo(self):
        self.validate()
        source_url = self.artist.artistSources[self.source_name]['url']
        # source_url = "https://www.residentadvisor.net/dj/adambeyer"
        logger.info("ResidentAdviserParser:: Scrape artist photo initiated for Artist Name: %s, Source: %s, url: %s" %
                    (self.artist.artistName, self.source_name, source_url))
        source_request = requests.get(source_url)

        logger.info("ResidentAdviserParser:: Url: %s, Response content length: %s"
                    % (source_url, len(source_request.content)))
        source_soup = BeautifulSoup(source_request.content, "html.parser")

        header_links = source_soup.select('section#featureHead')
        for header_link in header_links:
            try:
                style = header_link.get('style')
                matches = re.findall(r"background-image:\s*url\(([^\)]+)\)", style)
                url = matches[0] if matches else ""
                if url:
                    image_url = Parser.prepare_artist_image_url(ResidentAdviserParser.source_base_url, url)
                    logger.info("ResidentAdviserParser:: Downloading image (Header) from %s." % image_url)
                    processed_data = self.process_image(image_url)
                    return processed_data
            except Exception as ex:
                logger.exception(ex)

        events_links = source_soup.select('article div > img')

        for event_link in events_links:
            image_url = event_link.get("src")
            image_url = Parser.prepare_artist_image_url(ResidentAdviserParser.source_base_url, image_url)
            try:
                logger.info("ResidentAdviserParser:: Downloading image (Section) from %s." % image_url)
                processed_data= self.process_image(image_url)
                return processed_data
            except Exception as ex:
                logger.exception(ex)
        return {}

    def scrape(self):
        try:
            self.validate()
        except Exception as e:
            parsed_items = []
            report = ReportResident(created=self.today_date,
                                   artist=self.artDBObject)
            report.title = "Artist: %s was not scrapped" % self.artist.artistName
            report.description = "Internal error occured during scrapping process: " + str(e.message)
            report.event = UNKNOWN
            report.resolution = CKECK_URL
            report.save()
            return parsed_items

        source_url = self.artist.artistSources[self.source_name]['url']
        source_url = "%s/%s" % (source_url.replace("http:","https:"), "dates")
        logger.info("ResidentAdviserParser:: Scrapping initiated for Artist Name: %s, Source: %s, url: %s" %
                    (self.artist.artistName,
                     self.source_name, source_url))
        
        proxy_host = "proxy.crawlera.com"
        proxy_port = "8010"
        proxy_auth = "058419ac48d7467096eecef504c81ecc:" # Make sure to include ':' at the end
        proxies = {"https": "https://{}@{}:{}/".format(proxy_auth, proxy_host, proxy_port), "http": "http://{}@{}:{}/".format(proxy_auth, proxy_host, proxy_port)}

        certificate = r"/usr/share/ca-certificates/crawlera-ca.crt"
        source_request = requests.get(source_url, proxies=proxies, verify=certificate)
        
        # print("""
        # Requesting [{}]
        # through proxy [{}]
        #
        # Request Headers:
        # {}
        #
        # Response Time: {}
        # Response Code: {}
        # Response Headers:
        # {}
        #
        # """.format(source_url, proxy_host, source_request.request.headers, source_request.elapsed.total_seconds(),
        #          source_request.status_code, source_request.headers, source_request.text))
        
        logger.info("ResidentAdviserParser:: Url: %s, Response content length: %s"
                    % (source_url, len(source_request.content)))

        source_soup = BeautifulSoup(source_request.content, "html.parser")
        # print(source_soup)
        parsed_items = []
        try:
            count = 0
            for ultag in source_soup.find_all('ul', {'id': 'items'}):
                for litag in ultag.find_all('li'):
                    date = litag.find_all('span')[0].text.split('T')[0]  # Event date to store in DB

                    link = litag.find_all('a')
                    sent_str = ''
                    for i in link:
                        sent_str += str(i)

                    search = "<a href=\"/events/(.*.)\" itemprop=\"url(.*.)</a>"
                    eventid = re.search(search, sent_str)
                    eventid = eventid.group(1).replace('?', '').split('"')[0]  # Event id to store in DB

                    search1 = "itemprop=\"url(.+?)<a href=\"/"
                    eventname = re.findall(search1, sent_str)
                    sent_str = ''
                    for i in eventname:
                        sent_str += str(i)
                    cap = '<a href="http://www.residentadvisor.net/events/' + eventid
                    cap2 = '<a target=\"_blank\" href="http://www.residentadvisor.net/events/' + eventid
                    rest = "\" itemprop=\"url" + sent_str
                    eventname = concat(cap2, rest)

                    eventName = sent_str.split('">')[1].split('</')[0]  # Event name to store without link in DB

                    link = cap.split('<a href="')[1]
                    event_page = requests.get(link, proxies=proxies, verify=certificate)
                    result = BeautifulSoup(event_page.content, "html.parser")
                    try:
                        venue = result.find("ul", class_="clearfix").find_all("li")[1]
                    except TypeError as ex:
                        logger.error("ResidentAdviserParser:: Scrapping Ended for Artist Name: %s, Source: %s, url: %s"
                                     " with the following error %s " %
                                    (self.artist.artistName,
                                     self.source_name, self.artist.artistSources[self.source_name], str(ex.message)))
                        return parsed_items
                    venue = str(venue)

                    if 'class="cat-rev"' in venue:
                        vname = venue.split('<div>Venue /</div>')[1].split("<br/>")[0].split('">')[1].split('</a>')[0]
                    else:
                        vname = venue.split('<div>Venue /</div>')[1].split("<br/>")[0]
                    if '<span class="title' in eventName:
                        eventName=vname

                    country = venue.split('</div>')[2].split('</a>')[0].strip()[3:]  # Country to store in DB

                    venueAddress = venue.split('<br/>')[1]  # Venue adress to store in DB
                    venue = venue.split('</div>')[1].split("<br/>")[0].replace("class=\"cat-rev\"",
                                                                               'target=\"_blank\"')  # Venue name to store in DB

                    venueid = ''
                    if " href=" in venue:
                        venueid = venue.split("id=")[1].split('"')[0]  # Venue ID to store n DB
                    try:
                        time = (
                        str(result.find("ul", class_="clearfix").find_all("li")[0]).split("<br/>")[2].split('</li>')[
                            0] or
                        str(result.find("ul", class_="clearfix").find_all("li")[0]).split("<br>")[2].split('</li>')[0])

                        if '-' in time:
                            start_time = time.split('-')[0][:-1]  # Start time to store in DB
                            end_time = time.split('-')[1][1:]
                        else:
                            start_time = time
                            end_time = ''
                    except:
                        time = (
                        str(result.find("ul", class_="clearfix").find_all("li")[0]).split("<br/>")[1].split('</li>')[
                            0] or
                        str(result.find("ul", class_="clearfix").find_all("li")[0]).split("<br>")[1].split('</li>')[0])

                        start_time = time.split('-')[0][:-1]  # Start time to store in DB
                        try:
                            end_time = time.split('-')[1][1:]
                        except:
                            end_time = ''
                    parsed_item = ParsedItem()
                    parsed_item.artistID = self.artist.artistID
                    parsed_item.sourceID = self.artist.artistSources[self.source_name]['id'].decode('utf-8')
                    parsed_item.date = date.decode('utf-8')
                    parsed_item.eventID = eventid.decode('utf-8')
                    parsed_item.eventName = eventName.decode('utf-8')

                    parsed_item.venueName = vname.decode('utf-8')
                    parsed_item.startTime = start_time.decode('utf-8')
                    parsed_item.country = country.decode('utf-8')
                    parsed_item.venueAddress = venueAddress.decode('utf-8')
                    parsed_item.venueID = venueid.decode('utf-8')
                    parsed_item.endTime = end_time.decode('utf-8')
                    parsed_item.print_info(extra_info="%s-%s" % (self.artist.artistName, self.source_name))
                    parsed_items.append(parsed_item)

                    count = count + 1

            resident_advisor_log, created = DataLog.objects.get_or_create(created=ResidentAdviserParser.today_date)
            if resident_advisor_log.events_resident_advisor:
                events_resident_advisor = int(resident_advisor_log.events_resident_advisor)
                events_resident_advisor += count
                resident_advisor_log.events_resident_advisor = events_resident_advisor
            else:
                resident_advisor_log.events_resident_advisor = count
            resident_advisor_log.save()

            report = ReportResident(created=self.today_date,
                                   artist=self.artDBObject)
            report.title = "Artist: %s was scrapped" % self.artist.artistName
            report.description = "Succesful scrape Events : " + str(count)
            report.event = INFO
            report.resolution = NA
            report.save()
            logger.info("ResidentAdviserParser:: Scrapping Ended for Artist Name: %s, Source: %s, url: %s" %
                        (self.artist.artistName,
                         self.source_name, self.artist.artistSources[self.source_name]))
        except Exception as ex:
            report = ReportResident(created=self.today_date,
                                   artist=self.artDBObject)
            report.title = "Artist: %s was not scrapped" % self.artist.artistName
            report.description = "Internal error occured during scrapping process: " + str(ex.message)
            report.event = ERROR
            report.resolution = CKECK_URL
            report.save()
            logger.exception(ex)
        return parsed_items


class ResidentAdviserProcessor(Processor):

    def __init__(self):
        Processor.__init__(self, SourceType.ResidentAdviser)
        logger.info("Creating %s instance. Source: %s" %
                    (self.__class__.__name__, self.source_name))

    def process(self, database, sourceID, artistID):
        try:  # open text file with bandsintown profiles
            db = database.con
            cursor = db.cursor(buffered=True)
            print ("PR0CESS RESIDENTADVISOR")
            sleep(2)
            try:
                query = "SELECT PK, eventID, eventName, venueID, venueName, venueAddress, date, startTime, endTime, country, artistID, sourceID FROM dj_parsed_data where sourceID = '%s' and artistID='%s'" % (sourceID, artistID)
                cursor.execute(query)
            except Exception:
                print ("No data from ResidentAdvisor for this artistID = '%s'"%(artistID))
                return
            resident = cursor.fetchall()
            print (resident)
            query2 = "SELECT * FROM dj_Venues"
            cursor.execute(query2)
            venues = cursor.fetchall()
            query3 = "SELECT * FROM dj_Events"
            cursor.execute(query3)
            events = cursor.fetchall()
            comun = array("i")
            i = 0

            # (%s)
            # comun1=', '.join(map(lambda x: '%s',comun))
            # query=query % ('%s',comun1)

            for row in resident:

                pk = row[0]
                eventID = row[1]
                eventName = row[2]
                venueID = row[3]
                venueName = row[4]
                venueAddress = row[5]
                date = row[6]
                start = row[7]
                end = row[8]
                country = row[9]
                artistID = row[10]
                sourceID = row[11]
                if "\\" in venueName:
                    venueName = venueName.replace("\\", "")
                if "\\" in eventName:
                    eventName = eventName.replace("\\", "")
                if "\'" in venueName:
                    venueName = venueName.replace("\'", "\\'")
                elif "'" in venueName:
                    venueName = venueName.replace("'", "\'")
                if "\'" in eventName:
                    eventName = eventName.replace("\'", "\\'")
                elif "'" in eventName:
                    eventName = eventName.replace("'", "\'")
                if not venueID:
                    query = "Select venueID,venueResident,venueName from dj_Venues where venueName like '" + venueName.encode(
                        'utf-8') + "' and country like '" + country + "'"
                    cursor.execute(query)
                else:
                    query = "Select venueID,venueResident,venueName from dj_Venues where venueResident like '" + str(
                        venueID) + "'"
                    cursor.execute(query)
                result = cursor.fetchone()
                if cursor.rowcount != 0:
                    VID = result[0]
                    vname = result[2]
                    print (VID, vname, eventID)

                    print ("EXISTENT VENUE")
                    if "\\" in vname:
                        vname = vname.replace("\\", "")
                    if "\'" in vname:
                        vname = vname.replace("\'", "\\'")
                    elif "'" in vname:
                        vname = vname.replace("'", "\'")

                    query = "Select residentID,eventID from dj_Events where residentID like '" + str(eventID) + "'"
                    cursor.execute(query)
                    if cursor.rowcount > 0:
                        print ("EXISTENT EVENT")
                        comun.append(pk)
                        EID = cursor.fetchone()[1]
                    else:  # the event is completely new and we insert it
                        print ("CREATE NEW ENTRY")
                        if "\\" in eventName:
                            eventName = eventName.replace("\\", "")
                        if "\'" in eventName:
                            eventName = eventName.replace("\'", "\\'")
                        elif "'" in eventName:
                            eventName = eventName.replace("'", "\'")

                        # current_date_time = datetime.now()
                        # print "current_date_time +++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
                        # print current_date_time

                        query = "INSERT INTO dj_Events(eventName,evenueName,residentID,date,startTime,endTime,sourceResident) VALUES (%s,%s,%s,%s,%s,%s,%s)"
                        variable = (eventName, vname, eventID, date, start, end, sourceID)
                        cursor.execute(query, variable)
                        db.commit()

                        query = "Select eventID from dj_Events where residentID like '" + str(
                            eventID) + "' and date like '" + str(date) + "'"
                        comun.append(pk)
                        cursor.execute(query)
                        EID = cursor.fetchone()[0]
                    query = "Select * from dj_Master where artistID like '" + str(
                        artistID) + "' and eventID like '" + str(EID) + "' and venueID like '" + str(VID) + "'"
                    cursor.execute(query)
                    if cursor.rowcount == 0:
                        print ("Insert to master")
                        query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
                        variable = (artistID, EID, VID)
                        cursor.execute(query, variable)
                        db.commit()
                else:
                    for row in venues:
                        venueID1 = row[0]
                        venueName1 = row[1]
                        venueAddress1 = row[2]
                        venueBands = row[5]
                        venueResident = row[6]
                        venueGigatools = row[7]
                        city1 = row[8]
                        country1 = row[9]

                        VN = fuzz.token_set_ratio(venueName, venueName1)
                        CT = fuzz.token_set_ratio(country, country1)
                        if venueAddress == '' or venueAddress1 == '':
                            string = 1
                        else:
                            AD = fuzz.token_set_ratio(venueAddress, venueAddress1)
                            string = 2
                        # print "Ev1:%s"%event+"-Ev2:%s"%event1+"=%s"%ptr
                        if VN >= 95:
                            if string == 1:
                                if CT >= 90:
                                    print (venueName + " || " + venueName1 + "=" + str(VN) + " ARE THE SAME VENUES")
                                    address = venueAddress1
                                    region = country1
                                    venue = venueName1
                                    if "\\" in venue:
                                        venue = venue.replace("\\", "")
                                    if "\'" in venue:
                                        venue = venue.replace("\'", "\\'")
                                    elif "'" in venue:
                                        venue = venue.replace("'", "\'")
                                    if venueResident == 0:
                                        query = "Select venueID from dj_Venues where venueName like '" + venueName.encode(
                                            'utf-8') + "' or venueName like '" + venue.encode(
                                            'utf-8') + "'  and country like '" + region + "'"
                                        cursor.execute(query)
                                    else:
                                        query = "Select venueID from dj_Venues where venueResident like '" + str(
                                            venueResident) + "' and country like '" + region + "'"
                                        cursor.execute(query)
                                    comun.append(pk)
                                    if cursor.rowcount > 0:
                                        print ("DONT update")
                                        VID = cursor.fetchone()[0]
                                    else:
                                        print ("Update")
                                        query = "Update dj_Venues set venueResident=%s where venueName like '" + venueName.encode(
                                            'utf-8') + "' or venueName like '" + venue.encode(
                                            'utf-8') + "' and country like '" + region + "'"
                                        variable = (venueID)
                                        cursor.execute(query, variable)
                                        db.commit()
                                        query = "Select venueID from dj_Venues where venueName like '" + venueName.encode(
                                            'utf-8') + "' or venueName like '" + venue.encode(
                                            'utf-8') + "' and country like '" + region + "'"
                                        cursor.execute(query)
                                        VID = cursor.fetchone()[0]
                                    for row in events:
                                        eventName1 = row[1]
                                        venueEv = row[2]
                                        bandsintownID = row[3]
                                        residentID = row[4]
                                        gigatoolsID = row[5]
                                        date1 = row[6]
                                        start1 = row[7]
                                        end1 = row[8]
                                        if "\\" in eventName1:
                                            eventName1 = eventName1.replace("\\", "")
                                        if "\'" in eventName1:
                                            eventName1 = eventName1.replace("\'", "\\'")
                                        elif "'" in eventName1:
                                            eventName1 = eventName1.replace("'", "\'")
                                        if venue == venueEv:
                                            if date == date1:
                                                print (eventName + " || " + eventName1 + " ARE THE SAME EVENTS ON " + str(
                                                    date) + "=" + str(date1))
                                                query = "Select eventID from dj_Events where residentID like '" + str(
                                                    residentID) + "'  and date like '" + str(date1) + "'"
                                                cursor.execute(query)
                                                if cursor.rowcount > 0:
                                                    print ("Events exist")
                                                    EID = cursor.fetchone()[0]
                                                else:
                                                    print ("UPDATE EVENTS")
                                                    query = "Update dj_Events set residentID=%s,sourceResident=%s where eventName like '" + eventName1 + "' and evenueName like '" + venue.encode(
                                                        'utf-8') + "' and date like '" + str(date1) + "'"
                                                    variable = (eventID, sourceID)
                                                    cursor.execute(query, variable)
                                                    db.commit()
                                                    query = "Select eventID from dj_Events where residentID like '" + str(
                                                        eventID) + "'  and date like '" + str(date1) + "'"
                                                    cursor.execute(query)
                                                    EID = cursor.fetchone()[0]
                                                query = "Select * from dj_Master where artistID like '" + str(
                                                    artistID) + "' and eventID like '" + str(
                                                    EID) + "' and venueID like '" + str(VID) + "'"
                                                cursor.execute(query)
                                                if cursor.rowcount == 0:
                                                    print ("Insert to master")
                                                    query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
                                                    variable = (artistID, EID, VID)
                                                    cursor.execute(query, variable)
                                                    db.commit()
                            elif string == 2:
                                if CT >= 90 and AD > 70:
                                    print (venueName + " || " + venueName1 + "=" + str(
                                        VN) + " ARE THE SAME VENUES AFTER ADDRESS")
                                    address = venueAddress1
                                    region = country1
                                    venue = venueName1
                                    if "\\" in venue:
                                        venue = venue.replace("\\", "")
                                    if "\'" in venue:
                                        venue = venue.replace("\'", "\\'")
                                    elif "'" in venue:
                                        venue = venue.replace("'", "\'")
                                    '''if "\\" in venueName:
                                        venueName=venueName.replace("\\","")
                                    if "\'" in venueName:
                                        venueName=venueName.replace("\'","\\'")
                                    elif "'" in venueName:
                                        venueName=venueName.replace("'","\'")'''
                                    if venueResident == 0:
                                        query = "Select venueID from dj_Venues where venueName like '" + venueName.encode(
                                            'utf-8') + "' or venueName like '" + venue.encode(
                                            'utf-8') + "'  and country like '" + region + "'"
                                        cursor.execute(query)
                                    else:
                                        query = "Select venueID from dj_Venues where venueResident like '" + str(
                                            venueResident) + "' and country like '" + region + "'"
                                        cursor.execute(query)
                                    comun.append(pk)
                                    if cursor.rowcount > 0:
                                        print ("DONT update")
                                        VID = cursor.fetchone()[0]
                                    else:
                                        print ("Update")
                                        query = "Update dj_Venues set venueResident ='" + venueID + "' where venueName like '" + venueName.encode(
                                            'utf-8') + "' or venueName like '" + venue.encode(
                                            'utf-8') + "' and country like '" + region + "'"
                                        variable = (venueID)
                                        cursor.execute(query)
                                        db.commit()
                                        query = "Select venueID from dj_Venues where venueResident like '" + str(
                                            venueID) + "' and venueName like '" + venueName.encode(
                                            'utf-8') + "' or venueName like '" + venue.encode(
                                            'utf-8') + "' and country like '" + str(region) + "'"
                                        cursor.execute(query)
                                        VID = cursor.fetchone()[0]
                                    for row in events:
                                        eventName1 = row[1]
                                        venueEv = row[2]
                                        bandsintownID = row[3]
                                        residentID = row[4]
                                        gigatoolsID = row[5]
                                        date1 = row[6]
                                        start1 = row[7]
                                        end1 = row[8]
                                        if "\\" in eventName1:
                                            eventName1 = eventName1.replace("\\", "")
                                        if "\'" in eventName1:
                                            eventName1 = eventName1.replace("\'", "\\'")
                                        elif "'" in eventName1:
                                            eventName1 = eventName1.replace("'", "\'")
                                        if venue == venueEv:
                                            if date == date1:
                                                print (eventName + " || " + eventName1 + " ARE THE SAME EVENTS ON " + str(
                                                    date) + "=" + str(date1))
                                                query = "Select eventID from dj_Events where residentID like '" + str(
                                                    residentID) + "'  and date like '" + str(date1) + "'"
                                                cursor.execute(query)
                                                if cursor.rowcount > 0:
                                                    print ("Events exist")
                                                    EID = cursor.fetchone()[0]
                                                else:
                                                    print ("UPDATE EVENTS")
                                                    query = "Update dj_Events set residentID=%s,sourceResident=%s where eventName like '" + eventName1 + "' and evenueName like '" + venue.encode(
                                                        'utf-8') + "' and date like '" + str(date1) + "'"
                                                    variable = (eventID, sourceID)
                                                    cursor.execute(query, variable)
                                                    db.commit()
                                                    query = "Select eventID from dj_Events where eventName like '" + str(
                                                        eventID) + "'  and date like '" + str(date1) + "'"
                                                    cursor.execute(query)
                                                    EID = cursor.fetchone()[0]
                                                query = "Select * from dj_Master where artistID like '" + str(
                                                    artistID) + "' and eventID like '" + str(
                                                    EID) + "' and venueID like '" + str(VID) + "'"
                                                cursor.execute(query)
                                                if cursor.rowcount == 0:
                                                    print ("Insert to master")
                                                    query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
                                                    variable = (artistID, EID, VID)
                                                    cursor.execute(query, variable)
                                                    db.commit()

                i += 1
            myTuple = tuple(comun)
            try:
                query = "SELECT PK, eventID, eventName, venueID, venueName, venueAddress, date, startTime, endTime, country, artistID, sourceID FROM dj_parsed_data where sourceID = '%s' and artistID = '%s' and PK not in %s" % (sourceID, artistID, str(myTuple))
                print (query)
                cursor.execute(query)
            except Exception:
                print ("No data from ResidentAdviser for this artistID = '%s'"%artistID)
                return
            unic = cursor.fetchall()
            for row in unic:
                pk = row[0]
                eventID = row[1]
                eventName = row[2]
                venueID = row[3]
                venueName = row[4]
                venueAddress = row[5]
                date = row[6]
                start = row[7]
                end = row[8]
                country = row[9]
                artistID = row[10]
                sourceID = row[11]
                print (str(venueName), str(eventName), str(country))
                if "\\" in venueName:
                    venueName = venueName.replace("\\", "")
                '''if "\'" in venueName:
                    venueName=venueName.replace("\'","\\'")'''
                if "'" in venueName:
                    venueName = venueName.replace("'", "\'")
                if "\\" in eventName:
                    eventName = eventName.replace("\\", "")
                '''if "\'" in eventName:
                    eventName=eventName.replace("\'","\\'")'''
                if "'" in eventName:
                    eventName = eventName.replace("'", "\'")
                if venueID == 0:
                    query = "Select venueID from dj_Venues where venueName like '" + venueName.encode(
                        'utf-8') + "' and country like '" + str(country) + "'"
                    cursor.execute(query)
                else:
                    query = "Select venueID from dj_Venues where venueResident like '" + str(
                        venueID) + "' and country like '" + str(country) + "'"
                    cursor.execute(query)
                if cursor.rowcount > 0:
                    print("VENUE JUST INSERTED,DUPLICATE")
                    VID = cursor.fetchone()[0]
                else:
                    print ("REPLACE")
                    query = "REPLACE INTO dj_Venues(venueName,venueAddress,venueResident,country) VALUES (%s,%s,%s,%s)"
                    variable = (venueName, venueAddress, venueID, country)
                    cursor.execute(query, variable)
                    db.commit()
                    print (str(venueName), str(eventName), str(country))
                    if "\\" in venueName:
                        venueName = venueName.replace("\\", "")
                    if "\'" in venueName:
                        venueName = venueName.replace("\'", "\\'")
                    '''if "'" in venueName:
                        venueName=venueName.replace("'","\'")'''
                    query = "Select venueID from dj_Venues where  venueName like '" + venueName.encode(
                        'utf-8') + "' and country like '" + str(country) + "'"
                    # venueResident like '"+str(venueID)+"' and
                    cursor.execute(query)
                    VID = cursor.fetchone()[0]
                query = "Select eventID from dj_Events where residentID like '" + str(
                    eventID) + "' and date like '" + str(date) + "'"
                cursor.execute(query)
                if cursor.rowcount > 0:
                    print ("EVENT JUST INSERTED,DUPLICATE")
                    EID = cursor.fetchone()[0]
                else:
                    current_date_time = datetime.now()
                    query = "INSERT INTO dj_Events(eventName,evenueName,residentID,date,startTime,endTime,sourceResident,created) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)"
                    variable = (eventName, venueName, eventID, date, start, end, sourceID, current_date_time)
                    cursor.execute(query, variable)
                    db.commit()
                    if "\\" in eventName:
                        eventName = eventName.replace("\\", "")

                    if "'" in eventName:
                        eventName = eventName.replace("'", "\'")
                    query = "Select eventID from dj_Events where residentID like '" + str(
                        eventID) + "' and date like '" + str(date) + "'"
                    cursor.execute(query)
                    EID = cursor.fetchone()[0]
                query = "Select * from dj_Master where artistID like '" + str(artistID) + "' and eventID like '" + str(
                    EID) + "' and venueID like '" + str(VID) + "'"
                cursor.execute(query)
                if cursor.rowcount == 0:
                    print ("Insert to master")
                    query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
                    variable = (artistID, EID, VID)
                    cursor.execute(query, variable)
                    db.commit()

            cursor.close()

        except Exception as ex:
            logger.exception(ex);
