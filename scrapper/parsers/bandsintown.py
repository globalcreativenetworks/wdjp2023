import logging, requests
import scrapper.requests, urllib
from django.conf import settings

from datetime import date, datetime, timedelta

from scrapper.fuzzywuzzy import fuzz
from scrapper.fuzzywuzzy import process
from array import array
import difflib
from time import sleep
from bs4 import BeautifulSoup
import geocoder
from scrapper.parsers import Parser, ParsedItem, Processor
from scrapper.parsers.source import SourceType
from scrapper.logger import LOG_NAME

from social_data.models import DataLog
from report.models import ReportBandsintown
from report.utils import *

from artist.models import Artist, Source

import json
import re

logger = logging.getLogger(LOG_NAME)

today = date.today()
month_3 = today - timedelta(days=31 * 3)


class BandsintownParser(Parser):
    source_base_url = "https://www.bandsintown.com"
    today_date = date.today()

    def __init__(self, artist):
        Parser.__init__(self, artist, SourceType.Bandsintown)
        logger.info("Creating %s instance. Artist Name: %s, Source: %s" %
                    (self.__class__.__name__, self.artist.artistName, self.source_name))

    def scrap_photo(self):
        self.validate()
        source_url = self.artist.artistSources[self.source_name]['url']
        # source_url = "http://www.bandsintown.com/Astrix"
        logger.info("BandsintownParser:: Scrape artist photo initiated for Artist Name: %s, Source: %s, url: %s" %
                    (self.artist.artistName, self.source_name, source_url))
        source_request = requests.get(source_url)

        logger.info("BandsintownParser:: Url: %s, Response content length: %s"
                    % (source_url, len(source_request.content)))
        source_soup = BeautifulSoup(source_request.content, "html.parser")

        events_links = source_soup.select('#sidebar a > img.sidebar-image')

        parsed_items = []
        for event_link in events_links:
            image_url = event_link.get("src")
            image_url = Parser.prepare_artist_image_url(BandsintownParser.source_base_url, image_url)
            try:
                logger.info("BandsintownParser:: Downloading image from %s" % (image_url))
                processed_data = self.process_image(image_url)
                return processed_data
            except Exception, ex:
                logger.exception(ex)
        return {}

    def scrape_similar_artist(self):
        self.validate()
        source_url = self.artist.artistSources[self.source_name]['url']

        logger.info("BandsintownParser:: Scrape Similar artist initiated for Artist Name: %s, Source: %s, url: %s" %
                    (self.artist.artistName,
                     self.source_name, source_url))

        source_request = requests.get(source_url)

        logger.info("BandsintownParser:: Url: %s, Response content length: %s"
                    % (source_url, len(source_request.content)))

        source_soup = BeautifulSoup(source_request.content, "html.parser")
        events_links = source_soup.select('.content-container div > a')

        parsed_items = []
        for event_link in events_links:
            artist_name = event_link.text.decode("utf-8", "ignore")
            if artist_name:
                parsed_items.append(artist_name)

        return parsed_items

    def scrape(self):
            self.validate()
            # Sleep calls are used to allow IO buffer to write on console/file logging prints
            sleep(0.009)
            source_url = self.artist.artistSources[self.source_name]['url']

            logger.info("BandsintownParser:: Scrapping initiated for Artist Name: %s, Source: %s, url: %s" %
                        (self.artist.artistName,
                         self.source_name, source_url))

            parsed_items = []
            count = 0
            artist = Artist.objects.get(pk=self.artist.artistID)
            source = Source.objects.get(pk=self.artist.artistSources[self.source_name]['id'])
            ReportBandsintown.objects.filter(artist=artist).filter(created__lte=month_3).delete()

            # Access artist url
            try:
                source_request = requests.get(source_url)
            except requests.exceptions.ConnectionError:
                report = ReportBandsintown(created=BandsintownParser.today_date,
                                           source=source,
                                           artist=artist)
                report.title = "Artist: %s was not scrapped" % self.artist.artistName
                report.description = "Url: %s cannot be accessed" % source_url
                report.event = ERROR
                report.resolution = CKECK_URL
                report.save()
                return parsed_items
            # Check if page could be accessed and have a positive response
            if source_request.status_code == 200:
                logger.info("BandsintownParser:: Url: %s, Response content length: %s"
                            % (source_url, len(source_request.content)))

                source_soup = BeautifulSoup(source_request.content, "html.parser")
                try:
                    # check for json which has the events information and get it as a string
                    json_content_available = source_soup.select('#main + script + script')[0].text
                except IndexError:
                    # If not Upcomming Events section log this as a warning
                    logger.warning("BandsintownParser:: Artist Url: %s doesn't match source code parser"
                                   % source_url)
                    report = ReportBandsintown(created=BandsintownParser.today_date,
                                               source=source,
                                               artist=artist)
                    report.title = "Artist: %s was not scrapped" % self.artist.artistName
                    report.description = "Url: %s doesn't match source code parser" % source_url
                    report.event = ERROR
                    report.resolution = CKECK_URL
                    report.save()
                    logger.info("BandsintownParser:: Scrapping Ended for Artist Name: %s, Source: %s, url: %s" %
                            (self.artist.artistName,
                             self.source_name, self.artist.artistSources[self.source_name]))
                    return parsed_items
                # check for non-empty string (upcoming events are avai;lable)
                if json_content_available != '[]':
                    # parse string as json object array
                    json_parsed = json.loads(json_content_available)
                    # iterate over json object array
                    for item in json_parsed:
                        # check for element type to be event
                        if item['@type'] == 'MusicEvent':
                            # we have event so create report instance about it
                            report = ReportBandsintown(created=BandsintownParser.today_date,
                                                       source=source,
                                                       artist=artist)
                            # initiate parsed item obj
                            parsed_item = ParsedItem()
                            # assign artist id
                            parsed_item.artistID = self.artist.artistID
                            # assign source id
                            parsed_item.sourceID = self.artist.artistSources[self.source_name]['id']
                            # get the event name by processing ['description'] parameter
                            if item['description']:
                                description_str = item['description']
                                # search for date pattern in string to escape it
                                match = re.search(r'(\d+-\d+-\d+)', description_str)
                                # check if date match
                                if match.group(1):
                                    # eliminate date from string and assign the rest to event name
                                    parsed_item.eventName = description_str.split(match.group(1))[0].strip()
                            # get event ID by processing the ['url'] parameter
                            if item['url']:
                                parsed_item.eventID = item['url'].split('/')[4].split('-')[0]
                            # get start date by processing the ['startDate'] parameter
                            if item['startDate']:
                                # check if start time is mentioned
                                if "T" in item['startDate']:
                                    # get date and start time
                                    date_str, time_str = item['startDate'].split("T")
                                else:
                                    # get only date
                                    date_str = item['startDate']
                                    time_str = ''
                                # convert to datetime variable and date
                                parsed_item.date = datetime.strptime(date_str, '%Y-%m-%d').strftime('%Y-%m-%d')
                                # if we have start time
                                if time_str != '':
                                    # convert to datetime variable and assign
                                    parsed_item.startTime = datetime.strptime(time_str, '%H:%M:%S').strftime('%H:%M')
                            # get the venue information if exists
                            if item['location'] and item['location']['@type'] == 'Place':
                                # Get venue name
                                if item['location']['name']:
                                    parsed_item.venueName = item['location']['name']
                                    parsed_item.venueID = ''
                                # get venue address
                                if item['location']['address']:
                                    parsed_item.venueAddress = item['location']['address']
                                    try:
                                        # get country
                                        parsed_item.country = parsed_item.venueAddress.rsplit(',', 1)[1]
                                        # get city
                                        parsed_item.city = parsed_item.venueAddress.rsplit(',', 2)[1].split(',')[0]
                                    except IndexError:
                                        # nothing to split
                                        parsed_item.country = parsed_item.venueAddress
                                        parsed_item.city = ''
                                    # Get city information if address is long
                                    if parsed_item.city == parsed_item.country:
                                        parsed_item.city = parsed_item.venueAddress.split(',')[0]
                                # check for geo coordinates if available
                                if item['location']['geo'] and item['location']['geo']['@type'] == 'GeoCoordinates':
                                    # get latitude
                                    if item['location']['geo']['latitude']:
                                        parsed_item.latitude = item['location']['geo']['latitude']
                                    # get longitude
                                    if item['location']['geo']['latitude']:
                                        parsed_item.longitude = item['location']['geo']['longitude']
                                    # if latitude and longitude provided is zero
                                    if parsed_item.latitude == 0 and parsed_item.longitude == 0:
                                        # Make the address string to be geocoded
                                        try:
                                            address2geocode = parsed_item.venueName + parsed_item.venueAddress
                                            # Geocode
                                            g = geocoder.google(address2geocode, method='places',
                                                                key=settings.GOOGLE_PLACES_GEOCODE_KEY)
                                            parsed_item.latitude, parsed_item.longitude = g.latlng
                                            # Use only address and not venue if before returned 0
                                            if parsed_item.latitude == 0 and parsed_item.longitude == 0:
                                                address2geocode = parsed_item.venueAddress
                                                g = geocoder.google(address2geocode, method='places',
                                                                    key=settings.GOOGLE_PLACES_GEOCODE_KEY)
                                                parsed_item.latitude, parsed_item.longitude = g.latlng
                                        # Also if first try raise exception on geocoding results
                                        except TypeError:
                                            # Use only address and not venue
                                            try:
                                                address2geocode = parsed_item.venueAddress
                                                g = geocoder.google(address2geocode, method='places',
                                                                    key=settings.GOOGLE_PLACES_GEOCODE_KEY)
                                                parsed_item.latitude, parsed_item.longitude = g.latlng
                                            except (IndexError, TypeError):
                                                # Geocoding can't be performed
                                                warning_report = ReportBandsintown(created=BandsintownParser.today_date,
                                                                                   source=source,
                                                                                   artist=artist)
                                                warning_report.title = "Geocoding not performed"
                                                warning_report.description = "Address: %s was not able to geocode" % address2geocode
                                                warning_report.event = WARNING
                                                warning_report.resolution = NA
                                                warning_report.save()

                            parsed_item.print_info(extra_info="%s-%s" % (self.artist.artistName, self.source_name))
                            parsed_items.append(parsed_item)
                            report.title = "Event %s of Artist: %s was successfully scrapped" % (
                                parsed_item.eventName[:25], self.artist.artistName)
                            report.description = "Data was successfully retrieved from Url: %s" % source_url
                            report.event = INFO
                            report.resolution = NA
                            report.save()
                            count += 1
                        #
                        # except Exception as ex:
                        #     logger.exception(ex)

                    bandsin_town_log, created = DataLog.objects.get_or_create(created=BandsintownParser.today_date)

                    if bandsin_town_log.events_bandsin_town:
                        events_bandsin_town_count = int(bandsin_town_log.events_bandsin_town)
                        events_bandsin_town_count += count
                        bandsin_town_log.events_bandsin_town = events_bandsin_town_count
                    else:
                        bandsin_town_log.events_bandsin_town = count
                    bandsin_town_log.save()
                else:
                    report = ReportBandsintown(created=BandsintownParser.today_date,
                                               source=source,
                                               artist=artist)
                    sleep(0.009)
                    # If not Upcomming Events section log this as a warning
                    logger.warning("BandsintownParser:: Artist Url: %s doesn't have Upcoming Events"
                                   % source_url)
                    report.title = "Artist: %s was not scrapped" % self.artist.artistName
                    report.description = "Url: %s doesn't have Upcoming Events" % source_url
                    report.event = INFO
                    report.resolution = NA
                    report.save()
                logger.info("BandsintownParser:: Scrapping Ended for Artist Name: %s, Source: %s, url: %s" %
                            (self.artist.artistName,
                             self.source_name, self.artist.artistSources[self.source_name]))

            elif source_request.status_code == 404:
                report = ReportBandsintown(created=BandsintownParser.today_date,
                                           source=source,
                                           artist=artist)
                sleep(0.009)
                # If 404 log this as a warning
                logger.warning("BandsintownParser:: Artist Url: %s returned %s code"
                               % (source_url, source_request.status_code))
                report.title = "Artist: %s was not scrapped" % self.artist.artistName
                report.description = "Url: %s returned the following '404 - Page not found' error code" % source_url
                report.event = ERROR
                report.resolution = CKECK_URL + INEXISTENT_USER
                report.save()
                logger.info("BandsintownParser:: Scrapping Ended for Artist Name: %s, Source: %s, url: %s" %
                            (self.artist.artistName,
                             self.source_name, self.artist.artistSources[self.source_name]))
            else:
                report = ReportBandsintown(created=BandsintownParser.today_date,
                                           source=source,
                                           artist=artist)
                sleep(0.009)
                # If other code log this as an error
                logger.error("BandsintownParser:: Artist Url: %s returned %s"
                             % (source_url, source_request.status_code))
                report.title = "Artist: %s was not scrapped" % self.artist.artistName
                report.description = "Url: %s returned the following %s error code" % (
                    source_url, source_request.status_code)
                report.event = UNKNOWN
                report.resolution = CKECK_URL + NO_ACCESS + DEBUG
                report.save()
                logger.info("BandsintownParser:: Scrapping Ended for Artist Name: %s, Source: %s, url: %s" %
                            (self.artist.artistName,
                             self.source_name, self.artist.artistSources[self.source_name]))

            return parsed_items


class BandsintownProcessor(Processor):
    today_date = date.today()

    def __init__(self):
        Processor.__init__(self, SourceType.Bandsintown)
        logger.info("Creating %s instance. Source: %s" %
                    (self.__class__.__name__, self.source_name))

    def process(self, database, sourceID, artistID):
        print ("heelo i am in process ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        try:  # open text file with bandsintown profiles

            db = database.con
            cursor = db.cursor(buffered=True)
            print ("PR0CESS BANDSINTOWN")
            sleep(2)
            try:
                query = "SELECT PK, eventID, eventName, venueID, venueName, venueAddress, date, startTime, city, country, artistID, sourceID, latitude, longitude FROM dj_parsed_data where sourceID = '%s' and artistID = '%s'" % (
                sourceID,
                artistID)  # where eventID not in (Select bandsintownID from dj_Events where bandsintownID > 0) "
                cursor.execute(query)
            except Exception:
                print ("No data from Bandsintown for this artistID = '%s'" % (artistID))
                return
            bands = cursor.fetchall()
            query2 = "SELECT * FROM dj_Venues"
            cursor.execute(query2)
            venues = cursor.fetchall()
            query3 = "SELECT * FROM dj_Events"
            cursor.execute(query3)
            events = cursor.fetchall()
            comun = array('H')
            i = 0

            # (%s)
            # comun1=', '.join(map(lambda x: '%s',comun))
            # query=query % ('%s',comun1)

            for row in bands:
                pk = row[0]
                eventID = row[1]
                eventName = row[2]
                venueID = row[3]
                venueName = row[4]
                venueAddress = row[5]
                date = row[6]
                start = row[7]
                city = row[8]
                country = row[9]
                artistID = row[10]
                sourceID = row[11]
                latitude = round(row[12], 4)
                longitude = round(row[13], 4)

                if "\\" in eventName:
                    eventName = eventName.replace("\\", "")
                if "\'" in eventName:
                    eventName = eventName.replace("\'", "\\'")
                elif "'" in eventName:
                    eventName = eventName.replace("'", "\'")
                if "\\" in venueName:
                    venueName = venueName.replace("\\", "")
                if "\'" in venueName:
                    venueName = venueName.replace("\'", "\\'")
                elif "'" in venueName:
                    venueName = venueName.replace("'", "\'")
                print (pk)
                # query = "Select venueID,venueBands,venueName from dj_Venues where venueBands like '" + str(
                #     venueID) + "'"

                # query = "Select venueID,venueBands,venueName from dj_Venues where venueName like '" + str(
                #     venueName) + "and latitude like " + str(latitude) + "and longitude like" + str(longitude) "'"

                query = "select  venueID, venueBands, venueName from dj_Venues where venueName \
                        like %s and ROUND(venuelatitude,4) = %s and ROUND(venuelongitude,4) = %s "

                variable = (venueName, latitude, longitude)

                cursor.execute(query, variable)

                if cursor.rowcount > 0:
                    result = cursor.fetchone()
                    VID = result[0]
                    vname = result[2]
                    print (VID, vname, eventID, pk)
                    print ("EXISTENT VENUE")
                    if "\\" in vname:
                        vname = vname.replace("\\", "")
                    if "\'" in vname:
                        vname = vname.replace("\'", "\\'")
                    elif "'" in vname:
                        vname = vname.replace("'", "\'")
                    query = "Select bandsintownID,eventID from dj_Events where bandsintownID like '" + str(
                        eventID) + "'"
                    cursor.execute(query)
                    if cursor.rowcount > 0:
                        print ("EXISTENT EVENT")
                        comun.append(pk)
                        EID = cursor.fetchone()[1]
                        query = "Select * from dj_Master where artistID like '" + str(
                            artistID) + "' and eventID in (Select eventID from dj_Events where date like '" + str(
                            date) + "')"  # like '"+str(EID)+"' and venueID like '"+str(VID)+"'"
                        cursor.execute(query)
                        if cursor.rowcount == 0:
                            print ("Insert to master")
                            query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
                            variable = (artistID, EID, VID)
                            cursor.execute(query, variable)
                            db.commit()
                    else:  # the event is not in DB with this id and we try to match it with the existing one and find a possible match
                        for row in events:
                            eventName1 = row[1]
                            venueEv = row[2]
                            bandsintownID = row[3]
                            residentID = row[4]
                            gigatoolsID = row[5]
                            date1 = row[6]
                            start1 = row[7]
                            end1 = row[8]
                            if "\\" in eventName1:
                                eventName1 = eventName1.replace("\\", "")
                            if "\'" in eventName1:
                                eventName1 = eventName1.replace("\'", "\\'")
                            elif "'" in eventName1:
                                eventName1 = eventName1.replace("'", "\'")
                            if vname == venueEv:
                                if date == date1:  # if match found update it with bands ID
                                    print (eventName + " || " + eventName1 + " = EVENTS ON " + str(date) + "=" + str(
                                        date1))
                                    query = "Select eventID from dj_Events where bandsintownID like '" + str(
                                        eventID) + "'"
                                    cursor.execute(query)
                                    if cursor.rowcount > 0:
                                        print ("ALREADY")
                                        EID = cursor.fetchone()[0]
                                        print (pk)
                                        try:
                                            pk = ' '.join(map(str, pk))
                                            print (pk, type(pk))
                                            pk = int(pk)
                                            print (pk, type(pk))
                                        except:
                                            print ("except")
                                            pk = float(pk)
                                            pk = int(pk)

                                        comun.append(pk)
                                    else:
                                        print ("UPDATE EVENTS")
                                        query = "Update dj_Events set bandsintownID=%s,sourceBands=%s where eventName like '" + eventName1.encode(
                                            'utf-8') + "' and evenueName like '" + vname.encode(
                                            'utf-8') + "' and date like '" + str(date1) + "'"
                                        variable = (eventID, sourceID)
                                        cursor.execute(query, variable)
                                        db.commit()
                                        comun.append(pk)
                                        query = "Select eventID from dj_Events where eventName like '" + eventName1.encode(
                                            'utf-8') + "' and date like '" + str(date1) + "'"

                                        cursor.execute(query)

                                        pk = cursor.fetchone()
                                        EID = pk[0]

                                    query = "Select * from dj_Master where artistID like '" + str(
                                        artistID) + "' and eventID in (Select eventID from dj_Events where date like '" + str(
                                        date1) + "')"  # like '"+str(EID)+"' and venueID like '"+str(VID)+"'"

                                    cursor.execute(query)
                                    if cursor.rowcount == 0:
                                        print ("Insert to master")
                                        query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
                                        variable = (artistID, EID, VID)
                                        cursor.execute(query, variable)
                                        db.commit()
                                else:  # check again to see if was inserted on last iteration in events matching and if exists go to next
                                    query = "Select bandsintownID,eventID from dj_Events where bandsintownID like '" + str(
                                        eventID) + "'"
                                    cursor.execute(query)
                                    if cursor.rowcount > 0:
                                        print ("DUPLICATE EVENT")
                                        EID = cursor.fetchone()[1]
                                    else:  # the event is completely new and we insert it
                                        print "CREATING EVENT"
                                        query = "INSERT IGNORE INTO dj_Events(eventName,evenueName,bandsintownID,date,startTime,sourceBands) VALUES (%s,%s,%s,%s,%s,%s) ON DUPLICATE KEY UPDATE eventName=eventName,evenueName=evenueName,bandsintownID=bandsintownID,date=date,startTime=startTime,sourceBands=sourceBands"
                                        variable = (eventName, vname, eventID, date, start, sourceID)
                                        cursor.execute(query, variable)
                                        db.commit()
                                        query = "Select eventID from dj_Events where bandsintownID like '" + str(
                                            eventID) + "' and date like '" + str(date) + "'"
                                        print (pk)
                                        comun.append(pk)
                                        cursor.execute(query)
                                        if cursor.rowcount > 0:
                                            EID = cursor.fetchone()[0]
                                    query = "Select * from dj_Master where artistID like '" + str(
                                        artistID) + "' and eventID in (Select eventID from dj_Events where date like '" + str(
                                        date) + "')"  # eventID like '"+str(EID)+"' and venueID like '"+str(VID)+"'"

                                    cursor.execute(query)
                                    if cursor.rowcount == 0:
                                        print ("Insert to master")
                                        query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
                                        variable = (artistID, EID, VID)
                                        cursor.execute(query, variable)
                                        db.commit()
                                        EID = 0
                    VID = 0
                else:
                    for row in venues:
                        venueID1 = row[0]
                        venueName1 = row[1]
                        venueAddress1 = row[2]
                        venueBands = row[5]
                        venueResident = row[6]
                        venueGigatools = row[7]
                        city1 = row[9]
                        country1 = row[10]
                        venueFacebook = row[11]
                        latitude = round(row[3],4)
                        longitude = round(row[4],4)

                        VN = fuzz.token_set_ratio(venueName, venueName1)
                        CT = fuzz.token_set_ratio(country, country1)
                        if venueAddress == '' or venueAddress1 == '':
                            string = 1
                        else:
                            AD = fuzz.token_set_ratio(venueAddress, venueAddress1)
                            string = 2
                        # print "Ev1:%s"%event+"-Ev2:%s"%event1+"=%s" %ptr
                        if VN >= 95:
                            if string == 1:
                                if CT >= 90:
                                    print (venueName + " || " + venueName1 + "=" + str(VN) + " ARE THE SAME VENUES")
                                    address = venueAddress1
                                    region = country1
                                    venue = venueName1
                                    if "\\" in venue:
                                        venue = venue.replace("\\", "")
                                    if "\'" in venue:
                                        venue = venue.replace("\'", "\\'")
                                    elif "'" in venue:
                                        venue = venue.replace("'", "\'")
                                    city1 = city
                                    comun.append(pk)

                                    #query = "Select venueID from dj_Venues where venueName like '" + str(
                                        #venueName1) + "' and country like '" + region + "' LOCK IN SHARE MODE"
                                    query = "Select venueID from dj_Venues where venueName like %s and country like %s LOCK IN SHARE MODE"
                                    # query = "Select venueID from dj_Venues where venueName like %s "

                                    variable = (venueName1)
                                    #print(query, variable)
                                    cursor.execute(query, (venueName1, region))
                                    comun.append(pk)

                                    if cursor.rowcount > 0:
                                        VID = cursor.fetchone()[0]
                                    else:
                                        print ("Update")
                                        query = "Update dj_Venues set venueBands=%s,city=%s where venueName like '" + venue.encode(
                                            'utf-8') + "' and country like '" + region + "'"
                                        variable = (venueID1, city1)
                                        cursor.execute(query, variable)
                                        db.commit()
                                        query = "Select venueID from dj_Venues where venueName like '" + venue.encode(
                                            'utf-8') + "' or venueName like '" + venueName.encode(
                                            'utf-8') + "' and country like '" + region + "'"
                                        cursor.execute(query)
                                        VID = cursor.fetchone()[0]
                                    for row in events:
                                        eventName1 = row[1]
                                        venueEv = row[2]
                                        bandsintownID = row[3]
                                        residentID = row[4]
                                        gigatoolsID = row[5]
                                        date1 = row[6]
                                        start1 = row[7]
                                        end1 = row[8]
                                        if "\\" in eventName1:
                                            eventName1 = eventName1.replace("\\", "")
                                        if "\'" in eventName1:
                                            eventName1 = eventName1.replace("\'", "\\'")
                                        elif "'" in eventName1:
                                            eventName1 = eventName1.replace("'", "\'")
                                        if "\\" in eventName:
                                            eventName = eventName.replace("\\", "")
                                        if "\'" in eventName:
                                            eventName = eventName.replace("\'", "\\'")
                                        elif "'" in eventName:
                                            eventName = eventName.replace("'", "\'")
                                        if venue == venueEv:
                                            if date == date1:
                                                print (eventName + " || " + eventName1 + " ARE THE SAME EVENTS ON " + str(
                                                    date) + "=" + str(date1))

                                                query = "Select eventID from dj_Events where bandsintownID like '" + str(
                                                    eventID) + "' and eventName like '" + eventName1.encode(
                                                    'utf-8') + "' or eventName like '" + eventName.encode(
                                                    'utf-8') + "' and evenueName like '" + venue.encode(
                                                    'utf-8') + "' and date like '" + str(date1) + "' LOCK IN SHARE MODE"
                                                cursor.execute(query)
                                                if cursor.rowcount > 0:
                                                    print ("ALREADY")
                                                    EID = cursor.fetchone()[0]
                                                else:
                                                    print ("UPDATE EVENTS")
                                                    query = "Update dj_Events set bandsintownID=%s,sourceBands=%s where eventName like '" + eventName1.encode(
                                                        'utf-8') + "' and evenueName like '" + venue.encode(
                                                        'utf-8') + "' and date like '" + str(date1) + "'"
                                                    variable = (eventID, sourceID)
                                                    cursor.execute(query, variable)
                                                    db.commit()
                                                    query = "Select eventID from dj_Events where eventName like '" + eventName1.encode(
                                                        'utf-8') + "' or eventName like '" + eventName.encode(
                                                        'utf-8') + "' and evenueName like '" + venue.encode(
                                                        'utf-8') + "' and date like '" + str(date1) + "'"
                                                    cursor.execute(query)
                                                    EID = cursor.fetchone()[0]
                                                query = "Select * from dj_Master where artistID like '" + str(
                                                    artistID) + "' and eventID in (Select eventID from dj_Events where date like '" + str(
                                                    date1) + "')"  # eventID like '"+str(EID)+"' and venueID like '"+str(VID)+"'"
                                                cursor.execute(query)
                                                if cursor.rowcount == 0:
                                                    print ("Insert to master")
                                                    query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
                                                    variable = (artistID, EID, VID)
                                                    cursor.execute(query, variable)
                                                    db.commit()
                                                EID = 0
                                    VID = 0

                            elif string == 2:
                                if CT >= 90 and AD >= 60:
                                    print (venueName + " || " + venueName1 + "=" + str(VN) + " ARE THE SAME VENUES")
                                    address = venueAddress1
                                    region = country1
                                    venue = venueName1
                                    if "\\" in venue:
                                        venue = venue.replace("\\", "")
                                    if "\'" in venue:
                                        venue = venue.replace("\'", "\\'")
                                    elif "'" in venue:
                                        venue = venue.replace("'", "\'")

                                    city1 = city
                                    comun.append(pk)
                                    # query = "Select venueID from dj_Venues where venueName like '" + str(
                                    #     venue) + "' and country like '" + region + "' LOCK IN SHARE MODE"
                                    query = "Select venueID from dj_Venues where venueName like %s and country like %s and ROUND(venuelatitude,4) = %s and ROUND(venuelongitude,4) = %s  LOCK IN SHARE MODE "
                                    variable = (venue, region, latitude, longitude)
                                    cursor.execute(query, variable)
                                    comun.append(pk)
                                    if cursor.rowcount > 0:
                                        VID = cursor.fetchone()[0]
                                    else:
                                        query = "Update dj_Venues set venuelatitude=%s, venuelongitude=%s, city=%s where venueName like '" + venue + "' and country like '" + region + "'"
                                        # query = "Update dj_Venues set venueBands=%s, city=%s where venueName like %s and country like %s and ROUND(venuelatitude,4) = %s and ROUND(venuelongitude,4) = %s"
                                        # variable = (venueID, city1, venue, region, latitude, longitude)
                                        variable = (latitude, longitude, city1)
                                        cursor.execute(query, variable)
                                        db.commit()
                                        # query = "Select venueID from dj_Venues where venueName like '" + str(
                                        #     venue) + "' and country like '" + region + "'"
                                        query = "Select venueID from dj_Venues where venueName like %s and country like %s and ROUND(venuelatitude,4) = %s and ROUND(venuelongitude,4) = %s"
                                        variable = (venue, region, latitude, longitude)
                                        cursor.execute(query, variable)
                                        VID = cursor.fetchone()[0]


                                    for row in events:
                                        eventName1 = row[1]
                                        venueEv = row[2]
                                        bandsintownID = row[3]
                                        residentID = row[4]
                                        gigatoolsID = row[5]
                                        date1 = row[6]
                                        start1 = row[7]
                                        end1 = row[8]
                                        if "\\" in eventName1:
                                            eventName1 = eventName1.replace("\\", "")
                                        if "\'" in eventName1:
                                            eventName1 = eventName1.replace("\'", "\\'")
                                        elif "'" in eventName1:
                                            eventName1 = eventName1.replace("'", "\'")
                                        if "\\" in eventName:
                                            eventName = eventName.replace("\\", "")
                                        if "\'" in eventName:
                                            eventName = eventName.replace("\'", "\\'")
                                        elif "'" in eventName:
                                            eventName = eventName.replace("'", "\'")
                                        if venue == venueEv:
                                            if date == date1:
                                                print (eventName + " || " + eventName1 + " ARE THE SAME EVENTS ON " + str(
                                                    date) + "=" + str(date1))

                                                query = "Select eventID from dj_Events where bandsintownID like '" + str(
                                                    eventID) + "' and eventName like '" + eventName1.encode(
                                                    'utf-8') + "' or eventName like '" + eventName.encode(
                                                    'utf-8') + "' and evenueName like '" + venue.encode(
                                                    'utf-8') + "' and date like '" + str(date1) + "' LOCK IN SHARE MODE"
                                                cursor.execute(query)
                                                if cursor.rowcount > 0:
                                                    print ("ALREADY")
                                                    EID = cursor.fetchone()[0]
                                                else:
                                                    print ("UPDATE EVENTS")
                                                    query = "Update dj_Events set bandsintownID=%s,sourceBands=%s where eventName like '" + eventName1.encode(
                                                        'utf-8') + "' and evenueName like '" + venue.encode(
                                                        'utf-8') + "' and date like '" + str(date1) + "'"
                                                    variable = (eventID, sourceID)
                                                    cursor.execute(query, variable)
                                                    db.commit()
                                                    query = "Select eventID from dj_Events where eventName like '" + eventName1.encode(
                                                        'utf-8') + "' or bandsintownID like '" + str(
                                                        eventID) + "' and evenueName like '" + venue.encode(
                                                        'utf-8') + "' and date like '" + str(date1) + "'"
                                                    cursor.execute(query)
                                                    EID = cursor.fetchone()[0]
                                                query = "Select * from dj_Master where artistID like '" + str(
                                                    artistID) + "' and eventID in (Select eventID from dj_Events where date like '" + str(
                                                    date) + "')"  # eventID like '"+str(EID)+"' and venueID like '"+str(VID)+"'"
                                                cursor.execute(query)
                                                if cursor.rowcount == 0:
                                                    print ("Insert to master")
                                                    query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
                                                    variable = (artistID, EID, VID)
                                                    cursor.execute(query, variable)
                                                    db.commit()
                                                EID = 0
                                    VID = 0
                i += 1

            myTuple = tuple(comun)
            try:
                query = "SELECT PK, eventID, eventName, venueID, venueName, venueAddress, date, startTime, city, country, artistID, sourceID, ROUND(latitude,4), ROUND(longitude,4) FROM dj_parsed_data where sourceID = '%s' and artistID = '%s' and PK not in %s" % (
                    sourceID, artistID, str(myTuple))
                print (query)
                cursor.execute(query)
            except Exception:
                query = "SELECT PK, eventID, eventName, venueID, venueName, venueAddress, date, startTime, city, country, artistID, sourceID, ROUND(latitude,4), ROUND(longitude,4) FROM dj_parsed_data where sourceID = '%s' and artistID = '%s' " % (
                    sourceID, artistID)
                print (query)
                cursor.execute(query)
                # print "No data from Bandsintown for this artistID = '%s'" % (artistID)
                return
            unic = cursor.fetchall()
            for row in unic:
                print("row ++++++++++++++++++++++++++++++++")
                print(row)
                pk = row[0]
                eventID = row[1]
                eventName = row[2]
                venueID = row[3]
                venueName = row[4]
                venueAddress = row[5]
                date = row[6]
                start = row[7]
                city = row[8]
                country = row[9]
                artistID = row[10]
                sourceID = row[11]
                latitude = round(row[12], 4)
                longitude = round(row[13], 4)

                if "\\" in eventName:
                    eventName = eventName.replace("\\", "")
                '''if "\'" in eventName:
                    eventName=eventName.replace("\'","\\'")'''
                if "'" in eventName:
                    eventName = eventName.replace("'", "\'")
                if "\\" in venueName:
                    venueName = venueName.replace("\\", "")
                '''if "\'" in venueName:
                    venueName=venueName.replace("\'","\\'")'''
                if "'" in venueName:
                    venueName = venueName.replace("'", "\'")
                # query = "Select venueID from dj_Venues where venueBands like '" + str(
                #     venueID) + "' and country like '" + country + "'"
                query = "Select VenueID from dj_Venues where venueName like %s and country like %s and Round(venuelatitude,4) = %s and ROUND(venuelongitude,4) = %s"
                variable = (venueName, country, latitude, longitude)
                cursor.execute(query, variable)
                if cursor.rowcount > 0:
                    print ("VENUE JUST INSERTED,DUPLICATE")
                    VID = cursor.fetchone()[0]
                else:
                    query = "INSERT INTO dj_Events(eventName,evenueName,bandsintownID,date,startTime, sourceBands, residentID, gigatoolsID, songkickID, facebookID, endtime, sourceResident, sourceGigatools, sourceSongkick, sourceFacebook) VALUES (%s, %s, %s, %s, %s, %s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                    variable = (eventName, venueName, eventID, date, start, sourceID, 0, 0, 0, 0, 0, 0, 0, 0, 0)
                    # query = "INSERT INTO dj_Venues(venueName,venueAddress,venueBands,city,country, venuelatitude, venuelongitude, venueresident, venuegigatools, venuesongkick, venuefacebook) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                    # variable = (venueName, venueAddress, 0, city, country, latitude, longitude, 0, 0, 0, 0)

                    cursor.execute(query, variable)
                    db.commit()
                    if "\\" in venueName:
                        venueName = venueName.replace("\\", "")
                    '''if "\'" in venueName:
                        venueName=venueName.replace("\'","\\'")'''
                    if "'" in venueName:
                        venueName = venueName.replace("'", "\'")

                    query = "Select venueID from dj_Venues where venueName like %s and country like %s and ROUND(venuelatitude,4) = %s and ROUND(venuelongitude,4) = %s "
                    variable = (venueName, country, latitude, longitude)
                    cursor.execute(query, variable)

                    try:
                        VID = cursor.fetchone()[0]
                    except:
                        VID = None

                query = "Select eventID from dj_Events where bandsintownID like '" + str(
                    eventID) + "' and date like '" + str(date) + "'"  # eventName like '"+eventName+"' and
                cursor.execute(query)
                if cursor.rowcount > 0:
                    # query = "INSERT INTO dj_Events(eventName,evenueName,bandsintownID,date,startTime, sourceBands, residentID, gigatoolsID, songkickID, facebookID, endtime, sourceResident, sourceGigatools, sourceSongkick, sourceFacebook) VALUES (%s, %s, %s, %s, %s, %s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                    # variable = (eventName, venueName, eventID, date, start, sourceID, 0, 0, 0, 0, 0, 0, 0, 0, 0)
                    # cursor.execute(query, variable)
                    # db.commit()
                    # query = "Select eventID from dj_Events where bandsintownID like '" + str(
                    #     eventID) + "' and date like '" + str(date) + "'"
                    # cursor.execute(query)
                    # EID = cursor.fetchone()[0]
                    print (query)
                    print ("EVENT JUST INSERTED,DUPLICATE")
                    EID = cursor.fetchone()[0]
                else:
                    # current_date_time = datetime.now()
                    # print "current_date_time +++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
                    # print current_date_time
                    query = "INSERT INTO dj_Events(eventName,evenueName,bandsintownID,date,startTime, sourceBands, residentID, gigatoolsID, songkickID, facebookID, endtime, sourceResident, sourceGigatools, sourceSongkick, sourceFacebook) VALUES (%s, %s, %s, %s, %s, %s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                    variable = (eventName, venueName, eventID, date, start, sourceID, 0, 0, 0, 0, 0, 0, 0, 0, 0)
                    cursor.execute(query, variable)
                    db.commit()
                    query = "Select eventID from dj_Events where bandsintownID like '" + str(
                        eventID) + "' and date like '" + str(date) + "'"
                    cursor.execute(query)
                    EID = cursor.fetchone()[0]
                query = "Select * from dj_Master where artistID like '" + str(
                    artistID) + "' and eventID in (Select eventID from dj_Events where date like '" + str(
                    date) + "')"  # eventID like '"+str(EID)+"' and venueID like '"+str(VID)+"'"
                cursor.execute(query)
                if cursor.rowcount == 0:
                    print ("Insert to master")
                    try:
                        query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
                        variable = (artistID, EID, VID)
                        cursor.execute(query, variable)
                        db.commit()
                    except:
                        pass
                EID = 0
                VID = 0
            cursor.close()
        except Exception as ex:
            logger.exception(ex)
