#!/usr/bin/python
import logging, requests
import scrapper.requests, urllib
from artist.models import Artist
from report.models import ReportSongkick
from report.utils import *
from scrapper.fuzzywuzzy import fuzz
from scrapper.fuzzywuzzy import process
from array import array
import difflib
from time import sleep
from bs4 import BeautifulSoup

from scrapper.parsers import Parser, ParsedItem, Processor
from scrapper.parsers.source import SourceType
from scrapper.logger import LOG_NAME
from datetime import datetime, date

from social_data.models import DataLog

import sys

logger = logging.getLogger(LOG_NAME)


class SongkickParser(Parser):
	today_date = date.today()


	def __init__(self, artist):
		Parser.__init__(self, artist, SourceType.Songkick)
		self.artDBObject = Artist.objects.get(artistID=self.artist.artistID)
		logger.info("Creating %s instance. Artist Name: %s, Source: %s" %
					(self.__class__.__name__, self.artist.artistName, self.source_name))

	def scrape(self):
		try:
			self.validate()
			source_url = self.artist.artistSources[self.source_name]['url']
			source_url = "%s/%s" % (source_url, "calendar")
		except Exception as e:
			report = ReportSongkick(created=self.today_date,
									artist=self.artDBObject)
			report.title = "Artist: %s was not scrapped" % self.artist.artistName
			report.description = "Internal error occured during scrapping process: " + str(e.message)
			report.event = UNKNOWN
			report.resolution = CKECK_URL
			report.save()
			raise e


		logger.info("SongkickParser:: Scrapping initiated for Artist Name: %s, Source: %s, url: %s" %
					(self.artist.artistName,
					 self.source_name, source_url))

		source_request = requests.get(source_url)

		logger.info("SongkickParser:: Url: %s, Response content length: %s"
					% (source_url, len(source_request.content)))


		parsed_items = []
		endTime=''
		count = 0

		try:
			source_soup = BeautifulSoup(source_request.content, "html.parser")
			Name = source_soup.find("div", class_="component brief with-ads").find("h1")
			Name = Name.text

			links = source_soup.findAll("li", {"title":True})
			count = 0
			for a in links:
				parsed_item = ParsedItem()

				parsed_item.artistID = self.artist.artistID
				parsed_item.sourceID = self.artist.artistSources[self.source_name]['id']

				url=a.find('p',class_='artists summary').find('a')
				parsed_item.eventName=url.text.strip()
				parsed_item.eventName=' '.join(parsed_item.eventName.split())
				if '<span class="title' in parsed_item.eventName:
					parsed_item.eventName=Name
				urll=url['href']
				time=a.find('time')
				time=str(time['datetime'])
				venue=a.find('p',class_='location')
				try:
					parsed_item.venueID=a.find('a')['href'].split('-')[0].split('/')[2]
					# PARSE VENUE NAME
					if (a.find('p',class_='location').find(class_='venue-name') != None):
						parsed_item.venueName=a.find('p',class_='location').find(class_='venue-name').text
					else:
						parsed_item.venueName=a.find('p',class_='location').text.strip()
					# PARSE VENUE CITY
					if (len(a.find('p',class_='location').findAll('span')) < 3):
						parsed_item.city=a.find('p',class_='location').text.split(',')[0].strip()
						parsed_item.country=a.find('p',class_='location').text.split(',')[-1].strip()
					else:
						parsed_item.country = a.find('p',class_='location').findAll('span')[-2].text.split(',')[-1].strip()
						parsed_item.city = a.find('p',class_='location').findAll('span')[-2].text.split(',')[0].strip()
						pass


					try:
						if (a.find('p',class_='location').find(class_="street-address") != None):
							parsed_item.venueAddress=a.find('p',class_='location').find(class_="street-address").text.strip()
						else:
							parsed_item.venueAddress=parsed_item.city+', '+ parsed_item.country
						if parsed_item.country=='US':
							parsed_item.country='United States of America'
						elif parsed_item.country=='UK':
							parsed_item.country='United Kingdom'
					except IndexError:
						parsed_item.country=a.find('p',class_='location').text.split(',')[1].split('\n')[0].strip()
						parsed_item.venueAddress=""
						if parsed_item.country=='US':
							parsed_item.country='United States of America'
						elif parsed_item.country=='UK':
							parsed_item.country='United Kingdom'
				except TypeError:
					parsed_item.venueID=''
					parsed_item.venueName=a.find('p',class_='location').text.split(',')[0].strip()
					parsed_item.city=parsed_item.venueName
					parsed_item.venueAddress=''
					try:
						parsed_item.country=a.find('p',class_='location').text.split(',')[2].strip()
						if parsed_item.country=='US':
							parsed_item.country='United States of America'
						elif parsed_item.country=='UK':
							parsed_item.country='United Kingdom'
					except IndexError:
						parsed_item.country=a.find('p',class_='location').text.split(',')[1].strip()
						if parsed_item.country=='US':
							parsed_item.country='United States of America'
						elif parsed_item.country=='UK':
							parsed_item.country='United Kingdom'
				if 'T' in time:
					parsed_item.date=time.split('T')[0]
					if '+' in time:
						parsed_item.startTime=time.split('T')[1].split('+')[0]
					elif '-' in time :
						parsed_item.startTime=time.split('T')[1].split('-')[0]
				else:
					parsed_item.date=time
					parsed_item.startTime=''
				if '/festivals/' in urll:
					parsed_item.eventID=urll.split('/id/')[1].split('-')[0]
				else:
					parsed_item.eventID=urll.split('/concerts/')[1].split('-')[0]

				parsed_item.print_info(extra_info="%s-%s" % (self.artist.artistName, self.source_name))
				parsed_items.append(parsed_item)

				count += 1

			report = ReportSongkick(created=self.today_date,
									artist=self.artDBObject)
			report.title = "Artist: %s was scrapped" % self.artist.artistName
			report.description = "Succesful scrape Events : " + str(count)
			report.event = INFO
			report.resolution = NA
			report.save()

				# print eventID
				# print eventName
				# print venueID
				# print venueName
				# print venueAddress
				# print date
				# print startTime
				# print endTime
				# print city
				# print country

		except Exception, ex:
			report = ReportSongkick(created=self.today_date,
									artist=self.artDBObject)
			report.title = "Artist: %s was not scrapped" % self.artist.artistName
			report.description = "Internal error occured: " + str(ex.message)
			report.event = ERROR
			report.resolution = CKECK_URL +" check if artist have concerts or check URL"
			report.save()
			logger.exception(ex)

		songkick_log, created = DataLog.objects.get_or_create(created=SongkickParser.today_date)
		if songkick_log.events_song_kick:
			events_song_kick = int(songkick_log.events_song_kick)
			events_song_kick += count
			songkick_log.events_song_kick = events_song_kick
		else:
			songkick_log.events_song_kick = count
			songkick_log.save()

		logger.info("SongkickParser:: Scrapping Ended for Artist Name: %s, Source: %s, url: %s" %
					(self.artist.artistName,
					 self.source_name, self.artist.artistSources[self.source_name]))

		return parsed_items


class SongkickProcessor(Processor):
	def __init__(self):
		Processor.__init__(self, SourceType.Songkick)
		logger.info("Creating %s instance. Source: %s" %
					(self.__class__.__name__, self.source_name))

	def process(self, database, sourceID, artistID):
		try:  # open text file with bandsintown profiles
			db = database.con
			cursor = db.cursor(buffered=True)
			print "PR0CESS SONGKICK"
			sleep(2)
			try:
				query = "SELECT PK, eventID, eventName, venueID, venueName, venueAddress, date, startTime, city, country, artistID, sourceID FROM dj_parsed_data where sourceID = '%s' and artistID='%s'" % (sourceID, artistID)  # where eventID not in (Select songkickID from dj_Events where songkickID > 0) "
				cursor.execute(query)
			except Exception:
				print "No data from SONGKICK for this artistID = '%s'"%(artistID)
				return
			bands = cursor.fetchall()
			query2 = "SELECT * FROM dj_Venues"
			cursor.execute(query2)
			venues = cursor.fetchall()
			query3 = "SELECT * FROM dj_Events"
			cursor.execute(query3)
			events = cursor.fetchall()
			comun = array('H')
			i = 0

			# (%s)
			# comun1=', '.join(map(lambda x: '%s',comun))
			# query=query % ('%s',comun1)

			for row in bands:

				pk = row[0]
				eventID = row[1]
				eventName = row[2]
				venueID = row[3]
				venueName = row[4]
				venueAddress = row[5]
				date = row[6]
				start = row[7]
				city = row[8]
				country = row[9]
				artistID = row[10]
				sourceID = row[11]
				if "\\" in eventName:
					eventName = eventName.replace("\\", "")
				if "\'" in eventName:
					eventName = eventName.replace("\'", "\\'")
				elif "'" in eventName:
					eventName = eventName.replace("'", "\'")
				if "\\" in venueName:
					venueName = venueName.replace("\\", "")
				if "\'" in venueName:
					venueName = venueName.replace("\'", "\\'")
				elif "'" in venueName:
					venueName = venueName.replace("'", "\'")
				print pk
				query = "Select venueID,venueSongkick,venueName from dj_Venues where venueSongkick like '" + str(venueID) + "'"
				cursor.execute(query)

				if cursor.rowcount > 0:

					result = cursor.fetchone()
					VID = result[0]
					vname = result[2]
					print VID, vname, eventID, pk
					print "EXISTENT VENUE"
					if "\\" in vname:
						vname = vname.replace("\\", "")
					if "\'" in vname:
						vname = vname.replace("\'", "\\'")
					elif "'" in vname:
						vname = vname.replace("'", "\'")
					query = "Select songkickID,eventID from dj_Events where songkickID like '" + str(
						eventID) + "'"
					cursor.execute(query)
					if cursor.rowcount > 0:
						print "EXISTENT EVENT"
						comun.append(pk)
						EID = cursor.fetchone()[1]
						query = "Select * from dj_Master where artistID like '" + str(
							artistID) + "' and eventID in (Select eventID from dj_Events where date like '" + str(
							date) + "')"  # like '"+str(EID)+"' and venueID like '"+str(VID)+"'"
						cursor.execute(query)
						if cursor.rowcount == 0:
							print "Insert to master"
							query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
							variable = (artistID, EID, VID)
							cursor.execute(query, variable)
							db.commit()
					else:  # the event is not in DB with this id and we try to match it with the existing one and find a possible match
						for row in events:
							eventName1 = row[1]
							venueEv = row[2]
							bandsintownID = row[3]
							residentID = row[4]
							gigatoolsID = row[5]
							songkickID=row[6]
							date1 = row[7]
							start1 = row[8]
							end1 = row[9]
							if "\\" in eventName1:
								eventName1 = eventName1.replace("\\", "")
							if "\'" in eventName1:
								eventName1 = eventName1.replace("\'", "\\'")
							elif "'" in eventName1:
								eventName1 = eventName1.replace("'", "\'")
							if vname == venueEv:
								if date == date1:  # if match found update it with bands ID
									print eventName + " || " + eventName1 + " = EVENTS ON " + str(date) + "=" + str(
										date1)
									query = "Select eventID from dj_Events where songkickID like '" + str(
										eventID) + "'"
									cursor.execute(query)
									if cursor.rowcount > 0:
										print "ALREADY"
										EID = cursor.fetchone()[0]
										print pk
										try:
											pk = ' '.join(map(str, pk))
											print pk, type(pk)
											pk = int(pk)
											print pk, type(pk)
										except:
											print "except"
											pk = float(pk)
											pk = int(pk)

										comun.append(pk)
									else:
										print "UPDATE EVENTS"
										query = "Update dj_Events set songkickID=%s,sourceSongkick=%s where eventName like '" + eventName1.encode(
											'utf-8') + "' and evenueName like '" + vname.encode(
											'utf-8') + "' and date like '" + str(date1) + "'"
										variable = (eventID, sourceID)
										cursor.execute(query, variable)
										db.commit()
										comun.append(pk)
										query = "Select eventID from dj_Events where eventName like '" + eventName1.encode(
											'utf-8') + "' and date like '" + str(date1) + "'"

										cursor.execute(query)

										pk = cursor.fetchone()
										EID = pk[0]

									query = "Select * from dj_Master where artistID like '" + str(
										artistID) + "' and eventID in (Select eventID from dj_Events where date like '" + str(
										date1) + "')"  # like '"+str(EID)+"' and venueID like '"+str(VID)+"'"

									cursor.execute(query)
									if cursor.rowcount == 0:
										print "Insert to master"
										query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
										variable = (artistID, EID, VID)
										cursor.execute(query, variable)
										db.commit()
								else:  # check again to see if was inserted on last iteration in events matching and if exists go to next
									query = "Select songkickID,eventID from dj_Events where songkickID like '" + str(
										eventID) + "'"
									cursor.execute(query)
									if cursor.rowcount > 0:
										print "DUPLICATE EVENT"
										EID = cursor.fetchone()[1]
									else:  # the event is completely new and we insert it
										print "CREATING EVENT"
										query = "INSERT IGNORE INTO dj_Events(eventName,evenueName,songkickID,date,startTime,sourceSongkick) VALUES (%s,%s,%s,%s,%s,%s) ON DUPLICATE KEY UPDATE eventName=eventName,evenueName=evenueName,songkickID=songkickID,date=date,startTime=startTime,sourceSongkick=sourceSongkick"
										variable = (eventName, vname, eventID, date, start, sourceID)
										cursor.execute(query, variable)
										db.commit()
										query = "Select eventID from dj_Events where songkickID like '" + str(
											eventID) + "' and date like '" + str(date) + "'"
										print pk
										comun.append(pk)
										cursor.execute(query)
										if cursor.rowcount > 0:
											EID = cursor.fetchone()[0]
									query = "Select * from dj_Master where artistID like '" + str(
										artistID) + "' and eventID in (Select eventID from dj_Events where date like '" + str(
										date) + "')"  # eventID like '"+str(EID)+"' and venueID like '"+str(VID)+"'"

									cursor.execute(query)
									if cursor.rowcount == 0:
										print "Insert to master"
										query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
										variable = (artistID, EID, VID)
										cursor.execute(query, variable)
										db.commit()
										EID = 0
					VID = 0
				else:
					for row in venues:
						venueID1 = row[0]
						venueName1 = row[1]
						venueAddress1 = row[2]
						venueBands = row[5]
						venueResident = row[6]
						venueGigatools = row[7]
						venueSongkick=row[8]
						city1 = row[9]
						country1 = row[10]

						VN = fuzz.token_set_ratio(venueName, venueName1)
						CT = fuzz.token_set_ratio(country, country1)
						if venueAddress == '' or venueAddress1 == '':
							string = 1
						else:
							AD = fuzz.token_set_ratio(venueAddress, venueAddress1)
							string = 2
						# print "Ev1:%s"%event+"-Ev2:%s"%event1+"=%s" %ptr
						if VN >= 95:
							if string == 1:
								if CT >= 90:
									print venueName + " || " + venueName1 + "=" + str(VN) + " ARE THE SAME VENUES"
									address = venueAddress1
									region = country1
									venue = venueName1
									if "\\" in venue:
										venue = venue.replace("\\", "")
									if "\'" in venue:
										venue = venue.replace("\'", "\\'")
									elif "'" in venue:
										venue = venue.replace("'", "\'")
									city1 = city
									comun.append(pk)
									query = "Select venueID from dj_Venues where venueSongkick like '" + str(
										venueID) + "' and country like '" + region + "' LOCK IN SHARE MODE"
									cursor.execute(query)
									comun.append(pk)
									if cursor.rowcount > 0:
										print "dont update"
										VID = cursor.fetchone()[0]
									else:
										print "Update"
										query = "Update dj_Venues set venueSongkick=%s,city=%s where venueName like '" + venue.encode(
											'utf-8') + "' and country like '" + region + "'"
										variable = (venueID, city1)
										cursor.execute(query, variable)
										db.commit()
										query = "Select venueID from dj_Venues where venueName like '" + venue.encode(
											'utf-8') + "' or venueName like '" + venueName.encode(
											'utf-8') + "' and country like '" + region + "'"
										cursor.execute(query)
										VID = cursor.fetchone()[0]
									for row in events:
										eventName1 = row[1]
										venueEv = row[2]
										bandsintownIDID = row[3]
										residentID = row[4]
										gigatoolsID = row[5]
										songkickID=row[6]
										date1 = row[7]
										start1 = row[8]
										end1 = row[9]
										if "\\" in eventName1:
											eventName1 = eventName1.replace("\\", "")
										if "\'" in eventName1:
											eventName1 = eventName1.replace("\'", "\\'")
										elif "'" in eventName1:
											eventName1 = eventName1.replace("'", "\'")
										if "\\" in eventName:
											eventName = eventName.replace("\\", "")
										if "\'" in eventName:
											eventName = eventName.replace("\'", "\\'")
										elif "'" in eventName:
											eventName = eventName.replace("'", "\'")
										if venue == venueEv:
											if date == date1:
												print eventName + " || " + eventName1 + " ARE THE SAME EVENTS ON " + str(
													date) + "=" + str(date1)

												query = "Select eventID from dj_Events where songkickID like '" + str(
													eventID) + "' and eventName like '" + eventName1.encode(
													'utf-8') + "' or eventName like '" + eventName.encode(
													'utf-8') + "' and evenueName like '" + venue.encode(
													'utf-8') + "' and date like '" + str(date1) + "' LOCK IN SHARE MODE"
												cursor.execute(query)
												if cursor.rowcount > 0:
													print "ALREADY"
													EID = cursor.fetchone()[0]
												else:
													print "UPDATE EVENTS"
													query = "Update dj_Events set songkickID=%s,sourceSongkick=%s where eventName like '" + eventName1.encode(
														'utf-8') + "' and evenueName like '" + venue.encode(
														'utf-8') + "' and date like '" + str(date1) + "'"
													variable = (eventID, sourceID)
													cursor.execute(query, variable)
													db.commit()
													query = "Select eventID from dj_Events where eventName like '" + eventName1.encode(
														'utf-8') + "' or eventName like '" + eventName.encode(
														'utf-8') + "' and evenueName like '" + venue.encode(
														'utf-8') + "' and date like '" + str(date1) + "'"
													cursor.execute(query)
													EID = cursor.fetchone()[0]
												query = "Select * from dj_Master where artistID like '" + str(
													artistID) + "' and eventID in (Select eventID from dj_Events where date like '" + str(
													date1) + "')"  # eventID like '"+str(EID)+"' and venueID like '"+str(VID)+"'"
												cursor.execute(query)
												if cursor.rowcount == 0:
													print "Insert to master"
													query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
													variable = (artistID, EID, VID)
													cursor.execute(query, variable)
													db.commit()
												EID = 0
									VID = 0

							elif string == 2:
								if CT >= 90 and AD >= 60:
									print venueName + " || " + venueName1 + "=" + str(VN) + " ARE THE SAME VENUES"
									address = venueAddress1
									region = country1
									venue = venueName1
									if "\\" in venue:
										venue = venue.replace("\\", "")
									if "\'" in venue:
										venue = venue.replace("\'", "\\'")
									elif "'" in venue:
										venue = venue.replace("'", "\'")

									city1 = city
									comun.append(pk)
									query = "Select venueID from dj_Venues where venueSongkick like '" + str(
										venueID) + "' and country like '" + region + "' LOCK IN SHARE MODE"
									cursor.execute(query)
									comun.append(pk)
									if cursor.rowcount > 0:
										print "dont update"
										VID = cursor.fetchone()[0]
									else:
										print "Update"
										query = "Update dj_Venues set venueSongkick=%s,city=%s where venueName like '" + venue + "' and country like '" + region + "'"
										variable = (venueID, city1)
										cursor.execute(query, variable)
										db.commit()
										query = "Select venueID from dj_Venues where venueName like '" + str(
											venue) + "' and country like '" + region + "'"
										cursor.execute(query)
										VID = cursor.fetchone()[0]
									for row in events:
										eventName1 = row[1]
										venueEv = row[2]
										bandsintownID = row[3]
										residentID = row[4]
										gigatoolsID = row[5]
										songkickID=row[6]
										date1 = row[7]
										start1 = row[8]
										end1 = row[9]
										if "\\" in eventName1:
											eventName1 = eventName1.replace("\\", "")
										if "\'" in eventName1:
											eventName1 = eventName1.replace("\'", "\\'")
										elif "'" in eventName1:
											eventName1 = eventName1.replace("'", "\'")
										if "\\" in eventName:
											eventName = eventName.replace("\\", "")
										if "\'" in eventName:
											eventName = eventName.replace("\'", "\\'")
										elif "'" in eventName:
											eventName = eventName.replace("'", "\'")
										if venue == venueEv:
											if date == date1:
												print eventName + " || " + eventName1 + " ARE THE SAME EVENTS ON " + str(
													date) + "=" + str(date1)

												query = "Select eventID from dj_Events where songkickID like '" + str(
													eventID) + "' and eventName like '" + eventName1.encode(
													'utf-8') + "' or eventName like '" + eventName.encode(
													'utf-8') + "' and evenueName like '" + venue.encode(
													'utf-8') + "' and date like '" + str(date1) + "' LOCK IN SHARE MODE"
												cursor.execute(query)
												if cursor.rowcount > 0:
													print "ALREADY"
													EID = cursor.fetchone()[0]
												else:
													print "UPDATE EVENTS"
													query = "Update dj_Events set songkickID=%s,sourceSongkick=%s where eventName like '" + eventName1.encode(
														'utf-8') + "' and evenueName like '" + venue.encode(
														'utf-8') + "' and date like '" + str(date1) + "'"
													variable = (eventID, sourceID)
													cursor.execute(query, variable)
													db.commit()
													query = "Select eventID from dj_Events where eventName like '" + eventName1.encode(
														'utf-8') + "' or songkickID like '" + str(
														eventID) + "' and evenueName like '" + venue.encode(
														'utf-8') + "' and date like '" + str(date1) + "'"
													cursor.execute(query)
													EID = cursor.fetchone()[0]
												query = "Select * from dj_Master where artistID like '" + str(
													artistID) + "' and eventID in (Select eventID from dj_Events where date like '" + str(
													date) + "')"  # eventID like '"+str(EID)+"' and venueID like '"+str(VID)+"'"
												cursor.execute(query)
												if cursor.rowcount == 0:
													print "Insert to master"
													query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
													variable = (artistID, EID, VID)
													cursor.execute(query, variable)
													db.commit()
												EID = 0
									VID = 0
				i += 1

			myTuple = tuple(comun)
			try:
				query = "SELECT PK, eventID, eventName, venueID, venueName, venueAddress, date, startTime, city, country, artistID, sourceID FROM dj_parsed_data where sourceID = '%s' and PK not in %s" % (sourceID, artistID, str(myTuple))
				print query
				cursor.execute(query)
			except Exception:
				print "No data from Bandsintown for this artistID = '%s'"%(artistID)
				return
			unic = cursor.fetchall()
			for row in unic:
				pk = row[0]
				eventID = row[1]
				eventName = row[2]
				venueID = row[3]
				venueName = row[4]
				venueAddress = row[5]
				date = row[6]
				start = row[7]
				city = row[8]
				country = row[9]
				artistID = row[10]
				sourceID = row[11]
				if "\\" in eventName:
					eventName = eventName.replace("\\", "")
				'''if "\'" in eventName:
					eventName=eventName.replace("\'","\\'")'''
				if "'" in eventName:
					eventName = eventName.replace("'", "\'")
				if "\\" in venueName:
					venueName = venueName.replace("\\", "")
				'''if "\'" in venueName:
					venueName=venueName.replace("\'","\\'")'''
				if "'" in venueName:
					venueName = venueName.replace("'", "\'")
				query = "Select venueID from dj_Venues where venueSongkick like '" + str(
					venueID) + "' and country like '" + country + "'"
				cursor.execute(query)
				if cursor.rowcount > 0:
					print "VENUE JUST INSERTED,DUPLICATE"
					VID = cursor.fetchone()[0]
				else:
					query = "INSERT INTO dj_Venues( venueName,venueAddress,venueSongkick,city,country) VALUES (%s,%s,%s,%s,%s)"
					variable = (venueName, venueAddress, venueID, city, country)
					ven=venueName
					cursor.execute(query, variable)
					db.commit()
					if "\\" in venueName:
						venueName = venueName.replace("\\", "")
					'''if "\'" in venueName:
						venueName=venueName.replace("\'","\\'")'''
					if "'" in venueName:
						venueName = venueName.replace("'", "\'")
					try:
						query = "Select venueID from dj_Venues where venueSongkick like '" + str(venueID) + "' and country like '" + country + "'"
						cursor.execute(query)
						VID = cursor.fetchone()[0]
					except TypeError:
						query = "Select venueID from dj_Venues where venueName like '" + str(ven) + "' and country like '" + country + "'"
						cursor.execute(query)
						VID = cursor.fetchone()[0]

				query = "Select eventID from dj_Events where songkickID like '" + str(
					eventID) + "' and date like '" + str(date) + "'"  # eventName like '"+eventName+"' and
				cursor.execute(query)
				if cursor.rowcount > 0:
					print "EVENT JUST INSERTED,DUPLICATE"
					EID = cursor.fetchone()[0]
				else:
					current_date_time = datetime.now()
					query = "INSERT INTO dj_Events(eventName,evenueName,songkickID,date,startTime,sourceSongkick) VALUES (%s,%s,%s,%s,%s,%s)"
					variable = (eventName, venueName, eventID, date, start, sourceID)
					cursor.execute(query, variable)
					db.commit()
					query = "Select eventID from dj_Events where songkickID like '" + str(
						eventID) + "' and date like '" + str(date) + "'"
					cursor.execute(query)
					EID = cursor.fetchone()[0]
				query = "Select * from dj_Master where artistID like '" + str( artistID) + "' and eventID in (Select eventID from dj_Events where date like '" + str(date) + "')"  # eventID like '"+str(EID)+"' and venueID like '"+str(VID)+"'"
				cursor.execute(query)
				if cursor.rowcount == 0:
					print "Insert to master"
					query = "INSERT INTO dj_Master( artistID,eventID,venueID) VALUES (%s,%s,%s)"
					variable = (artistID, EID, VID)
					cursor.execute(query, variable)
					db.commit()
				EID = 0
				VID = 0
			cursor.close()
		except Exception, ex:
					logger.exception(ex);
