#!/usr/bin/python
import logging
from scrapper.parsers import Parser, ParsedItem, Processor
from scrapper.parsers.source import SourceType
from scrapper.logger import LOG_NAME

from social_data.twitter import scrap_twitter_api

logger = logging.getLogger(LOG_NAME)


class TwitterParser (Parser):


    def __init__(self, artist):
        Parser.__init__(self, artist, SourceType.Twitter)
        logger.info("Creating %s instance. Artist Name: %s, Source: %s" %
                    (self.__class__.__name__, self.artist.artistName, self.source_name))

    def scrape(self):
        self.validate()
        #Scraping the twitter data using the twitter api check social_data.twitter.TwitterAPI
        scrap_twitter_api([self.artist.artistID])
        logger.info("Data scrapped for the ArtistName %s, Source: %s" %
                    (self.artist.artistName, self.source_name))
        return []

    def scrape_artist_photo(self):
        pass

class TwitterProcessor (Processor):
    def __init__(self):
        Processor.__init__(self, SourceType.Twitter)
        logger.info("Creating %s instance. Source: %s" %
                    (self.__class__.__name__, self.source_name))

    def process(self, database, sourceID, artistID):
        pass
