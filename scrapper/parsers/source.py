#!/usr/bin/python
import logging
from scrapper.logger import LOG_NAME
logger = logging.getLogger(LOG_NAME)


class SourceType:
    Bandsintown = "Bandsintown"
    ResidentAdviser = "ResidentAdviser"
    Gigatools = "Gigatools"
    Facebook = "Facebook"
    Soundcloud = "Soundcloud"
    Songkick = "Songkick"
    Twitter = "Twitter"
    Youtube = "Youtube"
    Spotify = "Spotify"
    Instagram = "Instagram"
    ElectronicFestivals = "Electronic Festivals"

    @staticmethod
    def equals(first, second):
        return True if first.lower() == second.lower() else False


class Source (object):
    def __init__(self, sourceID = 0, sourceName = ""):
        self.sourceID = sourceID
        self.sourceName = sourceName

    def print_info(self):
        logger.info("Source Details:: ID: %s, Name:%s"
                    % (self.sourceID, self.sourceName))


class ActionType:
    Parser = 1
    Processor = 2
