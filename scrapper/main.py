#!/usr/bin/python
import sys, os
current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)

from scrapper import Scrapper, ScrapMode

if __name__ == '__main__':
    scrapper_instance = Scrapper()
    scrapper_instance.process(scrape_mode=ScrapMode.All, username="system", all=True)
    #scrapper_instance.process(scrape_mode=ScrapMode.All, username="system", selected_artist=['82'])
