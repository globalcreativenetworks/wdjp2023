#!/usr/bin/python
import os, sys, logging
from time import sleep
from proxy.renew_ip import Proxy
from logger import setup_logging
from database import DBInterface
from parsers import (BandsintownParser, GigatoolsParser, ResidentAdviserParser, SoundcloudParser, FacebookParser,
                     SongkickParser, TwitterParser, YoutubeParser, SpotifyParser, InstagramParser, ElectronicFestivalsParser)
from parsers import (BandsintownProcessor, GigatoolsProcessor, ResidentAdviserProcessor, SoundcloudProcessor,
                     FacebookProcessor, SongkickProcessor, TwitterProcessor, YoutubeProcessor, SpotifyProcessor,
                     InstagramProcessor, ElectronicFestivalsProcessor)
from parsers.source import SourceType, ActionType
# from scrapper.parsers.electronic_festivals import ,
from util.calendar import get_current_timestamp
import http.client

reload(sys)
sys.setdefaultencoding('utf-8')


class ScrapMode:
    All = 1
    Scrape = 2
    MachineLearning = 3

    ModeStr = {1: "Rebuild Database", 2: "Scrape Artists", 3: "Perform Machine Learning"}

    @staticmethod
    def get_mode_str(mode):
        if mode not in ScrapMode.ModeStr.keys():
            return "Unknown"
        return ScrapMode.ModeStr.get(mode)

    def __init__(self):
        pass


class Scrapper(object):
    def __init__(self):
        os.chdir(os.path.dirname(os.path.abspath(__file__)))
        # os.chdir(os.path.dirname(os.path.abspath(sys.argv[0])))
        global logger
        logger = setup_logging(os.path.join("logs"), scrnlog=True, txtlog=True, loglevel=logging.INFO)

    def process(self, scrape_mode=ScrapMode.All, selected_artist=[], username="", all=False, source=False):
        try:
            logger.info("Scrapper::process called with Mode: %s(%s) for Artist count(%d)" %
                        ( ScrapMode.get_mode_str(scrape_mode), scrape_mode, len(selected_artist)))
            current_id = 0
            with DBInterface() as db_instance:
                artists = db_instance.get_artists(filters={"artistID": selected_artist}, filter_list=True) if selected_artist else db_instance.get_artists()
                # artist_map = [artist.artistName for artist in artists]
                artist_ids = [str(artist.artistID) for artist in artists]

                #db_instance.start_artist_process_status(artists=artist_ids, status=1)
                if scrape_mode == ScrapMode.All or scrape_mode == ScrapMode.Scrape:
                    db_instance.delete_parsed_data(clear_db=all, artists=selected_artist)
                    # current_id = db_instance.insert_scrape_status("Count: %d" % len(artists), username, ScrapMode.get_mode_str(
                    #     scrape_mode) + " - Scrapping Artists")

                    self.scrape(selected_artist=artists, scrapeId=current_id, scrap_source=source)

                    current_id = db_instance.insert_scrape_status("Count: %d" % len(artists), username, ScrapMode.get_mode_str(
                        scrape_mode) + " - Scrapping Similar Artists")

                    # if not source or source == "Bandsintown":
                    #     self.scrape_similar_artist(selected_artist=artists, scrapeId = current_id, scrape_mode=scrape_mode)

                if scrape_mode == ScrapMode.All or scrape_mode == ScrapMode.MachineLearning:
                    current_id = db_instance.insert_scrape_status("Count: %d" % len(artists), username, ScrapMode.get_mode_str(
                        scrape_mode) + " - Machine Learning")
                    self.machine_learning(selected_artist=artists, scrapeId = current_id, scrape_mode=scrape_mode, scrap_source=source)

                artist_ids = [str(artist.artistID) for artist in artists]
                db_instance.update_artist_process_status(artists=artist_ids, status=3, only_status=1)

        except Exception as ex:
            logger.exception(ex)
            print('Exception caught in process : ' + str(ex))

            try:
                db_instance = DBInterface()
                db_instance.update_scrape_status(current_id,
                                    {"status": 4, "progress": 0})
                artist_ids = [str(artist.artistID) for artist in artists]
                db_instance.update_artist_process_status(artists=artist_ids, status=3, only_status=1)
            except Exception as ex:
                logger.exception(ex)
                print('Exception caught in Exception process : ' + str(ex))
                pass

    @staticmethod
    def get_class_name(source_name, type):
        classmap = {
            SourceType.Bandsintown:  {ActionType.Parser: BandsintownParser, ActionType.Processor: BandsintownProcessor},
            SourceType.Gigatools:  {ActionType.Parser: GigatoolsParser, ActionType.Processor: GigatoolsProcessor},
            SourceType.ResidentAdviser: {ActionType.Parser: ResidentAdviserParser, ActionType.Processor: ResidentAdviserProcessor},
            SourceType.Soundcloud:  {ActionType.Parser: SoundcloudParser, ActionType.Processor: SoundcloudProcessor},
            SourceType.Facebook:  {ActionType.Parser: FacebookParser, ActionType.Processor: FacebookProcessor},
            SourceType.Songkick:  {ActionType.Parser: SongkickParser, ActionType.Processor: SongkickProcessor},
            SourceType.Twitter: {ActionType.Parser: TwitterParser, ActionType.Processor: TwitterProcessor},
            SourceType.Youtube: {ActionType.Parser: YoutubeParser, ActionType.Processor: YoutubeProcessor},
            SourceType.Spotify: {ActionType.Parser: SpotifyParser, ActionType.Processor: SpotifyProcessor},
            SourceType.Instagram: {ActionType.Parser: InstagramParser, ActionType.Processor: InstagramProcessor},
            SourceType.ElectronicFestivals: {ActionType.Parser: ElectronicFestivalsParser,
                                         ActionType.Processor: ElectronicFestivalsProcessor},

        }
        if source_name not in classmap.keys():
            raise Exception("No Class implemented for Source (%s)." % source_name)

        source = classmap[source_name]
        if type not in source.keys():
            raise Exception("No ClassType Defined for Source (%s) type(%s)." % source_name, type)
        return source[type]

    def scrape_similar_artist(self, selected_artist=[], scrapeId = 0, scrape_mode=ScrapMode.All):
        with DBInterface() as db:
            try:

                sources = db.get_sources()
                proxy = Proxy()
                proxy.renew()
                db.update_scrape_status(scrapeId,
                                        {"status": 2, "process": "Scrapping, Scrapping Similar", "progress": 0})
                for artist in selected_artist:
                    try:
                        status_data = {"comment": "Scrap Similar Artist Process Started."}
                        db.update_artist_scrape_status(artistID=artist.artistID, data=status_data)
                        for source in sources:
                            print(source[type])
                            if source.sourceName != SourceType.Bandsintown:
                                continue
                            try:
                                logger.info("Initiating scrapping similar artist process for %s - %s" %
                                            (artist.artistName, source.sourceName) )
                                source_instance = Scrapper.get_class_name(source.sourceName, ActionType.Parser)(artist)
                                parsed_items = source_instance.scrape_similar_artist()
                                for artist_name in parsed_items:
                                    db.add_similar_data(source.sourceID, artist_name, artist.artistID)
                            except Exception as ex:
                                logger.exception(ex)
                        status_data = {"comment": "Scrap Similar Artist Process Finished.",
                                       "scrapStatus": 2 if scrape_mode==ScrapMode.Scrape else 1,
                                       "lastScrappedTime": get_current_timestamp(), "lastSuccessfulScrappedTime": get_current_timestamp()}
                        db.update_artist_scrape_status(artistID=artist.artistID, data=status_data)
                        sleep(.5)
                    except Exception as ex:
                        status_data = {"scrapStatus": 3, "comment": "Scrap Similar Artist Process Failed."}
                        db.update_artist_scrape_status(artistID=artist.artistID, data=status_data)
                        logger.exception(ex)
                try:
                    logger.info("Processing Data to link existing Artists.")
                    db.process_similar_data()
                except Exception as ex:
                    db.update_scrape_status(scrapeId,
                                            {"status": 4, "process": "Linking Similar Artist Failed", "progress": 0})
                    logger.exception(ex)
                db.update_scrape_status(scrapeId,
                                        {"status": 3, "process": "Scrapping Similar Artist Done", "progress": 100})
            except Exception as ex:
                logger.exception(ex)
                print('Exception caught in scrape_similar_artist : '+ str(ex))
                db.update_scrape_status(scrapeId, {"status": 4, "process": "Scrapping Similar Artist Failed", "progress": 0})
            return source[type]

    def scrape(self, selected_artist=[], scrapeId=0, scrape_mode=ScrapMode.All, scrap_source=False):
        with DBInterface() as db:
            try:
                if not scrap_source:
                    sources = db.get_sources()
                else:
                    sources = db.get_single_source(scrap_source)

                db.update_scrape_status(scrapeId, {"status": 2, "process": "Scrapping Started", "progress": 0})
                for artist in selected_artist:
                    try:
                        # proxy = Proxy()
                        # proxy.renew()
                        IPAddr = self.checkIP().replace("\n", "")
                        status_data = {"lastScrappedTime": get_current_timestamp(), "comment": "Scrap Process Started."}
                        db.update_artist_scrape_status(artistID=artist.artistID, data=status_data)
                        for source in sources:
                            try:
                                logger.info("Initiating scrapping process for %s - %s with IP %s" %
                                            (artist.artistName, source.sourceName, IPAddr))
                                source_instance = Scrapper.get_class_name(source.sourceName, ActionType.Parser)(artist)
                                parsed_items = source_instance.scrape()

                                for parsed_item in parsed_items:
                                    db.add_parsed_data(parsed_item)
                                logger.info("Finished scrapping process for %s - %s" %
                                            (artist.artistName, source.sourceName))
                            except Exception as ex:
                                logger.exception(ex)
                        status_data = {"lastScrappedTime": get_current_timestamp(), "lastSuccessfulScrappedTime": get_current_timestamp(),
                                       "comment": "Scrap Process Finished."}
                        db.update_artist_scrape_status(artistID=artist.artistID, data=status_data)
                        sleep(.05)
                    except Exception as ex:
                        status_data = {"scrapStatus" : 3,  "comment": "Scrap Process Failed."}
                        db.update_artist_scrape_status(artistID=artist.artistID, data=status_data)
                        logger.exception(ex)
                db.update_scrape_status(scrapeId, {"status": 3, "process": "Scrapping Done", "progress": 100})
            except Exception as ex:
                logger.exception(ex)
                print('Exception caught in scrape: ' + str(ex))
                db.update_scrape_status(scrapeId, {"status": 4, "process": "Scrapping Failed", "progress": 0})



    def scrape_artist_photos(self, selected_artist=[], scrapeId=0, scrape_mode=ScrapMode.All, scrap_source=False):
        with DBInterface() as db:
            try:
                if not scrap_source:
                    sources = db.get_sources()
                else:
                    sources = db.get_single_source(scrap_source)
                if selected_artist == []:
                    selected_artist = db.get_artists()
                # proxy = Proxy()
                # proxy.renew()
                for artist in selected_artist:
                    try:
                        status_data = {"lastScrappedTime": get_current_timestamp(), "comment": "Scrap Process Started."}
                        for source in sources:
                            try:
                                logger.info("Initiating scrapping process for %s - %s" %
                                            (artist.artistName, source.sourceName) )
                                source_instance = Scrapper.get_class_name(source.sourceName, ActionType.Parser)(artist)
                                parsed_items = source_instance.scrape_artist_photo()

                                logger.info("Finished scrapping process for %s - %s" %
                                            (artist.artistName, source.sourceName))
                            except Exception as ex:
                                logger.exception(ex)
                        status_data = {"lastScrappedTime": get_current_timestamp(), "lastSuccessfulScrappedTime": get_current_timestamp(),
                                       "comment": "Scrap Process Finished."}
                        sleep(.05)
                    except Exception as ex:
                        status_data = {"scrapStatus" : 3,  "comment": "Scrap Process Failed."}
                        logger.exception(ex)
            except Exception as ex:
                logger.exception(ex)
                print('Exception caught in scrape: ' + str(ex))





    def machine_learning(self, selected_artist=[], scrapeId=0, scrape_mode=ScrapMode.All, scrap_source=False):
        with DBInterface() as db:
            try:
                if not scrap_source:
                    sources = db.get_sources()
                else:
                    sources = db.get_single_source(scrap_source)
                db.update_scrape_status(scrapeId, {"status": 2, "process": "Machine Learning Started", "progress": 0})
                for artist in selected_artist:
                    try:
                        status_data = {"lastMLTime": get_current_timestamp(), "comment": "ML Process Started."}
                        db.update_artist_scrape_status(artistID=artist.artistID, data=status_data)
                        for source in sources:
                            try:
                                logger.info("Initiating Machine Learning process for %s - %s" %
                                            (artist.artistName, source.sourceName))
                                source_instance = Scrapper.get_class_name(source.sourceName, ActionType.Processor)()

                                source_instance.process(db, source.sourceID, artist.artistID)

                                logger.info("Finished Machine Learning process for %s - %s" %
                                            (artist.artistName, source.sourceName))
                            except Exception as ex:
                                logger.exception(ex)
                        status_data = {"lastMLTime": get_current_timestamp(),
                                       "lastSuccessfulMLTime": get_current_timestamp(),
                                       "comment": "ML Process Finished.", "scrapStatus": 2}
                        db.update_artist_scrape_status(artistID=artist.artistID, data=status_data)
                        sleep(.05)
                    except Exception as ex:
                        status_data = {"scrapStatus": 3, "comment": "ML Process Failed."}
                        db.update_artist_scrape_status(artistID=artist.artistID, data=status_data)
                        logger.exception(ex)
                db.update_scrape_status(scrapeId, {"status": 3, "process": "Machine Learning Done", "progress": 100})
            except Exception as ex:
                logger.exception(ex)
                db.update_scrape_status(scrapeId, {"status": 4, "process": "Machine Learning Failed", "progress": 0})
                print('Exception caught in MachineLearning : ' + str(ex))

    def scrape_photo(self, selected_artist=[], username="", new_source=False):
        logger.info("Artist photo scrapper initiated by %s for %d users." %
                    (username, len(selected_artist)))
        with DBInterface() as db:
            try:
                sources = db.get_sources()
                proxy = Proxy()
                proxy.renew()
                artists = db.get_artists(filters={"artistID": selected_artist},
                                         filter_list=True) if selected_artist else db.get_artists()
                counter = 0
                for artist in artists:
                    counter += 1
                    try:
                        for source in sources:
                            if source.sourceName not in [SourceType.Bandsintown, SourceType.ResidentAdviser]:
                                continue
                            try:
                                if new_source and source.sourceID == db.get_photo_source(artist.artistID):
                                    continue
                                logger.info("Initiating scrapping artist photo process for %s - %s" %
                                            (artist.artistName, source.sourceName))
                                source_instance = Scrapper.get_class_name(source.sourceName, ActionType.Parser)(artist)
                                processed_data = source_instance.scrap_photo()
                                if processed_data:
                                    processed_data['sourceID'] = source.sourceID
                                    processed_data['artistID'] = artist.artistID
                                    processed_data['lastUpdateTime'] = get_current_timestamp()
                                    processed_data['status'] = 0
                                    db.add_update_item(table_name='dj_ScrappedPhoto', parameter_dict=processed_data)
                                    data = ["%s: %s" % (key, value) for key, value in processed_data.iteritems()]
                                    logger.info("Artist Photo Scrapped: %s" % ", ".join(data))
                                    db.update('dj_Artists', artist.artistID, data={'remarks' : 'Approval Pending'})
                                    break
                            except Exception as ex:
                                logger.exception(ex)
                        sleep(.5)
                        if counter % 50 == 0:
                            logger.info("Proxy renew IP.")
                            proxy.renew()
                    except Exception as ex:
                        logger.exception(ex)
            except Exception as ex:
                logger.exception(ex)
                print('Exception caught in scrape_photo : ' + str(ex))

    def checkIP(self):
        conn = http.client.HTTPConnection("icanhazip.com")
        conn.request("GET", "/")
        response = conn.getresponse()
        return response.read()