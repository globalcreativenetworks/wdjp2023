#!/usr/bin/python
import mysql.connector
import logging, datetime
from scrapper.logger import LOG_NAME
from scrapper.parsers import ArtistCopy
from scrapper.database import DatabaseConfig
from scrapper.parsers.source import Source
logger = logging.getLogger(LOG_NAME)


class DBInterface:
    def __init__(self, server=DatabaseConfig.ServerName,
                 user=DatabaseConfig.DatabaseUserName,
                 password=DatabaseConfig.DatabasePassword,
                 database=DatabaseConfig.DatabaseName):
        logger.info("Creating %s instance. User: %s, Database: %s, Server: %s" %
                    (self.__class__.__name__, user, database, server))
        self.server = server
        self.user = user
        self.password = password
        self.database = database
        self.con = None

    def __enter__(self):
        if self.con is None:
            self.connect()
        return self

    def __exit__(self, type, value, traceback):
        self.disconnect()

    def connect(self):
        logger.info("DB Connection Initiated")
        try:
            self.con = mysql.connector.connect(host=self.server, user=self.user,
                                               password=self.password, database=self.database)
        except mysql.connector.errors.NotSupportedError:
            self.con = mysql.connector.connect(host=self.server, user=self.user,
                                               password=self.password, database=self.database,
                                               auth_plugin='mysql_native_password'
                                               )
        logger.info("DB Connection Established. User: %s, Database: %s, Server: %s" %
                    (self.user, self.database, self.server))

    def execute_query(self, query, select=True):
        if self.con is None or self.con.is_connected() is False:
            self.connect()

        cursor = self.con.cursor()
        cursor.execute(query)
        rows = []
        if select:
            rows = cursor.fetchall()
        else:
            self.con.commit()
        cursor.close()
        return rows

    def disconnect(self):
        logger.info("Database disconnected")
        if self.con:
            self.con.disconnect()
        self.con = None

    def add_update_item(self, table_name, parameter_dict):
        if parameter_dict is None or len(parameter_dict) <= 0:
            return 0
        query = "REPLACE into %s SET %s" % (table_name,"=%s, ".join(parameter_dict.keys()))
        query = query + "=%s;"
        value = (parameter_dict.values())

        if self.con is None or self.con.is_connected() is False:
            self.connect()

        cursor = self.con.cursor()
        cursor.execute(query, value)
        self.con.commit()
        return cursor.lastrowid

    def process_similar_data(self):
        query = 'update `dj_similar_artists` s inner join dj_Artists a ON a.artistName = s.artistName SET s.linkedArtistID = a.artistID'
        self.execute_query(query, False)

    def add_similar_data(self, source_id, artist_name, artist_id):
        table_name = 'dj_similar_artists'
        where = ' BINARY artistName = BINARY "%s" and sourceID = %s and artistID=%s' % (artist_name, source_id, artist_id)
        result = self.execute_query("select similarArtistID from %s  where %s" %
                                      (table_name, where))
        if not result:
            self.add_update_item(table_name=table_name,
                                 parameter_dict={"linkedArtistID": 0, "sourceID": source_id,
                                                   "artistName": artist_name, "artistID": artist_id})

    def add_parsed_data(self, parsed_item):
        parameter_dict =  parsed_item.__dict__
        del parameter_dict['PK']
        return self.add_update_item(table_name="dj_parsed_data", parameter_dict=parameter_dict)

    def delete_parsed_data(self, artists=[], clear_db=False):
        artists.append("0")
        query = "truncate table dj_parsed_data" if clear_db else 'delete from `dj_parsed_data` where artistID in (%s)' % (",".join(artists))
        self.execute_query(query, False)

    def get_sources(self):
        soruces = []
        for row in self.execute_query("select sourceID, sourceName from dj_Source"):
            soruces.append(Source(row[0], row[1]))
        return soruces

    def get_single_source(self, scrap_source):
        soruces = []
        for row in self.execute_query("select sourceID, sourceName from dj_Source where sourceName='{}'".format(scrap_source)):
            soruces.append(Source(row[0], row[1]))
        return soruces

    def get_artists(self, filters=None, filter_list=False):
        artists = {}

        where = ""
        if filter_list:
            where = " where " + filters.keys()[0] + " in ('" + "','".join(filters.values()[0]) + "')"
        else:
            if filters is not None and len(filters) > 0:
                where = " where " + " AND ".join(("%s='%s'" % (key,value)) for key,value in filters.items())

        for row in self.execute_query("select artistID, artistName from dj_Artists %s" % where):
            artist = ArtistCopy()
            artist.artistID = row[0]
            artist.artistName = row[1]
            artists[artist.artistID] = artist

        if len(artists) > 0:
            sources = {}
            for source in self.get_sources():
                sources[source.sourceID] = source.sourceName
            for row in self.execute_query("select artistID, sourceID, url from dj_artist_website where artistID in (%s) and is_verified_link = 1"
                                          % ",".join(str(v) for v in artists.keys())):
                sourceName = sources[row[1]] if row[1] in sources.keys() else "NA"
                artists[row[0]].artistSources[str(sourceName)] = {"id": str(row[1]), "url": str(row[2])}

        return artists.values()

    def insert_scrape_status(self, artistName, userName, title):
        query = "Insert into dj_ScrapeStatus SET artistName = %s, userName = %s, " \
                "title = %s, progress=0, process=0, status=1, startTime=%s"

        if self.con is None or self.con.is_connected() == False:
            self.connect()

        cursor = self.con.cursor()
        cursor.execute(query, [artistName, userName, title, datetime.datetime.now()])
        self.con.commit()
        return cursor.lastrowid

    def update_scrape_status(self, scrapeID, data):
        query = 'update `dj_ScrapeStatus` SET %s where scrapeID = %s' % (", ".join(("%s='%s'" % (key, value)) for key, value in data.items()), scrapeID)
        self.execute_query(query, False)

    def start_artist_process_status(self, artists=[], status=0, only_status=0):
        artists.append("0")
        query = 'REPLACE INTO dj_artist_scrapstatus (scrapStatus, artistID) select %s, artistID from  dj_Artists where artistID in (%s)' % \
                (str(status), ",".join(artists))
        self.execute_query(query, False)

    def update_artist_process_status(self, artists=[], status=0, only_status=0):
        artists.append("0")
        extra_condition = "" if only_status == 0 else " AND scrapStatus = %s" % only_status
        query = 'update dj_artist_scrapstatus set scrapStatus = %s where artistID in (%s)%s' % (status, ",".join(artists), extra_condition)
        self.execute_query(query, False)

    def update_artist_scrape_status(self, artistID, data={}):
        query = 'update `dj_artist_scrapstatus` SET %s where artistID = %s' % \
                (", ".join(("%s='%s'" % (key, value)) for key, value in data.items()), artistID)
        self.execute_query(query, False)

    def update(self, table, artistID, data={}):
        query = 'update `%s` SET %s where artistID = %s' % \
                (table, ", ".join(("%s='%s'" % (key, value)) for key, value in data.items()), artistID)
        self.execute_query(query, False)

    def get_photo_source(self, artistID):
        query = "select sourceID from dj_ScrappedPhoto where artistID = %s" % artistID
        row = self.execute_query(query, True)
        return int(row[0][0]) if row else 0
