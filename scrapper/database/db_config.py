#!/usr/bin/python

from django.conf import settings


class DatabaseConfig:
    try:
        ServerName = settings.SERVERNAME
        DatabaseName = settings.DATABASENAME
        DatabaseUserName = settings.DATABASEUSERNAME
        DatabasePassword = settings.DATABASEPASSWORD
    except NameError:
        raise "Database connection details are missing"

