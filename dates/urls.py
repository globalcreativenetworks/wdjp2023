from django.conf.urls import url

from dates import views

urlpatterns = [


   url(r'^$', views.dates, name='dates'),
   url(r'^single/genre/$', views.dates, name='genre_dates'),
   url(r'^single/budget/$', views.dates, name='budget_dates'),
   url(r'^single/genre_budget/$', views.dates, name='genre_budget_dates'),
   url(r'^weekend/genre/$', views.dates, name='genre_weekend'),
   url(r'^weekend/budget/$', views.dates, name='budget_weekend'),
   url(r'^weekend/genre_budget/$', views.dates, name='genre_budget_weekend'),
   url(r'^period/genre/$', views.dates, name='genre_period'),
   url(r'^period/budget/$', views.dates, name='budget_period'),
   url(r'^period/genre_budget/$', views.dates, name='genre_budget_period'),
   ]