import json
import random
from datetime import datetime as dt
from datetime import timedelta
from itertools import chain

from django.contrib.auth.decorators import login_required
from django.db.models import F
from django.http import HttpResponse
from django.shortcuts import render
from django.template import RequestContext
from django.template.loader import render_to_string

from accounts.decorators import *
from artist.models import (Artist, ArtistGenre, ArtistRequest, ArtistWebsite,
                           DjSimilarArtists, Genre, Source)
from artist.views import (filters, get_upcoming_events_dates,
                          prioritize_favourtie_artist, sort_by_popularity)
from events.models import DjMaster, Event
from profiles.views import save_recent_search
from scrapper.database.database_interface import DBInterface
from social_data.models import (FacebookData, SpotifyData, TwitterData,
                                YoutubeData)
from geography.models import Country


def date_range(start, end):
    r = (end+timedelta(days=1)-start).days
    return [(start+timedelta(days=i)).strftime("%Y-%m-%d") for i in range(r)]


@login_required
@check_email_validated
# @check_plan('Trial','Basic','Premium')
@check_plan('Trial', 'Artist/DJ', 'Promoter', 'Agent')
def dates(request):
    user = request.user
    pro_obj = Profile.objects.get(user=request.user)
    if pro_obj.user_types != "Promoter" and not request.user.is_superuser:
        return redirect('/')
    artist_limit = 30
    free_online_dates = []
    prioritize_my_fav = request.GET.get('prioritize_my_fav', '')

    # Getting Favourite Artists
    profile = Profile.objects.filter(user=request.user).first()
    favourite_artists = []

    if profile:
        favourite_artists = profile.favorite_artist.values_list('artistName')

    if request.GET.get('date_search'):
        date_search = int(request.GET.get('date_search'))
        if (request.GET.get('date')) or (request.GET.get('date1') and request.GET.get('date')):
            date = request.GET.get('date')
            month = date.split('/')[0]
            day = date.split('/')[1]
            year = date.split('/')[2]
            date = dt.strptime(date, "%m/%d/%Y").strftime("%a %d %b %Y")
            formated_date = "'" + year + '-' + month + '-' + day + "'"
            dat = dt.date(dt(year=int(year), month=int(month), day=int(day)))
            msg = 'YOUR SEARCH RESULTS FOR ARTISTS FREE > ON DATE ' + str(date)
            if request.GET.get('date1'):
                date1 = request.GET.get('date1')
                month = date1.split('/')[0]
                day = date1.split('/')[1]
                year = date1.split('/')[2]
                date1 = dt.strptime(date1, "%m/%d/%Y").strftime("%a %d %b %Y")
                formated_date1 = "'" + year + '-' + month + '-' + day + "'"
                dat1 = dt.date(
                    dt(year=int(year), month=int(month), day=int(day)))
                msg = 'YOUR SEARCH RESULTS FOR ARTISTS FREE > BETWEEN DATE  ' + \
                    str(date) + ' AND ' + str(date1)
            if request.GET.get('entries'):
                entries = int(request.GET.get('entries'))
                if (0 < entries < 30) or (30 < entries < 60) or (60 < entries < 120) or (120 < entries):
                    entries = 30
                artist_limit = entries
            else:
                entries = 30
            if request.GET.get('genre'):
                genre = int(request.GET.get('genre'))
                name = Genre.objects.filter(genreID=genre).get()
                msg += ' > FROM ' + name.genreName + ' GENRE'
            else:
                genre = 0
            if request.GET.get('budget'):
                budget = int(request.GET.get('budget'))
                name = " "
                if budget == 1:
                    name = " - UNDERGROUND STAR"
                elif budget == 2:
                    name = " - POPULAR ACT"
                elif budget == 3:
                    name = " - GLOBAL STAR"
                if budget == 0:
                    name = 'NOT SPECIFIED'
                msg += ' > AT FAME LEVEL' + name
            else:
                budget = 0

            if request.GET.get('country'):
                country_code = request.GET.get("country", None)
                if country_code is not None:
                    country_name = Country.objects.filter(code=country_code).get()
                    msg += ' FROM ' + country_name.name
            else:
                country_name = None
                country_code = None

            if request.GET.get('popularity'):
                popularity = int(request.GET.get('popularity'))
                name = Source.objects.filter(sourceID=popularity).get()
                msg += ' MOST POPULAR ON > ' + name.sourceName
                source = name.sourceName.lower() + 'data__source_id'
            else:
                popularity = 0

            if request.GET.get('shuffle'):
                shuffle = int(request.GET.get('shuffle'))
                if shuffle == 0 and not popularity:
                    msg += ' > DISPLAYED RANDOMLY'
            else:
                shuffle = 0
            if country_name is not None:
                allartists = Artist.objects.filter(country=country_name).order_by('artistName').distinct()
            else:
                allartists = Artist.objects.order_by('artistName').distinct()
            if allartists:
                if budget:
                    allartists = allartists.filter(artistBudget=budget)
                if genre:
                    allartists = allartists.filter(genre__genreID=genre)
                if shuffle == 1:
                    allartists = allartists.order_by('?')

                if allartists:
                    conn = DBInterface()
                    conn.connect()
                    free_artists_int_list = map(
                        lambda x: x.artistID, allartists)
                    free_date_artists_list = map(
                        lambda x: str(x.artistID), allartists)
                    free_artists = ",".join(free_date_artists_list)
                    free_artists = "(" + free_artists + ")"

                    if date_search == 1:
                        free_date = "(" + formated_date + ")"
                        booked_on_free_dates_query = '''
                                            SELECT dj_Master.artistID
                                                FROM dj_Master
                                                JOIN dj_Artists on dj_Master.artistID = dj_Artists.artistID
                                                JOIN dj_Events ON dj_Master.eventID = dj_Events.eventID
                                                WHERE dj_Artists.artistID IN {} AND dj_Events.date IN {};'''.format(free_artists, free_date)
                    else:
                        interval = date_range(dat, dat1)
                        free_dates1 = map(lambda x: "'" + x + "'", interval)
                        free_dates1 = ",".join(free_dates1)
                        free_dates1 = "(" + str(free_dates1) + ")"

                        booked_on_free_dates_query = '''
                                            SELECT dj_Master.artistID FROM dj_Master
                                                JOIN dj_Artists on dj_Master.artistID = dj_Artists.artistID
                                                JOIN dj_Events ON dj_Master.eventID = dj_Events.eventID
                                                WHERE dj_Artists.artistID IN {} AND dj_Events.date IN {} ;'''.format(free_artists, free_dates1)

                    free_artist_dates_tuple = conn.execute_query(
                        booked_on_free_dates_query)

                    booked_on_free_dates_artists = [
                        int(i[0]) for i in free_artist_dates_tuple]

                    booked_on_free_dates_artists = list(
                        set(booked_on_free_dates_artists))
                    free_artists_int_list = list(set(free_artists_int_list))
                    free_artists_int_list = map(
                        lambda x: int(x), free_artists_int_list)

                    # temp code for the removel of the artist who did not have any upcoming event.
                    artist_who_have_upcoming_event = DjMaster.objects.filter(
                        eventid__date__gt=dt.today()).values_list('artistid__artistID', flat=True)
                    artist_who_have_upcoming_event = map(
                        lambda x: int(x), artist_who_have_upcoming_event)
                    available_artists_list = [
                        i for i in free_artists_int_list if i not in booked_on_free_dates_artists]
                    available_artists_list = filter(
                        lambda x: x in artist_who_have_upcoming_event, available_artists_list)

                    free_artists = Artist.objects.filter(
                        artistID__in=available_artists_list)
                    booked_artists = Artist.objects.filter(
                        artistID__in=booked_on_free_dates_artists)

                    if shuffle == 0:
                        free_artists = free_artists.order_by('?')
                        booked_artists = booked_artists.order_by('?')
                    else:
                        free_artists = free_artists.order_by('artistName')
                        booked_artists = booked_artists.order_by('artistName')

                    if popularity:
                        source = Source.objects.filter(
                            sourceID=popularity).get()
                        free_artists = sort_by_popularity(
                            free_artists, source.sourceName.lower())

                    if popularity:
                        source = Source.objects.filter(
                            sourceID=popularity).get()
                        booked_artists = sort_by_popularity(
                            booked_artists, source.sourceName.lower())

                    if prioritize_my_fav == "Yes":
                        free_artists = prioritize_favourtie_artist(
                            request, free_artists)
                        booked_artists = prioritize_favourtie_artist(
                            request, booked_artists)
                        msg += ' > FAVORITE ARTIST'

                    # if popularity or prioritize_my_fav == "Yes":
                    #     f_artist = list(set(free_artists))
                    #     b_artist = list(set(booked_artists))
                    #     allartists = f_artist
                    #     allartists.extend(b_artist)
                    # else:

                    free_artists = list(free_artists)
                    booked_artists = list(booked_artists)

                    sample = free_artists[:]
                    allartists = sample

                    allartists.extend(booked_artists)
                    random.shuffle(allartists)
                else:
                    free_artists = []
                    booked_artists = []

            if date_search == 1:
                single_date = dat
                for artist in booked_artists:
                    upcoming_events = get_upcoming_events_dates(
                        artist, single_date)
                    if upcoming_events:
                        for event in upcoming_events:
                            event_country = event.country
                            free_online_dates.append(
                                {"artistName": artist, "events_country": event_country})
                            break
            else:
                date1 = request.GET.get('date1')
                month = date1.split('/')[0]
                day = date1.split('/')[1]
                year = date1.split('/')[2]
                date1 = dt.strptime(date1, "%m/%d/%Y").strftime("%a %d %b %Y")
                formated_date1 = "'" + year + '-' + month + '-' + day + "'"
                dat1 = dt.date(
                    dt(year=int(year), month=int(month), day=int(day)))
                search_date1 = dat
                search_date2 = dat1
                for artist in booked_artists:
                    for single_date in (search_date1 + timedelta(n) for n in range(int((search_date2-search_date1).days)+1)):
                        upcoming_events = get_upcoming_events_dates(
                            artist, single_date)
                        if upcoming_events:
                            for event in upcoming_events:
                                event_country = event.country
                                free_online_dates.append(
                                    {"artistName": artist, "events_country": event_country})
                                break
                            break

            if request.is_ajax():
                start = request.GET.get('start', '')
                start1 = request.GET.get('start1', '')
                # start2 = request.GET.get('start2', '')
                if start:
                    start = int(start)
                    allartists = free_artists[start:(start+artist_limit)]
                    context = {"free_artists": allartists, 'filter_popularity': popularity,
                               'start': start, 'message': msg, 'favourite_artists': favourite_artists}

                    template = render_to_string(
                        "dates/get_free.html", context, request=request)

                    start = int(start) + artist_limit
                    if len(allartists) < artist_limit:
                        status = "False"  # No More events
                    else:
                        status = "True"  # More events
                    response = {
                        "status": status,
                        "start": start,
                        "data": template,
                    }
                    return HttpResponse(json.dumps(response), content_type="application/json")

                if start1:
                    start1 = int(start1)
                    allartists = booked_artists[start1:(start1+artist_limit)]

                    context = {"booked_artists": allartists, 'filter_popularity': popularity,
                               'start': start1, 'free_online_dates': free_online_dates, 'message': msg, }

                    template = render_to_string(
                        "dates/get_booked.html", context, request=request)

                    start1 = int(start1) + artist_limit
                    if len(allartists) < artist_limit:
                        status = "False"  # No More events
                    else:
                        status = "True"  # More events
                    response = {
                        "status": status,
                        "start": start1,
                        "data": template,
                    }
                    return HttpResponse(json.dumps(response), content_type="application/json")

                # currently we do not need to see this search result for user

                # if start2:
                #     start2 = int(start2)
                #     allartists = allartists[start2:(start2 + artist_limit)]
                #
                #     context = {"allartists": allartists,
                #                "free_artists": free_artists,
                #                "booked_artists": booked_artists,
                #                "message": msg,
                #                }
                #
                #     template = render_to_string("dates/get_both.html", context, request=request)
                #
                #     start2 = int(start2) + artist_limit
                #     if len(allartists) < artist_limit:
                #         status = "False"  # No More events
                #     else:
                #         status = "True"  # More events
                #     response = {
                #         "status": status,
                #         "start": start2,
                #         "data": template,
                #     }
                #     return HttpResponse(json.dumps(response), content_type="application/json")

            if allartists:
                all_artists_id = [artist.artistID for artist in allartists]
            else:
                all_artists_id = None

            if free_artists:
                free_artists_id = [artist.artistID for artist in free_artists]
            else:
                free_artists_id = None

            if booked_artists:
                booked_artists_id = [
                    artist.artistID for artist in booked_artists]
            else:
                booked_artists_id = None
            print("COUNT", len(free_artists))
            print("COUNT", len(allartists))
            print("COUNT", len(booked_artists))
            allartists = allartists[0: entries]
            free_artists = free_artists[0: entries]
            booked_artists = booked_artists[0: entries]

            if (request.GET.get('date')) or (request.GET.get('date1') and request.GET.get('date')):
                search_type = 'DateSearch'
                message = msg
                recentsearch = save_recent_search(
                    request, search_type, message)

            context = {
                'allartists': allartists,
                "free_artists": free_artists,
                "booked_artists": booked_artists,
                'artist_start': artist_limit,
                'bookedartist_start': artist_limit,
                'allartist_start': artist_limit,
                'message': msg,
                'genres': filters()[0],
                'budget': filters()[1],
                'popularity': filters()[2],
                'countries': filters()[3],
                'date': str(date),
                'date_search': str(date_search),
                'all_artists_id': all_artists_id,
                'free_artists_id': free_artists_id,
                'booked_artists_id': booked_artists_id,
                'prioritize_my_fav': prioritize_my_fav,
                'entries': entries,
                'genre': genre,
                'filter_budget': budget,
                'filter_popularity': popularity,
                'filter_country': country_code,
                'shuffle': shuffle,
                'show_filters': True,
                'free_online_dates': free_online_dates,
                'show_loader': True,
            }
            if date_search == 2:
                context.update({'date1': str(date1)})
    else:
        msg = "  "
        context = {'welcome': msg,
                   'genres': filters()[0],
                   'budget': filters()[1],
                   'popularity': filters()[2],
                   'countries': filters()[3]
                   }
    context.update({"favourite_artists": favourite_artists})

    return render(request, 'dates/dates.html', context)
