# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import mailchimp
# import stripe
import pdfkit
import tempfile
import json
import requests

from datetime import datetime

from django.db.models import Q
from django.utils import timezone
from django.conf import settings
from django.shortcuts import render_to_response, redirect, render
from django.template import RequestContext
from django.contrib.auth import authenticate, login, logout
from django.utils.crypto import get_random_string
from django.contrib.sites.shortcuts import get_current_site
from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.template.loader import render_to_string

from .models import ForgetPassword, EmailConfirmation, UserFrom, Applications
from profiles.models import Profile, Subscription, Coupon, AdminSettings, Plan, CardDetails, StripeSubscription, WebhookHistory
from profiles.forms import FrequencyForm
from .forms import LoginForm, ForgetPasswordForm, ResetPasswordForm, RequestAnInviteForm, ChangePasswordForm, CouponForm, CardDetailForm
from geography.models import Country
from accounts.utils import SendEmail, create_username
from artist.models import Artist, Genre
from profiles.models import Coupon
from accounts.constants import USER_TYPES, user_types_list, temp_user_types_list, prod_user_types_list
from accounts.decorators import *
from marketing_pages.models import MarketingEmails
from .AgileCRM import agileCRM
# stripe.api_key = settings.STRIPE_SECRET_KEY

from accounts.decorators import check_email_validated
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth import update_session_auth_hash
from social_django.models import UserSocialAuth
from django.db.models import Q
from .payment_api import StripeAPIView

STRIPE_PLAN_MAP = {
    '1': "Trial",
    '2': "Basic",
    '3': "Premium",
    '4': "Artist/DJ",
    '5': "Promoter",
    '6': "Agent"
}

def convert_unix_time(date):
    return datetime.fromtimestamp(date).strftime('%Y-%m-%d %H:%M:%S')

def login_user(request):
    """
        Function is used for login the registered user in the wdjp.
    """

    if request.user.is_authenticated():
        return redirect(reverse('email_validation_done'))

    email = request.GET.get('email', '')
    next = request.GET.get('next')
    if request.method == "POST" and request.POST["action"] == "sign_in" :
        form = LoginForm(request.POST)

        if form.is_valid():
            profile_obj = Profile.objects.filter(user__email=form.cleaned_data['email']).first()

            is_first_login = User.objects.get(username=profile_obj.user.username).last_login

            user = authenticate(username=profile_obj.user.username, password=form.cleaned_data['password'])

            if user:

                if user.is_active:
                    # user_from  = UserFrom.objects.filter(user=user).first()
                    # if user_from:
                    #     if user_from.user_from == "Wheredjsplay":
                    #         login(request, user)
                    #         return redirect("/")
                    #     else:
                    #         return redirect(reverse('login_user'))
                    login(request, user)
                    

                    try:
                        custom_user_type = profile_obj.user_types
                        if profile_obj.user_types == "Artists/DJs":
                            custom_user_type = 'DJ'

                        contact_data = {
                            "properties":[
                                {
                                    "type": "SYSTEM",
                                    "name": "first_name",
                                    "value": user.first_name
                                },

                                {
                                    "type": "SYSTEM",
                                    "name": "last_name",
                                    "value": user.last_name
                                },

                                {
                                    "type": "SYSTEM",
                                    "name": "email",
                                    "value": user.email
                                },

                                {
                                    "type": "CUSTOM",
                                    "name": custom_user_type,
                                    "value": custom_user_type
                                },

                            ],

                            "tags": [
                                "%s_Signin"%(custom_user_type),
                            ],

                        }

                        agile = agileCRM("contacts","POST",contact_data,"application/json")

                        agile_result = json.dumps(agile)

                        if "Sorry" in agile_result :

                            agile =  agileCRM("search?q={}&page_size=10&type='PERSON'".format(user.email),"GET",None,"application/json")
                            agile_obj =  json.loads(agile)

                            id = agile_obj[0]["id"]

                            update_contact_data = {
                                "id": id,
                                "properties":[
                                    {
                                        "type": "SYSTEM",
                                        "name": "first_name",
                                        "value": user.first_name
                                    },

                                    {
                                        "type": "SYSTEM",
                                        "name": "last_name",
                                        "value": user.last_name
                                    },

                                    {
                                        "type": "CUSTOM",
                                        "name": custom_user_type,
                                        "value": custom_user_type
                                },

                                ],

                            }

                            update_tag_value = {
                                "id": id,
                                "tags": [
                                    "%s_Signin"%(custom_user_type),
                                ]
                            }

                            agileCRM("contacts/edit/tags","PUT",update_tag_value,"application/json")

                            agile = agileCRM("contacts/edit-properties","PUT",update_contact_data,"application/json")

                    except:
                        pass

                    if next:
                        return after_login(user, next)
                    else:
                        return after_login(user, next=None)

                messages.add_message(request, messages.ERROR, "Please check your email and verify the email address to log on to Djs.")

                return redirect(reverse('login_user'),site_key=settings.RECAPTCHA_SITE_KEY)

            error_message = "Sorry-  Invalid Password - if you would like to reset click - forgot your password"

            return render_to_response("accounts/login.html",{
                "form": form,
                "error_message": error_message,
                "next": request.POST.get('next', ''),
            }, context_instance=RequestContext(request))

        return render_to_response("accounts/login.html",{
            "form": form,
            "next": request.POST.get('next', ''),
        }, context_instance=RequestContext(request))

    # if request.method == "POST" and request.POST["action"] == "sign_up" :
    #     form = LoginForm()
    #     signupemailform = SignUpEmailForm(request.POST)
    #     if signupemailform.is_valid():
    #         name = signupemailform.cleaned_data['name']
    #         email = signupemailform.cleaned_data['email']
    #         password = signupemailform.cleaned_data['password']
    #         user = User.objects.create_user(
    #             username=create_username('%s'%(name)),
    #             email=email,
    #             password=password
    #         )
    #         user.first_name = name
    #         user.save()
    #
    #         return redirect(reverse("login_user"))
    #
    #     return render_to_response("accounts/login.html",{
    #         "form": form,
    #         "signupemailform": signupemailform,
    #     }, context_instance=RequestContext(request))
    #
    else:
        if email:
            form = LoginForm(initial={'email': email})
        else:
            form = LoginForm()
            # signupemailform = SignUpEmailForm()

        return render_to_response("accounts/login.html", {
            # "signupemailform": signupemailform,
            "form": form,
            "next": request.GET.get('next', ''),
        }, context_instance=RequestContext(request))


def logout_user(request):
    """
        Function is used to logout the user from the Peekhi.
    """

    logout(request)
    return redirect(reverse('index'))


def forget_password(request):
    """
        Function is used for the forgot password functionality.
        In this password is resetted by sending a mail to registered users.
        Function also maintains a token for resetting the password which is
        later used to identify the user.
    """

    if request.user.is_authenticated():
        return redirect("/")

    form = ForgetPasswordForm(request.POST or None)

    if request.method == "POST":
        if form.is_valid():
            user_obj = ForgetPassword.objects.create(
                email=form.cleaned_data["email"],
                token = get_random_string(length=10)
            )

            msg = SendEmail(request)
            msg.send_by_template([user_obj.email], "accounts/forget_password_email.html",
                context={
                    'reset_link': settings.SITE_URL+"/accounts/reset-password/{}".format(user_obj.token)
                },
                subject = "Reset Password!"
            )
            return render_to_response("accounts/forget_password_step.html", {}, context_instance=RequestContext(request))

    return render_to_response("accounts/forget_password.html", {
        "form": form
    }, context_instance=RequestContext(request))

def confirm_set_password(request):
    return render(request, "accounts/confirm_set_password.html")

def reset_password(request, token):
    """
        Function is used to reset the password of the user using the token
        identification which was created in forgot password.
    """
    if request.user.is_authenticated():
        return redirect("/")

    if not ForgetPassword.objects.filter(token=token).filter(is_used=True).exists():
        messages.add_message(request, messages.ERROR, "This token has been expired.")
        return redirect(reverse("login_user"))

    form = ResetPasswordForm(request.POST or None)
    token_obj = ForgetPassword.objects.get(token=token)
    user_obj = Profile.objects.get(user__email=token_obj.email)

    if form.is_valid():
        new_password = form.cleaned_data['new_password']
        user_obj.user.set_password(new_password)
        user_obj.user.save()

        token_obj.is_used = False
        token_obj.save()
        return redirect(reverse('confirm_set_password'))

    return render_to_response("accounts/reset_password.html", {
        "form": form
    }, context_instance=RequestContext(request))


@login_required
@check_email_validated
def plans(request):
    context = {}
    if request.user.is_authenticated():
        user = request.user
        profile = Profile.objects.filter(user=request.user).first()
        if not profile.stripe_plan:
            trial_period_days = AdminSettings.objects.get(pk=1).free_trial
            if request.method == "POST":
                if "free_trial" in request.POST:
                    plan = Plan.objects.get(id=request.POST["free_trial"])
                    profile.stripe_plan = plan
                    profile.is_trial_done = True
                    stripe_obj = StripeAPIView()
                    subscription_id = stripe_obj.create_subscription(
                        trial_period_days=trial_period_days,
                        customer_id=profile.stripe_customer_id,
                        plan_id=plan.stripe_plan_id,
                        coupon=None)
                    if subscription_id["status"] == 200:
                        subscription = StripeSubscription.objects.create(stripe_subscription_id=subscription_id["subscription_id"], start_time=convert_unix_time(subscription_id["current_period_start"]), end_time=convert_unix_time(subscription_id["current_period_end"]))
                        profile.subscription = subscription
                        profile.save()
                        messages.add_message(request, messages.SUCCESS, "Trial Plan Selected.")
                        return redirect(reverse("subscription_checkout_thanks"))
                else:
                    plan = Plan.objects.get(id=request.POST["plan"])
                    profile.stripe_plan = plan
                    profile.is_trial_done = False
                    messages.add_message(request, messages.SUCCESS, "Choosed Plan.")
                profile.save()
                return redirect(reverse('checkout_user_types')+ "?plan_id={}".format(plan.id))
            context["plans"] = Plan.objects.filter(user_type=profile.user_types)
            context["free_trial"] = trial_period_days
            context["has_subscription"] = profile.subscription
            return render_to_response("accounts/plans_user_types.html", context, context_instance=RequestContext(request))
        if ((not profile.is_trial_done) and (profile.is_subscription_failed or profile.carddetails_set.all().count() < 1)) or (profile.subscription.is_refund) or not profile.subscription.is_active:
            return redirect(reverse('checkout_user_types')+ "?plan_id={}".format(profile.stripe_plan.id))
        return redirect(reverse('my_membership'))
    return redirect(reverse('login_user'))


@login_required
@check_email_validated
@check_plan('Artist/DJ', 'Promoter', 'Agent')
def refund(request):
    context = {}
    user =  request.user
    current_time =  timezone.now()
    # subscription_obj = Subscription.objects.filter(user=user).first()
    profile = Profile.objects.get(user=request.user)

    if not profile.is_trial_done and not profile.subscription.is_refund:
        if profile.subscription:
            sub_created_date = profile.subscription.start_time
            plan = profile.stripe_plan
            diff = current_time - sub_created_date

            if diff:
                left_time = timezone.timedelta(days=15) - diff

                context.update({
                        "left_time":str(left_time)[:6],
                    })

                if diff >= timezone.timedelta(days=15):
                    context.update({
                        "time_limit_exceded":True,
                    })

            context.update({
                "subscription_obj": profile.subscription,
                "plan":plan,
                "sub_created_date":sub_created_date,
            })
        return render(request, "accounts/refund.html", context)
    return redirect(reverse('my_membership'))


@login_required
@check_email_validated
def confirm_refund(request):
    context = {}
    charge = None
    user = request.user
    current_time =  timezone.now()
    # subscription = Subscription.objects.filter(user=user, start_time__lte=current_time)
    # subscription = Subscription.objects.filter(user=user)
    # subscription_obj = Subscription.objects.filter(user=user).first()
    profile = Profile.objects.get(user=request.user)
    if not profile.is_trial_done:
        sub_created_date =  profile.subscription.start_time
        plan = profile.stripe_plan
        is_refund = profile.subscription.is_refund

        context.update({
            "sub_created_date":sub_created_date,
            "plan": plan,
            "is_refund":is_refund
        })

        if profile.subscription:
            stripe_obj = StripeAPIView()
            invoice = stripe_obj.get_invoice_list(profile.stripe_customer_id)
            charge = invoice["charge"]
            if invoice["status"] == 200:
               #  messages.add_message(request, messages.ERROR, "Please Try Again.")
                if not is_refund :
                    refund_stripe = stripe_obj.refund(charge)
                    cancel_sub = stripe_obj.delete_subscription(profile.subscription.stripe_subscription_id)
                    profile.subscription.is_refund = True
                    profile.subscription.refund_date = timezone.now()
                    profile.subscription.is_active = False
                    profile.stripe_plan = None
                    profile.save()
                    profile.subscription.save()
                    card_primary = CardDetails.objects.filter(user=profile, is_primary=True)
                    for temp_card in card_primary:
                        temp_card.is_primary = False
                        temp_card.save()
                    
                    context.update({
                        "refund":True,
                        "refunded_on": profile.subscription.refund_date,
                    })


        return render(request, "accounts/confirm_refund.html", context)
    return redirect(reverse('my_membership'))


@login_required
@check_email_validated
def checkout_user_types(request):
    """
        Function is used for the Payment Gateway of WhereDjs Play
    """
    publishable = settings.STRIPE_PUBLISHABEL_KEY
    form = CardDetailForm()
    user = Profile.objects.filter(user=request.user).first()
    if request.method == "POST":
        stripe_obj = StripeAPIView()
        user = Profile.objects.filter(user=request.user).first()
        customer_id = user.stripe_customer_id

        card_id = stripe_obj.create_card(
            customer_id = customer_id,
            token=request.POST["stripeToken"])

        coupon = None
        try:
            coupon = Coupon.objects.get(coupon_code=request.POST["coupon"]).stripe_coupon_code
        except:
            pass
        if card_id["status"] == 200:
            if user.is_subscription_failed:

                card_id = stripe_obj.make_primary(
                    customer_id= customer_id,
                    card_id = card_id["card_id"])

                if card_id["status"] == 200:
                    card_primary = CardDetails.objects.filter(user=user, is_primary=True)
                    for temp_card in card_primary:
                        temp_card.is_primary = False
                        temp_card.save()
                    CardDetails.objects.create(
                        card_number=request.POST["last4"],
                        user=user,
                        holder_name=request.POST["name"],
                        card_type=request.POST["brand"],
                        expiry_month=request.POST["exp_month"],
                        expiry_year=request.POST["expiryYear"],
                        is_primary=True)
                    user.is_subscription_failed = False
                    user.subscription.is_active = True
                    user.subscription.save()
                    user.save()
                    return redirect(reverse("subscription_checkout_thanks"))
            if ((user.subscription) and (not user.is_trial_done) and (not user.carddetails_set.all().count() > 0)):
                subscription_id = stripe_obj.end_trail_subscription(user.subscription.stripe_subscription_id)
                if subscription_id["status"] == 200:
                    user.subscription.stripe_subscription_id = subscription_id["subscription_id"]
                    user.subscription.start_time = convert_unix_time(subscription_id["current_period_start"])
                    user.subscription.end_time = convert_unix_time(subscription_id["current_period_end"])
                    user.subscription.is_active = True
                    user.subscription.save()
                    user.save()
                else:
                    context = {'user_logged': user}
                    body = render_to_string(
                        'accounts/email/failed-subscription.html', context)
                    # SG_send_mail(
                    #     settings.USER_FAILUER_EMAIL,
                    #     constants.FAILED_SUBSCRITION_SUBJECT,
                    #     body)
            
            else:
                subscription_id = stripe_obj.create_subscription(
                    trial_period_days=None,
                    customer_id=user.stripe_customer_id,
                    plan_id=user.stripe_plan.stripe_plan_id,
                    coupon=coupon)
                if subscription_id["status"] == 200:
                    subscription = StripeSubscription.objects.create(stripe_subscription_id=subscription_id["subscription_id"], start_time=convert_unix_time(subscription_id["current_period_start"]), end_time=convert_unix_time(subscription_id["current_period_end"]))
                    user.subscription = subscription
                    user.save()
                else:
                    context = {'user_logged': user}
                    body = render_to_string(
                        'accounts/email/failed-subscription.html', context)
                    # SG_send_mail(
                    #     settings.USER_FAILUER_EMAIL,
                    #     constants.FAILED_SUBSCRITION_SUBJECT,
                    #     body)
            card_primary = CardDetails.objects.filter(user=user, is_primary=True)
            for temp_card in card_primary:
                temp_card.is_primary = False
                temp_card.save()
            card = CardDetails.objects.create(
                card_number=request.POST["last4"],
                user=user,
                holder_name=request.POST["name"],
                card_type=request.POST["brand"],
                expiry_month=request.POST["exp_month"],
                expiry_year=request.POST["expiryYear"],
                is_primary=True)
            card.stripe_card_id = card_id["card_id"]
            card.save()

            current_date = timezone.now()
            plan_name = user.stripe_plan.user_type
            context = {
                'username': '{} {}'.format(request.user.first_name, request.user.last_name),
                'site_url': settings.SITE_URL,
                'current_date': current_date,
                'plan_name': user.stripe_plan.user_type,
                'user_email': request.user.email,
            }

            if coupon:
                coupon_details = Coupon.objects.get(coupon_code=request.POST["coupon"])
                context.update({
                    'coupon_code':coupon_details.stripe_coupon_code, 
                })

                if (user.stripe_plan.amount - coupon_details.amount_off) >= 0:
                    context.update({
                        'coupon_amount': coupon_details.amount_off,
                        'final_amount': "$%s.00/m"% (user.stripe_plan.amount - coupon_details.amount_off)
                    })
                else:
                    context.update({
                        'coupon_amount': user.stripe_plan.amount,
                        'final_amount': "$%s.00/m"% (0)
                    })


            context.update({
                "transaction_rate":"$%s.00/m"% (user.stripe_plan.amount)
            })

            options ={
                'page-size': 'A4',
                'margin-top': '0.50in',
                'margin-right': '0.50in',
                'margin-bottom': '0.50in',
                'margin-left': '0.50in',
            }

            start_time = timezone.now()
            file_name = 'WDJP-Payment-slip-%s_' % (str(start_time)[0:10])
            tmp_file = tempfile.NamedTemporaryFile(mode='wb', prefix=file_name, suffix=".pdf", delete="False")
            tmp_file.close()

            salary_pdf = render_to_string('accounts/checkout_pdf.html', context, context_instance=RequestContext(request))
            pdfkit.from_string(salary_pdf, tmp_file.name, options=options)

            msg = SendEmail(request, file=[tmp_file.name])

            msg.send_by_template([request.user.email], "accounts/checkout_email.html",
            context={
                'user': request.user,
                'site_url': settings.SITE_URL,
                'login_url': settings.LOGIN_URL,
                'plan': plan_name,
                'full_name': request.user.get_full_name(),
                'renewal_date': user.subscription.end_time,
            },
            subject = "WDJP Payment Slip!"
            )

            if plan_name :
                try:
                    agile =  agileCRM("search?q={}&page_size=10&type='PERSON'".format(request.user.email),"GET",None,"application/json")
                    agile_obj =  json.loads(agile)

                    id = agile_obj[0]["id"]

                    update_contact_data = {
                        "id": id,
                        "properties":[
                            {
                                "type": "SYSTEM",
                                "name": "email",
                                "value": request.user.email
                            },

                            {
                                "type": "CUSTOM",
                                "name": "StripeID",
                                "value": customer_id
                            },

                        ],

                    }

                    update_tag_value = {
                        "id": id,
                        "tags": [
                            "Paid",
                        ]
                    }

                    agile = agileCRM("contacts/edit-properties","PUT",update_contact_data,"application/json")

                    agileCRM("contacts/edit/tags","PUT",update_tag_value,"application/json")
                except:
                    pass

            return redirect(("subscription_checkout_thanks"))
        else:
            return render(request, "accounts/checkout.html",
                {"form": form,
               "stripe_key": publishable,
               "plan":user.stripe_plan,
               "message":card_id["message"]})
    if user.stripe_plan and user.subscription and user.subscription.is_active and not user.carddetails_set.all().count() < 1 and not user.is_trial_done and not user.is_subscription_failed:
    # if user.stripe_plan and not user.subscription.is_refund and not user.carddetails_set.all().count() < 1 and user.subscription.is_active:
        return redirect(reverse('my_membership'))
    plan_id = request.GET.get('plan_id', None)
    if plan_id is None:
        return redirect(reverse('plans'))

    plan = Plan.objects.get(pk=plan_id)
    return render(request, "accounts/checkout.html",
          {"form": form,
           "stripe_key": publishable,
           "plan":plan})


# @login_required
# def checkout(request):
# 	"""
# 		Function is used for the Payment Gateway of WhereDjs Play
# 	"""
# 	plan_id = str(request.GET.get('plan_id', ''))
# 	if not plan_id or plan_id not in ["2", "3"]:
# 		messages.add_message(request, messages.SUCCESS, "Invalid Plan")
# 		return redirect(reverse('plans'))

# 	if request.method == "POST":
# 		stripeToken = request.POST.get('stripeToken', '')
# 		plan_id = request.POST.get('plan_id', '')
# 		stripe_new = None
# 		plan_name = STRIPE_PLAN_MAP[plan_id]
# 		profile_obj = Profile.objects.get(user=request.user)

# 		if not plan_id or plan_id not in ["2", "3"]:
# 			messages.add_message(request, messages.SUCCESS, "Invalid Plan")
# 			return redirect(reverse('plans'))

# 		if plan_name == "Basic" and profile_obj.is_trial_done == False:
# 			plan_name = "Trial"
# 			plan_id = "1"

# 		if plan_name == "Trial" and (profile_obj.is_trial_done or profile_obj.plan == "Trial"):
# 			messages.add_message(request, messages.SUCCESS, "Trial Plan Is Already Taken")
# 			return redirect(reverse('plans'))
# 		else:
# 			if Subscription.objects.filter(user=request.user).exists():
# 				subscription_obj = Subscription.objects.get(user=request.user)

# 				# stripe_obj = stripe.Customer.retrieve(subscription_obj.customer_id)

# 				sub_id = stripe_obj.subscriptions['data'][0]['id']
# 				# stripe_sub = subscription = stripe.Subscription.retrieve(sub_id)
# 				stripe_sub.plan = plan_id
# 				stripe_sub.save()

# 				subscription_obj.plan = plan_name
# 				if plan_name == "Trial":
# 					subscription_obj.start_time = timezone.now()
# 					subscription_obj.end_time = timezone.now() + timezone.timedelta(days=15)
# 				else:
# 					subscription_obj.start_time = timezone.now()
# 					subscription_obj.end_time = timezone.now() + timezone.timedelta(days=30)
# 				subscription_obj.save()
# 			else:
# 				pass
# 				# plan = stripe.Plan.retrieve(plan_id)

# 				# stripe_new = stripe.Customer.create(
# 				#     source=stripeToken, # obtained from Stripe.js
# 				#     email=request.POST.get('email', ''),
# 				#     plan = plan,
# 				# )

# 			if stripe_new:
# 				subscription_obj = Subscription.objects.create(
# 					user=request.user,
# 					start_time=timezone.now(),
# 					end_time=timezone.now() + timezone.timedelta(days=30),
# 					customer_id=stripe_new.id,
# 					plan=plan_name,
# 				)
# 				if plan_name == "Trial":
# 					subscription_obj.end_time = timezone.now() + timezone.timedelta(days=15)
# 					subscription_obj.save()

# 			profile_obj = Profile.objects.filter(user=request.user)
# 			if plan_name == "Trial":
# 				profile_obj.update(
# 					is_trial_end_time= timezone.now() + timezone.timedelta(days=15),
# 					plan=plan_name,
# 				)
# 			else:
# 				profile_obj.update(
# 					is_trial_done=True,
# 					is_trial_end_time= timezone.now() + timezone.timedelta(days=30),
# 					plan=plan_name,
# 				)

# 			if plan_name == "Trial" or plan_name == "Basic":
# 				list = mailchimp.utils.get_connection().get_list_by_id(settings.MAILCHIMP_BASIC_LIST)
# 				list.subscribe(profile_obj[0].user.email, {'EMAIL': profile_obj[0].user.email, 'FNAME': profile_obj[0].user.first_name, 'LNAME': profile_obj[0].user.last_name})
# 			elif plan_name == "Premium":
# 				list = mailchimp.utils.get_connection().get_list_by_id(settings.MAILCHIMP_PREMIUM_LIST)
# 				list.subscribe(profile_obj[0].user.email, {'EMAIL': profile_obj[0].user.email, 'FNAME': profile_obj[0].user.first_name, 'LNAME': profile_obj[0].user.last_name})

# 			return redirect(reverse("checkout_thanks"))

# 	publishable = settings.STRIPE_PUBLISHABEL_KEY
# 	return render_to_response("accounts/checkout.html", {
# 		'publishable': publishable,
# 	}, context_instance=RequestContext(request))


def custom_page_not_found(request):
    return render(request, 'accounts/404.html', {})


def invitation_thanks(request):
    context = {}

    print (request.session.get('coupon_success'))
    if request.session.get('coupon_success'):
        try:
            coupon = Coupon.objects.get(id=request.session.get('coupon_success'))
            context.update({"coupon":coupon})
        except:
            pass
    try:
        if request.session['tmp_user_id']:
            user_id = request.session['tmp_user_id']
            user = User.objects.get(id=user_id)
            context.update({"user":user})
    except:
        user = None
        context.update({"user":user})

    return render_to_response(
        'accounts/invitation_thanks.html',
            context,
        context_instance=RequestContext(request)
    )


@login_required
def type_of_user(request):
    user_types = USER_TYPES
    user_types_list = []
    for types in user_types:
        user_types_list.append(types[0])
    if request.method == "POST":
        user = request.user
        user_type = request.POST.get("user_type",)
        pro_obj = Profile.objects.get(user=user)
        pro_obj.user_types = user_type
        pro_obj.save()
        return redirect(reverse("marketing_dashboard"))

    return render(request, "accounts/type_of_user.html", {"user_types":user_types_list})


@login_required
def promoter_1(request):
    return render(request, "accounts/promoter_1.html", {})


@login_required
def select_genre(request):
    """
        This for selecting the Genres
    """

    if request.method == "POST":
        genres_id = request.POST.getlist('genres', '')
        if genres_id:
            genres_id = [int(id) for id in genres_id]
            profile = Profile.objects.get(user=request.user)
            for id in genres_id:
                profile.genres.add(Genre.objects.get(genreID=id))
            profile.save()
        return redirect(reverse("promoter_3"))

    profile = Profile.objects.get(user=request.user)
    profile_genres = profile.genres.all().values_list('genreID', 'genreName')
    genres = Genre.objects.all().values_list('genreID', 'genreName')

    context = {
        'genres': genres,
        'profile_genres': profile_genres,
    }

    return render(request, "accounts/select_genre.html", context)

@login_required
def promoter_3(request):
    return render(request, "accounts/promoter_3.html", {})

@login_required
def promoter_4(request):
    return render(request, "accounts/promoter_4.html", {})

@login_required
def promoter_5(request):
    """
        This for Shortlisting the Artists
    """
    if request.method == "POST":
        shortlist_artist_id = request.POST.getlist('shortlist_artists', '')
        if shortlist_artist_id:
            shortlist_artist_id = [int(id) for id in shortlist_artist_id]
            profile = Profile.objects.get(user=request.user)
            for id in shortlist_artist_id:
                profile.shortlist_artist.add(Artist.objects.get(artistID=id))
            profile.save()
        return redirect("/")

    profile = Profile.objects.get(user=request.user)
    all_user_shortlist_artist = profile.shortlist_artist.all().values_list('artistID', 'artistName')
    artists = Artist.objects.all().values_list('artistID', 'artistName')

    context = {
        'artists': artists,
        'all_user_shortlist_artist': all_user_shortlist_artist,
    }
    return render(request, 'accounts/promoter_5.html', context)


def request_an_invite(request):
    if request.user.is_authenticated():
        return redirect(reverse('artists'))
    context = {}
    form = RequestAnInviteForm(request.POST or None, request.FILES)
    countries = Country.objects.all().order_by("name")

    if request.method == "POST":

        # captcha verification
        data = {
            'response': request.POST.get('g-recaptcha-response',''),
            'secret': settings.RECAPTCHA_SECRET_KEY
        }
        resp = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
        result_json = resp.json()
        if not result_json.get('success'):
            return render_to_response("accounts/request_an_invite.html", {
                    "form": form,
                    "next": request.GET.get('next', ''),
                    "is_robot":True,
                    'site_key': settings.RECAPTCHA_SITE_KEY
                    }, context_instance=RequestContext(request))

        if form.is_valid():
            user = User.objects.filter(email=form.cleaned_data["email"]).first()
            if user:
                application = Applications.objects.get(application_name="WhereDjsPlay")
                user_from, created = UserFrom.objects.get_or_create(user=user)
                user_from.user_from.add(application)
                user_from.save()

                profile, created = Profile.objects.get_or_create(user=user)
                user_type = request.POST.get("user_type", None)
                if user_type:
                    profile.user_types = user_type
                profile.save()

            else:
                user = User.objects.create_user(
                    username=create_username('%s' % (form.cleaned_data["first_name"])),
                    email=form.cleaned_data['email'],
                    password=form.cleaned_data['confirm_password'],
                    is_active = True,
                )

                first_name = form.cleaned_data['first_name']
                name = first_name.split(" ")
                first_name = name[0]
                user.first_name = first_name

                try:
                    last_name = name[1:]
                    last_name = " ".join(last_name)
                    user.last_name = last_name
                except:
                    pass

                user.save()
                user_type = request.POST.get("user_type", None)
                received_coupon = form.cleaned_data["coupon"]
                if received_coupon:
                    coupon = Coupon.objects.get(coupon_code=received_coupon)
                else:
                    coupon = None

                profile = Profile.objects.get_or_create(user=user)[0]
                profile.mobile_no = form.cleaned_data['mobile_no']
                # user_types = request.POST.getlist('user_types', '')
                # user_types = (",").join(user_types)
                # profile.user_types = user_types
                profile.is_verified = True

                # application = Applications.objects.filter(application_name="WhereDjsPlay").first()
                #
                # print "application +++++++++++++++++++++++++++++++++++"
                # print application
                #
                # user_from, created = UserFrom.objects.get_or_create(user=user)
                # if created:
                #     user_from.user_from = application
                #     user_from.save()


                if user_type:
                    profile.user_types = user_type
                # profile.nickname = form.cleaned_data['nick_name']

                if coupon:
                    profile.plan = "Trial"
                    profile.is_trial_end_time = timezone.now() + timezone.timedelta(days=coupon.coupon_time)
                profile.save()

                email_confirmation = EmailConfirmation.objects.create(email=user.email, token=get_random_string(length=10), is_latest=True)

                email = SendEmail(request)

                # email.send_by_template([user.email], "profiles/email/approved_email.html",
                #     context={
                #         'user': user,
                #         'login_url': settings.SITE_URL + '/accounts/login-user/',
                #         'profile': profile,
                #         'site_url': settings.SITE_URL,
                #     },
                #     subject = "Welcome to Wheredjsplay.com"
                # )


                # Email which is being sent to admin on every new Signup .

                admin_email = SendEmail(request)

                email.send_by_template(settings.DEFAULT_ADMIN_EMAILS, "profiles/email/admin_notify.html",
                    context={
                        'profile': profile,
                        'profile_url': settings.SITE_URL + '/admin/profiles/profile/{}/change/'.format(profile.id),
                        'user_url': settings.SITE_URL + '/admin/auth/user/{}/change/'.format(user.id),
                        'site_url': settings.SITE_URL,
                        'user_type': profile.user_types,
                        'date': datetime.now().date(),
                        'time': datetime.now().time(),
                    },
                    subject = "New User Applied For the Invitation"
                )

                email_obj = MarketingEmails.objects.filter(email_type="welcome_and_sign_up").first()

                site_url = settings.SITE_URL
                url = site_url+"/accounts/validate-email/"+email_confirmation.email+"/"+email_confirmation.token
                content = email_obj.email_content
                content = content.replace("{username}", user.first_name)
                content = content.replace("{user_type}", profile.user_types)
                content = content.replace('src="', 'src="'+site_url+'')
                a_url = ("<html><a href="+ url +">click here</a></html>")
                content = content.replace("{url}", a_url)

                user_email = SendEmail(request)
                email.send_by_template([user.email,],"marketing_pages/not_loged_5.html",
                    context={
                        "content": content,
                        "site_url":site_url
                    },
                    subject = email_obj.email_subject
                )

                # validate_email = MarketingEmails.objects.filter(email_type="validate_email").first()

                # if validate_email:
                #     url = site_url+"/accounts/validate-email/"+email_confirmation.email+"/"+email_confirmation.token
                #     content = validate_email.email_content
                #     content = content.replace("{username}", user.first_name.title())
                #     content = content.replace("{user_type}", profile.user_types)
                #     content = content.replace('src="', 'src="'+site_url+'')
                #     # content = content.replce("{token}", email_confirmation.token)
                #     a_url = ("<html><a href="+ url +">click here</a></html>")
                #     content = content.replace("{url}", a_url)

                #     email.send_by_template([user.email,],"marketing_pages/not_loged_5.html",
                #         context={
                #             "content": content,
                #             "site_url":site_url
                #         },
                #         subject = email_obj.email_subject
                #     )

                email_confirmation_list = EmailConfirmation.objects.filter(email=user.email).exclude(token=email_confirmation.token)
                for email_confirm_obj in email_confirmation_list:
                    email_confirm_obj.is_latest = False
                    email_confirm_obj.save()

                    # email_confirmation.is_latest = True
                    # email_confirmation.save()

                # list = mailchimp.utils.get_connection().get_list_by_id(settings.MAILCHIMP_BASIC_LIST)
                # list.subscribe(user.email, {'EMAIL': user.email, 'FNAME': user.first_name, 'LNAME': user.last_name})
                try:
                    custom_user_type = profile.user_types
                    if profile.user_types == "Artists/DJs":
                        custom_user_type = 'DJ'

                    contact_data = {
                        "properties":[
                            {
                                "type": "SYSTEM",
                                "name": "first_name",
                                "value": user.first_name
                            },

                            {
                                "type": "SYSTEM",
                                "name": "last_name",
                                "value": user.last_name
                            },

                            {
                                "type": "SYSTEM",
                                "name": "email",
                                "value": user.email
                            },

                            {
                                "type": "CUSTOM",
                                "name": custom_user_type,
                                "value": custom_user_type
                            },

                        ],

                        "tags": [
                            "%s_Signup"%(custom_user_type),
                        ],

                    }

                    agile = agileCRM("contacts","POST",contact_data,"application/json")

                    agile_result = json.dumps(agile)

                    if "Sorry" in agile_result :

                        agile =  agileCRM("search?q={}&page_size=10&type='PERSON'".format(user.email),"GET",None,"application/json")

                        agile_obj =  json.loads(agile)

                        id = agile_obj[0]["id"]

                        update_contact_data = {
                            "id": id,
                            "properties":[
                                {
                                    "type": "SYSTEM",
                                    "name": "first_name",
                                    "value": user.first_name
                                },

                                {
                                    "type": "SYSTEM",
                                    "name": "last_name",
                                    "value": user.last_name
                                },

                                {
                                "type": "CUSTOM",
                                    "name": custom_user_type,
                                    "value": custom_user_type
                                },

                            ],

                        }

                        update_tag_value = {
                            "id": id,
                            "tags": [
                                "%s_Signup"%(custom_user_type),
                            ]
                        }

                        agileCRM("contacts/edit/tags","PUT",update_tag_value,"application/json")

                        agile = agileCRM("contacts/edit-properties","PUT",update_contact_data,"application/json")
                except:
                    pass

                request.session['tmp_user_id'] = user.id
                if coupon:
                    request.session['coupon_success'] = coupon.id

            user_authenticate = authenticate(username=user.username, password=form.cleaned_data['confirm_password'])


            if user_authenticate:
                if user_authenticate.is_active:
                    login(request, user_authenticate)
                    return after_login(user_authenticate, next=None)

            # return redirect(reverse('thanks'))
    else:
        form = RequestAnInviteForm()

    try:
        from WDJP_ENV import *
        if ENV_NAME == "PRODUCTION":
            context.update({"user_types_list":prod_user_types_list})
        else:
            context.update({"user_types_list":temp_user_types_list})
    except:
        context.update({"user_types_list":temp_user_types_list})

    context.update({
        "form":form,
        "countries":countries,
        "site_key": settings.RECAPTCHA_SITE_KEY
    })

    return render_to_response("accounts/request_an_invite.html", context, context_instance=RequestContext(request))

@login_required
def email_not_validated(request):
    request_url = request.META.get('HTTP_REFERER')
    user = request.user
    profile = Profile.objects.filter(user=user).first()
    if not profile.is_email_validated:
        context = {}
        return render(request, "accounts/email_not_validated.html", context)
    else:
        messages.add_message(request, messages.SUCCESS, "This Email is already Validated")
        return redirect("/")

@login_required
def validate_email(request, email, token):
    email_confirmation = EmailConfirmation.objects.filter(email=email, token=token).first()
    if email_confirmation:
        if email_confirmation.is_latest:
            user = User.objects.filter(email=email).first()
            if user:
                profile = Profile.objects.filter(user=user).first()
                if profile:
                    profile.is_email_validated = True
                    '''Create customer at stripe'''
                    stripe_obj = StripeAPIView()
                    response = stripe_obj.create_customer(email)
                    if response["status"] == 200:
                        profile.stripe_customer_id = response["customer_id"]
                    else:
                        email = SendEmail(request)
                        site_url = settings.SITE_URL
                        admins = User.objects.filter(is_superuser=True)
                        admin_emails = [admin.email for admin in admins]

                        email.send_by_template(admin_emails,"accounts/stripe_customer_failure_create.html",
                            context={
                                "profile": profile,
                                "site_url":site_url
                            },
                            subject = "Stripe customer Create failure"
                        )
                    if profile.login_count == 0:
                        profile.save()
                        messages.add_message(request, messages.SUCCESS, "Your Email has been Validated.")
        else:
            messages.add_message(request, messages.INFO, "clicked link has been expired, Please click on the latest generated link sent to your email!")

    return redirect(reverse("login_user"))


def generate_confirmation_email(request):
    request_url = request.META.get('HTTP_REFERER')
    email = SendEmail(request)
    user = request.user
    profile = Profile.objects.filter(user=user).first()
    site_url = settings.SITE_URL

    if not profile.is_email_validated :
        email_confirmation = EmailConfirmation.objects.create(email=user.email, token=get_random_string(length=10), is_latest=True)
        validate_email = MarketingEmails.objects.filter(email_type="validate_email").first()

        if validate_email:
            url = site_url+"/accounts/validate-email/"+email_confirmation.email+"/"+email_confirmation.token
            content = validate_email.email_content
            content = content.replace("{username}", user.first_name)
            # content = content.replace("{user_type}", profile.user_types)
            # content = content.replace('src="', 'src="'+site_url+'')
            # content = content.replce("{token}", email_confirmation.token)
            # content = content.replace("{url}", url)
            a_url = ("<html><a href="+ url +">click here</a></html>")
            content = content.replace("{url}", a_url)

            try:
                email.send_by_template([user.email],"marketing_pages/not_loged_5.html",
                    context={
                        "content": content,
                        "site_url":site_url
                    },
                    subject = validate_email.email_subject
                )
                messages.add_message(request, messages.SUCCESS, "New Confirmation Email has been sent to your Registered Email")
            except:
                messages.add_message(request, messages.SUCCESS, "Please Try Again")

        email_confirmation_list = EmailConfirmation.objects.filter(email=user.email).exclude(token=email_confirmation.token)
        for email_confirm_obj in email_confirmation_list:
            email_confirm_obj.is_latest = False
            email_confirm_obj.save()

        # return redirect(request_url)
        return redirect("/")

    else:
        messages.add_message(request, messages.SUCCESS, "This Email is already Validated")
        return redirect('/')


def change_password(request):
    """
        Function is used to change the password of the user.
    """
    user = request.user
    if not request.user.is_authenticated():
        return redirect("/")

    form = ChangePasswordForm(request.user,(request.POST or None))
    if form.is_valid():
        new_password = form.cleaned_data['new_password']
        user.set_password(new_password)
        user.save()
        update_session_auth_hash(request, user)
        messages.add_message(request, messages.SUCCESS, "Password Updated Successfully..!")
        return redirect(reverse('dashboard'))
    return render_to_response("accounts/change_password.html", {
        "form": form
    }, context_instance=RequestContext(request))


def introduction(request):
    """
        When the user login first time into the system.
        User is redirected to this page
    """
    if not request.user.is_authenticated():
        return redirect("/")

    context = {}
    return render(request, 'accounts/introduction-1.html', context)


def introduction2(request):
    """
        This add the country for which User is interested
    """
    profile = Profile.objects.get(user=request.user)

    if request.method == "POST":
        country_code = request.POST.get('country', '')
        if country_code:
            country = Country.objects.get(code=country_code)
            profile.country_intrested_in = country.name
            profile.save()
        return redirect(reverse('introduction3'))

    countries = Country.objects.all()
    context = {
        'countries': countries,
        'profile': profile,
    }
    return render(request, 'accounts/introduction-2.html', context)


def introduction3(request):
    """
        This for add the Favorite Artist
    """
    if request.method == "POST":
        fav_artist_id = request.POST.getlist('fav_artists', '')
        if fav_artist_id:
            fav_artist_id = [int(id) for id in fav_artist_id]
            profile = Profile.objects.get(user=request.user)
            for id in fav_artist_id:
                profile.favorite_artist.add(Artist.objects.get(artistID=id))
            profile.save()
        return redirect(reverse('introduction4'))

    profile = Profile.objects.get(user=request.user)
    all_user_fav_artist = profile.favorite_artist.all().values_list('artistID', 'artistName')
    artists = Artist.objects.all().values_list('artistID', 'artistName')

    context = {
        'artists': artists,
        'all_user_fav_artist': all_user_fav_artist,
    }
    return render(request, 'accounts/introduction-3.html', context)


def introduction4(request):
    """
        This is the third step of introduction
    """
    profile = Profile.objects.get(user=request.user)
    context = {
        'profile': profile,
    }
    return render(request, 'accounts/introduction-4.html', context)


def introduction5(request):
    """
        This is the fifth step of introduction to select the email frequency
    """
    profile = Profile.objects.get(user=request.user)
    # all_user_fav_artist = profile.favorite_artist.all().values_list('artistID', 'artistName')
    # artists = Artist.objects.all().values_list('artistID', 'artistName')

    if request.method == "POST":
        form = FrequencyForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            email_frequency_choice = cd["email_frequency"]
            profile.email_frequency = ""
            if email_frequency_choice:
                profile.email_frequency = ",".join(email_frequency_choice)
            profile.save()

        return redirect(reverse('introduction6'))
    else:
        form = FrequencyForm()

    email_frequency_value = profile.email_frequency
    email_frequency_value = email_frequency_value.split(",")
    favorite_artist_name = profile.favorite_artist.all().values_list('artistName', flat=True)
    favorite_artist_name = (", ").join(favorite_artist_name)

    context = {
        'favorite_artist_name': favorite_artist_name,
        'email_frequency_value': email_frequency_value,
        'form': form,
    }

    return render(request, 'accounts/introduction-5.html', context)


def introduction6(request):
    """
        This is the final step of introduction
    """
    context = {}
    return render(request, 'accounts/introduction-6.html', context)


def dashboard(request):
    """
        This is the place where user will be redirected once he is login
    """
    context = {}
    return render(request, 'accounts/dashboard.html', context)


@login_required
def deactivate(request):
    if request.method == "POST":
        user = request.user
        user.is_active = False
        user.save()
        logout(request)
        return redirect(reverse('deactivate_msg'))
    return render(request, 'accounts/account_deactivate.html', {})


def deactivate_msg(request):
    return render(request, 'accounts/deactivate_msg.html', {})


def request_an_beta_invite(request):
    if request.user.is_authenticated():
        return redirect(reverse('artists'))

    form = RequestAnInviteForm(request.POST or None, request.FILES)
    countries = Country.objects.all().order_by("name")

    if request.method == "POST":
        if form.is_valid():
            user = User.objects.create_user(
                username=create_username('%s %s' % (form.cleaned_data["first_name"], form.cleaned_data["last_name"])),
                email=form.cleaned_data['email'],
                is_active = False,
            )
            user.first_name = form.cleaned_data['first_name']
            user.last_name = form.cleaned_data['last_name']
            user.save()

            profile = Profile.objects.create(
                user=user,
                plan = "Beta",
                mobile_no = form.cleaned_data['mobile_no'],
                facebook_url = form.cleaned_data['facebook_url'],
            )
            user_types = request.POST.getlist('user_types', '')
            user_types = (",").join(user_types)
            profile.user_types = user_types

            email = SendEmail(request)
            email.send_by_template([user.email], "profiles/email/beta_sign_up_thanks.html",
                context={
                },
                subject = "Welcome to Wheredjsplay.com"
            )

            admin_email = SendEmail(request)
            email.send_by_template(settings.DEFAULT_ADMIN_EMAILS, "profiles/email/admin_notify.html",
                context={
                    'profile': profile,
                    'profile_url': settings.SITE_URL + '/admin/profiles/profile/{}/change/'.format(profile.id),
                    'user_url': settings.SITE_URL + '/admin/auth/user/{}/change/'.format(user.id),
                },
                subject = "New User Applied For the Invitation"
            )

            list = mailchimp.utils.get_connection().get_list_by_id(settings.MAILCHIMP_BETA_LIST)
            list.subscribe(user.email, {'EMAIL': user.email, 'FNAME': user.first_name, 'LNAME': user.last_name})

            request.session['tmp_user_id'] = user.id

            return redirect(reverse('thanks'))

    else:
        form = RequestAnInviteForm()

    return render_to_response("accounts/request_an_beta_invite.html", {
        "form": form,
        "countries": countries,
    }, context_instance=RequestContext(request))


def checkout_thanks(request):
    return render_to_response("accounts/checkout_thanks.html", {
    }, context_instance=RequestContext(request))

def subscription_checkout_thanks(request):
    profile = None
    if request.user.is_authenticated():
        profile = Profile.objects.get(user = request.user)
    return render_to_response("accounts/subscription_checkout_thanks.html", {
        "profile":profile,
    }, context_instance=RequestContext(request))
    

@login_required
@check_email_validated
def my_membership(request):
    context = {}
    profile = Profile.objects.get(user=request.user)
    user_name = profile.user
    user_type = profile.user_types
    current_time =  timezone.now()
    trial_period_days = AdminSettings.objects.get(pk=1).free_trial
    # subscription_obj = Subscription.objects.filter(user=user_name).first()
    # if user.stripe_plan and user.subscription and user.subscription.is_active and not user.carddetails_set.all().count() < 1 and not user.is_trial_done:
    if not profile.stripe_plan:
        return redirect(reverse('plans'))
    
    if (profile.carddetails_set.all().count() < 1 and (not profile.is_trial_done)) or profile.is_subscription_failed:
        return redirect(reverse('checkout_user_types')+ "?plan_id={}".format(profile.stripe_plan.id))

    if not profile.subscription.is_refund and profile.subscription.is_active:
        try: 
            card_details = CardDetails.objects.get(user=profile, is_primary=True)
        except:
            card_details = None
        # if profile.subscription and profile.carddetails_set.all().count() < 1:
        # 	return redirect(reverse('checkout_user_types')+ "?plan_id={}".format(profile.stripe_plan.id))
        if profile.subscription:
            # start_time = subscription_obj.objects.get(start_time__lte=current_time)
            # end_time = subscription_obj.objects.get(end_time__gte=current_time)
            start_time = profile.subscription.start_time
            end_time = profile.subscription.end_time

            context.update({
                "start_time": start_time,
                "end_time": end_time,
                "subscription_obj": profile.subscription,
                "card_details": card_details,
                "trial_days":trial_period_days
            })

        context.update({
            "profile": profile,
            "user_name": user_name,
            "user_type": user_type,
            "card_details": card_details,
            "trial_days":trial_period_days
            })

        return render_to_response("accounts/my_membership.html", context,
        context_instance=RequestContext(request))
    elif profile.stripe_plan and profile.subscription.is_refund and profile.subscription.is_active:
        return redirect(reverse('checkout_user_types')+ "?plan_id={}".format(profile.stripe_plan.id))
    else:
        return redirect(reverse('plans'))

def social_auth_login_redirect(request):
    user = request.user
    return after_login(user, next=None)


def social_auth_error(request):
    return render(request, "accounts/social_auth_error.html", {})

def after_email(request):
    profile = request.user.profile_set.first()
    profile.login_count += 1
    profile.save()
    return redirect(reverse('marketing_dashboard'))
    # return render(request, "accounts/email_validation_done.html")
@check_email_validated
def email_validation_done(request):
    return render(request, "accounts/email_validation_done.html")

def after_login(user, next):
    profile = user.profile_set.all()
    profile = profile[0]

    if not profile.is_email_validated:
        return redirect("email_not_validated")
    else:
        profile.login_count += 1
        profile.save()
    if next:
        redirect(reverse('marketing_dashboard') + "?next={}".format(next))

    return redirect(reverse('marketing_dashboard'))


def user_details_after(strategy, details, user=None, *args, **kwargs):
    if not user:
        messages.info(strategy.request, "You have to sign up first.")


def account_already_in_use(strategy, details, user=None, *args, **kwargs):
    if strategy.request.user.is_authenticated():

        # applications = Applications.objects.get(application_name="WhereDjsPlay")
        # user_from = UserFrom.objects.filter(user_from=applications).values_list('user', flat=True)
        # social_auth_users = UserSocialAuth.objects.filter(id__in=user_from)
        #
        # social_auth_object = social_auth_users.filter(Q(uid=details["email"])|Q(uid=kwargs['uid'])).first()

        social_auth_users = UserSocialAuth.objects.all()

        social_auth_object = UserSocialAuth.objects.filter(Q(uid=details["email"])|Q(uid=kwargs['uid'])).first()

        if social_auth_object in social_auth_users:
            messages.info(strategy.request, "This Account is Already in Use")
            return redirect('/')


def check_user_from(strategy, details, user=None, *args, **kwargs):

    application = Applications.objects.filter(application_name="WhereDjsPlay")

    userfrom = UserFrom.objects.filter(user_from=application).values_list('user', flat=True)

    social_auth_users = UserSocialAuth.objects.filter(Q(uid=details["email"])|Q(uid=kwargs['uid'])).first()


    # data = {'backends': LazyDict(lambda: user_backends_data(strategy.request.user,
    #                                                         BACKENDS,
    #                                                         Storage))}

    # print "user bakends data +++++++++++++++++++++++++++++++"
    # data = user_backends_data(strategy.request.user, BACKENDS, Storage)

    # print "data ++++++++++++++++++++++++++++++"
    # print data["backends"]
    # print data.get("associated")
    #
    # print "kwargs association +++++++++++++++++++++++++++++"
    # print kwargs["social"].user

    if social_auth_users:
        if not social_auth_users.user.id in userfrom:
            messages.add_message(strategy.request, messages.ERROR, "Please Sign Up Your Profile.")
            return redirect("/")
    # else:
    #     messages.add_message(strategy.request, messages.ERROR, "Please Sign Up Your Profile.")
    #     return redirect("/")
    #



def registered_users_only(strategy, details, user=None, *args, **kwargs):
    user = None

    social_auth_users = UserSocialAuth.objects.all()

    social_auth_object = UserSocialAuth.objects.filter(Q(uid=details["email"])|Q(uid=kwargs['uid'])).first()

    if not social_auth_object in social_auth_users:
        messages.info(strategy.request, "Please connect your Profile First")
        return redirect(reverse("login_user")+"?connect={}".format("required"))

from django.views.decorators.csrf import csrf_exempt



@csrf_exempt
def coming_soon(request):
    if request.method=="POST":

        email = request.POST.get("email")
        name = request.POST.get("name")
        user_types = request.POST.get("user_types")
        try:
            contact_data = {
                "properties":[
                    {
                        "type": "SYSTEM",
                        "name": "email",
                        "value": email
                    },
                    {
                        "type": "SYSTEM",
                        "name": "name",
                        "value": name
                    },
                    {
                        "type": "SYSTEM",
                        "name": "user_types",
                        "value": user_types
                    },

                ]
            }

            agile = agileCRM("contacts","POST",contact_data,"application/json")
        except:
            pass
        return redirect("/")


@csrf_exempt
def footer_coming_soon(request):
    if request.method=="POST":
        email = request.POST.get("email")
        try:
            contact_data = {
                "properties":[
                    {
                        "type": "SYSTEM",
                        "name": "email",
                        "value": email
                    },
                ]
            }

            agile = agileCRM("contacts","POST",contact_data,"application/json")
        except:
            pass
        return redirect("/")



def social_auth_login(request, backend):
    """
        This view is a wrapper to social_auths auth
        It is required, because social_auth just throws ValueError and gets user to 500 error
        after every unexpected action. This view handles exceptions in human friendly way.
        See https://convore.com/django-social-auth/best-way-to-handle-exceptions/
    """
    from social_auth.views import auth

    try:
        # if everything is ok, then original view gets returned, no problem
        return auth(request, backend)
    except ValueError as error:
        # in case of errors, let's show a special page that will explain what happened
        return render_to_response('users/login_error.html',
                                  locals(),
                                  context_instance=RequestContext(request))


def request_a_coupon(request):
    profile = Profile.objects.filter(user=request.user).first()
    coupon_obj = coupon.objects.filter(user=user).first()
    coupon_name = coupon_obj.coupon_name
    context = {"user_types_list":user_types_list,
              "coupon_name": coupon_name,
                  }
    if request.method == "POST":
        first_name = request.POST.get("first_name", None)
        last_name = request.POST.get("last_name", None)
        email = request.POST.get("email", None)
        user_type = request.POST.get("user_type", None)

        email_obj = MarketingEmails.objects.filter(email_type="request_a_coupon").first()

        if email_obj:

            site_url = settings.SITE_URL
            content = email_obj.email_content
            content = content.replace("{first_name}", first_name)
            content = content.replace("{last_name}", last_name)
            content = content.replace("{email}", email)
            content = content.replace("{user_type}", user_type)
            content = content.replace('src="', 'src="'+site_url+'')

            user_email = SendEmail(request)
            user_email.send_by_template(settings.DEFAULT_ADMIN_EMAILS ,"marketing_pages/not_loged_5.html",
                context={
                    "content": content,
                },
                subject = email_obj.email_subject
            )

            messages.add_message(request, messages.SUCCESS, "Your Request for Coupon has been Sent to Admin.")

        return redirect(reverse("request_an_invite"))

    return render(request, 'accounts/request_a_coupon.html', context)


def update_primary_card(request):
    if request.user.is_authenticated():
        publishable = settings.STRIPE_PUBLISHABEL_KEY
        form = CardDetailForm()
        profile = Profile.objects.get(user=request.user)
        if request.method == "POST":
            card_primary = CardDetails.objects.filter(user=profile, is_primary=True)
            stripe_obj = StripeAPIView()
            card_id = stripe_obj.create_card(
                customer_id=profile.stripe_customer_id,
                token=request.POST["stripeToken"])
            if card_id["status"] == 200:
                if card_primary:
                    card = CardDetails.objects.create(
                        card_number=request.POST["last4"],
                        user=profile,
                        holder_name=request.POST["name"],
                        card_type=request.POST["brand"],
                        expiry_month=request.POST["exp_month"],
                        expiry_year=request.POST["expiryYear"],
                        is_primary=True)
                    for temp_card in card_primary:
                        temp_card.is_primary = False
                        temp_card.save()
                else:
                    StripeAPIView.make_primary(
                        customer_id=profile.stripe_customer_id, card_id=card_id["card_id"])
                    if card_id["status"] == 200:
                        card = CardDetails.objects.create(
                            card_number=request.POST["last4"],
                            user=profile,
                            holder_name=request.POST["name"],
                            card_type=request.POST["brand"],
                            expiry_month=request.POST["exp_month"],
                            expiry_year=request.POST["expiryYear"],
                            is_primary=True)
                card.stripe_card_id = card_id["card_id"]
                card.save()
                messages.add_message(request, messages.SUCCESS, "Added card")
                return redirect(reverse('my_membership'))
            else:
                messages.add_message(request, messages.ERROR, card_id["message"])
                return redirect(reverse('my_membership'))
        context = {'profile': profile, "form":form, "stripe_key": publishable}
        return render(request, "add_credit_card.html", context)
    return redirect(reverse('login_user'))

@csrf_exempt
def handle_webhook(request):
    event_json = json.loads(request.body.decode('utf-8'))
    WebhookHistory.objects.create(
        event_name=event_json["type"],
        data=event_json)
    if event_json["type"] == "charge.failed":
        user = Profile.objects.get(
            stripe_customer_id=event_json["data"]["object"]["customer"])
        user.is_subscription_failed = True
        '''send mail'''
        user.save()
    if event_json["type"] == "customer.subscription.updated":
        user = Profile.objects.get(
            stripe_customer_id=event_json["data"]["object"]["customer"])
        if "status" in event_json["data"]["previous_attributes"]:
            if Q(event_json["data"]["object"]["status"] == "active") & Q(
                    event_json["data"]["previous_attributes"]["status"] == "trialing"):
                if user.is_trial_done:
                    user.is_trial_done = False
                    user.save()
        user.subscription.last_renewal_date = convert_unix_time(
            event_json["data"]["previous_attributes"]["current_period_end"])
        user.subscription.next_renewal_date = convert_unix_time(
            event_json["data"]["previous_attributes"]["current_period_start"])
        user.save()
    return HttpResponse(status=200)


def cancel_subscription(request):
    if request.user.is_authenticated():
        stripe_obj = StripeAPIView()
        user = Profile.objects.get(user=request.user)
        del_sub = stripe_obj.delete_subscription(user.subscription.stripe_subscription_id)
        if del_sub["status"] == 200:
            user.stripe_plan = None
            user.subscription.is_active = False
            user.subscription.save()
            user.save()
            
            card_primary = CardDetails.objects.filter(user=user, is_primary=True)
            for temp_card in card_primary:
                temp_card.is_primary = False
                temp_card.save()

            return render_to_response("accounts/subscription_cancel_checkout.html", {
                    }, context_instance=RequestContext(request))
        return redirect(reverse('my_membership'))
    return redirect(reverse('login_user'))