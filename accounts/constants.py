USER_TYPES = (
    ('Promoter', 'Promoter'),
    ('Agent', 'Agent'),
    ('Artists/DJs', 'Artists/DJs'),
    # ('PressFanRecord', 'PressFanRecord'),
    # ('Companies', 'Companies'),
    ('Press', 'Press'),
    ('Management', 'Management')
)

EMAIL_CONDITIONS = (
    ('2_5', '2_5'),
    ('3_5', '3_5'),

)


user_types_list = ["Artists/DJs", "Promoter", "Agent", "Management", "Press"]
temp_user_types_list = ["Artists/DJs", "Promoter", "Agent"]
# prod_user_types_list = ["Promoter"]
prod_user_types_list = ["Artists/DJs", "Promoter", "Agent"]

TYPE_OF_PERFORMANCE = (
    ('Singer', 'Singer'),
    ('Dj', 'Dj'),
    ('Band', 'Band'),
    ('Solo Performer', 'Solo Performer'),
    ('Live Electronic', 'Live Electronic'),
    ('Singing Group', 'Singing Group'),
    ('Singing Group Female', 'Singing Group Female'),
    ('Singing Group male', 'Singing Group male'),
    ('Large Group of Musicians more than 8', 'Large Group of Musicians more than 8'),
)

NEWS_TYPES = (
    ('Artist News', 'Artist News'),
    ('Event News', 'Event News'),
    ('Festival News', 'Festival News'),
    ('Club News', 'Club News'),
)

NEWS_SOURCES = ["DataTransmission", "ChangeUnderground", "YourEdm", "DjTimes",
                "ResidentAdvisor", "PulseRadio", "MixMag", "DjMag", "IbizaSpotLight", 
                "FabricLondon", "TheGuardian", "MagneticMag", "ZeroHedge",
                "Mtv", "RecordOfTheDay", "Xlr8r", "DancingAstronaut", "RunTheTrap"]

COUPON_DURATION_TYPES = (
    ('forever', 'forever',),
    ('once', 'once'),
    ('repeating', 'repeating'),
    )