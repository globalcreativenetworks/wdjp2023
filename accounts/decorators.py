from functools import wraps

from django.utils import timezone
from django.conf import settings
from django.shortcuts import redirect

from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponse
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from django.contrib import messages

from profiles.models import Profile


def check_plan(*plan_type):
    def _check_plan(view_func):
        @wraps(view_func)
        def wrapper(request, *args, **kwargs):
            # plan_type.append("Artists/DJs")
            if request.user.is_authenticated():
                profile = Profile.objects.get(user=request.user)

                if request.user.is_superuser:
                    return view_func(request, *args, **kwargs)

                if not profile.stripe_plan:
                    messages.add_message(request, messages.INFO, "Please Select A Plan to continue")
                    return redirect(reverse('plans'))
                
                # if profile.stripe_plan not in plan_type:
                #     messages.add_message(request, messages.INFO, "You are not allowed to visit this page. Please check your plan")
                #     return redirect(reverse('dashboard'))

                # Trial User Check
                if profile.is_trial_done and profile.subscription and profile.carddetails_set.all().count() == 0:
                    if timezone.now() > profile.subscription.end_time:
                        messages.add_message(request, messages.INFO, "Fill Credit Card details and subscribe to your plan")
                        return redirect(reverse('checkout_user_types') + "?plan_id={}".format(profile.stripe_plan.id))
                    else:
                        return view_func(request, *args, **kwargs)

                # Normal User
                if (not profile.is_trial_done) and profile.subscription and profile.subscription.is_active and (not profile.is_subscription_failed):
                    
                    if profile.carddetails_set.all().count() == 0:
                        messages.add_message(request, messages.INFO, "Fill Credit Card details and subscribe to your plan")
                        return redirect(reverse('checkout_user_types') + "?plan_id={}".format(profile.stripe_plan.id))

                    if timezone.now() > profile.subscription.end_time:
                        messages.add_message(request, messages.INFO, "Fill Credit Card details and subscribe to your plan")
                        return redirect(reverse('checkout_user_types') + "?plan_id={}".format(profile.stripe_plan.id))
                    else:
                        return view_func(request, *args, **kwargs)
                else:
                    messages.add_message(request, messages.INFO, "Fill Credit Card details and subscribe to your plan")
                    return redirect(reverse('checkout_user_types') + "?plan_id={}".format(profile.stripe_plan.id))
            else:
                return view_func(request, *args, **kwargs)
        return wrapper
    return _check_plan


# ------------------ OLD CHECK PLAN DECORATOR -----------------------
# def check_plan(*plan_type):
#     def _check_plan(view_func):
#         @wraps(view_func)
#         def wrapper(request, *args, **kwargs):
#             if request.user.is_authenticated():
#                 profile = Profile.objects.get(user=request.user)

#                 if request.user.is_superuser:
#                     return view_func(request, *args, **kwargs)

#                 #Handle the Beta User
#                 if profile.plan == "Beta" and profile.is_trial_end_time >= timezone.now():
#                     return view_func(request, *args, **kwargs)
#                 elif profile.plan == "Beta" and profile.is_trial_end_time < timezone.now():
#                     messages.add_message(request, messages.INFO, "Your Beta Trial Period Has Been Expired. Please Purchase a Plan to continue")
#                     return redirect(reverse('plans'))

#                 #Handle the trial User
#                 if profile.plan == "Trial" and "Trial" in plan_type:
#                     if check_trail_user(request, profile):
#                         return view_func(request, *args, **kwargs)
#                     else:
#                         messages.add_message(request, messages.INFO, "Please Upgrade Your Plan")
#                         return redirect(reverse('plans'))

#                 if request.user.is_superuser or profile.plan and profile.plan in plan_type :
#                     return view_func(request, *args, **kwargs)
#                 else:
#                     if not profile.plan:
#                         messages.add_message(request, messages.INFO, "Please Purchase A Plan")
#                     else:
#                         messages.add_message(request, messages.INFO, "Please Upgrade Your Plan")
#                     return redirect(reverse('plans'))
#             else:
#                 return view_func(request, *args, **kwargs)
#         return wrapper
#     return _check_plan


def check_email_validated(view_func):
    def _wrapped_view_func(request, *args, **kwargs):
        if request.user.is_authenticated():
            profile = Profile.objects.filter(user=request.user).first()
            if profile.is_email_validated:
                return view_func(request, *args, **kwargs)

            return redirect(reverse("email_not_validated"))

    return _wrapped_view_func


def have_plan(view_func):
    def _wrapped_view_func(request, *args, **kwargs):
        if request.user.is_authenticated():
            profile = Profile.objects.get(user=request.user)

            trial_user = check_trail_user(request, profile)
            if trial_user:
                return view_func(request, *args, **kwargs)

            if (profile.plan == "Trial" or not profile.plan) and not request.user.is_superuser:
                return redirect(reverse('plans'))
            else:
                return view_func(request, *args, **kwargs)
        else:
            return view_func(request, *args, **kwargs)
    return _wrapped_view_func


def is_premium(view_func):
    def _wrapped_view_func(request, *args, **kwargs):
        if request.user.is_authenticated():
            profile = Profile.objects.get(user=request.user)

            trial_user = check_trail_user(request, profile)
            if trial_user:
                return view_func(request, *args, **kwargs)

            if not profile.is_premium and not request.user.is_superuser:
                return redirect(reverse('plans'))
            else:
                return view_func(request, *args, **kwargs)
        else:
            return view_func(request, *args, **kwargs)
    return _wrapped_view_func


def check_trail_user(request, profile):
    trial_user = False

    if not profile.is_trial_done:
        if profile.is_trial_end_time:
            if profile.is_trial_end_time >= timezone.now():
                trial_user = True
            else:
                if profile.plan != "Beta":
                    profile.is_trial_done = True
                    profile.save()

    return trial_user
