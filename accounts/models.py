from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
from django.contrib.auth.models import User
from profiles.models import Profile


USER_FROM = (
    ('Wheredjsplay', 'Wheredjsplay'),
    ('CompareDjs', 'CompareDjs'),
    ('MusicRiver', 'MusicRiver'),
)


class Applications(models.Model):
    application_name = models.CharField(max_length=100)

    def __unicode__(self):
        return u'%s' % (self.application_name)


class ForgetPassword(models.Model):
    email = models.EmailField(max_length=75)
    token = models.CharField(max_length=10)
    is_used = models.BooleanField(default=True)

    def __unicode__(self):
        return u'%s' % (self.email)

# ToDo move to independent model due to cyclic dependency on artists. Now created a duplication in artist models
class EmailConfirmation(models.Model):
    email = models.EmailField(max_length=150)
    token = models.CharField(max_length=10)
    is_latest = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return u'%s' % (self.email)


class UserFrom(models.Model):
    user = models.ForeignKey(User, null=True, blank=True)
    user_from = models.ManyToManyField(Applications)
    source = models.CharField(max_length=50, choices=USER_FROM, null=True, blank=True)

    def __unicode__(self):
        return u'%s' % (self.user.email)


    # class Meta:
    #     managed = False
    #     db_table = 'profiles_profile_favorite_venues'
    #     unique_together = (('profile', 'djvenues'),)
