# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2018-06-04 13:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='EmailConfirmation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(max_length=150)),
                ('token', models.CharField(max_length=10)),
                ('is_latest', models.BooleanField(default=False)),
            ],
        ),
    ]
