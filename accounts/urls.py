from django.conf.urls import url

from accounts import views

urlpatterns = [
    url(r'^login-user/$', views.login_user, name='login_user'),
    url(r'^logout-user/$', views.logout_user, name='logout_user'),
    url(r'^forget-password/$', views.forget_password, name='forget_password'),
    url(r'^reset-password/(?P<token>[a-zA-Z0-9]+)$', views.reset_password, name='reset_password'),
    url(r'^plans/$', views.plans, name="plans"),
    # url(r'^checkout/$', views.checkout, name="checkout"),
    url(r'^checkout/$', views.checkout_user_types, name="checkout_user_types"),
    url(r'^checkout-thanks/$', views.checkout_thanks, name="checkout_thanks"),
    url(r'^invitation_thanks/$', views.invitation_thanks, name="thanks"),
    url(r'^signup/$', views.request_an_invite, name="request_an_invite"),
    url(r'^change-password/$', views.change_password, name='change_password'),
    url(r'^introduction-1/$', views.introduction, name='introduction'),
    # url(r'^introduction-2/$', views.introduction2, name='introduction2'),
    url(r'^introduction-2/$', views.introduction3, name='introduction3'),
    url(r'^introduction-3/$', views.introduction4, name='introduction4'),
    url(r'^introduction-4/$', views.introduction5, name='introduction5'),
    url(r'^introduction-5/$', views.introduction6, name='introduction6'),
    url(r'^dashboard/$', views.dashboard, name='dashboard'),
    url(r'^deactivate/$', views.deactivate, name='deactivate'),
    url(r'^deactivate-msg/$', views.deactivate_msg, name='deactivate_msg'),
    url(r'^request-an-beta-invite/$', views.request_an_beta_invite, name="request_an_beta_invite"),
    # url(r'^signup/$', views.signup, name="request_an_invite"),
    url(r'^type-of-user/$', views.type_of_user, name="type_of_user"),
    url(r'^promoter-1/$', views.promoter_1, name="promoter_1"),
    url(r'^promoter-2/$', views.select_genre, name="select_genre"),
    url(r'^promoter-3/$', views.promoter_3, name="promoter_3"),
    url(r'^promoter-4/$', views.promoter_4, name="promoter_4"),
    url(r'^promoter-5/$', views.promoter_5, name="promoter_5"),
    url(r'^social_auth_login_redirect/$', views.social_auth_login_redirect, name="social_auth_login_redirect"),
    url(r'^social-auth-error/$', views.social_auth_error, name="social_auth_error"),
    url(r'^my-membership/$', views.my_membership, name='my_membership'),
    url(r'^request-coupon/$', views.request_a_coupon, name='request_a_coupon'),
    url(r'^validate-email/(?P<email>.+)/(?P<token>[a-zA-Z0-9]+)$', views.validate_email, name='validate_email'),
    url(r'^generate-confirmation-email/$', views.generate_confirmation_email, name='generate_confirmation_email'),
    url(r'^email-not-validated/$', views.email_not_validated, name='email_not_validated'),
    url(r'^refund/$', views.refund, name='refund'),
    url(r'^confirm-refund/$', views.confirm_refund, name='confirm_refund'),
    url(r'^coming-soon/$', views.coming_soon, name='coming_soon'),
    url(r'^footer-coming-soon/$', views.footer_coming_soon, name='footer_coming_soon'),
    url(r'^confirm-set-password/$', views.confirm_set_password, name='confirm_set_password'),
    url(r'^email-validation-done/$', views.email_validation_done, name='email_validation_done'),
    url(r'^after-email/$', views.after_email, name='after_email'),
    url(r'^status/$', views.handle_webhook, name="status"),
    url(r'^add_card/$', views.update_primary_card, name="add_card"),
    url(r'^cancel_subscription/$', views.cancel_subscription, name="cancel_subscription"),
    url(r'^subscription_checkout_thanks/$', views.subscription_checkout_thanks, name="subscription_checkout_thanks"),
    

   ]
