from django import forms
from django.forms import FileInput
from django.contrib.auth.models import User

from .models import ForgetPassword, UserFrom, Applications
from profiles.models import Profile, Coupon, CardDetails

class LoginForm(forms.Form):
    email = forms.EmailField(max_length=75, widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'email', 'required':'required'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'required':'required'}))
    #
    # def clean(self):
    #     cleaned_data = super(LoginForm, self).clean()
    #     email = cleaned_data.get("email", "")
    #
    #     if not Profile.objects.filter(user__email=email).exists():
    #         raise forms.ValidationError("This email address does not exists.")
    #     return cleaned_data

    class Meta:
        model = Profile
        fields = ('email', 'password')
        # widgets = {
        #     'image': FileInput(),
        # }

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        # self.fields['email'].widget.attrs.update({'class': 'py-form-control'})
        # self.fields['facebook_url'].widget.attrs.update({'class': 'py-form-control'})
        # self.fields["password"].required = False

    def clean_email(self):
        email = self.cleaned_data['email']
        if not Profile.objects.filter(user__email=email).exists():
            self._errors["email"] = self.error_class(["This email address does not exists."])

        user = User.objects.filter(email=email).first()
        if user:
            if not user.is_superuser:
                user_from = UserFrom.objects.filter(user=user).first()
                # compare_djs_application = Applications.objects.get(application_name="CompareDjs")
                wdjp_application = Applications.objects.get(application_name="WhereDjsPlay")
                if user_from:
                    user_applications = user_from.user_from.all()
                    if wdjp_application not in user_applications:
                        # logout(request)
                        # return redirect("/")
                    # if user_from.user_from == application:
                        self._errors["email"] = self.error_class(["This email address does not exists."])

        return self.cleaned_data['email']


class CouponForm(forms.Form):
    coupon = forms.CharField(max_length=75, widget=forms.TextInput(attrs={'class': 'form-control', 'autocomplete': 'off' }))

    def clean(self):
        cleaned_data = super(CouponForm, self).clean()
        coupon = cleaned_data.get("coupon", "")

        if not Coupon.objects.filter(coupon_code=coupon).exists():
            raise forms.ValidationError("Please enter a valid coupon.")
        return cleaned_data



class ForgetPasswordForm(forms.Form):
    email = forms.EmailField(max_length=75, widget=forms.TextInput(attrs={'class': 'form-control',}))

    def clean(self):
        cleaned_data = super(ForgetPasswordForm, self).clean()
        email = cleaned_data.get("email", "")

        if not Profile.objects.filter(user__email=email).exists():
            raise forms.ValidationError("This email address does not exists.")
        return cleaned_data


class ResetPasswordForm(forms.Form):
    new_password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())

    def clean(self):
        cleaned_data = super(ResetPasswordForm, self).clean()
        new_password = cleaned_data.get("new_password", "")
        confirm_password = cleaned_data.get("confirm_password", "")
        if new_password and confirm_password and not new_password == confirm_password:
            raise forms.ValidationError("Password did not match")
        return cleaned_data

    def __init__(self, *args, **kwargs):
        super(ResetPasswordForm, self).__init__(*args, **kwargs)
        self.fields['new_password'].widget.attrs.update({'class': 'form-control input_width'})
        self.fields['confirm_password'].widget.attrs.update({'class': 'form-control input_width'})


# class SignUpEmailForm(forms.ModelForm):
#     name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control','required':'required'}))
#     email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control', 'required':'required'}))
#     password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'required':'required'}))
#
#     class Meta:
#         model = Profile
#         fields = ("name", "email", "password")
#
#     def clean(self):
#         cleaned_data = super(SignUpEmailForm, self).clean()
#         try:
#             existing_user = User.objects.get(email__iexact=self.cleaned_data['email'])
#             if existing_user:
#                 raise forms.ValidationError("This email already exist")
#         except User.MultipleObjectsReturned:
#             raise forms.ValidationError("This email already exist")
#         except:
#             pass
#         return cleaned_data


class RequestAnInviteForm(forms.ModelForm):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control','required':'required'}))
    # last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control','required':'required'}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'py-form-control', 'type': 'email', 'required':'required'}))
    coupon = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control'}))
    # password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'py-form-control', 'required':'required'}))
    confirm_password = forms.CharField(min_length=6, widget=forms.PasswordInput(attrs={'class': 'py-form-control', 'required':'required'}))
    # nick_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control','required':'required'}))
    # nick_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control'}))


    class Meta:
        model = Profile
        fields = ('mobile_no', 'facebook_url')
        widgets = {
            'image': FileInput(),
        }

    def __init__(self, *args, **kwargs):
        super(RequestAnInviteForm, self).__init__(*args, **kwargs)
        self.fields['mobile_no'].widget.attrs.update({'class': 'py-form-control'})
        # self.fields['facebook_url'].widget.attrs.update({'class': 'py-form-control'})
        self.fields["coupon"].required = False

    def clean_coupon(self):
        coupon = self.cleaned_data['coupon']
        if coupon:
            coupon = Coupon.objects.filter(
                coupon_code=coupon,
                is_live=True
            )
            if not coupon:
                self._errors['coupon'] = self.error_class(
                    ["Invalid Coupon"]
                )
        return self.cleaned_data['coupon']

    def clean_email(self):
        user = User.objects.filter(email=self.cleaned_data['email']).first()
        if user:
            user_from = UserFrom.objects.filter(user=user).first()
            if user_from:
                wdjp_application = Applications.objects.get(application_name="WhereDjsPlay")
                user_application = user_from.user_from.all()

                if wdjp_application in user_application :
                    try:
                        existing_user = User.objects.get(email__iexact=self.cleaned_data['email'])
                        if existing_user:
                            self._errors["email"] = self.error_class(["An account already exists under {}.".format(self.cleaned_data['email'])])
                    except User.MultipleObjectsReturned:
                        self._errors["email"] = self.error_class(["An account already exists under {}. ".format(self.cleaned_data['email'])])
                    except:
                        pass

        return self.cleaned_data['email']

    # def clean_confirm_password(self):
    #     cleaned_data = super(RequestAnInviteForm, self).clean()
    #     password = cleaned_data.get("password", "")
    #     confirm_password = cleaned_data.get("confirm_password", "")
    #     try:
    #         if password != confirm_password :
    #             self._errors["confirm_password"] = self.error_class(["Password and Confirm Password should be same."])
    #     except:
    #         pass
    #     return self.cleaned_data['confirm_password']

class ChangePasswordForm(forms.Form):
    new_password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())
    current_password = forms.CharField(widget=forms.PasswordInput())

    def clean(self):
        cleaned_data = super(ChangePasswordForm, self).clean()
        new_password = cleaned_data.get("new_password", "")
        confirm_password = cleaned_data.get("confirm_password", "")
        current_password = self.cleaned_data.get('current_password', None)
        if current_password and not self.user.check_password(current_password):
            raise forms.ValidationError({'current_password':'Invalid current password'})

        if new_password and confirm_password and not new_password == confirm_password:
            raise forms.ValidationError({"new_password":"Password did not match"})

        return cleaned_data

    def __init__(self, user, *args, **kwargs):
        self.user = user._wrapped if hasattr(user,'_wrapped') else user
        super(ChangePasswordForm, self).__init__(*args, **kwargs)
        self.fields['new_password'].widget.attrs.update({'class': 'form-control input_width'})
        self.fields['confirm_password'].widget.attrs.update({'class': 'form-control input_width'})
        self.fields['current_password'].widget.attrs.update({'class': 'form-control input_width'})



class CardDetailForm(forms.ModelForm):
    cvv_number = forms.IntegerField(
        widget=forms.TextInput(attrs={
            'maxlength': '4',
        })
    )
    coupon = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control'}))

    class Meta:
        model = CardDetails
        fields = (
            'card_number',
            'holder_name',
            'card_type',
            'expiry_month',
            'expiry_year',
            'cvv_number',
            'coupon')