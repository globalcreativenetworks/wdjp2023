import itertools
# import stripe
import tempfile

from functools import wraps
from django.core.mail import EmailMultiAlternatives
from django.conf import settings
from django.template.loader import render_to_string
from django.template import RequestContext
from django.contrib.auth.models import User
from django.shortcuts import render, render_to_response, redirect
from django.core.urlresolvers import reverse
from django.utils import timezone

from profiles.models import Subscription

# stripe.api_key = settings.STRIPE_SECRET_KEY

class SendEmail(object):

    def __init__(self, request=None, headers=None, sender=None, backend=None , file=[]):
        self.request = request
        self.headers = headers
        self.from_name = settings.DEFAULT_FROM_EMAIL_NAME
        self.file = file

        if sender:
            self.sender = sender
        else:
            self.sender = settings.DEFAULT_FROM_EMAIL

    def send_by_template(self, recipient, template_path, context, subject, bcc_email=[], cc_email=[], fail_silently=True):
        """
        send email function with template_path. will do both rendering & send in this function.
        should not change the interface.
        """
        body = self.email_render(template_path, context)
        self.send_email(recipient, subject, body, bcc_email, cc_email)

    def send_by_body(self, recipient, subject, body, bcc_email=[], cc_email=[], fail_silently=True):
        """
        send email function with text. will do both rendering & send in this function.
        should not change the interface.
        """
        try:
            self.send_email(recipient, subject, body, bcc_email, cc_email)
        except Exception:
            pass

    def send_email(self, recipient, subject, body, bcc_email, cc_email, fail_silently=True):
        """
        send email with rendered subject and body
        """
        msg = EmailMultiAlternatives(subject, subject, self.sender, recipient, bcc=bcc_email, cc=cc_email)

        msg.attach_alternative(body, "text/html")

        # if self.file:
        #     for file in self.file:
        #        tmp_file = tempfile.NamedTemporaryFile(mode='wb', delete="True")
        #        tmp_file.close()
        #        # tmp_file = open(file.name, 'w')
        #        tmp_file = open(file, 'w')
        #        # tmp_file.write(file.read())
        #        tmp_file.write(file)
        #        tmp_file.close()
        #        msg.attach_file(tmp_file.name)
        #        tmp_file.close()

        if self.file:
            for file in self.file:
               # tmp_file = tempfile.NamedTemporaryFile(mode='wb', delete="True")
               # tmp_file.close()
               # tmp_file = open(file.name, 'w')
               # tmp_file = open(file, 'w')
               # tmp_file.write(file.read())
               # tmp_file.write(file)
               # tmp_file.close()
               msg.attach_file(file)
               # tmp_file.close()

        msg.send()

    def email_render(self, template_path, context):
        """
        wrapper to generate email subject and body
        """
        if self.request is None:
            body = render_to_string(template_path, context)
        else:
            body = render_to_string(template_path, context, RequestContext(self.request))
        return body

def create_username(name):
    """create the unique username of every new user by using its name"""
    name = ''.join(e for e in name if e.isalnum())
    name = name[:29]
    base_name = name
    ctr = 1

    while True:
        try:
            user = User.objects.get(username=name)
            name = base_name + (str(ctr))
            ctr += 1
        except User.DoesNotExist:
            break

    return name


def send_beta_remind_email(user_email):
    toni_email = settings.TONI_EMAIL
    email = SendEmail(sender=toni_email)
    email.send_by_template([user_email], "accounts/emails/beta_remind_email.html",
        context={
        },
        subject = "Beta Trial Ending - WDJP"
    )


def send_basic_remind_email(user_email):
    toni_email = settings.TONI_EMAIL
    email = SendEmail(sender=toni_email)
    subject = "Price Change Notification -WDJP"
    email.send_by_template([user_email], "accounts/emails/basic_remind_email.html",
        context={
            "site_url": settings.SITE_URL
        },
        subject = subject
    )

def change_user_plan_to_basic_plan(profile):
    plan_name = "Basic"
    subscription_obj = Subscription.objects.get(user=profile.user)

    # stripe_obj = stripe.Customer.retrieve(subscription_obj.customer_id)
    # sub_id = stripe_obj.subscriptions['data'][0]['id']
    # stripe_sub = subscription = stripe.Subscription.retrieve(sub_id)
    stripe_sub.plan = "2"
    stripe_sub.save()

    subscription_obj.plan = plan_name
    subscription_obj.start_time = timezone.now()
    subscription_obj.end_time = timezone.now() + timezone.timedelta(days=30)
    subscription_obj.save()

    profile.is_trial_done=True,
    profile.plan=plan_name
    profile.save()


# def tokengenretor


def end_trial_plan(profile):
    subscription_obj = Subscription.objects.get(user=profile.user)

    profile.is_trial_done=True,
    profile.save()


def end_plan(profile):
    # subscription_obj = Subscription.objects.filter(user=profile.user)
    profile.plan = " "
    profile.save()
