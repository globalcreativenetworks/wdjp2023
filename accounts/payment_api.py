from django.conf import settings
import stripe

stripe.api_key = settings.STRIPE_SECRET_KEY


class StripeAPIView():

	def create_customer(self, email):
		try:
			response = stripe.Customer.create(email=email)
			return {
				"status": 200,
				"message": "Success",
				"customer_id": response["id"]}
		except Exception as e:
			return {"status": 301, "message": e}

	# def update_customer(customer_id, email):
	#     try:
	#         response = stripe.Customer.retrieve(customer_id)
	#         response.email = email
	#         response.save()
	#         return {
	#             "status": 200,
	#             "message": "Success",
	#             "customer_id": response["id"]}
	#     except Exception as e:
	#         return {"status": 301, "message": e}

	def create_plan(self, amount, title):
		try:
			response = stripe.Plan.create(
				amount=amount*100,
				interval="month",
				product={
					"name": title
				},
				currency="usd",
			)
			return {
				"status": 200,
				"message": "Success",
				"plan_id": response.id}
		except Exception as e:
			return {"status": 301, "message": e}

	def create_card(self, customer_id, token):
		try:
			customer = stripe.Customer.retrieve(customer_id)
			response = customer.sources.create(source=token)
			return {
				"status": 200,
				"message": "Success",
				"card_id": response.id}
		except Exception as e:
			return {"status": 301, "message": e}

	def make_primary(self, customer_id, card_id):
		try:
			try:
				response = stripe.Customer.retrieve(customer_id)
				response.default_source = card_id
				response.save()
				return {
					"status": 200,
					"message": "Success",
					"customer_id": response.id}
			except Exception as e:
				return {"status": 301, "message": e}
		except Exception as e:
			return {"status": 301, "message": e}

	def create_subscription(self, trial_period_days, customer_id, plan_id, coupon=None):
		try:
			if trial_period_days:
				response = stripe.Subscription.create(
					customer=customer_id,
					items=[
						{
							"plan": plan_id,
						},
					],
					trial_period_days=trial_period_days,
					coupon=coupon,
				)
			else:
				response = stripe.Subscription.create(
					customer=customer_id,
					items=[
						{
							"plan": plan_id,
						},
					],
					coupon=coupon,
				)
			return {
				"status": 200,
				"message": "Success",
				"subscription_id": response.id,
				"current_period_start": response.current_period_start,
				"current_period_end": response.current_period_end}
		except Exception as e:
			return {"status": 301, "message": e}

	def end_trail_subscription(self, subscription_id):
		try:
			response = stripe.Subscription.modify(subscription_id,
				trial_end='now',
				)
			return {
				"status": 200,
				"message": "Success",
				"subscription_id": response.id,
				"current_period_start": response.current_period_start,
				"current_period_end": response.current_period_end}
		except Exception as e:
			return {"status": 301, "message": e}

	# def delete_card(customer_id, card_id):
	#     try:
	#         customer = stripe.Customer.retrieve(customer_id)
	#         customer.sources.retrieve(card_id).delete()
	#         return {"status": 200, "message": "Success"}
	#     except Exception as e:
	#         return {"status": 301, "message": e}

	def cancel_subscription(self, subscription_id):
		try:
			sub = stripe.Subscription.delete(subscription_id)
			return {"status": 200, "message": sub}
		except Exception as e:
			return {"status": 301, "message": e}

	def get_invoice_list(self, customer_id):
		try:
			response = stripe.Invoice.list(limit=1, customer=customer_id)
			# response = customer.sources.create(source=token)
			return {
				"status": 200,
				"message": "Success",
				"charge": response['data'][0]['charge']}
		except Exception as e:
			return {"status": 301, "message": e}

	def refund(self, charge):
		try:
			refund = stripe.Refund.create(
				charge=charge
			)
		except Exception as e:
			return {"status": 301, "message": e}

	def create_coupon(self, duration, amount_off, coupon_code, coupon_name):
		try:
			if duration == "repeating":
				res = stripe.Coupon.create(
					amount_off=amount_off,
					duration=duration,
					duration_in_months=1,
					name=coupon_name,
					id=coupon_code,
					currency='usd',
				)
			else:
				res = stripe.Coupon.create(
					amount_off=amount_off,
					duration=duration,
					name=coupon_name,
					id=coupon_code,
					currency='usd',
				)
			return {"status": 200, "message": res}
		except Exception as e:
			return {"status": 301, "message": e}
	
	def delete_subscription(self, subscription_id):
		try:
			sub = stripe.Subscription.retrieve(subscription_id)
			sub.delete()
			return {"status": 200, "message": "Success"}
		except Exception as e:
			return {"status": 301, "message": e}