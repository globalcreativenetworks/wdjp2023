from django.contrib import admin
from django import forms

from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.db.models import Q

from .models import ForgetPassword, EmailConfirmation, UserFrom, Applications
from profiles.models import Profile
from django.contrib.admin import SimpleListFilter


class CustomUserAdmin(UserAdmin):
    def get_queryset(self, request):
        applications = Applications.objects.get(application_name="WhereDjsPlay")
        user_from = UserFrom.objects.filter(user_from=applications).values_list('user', flat=True)
        qs = super(CustomUserAdmin, self).get_queryset(request)
        return qs.filter(Q(id__in=user_from)|Q(is_superuser=True))

    list_display = UserAdmin.list_display + ('profile_link', 'source')

    def profile_link(self, obj):
        user_profile = Profile.objects.filter(user=obj)
        if user_profile:
            return '<a target="_blank" href="/admin/profiles/profile/{}/change/">Profile Link</a>'.format(user_profile[0].id)
        else:
            return "No Profile"

    profile_link.allow_tags = True
    profile_link.short_description = 'Profile Link'

    def source(self, obj):
        user_source = UserFrom.objects.filter(user=obj).first()
        if user_source:
            return user_source.source

    list_filter = UserAdmin.list_filter+('userfrom__source',)


# class CustomUserAdmin(UserAdmin):
#     list_display = UserAdmin.list_display + ('profile_link',)
#
#     def profile_link(self, obj):
#         user_profile = Profile.objects.filter(user=obj)
#         if user_profile:
#             return '<a target="_blank" href="/admin/profiles/profile/{}/change/">Profile Link</a>'.format(user_profile[0].id)
#         else:
#             return "No Profile"
#
#     profile_link.allow_tags = True
#     profile_link.short_description = 'Profile Link'

class EmailConfirmationAdmin(admin.ModelAdmin):
    list_display = ("email", "token", "is_latest",)


class UserFromAdmin(admin.ModelAdmin):
    search_fields = ['user__email']


class ApplicationsAdmin(admin.ModelAdmin):
    list_display = ("application_name",)


# class UserFromAdmin(admin.ModelAdmin):
#     list_display = ("email", "user_from")


admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)

admin.site.register(ForgetPassword)
admin.site.register(EmailConfirmation, EmailConfirmationAdmin)

admin.site.register(UserFrom, UserFromAdmin)
admin.site.register(Applications, ApplicationsAdmin)
