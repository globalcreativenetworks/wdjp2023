from django.core.management.base import BaseCommand
from django.utils import timezone
from django.conf import settings

from profiles.models import Profile
from accounts.utils import send_beta_remind_email, SendEmail

class Command(BaseCommand):
    help = "Remind Beta User For Trial Period And Upgrade It"

    def handle(self, *args, **options):
        profiles = Profile.objects.filter(plan="Beta", user__is_active=True)
        reminded_users = []
        for profile in profiles:
            if profile.is_trial_end_time:
                expirary_date_interval = (profile.is_trial_end_time - timezone.now()).days
                print (expirary_date_interval)
                if expirary_date_interval == 2:
                    send_beta_remind_email(profile.user.email)
                    reminded_users.append(profile.user.email)

        if reminded_users:
            admin_email = SendEmail()
            subject = "Beta Trial reminder script ran succesfully. The following users are reminded"
            body = "USER {} has been notified they only have 2 days left means 13 days of their trial period. User will not able to access anything after 2 days ".format((", ").join(reminded_users)) 
            admin_email.send_by_body(settings.DEFAULT_ADMIN_EMAILS,
                subject = subject,
                body=body,
            )
