from django.core.management.base import BaseCommand
from django.core.management import call_command

from watson.models import SearchEntry

class Command(BaseCommand):
    help = "This will delete all the data from watson SearchEntry model rebuild the watson data again"

    def handle(self, *args, **options):
        search_entry = SearchEntry.objects.all().delete()
        call_command('buildwatson')
        print ("watson rebuilded again")
