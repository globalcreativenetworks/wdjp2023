from django.core.management.base import BaseCommand
from django.utils import timezone
from django.conf import settings
from django.contrib.auth.models import User

from profiles.models import Profile
from accounts.utils import send_beta_remind_email, SendEmail
from accounts.models import UserFrom

class Command(BaseCommand):
    help = "Mapping user for Compare Djs "

    def handle(self, *args, **options):
        users = User.objects.all().exclude(is_superuser=True)
        application = Applications.objects.get(applicaton_name="CompareDjs")

        for user in users:
            user_from, created = UserFrom.objects.get_or_create(user=user)
            if created:
                user_from.user_from.add(application)
                user_from.save()
