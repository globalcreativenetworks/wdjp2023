from django.core.management.base import BaseCommand

from profiles.models import Plan, AdminSettings
from accounts.payment_api import StripeAPIView


plans = [{
    "title": "Artist/DJ",
    "amount": 25,
    "user_type":"Artists/DJs"
},
{
    "title": "Agent",
    "amount": 50,
    "user_type": "Agent"
}]

# This is for promoter user
# 
# {
#     "title": "Promoter",
#     "amount": 60,
#     "user_type": "Promoter"
# }


def load_plan():
    AdminSettings.objects.get_or_create(free_trial=30)
    stripe_obj = StripeAPIView()
    for plan in plans:
        plan_id = stripe_obj.create_plan(
            plan["amount"], plan["title"])
        if plan_id["status"] == 200:
            plan_obj = Plan.objects.create(
                title=plan["title"],
                amount=plan["amount"], user_type=plan["user_type"])
            plan_obj.stripe_plan_id = plan_id["plan_id"]
            plan_obj.save()
            print("Created plan")
        else:
            print("Failed to create plan")
            return False


class Command(BaseCommand):
    def handle(self, **options):
        load_plan()
