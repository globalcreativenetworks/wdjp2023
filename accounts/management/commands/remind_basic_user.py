from django.core.management.base import BaseCommand
from django.utils import timezone
from django.conf import settings

from profiles.models import Profile, Subscription
from accounts.utils import send_beta_remind_email, send_basic_remind_email, change_user_plan_to_basic_plan, SendEmail, end_trial_plan, end_plan


class Command(BaseCommand):
    help = "Remind Basic User For Upgrading of Account"

    def handle(self, *args, **options):
        profiles = Profile.objects.filter(user__is_active=True)
        reminded_users = []
        account_changed_user = []
        for profile in profiles:
            if Subscription.objects.filter(user=profile.user):
                if profile.is_trial_end_time:
                    expirary_date_interval = (profile.is_trial_end_time - timezone.now()).days
                    if expirary_date_interval == 2:
                        send_basic_remind_email(profile.user.email)
                        reminded_users.append(profile.user.email)
                    elif expirary_date_interval < 1:
                        # change_user_plan_to_basic_plan(profile)
                        # end_trial_plan(profile)
                        end_plan(profile)
                        account_changed_user.append(profile.user.email)
                    else:
                        pass

        if reminded_users:
            admin_email = SendEmail()
            subject = "Basic reminder script ran succesfully. The following users are reminded"
            body = "Following User are Reminded {}".format((", ").join(reminded_users))
            admin_email.send_by_body(settings.DEFAULT_ADMIN_EMAILS,
                subject = subject,
                body=body,
            )

        # if account_changed_user:
        #     admin_email = SendEmail()
        #     subject = "You Trial Plan has been Expired "
        #     body = "Following User accounts is changed {}".format((", ").join(account_changed_user))
        #     admin_email.send_by_body(settings.DEFAULT_ADMIN_EMAILS,
        #         subject = subject,
        #         body=body,
        #     )
