from django import forms
from django.contrib.auth.models import User
from django.forms import FileInput, Textarea
from .models import WdjpNews

from ckeditor.widgets import CKEditorWidget


class WdjpNewsImageForm(forms.ModelForm):
    class Meta:
        model = WdjpNews
        fields = ('image',)
        widgets = {
            'image': FileInput(),
        }

    def __init__(self, *args, **kwargs):
        super(WdjpNewsImageForm, self).__init__(*args, **kwargs)
        self.fields['image'].widget.attrs.update({'class': 'py-form-control', 'required':'required'})



class WdjpNewsForm(forms.ModelForm):
    text = forms.CharField(widget=CKEditorWidget())
    # is_visible = forms.BooleanField()

    class Meta:
        model = WdjpNews
        fields = ('image', 'title', 'text', 'highlighted_text', 'type', 'artists', 'is_visible', 'news_for', 'url')
        widgets = {
            'image': FileInput(),
            'highlighted_text': Textarea(attrs={'cols': 90, 'rows': 6}),
        }

    def __init__(self, *args, **kwargs):
        super(WdjpNewsForm, self).__init__(*args, **kwargs)
        self.fields['image'].widget.attrs.update({'class': 'py-form-control', 'required':'required'})
        self.fields['title'].widget.attrs.update({'class': 'py-form-control', 'required':'required'})
        # self.fields['text'].widget.attrs.update({'class': 'py-form-control', 'required':'required'})
        self.fields['highlighted_text'].widget.attrs.update({'class': 'form-control custom-textarea', 'required':'required', 'cols': 90, 'rows': 6})
        self.fields['type'].widget.attrs.update({'class': 'py-form-control', 'required':'required'})
        self.fields['text'].widget.attrs.update({'class': 'richtexteditor', 'cols': '90', 'rows': '5', 'required':'required'})
        self.fields['artists'].widget.attrs.update({'class': 'py-form-control', 'required':'required'})
        self.fields['news_for'].widget.attrs.update({'class': 'py-form-control', 'required':'required'})
        self.fields['is_visible'].widget.attrs.update({'class': 'py-form-control', 'required':'required'})
        self.fields['url'].widget.attrs.update({'class': 'py-form-control'})
