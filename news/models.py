from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField

from accounts.models import Applications
from artist.models import Artist
from accounts.constants import NEWS_TYPES
from geography.models import Country, City

# Create your models here.
class NewsSource(models.Model):
    sourceName = models.CharField(max_length=50)
    sourceUrl = models.CharField(max_length=100)
    is_non_dance_website = models.BooleanField(default=False)

    def __str__(self):
        return u"{}".format(self.sourceName)


class News(models.Model):
    source = models.ForeignKey(NewsSource)
    imageurl = models.CharField(max_length=400, blank=True, null=True)
    url = models.CharField(max_length=300)
    title = models.CharField(max_length=500)
    text = models.TextField()
    modified_text = models.TextField(null=True, blank=True)
    publish_date = models.DateTimeField(null=True, blank=True)
    artists = models.ManyToManyField(Artist, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    country = models.ManyToManyField(Country, null=True, blank=True)

    def __str__(self):
        return u"{}-{}".format(self.url, self.source.sourceName)


class WdjpNews(models.Model):
    author = models.ForeignKey(User)
    published_date = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    image = models.ImageField(upload_to='newsphotos/', blank=True, null=True)
    title = models.CharField(max_length=400, blank=True, null=True)
    text = RichTextField(verbose_name='text', null=True, blank=True)
    highlighted_text = models.CharField(max_length=400, blank=True, null=True)
    type = models.CharField(max_length=100, blank=True, null=True, choices=NEWS_TYPES)
    artists = models.ManyToManyField(Artist, related_name="artists",
                                                 null=True, blank=True)
    is_visible = models.BooleanField(default=True)
    news_for = models.ManyToManyField(Applications, null=True, blank=True)
    url = models.URLField(null=True, blank=True) 
    
    def __str__(self):
        return u"{}".format(self.title)
