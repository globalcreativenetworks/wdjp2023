# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-05-18 08:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0005_auto_20170518_0858'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='modified_text',
            field=models.TextField(blank=True, null=True),
        ),
    ]
