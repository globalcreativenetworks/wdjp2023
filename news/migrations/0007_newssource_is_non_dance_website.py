# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-05-23 13:02
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0006_news_modified_text'),
    ]

    operations = [
        migrations.AddField(
            model_name='newssource',
            name='is_non_dance_website',
            field=models.BooleanField(default=False),
        ),
    ]
