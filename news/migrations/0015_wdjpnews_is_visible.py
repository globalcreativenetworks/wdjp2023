# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2018-10-05 08:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0014_wdjpnews_artists'),
    ]

    operations = [
        migrations.AddField(
            model_name='wdjpnews',
            name='is_visible',
            field=models.BooleanField(default=True),
        ),
    ]
