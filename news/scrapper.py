import newspaper

from news.parsers import *
from news.models import *

class SourceClass(object):
    sources_classes = {
        "DataTransmission": DataTransmissionParser(),
        "ChangeUnderground": (),
        "YourEdm": YourEdm(),
        "DjTimes": DjTimesParser(),
        "ResidentAdvisor": ResidentAdvisorParser(),
        "MixMag": MixMagParser(),
        "DjMag": DjMagParser(),
        "IbizaSpotLight": IbizaSpotLightParser(),
        "TheGuardian": TheGuardianParser(),
        "MagneticMag": MagneticMagParser(),
        "ZeroHedge": ZeroHedgeParser(),
        "Mtv": MtvParser(),
        "RecordOfTheDay": RecordOfTheDayParser(),
        "Xlr8r": Xlr8rParser(),
        "DancingAstronaut":DancingAstronautParser(),
        "RunTheTrap":RunTheTrap(),


        # "FabricLondon": FabricLondonParser(),    # throws error
        # "PulseRadio": PulseRadioParser(),        # offline
        # "PrNewsWire": PrNewsWireParser(),
        # "BillBoard": BillBoardParser(),
        # "Vice": ViceParser(),
        # "TheTimes": TheTimesParser(),
        # "HyperBot": HyperBotParser(),
    }

    def get_sources_name(self):
        return self.sources_classes


class NewsScrapper(SourceClass):

    def scrap_news(self):
        all_sources = NewsSource.objects.all()
        sources_names = self.get_sources_name()
        for source in all_sources:
            if sources_names.get(source.sourceName):
                class_name = sources_names[source.sourceName]
                instance = class_name
                instance.scrap()
            else:
                print "Class Name is not Implemented For The Source- {}".format(source)
