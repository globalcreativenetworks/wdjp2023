from __future__ import division
from datetime import datetime, timedelta
from django.utils import timezone
from django.core.management.base import BaseCommand

from accounts.utils import SendEmail

from django.contrib.auth.models import User
from django.conf import settings

from profiles.models import Profile, NewsLetter
from artist.models import Artist
from news.models import News


class Command(BaseCommand):
    help = "News Letter emails"

    def handle(self, *args, **options):
        all_profile = Profile.objects.all()
        # all_profile = Profile.objects.filter(user_id=4)

        for profile in all_profile:
            total_news_count = 0
            template_response = []

            if not is_send_email(profile):
                continue

            user_fav_artist = profile.favorite_artist.all()
            fav_artist = user_fav_artist.count()

            if not user_fav_artist:
                continue

            for artist_object in user_fav_artist:
                news = News.objects.filter(artists=artist_object).order_by('-publish_date').distinct()[:4]
                total_news_count = total_news_count + news.count()
                template_response.append({
                    'artist': artist_object,
                    'news': news,
                })

            user_email = profile.user.email
            if user_email and total_news_count > 5:
                newsletter = NewsLetter.objects.create(
                    sender=profile,
                )
                newsletter.save()
                subject = "Headline on "
                i = 1
                temp_list = []
                for ar in template_response:
                    if ar['news'] and not i > 5:
                        temp_list.append(ar["artist"].artistName)
                        i = i + 1

                subject = subject + ", ".join(temp_list)
                mail = SendEmail()
                try:
                    mail.send_by_template([user_email], "news/news_letter_mail.html",
                        context={
                            "template_response": template_response,
                            'site_url': settings.SITE_URL,
                        },
                        subject =subject
                    )
                except Exception as e:
                    print e

def is_send_email(profile):
    week_days = {
        '0': 'Monday',
        '1': 'Tuesday',
        '2': 'Wednesday',
        '3': 'Thursday',
        '4': 'Friday',
        '5': 'Saturday',
        '6': 'Sunday'
    }
    user_email_days = profile.email_frequency
    user_email_days = user_email_days.split(",")
    if user_email_days:
        current_day = timezone.now().weekday()
        current_day = week_days[str(current_day)]
        if current_day in user_email_days:
            return True
    return False
