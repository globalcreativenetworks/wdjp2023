#!/usr/bin/python
import sys, os

from django.core.management.base import BaseCommand
from scrapper import Scrapper, ScrapMode
from news.scrapper import NewsScrapper
from django.core.management import call_command


current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)


class Command(BaseCommand):
    help = "Run Scrapping process"

    def handle(self, *args, **options):
        news = NewsScrapper()
        news.scrap_news()
        call_command('generate_log')
