from django.contrib import admin

from news.models import NewsSource, News, WdjpNews
# Register your models here.

class NewsSourceAdmin(admin.ModelAdmin):
    list_display = ("sourceName", "sourceUrl", "is_non_dance_website")
    search_fields = ['sourceName', "sourceUrl"]
    ordering = ('sourceName',)
    list_filter = ["is_non_dance_website"]

class NewsAdmin(admin.ModelAdmin):
    list_display = ("source", "url", "title", "publish_date")
    search_fields = ['source__sourceName', "url", "title"]
    ordering = ('-created',)
    filter_horizontal = ['artists']
    list_filter = ["source"]

class WdjpNewsAdmin(admin.ModelAdmin):
    list_display = ("author", "title", "highlighted_text", "published_date", "updated_date")
    search_fields = ["author__username", "title"]
    ordering = ('-published_date',)
    # filter_horizontal = ['artists']
    # list_filter = ["source"]


admin.site.register(NewsSource, NewsSourceAdmin)
admin.site.register(News, NewsAdmin)
admin.site.register(WdjpNews, WdjpNewsAdmin)
