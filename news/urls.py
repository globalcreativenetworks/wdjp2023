from django.conf.urls import patterns, include, url

from news import views

urlpatterns = [
    url(r'^$', views.news, name='news'),
    url(r'^email-frequency/$', views.email_frequency, name='email_frequency'),
    url(r'^create-news/$', views.create_news, name='create_news'),
    url(r'^wdjp-news/$', views.wdjp_news, name='wdjp_news'),
    url(r'^news-detail/(?P<id>\d+)/$', views.news_detail, name="news_detail"),
    url(r'^wdjp-news/(?P<id>[0-9]+)/edit/$', views.news_edit, name='news_edit'),
    url(r'^wdjp-news/(?P<news_id>[0-9]+)$', views.delete_news, name='delete_news'),
    url(r'^search_news/$', views.search_news, name="search_news"),
    url(r'^my_news/$', views.my_news_feed, name="my_news_feed"),

]
