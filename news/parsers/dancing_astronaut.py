from urlparse import urlparse
from datetime import date

import newspaper

from news.parsers.base_newspaper import NewsPaperBaseParser, SavedNewsToDB
from news.models import News, NewsSource
from social_data.models import DataLog



class DancingAstronautParser(NewsPaperBaseParser, SavedNewsToDB):
    today_date = date.today()

    def scrap(self):
        newssource = NewsSource.objects.get(sourceName="DancingAstronaut")
        sourceUrl = newssource.sourceUrl

        count = 0
        all_urls = self.get_scrapping_urls()
        for url in all_urls:
            all_data = self.build_data_from_source(url)
            self.save_news_data(all_data, newssource)
            count += 1

        dancing_astronaut_log, created = DataLog.objects.get_or_create(created=DancingAstronautParser.today_date)

        if dancing_astronaut_log.news_dancing_astronaut:
            dancing_astronaut_log_count = int(dancing_astronaut_log.news_dancing_astronaut)
            dancing_astronaut_log_count += count
            dancing_astronaut_log.news_dancing_astronaut = dancing_astronaut_log_count
        else:
            dancing_astronaut_log.news_dancing_astronaut = count
        dancing_astronaut_log.save()


    def get_scrapping_urls(self):
        newssource = NewsSource.objects.get(sourceName="DancingAstronaut")
        parsed_uri = urlparse(newssource.sourceUrl)
        base_url = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        # return_url = [newssource.sourceUrl]
        return_url = []
        for i in range(1, 11):
            temp_url = base_url + "news/page/{}/".format(i)
            return_url.append(temp_url)
        return return_url

    def get_article_urls(self, url):
        all_articles = newspaper.build(u'{}'.format(url), memoize_articles=False)
        all_articles = all_articles.article_urls()
        new_list = []
        for article in all_articles:
            if "http://www.dancingastronaut.com/news/" not in article:
                new_list.append(article)
        return new_list
