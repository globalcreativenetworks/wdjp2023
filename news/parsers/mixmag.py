from urlparse import urlparse
from datetime import date

from news.parsers.base_newspaper import NewsPaperBaseParser, SavedNewsToDB
from news.models import News, NewsSource
from social_data.models import DataLog


class MixMagParser(NewsPaperBaseParser, SavedNewsToDB):
    today_date = date.today()

    def scrap(self):
        newssource = NewsSource.objects.get(sourceName="MixMag")
        sourceUrl = newssource.sourceUrl
        all_urls = self.get_scrapping_urls()
        count = 0
        for url in all_urls:
            all_data = self.build_data_from_source(url)
            final_data = []

            for data in final_data:
                if not "news/p" in data['url']:
                    final_data.append(data)

            self.save_news_data(final_data, newssource)
            count += 1

        mix_mag_parser_log , created = DataLog.objects.get_or_create(created=MixMagParser.today_date)

        if mix_mag_parser_log.news_mixmag:
            mix_mag_parser_log_count = int(mix_mag_parser_log.news_mixmag)
            mix_mag_parser_log_count += count
            mix_mag_parser_log.news_mixmag = mix_mag_parser_log_count
        else:
            mix_mag_parser_log.news_mixmag = count

        mix_mag_parser_log.save()

    def get_scrapping_urls(self):
        newssource = NewsSource.objects.get(sourceName="MixMag")
        parsed_uri = urlparse(newssource.sourceUrl)
        base_url = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        return_url = [newssource.sourceUrl]
        for i in range(2, 11):
            temp_url = base_url + "news/p{}/".format(i)
            return_url.append(temp_url)
        return return_url
