import requests
import datetime
from urlparse import urlparse
from bs4 import BeautifulSoup
from unidecode import unidecode
from datetime import date

from news.parsers.base_newspaper import NewsPaperBaseParser, SavedNewsToDB
from news.models import News, NewsSource
from social_data.models import DataLog


class Xlr8rParser(NewsPaperBaseParser, SavedNewsToDB):
    today_date = date.today()

    def scrap(self):
        newssource = NewsSource.objects.get(sourceName="Xlr8r")
        sourceUrl = newssource.sourceUrl
        all_urls = self.get_scrapping_urls()
        count = 0
        for url in all_urls:
            all_data = self.build_data_from_source(url)
            self.save_news_data(all_data, newssource)
            count += 1

        news_xlr8_log, created = DataLog.objects.get_or_create(created=Xlr8rParser.today_date)

        if news_xlr8_log.news_xlr8r:
            news_xlr8_log_count = int(news_xlr8_log.news_xlr8r)
            news_xlr8_log_count += count
            news_xlr8_log.news_xlr8r = news_xlr8_log_count
        else:
            news_xlr8_log.news_xlr8r = count

        news_xlr8_log.save()


    def get_scrapping_urls(self):
        newssource = NewsSource.objects.get(sourceName="Xlr8r")
        parsed_uri = urlparse(newssource.sourceUrl)
        base_url = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        return_url = [newssource.sourceUrl]
        for i in range(1, 11):
            temp_url = base_url + "page/{}/".format(i)
            return_url.append(temp_url)
        return return_url

    def fetch_custom_date(self, url):
        try:
            content = requests.get(url).content
            soup = BeautifulSoup(content)
            article_div = soup.find("div", {"class": "post-detail-row"})
            if article_div:
                date = article_div.find('time')['content']
                if date:
                    time = datetime.datetime.strptime(date[:10], '%Y-%m-%d')
                    print time
                    return time
            return None
        except:
            return None

    def save_news_data(self, all_data, newssource):
        if all_data:
            for scrap_news in all_data:
                if not News.objects.filter(url=scrap_news["url"]):
                    if scrap_news["artists"]:
                        custom_date = self.fetch_custom_date(scrap_news["url"])
                        news = News.objects.create(
                            source = newssource,
                            url = scrap_news["url"],
                            title = scrap_news["title"],
                            text = scrap_news["text"],
                        )
                        news.save()

                        if custom_date:
                            news.publish_date = custom_date
                        else:
                            if scrap_news["date"]:
                                news.publish_date = scrap_news["date"]
                            else:
                                news.publish_date = news.created

                        if scrap_news["image"]:
                            news.imageurl = scrap_news["image"]

                        for artist in scrap_news["artists"]:
                            news.artists.add(artist)
                        news.save()
