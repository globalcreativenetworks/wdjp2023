from urlparse import urlparse
from datetime import date


from news.parsers.base_newspaper import NewsPaperBaseParser, SavedNewsToDB
from news.models import News, NewsSource
from social_data.models import DataLog

class YourEdm(NewsPaperBaseParser, SavedNewsToDB):
    today_date = date.today()

    def scrap(self):
        newssource = NewsSource.objects.get(sourceName="YourEdm")
        sourceUrl = newssource.sourceUrl
        all_urls = self.get_scrapping_urls()
        count = 0
        for url in all_urls:
            all_data = self.build_data_from_source(url)
            final_data = []
            try:
                for data in all_data:
                    if not "http://www.youredm.com/news/page" in data['url']:
                        final_data.append(data)
            except:
                pass
            self.save_news_data(final_data, newssource)
            count += 1

        news_your_edm_log, created = DataLog.objects.get_or_create(created=YourEdm.today_date)

        if news_your_edm_log.news_youredm:
            news_your_edm_log_count = int(news_your_edm_log.news_youredm)
            news_your_edm_log_count += count
            news_your_edm_log.news_youredm = news_your_edm_log_count
        else:
            news_your_edm_log.news_youredm = count

        news_your_edm_log.save()


    def get_scrapping_urls(self):
        newssource = NewsSource.objects.get(sourceName="YourEdm")
        parsed_uri = urlparse(newssource.sourceUrl)
        base_url = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        return_url = [newssource.sourceUrl]
        for i in range(2, 15):
            temp_url = base_url + "page/{}/".format(i)
            return_url.append(temp_url)
        return return_url
