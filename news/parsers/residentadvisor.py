from news.parsers.base_newspaper import NewsPaperBaseParser, SavedNewsToDB
from news.models import News, NewsSource

from datetime import date
from social_data.models import  DataLog

class ResidentAdvisorParser(NewsPaperBaseParser, SavedNewsToDB):
    today_date = date.today()

    def scrap(self):
        newssource = NewsSource.objects.get(sourceName="ResidentAdvisor")
        sourceUrl = newssource.sourceUrl
        all_data = self.build_data_from_source(sourceUrl)
        count  = self.save_news_data(all_data, newssource)

        if count :
            news_residentadvider_log, created = DataLog.objects.get_or_create(created=ResidentAdvisorParser.today_date)

            if news_residentadvider_log.news_residentadvisor:
                news_residentadvisor_log_count = int(news_residentadvider_log.news_residentadvisor)
                news_residentadvisor_log_count += count
                news_residentadvider_log.news_residentadvisor = news_residentadvisor_log_count
            else:
                news_residentadvider_log.news_residentadvisor = count

            news_residentadvider_log.save()
