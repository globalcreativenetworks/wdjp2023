import requests
from urlparse import urlparse
from bs4 import BeautifulSoup

from news.parsers.base_newspaper import NewsPaperBaseParser, SavedNewsToDB
from news.models import News, NewsSource

class ViceParser(NewsPaperBaseParser, SavedNewsToDB):
    """
    Code is not in run condition there may some legal issue for this
    """

    def scrap(self):
        newssource = NewsSource.objects.get(sourceName="Vice")
        sourceUrl = newssource.sourceUrl
        all_urls = self.get_scrapping_urls()
        print all_urls
        # for url in all_urls:
        #     all_data = self.build_data_from_source(url)
        #     self.save_news_data(all_data, newssource)


    def get_scrapping_urls(self):
        newssource = NewsSource.objects.get(sourceName="Vice")
        parsed_uri = urlparse(newssource.sourceUrl)
        base_url = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        return_url = [newssource.sourceUrl]
        # for i in range(2, 20):
        #     temp_url = base_url + "en_us/latest?page={}".format(i)
        #     return_url.append(temp_url)
        return return_url
