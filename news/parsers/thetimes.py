from urlparse import urlparse
from datetime import date

from news.parsers.base_newspaper import NewsPaperBaseParser, SavedNewsToDB
from news.models import News, NewsSource
from social_data.models import DataLog



class TheTimesParser(NewsPaperBaseParser, SavedNewsToDB):
    today_date = date.today()

    def scrap(self):
        newssource = NewsSource.objects.get(sourceName="TheTimes")
        sourceUrl = newssource.sourceUrl
        all_urls = self.get_scrapping_urls()
        count = 0
        for url in all_urls:
            all_data = self.build_data_from_source(url)
            self.save_news_data(all_data, newssource)
            count += 1


        news_the_times_log, created = DataLog.objects.get_or_create(created=TheTimesParser.today_date)

        if news_the_times_log.news_the_times:
            news_the_times_log_count = int(news_the_times_log.news_the_times)
            news_the_times_log_count += count
            news_the_times_log.news_the_times = news_the_times_log_count
        else:
            news_the_times_log.news_the_times = count

        news_the_times_log.save()


    def get_scrapping_urls(self):
        newssource = NewsSource.objects.get(sourceName="TheTimes")
        return_url = [newssource.sourceUrl]
        return return_url
