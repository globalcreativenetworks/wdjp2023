from urlparse import urlparse
from datetime import date

from news.parsers.base_newspaper import NewsPaperBaseParser, SavedNewsToDB
from news.models import News, NewsSource
from social_data.models import DataLog


class IbizaSpotLightParser(NewsPaperBaseParser, SavedNewsToDB):
    today_date = date.today()

    def scrap(self):
        newssource = NewsSource.objects.get(sourceName="IbizaSpotLight")
        sourceUrl = newssource.sourceUrl
        all_urls = self.get_scrapping_urls()

        count = 0
        for url in all_urls:
            all_data = self.build_data_from_source(url)
            self.save_news_data(all_data, newssource)
            count += 1

        ibiza_spotlight_light_log , created = DataLog.objects.get_or_create(created=IbizaSpotLightParser.today_date)

        if ibiza_spotlight_light_log.news_ibizaspotlight:
            ibiza_spotlight_light_log_count = int(ibiza_spotlight_light_log.news_ibizaspotlight)
            ibiza_spotlight_light_log_count += count
            ibiza_spotlight_light_log.news_ibizaspotlight = ibiza_spotlight_light_log_count
        else:
            ibiza_spotlight_light_log.news_ibizaspotlight = count

        ibiza_spotlight_light_log.save()


    def get_scrapping_urls(self):
        newssource = NewsSource.objects.get(sourceName="IbizaSpotLight")
        parsed_uri = urlparse(newssource.sourceUrl)
        base_url = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        return_url = [newssource.sourceUrl]
        for i in range(2, 11):
            temp_url = base_url + "magazine?page={}".format(i)
            return_url.append(temp_url)
        return return_url
