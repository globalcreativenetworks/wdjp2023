import requests
import datetime
from urlparse import urlparse
from bs4 import BeautifulSoup
from unidecode import unidecode
from datetime import date

from news.parsers.base_newspaper import NewsPaperBaseParser, SavedNewsToDB
from news.models import News, NewsSource
from social_data.models import DataLog


class ChangeUndergroundParser(NewsPaperBaseParser, SavedNewsToDB):
    today_date = date.today()

    def scrap(self):
        newssource = NewsSource.objects.get(sourceName="ChangeUnderground")
        sourceUrl = newssource.sourceUrl
        all_urls = self.get_scrapping_urls()

        count = 0
        for url in all_urls:
            all_data = self.build_data_from_source(url)
            self.save_news_data(all_data, newssource)
            count += 1

        change_underground_log, created = DataLog.objects.get_or_create(created=ChangeUndergroundParser.today_date)

        if change_underground_log.news_change_underground:
            change_underground_log_count = int(change_underground_log.news_change_underground)
            change_underground_log_count += count
            change_underground_log.news_change_underground = change_underground_log_count
        else:
            change_underground_log.news_change_underground = count
        change_underground_log.save()


    def get_scrapping_urls(self):
        # newssource = NewsSource.objects.get(sourceName="ChangeUnderground")
        # parsed_uri = urlparse(newssource.sourceUrl)
        # base_url = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        return_url = [
            "https://change-underground.com/news/",
            "https://change-underground.com/music/essential-premiere/",
            "https://change-underground.com/music/essentialmix/",
            "https://change-underground.com/music/charts/",
        ]
        return return_url

    def fetch_custom_date(self, url):
        try:
            content = requests.get(url).content
            soup = BeautifulSoup(content)
            date_tag = soup.find("div", {"class": "post-meta","class": "background"}).find("span")
            if date_tag:
                date = date_tag.text
                time = datetime.datetime.strptime(date, '%B %d, %Y')
                print time
                return time
            return None
        except:
            return None

    def save_news_data(self, all_data, newssource):
        if all_data:
            for scrap_news in all_data:
                if not News.objects.filter(url=scrap_news["url"]):
                    if scrap_news["artists"]:
                        custom_date = self.fetch_custom_date(scrap_news["url"])
                        news = News.objects.create(
                            source = newssource,
                            url = scrap_news["url"],
                            title = scrap_news["title"],
                            text = scrap_news["text"],
                        )
                        news.save()

                        if custom_date:
                            news.publish_date = custom_date
                        else:
                            if scrap_news["date"]:
                                news.publish_date = scrap_news["date"]
                            else:
                                news.publish_date = news.created

                        if scrap_news["image"]:
                            news.imageurl = scrap_news["image"]

                        for artist in scrap_news["artists"]:
                            news.artists.add(artist)
                        news.save()
