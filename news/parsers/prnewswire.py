from urlparse import urlparse
from datetime import date

from news.parsers.base_newspaper import NewsPaperBaseParser, SavedNewsToDB
from news.models import News, NewsSource

class PrNewsWireParser(NewsPaperBaseParser, SavedNewsToDB):
    today_date = date.today()

    def scrap(self):
        newssource = NewsSource.objects.get(sourceName="PrNewsWire")
        sourceUrl = newssource.sourceUrl
        all_urls = self.get_scrapping_urls()
        count = 0
        for url in all_urls:
            all_data = self.build_data_from_source(url)
            self.save_news_data(all_data, newssource)
            count += 1

        news_pr_news_wire_log, created = DataLog.objects.get_or_create(created=PrNewsWireParser.today_date)

        if news_pr_news_wire_log.news_prnewsire:
            news_pr_news_wire_log_count = int(news_pr_news_wire_log.news_prnewsire)
            news_pr_news_wire_log_count += count
            news_pr_news_wire_log.news_prnewsire = news_pr_news_wire_log_count
        else:
            news_pr_news_wire_log.news_prnewsire = count

        news_pr_news_wire_log.save()


    def get_scrapping_urls(self):
        newssource = NewsSource.objects.get(sourceName="PrNewsWire")
        # parsed_uri = urlparse(newssource.sourceUrl)
        # base_url = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        return_url = [newssource.sourceUrl]
        # for i in range(2, 11):
        #     temp_url = base_url + "news?page={}".format(i)
        #     return_url.append(temp_url)
        return return_url
