from urlparse import urlparse

from news.parsers.base_newspaper import NewsPaperBaseParser, SavedNewsToDB
from news.models import News, NewsSource
from datetime import date
from social_data.models import DataLog

class DjMagParser(NewsPaperBaseParser, SavedNewsToDB):
    today_date = date.today()

    def scrap(self):
        newssource = NewsSource.objects.get(sourceName="DjMag")
        sourceUrl = newssource.sourceUrl
        all_urls = self.get_scrapping_urls()

        count = 0
        for url in all_urls:
            all_data = self.build_data_from_source(url)
            self.save_news_data(all_data, newssource)
            count += 1

        dj_mag_log, created = DataLog.objects.get_or_create(created=DjMagParser.today_date)

        if dj_mag_log.news_djmag:
            dj_mag_log_count = int(dj_mag_log.news_djmag)
            dj_mag_log_count += count
            dj_mag_log.news_djmag = dj_mag_log_count
        else:
            dj_mag_log.news_djmag = count

        dj_mag_log.save()


    def get_scrapping_urls(self):
        newssource = NewsSource.objects.get(sourceName="DjMag")
        parsed_uri = urlparse(newssource.sourceUrl)
        base_url = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        return_url = [newssource.sourceUrl]
        for i in range(2, 11):
            temp_url = base_url + "news?page={}".format(i)
            return_url.append(temp_url)
        return return_url
