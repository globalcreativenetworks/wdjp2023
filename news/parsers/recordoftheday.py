from urlparse import urlparse
from newspaper import Article
from unidecode import unidecode
from datetime import date

from news.parsers.base_newspaper import NewsPaperBaseParser, SavedNewsToDB
from news.models import News, NewsSource
from social_data.models import DataLog

class RecordOfTheDayParser(NewsPaperBaseParser, SavedNewsToDB):
    today_date = date.today()

    def scrap(self):
        newssource = NewsSource.objects.get(sourceName="RecordOfTheDay")
        sourceUrl = newssource.sourceUrl
        all_urls = self.get_scrapping_urls()
        count = 0
        for url in all_urls:
            all_data = self.build_data_from_source(url)
            self.save_news_data(all_data, newssource)
            count += 1

        news_rec_of_the_day_log, created = DataLog.objects.get_or_create(created=RecordOfTheDayParser.today_date)

        if news_rec_of_the_day_log.news_record_of_the_day:
            news_rec_of_the_day_log_count = int(news_rec_of_the_day_log.news_record_of_the_day)
            news_rec_of_the_day_log_count += count
            news_rec_of_the_day_log.news_record_of_the_day = news_rec_of_the_day_log_count
        else:
            news_rec_of_the_day_log.news_record_of_the_day = count

        news_rec_of_the_day_log.save()

    def get_scrapping_urls(self):
        newssource = NewsSource.objects.get(sourceName="RecordOfTheDay")
        parsed_uri = urlparse(newssource.sourceUrl)
        base_url = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        return_url = [newssource.sourceUrl]
        for i in range(2, 11):
            temp_url = base_url + "news-and-press/page/{}/30".format(i)
            return_url.append(temp_url)
        return return_url

    def get_article_data(self, url):
        if News.objects.filter(url=url).exists():
            return {}

        article_dict = {}
        article = Article(url)
        article.download()
        article.parse()
        if "recordoftheday" in url:
            if not article.title:
                split_text = article.text.split("\n\n")
                if split_text:
                    extracted_title = split_text[0]
                    article_dict['title'] = extracted_title[:100]
                else:
                    return {}
            if article.text :
                article_dict["text"] = unidecode(article.text)
                if article.title:
                    article_dict["title"] = unidecode(article.title)
                article_dict["date"] = article.publish_date
                article_dict["url"] = article.url
                article_dict["image"] = article.top_image
                article_dict["artists"] = self.scan_artist(article_dict["text"])
                return article_dict
            else:
                return {}
        else:
            return {}
