
from djtimes import DjTimesParser
from residentadvisor import ResidentAdvisorParser
from pulseradio import PulseRadioParser
from mixmag import MixMagParser
from djmag import DjMagParser
from billboard import BillBoardParser
from vice import ViceParser
from ibizaspotlight import IbizaSpotLightParser
from fabriclondon import FabricLondonParser
from theguardian import TheGuardianParser
from magneticmag import MagneticMagParser
from thetimes import TheTimesParser
from zerohedge import ZeroHedgeParser
from mtv import MtvParser
from hyperbot import HyperBotParser
from recordoftheday import RecordOfTheDayParser
from prnewswire import PrNewsWireParser
from youredm import YourEdm
from xlr8r import Xlr8rParser
from datatransmission import DataTransmissionParser
from changeunderground import ChangeUndergroundParser
from dancing_astronaut import DancingAstronautParser
from runthetrap import RunTheTrap

from base_newspaper import NewsPaperBaseParser
