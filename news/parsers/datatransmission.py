import requests
from unidecode import unidecode
from datetime import datetime
from bs4 import BeautifulSoup
from urlparse import urlparse
from datetime import date


from news.parsers.base_newspaper import NewsPaperBaseParser, SavedNewsToDB
from news.models import News, NewsSource
from social_data.models import DataLog


class DataTransmissionParser(NewsPaperBaseParser, SavedNewsToDB):
    today_date = date.today()

    def __init__(self):
        self.session_obj = requests.session()
        self.session_obj.headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}

    def scrap(self):
        newssource = NewsSource.objects.get(sourceName="DataTransmission")
        sourceUrl = newssource.sourceUrl
        all_urls = self.get_scrapping_urls()
        count = 0
        for url in all_urls:
            all_data = self.build_data_from_source(url)
            self.save_news_data(all_data, newssource)
            count += 1

        data_transmission_log, created = DataLog.objects.get_or_create(created=DataTransmissionParser.today_date)

        if data_transmission_log.news_datatransmission:
            data_transmission_log_count = int(data_transmission_log.news_datatransmission)
            data_transmission_log_count += count
            data_transmission_log.news_datatransmission = data_transmission_log_count
        else:
            data_transmission_log.news_datatransmission = count

        data_transmission_log.save()

    def _make_request(self, url):
        response = self.session_obj.get(url)
        return response

    def _link_extractor(self, data):
        soup = BeautifulSoup(data)
        all_links = []
        all_parent_div = soup.find_all('div', {'class': 'col-sm-6'})

        try:
            for parent_div in all_parent_div:
                link = parent_div.find('a')['href']
                all_links.append(link)
        except:
            pass

        return all_links

    def get_scrapping_urls(self):
        newssource = NewsSource.objects.get(sourceName="DataTransmission")
        return_url = []
        urls = [newssource.sourceUrl]
        for i in range(2, 10):
            urls.append(newssource.sourceUrl + "page/{}/".format(i))
        return urls

    def get_article_urls(self, url):
        return_url = []
        response = self._make_request(url)
        if response.status_code == 200:
            return_url.extend(self._link_extractor(response.content))
        return return_url


    def get_article_data(self, url):
        url = url.split("#")[0]

        if News.objects.filter(url=url).exists():
            return {}

        response = self._make_request(url)
        article_dict = {}
        if response.status_code == 200:
            soup = BeautifulSoup(response.content)
            title = soup.find('h1').text.strip()
            top_image = soup.find('div',{'id': 'content'}).find('img')
            if top_image:
                top_image = top_image['src']
            else:
                top_image = None

            all_text = soup.find('div',{'id': 'content'}).findAll('p')[:-2]
            all_text = [ text.text.strip() for text in all_text]

            text = ("\n").join(all_text)
            time = soup.find('time')['datetime']
            date = datetime.strptime(time, "%Y-%m-%d")


            if title and text:
                article_dict['title'] = unidecode(title)
                article_dict["text"] = unidecode(text)
                article_dict["date"] = date
                article_dict["url"] = url
                article_dict["image"] = top_image
                article_dict["artists"] = self.scan_artist(article_dict["text"])

        return article_dict
