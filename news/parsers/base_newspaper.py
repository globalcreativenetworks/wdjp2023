import re
import newspaper
from unidecode import unidecode
from newspaper import Article

from artist.models import Artist
from news.models import News, NewsSource
from geography.models import Country

IGNORE_ARTIST_LIST = [
    'Justice',
    'Function',
    'Break',
    'Disclosure',
    'Busy P?',
    'Breach',
    'Zip'
]


class SavedNewsToDB(object):

    def update_glossary(self, text, artists):
        for artist in artists:
            modified_text = re.sub(r'\b({})\b'.format(artist.artistName), r'<b>\1</b>', text, flags=re.I)
            text = modified_text
        return modified_text

    def save_news_data(self, all_data, newssource):
        if all_data:
            count = 0
            for scrap_news in all_data[0]:
                if not News.objects.filter(title=scrap_news["title"], source=newssource):
                    if not News.objects.filter(url=scrap_news["url"]):
                        if scrap_news["artists"]:
                                if newssource.is_non_dance_website and len(scrap_news["artists"]) < 2:
                                    pass
                                else:
                                    try:
                                        news = News.objects.create(
                                            source = newssource,
                                            url = scrap_news["url"],
                                            title = scrap_news["title"],
                                            text = scrap_news["text"],
                                        )
                                        news.save()

                                        news.modified_text = self.update_glossary(news.text, scrap_news["artists"])

                                        if scrap_news["date"] and newssource.sourceName != "Xlr8r":
                                            news.publish_date = scrap_news["date"]
                                        else:
                                            news.publish_date = news.created

                                        if scrap_news["image"]:
                                            news.imageurl = scrap_news["image"]

                                        for artist in scrap_news["artists"]:
                                            news.artists.add(artist)

                                        # Add Country
                                        for country in scrap_news["country"]:
                                            news.country.add(country)

                                        news.save()

                                    except Exception as e:
                                        print e

            count += 1
            return count


class ScanArtistInText(object):
    def scan_artist(self, text):
        return_list = []
        all_artist = Artist.objects.all().exclude(artistName__in=IGNORE_ARTIST_LIST)
        for artist in all_artist:
            try:
                if re.search(r'\b{}\b'.format(artist.artistName), text, re.IGNORECASE):
                    return_list.append(artist)
            except Exception as e:
                print e
                pass

        return return_list

    def scan_country(self, text):
        return_list = []
        igonre_country_code = ['DJ']
        all_country = Country.objects.all()
        for country in all_country:
            try:
                if re.search(r'\b{}\b'.format(country.name), text, re.IGNORECASE) or \
                    (re.search(r'\b{}\b'.format(country.code), text) and country.code not in igonre_country_code):
                    return_list.append(country)
            except Exception as e:
                print e
                pass

        return return_list


class NewsPaperBaseParser(ScanArtistInText):

    def get_article_urls(self, url):
        all_articles = newspaper.build(u'{}'.format(url), memoize_articles=False)
        return all_articles.article_urls()

    def get_article_data(self, url):

        # Strip The # from the url
        url = url.split("#")[0]

        if News.objects.filter(url=url).exists():
            return {}

        article_dict = {}
        article = Article(url)
        article.download()
        article.parse()

        if article.text and article.title:
            article_dict["text"] = unidecode(article.text)
            article_dict["title"] = unidecode(article.title)
            article_dict["date"] = article.publish_date
            article_dict["url"] = article.url

            # https://runthetrap.com/2018/02/21/dnmo-releases-new-single-better/

            article_dict["image"] = article.top_image
            article_dict["artists"] = self.scan_artist(article_dict["text"])
            article_dict["country"] = self.scan_country(article_dict["text"])
            return article_dict
        else:
            return {}

    def parser_all_url_data(self, urllist):
        return_list = []
        temp_dict = {}
        count = 0
        for article in urllist:
            print "Scrapping- {}".format(article)
            article_data = self.get_article_data(article)
            if article_data:
                return_list.append(article_data)
            else:
                pass
            count += 1
        return return_list,

    def build_data_from_source(self, url, baked_urls=None):
        all_articles = self.get_article_urls(url)
        if baked_urls:
            all_articles.extend(baked_urls)
        try:
            # all_scrap_data, count = self.parser_all_url_data(all_articles)
            all_scrap_data = self.parser_all_url_data(all_articles)
            return all_scrap_data
        except:
            return None
