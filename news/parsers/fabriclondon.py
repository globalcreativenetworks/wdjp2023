import requests
from urlparse import urlparse
from bs4 import BeautifulSoup
from unidecode import unidecode
from datetime import date

from news.parsers.base_newspaper import NewsPaperBaseParser, SavedNewsToDB
from news.models import News, NewsSource
from social_data.models import DataLog

class FabricLondonParser(NewsPaperBaseParser, SavedNewsToDB):
    today_date = date.today()

    def scrap(self):
        count = 0
        newssource = NewsSource.objects.get(sourceName="FabricLondon")
        sourceUrl = newssource.sourceUrl
        all_urls = self.get_scrapping_urls()
        for url in all_urls:
            all_data = self.build_data_from_source(url)
            self.save_news_data(all_data, newssource)
            count += 1

        fabric_london_parser_log , created = DataLog.objects.get_or_create(created=FabricLondonParser.today_date)

        if fabric_london_parser_log.news_fabriclondon:
            fabric_london_parser_log_count = int(dj_times_log.news_djtimes)
            fabric_london_parser_log_count += count
            fabric_london_parser_log.news_fabriclondon = fabric_london_parser_log_count
        else:
            fabric_london_parser_log.news_fabriclondon = count

        fabric_london_parser_log.save()


    def get_scrapping_urls(self):
        newssource = NewsSource.objects.get(sourceName="FabricLondon")
        parsed_uri = urlparse(newssource.sourceUrl)
        base_url = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        return_url = [newssource.sourceUrl]
        for i in range(2, 11):
            temp_url = base_url + "blog/category/news?page={}".format(i)
            return_url.append(temp_url)
        return return_url

    def fetch_custom_text_data(self, url):
        content = requests.get(url).content
        soup = BeautifulSoup(content)
        text = soup.find('article').text
        if text:
            return text
        else:
            return ""

    def save_news_data(self, all_data, newssource):
        if all_data:
            for scrap_news in all_data:
                if not News.objects.filter(url=scrap_news["url"]):
                    if scrap_news["artists"]:
                        custom_text = self.fetch_custom_text_data(scrap_news["url"])
                        if custom_text:
                            scrap_news["text"] = unidecode(custom_text)
                        news = News.objects.create(
                            source = newssource,
                            url = scrap_news["url"],
                            title = scrap_news["title"],
                            text = scrap_news["text"],
                        )
                        news.save()

                        if scrap_news["date"]:
                            news.publish_date = scrap_news["date"]
                        else:
                            news.publish_date = news.created

                        if scrap_news["image"]:
                            news.imageurl = scrap_news["image"]

                        for artist in scrap_news["artists"]:
                            news.artists.add(artist)
                        news.save()
