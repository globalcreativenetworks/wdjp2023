from urlparse import urlparse
from datetime import date

from news.parsers.base_newspaper import NewsPaperBaseParser, SavedNewsToDB
from news.models import News, NewsSource

class ZeroHedgeParser(NewsPaperBaseParser, SavedNewsToDB):
    today = date.today()

    def scrap(self):
        newssource = NewsSource.objects.get(sourceName="ZeroHedge")
        sourceUrl = newssource.sourceUrl
        all_urls = self.get_scrapping_urls()
        count = 0
        for url in all_urls:
            all_data = self.build_data_from_source(url)
            self.save_news_data(all_data, newssource)
            count += 0

        news_zero_hedge_log, created = DataLog.objects.get_or_create(created=ZeroHedgeParser.today_date)

        if news_zero_hedge_log.news_zerohedge:
            news_zero_hedge_log_count = int(news_zero_hedge_log.news_zerohedge)
            news_zero_hedge_log_count += count
            news_zero_hedge_log.news_zerohedge = news_zero_hedge_log_count
        else:
            news_zero_hedge_log.news_zerohedge = count

        news_zero_hedge_log.save()


    def get_scrapping_urls(self):
        newssource = NewsSource.objects.get(sourceName="ZeroHedge")
        parsed_uri = urlparse(newssource.sourceUrl)
        base_url = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        return_url = [newssource.sourceUrl]
        for i in range(2, 11):
            temp_url = base_url + "?page={}".format(i)
            return_url.append(temp_url)
        return return_url
