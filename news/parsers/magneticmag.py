from urlparse import urlparse
from datetime import date

from news.parsers.base_newspaper import NewsPaperBaseParser, SavedNewsToDB
from news.models import News, NewsSource
from social_data.models import DataLog

class MagneticMagParser(NewsPaperBaseParser, SavedNewsToDB):
    today_date = date.today()

    def scrap(self):
        newssource = NewsSource.objects.get(sourceName="MagneticMag")
        sourceUrl = newssource.sourceUrl
        all_urls = self.get_scrapping_urls()
        count = 0
        for url in all_urls:
            all_data = self.build_data_from_source(url)
            self.save_news_data(all_data, newssource)
            count += 1

        magnetic_mag_parser_log , created = DataLog.objects.get_or_create(created=MagneticMagParser.today_date)

        if magnetic_mag_parser_log.news_magneticmag:
            magnetic_mag_parser_log_count = int(magnetic_mag_parser_log.news_magneticmag)
            magnetic_mag_parser_log_count += count
            magnetic_mag_parser_log.news_magneticmag = magnetic_mag_parser_log_count
        else:
            magnetic_mag_parser_log.news_magneticmag = count

        magnetic_mag_parser_log.save()


    def get_scrapping_urls(self):
        newssource = NewsSource.objects.get(sourceName="MagneticMag")
        parsed_uri = urlparse(newssource.sourceUrl)
        base_url = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        return_url = [newssource.sourceUrl]
        for i in ["news", "music", "culture", "gear", "events"]:
            temp_url = base_url + "{}/".format(i)
            return_url.append(temp_url)
        return return_url
