from urlparse import urlparse

from news.parsers.base_newspaper import NewsPaperBaseParser, SavedNewsToDB
from news.models import News, NewsSource
from datetime import date

from social_data.models import DataLog


class BillBoardParser(NewsPaperBaseParser, SavedNewsToDB):
    today_date = date.today()

    def scrap(self):
        count = 0
        newssource = NewsSource.objects.get(sourceName="BillBoard")
        sourceUrl = newssource.sourceUrl
        all_urls = self.get_scrapping_urls()
        for url in all_urls:
            all_data = self.build_data_from_source(url)
            self.save_news_data(all_data, newssource)
            count += 1

        billboard_log, created = DataLog.objects.get_or_create(created=BillBoardParser.today_date)

        if billboard_log.news_billboard:
            billboard_log_count = int(change_underground_log.news_billboard)
            billboard_log_count += count
            billboard_log.news_billboard = billboard_log_count
        else:
            billboard_log.news_billboard = count
        change_underground_log.save()

    def get_scrapping_urls(self):
        newssource = NewsSource.objects.get(sourceName="BillBoard")
        parsed_uri = urlparse(newssource.sourceUrl)
        base_url = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        return_url = [newssource.sourceUrl]
        source_list = ["pop", "festivals", "hip-hop-rap-r-and-b", "dance", "style", "country", "latin", "rock", "hub/broadway", "k-pop"]
        for i in source_list:
            temp_url = base_url + "{}".format(i)
            return_url.append(temp_url)
        return return_url
