from urlparse import urlparse
from datetime import date

import newspaper

from news.parsers.base_newspaper import NewsPaperBaseParser, SavedNewsToDB
from news.models import News, NewsSource
from social_data.models import DataLog

class RunTheTrap(NewsPaperBaseParser, SavedNewsToDB):
    today_date = date.today()

    def scrap(self):
        newssource = NewsSource.objects.get(sourceName="RunTheTrap")
        sourceUrl = newssource.sourceUrl
        all_urls = self.get_scrapping_urls()
        count = 0

        for url in all_urls:
            all_data = self.build_data_from_source(url)
            self.save_news_data(all_data, newssource)
            count += 1

        news_run_the_trap_log, created = DataLog.objects.get_or_create(created=RunTheTrap.today_date)

        if news_run_the_trap_log.news_run_the_trap:
            news_run_the_trap_log_count = int(news_run_the_trap_log.news_run_the_trap)
            news_run_the_trap_log_count += count
            news_run_the_trap_log.news_run_the_trap = news_run_the_trap_log_count
        else:
            news_run_the_trap_log.news_run_the_trap = count

        news_run_the_trap_log.save()


    def get_scrapping_urls(self):
        newssource = NewsSource.objects.get(sourceName="RunTheTrap")
        parsed_uri = urlparse(newssource.sourceUrl)
        base_url = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        # return_url = [newssource.sourceUrl]
        return_url = []
        for i in range(1, 11):
            temp_url = base_url + "news/page/{}/".format(i)
            return_url.append(temp_url)
        return return_url

    def get_article_urls(self, url):
        all_articles = newspaper.build(u'{}'.format(url), memoize_articles=False)
        all_articles = all_articles.article_urls()
        new_list = []
        for article in all_articles:
            new_list.append(article)
        return new_list
