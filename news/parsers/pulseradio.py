import requests
from urlparse import urlparse
from bs4 import BeautifulSoup
from datetime import date

from news.parsers.base_newspaper import NewsPaperBaseParser, SavedNewsToDB
from news.models import News, NewsSource
from social_data.models import DataLog

class PulseRadioParser(NewsPaperBaseParser, SavedNewsToDB):
    BASE_URL = "https://pulseradio.net"
    today_date = date.today()

    def scrap(self):
        newssource = NewsSource.objects.get(sourceName="PulseRadio")
        sourceUrl = newssource.sourceUrl
        baked_urls = self.get_baked_urls()
        all_data = self.build_data_from_source(sourceUrl, baked_urls=baked_urls)
        count = self.save_news_data(all_data, newssource)

        if count:
            news_pulse_radio_log, created = DataLog.objects.get_or_create(created=PulseRadioParser.today_date)

            if news_pulse_radio_log.news_pulse_radio:
                news_pulse_radio_log_count = int(news_pulse_radio_log.news_pulse_radio)
                news_pulse_radio_log_count += count
                news_pulse_radio_log.news_pulse_radio = news_pulse_radio_log_count
            else:
                news_pulse_radio_log.news_residentadvisor = count

            news_pulse_radio_log.save()

    def get_scrapping_urls(self):
        newssource = NewsSource.objects.get(sourceName="PulseRadio")
        return_url = [newssource.sourceUrl]

    def get_baked_urls(self):
        newssource = NewsSource.objects.get(sourceName="PulseRadio")
        parsed_uri = urlparse(newssource.sourceUrl)
        base_url = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        return_url = []
        for i in range(1,15):
            temp_url = base_url + "home/load_more/filter:whatshot/page:{}".format(i)
            return_url.append(temp_url)
        baked_urls = []
        for url in return_url:
            response = requests.get(url).content
            if response:
                soup = BeautifulSoup(response)
                for div in soup.findAll('div', {'class': 'box-main'}):
                    link = div.find('a')
                    if link:
                        link = div.find('a')['href']
                        baked_urls.append(self.BASE_URL + link )
        return baked_urls
