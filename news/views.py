import json
import datetime
from django.shortcuts import render
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.conf import settings

from accounts.decorators import *
from geography.models import Country

from accounts.constants import NEWS_SOURCES
from news.models import NewsSource, News, WdjpNews
from artist.models import Artist, Genre
from news.forms import WdjpNewsForm, WdjpNewsImageForm
from accounts.models import Applications


@login_required
# @check_plan('Trial','Premium')
@check_plan('Trial','Promoter', 'Artist/DJ', 'Agent')
def news(request):
    limit = 10
    start = 0
    artist_name = request.GET.get('artist_name', '')
    query = request.GET.get('query', '')
    genre = request.GET.get('genre')
    country = request.GET.get('country')
    city = request.GET.get('city')

    if country:
        country_obj = Country.objects.filter(code=country)[0]

    profile_obj = Profile.objects.get(user=request.user)
    fav_artists = profile_obj.favorite_artist.all()
    email_frequency_value = profile_obj.email_frequency
    email_frequency_value_list = email_frequency_value.split(',')

    try:
        artist = Artist.objects.get(artistName=artist_name)
    except:
        artist = None
    all_news = News.objects.all().order_by('-publish_date')

    if genre:
        if genre == "edm":
            edm_genre = Genre.objects.get(genreName="EDM")
            edm_artist = Artist.objects.filter(genre=edm_genre)
            all_news = News.objects.filter(artists__in=edm_artist).order_by('-publish_date').distinct()

        if genre == "bass trap":
            bass_trap_genre = Genre.objects.get(genreName="Bass/Trap")
            bass_trap_artist = Artist.objects.filter(genre=bass_trap_genre)
            all_news = News.objects.filter(artists__in=bass_trap_artist).order_by('-publish_date').distinct()

        if genre == "Techno":
            techno_genre = Genre.objects.get(genreName="Techno")
            techno_artist = Artist.objects.filter(genre=techno_genre)
            all_news = News.objects.filter(artists__in=techno_artist).order_by('-publish_date').distinct()

        if genre == "Trance":
            trance_genre = Genre.objects.get(genreName="Trance")
            trance_artist = Artist.objects.filter(genre=trance_genre)
            all_news = News.objects.filter(artists__in=trance_artist).order_by('-publish_date').distinct()

        # if genre == "desert":
        #     desert_genre = Genre.objects.get(genreName="DESERT TRIBAL BEATS")
        #     desert_artist = Artist.objects.filter(genre=desert_genre)
        #     all_news = News.objects.filter(artists__in=desert_artist).order_by('-publish_date').distinct()

        # if genre == "edm":
        #     edm_genre = Genre.objects.get(genreName="EDM")
        #     edm_artist = Artist.objects.filter(genre=edm_genre)
        #     all_news = News.objects.filter(artists__in=edm_artist).order_by('-publish_date').distinct()[:15]

    if artist:
        all_news = all_news.filter(artists=artist)

    if query:
        all_news = all_news.filter(Q(title__icontains=query) | Q(text__icontains=query))

    # if country:
    #     all_news = all_news.filter(country=country_obj)
    #
    # if city:
    #     all_news = all_news.filter(Q(title__icontains=city) | Q(text__icontains=city))

    if request.is_ajax():
        start = request.GET.get('start', '')
        start = int(start)

        if all_news.count() > start + limit:
            more = True
        else:
            more = False


        all_news = all_news[start: start + limit]

        context = {
            'all_news': all_news,
        }
        template = render_to_string("news/load_more_news.html", context ,context_instance=RequestContext(request))

        response = {
            'data': template,
            'more': more,
            'start': start + limit,
        }

        return HttpResponse(json.dumps(response), content_type="application/json")

    countries = Country.objects.all()

    context = {
        'total_result': all_news.count(),
        'all_news': all_news[start:start + limit],
        'artist_name': artist_name,
        'start': start + limit,
        'query': query,
        'email_frequency_value': email_frequency_value,
        "email_frequency_value_list": email_frequency_value_list,
        'countries':countries,
        'city':city,
        'fav_artists':fav_artists
    }

    if country:
        context.update({"country_obj":country_obj})

    return render(request, 'news/all_news.html',context)


def email_frequency(request):
    profile_obj = Profile.objects.get(user=request.user)
    if request.method == "POST":
        email_frequency_choice = request.POST.getlist("email_frequency")
        if len(email_frequency_choice) >= 1 :
            if email_frequency_choice:
                profile_obj.email_frequency = ",".join(email_frequency_choice)
                profile_obj.save()
        else:
            profile_obj.email_frequency = " "
            profile_obj.save()

    # return redirect(reverse('news'))
    return redirect(request.META.get('HTTP_REFERER'))


@login_required
def create_news(request):
    if not request.user.is_superuser:
        return redirect("/")

    context = {}

    form = WdjpNewsForm(request.POST or None, request.FILES)

    image_form = WdjpNewsImageForm(request.POST or None, request.FILES)

    artists = Artist.objects.all().values_list('artistID', 'artistName')

    applications = Applications.objects.all().values_list('id', 'application_name')

    context.update({
        "form": form,
        "image_form": image_form,
        "artists": artists,
        "applications": applications
        })

    if request.method == "POST":

        if form.is_valid():
            is_visible = request.POST.get("is_visible")

            if is_visible == None:
                is_visible = False

            wdjp_news = WdjpNews.objects.create(
                title=form.cleaned_data["title"],
                text =form.cleaned_data["text"],
                highlighted_text=form.cleaned_data["highlighted_text"],
                type=form.cleaned_data["type"],
                is_visible=is_visible,
                author=request.user,
                url = form.cleaned_data["url"]
            )

            tag_artists_id = request.POST.getlist("tag_artists", " ")

            if tag_artists_id:
                tag_artists_id = [int(id) for id in tag_artists_id ]
                for id in tag_artists_id:
                    wdjp_news.artists.add(Artist.objects.get(artistID=id))


            tag_apps_id = request.POST.getlist("tag_apps", " ")

            if tag_apps_id:
                tag_apps_id = [int(id) for id in tag_apps_id ]
                for id in tag_apps_id:
                    wdjp_news.news_for.add(Applications.objects.get(id=id))


            if request.FILES:
                wdjp_news.image = request.FILES.get("image")

            wdjp_news.save()

            return redirect("wdjp_news")

    return render(request, "news/create_news.html", context)

# @login_required
# def create_news(request):
#     if not request.user.is_superuser:
#         return redirect("/")
#     context = {}

#     form = WdjpNewsForm(request.POST or None, request.FILES)

#     image_form = WdjpNewsImageForm(request.POST or None, request.FILES)

#     artists = Artist.objects.all().values_list('artistID', 'artistName')

#     applications =  Applications.objects.all().values_list('id', 'application_name')

#     context.update({
#         "form":form,
#         "image_form": image_form,
#         "artists": artists,
#         "applications": applications,

#     })

#     if request.method == "POST":

#         if form.is_valid():

# 			is_visible = request.POST.get("is_visible")

# 			if is_visible == None:
# 				is_visible = False

# 			wdjp_news = WdjpNews.objects.create(
# 				title = form.cleaned_data["title"],
# 				text=form.cleaned_data["text"],
# 				highlighted_text=form.cleaned_data["highlighted_text"],
# 				type=form.cleaned_data["type"],
# 				is_visible = is_visible,
# 				author=request.user
# 			)

#             tag_artists_id = request.POST.getlist("tag_apps", )


#             tag_apps_id =  request.POST.getlist("tag_apps", )

# 			if tag_artists_id:
# 				tag_artists_id = [int(id) for id in tag_artists_id]
# 				for id in tag_artists_id:
# 					wdjp_news.artists.add(Artist.objects.get(artistID=id))

# 			if request.FILES:
# 				wdjp_news.image = request.FILES.get("image")

# 			wdjp_news.save()
# 			return redirect("wdjp_news")

#     return render(request, "news/create_news.html", context)

@check_plan('Trial','Promoter', 'Artist/DJ', 'Agent')
def wdjp_news(request):
    context = {}

    application = Applications.objects.filter(application_name="WheredjsPlay").first()

    wdjp_news = WdjpNews.objects.filter(is_visible=True).order_by('-published_date')

    context.update({
        "wdjp_news": wdjp_news,
        "site_url": settings.SITE_URL,
    })

    return render(request, "news/wdjp_news.html", context)


def news_detail(request, id):
    context = {}
    limit = 21
    start = 0
    application = Applications.objects.filter(application_name="WheredjsPlay").first()

    wdjp_news_all = WdjpNews.objects.filter(news_for=application, is_visible=True).order_by('-published_date')
    wdjp_news_all = wdjp_news_all[start: start + limit]
    wdjp_news = WdjpNews.objects.get(id=id)
    tag_artists = wdjp_news.artists.all()

    context.update({
        "wdjp_news": wdjp_news,
        "wdjp_news_all":wdjp_news_all,
        "site_url": settings.SITE_URL,
        "tag_artists": tag_artists
    })

    return render(request, "news/news_detail.html", context)


@login_required
def news_edit(request, id):
    if not request.user.is_superuser:
        return redirect("/")
    context = {}

    wdjp_news = WdjpNews.objects.get(id=id)

    form = WdjpNewsForm(request.POST or None, request.FILES, instance=wdjp_news)

    image_form = WdjpNewsForm(request.POST or None, request.FILES, instance=wdjp_news)

    artists = Artist.objects.all().values_list('artistID', 'artistName')

    selected_artist = wdjp_news.artists.all().values_list('artistID', 'artistName')

    applications =  Applications.objects.all().values_list('id', 'application_name')

    selected_applications = wdjp_news.news_for.all().values_list('id', 'application_name')

    if request.method == "POST":
        if form.is_valid():
            wdjp_news = form.save(commit=False)
            wdjp_news.author = request.user
            wdjp_news.published_date = timezone.now()

            tag_artists_id = request.POST.getlist('tag_artists')

            if tag_artists_id:
                tag_artists_id = [int(id) for id in tag_artists_id]

                tag_artists = wdjp_news.artists.all()

                for artist in tag_artists:
                    wdjp_news.artists.remove(artist)

                for id in tag_artists_id:
                    wdjp_news.artists.add(Artist.objects.get(artistID=id))

            tag_apps_id = request.POST.getlist('tag_apps', '')

            if tag_apps_id:

                tag_apps_id = [int(id) for id in tag_apps_id]

                tag_apps = wdjp_news.news_for.all()

                for apps in tag_apps:
                    wdjp_news.news_for.remove(apps)

                for id in tag_apps_id:
                    wdjp_news.news_for.add(Applications.objects.get(id=id))

            wdjp_news.save()

            return redirect(reverse('news_detail', kwargs={"id":wdjp_news.id}))

    else:
        form = WdjpNewsForm(instance=wdjp_news)

        context.update({
            "news":wdjp_news,
            "artists": artists,
            "form": form,
            "image_form": image_form,
            "selected_artist": selected_artist,
            "applications": applications,
            "selected_applications": selected_applications
        })

        return render(request, "news/news_edit.html", context)


@login_required
def delete_news(request, news_id):
    if not request.user.is_superuser:
        return redirect("/")

    wdjp_news = WdjpNews.objects.get(id=news_id)
    wdjp_news.delete()
    return redirect(reverse('wdjp_news'))


# def news_edit(request, id):
# 	context = {}
#
# 	wdjp_news = WdjpNews.objects.get(id=id)
# 	form = WdjpNewsForm(request.POST or None, request.FILES, instance=wdjp_news)
# 	image_form = WdjpNewsImageForm(request.POST or None, request.FILES, instance=wdjp_news )
# 	artists = Artist.objects.all().values_list('artistID', 'artistName')
# 	selected_artist = wdjp_news.artists.all().values_list('artistID', 'artistName')
#
# 	if request.method == "POST":
#
# 		if form.is_valid():
# 			wdjp_news = form.save(commit=False)
# 			wdjp_news.author = request.user
# 			wdjp_news.published_date = timezone.now()
#
# 			tag_artists_id = request.POST.getlist('tag_artists')
#
# 			if tag_artists_id:
# 				tag_artists_id = [int(id) for id in tag_artists_id]
#
# 				for id in tag_artists_id:
# 					wdjp_news.artists.add(Artist.objects.get(artistID=id))
#
# 			wdjp_news.save()
#
# 			return redirect(reverse('news_detail', kwargs={"id":wdjp_news.id}))
#
# 	else:
# 		form = WdjpNewsForm(instance=wdjp_news)
#
# 		context.update({
# 			"news": wdjp_news,
# 			"artists": artists,
# 			'form': form,
# 			"image_form": image_form,
# 			"selected_artist": selected_artist
# 		})
#
# 		return render(request, "news/news_edit.html", context)
# wdjp_news = WdjpNews.objects.get(id=id)
# form = WdjpNewsForm(request.POST, instance=wdjp_news)
# if request.method == "POST":
# 	if form.is_valid():
# 		wdjp_news = form.save(commit=False)
# 		wdjp_news.author = request.user
# 		wdjp_news.published_date = timezone.now()
# 		wdjp_news.save()
# 		return redirect(reverse('news/wdjp_news', kwargs={"id":id}))
# else:
# 	form = WdjpNewsForm(instance=wdjp_news)
# 	return render(request, 'news/news_edit.html', {'form': form})


def get_search_news(request, news_list, news_type, query='', page=1, limit=10):
    news_list = news_list
    page = page

    paginator = Paginator(news_list, limit)
    
    try:
        all_news = paginator.page(page)
    except PageNotAnInteger:
        all_news = paginator.page(1)
    except EmptyPage:
        all_news = paginator.page(paginator.num_pages)

    context = {
        'all_news': all_news,
        'news_type': news_type,
        'query': query,
    }
    template = render_to_string("news/load_more_news.html", context = context)
    return template


@login_required
@check_plan('Trial', 'Artist/DJ', 'Promoter', 'Agent')
def search_news(request):

    limit = 10
    start = 0
    query=''
    # news_sources = NEWS_SOURCES
    total = 0
    page = request.GET.get('page', 1)
    artists_count = Artist.objects.all().count()
    news_count = News.objects.all().count()
    source_count = NewsSource.objects.all().count()

    # Past Events and Future News Variables
    today_date = datetime.datetime.now()
    requested_news = []
    requested_news_future = []
    requested_news_past = []

    if request.GET.get('query'):
        query = request.GET.get('query','')
        query_str = Q(title__icontains=query) | Q(text__icontains=query)
        requested_news = News.objects.filter(query_str).order_by('-publish_date')

    if request.is_ajax():
        news_type = request.GET.get('news_type', 'former')
        news_template = ""
        if news_type == 'upcoming':
            requested_news_future = requested_news.filter(publish_date__gte=today_date).order_by('-publish_date')
            news_template = get_search_news(request, requested_news_future, news_type, query, page, limit)
        else:
            requested_news_past = requested_news.filter(publish_date__lte=today_date).order_by('-publish_date')
            news_template = get_search_news(request, requested_news_past, news_type, query, page, limit)
        
        response = {
            'data': news_template,
            'news_type': news_type,

        }
        return HttpResponse(json.dumps(response), content_type="application/json")
    else: 
        context = dict()
        if len(requested_news) > 0:
            requested_news_future = requested_news.filter(publish_date__gte=today_date).order_by('-publish_date')
            requested_news_past = requested_news.filter(publish_date__lte=today_date).order_by('-publish_date')
            
            total = requested_news_future.count() + requested_news_past.count()
    
        future_news_template = get_search_news(request, requested_news_future,'upcoming', query, page, limit)
        future_response = {
                'data': future_news_template,
            }
        news_future = future_response
    
        past_news_template = get_search_news(request, requested_news_past,'former', query, page, limit)
        past_response = {
                'data': past_news_template,
            }
        news_past = past_response

        # Added all news
        all_news = News.objects.all().order_by('-publish_date')[:5]
        template = render_to_string("news/load_more_news.html", {"all_news": all_news} ,context_instance=RequestContext(request))

        context.update({
        "news_future": news_future,
        "news_past": news_past,
        "news_future_count":len(requested_news_future),
        "news_past_count":len(requested_news_past),
        "news_count": news_count,
        "artists_count": artists_count,
        "query": query,
        "total": total,
        'source_count': source_count,
        "all_news": template,
        })

        return render(request, 'news/news_search.html', context)


@login_required
@check_plan('Trial', 'Artist/DJ', 'Promoter', 'Agent')
def my_news_feed(request):
    context = dict()
    profile_obj = Profile.objects.get(user=request.user)
    fav_artists = profile_obj.favorite_artist.all()
    email_frequency_value = profile_obj.email_frequency
    email_frequency_value_list = email_frequency_value.split(',')

    context.update({
        "fav_artists": fav_artists,
        'email_frequency_value': email_frequency_value,
        "email_frequency_value_list": email_frequency_value_list,
    })

    return render(request, 'news/news_feed.html', context)
