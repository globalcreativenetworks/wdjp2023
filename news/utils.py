from news.models import News
from django.db.models import Q


def get_local_news(request, count=10):
    if request.COOKIES.get('user_session_city'):
        city = request.COOKIES.get('user_session_city')
        all_news = News.objects.filter(
            Q(title__icontains=city) | Q(text__icontains=city)
        )

        if all_news.exists():
            return all_news.order_by('-publish_date').distinct()[:count]

    return []


def get_artist_country_news(artist, country, count=10, multiple_artists=False):

    if multiple_artists:
        all_news = News.objects.filter(
            artists__artistID__in=artist,
            country__name=country,
        ).order_by('-publish_date')[:count]
    else:
        all_news = News.objects.filter(
            artists__artistID=artist,
            country__name=country,
        ).order_by('-publish_date')[:count]

    return all_news
