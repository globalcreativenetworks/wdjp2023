# **Pyadmin** #

This Project is use to fetch the information for **wheredjsplay.com**. Technologies used in the project are **Python2.7** and **Django1.9**.

**Steps to Setup project locally:**

1. Clone the project repository using SSH access and command will be
```
#!shell

git clone git@bitbucket.org:wjds/pyadmin.git
```

2. Now create a virtual environment for the Pyadmin.

3. Activate the virtual Environment and make the project repository as your current directory
```
#!shell

virtualenv --no-site-packages venv
```

4. Install all other dependencies by running
```
#!shell

pip install -r requirements.txt
```

5. Ask admin for a database dump as all tables are not covered under syncdb.

7. Now you should be able to run celery using
```
#!shell

python manage.py celeryd
```

6. In new terminal you should be able to run server using
```
#!shell

python manage.py runserver
```

If you are still facing any problem, Please contact the admins.
