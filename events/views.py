import datetime
import json
import math
import time
from collections import OrderedDict

import pycountry
# from datetime import timedelta, date
from colorhash import ColorHash
from dateutil.relativedelta import relativedelta
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db.models import Q
from django.shortcuts import get_object_or_404, render
from django.template import RequestContext
from django.template.loader import render_to_string
from django.views.generic.edit import View

from accounts.decorators import *
from accounts.utils import SendEmail
from artist.models import Artist, ArtistGenre, ArtistWebsite, Genre, Source
from events.forms import (BookingRequestForm, EventBookingRequestForm,
                          EventCommentsForm, EventNotesForm, VenueCommentsForm,
                          VenueNotesForm, EventTypeTemplateForm)
from events.get_venue_data import get_data, getImages
from events.models import (BookedArtistForUserEvent, DjMaster, DjVenues, Event,
                           EventBookingRequest, EventComment, EventNote,
                           OfferedArtistForUserEvent, ShortlistedEvent,
                           ShortlistedVenue, VenueComment, VenueNote,
                           WpMyEvents, EventTypeTemplate)
from events.utils import (get_artist_spotify_likes, get_artist_youtube_likes,
                          get_event_recommended_artists,
                          save_event_venue_lat_long, get_artist_instagram_followers,
                          get_artist_latest_followers)
from geography.models import City, Country
from home.views import (get_artist_event_near_by_to_city,
                        get_artist_most_recent_event)
from news.models import News
from profiles.models import Coupon, ProfileConnections
from scrape.models import ScrapePhoto
from scrapper.database.database_interface import DBInterface
from social_data.models import FacebookLike, TimeTrends

limit = 10
past_event_ids = []


@login_required
@check_email_validated
# @check_plan('Trial','Basic','Premium')
@check_plan('Trial', 'Promoter')
def my_events(request):
    response = {"status": False, "errors": []}
    context = {}

    if request.is_ajax():
        event_id = request.POST['event_id']
        user = request.user
        response = {}
        response['status'] = False
        event = WpMyEvents.objects.filter(event_id=event_id, user_id=user.id)
        if event.exists():
            event = event[0]
            context.update({"event": event})

            all_user_shortlist_artist = event.shortlisted_artists.all(
            ).values_list('artistID', flat=True)

            booked_artist = event.invited_users.all()
            booked_artist_id = []

            for artist in booked_artist:
                booked_artist_id.append(artist.artistID)

            artists = Artist.objects.all().exclude(artistID__in=booked_artist_id)

            context.update(
                {"artists": artists, "all_user_shortlist_artist": all_user_shortlist_artist})

            template = render_to_string(
                "events/_event_artists.html", context, context_instance=RequestContext(request))

            response['template'] = template
            response['status'] = True

        return HttpResponse(json.dumps(response), content_type="applicaton/json")
    else:
        all_events = WpMyEvents.objects.filter(
            user_id=request.user.id).order_by('-created_on')
        event_count = all_events.count()

        context.update({
            "all_events": all_events,
            "event_count": event_count,
        })

        return render(request, "events/my_events.html", context)


@login_required
@check_email_validated
def assign_shorlisted_artist_to_events(request):
    context = {}
    event_id = request.POST.get('event_id')
    artist_ids = request.POST.getlist('assignedArtId[]')
    response = {}
    response['status'] = False
    artist_append_list = []

    if request.is_ajax():

        artist_ids = map(lambda x: int(x), artist_ids)
        artists = Artist.objects.filter(artistID__in=artist_ids)
        event = WpMyEvents.objects.filter(
            event_id=event_id, user_id=request.user.id)
        event_shortlisted = event[0].shortlisted_artists.all()
        # if artist_ids:
        #     artist = Artist.objects.filter(artistID__in = artist_ids)
        #     for art in artist:
        #         if art not in event_shortlisted:
        #             artist_append_list.append(art.artistName)
        #             response['artist_append_list'] = artist_append_list

        if event.exists():
            event = event[0]
            event.shortlisted_artists = artists
            event.save()
            response['status'] = True
            artist_shortlisted = event.shortlisted_artists.all(
            ).values_list('artistName', flat=True)

            event_shortlisted_artist = event.shortlisted_artists.all()

            context.update(
                {"artist_shortlisted": artist_shortlisted, "event_id": event.event_id})

            template = render_to_string(
                "events/_shortlist_artist.html", context, context_instance=RequestContext(request))
            response['template'] = template

    return HttpResponse(json.dumps(response), content_type="applicaton/json")


@login_required
@check_email_validated
# @check_plan('Trial','Basic','Premium')
def delete_event(request):

    if request.is_ajax():

        response = {"status": False, "errors": []}

        event_id = request.GET.get('event_id', '')

        event = WpMyEvents.objects.get(event_id=event_id)
        event.delete()
        response["status"] = True
        return HttpResponse(json.dumps(response))


@login_required
@check_email_validated
# @check_plan('Trial','Basic','Premium')
@check_plan('Promoter', 'Agent', 'Artist/DJ')
def booking_request(request):
    genres = Genre.objects.all()
    artists = Artist.objects.all()

    if request.method == "POST":
        genre_id = request.POST.get("genres", None)
        shortlist_artist_id = request.POST.getlist('shortlisted_artists')
        template_content = request.POST.get("event_type", "")

        form = BookingRequestForm(request.POST, user=request.user)
        data = {}

        print(form.errors)

        if form.is_valid():
            data['venue_name'] = form.cleaned_data['venue_name']
            data['capacity_of_venue'] = form.cleaned_data['capacity_of_venue']
            data['country'] = form.cleaned_data['country']
            data['nearest_airport'] = form.cleaned_data['nearest_airport']
            data['venue_address'] = form.cleaned_data['venue_address']
            data['estimate_finanical_offer'] = form.cleaned_data['estimate_finanical_offer']
            data['estimate_currency'] = form.cleaned_data['estimate_currency']
            data['estimate_amount'] = form.cleaned_data['estimate_amount']
            data['airport_to_venue_distance'] = form.cleaned_data['airport_to_venue_distance']
            data['suggested_hotels'] = form.cleaned_data['suggested_hotels']
            data['any_other_requested_dj'] = form.cleaned_data['any_other_requested_dj']
            data['contact_email'] = form.cleaned_data['any_other_requested_dj']
            data['ticket_price'] = form.cleaned_data['ticket_price']
            data['vip_sections'] = form.cleaned_data['vip_sections']
            data['recent_artwork_link'] = form.cleaned_data['recent_artwork_link']
            data['recent_video_link'] = form.cleaned_data['recent_video_link']
            data['artists_before'] = form.cleaned_data['artists_before']

            if form.cleaned_data['stage_time']:
                data['stage_time'] = datetime.time.strftime(
                    form.cleaned_data['stage_time'], '%I:%M %p')
            if form.cleaned_data['set_length']:
                data['set_length'] = datetime.time.strftime(
                    form.cleaned_data['set_length'], '%I:%M %p')
            data['stage_or_dj'] = form.cleaned_data['stage_or_dj']

            data['requested_show_date'] = form.cleaned_data['requested_show_date']
            data['rough_financial_offer'] = form.cleaned_data['rough_financial_offer']
            data['indoor_or_outdoor'] = form.cleaned_data['indoor_or_outdoor']
            data['min_age_req'] = form.cleaned_data['min_age_req']
            data['working_visa_provided'] = form.cleaned_data['working_visa_provided']
            data['vat_no'] = form.cleaned_data['vat_no']
            data['company_address'] = form.cleaned_data['company_address']
            data['company_city'] = form.cleaned_data['company_city']
            data['company_country'] = form.cleaned_data['company_country']
            
            # Creating template object
            name = "{} template for event {}".format(request.user.username, data['venue_name'][:10])
            event_type_template_obj = EventTypeTemplate(name=name, body=template_content, role="user", user=request.user)
            event_type_template_obj.save()

            form_data = json.dumps(data)

            wpmyevents = WpMyEvents.objects.create(data=form_data, content="", offersheet="",
                                                   event_name=form.cleaned_data["event_name"],
                                                   city=form.cleaned_data["event_city"],
                                                   user_id=request.user.id,
                                                   created_on=datetime.datetime.now(), event_type_template=event_type_template_obj)

            if genre_id:
                genre_obj = Genre.objects.get(genreID=genre_id)
                wpmyevents.genre.add(genre_obj)

            if shortlist_artist_id:
                shortlist_artist_id = [int(id) for id in shortlist_artist_id]
                for id in shortlist_artist_id:
                    wpmyevents.shortlisted_artists.add(Artist.objects.get(artistID=id))

            if form.cleaned_data["event_date"]:
                wpmyevents.event_date = form.cleaned_data["event_date"]
                wpmyevents.save()

            if form.cleaned_data['venue_address']:
                save_event_venue_lat_long(
                    form.cleaned_data['venue_address'], wpmyevents)

            messages.add_message(request, messages.SUCCESS,
                                 " A New Event Successfully Added")

            return redirect(reverse('my_events'))

        else:
            for key, value in form.errors.items():
                tmp = {'key': key, 'error': value.as_text()}
    else:
        form = BookingRequestForm(user=request.user)

    countries = Country.objects.all().order_by("name")

    context = {
        "artists": artists,
        "genres": genres,
        "form": form,
        "countries": countries,
        "age_loop": map(lambda x: str(x), range(22)),
    }

    if request.method == "GET":
        # Getting all templates created by admin
        event_types = EventTypeTemplate.objects.filter(role="admin")
        context.update({
            "event_types": event_types,
        })

        # Getting date
        add_event_date = request.GET.get("date", "")
        try:
            date = datetime.datetime.strptime(add_event_date, '%m/%d/%Y')
            print(date)
            if date.date() < datetime.datetime.now().date():
                raise("Can't take the date")
            context.update({
                "add_event_date": add_event_date,
            })
        except:
            print("This is in exception, Please check booking request function in events views")
            pass

        # Getting artist id
        picked_artist_id = request.GET.get("artist_id", "")

        if picked_artist_id and picked_artist_id != "":
            print(picked_artist_id)
            context.update({
                "picked_artist_id": int(picked_artist_id),
            })
        else:
            pass

    return render(request, "events/booking_request.html", context)


@login_required
@check_email_validated
# @check_plan('Trial','Basic','Premium')
@check_plan('Promoter')
def edit_event_detail(request, event_id):
    genres = Genre.objects.all()

    my_event = WpMyEvents.objects.get(event_id=event_id)

    data = {}
    try:
        if request.method == "POST":
            genre_id = request.POST.get("genres", " ")
            event_type_body = request.POST.get("event_body", "")

            data['venue_name'] = request.POST.get("venue name", " ")
            data['capacity_of_venue'] = request.POST.get(
                "capacity of venue", " ")
            data['country'] = request.POST.get("country", "")
            data['nearest_airport'] = request.POST.get("nearest airport", " ")
            data['venue_address'] = request.POST.get("venue address", " ")
            data['estimate_finanical_offer'] = request.POST.get(
                "estimate finanical offer", " ")
            data['estimate_currency'] = request.POST.get(
                "estimate currency", " ")
            data['estimate_amount'] = request.POST.get("estimate amount", " ")
            data['airport_to_venue_distance'] = request.POST.get(
                "airport to venue distance", " ")
            data['suggested_hotels'] = request.POST.get(
                "suggested hotels", " ")
            data['any_other_requested_dj'] = request.POST.get(
                "any other requested dj", "")
            data['contact_email'] = request.POST.get("contact email", " ")
            data['ticket_price'] = request.POST.get("ticket price", " ")
            data['vip_sections'] = request.POST.get("vip sections", " ")
            data['recent_artwork_link'] = request.POST.get(
                "recent artwork link", " ")
            data['recent_video_link'] = request.POST.get(
                "recent video link", " ")
            data['artists_before'] = request.POST.get("artists before", "")
            data['stage_time'] = request.POST.get("stage time", "")
            data['set_length'] = request.POST.get("set length", "")
            data['stage_or_dj'] = request.POST.get("stage or dj", "")
            data['requested_show_date'] = request.POST.get(
                "requested show date", "")
            data['rough_financial_offer'] = request.POST.get(
                "rough financial offer", "")
            data['indoor_or_outdoor'] = request.POST.get(
                "indoor or outdoor", "")
            data['min_age_req'] = request.POST.get("min age req", "")
            data['working_visa_provided'] = request.POST.get(
                "working visa provided", "")
            data['vat_no'] = request.POST.get("vat no", "")
            data['company_address'] = request.POST.get('company address')
            data['company_city'] = request.POST.get('company city')
            data['company_country'] = request.POST.get('company country')

            form_data = json.dumps(data)

            my_event.data = form_data

            event_date = request.POST.get("event_date", "")
            date = datetime.datetime.strptime(event_date, '%m/%d/%Y')
            formated_event_date = datetime.date.strftime(date, "%Y-%m-%d")

            my_event.event_date = formated_event_date

            my_event.event_name = request.POST.get("event_name", "")

            genre_id = request.POST.get("genres", None)

            for gen in my_event.genre.all():
                my_event.genre.remove(gen)

            event_name = request.POST.get("event_name", "")

            event_city = request.POST.get("event_city", "")

            if formated_event_date:
                my_event.event_date = formated_event_date

            if event_city:
                my_event.city = event_city

            if event_name:
                my_event.event_name = event_name

            if genre_id:
                genre = Genre.objects.get(genreID=genre_id)
                my_event.genre.add(genre)

            if data['venue_address']:
                save_event_venue_lat_long(data['venue_address'], my_event)

            my_event.user_id = request.user.id

            # Creating/Updating Event Template Type
            template = my_event.event_type_template
            try:
                template.body = event_type_body
            except:
                name = "{} template for event {}".format(request.user.username, data['venue_name'][:10])
                template = EventTypeTemplate(
                    name=name,
                    body=event_type_body,
                    role="user",
                    user=request.user
                )
            template.save()

            my_events.event_type_template = template
            my_event.save()
    except Exception as e:
        print e
        messages.add_message(request, messages.SUCCESS,
                             " Something is not going well on server ")

    return redirect(reverse("event_detail", kwargs={"event_id": my_event.event_id}))


@login_required
@check_email_validated
# @check_plan('Trial','Basic','Premium')
@check_plan('Promoter', 'Agent', 'Artist/DJ')
def update_booking_request(request, event_id):
    genres = Genre.objects.all()

    my_event = WpMyEvents.objects.get(event_id=event_id)

    if request.method == "POST":
        genre_id = request.POST.get("genres", None)
        event_type_body = request.POST.get("event_type", None)

        form = BookingRequestForm(
            request.POST, user=request.user, event=my_event)
        data = {}
        if form.is_valid():
            data['venue_name'] = form.cleaned_data['venue_name']
            data['capacity_of_venue'] = form.cleaned_data['capacity_of_venue']
            data['country'] = form.cleaned_data['country']
            data['nearest_airport'] = form.cleaned_data['nearest_airport']
            data['venue_address'] = form.cleaned_data['venue_address']
            data['estimate_finanical_offer'] = form.cleaned_data['estimate_finanical_offer']
            data['estimate_currency'] = form.cleaned_data['estimate_currency']
            data['estimate_amount'] = form.cleaned_data['estimate_amount']
            data['airport_to_venue_distance'] = form.cleaned_data['airport_to_venue_distance']
            data['suggested_hotels'] = form.cleaned_data['suggested_hotels']

            data['any_other_requested_dj'] = form.cleaned_data['any_other_requested_dj']
            data['contact_email'] = form.cleaned_data['any_other_requested_dj']
            data['ticket_price'] = form.cleaned_data['ticket_price']
            data['vip_sections'] = form.cleaned_data['vip_sections']
            data['recent_artwork_link'] = form.cleaned_data['recent_artwork_link']
            data['recent_video_link'] = form.cleaned_data['recent_video_link']
            data['artists_before'] = form.cleaned_data['artists_before']

            data['stage_time'] = form.cleaned_data['stage_time']

            data['set_length'] = form.cleaned_data['set_length']
            data['stage_or_dj'] = form.cleaned_data['stage_or_dj']

            data['requested_show_date'] = form.cleaned_data['requested_show_date']
            data['rough_financial_offer'] = form.cleaned_data['rough_financial_offer']
            data['indoor_or_outdoor'] = form.cleaned_data['indoor_or_outdoor']
            data['min_age_req'] = form.cleaned_data['min_age_req']
            data['working_visa_provided'] = form.cleaned_data['working_visa_provided']
            data['vat_no'] = form.cleaned_data['vat_no']
            data['company_address'] = form.cleaned_data['company_address']
            data['company_city'] = form.cleaned_data['company_city']
            data['company_country'] = form.cleaned_data['company_country']

            form_data = json.dumps(data)

            my_event.data = form_data
            my_event.event_date = form.cleaned_data["event_date"]

            my_event.event_name = form.cleaned_data["event_name"]
            my_event.city = form.cleaned_data["event_city"]
            my_event.user_id = request.user.id

            # Updating the template
            template = my_event.event_type_template
            try:
                template.body = event_type_body
            except:
                template = EventTypeTemplate(
                    name="duplicated from the event {}".format(my_event.event_id),
                    body=event_type_body,
                    role="user",
                    user=request.user
                )
            template.save()

            my_events.event_type_template = template

            my_event.save()

            if genre_id:
                genre_obj = Genre.objects.get(genreID=genre_id)
                for gen in my_event.genre.all():
                    my_event.genre.remove(gen)

                my_event.genre.add(genre_obj)

            if data['venue_address']:
                save_event_venue_lat_long(data['venue_address'], my_event)

            messages.add_message(request, messages.SUCCESS,
                                 "Your Event has been Updated ")

            return redirect(reverse('event_detail', kwargs={'event_id': my_event.event_id}))
    else:
        data = json.loads(my_event.data)
        data["event_date"] = my_event.event_date
        data["event_name"] = my_event.event_name
        data["event_city"] = my_event.city
        form = BookingRequestForm(initial=data, user=request.user)
    countries = Country.objects.all().order_by("name")
    shortlisted_artists = my_event.shortlisted_artists.all()
    artists = Artist.objects.all()
    event_types = EventTypeTemplate.objects.filter(role="admin")

    context = {
        "event_types": event_types,
        "artists": artists,
        "data": data,
        "genres": genres,
        "form": form,
        "my_event": my_event,
        "countries": countries,
        "all_shortlisted_artists": shortlisted_artists,
        "age_loop": map(lambda x: str(x), range(22)),
    }

    template_body = ""
    try:
        template_body = my_event.event_type_template.body
    except:
        print("event has no template tag connected to it")

    context.update({
        "event_type_body": template_body
    })

    return render(request, "events/booking_request.html", context)


@login_required
@check_email_validated
# @check_plan('Premium')
@check_plan('Promoter')
def event_booking_request_email(request, event_id, artist_id):
    context = {}

    user_profile = Profile.objects.get(user=request.user)
    user_profile_email = user_profile.user.email
    artist = Artist.objects.get(artistID=artist_id)

    event = WpMyEvents.objects.get(event_id=event_id)
    event_id = event.event_id
    event_date = event.event_date
    action = request.GET.get('action', None)
    my_events = request.GET.get('event', None)

    context.update({
        "user_profile": user_profile,
        "artist": artist,
        "event": event,
        "to": user_profile_email,
    })

    if event_date and event_id:
        if action == "offer":
            is_offersheet = True
            context.update({"is_offersheet": True})
        else:
            is_offersheet = False

        if not is_offersheet:
            subject = "Booking Request"
        else:
            subject = "Offer Sheet"

        context.update({
            "subject": subject
        })

    user_ref = format(request.user.id, "05")
    event_ref = format(int(event_id), "05")
    artist_id = format(int(artist_id), "05")
    ref_no = "BR-{}-{}-{}".format(user_ref, event_ref, artist_id)

    data = json.loads(event.data)

    context.update({
        "ref_no": ref_no,
        "data": data
    })

    if request.method == "POST":
        form = EventBookingRequestForm(request.POST or None)
        if form.is_valid():
            receiver = form.cleaned_data['to']
            subject = form.cleaned_data['subject']
            body = form.cleaned_data['body']

            event_booking = EventBookingRequest()
            event_booking.to = receiver
            event_booking.subject = subject
            event_booking.body = body
            event_booking.event = event
            event_booking.artist = artist
            event_booking.save()

            if action == "booking":
                if artist not in event.invited_users.all():
                    event.invited_users.add(artist)
                    # if artist in event.shortlisted_artists.all():
                    #     event.shortlisted_artists.remove(artist)
                    #     event.save()

            if action == "offer":
                if artist not in event.booked_artists.all():
                    BookedArtistForUserEvent.objects.create(
                        artist=artist,
                        event=event,
                    )
                    # event.invited_users.remove(artist)
                    # if artist in event.shortlisted_artists.all():
                    #     event.shortlisted_artists.remove(artist)
                    event.save()

            receiver_list = receiver.split(',')

            msg = SendEmail(request)

            msg.send_by_body(receiver_list, subject, body,)

            messages.add_message(request, messages.SUCCESS,
                                 "Email has been sent")

            if my_events == "from_shortlist":
                return redirect(reverse("shortlisted_artist", kwargs={"event_id": event_id}))

            if my_events == "from_my_event":
                return redirect(reverse("my_events"))

            return redirect(reverse('artist_detail', kwargs={'artist_id': artist_id}))

    else:
        body = render_to_string(
            "events/email/event_booking_email.html", context)
        form = EventBookingRequestForm()
        date = datetime.date.today()
        context.update({
            "form": form,
            "body": body,
            "date": date
        })

    return render(request, "events/event_booking_email.html", context)


@login_required
@check_email_validated
# @check_plan('Premium')
@check_plan('Promoter', 'Artist/DJ', 'Agent')
def invitation(request):
    # request_url = request.META.get('HTTP_REFERER')
    if request.method == "POST":
        booking_action = request.POST.get('booking_action', '')
        artist_id = request.POST.get('artist_id')
        action = request.POST.get('action', '')
        event_id = request.POST.get('event_id', '')
        user_profile = Profile.objects.get(user=request.user)
        artist = Artist.objects.get(artistID=artist_id)
        new_event_date = request.POST.get('new_event_date', '')

        # user_ref = format(request.user.id, "05")
        # event_ref = format(int(event_id), "05")
        # artist_id = format(int(artist_id), "05")
        # ref_no = "BR-{}-{}-{}".format(user_ref, event_ref, artist_id)

        if new_event_date and event_id:
            if action == "offer":
                is_offersheet = True
            else:
                is_offersheet = False

            event = WpMyEvents.objects.get(event_id=event_id)
            event.event_date = datetime.datetime.strptime(
                new_event_date, '%m/%d/%Y').date()
            event.save()

            if artist not in event.invited_users.all():
                event.invited_users.add(artist)

            # if not is_offersheet:
            #     subject = "Booking Request"
            # else:
            #     subject = "Offer Sheet"

        response = redirect(reverse("event_booking_request_email", kwargs={
                            "event_id": event_id, "artist_id": artist_id}))
        response['location'] += '?action={}'.format(action)

    # return redirect(reverse("event_booking_request_email", kwargs={"event_id":event_id, "artist_id":artist_id }))
    return response


def amsterdam_event(request):
    name = "amsterdam_dance_event"
    try:
        coupon = Coupon.objects.get(coupon_name=name)
        context = {"coupon": coupon}
    except:
        context = {}
        pass
    return render(request, "events/amsterdam_dance.html", context)


@login_required
@check_email_validated
def event_detail(request, event_id):
    genres = Genre.objects.all()
    context = {}
    event_id = event_id
    user = request.user
    # event = WpMyEvents.objects.get(event_id=event_id)
    event = get_object_or_404(WpMyEvents, event_id=event_id, user_id=user.id)

    event_data = event.data

    event_data_obj = json.loads(event_data)

    initial_information_from_dict = [
        'venue name',
        'venue address',
        'country',
        'capacity of venue',
    ]

    context.update({
        "event": event,
        "event_info": "event_info",
        "event_data_obj": event_data_obj,
        "initial_information_from_dict": initial_information_from_dict,
        "genres": genres,
        "age_loop": map(lambda x: str(x), range(22)),

    })

    return render(request, "events/event_detail.html", context)


@login_required
@check_email_validated
def shortlisted_artist(request, event_id):
    context = {}

    event_id = event_id

    user = request.user

    # event = WpMyEvents.objects.get(event_id=event_id, user_id=user.id)
    event = get_object_or_404(WpMyEvents, event_id=event_id, user_id=user.id)
    booked_artist_on_event_date = available_or_booked_on_date(event)
    context.update({
        "event": event,
        "booked_artist_on_event_date": booked_artist_on_event_date,
    })

    if request.method == "POST":
        shortlist_artist_id = request.POST.getlist('shortlisted_artists')

        for artist in event.shortlisted_artists.all():
            event.shortlisted_artists.remove(artist)

        if shortlist_artist_id:
            shortlist_artist_id = [int(id) for id in shortlist_artist_id]
            for id in shortlist_artist_id:
                event.shortlisted_artists.add(Artist.objects.get(artistID=id))
                event.save()

    all_user_shortlist_artist = event.shortlisted_artists.all()
    all_shortlist_artist = event.shortlisted_artists.all().count()

    booked_artist_id = event.booked_artists.all().values_list('artistID', flat=True)

    artists = Artist.objects.all().exclude(artistID__in=booked_artist_id)
    city = City.objects.filter(city__iexact=event.city)

    # Get Most Recent and Most Upcoming Event On Artist Before and After event date
    most_recent_past_event = {}
    most_upcoming_event = {}
    for artist in all_user_shortlist_artist:
        most_upcoming_event[artist] = get_artist_most_recent_event(
            artist, event.event_date)
        most_recent_past_event[artist] = get_artist_most_recent_event(
            artist, event.event_date, is_future_event=False)

    past_event = {}
    future_event = {}
    artist_events_near_to_user = {}

    if city.exists():
        city = city[0]
        latitude = city.lat
        longitude = city.lng
        from artist.views import get_artist_last_event_near_to_user
        for artist in all_user_shortlist_artist:
            past_event[artist] = get_artist_event_near_by_to_city(
                artist,
                latitude,
                longitude,
                is_future_event=False,
            )
            future_event[artist] = get_artist_event_near_by_to_city(
                artist,
                latitude,
                longitude,
                is_future_event=True,
            )
            artist_events_near_to_user[artist] = get_artist_last_event_near_to_user(
                artist,
                request,
            )[0]

    context.update({
        "artists": artists,
        "all_user_shortlist_artist": all_user_shortlist_artist,
        "past_event": past_event,
        "future_event": future_event,
        'most_recent_past_event': most_recent_past_event,
        'most_upcoming_event': most_upcoming_event,
        "all_shortlist_artist": all_shortlist_artist,
        "artist_events_near_to_user": artist_events_near_to_user
    })

    return render(request, "events/shortlisted_artist.html", context)


def get_artist_fb_likes_of_a_country(artist, country_name="None", months=6):
    return_dict = {}
    return_dict[str(artist.artistName)] = {}
    six_months_ago = datetime.date.today() - relativedelta(months=+months)

    all_facebook_object = FacebookLike.objects.filter(
        artist=artist,
        created_at__gte=six_months_ago,
        like_count__isnull=False,
        country_name=country_name,
    ).values_list('like_count', 'created_at').order_by('created_at')

    for facebook_object in all_facebook_object:
        timestamp = time.mktime(facebook_object[1].timetuple())
        return_dict[str(artist.artistName)].update(
            {
                int(timestamp) * 1000: facebook_object[0],
            }
        )

    temp_dict = return_dict[str(artist.artistName)]
    temp_dict = OrderedDict(sorted(temp_dict.items(), key=lambda t: t[0]))
    return_dict[str(artist.artistName)] = temp_dict
    return return_dict


def get_todays_like(artist, country=None):
    if country:
        fb_likes = FacebookLike.objects.filter(
            artist_id=artist.artistID, country_name=country).last()
        likes = fb_likes.like_count if fb_likes else 0
        return {
            str(artist.artistName): likes
        }

    fb_likes = FacebookLike.objects.filter(artist_id=artist.artistID).last()
    if fb_likes:
        likes = fb_likes.like_count
    else:
        likes = 0
    resp = {
        str(artist.artistName): likes
    }
    return resp


def get_artist_last_event_near_to_user(artist, request, city):
    withinmiles = 100
    try:
        city = City.objects.get(city=city)
        user_lat = city.lat
        user_long = city.lng
    except Exception as e:
        return [], []
    user_lat = float(user_lat)
    user_long = float(user_long)
    past_happend_events = []
    future_happening_events = []
    artist_id = "(" + str(artist.artistID) + ")"
    all_artist_events = DjMaster.objects.raw(
        "SELECT * FROM dj_Master JOIN dj_Events ON dj_Events.eventID = dj_Master.eventID JOIN dj_Venues ON dj_Venues.venueID = dj_Master.venueID WHERE dj_Master.artistID IN {} ORDER BY dj_Events.date DESC".format(artist_id))
    for event in all_artist_events:
        if event.venueLatitude != 0E-9 and event.venueLongitude != 0E-9 and event.venueLatitude != None and event.venueLongitude != None:
            venue_latitude = event.venueLatitude
            venue_longitude = event.venueLongitude

            distance = distance_using_lati_long(
                user_lat, user_long, float(venue_latitude), float(venue_longitude))

            if distance <= withinmiles:
                if datetime.date.today() < event.date and event.date:
                    future_happening_events.append(event)
                else:
                    past_happend_events.append(event)

    if past_happend_events:
        past_happend_events = [past_happend_events[0]]
    return past_happend_events, future_happening_events


def distance_using_lati_long(user_latitude, user_longitude, venue_latitude, venue_longitude):
    degrees_to_radians = math.pi/180.0

    phi1 = (90.0 - user_latitude)*degrees_to_radians
    phi2 = (90.0 - venue_latitude)*degrees_to_radians

    theta1 = user_longitude*degrees_to_radians
    theta2 = venue_longitude*degrees_to_radians

    cos = (math.sin(phi1)*math.sin(phi2)*math.cos(theta1 - theta2) +
           math.cos(phi1)*math.cos(phi2))
    arc = math.acos(cos)
    earth_radius = 3959
    return arc * 3959


def get_comparison_data(past_events, artist):
    data = {
        str(artist.artistName): past_events.filter(artistid=artist).count()
    }
    return data


def events(place, country=1, limit=None):
    if country == 1:
        venues = DjVenues.objects.filter(Q(country=place))
    elif country == 0:
        venues = DjVenues.objects.filter(Q(city=place))
    else:
        venues = DjVenues.objects.filter(country__in=place)
    venuesids = [item.venueid for item in venues]
    if limit:
        up_events = DjMaster.objects.filter(venueid__in=venuesids).filter(
            eventid__date__gt=datetime.date.today()).order_by('eventid__date')[:limit]
        year_ago = datetime.date.today() - datetime.timedelta(730)
        past_events = DjMaster.objects.filter(venueid__in=venuesids).filter(eventid__date__lt=datetime.date.today()).exclude(
            eventid__date__lt=year_ago, venueid__venuelatitude=None).exclude(venueid__venuelatitude="0.000000000").order_by('-eventid__date')[:limit]

    else:
        year_ago = datetime.date.today() - datetime.timedelta(730)
        past_events = DjMaster.objects.filter(venueid__in=venuesids).filter(
            eventid__date__lt=datetime.date.today()).exclude(eventid__date__lt=year_ago).order_by('-eventid__date')
        up_events = DjMaster.objects.filter(venueid__in=venuesids).filter(
            eventid__date__gt=datetime.date.today()).order_by('eventid__date')
    return up_events, past_events


@login_required
@check_email_validated
def event_stats(request, event_id):
    context = {}
    event_id = event_id

    user = request.user
    event = get_object_or_404(WpMyEvents, event_id=event_id, user_id=user.id)
    shortlisted_artists = event.shortlisted_artists.all()

    top_artists = TimeTrends.objects.filter(artist__in=shortlisted_artists).filter(top=6).order_by("-current_count")[:4]
    
    context.update({
        "top_artists": top_artists
    })

    context.update({
        "event": event,
        "shortlisted_artists": shortlisted_artists
    })

    compare_artist = {}
    data = event.data
    data_obj = json.loads(data)
    country = data_obj.get('country', None)
    city = event.city

    if country:
        up_events, past_events = events(country)
        for artist in shortlisted_artists:
            compare_artist.update(get_comparison_data(past_events, artist))
        context.update({"country": country})

    # Getting Instagram Followers Data
    instagram_followers_data = {}
    for artist in shortlisted_artists:
        instagram_followers_data.update(get_artist_latest_followers("instagram", artist))

    # Getting Twitter Followers Data
    twitter_followers_data = {}
    for artist in shortlisted_artists:
        twitter_followers_data.update(get_artist_latest_followers("twitter", artist))

    # Getting Youtube Views Data
    youtube_views_data = {}
    for artist in shortlisted_artists:
        youtube_views_data.update(get_artist_latest_followers("youtube_views", artist))

    # Getting Facebook Likes Data
    facebook_likes_data = {}
    for artist in shortlisted_artists:
        facebook_likes_data.update(get_artist_latest_followers("facebook_likes", artist))

    # Getting Spotify Followers Data
    likes_data = {}
    for artist in shortlisted_artists:
        likes_data.update(get_artist_spotify_likes(artist))

    # Getting Youtube Followers Data
    youtube_likes_data = {}
    for artist in shortlisted_artists:
        youtube_likes_data.update(get_artist_youtube_likes(artist))

    if shortlisted_artists:
        current_month_year = datetime.datetime.now().strftime('%B %Y')
        last_six_month_year = (datetime.datetime.now(
        ) - datetime.timedelta(6*365/12)).strftime('%B %Y')

        bar_graph_title = "%s- %s- Artist total Facebook Likes Between %s - %s  " % (
            event.event_name, country, last_six_month_year, current_month_year)

        color_code = {}

        youtube_color_code = {}

        instagram_color_code = {}

        # rand_color = randomcolor.RandomColor()
        for key in likes_data.keys():
            color_code[key] = ColorHash(key).hex

        for key in youtube_likes_data.keys():
            youtube_color_code[key] = ColorHash(key).hex

        for key in instagram_color_code.keys():
            instagram_color_code[key] = ColorHash(key).hex

        # Artist Likes On FaceBook
        country_fb_likes = {}
        for artist in shortlisted_artists:
            country_fb_likes.update(get_todays_like(artist, country))

        context.update({
            'short_listed': event,
            'likes_data': likes_data,
            'bar_graph_title': bar_graph_title,
            'artist_number_of_event': compare_artist,
            'country_fb_likes': country_fb_likes,
            'color_code': color_code,
            "youtube_likes_data": youtube_likes_data,
            "instagram_followers_data": instagram_followers_data,
            "twitter_followers_data": twitter_followers_data,
            "youtube_views_data": youtube_views_data,
            "facebook_likes_data": facebook_likes_data,
        })

        # Check for Spotify
        likes_data_check = False
        for like_data in likes_data.values():
            if like_data:
                likes_data_check = True
                context.update({
                    'likes_data_check': likes_data_check,
                })
                break
            else:
                context.update({
                    'likes_data_check': False,
                })

        # Check for Facebook Likes
        fb_like_check = False
        for fb_like in facebook_likes_data.values():
            if fb_like:
                fb_like_check = True
                context.update({
                    "fb_like_check": fb_like_check,
                })
                break
            else:
                context.update({
                    "fb_like_check": False,
                })

        # Check for Country Facebook
        country_fb_like_check = False
        for coun_fb_like in country_fb_likes.values():
            if country_fb_likes:
                country_fb_like_check = True
                context.update({
                    "country_fb_like_check": country_fb_like_check,
                })
                break
            else:
                context.update({
                    "country_fb_like_check": False,
                })

        # Check for Youtube
        youtube_likes_data_check = False
        for you_like_data in youtube_likes_data.values():
            if you_like_data:
                youtube_likes_data_check = True
                context.update({
                    "youtube_likes_data_check": youtube_likes_data_check,
                })

                break
            else:
                context.update({
                    "youtube_likes_data_check": False,
                })

        # Check for Instagram
        instagram_likes_data_check = False
        for insta_like_data in instagram_followers_data.values():
            if insta_like_data:
                instagram_likes_data_check = True
                context.update({
                    "instagram_likes_data_check": instagram_likes_data_check,
                })

                break
            else:
                context.update({
                    "instagram_likes_data_check": False,
                })

        # Check for Twitter
        twitter_likes_data_check = False
        for twitter_like_data in twitter_followers_data.values():
            if twitter_like_data:
                twitter_likes_data_check = True
                context.update({
                    "twitter_likes_data_check": twitter_likes_data_check,
                })

                break
            else:
                context.update({
                    "twitter_likes_data_check": False,
                })

        # Check for Youtube Views
        youtube_views_data_check = False
        for youtube_views_data in youtube_views_data.values():
            if youtube_views_data:
                youtube_views_data_check = True
                context.update({
                    "youtube_views_data_check": youtube_views_data_check,
                })

                break
            else:
                context.update({
                    "youtube_views_data_check": False,
                })

        no_of_eve_check = False
        for no_of_eve in compare_artist.values():
            if no_of_eve:
                no_of_eve_check = True
                context.update({
                    "no_of_eve_check": no_of_eve_check,
                })
                break
            else:
                context.update({
                    "no_of_eve_check": False,
                })

    return render(request, "events/event_stats.html", context)


@login_required
@check_email_validated
def offered_artists(request, event_id):

    context = {}
    event_id = event_id
    user = request.user
    event = get_object_or_404(WpMyEvents, event_id=event_id, user_id=user.id)
    booked_artists = event.invited_users.all()
    # vantur
    created = BookedArtistForUserEvent.objects.filter(event_id=event_id)
    booked_artists = zip(booked_artists, created)
    # end vantur

    if request.method == "POST":
        bookedartist = request.POST.get('bookedartist')
        if bookedartist:
            bookedartist = int(bookedartist)
            artist = get_object_or_404(Artist, artistID=bookedartist)
            BookedArtistForUserEvent.objects.create(
                artist=artist,
                event=event,
            )
            # event.invited_users.remove(artist)
            event.save()

    context.update({
        "event": event,
        "booked_artists": booked_artists,
    })
    return render(request, "events/offered_artists.html", context)


@login_required
@check_email_validated
def booked_artists(request, event_id):
    context = {}
    event_id = event_id
    user = request.user
    event = get_object_or_404(WpMyEvents, event_id=event_id, user_id=user.id)
    booked_artists = event.booked_artists.all()

    context.update({
        "event": event,
        "booked_artists": booked_artists
    })
    return render(request, "events/booked_artists.html", context)


@login_required
@check_email_validated
@check_plan('Promoter')
def event_booking_request(request, event_id, artist_id):

    action = request.POST.get("action", None)

    request_url = request.META.get('HTTP_REFERER')
    user = request.user
    user_profile = Profile.objects.get(user=user)

    event = WpMyEvents.objects.get(event_id=event_id)
    artist = Artist.objects.get(artistID=artist_id)

    user_ref = format(user.id, "05")
    event_ref = format(int(event_id), "05")
    artist_id = format(int(artist_id), "05")
    ref_no = "BR-{}-{}-{}".format(user_ref, event_ref, artist_id)

    # Commented the removel as per sam had said
    # event.shortlisted_artists.remove(artist)

    if artist not in event.invited_users.all():
        event.invited_users.add(artist)

    if action == "offer":
        is_offersheet = True
    else:
        is_offersheet = False

    if not is_offersheet:
        subject = "Booking Request"
    else:
        subject = "Offer Sheet"

    data = json.loads(event.data)
    msg = SendEmail(request)
    msg.send_by_template([user.email], "events/email/artist_booking_request.html",
                         context={
        "artist": artist,
        "event": event,
        "data": data,
        "user_profile": user_profile,
        "is_offersheet": is_offersheet,
        "ref_no": ref_no
    },
        subject=subject
    )

    messages.add_message(request, messages.SUCCESS, "Email has been sent")

    return redirect(request_url)


def available_or_booked_on_date(event):
    shortlisted_artist = list(
        event.shortlisted_artists.all().values_list('artistID', flat=True))

    shortlisted_artist = map(lambda x: str(x), shortlisted_artist)
    if shortlisted_artist and event.event_date:
        shortlisted_artist = ",".join(shortlisted_artist)

        shortlisted_artist = "("+shortlisted_artist+")"

        free_dates = event.event_date.strftime('%Y-%m-%d')

        conn = DBInterface()
        conn.connect()

        booked_on_event_dates_query = '''
            SELECT dj_Master.artistID
                FROM dj_Master
                JOIN dj_Events ON dj_Master.eventID = dj_Events.eventID
                WHERE dj_Master.artistID IN {} AND
            dj_Events.date = '{}';'''.format(shortlisted_artist, free_dates)

        booked_on_event_dates_artist = conn.execute_query(
            booked_on_event_dates_query)
        booked_on_event_dates_artist = [
            int(i[0]) for i in booked_on_event_dates_artist]
        return booked_on_event_dates_artist
    else:
        return None


@login_required
@check_email_validated
def event_news(request, event_id):
    context = {}
    event_id = event_id

    user = request.user
    event = get_object_or_404(WpMyEvents, event_id=event_id, user_id=user.id)
    event_city = event.city
    event_genre = event.genre.all()
    profile_obj = Profile.objects.get(user=user)
    email_frequency_value = profile_obj.email_frequency

    shortlisted_artists_of_events = event.invited_users.all(
    ) | event.shortlisted_artists.all()

    event_city_related_news = News.objects.filter(
        Q(title__icontains=event_city) | Q(text__icontains=event_city)).order_by('-created')[:3]

    shortlist_artists_news = News.objects.filter(
        artists__in=shortlisted_artists_of_events).order_by('-created')[:3]

    if event_genre:
        genre = Artist.objects.filter(genre=event_genre[0])
        event_genre_news = News.objects.filter(
            artists__in=genre).order_by('-created')[:3]
    else:
        event_genre_news = None

    all_news = {
        "event_city_related_news": event_city_related_news,
        "shortlist_artists_news": shortlist_artists_news,
        "event_genre_news": event_genre_news,
    }

    news_labels = {
        "event_city_related_news": "Event City Related News",
        "shortlist_artists_news": "Shortlisted Artists News",
        "event_genre_news": "Event Genre News",
    }

    context.update({
        "event": event,
        "all_news": all_news,
        "booked_artists": booked_artists,
        "news_labels": news_labels,
        "email_frequency_value": email_frequency_value
    })

    return render(request, "events/event_news.html", context)


@login_required
@check_email_validated
def recommended_artists(request, event_id):
    context = {}
    event_id = event_id

    user = request.user
    event = get_object_or_404(WpMyEvents, event_id=event_id, user_id=user.id)
    recommended_artists = get_event_recommended_artists(event)

    context = {
        "event_id": event_id,
        "event": event,
        "recommended_artists": recommended_artists
    }
    return render(request, 'events/recommended_artists.html', context)


@login_required
@check_email_validated
def shortlist_artist(request):
    response = {"status": False, "errors": []}

    if request.is_ajax():
        context = {}

        artist_id = request.POST['artist_id']
        event_id = request.POST['event_id']
        user = request.user

        event = get_object_or_404(
            WpMyEvents, event_id=event_id, user_id=user.id)
        # for artist in event.shortlisted_artists.all():
        #     event.shortlisted_artists.remove(artist)

        if artist_id:
            artist = Artist.objects.get(artistID=artist_id)
            event.shortlisted_artists.add(artist)
            response["artist_name"] = artist.artistName
            response["artist_photo_url"] = artist.artistPhoto.url

        response["status"] = True
        response["artist_id"] = artist_id
        response["event_id"] = event_id

        return HttpResponse(json.dumps(response))


def get_artist_social_url(artist):
    sources = Source.objects.all().exclude(
        sourceName__in=['Spotify', 'Bandsintown', 'Gigatools', 'Resident Adviser', 'Songkick'])
    social_url = {}
    for source in sources:
        artistwebsite = ArtistWebsite.objects.filter(
            sourceID=source, artistID=artist)
        if artistwebsite:
            social_url[source.sourceName.lower()] = artistwebsite[0].url
            if source.sourceName.lower() == "youtube":
                if len(artistwebsite[0].url.split("/")) > 1:
                    social_url[source.sourceName.lower(
                    ) + "_username"] = artistwebsite[0].url.split("/")[-1]

            if source.sourceName.lower() == "soundcloud":
                """
                This condition remove the www. from the url
                """
                social_url[source.sourceName.lower(
                )] = artistwebsite[0].url.replace("www.", "")

    return social_url


@login_required
# @check_plan('Trial','Basic','Premium')
@check_plan('Trial', 'Promoter', 'Artist/DJ', 'Agent')
@check_email_validated
def event_show(request, event_id):
    start_time = time.time()
    free_online_dates = []

    if request.user.is_authenticated():
        pro_obj = Profile.objects.get(user=request.user)
    else:
        pro_obj = None

    global limit
    context = {}
    event = Event.objects.get(eventID=event_id)
    weekday = event.date.strftime("%A")
    month = event.date.strftime("%b")
    all_in_master = DjMaster.objects.filter(eventid=event)
    # all_events = WpMyEvents.objects.filter(event_id=event_id)
    artist = all_in_master[0].artistid

    artists = [x.artistid for x in all_in_master]

    general_manager = artist.general_manager
    if general_manager:
        general_manager = general_manager.replace(" ", "")
        general_manager_list = general_manager.split(",")

        context.update({"general_manager_list": general_manager_list})

    message_str = request.GET.get('message_str', '')
    try:
        # artist = Artist.allartistobjects.get(artistID=artist_id)
        artist_genres = ArtistGenre.objects.filter(artistID=artist)
    except:
        raise Http404()

    social_urls = get_artist_social_url(artists[0])

    statistics = []
    similar_artists = []
    if len(similar_artists) == 0:
        similar_artists = []
    upcoming_events_data = []
    upcoming_events = [event]
    upcoming_events_length = 1
    past_events_data = []
    past_events = []
    old_events_length = []
    latest_news = []
    # artist_top_ten_country = get_user_top_country_an_likes(artist)
    code = ""
    try:
        code = str(pycountry.countries.get(
            name=all_in_master[0].venueid.country).alpha_2).lower()
    except:
        pass
    past_happend_events = []
    future_happening_events = []
    artist_requested_event_list = []
    limit = 0

    fav_venues = pro_obj.favorite_venues.all()
    fav_venues_list = []
    for venue in fav_venues:
        fav_venues_list.append(venue.venuename)

    venue = all_in_master[0].venueid
    context.update({
        'artist': artists[0],
        'artists': artists,
        'weekday': weekday,
        'month': month,

        'code': code,
        'event': event,
        'venue': venue,
        'pro_obj': pro_obj,
        'artist_genres': artist_genres,
        'statistics': statistics,
        'social_urls': social_urls,
        'upcoming_events': upcoming_events,
        'upcoming_events_length': upcoming_events_length,
        'old_events': past_events,
        'old_events_length': old_events_length,
        'start': limit,
        'similar_artists': similar_artists,
        'all_events': [],
        'past_happend_events': past_happend_events,
        'future_happening_events': future_happening_events,
        'artist_requested_event_list': artist_requested_event_list,
        'free_online_dates': free_online_dates,
        'latest_news': latest_news,
        'fav_venues_list': fav_venues_list,
        # 'artist_top_ten_country': artist_top_ten_country,
        "site_url": settings.SITE_URL,
        'show_loader': True,
    })
    # Notes Part - Start
    if request.user.is_authenticated():
        data = {'event': event, 'user': request.user}
        form_note = EventNotesForm(initial=data)
        notes = EventNote.objects.filter(
            event=event, user=request.user).order_by('-modified_date')
        context.update({
            'notes': notes,
            'form_note': form_note,
        })
    # Notes Part End

    # Comments part - Start

    comments = EventComment.objects.filter(
        event=event, reply=None).order_by('-id')
    comment_form = EventCommentsForm
    context.update({
        'comments': comments,
        'comment_form': comment_form})

    # Comments part -End

    # Connections part Start

    event_connections = ProfileConnections.objects.filter(event=event)
    event_in_connections = ProfileConnections.objects.filter(
        event=event, requested_by=request.user).first()

    if event_in_connections and (event_in_connections in event_connections):
        context.update({
            'register_venue': False,
        })
    else:
        context.update({
            'register_venue': True,
        })

    context.update({
        'event_connections': event_connections.filter(status=1),
    })
    # Connections part End

    end_time = time.time()

    print("time for execution ++++++++++++++++++++++++++++++++++++")

    print(end_time - start_time)

    return render(request, 'events/event_show.html', context)


@login_required
@check_email_validated
# @check_plan('Trial','Premium')
@check_plan('Trial', 'Artist/DJ', 'Promoter', 'Agent')
def favorite(request):
    order_by = request.GET.get("order_by", "country")
    events_limit = 10
    pro_obj = Profile.objects.get(user=request.user)
    if pro_obj.user_types == "Promoter" and not request.user.is_superuser:
        return redirect("/")

    all_events_object = list(pro_obj.favorite_events.all())
    artist_performing_on_event = []
    venues_of_event = []
    now_date = datetime.datetime.now().date()

    for event in all_events_object:
        artists_id = DjMaster.objects.filter(eventid=event).order_by(
            '-eventid__date').values_list('artistid', flat=True)[:5]
        ven = DjMaster.objects.filter(eventid=event).all()[0].venueid
        artists = []
        for id in artists_id:
            try:
                temp_artist = Artist.objects.get(artistID=int(id))
                if temp_artist not in artists:
                    artists.append(temp_artist)
            except Exception:
                pass

        artist_performing_on_event.append(artists)
        venues_of_event.append(ven)

    all_events = zip(all_events_object,
                     artist_performing_on_event, venues_of_event)

    if request.is_ajax() and request.method == "POST":
        start = request.POST.get('start')
        all_events = all_events[: int(start) + events_limit]
        user_email = request.user.email
        if user_email:
            msg = SendEmail(request)
            msg.send_by_template([user_email], "geography/email/fav_venues_email.html",
                                 context={
                'all_venues': all_events,
                "site_url": settings.SITE_URL,
            },
                subject="Favorite Venues Details - Wheredjsplay.com"
            )
            response = {
                "status": True,
            }
            return HttpResponse(json.dumps(response), content_type="applicaton/json")

    if request.is_ajax() and request.GET.get('start', ''):
        start = request.GET.get('start', '')
        action = request.GET.get('action', '')
        if action == 'upcoming':
            start = int(start)
            all_events = all_events[start:(start + events_limit)]

            context = {
                "allvenues": all_events,
                "pro_obj": pro_obj,
            }

            template = render_to_string(
                "geography/get_favorite_venues.html", context, context_instance=RequestContext(request))

            start = int(start) + events_limit
            if (len(all_events) < events_limit):
                status = "False"  # No More events
            else:
                status = "True"  # More events
            response = {
                "status": status,
                "start": start,
                "data": template,
            }
            return HttpResponse(json.dumps(response), content_type="applicaton/json")

    context = {
        'all_events': all_events[0:events_limit],
        "pro_obj": pro_obj,
        "start": events_limit
    }

    return render(request, 'events/favorite_events.html', context)


@login_required
@check_email_validated
# @check_plan('Trial','Premium')
@check_plan('Trial', 'Artist/DJ', 'Agent')
def favorite_event(request):
    response = {"status": False, "errors": []}
    if request.is_ajax() and request.POST['action'] == 'create_favorite_venue':
        event_id = request.POST['id']
        event = Event.objects.get(eventID=event_id)
        profile = Profile.objects.get(user=request.user)
        profile.favorite_events.add(event)
        profile.favorite_events.all()
        profile.save()

    if request.is_ajax() and request.POST['action'] == 'remove_favorite_venue':
        event_id = request.POST['id']
        event = Event.objects.get(eventID=event_id)
        profile = Profile.objects.get(user=request.user)
        profile.favorite_events.remove(event)
        profile.favorite_events.all()

        profile.save()

    response["status"] = True
    response["id"] = event_id
    response["venue"] = event.eventName
    return HttpResponse(json.dumps(response))


@login_required
@check_email_validated
# @check_plan('Trial','Basic','Premium')
@check_plan('Trial', 'Artist/DJ', 'Promoter', 'Agent')
def event_map(request):
    ev_id = request.GET.get('event')
    if ev_id:
        event = Event.objects.get(eventID=ev_id)
        djm = DjMaster.objects.filter(eventid=ev_id)[0]
        artist = djm.artistid
        # event = get_upcoming_events(artist, map_event=True)
        events = [event]
        # past_events = get_past_events(artist, map_event=True)
        # for event in past_events:
        #     events.append(event)
        data_list = []

        artist_info = {}
        artist_info['id'] = artist.artistID
        artist_info['artist'] = artist.artistName
        sourceImage = None
        try:
            sourceImage = Artist.objects.get(
                artistID=artist.artistID).artistPhoto.url
        except Exception as e:
            sourceImage = None
        if sourceImage == None:
            try:
                sourceImage = ScrapePhoto.objects.get(
                    artistID=artist.artistID).flipImage
                sourceImage = "/media%s" % (sourceImage)
                sourceImage = ""
            except Exception as e:
                sourceImage = ""
        artist_info['artist_image'] = sourceImage

        all_events = []
        for event in events:
            e = {}
            e['eventID'] = event.eventID
            e['eventName'] = event.eventName
            e['date'] = str(event.date)
            e['startTime'] = event.startTime
            e['endTime'] = event.endTime
            if event.date < datetime.datetime.now().date():
                e['eventType'] = "past"
            else:
                e['eventType'] = "future"
            v = {}
            v['venueID'] = str(djm.venueid)
            v['venueName'] = event.evenueName
            v['venueAddress'] = djm.venueid.venueaddress
            if djm.venueid.venuelatitude == 0E-9:
                v['venueLatitude'] = "0.000000000"
            else:
                v['venueLatitude'] = str(djm.venueid.venuelatitude)
            if djm.venueid.venuelongitude == 0E-9:
                v['venueLongitude'] = "0.000000000"
            else:
                v['venueLongitude'] = str(djm.venueid.venuelongitude)
            v['city'] = djm.venueid.city
            v['country'] = djm.venueid.country
            event_dict = {}
            event_dict['eventDetail'] = e
            event_dict['venueDetail'] = v
            all_events.append(event_dict)
        data = {}
        data['artistDetail'] = artist_info
        data['events'] = all_events
        data_list = [data]
    else:
        data_list = []
    return HttpResponse(json.dumps(data_list))


def get_search_events(request, events_list, event_type, query='', page=1, limit=10):
    events_list = events_list
    page = page

    paginator = Paginator(events_list, limit)

    try:
        all_events = paginator.page(page)
    except PageNotAnInteger:
        all_events = paginator.page(1)
    except EmptyPage:
        all_events = paginator.page(paginator.num_pages)

    context = {
        'all_events': all_events,
        'event_type': event_type,
        'query': query,
    }
    template = render_to_string(
        "events/load_more_events.html", context=context)
    return template


@login_required
@check_plan('Trial', 'Artist/DJ', 'Promoter', 'Agent')
def search_events(request):
    limit = 10
    start = 0
    query = ''
    all_events = []
    requested_events = []
    total = 0

    page = request.GET.get('page', 1)

    # Past Events and Future Events Variables
    today_date = datetime.datetime.now()
    requested_events_future = []
    requested_events_past = []

    if request.GET.get('query'):
        query = request.GET.get('query', '')
        query_list = query.split(',')

        query_str = Q()
        for query in query_list:
            query_str = query_str | Q(eventid__eventName__icontains=query) | Q(artistid__artistName__icontains=query) | Q(
                venueid__city__icontains=query) | Q(venueid__venuename__icontains=query)

        requested_events = DjMaster.objects.filter(
            query_str).values_list('eventid').distinct()

    if request.is_ajax():
        event_type = request.GET.get('event_type', 'former')
        events_template = ""
        if event_type == 'upcoming':
            requested_events_future = Event.objects.filter(
                eventID__in=requested_events, date__gte=today_date).order_by('date')
            events_template = get_search_events(
                request, requested_events_future, event_type, query, page, limit)
        else:
            requested_events_past = Event.objects.filter(
                eventID__in=requested_events, date__lt=today_date).order_by('-date')
            events_template = get_search_events(
                request, requested_events_past, event_type, query, page, limit)

        response = {
            'data': events_template,
            'event_type': event_type,

        }
        return HttpResponse(json.dumps(response), content_type="application/json")
    else:
        events_count_future = Event.objects.filter(
            date__gte=today_date).count()
        events_count_past = Event.objects.filter(date__lt=today_date).count()
        artists_count = Artist.objects.all().count()
        countries_count = Country.objects.all().count()
        context = dict()

        requested_events_future = Event.objects.filter(
            eventID__in=requested_events, date__gte=today_date).order_by('date')
        requested_events_past = Event.objects.filter(
            eventID__in=requested_events, date__lt=today_date).order_by('-date')

        total = requested_events_future.count() + requested_events_past.count()

        future_events_template = get_search_events(
            request, requested_events_future, 'upcoming', query, page, limit)
        future_response = {
            'data': future_events_template,
        }
        events_future = future_response

        past_events_template = get_search_events(
            request, requested_events_past, 'former', query, page, limit)
        past_response = {
            'data': past_events_template,
        }
        events_past = past_response

        updated_on = "NA"

        try:
            updated_on = Event.objects.all().order_by('-eventID')[0].created
        except:
            pass

        if updated_on is None:
            updated_on = "NA"

        context.update({
            "events_future": events_future,
            "events_past": events_past,
            "events_future_count": len(requested_events_future),
            "events_past_count": len(requested_events_past),
            "events_count_future": events_count_future,
            "events_count_past": events_count_past,
            "artists_count": artists_count,
            "countries_count": countries_count,
            "updated_on": updated_on,
            "query": query,
            "total": total,
        })

        context.update({
            'start': start + limit,
        })

        return render(request, 'events/event_search.html', context)


class EventNotes(View):

    def post(self, request, note_id):
        notes = None
        if int(note_id) == 0:
            form = EventNotesForm(request.POST or None)
            if form.is_valid():
                form.instance.modified_date = datetime.datetime.now()
                form.save()
            notes = EventNote.objects.filter(
                user=form.data['user'], event=form.data['event']).order_by('-modified_date')
        else:
            note_obj = EventNote.objects.filter(pk=int(note_id)).first()
            if note_obj:
                note_obj.note = request.POST.get('note_text', '')
                note_obj.modified_date = datetime.datetime.now()
                note_obj.save()
                notes = EventNote.objects.filter(
                    user=note_obj.user, event=note_obj.event).order_by('-modified_date')

        return render(request, "events/event_note.html", context={'notes': notes})


event_note = EventNotes.as_view()


class DeleteEventNote(View):

    def get(self, request, note_id):
        notes = None
        note_obj = EventNote.objects.filter(pk=int(note_id)).first()
        if note_obj:
            note_obj.delete()
            notes = EventNote.objects.filter(
                user=note_obj.user, event=note_obj.event).order_by('-modified_date')
        return render(request, "events/event_note.html", context={'notes': notes})


delete_event_note = DeleteEventNote.as_view()


class EventComments(View):

    def post(self, request, event_id):
        comments = None
        form = EventCommentsForm(request.POST or None)
        event = Event.objects.filter(eventID=event_id).first()
        comment_form = EventCommentsForm()
        if form.is_valid():
            form.instance.user = request.user
            form.instance.event = event
            reply_id = request.POST.get('comment_id')
            if reply_id:
                comment_qs = EventComment.objects.get(id=reply_id)
                form.instance.reply = comment_qs
            form.save()
            comments = EventComment.objects.filter(
                event=event, reply=None).order_by('-id')

        return render(request, "events/event_comments.html", context={'comments': comments, 'comment_form': comment_form, 'event': event})


event_comment = login_required(EventComments.as_view())


def get_upcoming_events(venue, start=0, map_event=None):
    global limit
    event_ids = DjMaster.objects.filter(
        venueid=venue.venueid).values_list('eventid', flat=True).distinct()
    if map_event:
        venue_upcoming_event_ids = Event.objects.filter(eventID__in=event_ids).filter(
            date__gte=datetime.datetime.now().date()).values_list('eventID', flat=True).order_by('date')
    else:
        venue_upcoming_event_ids = Event.objects.filter(eventID__in=event_ids).filter(
            date__gte=datetime.datetime.now().date()).values_list('eventID', flat=True).order_by('date')[start: start+limit]
    if venue_upcoming_event_ids:
        venue_upcoming_event_ids = [str(x) for x in venue_upcoming_event_ids]
        event_id_string = '('
        event_id_string += ','.join(venue_upcoming_event_ids) + ')'

        upcoming_events = DjMaster.objects.raw(
            "SELECT * FROM dj_Master JOIN dj_Events ON dj_Events.eventID = dj_Master.eventID JOIN dj_Venues ON dj_Venues.venueid = dj_Master.venueid WHERE dj_Master.venueid = {} AND dj_Events.eventID IN {} ORDER BY dj_Events.date".format(venue.venueid, event_id_string))
    else:
        upcoming_events = []

    return upcoming_events


def get_past_events(venue, start=0, map_event=None):
    global limit, past_event_ids
    event_ids = DjMaster.objects.filter(venueid=venue.venueid).order_by(
        'eventid').values_list('eventid', flat=True).distinct()
    if map_event:
        venue_past_event_ids = Event.objects.filter(eventID__in=event_ids).filter(
            date__lt=datetime.datetime.now().date()).values_list('eventID', flat=True).order_by('-date')[0:30]
    else:
        venue_past_event_ids = Event.objects.filter(eventID__in=event_ids).filter(date__lt=datetime.datetime.now(
        ).date()).values_list('eventID', flat=True).distinct().order_by('-date')[start: start+limit]
    past_event_ids = venue_past_event_ids
    if venue_past_event_ids:
        venue_past_event_ids = [str(x) for x in venue_past_event_ids]
        event_id_string = '('
        event_id_string += ','.join(venue_past_event_ids) + ')'
        past_events = Event.objects.filter(
            eventID__in=venue_past_event_ids).order_by('-date')
    else:
        past_events = []
    return past_events


def get_artists(venue_past_event_ids):
    artist_ids = DjMaster.objects.filter(
        eventid__in=[x for x in venue_past_event_ids]).values_list('artistid', flat=True)
    artists = Artist.objects.filter(artistID__in=[x for x in artist_ids])
    return artists


def get_unique_venues(venues):
    venue_names = []
    unique_venues = []
    for venue in venues:
        if venue.venuename.lower().strip() == "" or venue.venueaddress.lower().strip() == "":
            continue
        if venue.venuename.lower().strip() not in venue_names:
            venue_names.append(venue.venuename.lower().strip())
            unique_venues.append(venue)
    return unique_venues


@login_required
@check_plan('Artist/DJ', 'Promoter', 'Agent')
def venue_detail(request, venue_id):
    global past_event_ids
    context = {}
    Venues = DjVenues.objects.get(venueid=venue_id)
    other_venues = DjVenues.objects.filter(
        city=Venues.city).order_by('venuename')
    unique_venues = get_unique_venues(other_venues)

    address = ""
    venue_name = ""
    photo_reference_list = ""
    venue_images = ""
    venue_icon_image = ""
    total_events = DjMaster.objects.filter(venueid=Venues.venueid).values_list(
        'eventid', flat=True).distinct().count()

    try:
        venue_data = get_data(Venues.venueaddress, Venues.venuename)
        address = venue_data[0]
        venue_name = venue_data[1]
        if venue_data[2]:
            photo_reference_list = venue_data[2]
            venue_images = getImages(photo_reference_list)
            venue_icon_image = venue_images[0]
        else:
            venue_images = None
            venue_icon_image = None
    except Exception as e:
        pass
    upcoming_events = get_upcoming_events(Venues)
    past_events = get_past_events(Venues)
    artists = get_artists(past_event_ids)
    genre_ids = ArtistGenre.objects.filter(
        artistID__in=artists).values_list('genreID_id', flat=True).distinct()
    genres = Genre.objects.filter(genreID__in=genre_ids)
    profile_obj = Profile.objects.get(user=request.user)
    context.update({
        "Venues": Venues,
        "upcoming_events": upcoming_events[:limit],
        "start": limit,
        "previous_events": past_events[:limit],
        "person_contacts_flag": 1,
        "venue_detail_page": 1,
        "venue_contact_flag": 1,
        "venue_contact_flagsend": 1,
        "address": address,
        "venue_name": venue_name,
        "venue_image": venue_images,
        "venue_icon_image": venue_icon_image,
        "artists": artists,
        "total_events": total_events,
        "music_types": genres,
        "other_venues": unique_venues,
        "profile_obj": profile_obj,
    })

    # Notes Part - Start
    if request.user.is_authenticated():
        data = {'venue': Venues, 'user': request.user}
        form_note = VenueNotesForm(initial=data)
        notes = VenueNote.objects.filter(
            venue=Venues, user=request.user).order_by('-modified_date')
        context.update({
            'notes': notes,
            'form_note': form_note,
        })
    # Notes Part End

    # Comments part - Start

    comments = VenueComment.objects.filter(
        venue=Venues, reply=None).order_by('-id')
    comment_form = VenueCommentsForm
    context.update({
        'comments': comments,
        'comment_form': comment_form})

    # Comments part -End

    # Connections part Start

    venue_connections = ProfileConnections.objects.filter(venue=Venues)
    venue_in_connections = ProfileConnections.objects.filter(
        venue=Venues, requested_by=request.user).first()

    if venue_in_connections and (venue_in_connections in venue_connections):
        context.update({
            'register_venue': False,
        })
    else:
        context.update({
            'register_venue': True,
        })

    context.update({
        'venue_connections': venue_connections.filter(status=1),
    })
    # Connections part End

    return render(request, "events/venue_detail.html", context)


def paginate_venues(request, Venues):
    page_length = 10
    page = request.GET.get('page', 1)
    paginator = Paginator(Venues, page_length)
    try:
        Venues = paginator.page(page)
    except PageNotAnInteger:
        Venues = paginator.page(1)
    except EmptyPage:
        Venues = paginator.page(paginator.num_pages)
    index = Venues.number - 1
    max_index = len(paginator.page_range)
    start_index = index - 3 if index >= 3 else 0
    end_index = index + 3 if index <= max_index - 3 else max_index
    page_range = list(paginator.page_range)[start_index:end_index]
    return Venues, page_range


@login_required
@check_plan('Artist/DJ', 'Promoter', 'Agent')
def venues(request):

    context = {}
    search_results = None
    venues = DjVenues.objects.all()
    city = request.GET.get('city', '')
    selected_country = ""
    flag_venues = False
    all_country = Country.objects.all().order_by('name')
    if city:
        venues = venues.filter(city__startswith=city).order_by('venuename')
        context['city'] = city
        flag_venues = True
    venue_name = request.GET.get('venue_name', '')
    if venue_name:
        flag_venues = True
        venues = venues.filter(
            venuename__startswith=venue_name).order_by('venuename')

    country = request.GET.get('country', '')
    if country:
        flag_venues = True
        if not request.is_ajax():
            selected_country = country
        venues = venues.filter(
            country__startswith=country).order_by('venuename')

    filtered_venues = len(venues)
    pagination = paginate_venues(request, venues)
    venues = pagination[0]
    page_range = pagination[1]
    profile_obj = Profile.objects.get(user=request.user)
    all_artists = []

    if request.user.is_superuser:
        all_artists = Artist.objects.all()
    elif request.user.is_authenticated():
        all_artists = profile_obj.profile_managed_djs.all()

    context.update({
        "Venues": venues,
        "all_country": all_country,
        "selected_country": selected_country,
        "filtered_venues": filtered_venues,
        "page_range": page_range,
        "flag_venues": flag_venues,
        "profile_obj": profile_obj,
        "all_artists": all_artists
    })
    if request.is_ajax():
        context.update({
            "venue_name": venue_name,
            "country": country,
            "city": city

        }
        )
        template = render_to_string(
            "events/get_venue_list_table.html", context, context_instance=RequestContext(request))
        response = {
            "data": template,
            "filtered_venues": filtered_venues
        }
        return HttpResponse(json.dumps(response), content_type="applicaton/json")
    else:
        return render(request, "events/venue_list.html", context)


@login_required
@check_plan('Artist/DJ', 'Promoter', 'Agent')
def load_events_venues(request, venue_id):
    venue = DjVenues.objects.get(venueid=venue_id)
    start = request.GET.get('start')
    action = request.GET.get('action', '')
    events = None
    if action == "upcoming" or action == "past":
        if action == "upcoming":
            events = get_upcoming_events(venue, start=int(start))
        else:
            events = get_past_events(venue, start=int(start))

        context = {"events": events, "venue": venue}
        template = render_to_string(
            "events/get_events.html", context, context_instance=RequestContext(request))
        start = int(start) + limit
        if events:
            status = "True"  # More events
        else:
            status = "False"  # No More events
        response = {
            "status": status,
            "start": start,
            "data": template,

        }

        return HttpResponse(json.dumps(response), content_type="applicaton/json")

# Venue Comments and Notes


class VenueNotes(View):

    def post(self, request, note_id):
        notes = None
        if int(note_id) == 0:
            form = VenueNotesForm(request.POST or None)
            if form.is_valid():
                form.instance.modified_date = datetime.datetime.now()
                form.save()
            notes = VenueNote.objects.filter(
                user=form.data['user'], venue=form.data['venue']).order_by('-modified_date')
        else:
            note_obj = VenueNote.objects.filter(pk=int(note_id)).first()
            if note_obj:
                note_obj.note = request.POST.get('note_text', '')
                note_obj.modified_date = datetime.datetime.now()
                note_obj.save()
                notes = VenueNote.objects.filter(
                    user=note_obj.user, venue=note_obj.venue).order_by('-modified_date')

        return render(request, "events/venue_note.html", context={'notes': notes})


venue_note = VenueNotes.as_view()


class DeleteVenueNote(View):

    def get(self, request, note_id):
        notes = None
        note_obj = VenueNote.objects.filter(pk=int(note_id)).first()
        if note_obj:
            note_obj.delete()
            notes = VenueNote.objects.filter(
                user=note_obj.user, venue=note_obj.venue).order_by('-modified_date')
        return render(request, "events/venue_note.html", context={'notes': notes})


delete_venue_note = DeleteVenueNote.as_view()


class VenueComments(View):

    def post(self, request, venue_id):
        form = VenueCommentsForm(request.POST or None)
        venue = DjVenues.objects.filter(venueid=venue_id).first()
        comment_form = VenueCommentsForm()
        comments = None
        if form.is_valid():
            form.instance.user = request.user
            form.instance.venue = venue
            reply_id = request.POST.get('comment_id')
            if reply_id:
                comment_qs = VenueComment.objects.get(id=reply_id)
                form.instance.reply = comment_qs
            form.save()
            comments = VenueComment.objects.filter(
                venue=venue, reply=None).order_by('-id')

        return render(request, "events/venue_comments.html", context={'comments': comments, 'comment_form': comment_form, 'venue': venue})


venue_comment = login_required(VenueComments.as_view())


def remove_venue(request, venue, artist):
    artist = Artist.objects.get(artistID=artist)
    shotlisted_venue = ShortlistedVenue.objects.filter(artist=artist).first()
    if shotlisted_venue:
        shotlisted_venue.venueids.remove(venue)
        shotlisted_venue.venueids.all()
        shotlisted_venue.save()
    else:
        shotlisted_venue = ShortlistedVenue.objects.create(
            artist=artist,
            user=request.user
        )
        shotlisted_venue.save()


def add_venue(request, venue, artist):
    artist = Artist.objects.get(artistID=artist)
    shotlisted_venue = ShortlistedVenue.objects.filter(
        artist=artist, user=request.user).first()
    if shotlisted_venue:
        shotlisted_venue.venueids.add(venue)
        shotlisted_venue.venueids.all()
        shotlisted_venue.save()
    else:
        shotlisted_venue = ShortlistedVenue.objects.create(
            artist=artist,
            user=request.user
        )
        shotlisted_venue.venueids.add(venue)
        shotlisted_venue.venueids.all()
        shotlisted_venue.save()


def shortlist_venue(request):
    if request.is_ajax():
        venue_id = request.POST.get('id')
        artist_ids = request.POST.getlist('artist_ids[]')
        venue = DjVenues.objects.get(venueid=venue_id)
        [add_venue(request, venue, artist) for artist in artist_ids]
        artists_names = ",".join(
            [artist.artistName for artist in Artist.objects.filter(artistID__in=artist_ids)])
    return HttpResponse(json.dumps({
        "data": "true",
        "artists_names": artists_names,
        "venuename": venue.venuename
    }))


def add_event(request, event, artist):
    artist = Artist.objects.get(artistID=artist)
    shotlisted_event = ShortlistedEvent.objects.filter(
        artist=artist, user=request.user).first()
    if shotlisted_event:
        shotlisted_event.eventids.add(event)
        shotlisted_event.save()
    else:
        shotlisted_event = ShortlistedEvent.objects.create(
            artist=artist,
            user=request.user
        )
        shotlisted_event.eventids.add(event)
        shotlisted_event.save()


def shortlist_event(request):
    if request.is_ajax():
        event_id = request.POST.get('id')
        artist_ids = request.POST.getlist('artist_ids[]')
        event = Event.objects.get(eventID=event_id)
        [add_event(request, event, artist) for artist in artist_ids]
        artists_names = ",".join(
            [artist.artistName for artist in Artist.objects.filter(artistID__in=artist_ids)])
    return HttpResponse(json.dumps({
        "data": "true",
        "artists_names": artists_names,
        "eventname": event.eventName,
    }))

@login_required
@check_email_validated
def events_notes(request):
    events_notes_data = EventNote.objects.filter(user=request.user).order_by('-modified_date')
    events = events_notes_data.order_by('event__eventName').values_list('event','event__eventName').distinct()

    event = request.GET.get('event', '')
    date = request.GET.get('date', '')

    context = {}

    if event != '':
        events_notes_data = events_notes_data.filter(event__eventID=event)
        context.update({
            "filter_event": int(event)
        })

    context.update({
        "option_events": events,
        "events": events_notes_data,
        "date": date,
    })


    return render(request, "events/events_notes.html", context)

def duplicate_event(request, event_id):
    # Getting event object
    user = request.user
    event = get_object_or_404(WpMyEvents, event_id=event_id, user_id=user.id)

    # Getting new date for event
    event_date = request.POST.get("event_date", "")
    date = datetime.datetime.strptime(event_date, '%m/%d/%Y')
    formated_event_date = datetime.date.strftime(date, "%Y-%m-%d")

    # Getting data of m2m field from current event object
    shortlisted_artists = event.shortlisted_artists.all()
    genre = event.genre.all()
    invited_users = event.invited_users.all()
    # booked_artists = event.booked_artists.all()

    # Duplicating an object
    event_obj = event
    event_obj.event_id = None
    event_obj.event_date = date
    event_obj.save()

    # Adding m2m data
    event_obj.shortlisted_artists.add(*shortlisted_artists)
    event_obj.genre.add(*genre)
    event_obj.invited_users.add(*invited_users)
    # event_obj.booked_artists.add(*booked_artists)

    # Redirecting to the update event page
    return redirect('update_booking_request', event_id=event_obj.event_id)

def get_event_template(request, template_id):

    # Getting event type template object
    event_type = EventTypeTemplate.objects.filter(id=template_id).first()

    return HttpResponse(event_type.body)

@login_required
def create_event_template(request):
    
    # Only letting superadmin enter create event type template
    if not request.user.is_superuser:
        return redirect('/')

    form = EventTypeTemplateForm()

    context = {
        "form": form
    }

    if request.method == "POST":
        template_name = request.POST.get("template_name")
        template_body = request.POST.get("event_body")

        event_type_template = EventTypeTemplate(name=template_name, body=template_body, role="admin", user=request.user)
        event_type_template.save()

    return render(request, 'events/create_event_template.html', context)
