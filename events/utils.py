import requests
import time
import datetime as dt
from datetime import datetime
from datetime import timedelta
from dateutil.relativedelta import relativedelta
from collections import OrderedDict
from colorhash import ColorHash

from artist.models import DjSimilarArtists, Artist
from scrapper.database.database_interface import DBInterface
from social_data.models import FacebookData, YoutubeData, TwitterData, SpotifyData, InstagramData


def get_color_hashes(key_dict):
    color_code = {}
    for key in key_dict.keys():
        color_code[key] = ColorHash(key).hex

    return color_code


def get_artist_fb_likes_difference(artist):
    """
        Function Helps to Get the Fb Likes of Artist of Last Six Month
    """
    return_dict = {}
    return_dict[str(artist.artistName)] = {}
    six_months_ago = dt.date.today() - relativedelta(months=+6)

    all_facebook_object = FacebookData.objects.filter(
        artist=artist,
        created__gte=six_months_ago,
        fanCount__isnull=False,
    ).values_list('fanCount', 'created').order_by('created')

    temp = all_facebook_object[0][1]
    temp_count = all_facebook_object[0][0]

    ls = []
    return_dict = {}
    return_dict[str(artist.artistName)] = {}
    for index, obj in enumerate(all_facebook_object):
        timestamp = time.mktime(obj[1].timetuple())
        if index > 0:
            if (obj[1]- temp).days >= 7:
                # print obj[1], "sfddf", temp
                temp = obj[1]

                diff = int(temp_count) - int(obj[0])

                if diff > 0:
                    diff = -(diff)
                if diff < 0:
                    diff  = -(diff)

                # diff = int(all_facebook_object[index][0]) - int(temp_count)

                temp_count = int(all_facebook_object[index][0])
                return_dict[str(artist.artistName)].update(
                    {
                        # int(timestamp) * 1000: int(temp_count) + diff,
                        int(timestamp) * 1000: diff,
                    }
                )

    temp_dict = return_dict[str(artist.artistName)]
    temp_dict = OrderedDict(sorted(temp_dict.items(), key=lambda t: t[0]))
    return_dict[str(artist.artistName)] = temp_dict

    return return_dict


def get_artist_fb_likes(artist):
    """
        Function Helps to Get the Fb Likes of Artist of Last Six Month
    """
    return_dict = {}
    return_dict[str(artist.artistName)] = {}
    six_months_ago = dt.date.today() - relativedelta(months=+6)

    all_facebook_object = FacebookData.objects.filter(
        artist=artist,
        created__gte=six_months_ago,
        fanCount__isnull=False,
    ).values_list('fanCount', 'created').order_by('created')

    for facebook_object in all_facebook_object:
        timestamp = time.mktime(facebook_object[1].timetuple())
        return_dict[str(artist.artistName)].update(
            {
                int(timestamp) * 1000: facebook_object[0],
            }
        )

    temp_dict = return_dict[str(artist.artistName)]
    temp_dict = OrderedDict(sorted(temp_dict.items(), key=lambda t: t[0]))
    return_dict[str(artist.artistName)] = temp_dict
    return return_dict



def get_artist_youtube_likes(artist):
    """
        Function Helps to Get the Fb Likes of Artist of Last Six Month
    """
    return_dict = {}
    return_dict[str(artist.artistName)] = {}
    six_months_ago = dt.date.today() - relativedelta(months=+6)

    # all_youtube_object = YoutubeData.objects.filter(
    #     artist=artist,
    #     created__gte=six_months_ago,
    #     subscriberCount__isnull=False,
    # ).values_list('subscriberCount', 'created').order_by('created')


    # for youtube_object in all_youtube_object:
    #     timestamp = time.mktime(youtube_object[1].timetuple())
    #     return_dict[str(artist.artistName)].update(
    #         {
    #             int(timestamp) * 1000: youtube_object[0],
    #         }
    #     )

    # temp_dict = return_dict[str(artist.artistName)]
    # temp_dict = OrderedDict(sorted(temp_dict.items(), key=lambda t: t[0]))
    # return_dict[str(artist.artistName)] = temp_dict
    all_youtube_object = None
    if YoutubeData.objects.filter(
        artist=artist,
        created__gte=six_months_ago,
        subscriberCount__isnull=False,
    ).exists():
        all_youtube_object = YoutubeData.objects.filter(
        artist=artist,
        created__gte=six_months_ago,
        subscriberCount__isnull=False,
        ).order_by("-created")[0]
        return_dict[str(artist.artistName)] = all_youtube_object.subscriberCount
    else:
        return_dict[str(artist.artistName)] = 0
    return return_dict




def get_artist_youtube_likes_difference(artist):
    """
        Function Helps to Get the Fb Likes of Artist of Last Six Month
    """
    return_dict = {}
    return_dict[str(artist.artistName)] = {}
    six_months_ago = dt.date.today() - relativedelta(months=+6)

    all_youtube_object = YoutubeData.objects.filter(
        artist=artist,
        created__gte=six_months_ago,
        subscriberCount__isnull=False,
    ).values_list('subscriberCount', 'created').order_by('created')

    temp = all_youtube_object[0][1]
    temp_count = all_youtube_object[0][0]

    ls = []
    return_dict = {}
    return_dict[str(artist.artistName)] = {}
    for index, obj in enumerate(all_youtube_object):
        timestamp = time.mktime(obj[1].timetuple())
        if index > 0:
            if (obj[1]- temp).days >= 7:
                # print obj[1], "sfddf", temp
                temp = obj[1]

                diff = int(temp_count) - int(obj[0])

                if diff > 0:
                    diff = -(diff)
                if diff < 0:
                    diff  = -(diff)

                # diff = int(all_facebook_object[index][0]) - int(temp_count)

                temp_count = int(all_youtube_object[index][0])
                return_dict[str(artist.artistName)].update(
                    {
                        # int(timestamp) * 1000: int(temp_count) + diff,
                        int(timestamp) * 1000: diff,
                    }
                )

    temp_dict = return_dict[str(artist.artistName)]
    temp_dict = OrderedDict(sorted(temp_dict.items(), key=lambda t: t[0]))
    return_dict[str(artist.artistName)] = temp_dict

    return return_dict


def get_artist_spotify_likes(artist):
    """
        Function Helps to Get the Youtube Likes of Artist of Last Six Month
    """
    return_dict = {}
    return_dict[str(artist.artistName)] = {}
    six_months_ago = dt.date.today() - relativedelta(months=+6)

    # all_spotify_object = SpotifyData.objects.filter(
    #     artist=artist,
    #     created__gte=six_months_ago,
    #     followersCount__isnull=False,
    # ).values_list('followersCount', 'created').order_by('created')

    # for spotify_object in all_spotify_object:
    #     timestamp = time.mktime(spotify_object[1].timetuple())
    #     return_dict[str(artist.artistName)].update(
    #         {
    #             int(timestamp) * 1000: spotify_object[0],
    #         }
    #     )
    all_spotify_object = None
    if  SpotifyData.objects.filter(
        artist=artist,
        created__gte=six_months_ago,
        followersCount__isnull=False,
    ).exists():
        all_spotify_object = SpotifyData.objects.filter(
            artist=artist,
            created__gte=six_months_ago,
            followersCount__isnull=False,
        ).order_by('-created')[0]

        return_dict[str(artist.artistName)] = all_spotify_object.followersCount

        # timestamp = time.mktime(all_spotify_object.created.timetuple())
        # return_dict[str(artist.artistName)].update(
        #     {
        #         int(timestamp) * 1000: all_spotify_object.followersCount,
        #     }
        # )

    # temp_dict = return_dict[str(artist.artistName)]
    # temp_dict = OrderedDict(sorted(temp_dict.items(), key=lambda t: t[0]))
    # return_dict[str(artist.artistName)] = temp_dict

    return return_dict


def get_artist_youtube_likes_difference(artist):
    """
        Function Helps to Get the Fb Likes of Artist of Last Six Month
    """
    return_dict = {}
    return_dict[str(artist.artistName)] = {}
    six_months_ago = dt.date.today() - relativedelta(months=+6)

    all_youtube_object = YoutubeData.objects.filter(
        artist=artist,
        created__gte=six_months_ago,
        subscriberCount__isnull=False,
    ).values_list('subscriberCount', 'created').order_by('created')

    temp = all_youtube_object[0][1]
    temp_count = all_youtube_object[0][0]

    ls = []
    return_dict = {}
    return_dict[str(artist.artistName)] = {}
    for index, obj in enumerate(all_youtube_object):
        timestamp = time.mktime(obj[1].timetuple())
        if index > 0:
            if (obj[1]- temp).days >= 7:
                # print obj[1], "sfddf", temp
                temp = obj[1]

                diff = int(temp_count) - int(obj[0])

                if diff > 0:
                    diff = -(diff)
                if diff < 0:
                    diff  = -(diff)

                # diff = int(all_facebook_object[index][0]) - int(temp_count)

                temp_count = int(all_youtube_object[index][0])
                return_dict[str(artist.artistName)].update(
                    {
                        # int(timestamp) * 1000: int(temp_count) + diff,
                        int(timestamp) * 1000: diff,
                    }
                )

    temp_dict = return_dict[str(artist.artistName)]
    temp_dict = OrderedDict(sorted(temp_dict.items(), key=lambda t: t[0]))
    return_dict[str(artist.artistName)] = temp_dict

    return return_dict


def get_artist_twitter_followers_difference(artist):
    """
        Function Helps to Get the Fb Likes of Artist of Last Six Month
    """
    return_dict = {}
    return_dict[str(artist.artistName)] = {}
    six_months_ago = dt.date.today() - relativedelta(months=+6)

    all_twitter_object = TwitterData.objects.filter(
        artist=artist,
        created__gte=six_months_ago,
        followersCount__isnull=False,
    ).values_list('followersCount', 'created').order_by('created')

    temp = all_twitter_object[0][1]
    temp_count = all_twitter_object[0][0]

    ls = []
    return_dict = {}
    return_dict[str(artist.artistName)] = {}
    for index, obj in enumerate(all_twitter_object):
        timestamp = time.mktime(obj[1].timetuple())
        if index > 0:
            if (obj[1]- temp).days >= 7:
                # print obj[1], "sfddf", temp
                temp = obj[1]

                diff = int(temp_count) - int(obj[0])

                if diff > 0:
                    diff = -(diff)
                if diff < 0:
                    diff  = -(diff)

                # diff = int(all_facebook_object[index][0]) - int(temp_count)

                temp_count = int(all_twitter_object[index][0])
                return_dict[str(artist.artistName)].update(
                    {
                        # int(timestamp) * 1000: int(temp_count) + diff,
                        int(timestamp) * 1000: diff,
                    }
                )

    temp_dict = return_dict[str(artist.artistName)]
    temp_dict = OrderedDict(sorted(temp_dict.items(), key=lambda t: t[0]))
    return_dict[str(artist.artistName)] = temp_dict

    return return_dict


def get_artist_twitter_followers(artist):
    """
        Function Helps to Get the Twitter Likes of Artist of Last Six Month
    """
    return_dict = {}
    return_dict[str(artist.artistName)] = {}
    six_months_ago = dt.date.today() - relativedelta(months=+6)

    all_twitter_object = TwitterData.objects.filter(
        artist=artist,
        created__gte=six_months_ago,
        followersCount__isnull=False,
    ).values_list('followersCount', 'created').order_by('created')

    for twitter_object in all_twitter_object:
        timestamp = time.mktime(twitter_object[1].timetuple())
        return_dict[str(artist.artistName)].update(
            {
                int(timestamp) * 1000: twitter_object[0],
            }
        )

    temp_dict = return_dict[str(artist.artistName)]
    temp_dict = OrderedDict(sorted(temp_dict.items(), key=lambda t: t[0]))
    return_dict[str(artist.artistName)] = temp_dict
    return return_dict


def get_artist_spotify_followers_difference(artist):
    """
        Function Helps to Get the Fb Likes of Artist of Last Six Month
    """
    return_dict = {}
    return_dict[str(artist.artistName)] = {}
    six_months_ago = dt.date.today() - relativedelta(months=+6)

    all_spotify_object = SpotifyData.objects.filter(
        artist=artist,
        created__gte=six_months_ago,
        followersCount__isnull=False,
    ).values_list('followersCount', 'created').order_by('created')

    temp = all_spotify_object[0][1]
    temp_count = all_spotify_object[0][0]

    ls = []
    return_dict = {}
    return_dict[str(artist.artistName)] = {}
    for index, obj in enumerate(all_spotify_object):
        timestamp = time.mktime(obj[1].timetuple())
        if index > 0:
            if (obj[1]- temp).days >= 7:
                # print obj[1], "sfddf", temp
                temp = obj[1]

                diff = int(temp_count) - int(obj[0])

                if diff > 0:
                    diff = -(diff)
                if diff < 0:
                    diff  = -(diff)

                # diff = int(all_facebook_object[index][0]) - int(temp_count)

                temp_count = int(all_spotify_object[index][0])
                return_dict[str(artist.artistName)].update(
                    {
                        # int(timestamp) * 1000: int(temp_count) + diff,
                        int(timestamp) * 1000: diff,
                    }
                )

    temp_dict = return_dict[str(artist.artistName)]
    temp_dict = OrderedDict(sorted(temp_dict.items(), key=lambda t: t[0]))
    return_dict[str(artist.artistName)] = temp_dict

    return return_dict


def get_artist_spotify_followers(artist):
    """
        Function Helps to Get the spotify Likes of Artist of Last Six Month
    """
    return_dict = {}
    return_dict[str(artist.artistName)] = {}
    six_months_ago = dt.date.today() - relativedelta(months=+6)

    all_spotify_object = SpotifyData.objects.filter(
        artist=artist,
        created__gte=six_months_ago,
        followersCount__isnull=False,
    ).values_list('followersCount', 'created').order_by('created')

    for spotify_object in all_spotify_object:
        timestamp = time.mktime(spotify_object[1].timetuple())
        return_dict[str(artist.artistName)].update(
            {
                int(timestamp) * 1000: spotify_object[0],
            }
        )

    temp_dict = return_dict[str(artist.artistName)]
    temp_dict = OrderedDict(sorted(temp_dict.items(), key=lambda t: t[0]))
    return_dict[str(artist.artistName)] = temp_dict
    return return_dict


def get_artist_instagram_followers_difference(artist):
    """
        Function Helps to Get the Fb Likes of Artist of Last Six Month
    """
    return_dict = {}
    return_dict[str(artist.artistName)] = {}
    six_months_ago = dt.date.today() - relativedelta(months=+6)

    all_instagram_object = InstagramData.objects.filter(
        artist=artist,
        created__gte=six_months_ago,
        followed_by__isnull=False,
    ).values_list('followed_by', 'created').order_by('created')

    temp = all_instagram_object[0][1]
    temp_count = all_instagram_object[0][0]

    if temp and temp_count :
        ls = []
        return_dict = {}
        return_dict[str(artist.artistName)] = {}
        for index, obj in enumerate(all_instagram_object):
            timestamp = time.mktime(obj[1].timetuple())
            if index > 0:
                if (obj[1]- temp).days >= 7:
                    # print obj[1], "sfddf", temp
                    temp = obj[1]

                    diff = int(temp_count) - int(obj[0])

                    if diff > 0:
                        diff = -(diff)
                    if diff < 0:
                        diff  = -(diff)

                    # diff = int(all_facebook_object[index][0]) - int(temp_count)

                    temp_count = int(all_instagram_object[index][0])
                    return_dict[str(artist.artistName)].update(
                        {
                            # int(timestamp) * 1000: int(temp_count) + diff,
                            int(timestamp) * 1000: diff,
                        }
                    )

        temp_dict = return_dict[str(artist.artistName)]
        temp_dict = OrderedDict(sorted(temp_dict.items(), key=lambda t: t[0]))
        return_dict[str(artist.artistName)] = temp_dict

        return return_dict

    else:
        return None



def get_artist_instagram_followers(artist):
    """
        Function Helps to Get the instagram Likes of Artist of Last Six Month
    """
    return_dict = {}
    return_dict[str(artist.artistName)] = {}
    six_months_ago = dt.date.today() - relativedelta(months=+6)

    all_instagram_object = InstagramData.objects.filter(
        artist=artist,
        created__gte=six_months_ago,
        followed_by__isnull=False,
    ).values_list('followed_by', 'created').order_by('created')

    for instagram_object in all_instagram_object:
        timestamp = time.mktime(instagram_object[1].timetuple())
        return_dict[str(artist.artistName)].update(
            {
                int(timestamp) * 1000: instagram_object[0],
            }
        )

    temp_dict = return_dict[str(artist.artistName)]
    temp_dict = OrderedDict(sorted(temp_dict.items(), key=lambda t: t[0]))
    return_dict[str(artist.artistName)] = temp_dict
    return return_dict

def get_artist_latest_followers(platform, artist):
    """
        Function Helps to Get the instagram Likes of Artist of Last Six Month
    """
    return_dict = {}

    if platform == "instagram":
        latest_object = InstagramData.objects.order_by('created').filter(
            artist=artist,
            followed_by__isnull=False,
        ).first()

        if latest_object:
            followers_count = latest_object.followed_by
    elif platform == "twitter":
        latest_object = TwitterData.objects.order_by('created').filter(
            artist=artist,
            followersCount__isnull=False,
        ).first()

        if latest_object:
            followers_count = latest_object.followersCount
    elif platform == "youtube_views":
        latest_object = YoutubeData.objects.filter(
            artist=artist,
            viewCount__isnull=False,
        ).order_by("-created").first()

        if latest_object:
            followers_count = latest_object.viewCount
    elif platform == "facebook_likes":
        latest_object = FacebookData.objects.filter(
            artist=artist,
            fanCount__isnull=False,
        ).order_by('-created').first()

        if latest_object:
            followers_count = latest_object.fanCount

    if latest_object:
        return_dict[str(artist.artistName)] = followers_count
    
    return return_dict


def get_multiple_similiar_artist(artists):
    similar_artist_id = DjSimilarArtists.objects.filter(artistid__in=artists).exclude(linkedartistid=0).\
        values_list('linkedartistid', flat=True)
    similar_artist_id = set(list(similar_artist_id))

    similar_artist_id = similar_artist_id - set(artists)
    return list(similar_artist_id)


def save_event_venue_lat_long(address, event):
    try:
        google_location_api_url = "http://maps.google.com/maps/api/geocode/json?address="
        response = requests.get(google_location_api_url+address.replace(" ", "+"))
        json_response = response.json()
        if json_response['status'] == "OK":
            event_lat = json_response['results'][0]['geometry']['location']['lat']
            event_long = json_response['results'][0]['geometry']['location']['lng']
            event.latitude = event_lat
            event.longitude = event_long
            event.save()

    except Exception:
        pass


def get_event_recommended_artists(event):
    all_event_artists = event.shortlisted_artists.all() | event.invited_users.all() \
        | event.booked_artists.all()

    all_simliar_artists = get_multiple_similiar_artist(all_event_artists.values_list(
        'artistID', flat=True))

    similar_artists_int_list = map(lambda x: str(x), all_simliar_artists)
    similar_artists_int_list = ",".join(similar_artists_int_list)
    similar_artists_int_list = "("+similar_artists_int_list+")"

    today_date = datetime.now().date()
    past_date = today_date - timedelta(days=180)

    today_date = "'{}'".format(str(today_date))
    past_date = "'{}'".format(str(past_date))
    if all_event_artists and all_simliar_artists:
        query = '''
        SELECT DISTINCT dj_Master.artistID
            FROM dj_Master
            JOIN dj_Events ON dj_Master.eventID = dj_Events.eventID
            JOIN dj_Venues ON dj_Venues.venueID = dj_Master.venueID
            WHERE dj_Master.artistID IN {}
            AND dj_Events.date < {} and dj_Events.date > {}
            AND UPPER(dj_Venues.city) = UPPER('{}');
        '''.format(similar_artists_int_list, today_date, past_date, event.city)

        conn = DBInterface()
        conn.connect()
        booked_dates_artists = conn.execute_query(query)
        booked_date_artists_list = [i[0] for i in booked_dates_artists]

        final_similar_artist = set(all_simliar_artists) - set(booked_date_artists_list)

        return Artist.objects.filter(artistID__in=final_similar_artist)
    else:
        return None