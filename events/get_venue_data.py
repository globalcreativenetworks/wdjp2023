import requests

import json
from urllib2 import urlopen


def get_data(address,venuename):

	placeid=getPlaceId(address,None)
	MyUrl = ('https://maps.googleapis.com/maps/api/place/details/json'
	           '?placeid=%s'
	           '&fields=name,photo,formatted_address'
	           '&key=AIzaSyCJTSEiRDKSHOsV5jbnxrISJXoopUUlhgw') % (placeid)
	response = urlopen(MyUrl)
	jsonRaw = response.read()
	jsonData = json.loads(jsonRaw)
	photo_reference_list=[]
	if 'photos' in jsonData['result'].keys():
		for photo in jsonData['result']['photos']:
	  		photo_reference_list.append(photo['photo_reference'])
		return jsonData['result']['formatted_address'],jsonData['result']['name'],photo_reference_list
	else:
		try:
			placeid=getPlaceId(None,venuename)
			MyUrl =('https://maps.googleapis.com/maps/api/place/details/json'
	           '?placeid=%s'
	           '&fields=name,photo,formatted_address'
	           '&key=AIzaSyCJTSEiRDKSHOsV5jbnxrISJXoopUUlhgw') % (placeid)
		  	response = urlopen(MyUrl)
		  	jsonRaw = response.read()
		  	jsonData = json.loads(jsonRaw)
		  	photo_reference_list=[]
		  	if 'photos' in jsonData['result'].keys():
				for photo in jsonData['result']['photos']:
	  				photo_reference_list.append(photo['photo_reference'])
				return jsonData['result']['formatted_address'],jsonData['result']['name'],photo_reference_list

		  	else:
				return jsonData['result']['formatted_address'],jsonData['result']['name'],None
		except Exception as e:
			return jsonData['result']['formatted_address'],jsonData['result']['name'],None

def getPlaceId(address=None,venuename=None):
	if address:
		google_location_api_url = ("https://maps.google.com/maps/api/geocode/json?address=%s&types=establishment&key=AIzaSyCJTSEiRDKSHOsV5jbnxrISJXoopUUlhgw")%(address.strip().replace(" ", "+").encode('UTF-8'))
	else:
		google_location_api_url = ("https://maps.google.com/maps/api/geocode/json?address=%s&types=establishment&key=AIzaSyCJTSEiRDKSHOsV5jbnxrISJXoopUUlhgw")%(venuename.strip().replace(" ", "+").encode('UTF-8'))

	response = urlopen(google_location_api_url)
	jsonRaw = response.read()
	json_response = json.loads(jsonRaw)	
	if json_response['status'] == "OK":
		return json_response['results'][0]['place_id']

def getImages(photo_reference_list):
	url_list=[]
	for photoreference in photo_reference_list:
		MyUrl = ('https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=%s&key=AIzaSyCmEPHe8UrFGQYqRnaLG9c7mKPIpBSb7sQ'
	)%(photoreference)
		url_list.append(MyUrl)
	return url_list
