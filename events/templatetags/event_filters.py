import pycountry
from django.db.models import Q
from django.template import Library
from django.template.defaultfilters import stringfilter

from events.models import DjMaster
from geography.models import City, Country

register = Library()


@register.filter
def get_country_code(country):
    country_code = Country.objects.filter(
        Q(full_name__iexact=country) | Q(full_name__contains=country) | Q(
            code__iexact=country) | Q(name__iexact=country)
    ).first()
    if country_code:
        return country_code.code.lower()
    else:
        return ""


@register.filter
def event_unique(event):
    try:
        return DjMaster.objects.filter(eventid=event)
    except (ValueError, ZeroDivisionError):
        return ""


@register.filter
def event_genres(event):
    genres = []
    try:
        master_events = DjMaster.objects.filter(
            eventid=event).select_related('artistid')
        for master in master_events:
            genres.extend(
                [gen.genreName for gen in master.artistid.genre.all()])
        genres = list(set(genres))
    except:
        pass
    return genres


@register.filter(name='split')
@stringfilter
def split(value, key):
    """
        Returns the value turned into a list.
    """
    return value.split(key)


@register.filter
def placeholder(value, token):
    value.field.widget.attrs["placeholder"] = token
    return value
