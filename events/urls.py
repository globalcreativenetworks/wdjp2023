from django.conf.urls import url

from events import views

urlpatterns = [
   url(r'^event/(?P<event_id>\d+)/$', views.event_show, name='event_show'),
   url(r'^my-favourite-event/$', views.favorite, name="my_favorite_event"),
   url(r'^add-or-remove-favorite-venue/$', views.favorite_event, name='add_or_remove_favorite_event'),
   url(r'^maps/$', views.event_map, name="event_map"),

   url(r'delete-event-note/(?P<note_id>\d+)/$', views.delete_event_note, name='delete_event_note'),
   url(r'^my-events/$', views.my_events, name="my_events"),
   url(r'^delete-event/$', views.delete_event, name="delete_event"),
   url(r'^add-event/$', views.booking_request, name="booking_request"),
   url(r'^update-event/(?P<event_id>\d+)/$', views.update_booking_request, name="update_booking_request"),
   url(r'^multiple-invitation/$', views.invitation, name="invitation"),
   # url(r'^amsterdam-event/$', views.amsterdam_event, name="amsterdam_event"),
   url(r'^event-detail/(?P<event_id>\d+)/$', views.event_detail, name="event_detail"),
   url(r'^shortlisted-artist/(?P<event_id>\d+)/$', views.shortlisted_artist, name="shortlisted_artist"),
   url(r'^event-stats/(?P<event_id>\d+)/$', views.event_stats, name="event_stats"),
   url(r'^recommended-artists/(?P<event_id>\d+)/$', views.recommended_artists, name="recommended_artists"),
   url(r'^booked-artists/(?P<event_id>\d+)/$', views.booked_artists, name="booked_artists"),
   url(r'^offered-artists/(?P<event_id>\d+)/$', views.offered_artists, name="offered_artists"),
   url(r'^event-booking-request/(?P<event_id>\d+)/(?P<artist_id>\d+)/$', views.event_booking_request, name="event_booking_request"),
   url(r'^add-shortlist_artists/$', views.assign_shorlisted_artist_to_events, name="assign_shorlisted_artist_to_events"),
   url(r'^event-news/(?P<event_id>\d+)/$', views.event_news, name="event_news"),
   url(r'^edit-event-details/(?P<event_id>\d+)/$', views.edit_event_detail, name="edit_event_detail"),
   url(r'^shortlist-artist$', views.shortlist_artist, name="shortlist_artist"),
   url(r'^event-booking-request-email/(?P<event_id>\d+)/(?P<artist_id>\d+)/$', views.event_booking_request_email, name="event_booking_request_email"),
   url(r'^search_events/$', views.search_events, name="search_events"),
   url(r'^event-note/(?P<note_id>\d+)/$', views.event_note, name='event_note'),
   url(r'^event-comment/(?P<event_id>\d+)/$', views.event_comment, name='event_comment'),
   url(r'^venues/shortlist-venue/$', views.shortlist_venue, name="shortlist_venue"),
   url(r'^shortlist-event/$', views.shortlist_event, name="shortlist_event"),
   url(r'^venue-detail/(?P<venue_id>\d+)/$' , views.venue_detail , name = "venue_detail"),
   url(r'^venues/$' , views.venues , name = "venues"),
   url(r'^load-events/(?P<venue_id>\d+)/$' , views.load_events_venues , name = "load_events_venues"),
   url(r'^venue-note/(?P<note_id>\d+)/$', views.venue_note, name='venue_note'),
   url(r'^venue-comment/(?P<venue_id>\d+)/$', views.venue_comment, name='venue_comment'),
   url(r'delete-venue-note/(?P<note_id>\d+)/$', views.delete_venue_note, name='delete_venue_note'),
   
   # All events notes
    url(r'^notes/$', views.events_notes, name='events_notes'),

    # Create Duplicate Event
    url(r'^duplicate-event/(?P<event_id>\d+)/$', views.duplicate_event, name='create_duplicate_event'),

    # Get Event Type Template
    url(r'^event-template/(?P<template_id>\d+)/$', views.get_event_template, name='get_event_template'),

    # Create Event Template
    url(r'^create-event-template/$', views.create_event_template, name='create_event_template'),
]
