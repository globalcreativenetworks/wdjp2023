from django.contrib import admin

from .models import WpMyEvents, EventBookingRequest, EventNote, VenueNote, DjVenues, ShortlistedVenue, ShortlistedEvent, DjMaster, EventTypeTemplate

class ShortlistedVenueAdmin(admin.ModelAdmin):
    list_display = ['artist', 'user']

class ShortlistedEventAdmin(admin.ModelAdmin):
    list_display = ['artist', 'user']

class EventTypeTemplateAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'role', 'user']


admin.site.register(WpMyEvents)
admin.site.register(EventBookingRequest)
admin.site.register(EventNote)
admin.site.register(VenueNote)
admin.site.register(DjVenues)
admin.site.register(ShortlistedVenue, ShortlistedVenueAdmin)
admin.site.register(ShortlistedEvent, ShortlistedEventAdmin)
admin.site.register(DjMaster)
admin.site.register(EventTypeTemplate, EventTypeTemplateAdmin)