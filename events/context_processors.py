from datetime import datetime
from profiles.models import Profile
from artist.models import Artist
from .models import WpMyEvents
from accounts.decorators import check_trail_user
from django.conf import settings
from accounts.models import UserFrom, Applications

from artist.models import UserNote
from events.models import EventNote
from news.models import News, WdjpNews
from profiles.models import ProfilesProfileFavoriteVenues


def header_events(request):
    header_my_events = []
    future_events = []
    past_events = []

    if request.user.is_authenticated():

        today_date = str(datetime.today().strftime('%Y-%m-%d'))

        future_events = WpMyEvents.objects.filter(event_date__gte=today_date, user_id=request.user.id)

        past_events = WpMyEvents.objects.filter(event_date__lte=today_date, user_id=request.user.id)

        header_my_events = WpMyEvents.objects.filter(user_id=request.user.id).order_by('-created_on')
    return {
        'header_my_events': header_my_events,
        'future_events': future_events,
        'past_events': past_events,
    }

def return_settings(request):
    return {
        'settings': settings,
    }

def return_env(request):
    try:
        from WDJP_ENV import *
        if ENV_NAME == "PRODUCTION":
            return {
                'system_env': 'PRODUCTION'
            }
        elif ENV_NAME == "STAGING":
            return {
                'system_env': 'STAGING'
            }
    except:
        return {
                'system_env': 'LOCAL'
            }


def is_premium(request):
    is_premium_user = False

    # if request.user.is_authenticated():
    #     user = request.user
    #     user_from = UserFrom.objects.filter(user=user).first()

    #     wdjp_application = Applications.objects.filter(application_name="Wheredjsplay").first()

    #     if wdjp_application in user_from.user_from.all():
    #         profile = Profile.objects.get(user=request.user)
    #         if profile.plan:
    #             is_premium_user = True

    #     return {
    #         'is_premium_user': is_premium_user,
    #     }


    #     trial_user = check_trail_user(request, profile)
    #     if trial_user:
    #         is_premium_user = True
    #     if profile.is_premium or request.user.is_superuser:
    #         is_premium_user = True

    return {
        'is_premium_user': True,
    }

def get_footer_artist(request):
    footer_artists = Artist.objects.all().order_by('-artistID')[:4]
    return {
        'footer_artists': footer_artists,
    }

def artist_of_the_day(request):
    date = datetime.now().day
    month = datetime.now().month
    artist_id = date * month
    artist_of_the_day = get_artist_random(artist_id)

    return {
        'artist_of_the_day': artist_of_the_day,
    }


def get_artist_random(artist_id):
    try:
        artist = Artist.objects.get_queryset().get(artistID=artist_id)
        return artist
    except:
        return Artist.objects.get_queryset().order_by('?').first()

def user_location(request):
    try:
        user_session_lat = request.COOKIES['user_session_lat']
    except:
        user_session_lat = ""
    try:
        user_session_long =  request.COOKIES['user_session_long']
    except:
        user_session_long = ""

    try:
        user_session_city = request.COOKIES['user_session_city']
    except:
        user_session_city = ""

    user_session_location = {
        'lat': user_session_lat,
        'long': user_session_long,
        'city': user_session_city,
    }

    return {
        'user_session_location': user_session_location,
    }

# def get_footer_events(request):
#     footer_my_events = []
#     if request.user.is_authenticated():
#         footer_my_events = WpMyEvents.objects.filter(user_id=request.user.id).order_by('-created_on')[:4]
#     return {
#         'footer_my_events': footer_my_events,
#     }

def get_header_news(request):
    try:
        # header_news=News.objects.latest('publish_date')
        header_news = News.objects.all().order_by('-publish_date')[:6]
        news_artist = []

        for news in header_news:
            artist = news.artists.all()
            if artist:
                news_artist.append(artist[0])

    except:
        header_news = []
        news_artist = []

    return{
        'header_news': header_news,
        'news_artist':news_artist
    }

def user_type(request):
    try:
        user = request.user
        pro_obj = user.profile_set.all()
        profile = pro_obj[0]
        user_type = profile.user_types
        return {
            'user_type': user_type,
            'user_profile':profile,
        }
    except:
        return {
            'user_type': "",
            'user_profile':None,
        }


def wdjp_news(request):
    limit1 = 8
    limit2 = 3
    start2 = 0
    start1 = 3
    application = Applications.objects.filter(application_name="WheredjsPlay").first()

    wdjp_news = WdjpNews.objects.filter(news_for=application, is_visible=True).order_by('-published_date')
    wdjp_news = wdjp_news[start1: start1 + limit1]
    wdjp_news_all = WdjpNews.objects.filter(news_for=application, is_visible=True).order_by('-published_date')
    wdjp_news_all = wdjp_news_all[start2: start2 + limit2]

    return {
        "wdjp_news":wdjp_news,
        "wdjp_news_all":wdjp_news_all,
        "site_url":settings.SITE_URL,
    }


def my_favorite_venue(request):
    my_fav_venue=[]

    if request.user.is_authenticated():
            pro_obj = Profile.objects.filter(user=request.user).first()

            try:
                my_fav_venue = pro_obj.favorite_venues.all().count()
            except AttributeError:
                my_fav_venue = 0
    return {
        'my_fav_venue': my_fav_venue,
        }

def my_favorite_event(request):
    my_fav_venue=[]

    if request.user.is_authenticated():
            pro_obj = Profile.objects.filter(user=request.user).first()
            try:
                my_fav_venue = pro_obj.favorite_events.all().count()
            except AttributeError:
                my_fav_venue = 0
    return {
        'my_fav_event': my_fav_venue,
        }

def my_favorite_artist(request):
    my_fav_artist=[]

    if request.user.is_authenticated():
            pro_obj = Profile.objects.filter(user=request.user).first()
            try:
                my_fav_artist = pro_obj.favorite_artist.all().count()
            except AttributeError:
                my_fav_artist = 0
    return {
        'my_fav_artist': my_fav_artist,
        }


def my_account(request):
    my_account_image = []

    if request.user.is_authenticated():
            pro_obj = Profile.objects.filter(user=request.user).first()
            try:
                my_account_image = pro_obj.image
            except AttributeError:
                my_account_image = ''

    return {
        'my_account_image': my_account_image,
    }

def my_managed_artists(request):
    my_managed_artist_list=[]

    if request.user.is_authenticated():
        pro_obj = Profile.objects.filter(user=request.user).first()
        try:
            my_managed_artist_list = pro_obj.profile_managed_djs.all()
        except AttributeError:
            my_managed_artist_list = []
    return {
        'my_managed_artists': my_managed_artist_list,
        }

# Returning Artist Notes Count
def my_artist_notes(request):
    my_artist_notes = 0
    if request.user.is_authenticated():
        notes = UserNote.objects.filter(user=request.user)
        if notes:
            my_artist_notes = notes.count()         

    return {
        'my_artist_notes': my_artist_notes
    }

# Returning Event Notes Count
def my_event_notes(request):
    my_event_notes = 0
    if request.user.is_authenticated():
        notes = EventNote.objects.filter(user=request.user)
        if notes:
            my_event_notes = notes.count()

    return {
        'my_event_notes': my_event_notes
    }