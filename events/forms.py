from __future__ import unicode_literals
from datetime import datetime

from ckeditor.widgets import CKEditorWidget

from django import forms
from events.models import WpMyEvents, EventBookingRequest, EventNote, EventComment, VenueNote, VenueComment, EventTypeTemplate


BOOL_CHOICES = ((True, 'Yes'), (False, 'No'))

time_widget = forms.widgets.TimeInput(attrs={'class': 'timepicker py-form-control'})
time_widget.format = '%I:%M %p'

class BookingRequestForm(forms.Form):
    event_date = forms.DateField(widget=forms.TextInput(attrs={'class': 'py-form-control datepicker',}), required=False)
    event_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control', 'required': 'required'}))
    venue_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control', 'required': 'required'}))
    event_city = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control', 'required': 'required'}))
    capacity_of_venue = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control'}), required=False)
    country = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control', 'required': 'required'}))
    nearest_airport = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control'}), required=False)
    airport_to_venue_distance = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control'}), required=False)
    venue_address = forms.CharField(widget=forms.Textarea(attrs={'class': 'py-form-control', 'cols': '', 'rows': '', 'required': 'required'}))
    estimate_finanical_offer = forms.ChoiceField(choices=(('Will send later', 'WILL SEND LATER'), ('Amount', 'AMOUNT')), widget=forms.Select(attrs={'class':'py-form-control'}), required=False)
    estimate_currency = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control'}), required=False)
    estimate_amount = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control'}), required=False)
    suggested_hotels = forms.CharField(widget=forms.Textarea(attrs={'class': 'py-form-control', 'cols': '', 'rows': ''}), required=False)

    any_other_requested_dj = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control'}), required=False)
    contact_email = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control'}), required=False)
    ticket_price = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control'}), required=False)
    vip_sections = forms.ChoiceField(choices=(('Yes', 'Yes'), ('No', 'No')), widget=forms.RadioSelect(), required=False)
    recent_artwork_link = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control'}), required=False)
    recent_video_link = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control'}), required=False)
    artists_before = forms.CharField(widget=forms.Textarea(attrs={'class': 'py-form-control', 'cols': '', 'rows': ''}), required=False)

    # stage_time = forms.CharField(widget=forms.TimeInput(attrs={'class': 'py-form-control'}), required=False)
    # set_length = forms.CharField(widget=forms.TimeInput(attrs={'class': 'py-form-control'}), required=False)
    stage_time = forms.TimeField(input_formats=['%I:%M %p'], widget=time_widget, required=False)
    set_length = forms.TimeField(input_formats=['%I:%M %p'], widget=time_widget, required=False)
    stage_or_dj = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control'}), required=False)
    requested_show_date = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control datepicker',}), required=False)
    rough_financial_offer = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control'}), required=False)
    indoor_or_outdoor = forms.ChoiceField(choices=(('Indoor', 'Indoor'), ('Outdoor', 'Outdoor')), widget=forms.RadioSelect(), required=False)
    min_age_req = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control'}), required=False)
    working_visa_provided = forms.ChoiceField(choices=(('Yes', 'Yes'), ('No', 'No')), widget=forms.RadioSelect(), required=False)

    vat_no = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control'}), required=False)
    company_address = artists_before = forms.CharField(widget=forms.Textarea(attrs={'class': 'py-form-control', 'cols': '', 'rows': ''}), required=False)
    company_city = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control'}), required=False)
    company_country = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control'}), required=False)
    # genres = forms.CharField(widget=forms.TextInput(attrs={'class': 'py-form-control'}), required=True)


    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user',None)
        self.event = kwargs.pop('event', None)
        super(BookingRequestForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(BookingRequestForm, self).clean()
        event_name = cleaned_data.get("event_name", "")
        event_date = cleaned_data.get("event_date", "")
        if not self.event:
            # if WpMyEvents.objects.filter(event_name=event_name, event_date=event_date, user_id=self.user.id).exists():
            #     raise forms.ValidationError("This event name and event date exists.")
            pass
        else:
            if WpMyEvents.objects.all().exclude(event_id=self.event.event_id).filter(event_name=event_name, event_date=event_date, user_id=self.user.id).exists():
                raise forms.ValidationError("This event name and event date exists.")
        return cleaned_data


class EventBookingRequestForm(forms.ModelForm):
    body = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = EventBookingRequest
        fields = ('to', 'subject', 'body' )

        # widgets = {
        #     'artistPhoto': FileInput(),
        # }

    def __init__(self, *args, **kwargs):
        super(EventBookingRequestForm, self).__init__(*args, **kwargs)
        self.fields['to'].widget.attrs.update({'class': 'py-form-control', 'required': 'required', 'cols': '100', 'rows': '5'})
        self.fields['subject'].widget.attrs.update({'class': 'py-form-control', 'cols': '100', 'rows': '5' })
        # self.fields['body'].widget.attrs.update({'class': 'richtexteditor', 'cols': '100', 'rows': '5' })


class EventNotesForm(forms.ModelForm):
    
    class Meta:
        model = EventNote
        fields = ('event','note','user')
        widgets = {'event': forms.HiddenInput(), 'user': forms.HiddenInput()}

    def __init__(self, *args, **kwargs):
        super(EventNotesForm, self).__init__(*args, **kwargs)
        self.fields['note'].widget.attrs.update({'class': 'form-control', 'rows':5})


class EventCommentsForm(forms.ModelForm):
    content = forms.CharField(label="", widget=forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Enter your comment here.', 'rows':'4', 'cols':'50'}))
    
    class Meta:
        model = EventComment
        fields = ('content',)


class VenueNotesForm(forms.ModelForm):
    
    class Meta:
        model = VenueNote
        fields = ('venue','note','user')
        widgets = {'venue': forms.HiddenInput(), 'user': forms.HiddenInput()}

    def __init__(self, *args, **kwargs):
        super(VenueNotesForm, self).__init__(*args, **kwargs)
        self.fields['note'].widget.attrs.update({'class': 'form-control', 'rows':5})


class VenueCommentsForm(forms.ModelForm):
    content = forms.CharField(label="", widget=forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Enter your comment here.', 'rows':'4', 'cols':'50'}))
    
    class Meta:
        model = VenueComment
        fields = ('content',)

class EventTypeTemplateForm(forms.ModelForm):

    class Meta:
        model  = EventTypeTemplate
        fields = ('name', 'body')