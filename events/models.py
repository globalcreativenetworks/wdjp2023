from __future__ import unicode_literals

import json

from ckeditor.fields import RichTextField
from django.contrib.auth.models import User
from django.db import models
from django.db.models.query import QuerySet
from django.utils import timezone
from django_group_by import GroupByMixin

from artist.models import Artist, Genre

# Create your models here.

class Event(models.Model):
    eventID = models.AutoField(primary_key=True, db_column='eventID')
    eventName = models.CharField(max_length=500)
    evenueName = models.CharField(max_length=500)
    bandsintownID = models.IntegerField(blank=True, null=True)
    residentID = models.IntegerField(blank=True, null=True)
    gigatoolsID = models.IntegerField(blank=True, null=True)
    songkickID = models.IntegerField(blank=True, null=True)
    facebookID = models.IntegerField(blank=True, null=True)
    date = models.DateField(editable=False)
    startTime = models.TimeField(editable=False)
    endTime = models.TimeField(editable=False)
    created = models.DateTimeField(default=timezone.now(), null=True, blank=True)


    def __str__(self):
        return self.eventName

    class Meta:
        managed = False
        db_table = 'dj_Events'
        verbose_name = "Event"
        verbose_name_plural = "Events"

class DjVenues(models.Model):
    venueid = models.AutoField(db_column='venueid', primary_key=True)  # Field name made lowercase.
    venuename = models.CharField(db_column='venueName', max_length=200)  # Field name made lowercase.
    venueaddress = models.CharField(db_column='venueAddress', max_length=200)  # Field name made lowercase.
    venuelatitude = models.DecimalField(db_column='venueLatitude', max_digits=12, decimal_places=9, blank=True, null=True)  # Field name made lowercase.
    venuelongitude = models.DecimalField(db_column='venueLongitude', max_digits=13, decimal_places=9, blank=True, null=True)  # Field name made lowercase.
    venuebands = models.IntegerField(db_column='venueBands')  # Field name made lowercase.
    venueresident = models.IntegerField(db_column='venueResident')  # Field name made lowercase.
    venuegigatools = models.CharField(db_column='venueGigatools', max_length=100)  # Field name made lowercase.
    venuesongkick = models.IntegerField(db_column='venueSongkick')  # Field name made lowercase.
    city = models.CharField(db_column='city',max_length=50)
    country = models.CharField(db_column='country',max_length=50)
    venuefacebook = models.BigIntegerField(db_column='venueFacebook')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'dj_Venues'

    def __str__(self):
        return self.venuename



class DjMasterQuerySet(QuerySet, GroupByMixin):
    pass


class DjMaster(models.Model):
    masterid = models.AutoField(db_column='masterID', primary_key=True)  # Field name made lowercase.
    artistid = models.ForeignKey(Artist,on_delete=models.CASCADE,db_column='artistID')  # Field name made lowercase.
    eventid = models.ForeignKey(Event,on_delete=models.CASCADE,db_column='eventID')  # Field name made lowercase.
    venueid = models.ForeignKey(DjVenues,on_delete=models.CASCADE,db_column='venueID')  # Field name made lowercase.

    objects = DjMasterQuerySet.as_manager()

    class Meta:
        managed = False
        db_table = 'dj_Master'
        verbose_name = 'Master'
        verbose_name_plural = 'Master'

class EventTypeTemplate(models.Model):
    ROLES = (('admin', 'Admin'), ('user', 'User'))

    name = models.CharField(max_length=200)
    body = models.TextField(blank=True, null=True)
    role = models.CharField(choices=ROLES, max_length=100)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

class WpMyEvents(models.Model):
    event_id = models.AutoField(primary_key=True, db_column='event_id')
    event_name = models.CharField(max_length=255)
    content = models.TextField(null=True,blank=True)
    offersheet = models.TextField(null=True,blank=True)
    city = models.CharField(max_length=255)
    event_date = models.DateField(null=True, blank=True)
    user_id = models.BigIntegerField()
    created_on = models.DateTimeField()
    data = models.TextField(null=True,blank=True)
    invited_users = models.ManyToManyField(Artist, db_table='wp_my_events_invited_users', related_name="invited_users")
    shortlisted_artists = models.ManyToManyField(Artist, related_name="shortlisted_artists", blank=True)
    booked_artists = models.ManyToManyField(Artist, through='BookedArtistForUserEvent', related_name="booked_artists")
    genre = models.ManyToManyField(Genre)
    latitude = models.DecimalField(max_digits=12, decimal_places=9, blank=True, null=True)
    longitude = models.DecimalField(max_digits=13, decimal_places=9, blank=True, null=True)
    event_type_template = models.ForeignKey(EventTypeTemplate, on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        managed = True
        db_table = 'wp_my_events'

    def __str__(self):
        return self.event_name or None

    def __unicode__(self):
       return self.event_name or u'None'

    def get_genre_string(self):
        return (", ").join(self.genre.all().values_list('genreName', flat=True))

    @property
    def get_country(self):
        if not self.data:
            return ''
        else:
            return json.loads(self.data).get('country')

    def data_all_fields(self):
        data_dict={}
        if not self.data:
            return {}
        object_data = json.loads(self.data)
        for key, value in object_data.iteritems():
            key = key.replace("_", " ")
            data_dict[key] = value
        return data_dict

    def have_offer_data(self):
        data = self.data
        if not data:
            return "false"
        object_data = json.loads(self.data)
        object_data.pop('country', '')
        object_data.pop('venue_name', '')
        object_data.pop('capacity_of_venue', '')
        object_data.pop('nearest_airport', '')
        object_data.pop('airport_to_venue_distance', '')
        object_data.pop('venue_address', '')
        object_data.pop('suggested_hotels', '')
        object_data.pop('estimate_finanical_offer', '')
        object_data.pop('estimate_amount', '')
        object_data.pop('estimate_currency', '')
        for key,value in object_data.iteritems():
            if object_data[key]:
                return "true"
        return "false"


class BookedArtistForUserEvent(models.Model):
    event = models.ForeignKey(WpMyEvents)
    artist = models.ForeignKey(Artist)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.created_on

class EventBookingRequest(models.Model):
    to = models.TextField(null=True, blank=True)
    subject = models.TextField(null=True, blank=True)
    # body = models.TextField(null=True, blank=True)
    body = RichTextField(verbose_name='body', null=True, blank=True)
    event = models.ForeignKey(WpMyEvents)
    artist = models.ForeignKey(Artist)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.subject


# wp_my_events_invited_users
class OfferedArtistForUserEvent(models.Model):
    wpmyevents = models.ForeignKey(WpMyEvents)
    artist = models.ForeignKey(Artist)
    created_on = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.wpmyevents
    
    def __unicode__(self):
           return self.wpmyevents or u'None'

    class Meta:
        managed = True
        db_table = 'wp_my_events_invited_users'


class DjParsedData(models.Model):
    id = models.AutoField(db_column='PK', primary_key=True)  # Field name made lowercase.
    eventid = models.BigIntegerField(db_column='eventID')  # Field name made lowercase.
    eventname = models.CharField(db_column='eventName', max_length=500)  # Field name made lowercase.
    venueid = models.CharField(db_column='venueID', max_length=255)  # Field name made lowercase.
    venuename = models.CharField(db_column='venueName', max_length=500)  # Field name made lowercase.
    venueaddress = models.CharField(db_column='venueAddress', max_length=100)  # Field name made lowercase.
    latitude = models.DecimalField(max_digits=12, decimal_places=9)
    longitude = models.DecimalField(max_digits=13, decimal_places=9)
    date = models.DateField()
    starttime = models.CharField(db_column='startTime', max_length=10)  # Field name made lowercase.
    endtime = models.CharField(db_column='endTime', max_length=10)  # Field name made lowercase.
    city = models.CharField(max_length=50)
    country = models.CharField(max_length=50)
    artistid = models.IntegerField(db_column='artistID')  # Field name made lowercase.
    sourceid = models.IntegerField(db_column='sourceID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'dj_parsed_data'

class EventNote(models.Model):
    event = models.ForeignKey(Event)
    user = models.ForeignKey(User)
    note = models.TextField(blank=True, null=True)
    modified_date = models.DateTimeField(null=True,blank=True)

    class Meta:
        verbose_name = 'Event Note'
        verbose_name_plural = 'Event Notes'

    def __str__(self):
        return self.note

class EventComment(models.Model):
    event = models.ForeignKey(Event)
    user = models.ForeignKey(User)
    reply = models.ForeignKey('EventComment', null=True,blank=True, related_name="event_replies")
    content = models.TextField(max_length=250)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{}...-{}-{}'.format(self.content[:10], self.event.eventName, str(self.user.username))


class VenueNote(models.Model):
    venue = models.ForeignKey(DjVenues)
    user = models.ForeignKey(User)
    note = models.TextField(blank=True, null=True)
    modified_date = models.DateTimeField(null=True,blank=True)

    class Meta:
        verbose_name = 'Venue Note'
        verbose_name_plural = 'Venue Notes'

    def __str__(self):
        return self.note

class VenueComment(models.Model):
    venue = models.ForeignKey(DjVenues)
    user = models.ForeignKey(User)
    reply = models.ForeignKey('VenueComment', null=True,blank=True, related_name="venue_replies")
    content = models.TextField(max_length=250)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{}...-{}-{}'.format(self.content[:10], self.venue.venuename, str(self.user.username))

class ShortlistedVenue(models.Model):
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE)
    user = models.ForeignKey(User,  on_delete=models.CASCADE)
    venueids = models.ManyToManyField(DjVenues, blank=True)

    def __str_(self):
        return self.artist

class ShortlistedEvent(models.Model):
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    eventids = models.ManyToManyField(Event, blank=True)

    def __str_(self):
        return self.artist