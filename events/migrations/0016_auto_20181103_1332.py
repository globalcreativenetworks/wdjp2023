# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-11-03 13:32
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0015_auto_20181103_1331'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='created',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2018, 11, 3, 13, 32, 26, 809483, tzinfo=utc), null=True),
        ),
    ]
