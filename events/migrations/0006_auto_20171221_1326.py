# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-12-21 13:26
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('artist', '0011_artist_contact_no'),
        ('events', '0005_wpmyevents_genre'),
    ]

    operations = [
        migrations.CreateModel(
            name='BookedArtistForUserEvent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('artist', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='artist.Artist')),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='events.WpMyEvents')),
            ],
        ),
        migrations.AddField(
            model_name='wpmyevents',
            name='booked_artists',
            field=models.ManyToManyField(related_name='booked_artists', through='events.BookedArtistForUserEvent', to='artist.Artist'),
        ),
    ]
