# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2019-11-13 10:07
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('artist', '0026_generalartist_status'),
        ('events', '0021_venuecomment_venuenote'),
    ]

    operations = [
        migrations.CreateModel(
            name='ShortlistedVenue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('artist', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='artist.Artist')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('venueids', models.ManyToManyField(blank=True, to='events.DjVenues')),
            ],
        ),
    ]
