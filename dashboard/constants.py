USER_TYPES = (
    ('Promoter', 'Promoter'),
    ('Agent', 'Agent'),
    ('Artists/DJs', 'Artists/DJs'),
    ('PressFanRecord', 'PressFanRecord'),
    ('Companies', 'Companies'),
    ('Management', 'Management')
)

WELCOME_USER_TYPES = (
    ('Promoter', 'Promoter'),
    ('Artists/DJs', 'DJ'),
    ('Agent', 'Agent'),
)


DEFAULT_DASHBOARD = [
    "Promoter",
    "PressFanRecord",
    "Companies",
    "Management",
    None,
    "",
]

AGENT_DASHBOARD = [
    "Agent"
]
