from django import forms

from ckeditor.widgets import CKEditorWidget

from .models import WelcomeMessage

from dashboard.constants import WELCOME_USER_TYPES


class WdjpWelcomeMessageForm(forms.ModelForm):
    welcome_text = forms.CharField(widget=CKEditorWidget())
    # is_visible = forms.BooleanField()

    class Meta:
        model = WelcomeMessage
        fields = ('welcome_text','type')

    def __init__(self, *args, **kwargs):
        super(WdjpWelcomeMessageForm, self).__init__(*args, **kwargs)
        self.fields['welcome_text'].widget.attrs.update({'class': 'richtexteditor', 'cols': '90', 'rows': '5', 'required':'required'})
        self.fields['type'].widget.attrs.update({'class': 'py-form-control', 'required':'required'})
        self.fields['type'].choices = WELCOME_USER_TYPES