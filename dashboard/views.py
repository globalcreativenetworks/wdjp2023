import json
from datetime import datetime

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.conf import settings
from django.http import HttpResponse

from artist.views import get_artist_statistics, get_artist_social_url, \
    get_similar_artist, get_upcoming_events, get_past_events, get_artist_news
from profiles.models import Profile, ProfileTracker

from news.models import News
from news.utils import get_local_news

from accounts.decorators import check_email_validated
from events.models import WpMyEvents
from events.context_processors import get_artist_random
from home.views import get_user_near_event
from dashboard.constants import DEFAULT_DASHBOARD
from social_data.utils import get_user_top_country_an_likes
from events.utils import get_artist_fb_likes, get_color_hashes
from .forms import WdjpWelcomeMessageForm
from .models import WelcomeMessage

@login_required
@check_email_validated
def dashboard(request):
    profile_obj = Profile.objects.get(user=request.user)
    dj_artist = profile_obj.dj_artist

    if profile_obj.user_types == "Artists/DJs":

        # if dj_artist is None:
        #     return render(request, "dashboard/create_dj_artist.html")
        # else:
        return redirect(reverse("dj_dashboard"))

    else:
        context = {}
        fav_artist_news_list = []

        trackers = profile_obj.profiletracker_set.all()
        if len(trackers) > 0:
            context.update({
                "all_trackers": trackers,
            })

        if profile_obj.user_types == "Agent":

            agent_artists_facebook_stats = {}
            for shortlisted_artist in profile_obj.all_managed_artists.all():
                agent_artists_facebook_stats.update(
                    get_artist_fb_likes(shortlisted_artist.artist)
                )

            color_code = get_color_hashes(agent_artists_facebook_stats)

            agent_artists_news = News.objects.filter(
                artists__in=profile_obj.shortlist_artist.all(),
            ).order_by('-publish_date').distinct()[:12]

            # Fav Venues
            if profile_obj.favorite_venues.all().exists():
                favorite_venues = profile_obj.favorite_venues.all()[:5]
            else:
                favorite_venues = []

            # Managed Artist News
            managed_artists_news_list = []
            pro_man_artists = profile_obj.profile_managed_djs.all().order_by('?')
            for man_art in pro_man_artists:
                man_news = {}
                if len(man_news.keys()) < 6:
                    man_artist_news = News.objects.filter(artists=man_art).order_by('-publish_date').distinct()[:5]
                    if man_artist_news:
                        man_news["artist"] = man_art
                        man_news["news"] = man_artist_news
                        managed_artists_news_list.append(man_news)

            context.update({
                "agent_artists_facebook_stats": agent_artists_facebook_stats,
                "agent_artists_news": agent_artists_news,
                "color_code": color_code,
                "favorite_venues": favorite_venues,
                "managed_artists_news_list":managed_artists_news_list
            })

        else:

            if profile_obj.favorite_venues.all().exists():
                favorite_venues = profile_obj.favorite_venues.all()[:3]
            else:
                favorite_venues = []

            # active_tracker = profile_obj.profiletracker_set.filter(is_active=True)
            # if len(active_tracker) > 0 :
            #     context.update({
            #         "active_tracker":active_tracker[0],
            #     })
            user_coming_events = WpMyEvents.objects.filter(
                user_id=request.user.id,
                event_date__gt=datetime.now().date())

            # User Local News
            local_news = get_local_news(request, count=5)

            all_events = WpMyEvents.objects.filter(user_id=request.user.id).order_by('-created_on')


            context.update({
                "user_coming_events": user_coming_events,
                "local_news": local_news,
                "favorite_venues": favorite_venues,
                "all_events": all_events
            })


        # Fav Artist News
        pro_fav_artist = profile_obj.favorite_artist.all().order_by('?')
        for artist in pro_fav_artist:
            fav_news = {}
            if len(fav_news.keys()) < 6:
                artist_news = News.objects.filter(artists=artist).order_by('-publish_date').distinct()[:5]
                if artist_news:
                    fav_news["artist"] = artist
                    fav_news["news"] = artist_news
                    fav_artist_news_list.append(fav_news)

        # Get Near By Events
        events_added = WpMyEvents.objects.filter(user_id=request.user.id).order_by("-event_date")

        djs_list = []

        for event in events_added:
            for artist in event.shortlisted_artists.all():
                djs_list.append(artist)
        try:
            near_by_events = get_user_near_event(request)[0][:10]
        except:
            near_by_events = []

        all_news = News.objects.all().order_by('-publish_date')[:10]

        date = datetime.now().day
        month = datetime.now().month
        artist_id = date * month
        artist_of_the_day = get_artist_random(artist_id)

        social_auth_facebook = request.user.social_auth.filter(provider='facebook')
        social_auth_google = request.user.social_auth.filter(provider='google-oauth2')

        wdjp_welcome_message_obj = None
        wdjp_welcome_text = None
        if WelcomeMessage.objects.filter(type= profile_obj.user_types).exists():
            wdjp_welcome_message_obj= WelcomeMessage.objects.filter(type= profile_obj.user_types).order_by('-created_date')[0]
            wdjp_welcome_text = wdjp_welcome_message_obj.welcome_text
        context.update({
            "djs_list": djs_list,
            "events_added": events_added[:6],
            "near_by_events": near_by_events,
            "all_news": all_news,
            "pro_fav_artist": pro_fav_artist,
            "artist_of_the_day": artist_of_the_day,
            "fav_artist_news_list": fav_artist_news_list,
            "week_name": datetime.today().strftime('%A'),
            "social_auth_facebook": social_auth_facebook,
            "social_auth_google": social_auth_google,
            "have_social_account": request.user.social_auth.all().count(),
            "profile_obj": profile_obj,
            "DEFAULT_DASHBOARD": DEFAULT_DASHBOARD,
            "user_type": profile_obj.user_types,
            "STATIC_URL": settings.STATIC_URL,
            "wdjp_welcome_text":wdjp_welcome_text
        })

        return render(request, "dashboard/dashboard.html", context)


limit = 20

@login_required
@check_email_validated
def dj_dashboard(request):

    profile_obj = Profile.objects.get(user=request.user)

    if profile_obj.user_types != "Artists/DJs":

        return redirect(reverse("dashboard"))

    context = {}
    fav_artist_news_list = []

    profile_obj = Profile.objects.prefetch_related().get(user=request.user)
    artist = profile_obj.dj_artist

    trackers = profile_obj.profiletracker_set.all()
    if len(trackers) > 0:
        context.update({
            "all_trackers": trackers,
        })

    agent_artists_facebook_stats = {}
    for shortlisted_artist in profile_obj.all_managed_artists.all():
        agent_artists_facebook_stats.update(
            get_artist_fb_likes(shortlisted_artist.artist)
        )

    color_code = get_color_hashes(agent_artists_facebook_stats)

    agent_artists_news = News.objects.filter(
        artists__in=profile_obj.shortlist_artist.all(),
    ).order_by('-publish_date').distinct()[:12]

    # Fav Venues
    if profile_obj.favorite_venues.all().exists():
        favorite_venues = profile_obj.favorite_venues.all()[:3]
    else:
        favorite_venues = []

    # Managed Artist News
    managed_artists_news_list = []
    pro_man_artists = profile_obj.profile_managed_djs.all().order_by('?')
    for man_art in pro_man_artists:
        man_news = {}
        if len(man_news.keys()) < 6:
            man_artist_news = News.objects.filter(artists=man_art).order_by('-publish_date').distinct()[:5]
            if man_artist_news:
                man_news["artist"] = man_art
                man_news["news"] = man_artist_news
                managed_artists_news_list.append(man_news)

    context.update({
        "agent_artists_facebook_stats": agent_artists_facebook_stats,
        "agent_artists_news": agent_artists_news,
        "color_code": color_code,
        "favorite_venues": favorite_venues,
        "managed_artists_news_list":managed_artists_news_list
    })

    # Fav Artist News
    pro_fav_artist = profile_obj.favorite_artist.all().order_by('?')
    for artist in pro_fav_artist:
        fav_news = {}
        if len(fav_news.keys()) < 6:
            artist_news = News.objects.filter(artists=artist).order_by('-publish_date').distinct()[:5]
            if artist_news:
                fav_news["artist"] = artist
                fav_news["news"] = artist_news
                fav_artist_news_list.append(fav_news)

    # Get Near By Events
    events_added = WpMyEvents.objects.filter(user_id=request.user.id).order_by("-event_date")

    djs_list = []

    for event in events_added:
        for artist in event.shortlisted_artists.all():
            djs_list.append(artist)
    try:
        near_by_events = get_user_near_event(request)[0][:10]
    except:
        near_by_events = []

    all_news = News.objects.all().order_by('-publish_date')[:10]

    date = datetime.now().day
    month = datetime.now().month
    artist_id = date * month
    artist_of_the_day = get_artist_random(artist_id)

    social_auth_facebook = request.user.social_auth.filter(provider='facebook')
    social_auth_google = request.user.social_auth.filter(provider='google-oauth2')

    wdjp_welcome_message_obj = None
    wdjp_welcome_text = None
    if WelcomeMessage.objects.filter(type= profile_obj.user_types).exists():
        wdjp_welcome_message_obj= WelcomeMessage.objects.filter(type= profile_obj.user_types).order_by('-created_date')[0]
        wdjp_welcome_text = wdjp_welcome_message_obj.welcome_text

    context.update({
            "djs_list": djs_list,
            "events_added": events_added[:6],
            "near_by_events": near_by_events,
            "all_news": all_news,
            "pro_fav_artist": pro_fav_artist,
            "artist_of_the_day": artist_of_the_day,
            "fav_artist_news_list": fav_artist_news_list,
            "week_name": datetime.today().strftime('%A'),
            "social_auth_facebook": social_auth_facebook,
            "social_auth_google": social_auth_google,
            "have_social_account": request.user.social_auth.all().count(),
            "profile_obj": profile_obj,
            "DEFAULT_DASHBOARD": DEFAULT_DASHBOARD,
            "user_type": profile_obj.user_types,
            "STATIC_URL": settings.STATIC_URL,
            "wdjp_welcome_text":wdjp_welcome_text
        })


    if artist:
        social_urls = get_artist_social_url(artist)
        statistics = get_artist_statistics(artist)

        upcoming_events = get_upcoming_events(artist)
        past_events = get_past_events(artist)

        latest_news = get_artist_news(artist)
        artist_top_ten_country = get_user_top_country_an_likes(artist)

        # Facebook Stats
        dj_similar_artists_facebook_stats = {}
        similar_artists = get_similar_artist(artist, limit=5)

        for shortlisted_artist in similar_artists:
            dj_similar_artists_facebook_stats.update(
                get_artist_fb_likes(shortlisted_artist)
            )
        dj_similar_artists_facebook_stats.update(get_artist_fb_likes(artist))
        color_code = get_color_hashes(dj_similar_artists_facebook_stats)

        context.update({
            "artist": artist,
            "social_urls": social_urls,
            "statistics": statistics,
            "upcoming_events": upcoming_events,
            "past_events": past_events,
            "latest_news": latest_news,
            "artist_top_ten_country": artist_top_ten_country,
            "dj_similar_artists_facebook_stats": dj_similar_artists_facebook_stats,
            "color_code": color_code,
            "similar_artists": similar_artists,
            "roster_dj_name":profile_obj.profile_managed_djs.all().order_by('artistName')

        })

    # Fav Venues
    if profile_obj.favorite_venues.all().exists():
        favorite_venues = profile_obj.favorite_venues.all()[:3]
    else:
        favorite_venues = []

    fav_artist_news_list = []

    pro_fav_artist = profile_obj.favorite_artist.all().order_by('?')
    for artist in pro_fav_artist:
        fav_news = {}
        if len(fav_news.keys()) < 6:
            artist_news = News.objects.filter(artists=artist).order_by('-publish_date').distinct()[:5]
            if artist_news:
                fav_news["artist"] = artist
                fav_news["news"] = artist_news
                fav_artist_news_list.append(fav_news)


    all_news = News.objects.all().order_by('-publish_date')[:10]

    context.update({
        "start": limit,
        "STATIC_URL": settings.STATIC_URL,
        "profile_obj": profile_obj,
        "favorite_venues": favorite_venues,
        "fav_artist_news_list": fav_artist_news_list,
        "all_news":all_news
    })

    return render(request, "dashboard/dj_dashboard.html", context)


@login_required
def create_welcome_message(request):
    if not request.user.is_superuser:
        return redirect("/")

    context = {}
    form = None

    if request.method != "POST":
        profile_obj = Profile.objects.get(user=request.user)
        if WelcomeMessage.objects.filter(type=profile_obj.user_types).exists():
            welcome_message_obj = WelcomeMessage.objects.filter(type=profile_obj.user_types).order_by('-created_date')[0]
            data = {"welcome_text":welcome_message_obj.welcome_text,"type":profile_obj.user_types}
            form = WdjpWelcomeMessageForm(initial=data)
        else:
            form = WdjpWelcomeMessageForm()
    else:
        form = WdjpWelcomeMessageForm(request.POST)

    context.update({
        "form": form
    })

    if request.method == "POST":
        if form.is_valid():
            WelcomeMessage.objects.create(
                welcome_text = form.cleaned_data["welcome_text"],
                type = form.cleaned_data["type"]
            )
            return redirect("dashboard")

    return render(request, "dashboard/create_welcome_message.html", context)

def user_events_json(request):
    """
    Get events
    """
    
    calendar_date = request.GET.get("calendar_date")
    calendar_list = calendar_date.split("-") 
    calendar_year = int(calendar_list[0])
    calendar_month = int(calendar_list[-1])
    user_events = None
    events_dict={}
    user_events = WpMyEvents.objects.filter(user_id=request.user.id,event_date__year=calendar_year,event_date__month=calendar_month)
    for event in user_events:
        event_date = event.event_date.strftime("%Y-%m-%d")
        event_type = None
        value = {
            "event_name":event.event_name,
            "event_id":event.event_id,
            "event_type":"system_event"
        }
        if event_date in events_dict:
            events_dict[event_date].append(value)
        else:
            events_dict[event_date]=[value]
        
    return HttpResponse(json.dumps(events_dict), content_type="application/json")