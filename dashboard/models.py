from __future__ import unicode_literals

from django.db import models

from ckeditor.fields import RichTextField

from .constants import USER_TYPES

# Create your models here.

class WelcomeMessage(models.Model):

    welcome_text = RichTextField(verbose_name='welcome_text', null=True, blank=True)
    type = models.CharField(max_length=100, blank=True, null=True, choices=USER_TYPES)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)


