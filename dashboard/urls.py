from django.conf.urls import url


from dashboard import views

urlpatterns = [
    url(r'^$', views.dashboard, name="dashboard"),
    url(r'^dj/$', views.dj_dashboard, name="dj_dashboard"),
    url(r'^create-welcome-message/$', views.create_welcome_message, name="create_welcome_message"),

    # URL for events
    url(r'^user-events-json/', views.user_events_json, name="user_events_json"),
   ]
