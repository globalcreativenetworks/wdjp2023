# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2018-12-28 09:51
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('social_data', '0022_auto_20181220_1752'),
    ]

    operations = [
        migrations.AlterField(
            model_name='artisttop',
            name='created',
            field=models.DateField(auto_now_add=True, null=True),
        ),
    ]
