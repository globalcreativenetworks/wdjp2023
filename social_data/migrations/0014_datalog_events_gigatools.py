# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2018-08-28 13:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('social_data', '0013_auto_20180828_1127'),
    ]

    operations = [
        migrations.AddField(
            model_name='datalog',
            name='events_gigatools',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
