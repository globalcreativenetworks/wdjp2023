import requests

from django.db.models import Max

from social_data.models import FacebookLike


def _get_url_for_response(url):
    response = requests.get(url)
    return response


def get_user_top_country_an_likes(artist):

    facebook_likes = FacebookLike.objects.filter(
        artist=artist,
    ).values('country_name', 'country_code').annotate(likes=Max('like_count')).order_by('-likes')[:10]

    for likes in facebook_likes:
        likes['country_code'] = likes['country_code'].lower()

    return facebook_likes
