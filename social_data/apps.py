from __future__ import unicode_literals

from django.apps import AppConfig


class SocialDataConfig(AppConfig):
    name = 'social_data'
