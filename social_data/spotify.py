import os
import requests

from django.conf import settings
from datetime import datetime, date
from django.utils import timezone

from report.models import ReportSpotify
from report.utils import *
from social_data.utils import _get_url_for_response
from artist.models import Source, Artist, ArtistWebsite, DjSimilarArtists
from artist.utils import find_SpotifyId, similar_artists, popular_tracks
from social_data.models import SpotifyData, SpotifyArtistImages, SpotifyArtistGenre, SpotifyArtistTopTracks, DataLog

from datetime import date

class SpotifyAPI(object):
    today_date = date.today()


    def __init__(self, username):
        self.token_url = "https://accounts.spotify.com/api/token"
        self.statistics_url = "https://api.spotify.com/v1/artists/"
        self.client_id = settings.SPOTIFY_CONSUMER_KEY
        self.client_secret = settings.SPOTIFY_CONSUMER_SECRET
        self.access_token = self.get_access_token()

        if len(username.split("?")) > 1:
            username_list = username.split("?")
            username = username_list[0]

        if len(username.split("/")) > 1:
            self.username = username.split("/")[-1]
        else:
            self.username = username

    def get_access_token(self):
        body_params = {'grant_type' : 'client_credentials'}
        response=requests.post(self.token_url, data=body_params, auth = (self.client_id, self.client_secret))
        return response.json()['access_token']

    def get_statistics(self):
        url = self.statistics_url + self.username
        headers = {"Authorization": "Bearer %s" % self.access_token}
        response = requests.get(url, headers=headers)
        return response.json()

    def create_instance(self, artist, data):
        """
        This method is use to create SpotifyData instance of a artist.
        """

        if data.has_key('error'):
            instance = None
        else:
            instance = SpotifyData.objects.create(artist=artist)
            if data.has_key('followers') and data['followers'].has_key('total'):
                instance.followersCount = data['followers']['total']
                artist.spotify_fancount = data['followers']['total']
                genre = data["genres"]

                for genre_name in genre:
                    genre_obj, created = SpotifyArtistGenre.objects.get_or_create(genre_name=genre_name)
                    instance.genre.add(genre_obj)
                i=0

                artist.save()
            if data.has_key('popularity'):
                instance.popularityIndex = data['popularity']
            instance.save()
        return instance




    def create_photos_instance(self,artist,data):
        """
        This method is use to create SpotifyData instance of a artist.
        """

        if data.has_key('error'):
            instance = None
        else:
            instance = SpotifyData.objects.create(artist=artist)
            if data.has_key('followers') and data['followers'].has_key('total'):
                instance.followersCount = data['followers']['total']
                artist.spotify_fancount = data['followers']['total']
                genre = data["genres"]

                for genre_name in genre:
                    genre_obj, created = SpotifyArtistGenre.objects.get_or_create(genre_name=genre_name)
                    instance.genre.add(genre_obj)
                i=0
                for image in data['images']:
                    try:

                        spotify_artist_image = SpotifyArtistImages.objects.create(spotify_data=instance)
                        image = image['url']
                        image = requests.get(image)
                        image_name = artist.artistName.replace(" ", "_").decode('unicode_escape').encode('ascii','ignore') +"_" + str(i) + ".jpg"

                        full_image_path = os.path.join(settings.MEDIA_ROOT, 'artistphotos', image_name)
                        artist_image_file = open(full_image_path, 'wb+')
                        artist_image_file.write(image.content)
                        artist_image_file.close()

                        artist_image_path = full_image_path.split("media/")[1]
                        spotify_artist_image.artistPhoto = artist_image_path

                        spotify_artist_image.save()

                        i += 1
                    except :
                        pass

                artist.save()
            if data.has_key('popularity'):
                instance.popularityIndex = data['popularity']
            instance.save()
        return instance


def scrape_artist_photo(artists=[]):
    today_date = date.today()
    count = 0
    for artist in artists:
        artist = Artist.objects.get(artistID=artist)

        source, created = Source.objects.get_or_create(sourceName="Spotify")
        artist_website = False
        try:
            artist_website = ArtistWebsite.objects.get(sourceID=source.sourceID, artistID=artist)
        except ArtistWebsite.DoesNotExist:
            artist_website = None
        except ArtistWebsite.MultipleObjectsReturned:
            artist_website = ArtistWebsite.objects.filter(sourceID=source.sourceID, artistID=artist)[0]

        if not artist_website:
            count += 1
        if artist_website:
            spotify_id = artist_website.url.split('/')[-1]
            instance = None
            try:
                artist_spotifyapi = SpotifyAPI(artist_website.url)
                data = artist_spotifyapi.get_statistics()

                instance = artist_spotifyapi.create_photos_instance(artist, data)
                if data and instance :
                    count += 1
                if instance is None:
                    raise Exception("Data was not scraped")

            except Exception as e:

                # continue if error
                continue
            #    if everything good write good report

        else:
            pass


def scrap_spotify_api(artists=[]):

    today_date = date.today()
    count = 0
    for artist in artists:
        artist = Artist.objects.get(artistID=artist)
        #
        # try :
        #     spotify_id = find_SpotifyId(artist.artistName)
        # except Exception as e:
        #     report = ReportSpotify(created=today_date,
        #                            artist=artist)
        #     report.title = "Artist: %s was not scrapped" % artist.artistName
        #     report.description = "Internal error occured during scrapping process : " + str(e.message)
        #     report.event = ERROR
        #     report.resolution = INTERNAL_SERVER_ERROR
        #     report.save()
        #     continue

        source, created = Source.objects.get_or_create(sourceName="Spotify")
        artist_website = False
        try:
            artist_website = ArtistWebsite.objects.get(sourceID=source.sourceID, artistID=artist)
        except ArtistWebsite.DoesNotExist:
            artist_website = None
        except ArtistWebsite.MultipleObjectsReturned:
            artist_website = ArtistWebsite.objects.filter(sourceID=source.sourceID, artistID=artist)[0]

        if not artist_website:
            count += 1
        if artist_website:
            spotify_id = artist_website.url.split('/')[-1]
            instance = None
            try:
                artist_spotifyapi = SpotifyAPI(artist_website.url)
                data = artist_spotifyapi.get_statistics()

                instance = artist_spotifyapi.create_instance(artist, data)
                if data and instance :
                    count += 1
                if instance is None:
                    raise Exception("Data was not scraped")

            except Exception as e:
                report = ReportSpotify(created=today_date,
                                       artist=artist)
                report.title = "Artist: %s was not scrapped" % artist.artistName
                report.description = "Internal error occured during scrapping process: " + str(e.message)+'\n' + "SpotifyUrl: " + artist_website.url
                report.event = ERROR
                report.resolution = INTERNAL_SERVER_ERROR
                report.save()
                # continue if error
                continue
            #    if everything good write good report
            report = ReportSpotify(created=today_date,
                                   artist=artist)
            report.title = "Artist: %s was scrapped" % artist.artistName
            report.description = "FanCount " + str(instance.followersCount) + " was scrapped from url: "+ artist_website.url
            report.event = INFO
            report.resolution = NA
            report.save()
            try:
                tracks = popular_tracks(spotify_id)
                for track in tracks:
                    social_data = SpotifyArtistTopTracks.objects.get_or_create(spotify_data=instance, track_name=track)
            except Exception as e:
                report = ReportSpotify(created=today_date,
                                       artist=artist)
                report.title = "Track for artist: %s was not scrapped" % artist.artistName
                report.description = "Internal error occured during scrapping process: " + str(e.message)
                report.event = WARNING
                report.resolution = CKECK_URL
                report.save()
                pass
        else:
            report = ReportSpotify(created=today_date,
                                   artist=artist)
            report.title = "Artist: %s was not scrapped" % artist.artistName
            report.description = "Artist does not have an URL"
            report.event = UNKNOWN
            report.resolution = CKECK_URL
            report.save()

            count += 1

    spotify_log, created = DataLog.objects.get_or_create(created=today_date)
    if spotify_log.spotify_count:
        spotify_count = int(spotify_log.spotify_count)
        spotify_count += count
        spotify_log.spotify_count = spotify_count
    else:
        spotify_log.spotify_count = count
    spotify_log.save()
