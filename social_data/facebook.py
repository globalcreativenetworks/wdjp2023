import os
import urllib
import requests
import time
from unidecode import unidecode
import datetime
from datetime import date

from django.conf import settings
from social_data.utils import _get_url_for_response
from social_data.models import FacebookData, DataLog
from django.conf import settings
from artist.models import Source, Artist, ArtistWebsite
from requests.exceptions import ConnectionError


class FacebookAPI(object):

    def __init__(self, page_id=None):
        self.base_url = "https://graph.facebook.com/v2.8/"
        self.token_url = "https://graph.facebook.com/oauth/access_token?"
        self.client_id = settings.FACEBOOK_API_KEY
        self.client_secret = settings.FACEBOOK_API_SECRET
        self.access_token = self.get_access_token()

        if page_id:
            self.page_id = page_id.split("?")[0]

    def set_pageid_from_artist(self, artistID):
        try:
            artist = Artist.allartistobjects.get(artistID=artistID)
            source, created = Source.objects.get_or_create(sourceName="Facebook")
        except ArtistWebsite.DoesNotExist:
            artist_website = None
        try:
            artist_website = ArtistWebsite.objects.get(sourceID=source.sourceID, artistID=artist)
        except ArtistWebsite.DoesNotExist:
            artist_website = None
        except ArtistWebsite.MultipleObjectsReturned:
            artist_website = ArtistWebsite.objects.filter(sourceID=source.sourceID, artistID=artist)[0]
        if artist_website:
            self.page_id = artist_website.url.split("?")[0]
        else:
            self.page_id = None

    def get_access_token(self):
        """
        This function return the access token for facebook api.
        """
        oauth_args = {
            'grant_type': 'client_credentials',
            'client_id': self.client_id,
            'client_secret': self.client_secret
        }
        url = self.token_url + urllib.urlencode(oauth_args)
        response = _get_url_for_response(url)

        return response.json()

    def get_details(self):
        """
        This function return the details of page i.e
        id, name, category, website, about, checkins, description,
        link, likes, cover and events.
        """
        if not self.page_id:
            return {'error':'Page Url is not set for the artist'}
        url = self.base_url + self.page_id + "?access_token=" + self.access_token['access_token'] + "&fields=id,name,category,website,about,checkins,description,link,likes,cover,events,fan_count,booking_agent,press_contact,general_manager,talking_about_count,bio,picture.type(large)"
        print url
        response = _get_url_for_response(url)
        return response.json()

    def page_fans_country(self):
        if not self.page_id:
            return {'error':'Page Url is not set for the artist'}
        tznow = datetime.datetime.utcnow().strftime('%Y-%m-%d')
        page_id  = self.page_id.replace("https://www.facebook.com/","")
        if not page_id.endswith('/'):
            page_id = "%s/"%page_id
        url = self.base_url + page_id + "insights/page_fans_country/lifetime?since="+tznow+"&until="+tznow+"&access_token=" + self.access_token['access_token']
        for i in range(0,2):
            while True:
                try:
                    time.sleep(5)
                    response = _get_url_for_response(url)
                except ConnectionError as e:
                    print "Retry",e
                    continue
                return response.json()
        return {'error':'Page Url is not set for the artist'}


def scrap_facebook_data(data, artists=[]):
    
    today_date = date.today()
    count = 0
    for artist in artists:
        artist = Artist.allartistobjects.get(artistID=artist)
        print "Scrapping Facebook Data for " + artist.artistName
        source, created = Source.objects.get_or_create(sourceName="Facebook")

        try:
            artist_website = ArtistWebsite.objects.get(sourceID=source.sourceID, artistID=artist)
        except ArtistWebsite.DoesNotExist:
            artist_website = None
        except ArtistWebsite.MultipleObjectsReturned:
            artist_website = ArtistWebsite.objects.filter(sourceID=source.sourcceID, artistID=artist)[0]

        if artist_website:
            if data.has_key('error'):
                raise Exception("Page url is wrong for %s artist" % artist.artistName)
                return
            else:
                instance = FacebookData.objects.create(artist=artist)
                if data.get('fan_count'):
                    instance.fanCount = data['fan_count']
                    artist.facebook_fancount = data.get('fan_count', '')
                    artist.save()

                booking_agent = data.get('booking_agent', '')
                if booking_agent:
                    artist.artistAgent = booking_agent
                    artist.save()

                press_contact = data.get('press_contact', '')
                if press_contact:
                    instance.press_contact = press_contact
                    artist.press_contact = press_contact

                general_manager = data.get('general_manager', '')
                if general_manager:
                    general_manager_list = general_manager.split("\n")
                    general_managers = ",".join(general_manager_list)
                    instance.general_manager = general_managers
                    artist.general_manager = general_managers

                instance.talkingAboutCount = data['talking_about_count']
                if data.has_key('bio'):
                    instance.bio = unidecode(data['bio'])
                instance.save()
                if data.has_key('picture'):
                    image_url = data['picture']['data']['url']
                    response = requests.get(image_url)
                    image_name = artist.artistName.replace(" ", "_").decode('unicode_escape').encode('ascii','ignore') + ".jpg"

                    full_image_path = os.path.join(settings.MEDIA_ROOT, 'artistphotos', image_name)
                    artist_image_file = open(full_image_path, 'wb+')
                    artist_image_file.write(response.content)
                    artist_image_file.close()

                    artist_image_path = full_image_path.split("media/")[1]
                    artist.artistPhoto = artist_image_path
                    artist.save()

                count = count+1

        else:
            count = count+1


    facebook_log, created = DataLog.objects.get_or_create(created=today_date)

    if facebook_log.facebook_count:
        facebook_count = int(facebook_log.facebook_count)
        facebook_count += count
        facebook_log.facebook_count = facebook_count
    else:
        facebook_log.facebook_count = count
    facebook_log.save()
