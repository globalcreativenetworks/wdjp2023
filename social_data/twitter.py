import json
import time
from datetime import date

import oauth2 as oauth
from django.conf import settings

from artist.models import Source, Artist, ArtistWebsite
from report.models import ReportTwitter
from report.utils import *
from social_data.models import TwitterData, DataLog


class TwitterAPI(object):


    def __init__(self, username):
        self.statistics_url = "https://api.twitter.com/1.1/users/show.json"
        self.timeline_url = "https://api.twitter.com/1.1/statuses/user_timeline.json"
        self.consumer = oauth.Consumer(key=settings.TWITTER_CONSUMER_KEY, secret=settings.TWITTER_CONSUMER_SECRET)
        self.access_token = oauth.Token(key=settings.TWITTER_ACCESS_KEY, secret=settings.TWITTER_ACCESS_SECRET)
        self.client = oauth.Client(self.consumer, self.access_token)

        if len(username.split("?")) > 1:
            username_list = username.split("?")
            username = username_list[0]

        if len(username.split("/")) > 1:
            self.username = username.split("/")[-1]
        else:
            self.username = username


    def get_statistics(self):
        """
        This function return all the user data i.e
        followers_count, friends_count, favourites_count,
        created_at, statuses_count(total_tweets) etc
        """
        url = self.statistics_url + "?screen_name=" + self.username
        response, data = self.client.request(url)
        return json.loads(data)


    def get_recent_tweets(self, count):
        """
        :param count: The no of recent tweets
        This function return a list of the recent tweets
        """
        url = self.timeline_url + "?screen_name=" + self.username + "&count=" + str(count)
        response, data = self.client.request(url)
        return json.loads(data)


    def create_instance(self, artist, data):
        """
        This method is use to create or update the TwitterData instance on a artist.
        """

        if not data or data.has_key('errors') :
            pass
        else:
            instance = TwitterData.objects.create(artist=artist)
            instance.friendsCount = data['friends_count']
            instance.favouritesCount = data['favourites_count']
            instance.statusesCount = data['statuses_count']
            instance.listedCount = data['listed_count']
            if data.get("followers_count"):
                instance.followersCount = data['followers_count']
                artist.twitter_fancount = data.get("followers_count","")
                artist.save()
            if data['status'].has_key('retweet_count'):
                instance.retweetCount = data['status']['retweet_count']
            instance.createdAt = time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(data['created_at'],'%a %b %d %H:%M:%S +0000 %Y'))
            instance.save()

            return True


    def get_clean_data(self):
        data = {}
        statistics = self.get_statistics()
        if statistics.has_key('errors'):
            data['errors'] = statistics['errors']
        else:
            return statistics


def scrap_twitter_api(artists=[]):
    today_date = date.today()

    count = 0
    for artist in artists:
        artist = Artist.objects.get(artistID=artist)
        source, created = Source.objects.get_or_create(sourceName="Twitter")

        try:
            artist_website = ArtistWebsite.objects.get(sourceID=source.sourceID, artistID=artist)
        except ArtistWebsite.DoesNotExist:
            artist_website = None
        except ArtistWebsite.MultipleObjectsReturned:
            artist_website = ArtistWebsite.objects.filter(sourceID=source.sourceID, artistID=artist)[0]

        if artist_website:
            artist_twitterapi = TwitterAPI(artist_website.url)
            data = artist_twitterapi.get_clean_data()

            try:
                instance = artist_twitterapi.create_instance(artist, data)
                if data and instance :
                    count = count + 1
            except Exception as e:
                report = ReportTwitter(created=today_date,
                                       artist=artist)
                report.title = "Artist: %s was not scrapped" % artist.artistName
                report.description = "Internal error occured during scrapping process: " + str(e.message)+'\n' + "Url: " + artist_website.url
                report.event = ERROR
                report.resolution = INTERNAL_SERVER_ERROR
                report.save()
                continue
        else:
            report = ReportTwitter(created=today_date,
                                   artist=artist)
            report.title = "Artist: %s was not scrapped" % artist.artistName
            report.description = "Artist does not have an URL"
            report.event = UNKNOWN
            report.resolution = CKECK_URL
            report.save()
            continue
        report = ReportTwitter(created=today_date,
                               artist=artist)
        report.title = "Artist: %s was scrapped" % artist.artistName
        report.description = "Artist was scrapped from url: " + artist_website.url
        report.event = INFO
        report.resolution = NA
        report.save()

        # count = count + 1

    twitter_log, created = DataLog.objects.get_or_create(created=today_date)
    twitter_log.twitter_count = 10
    if twitter_log.twitter_count:
        twitter_count = int(twitter_log.instagram_count)
        twitter_count += count
        twitter_log.instagram_count = twitter_count
    else:
        twitter_log.instagram_count = count

    twitter_log.save()



# art_list = Artist.objects.all()
# twitter_api = TwitterAPI(art_list)
# scrap_twitter = twitter_api.scrap_twitter_api(art_list)
