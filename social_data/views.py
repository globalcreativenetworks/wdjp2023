from datetime import date, timedelta
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from social_data.models import DataLog
from accounts.decorators import check_email_validated

@login_required
@check_email_validated
def diagnostic_report(request):

    if not request.user.is_superuser:
        return redirect("/")

    context = {}

    today_date = date.today()
    date_for_day = None
    data_log_today = None

    # try:
    #     data_log_today = DataLog.objects.get(created=today_date)
    # except FirstException :
    #     date_for_day = today_date - timedelta(days=1)
    #     data_log_today = DataLog.objects.get(created=date_for_day)
    # except SecondException:
    #     date_for_day = today_date - timedelta(days=2)
    #     data_log_today = DataLog.objects.get(created=date_for_day)
    # except ThirdException:
    #     date_for_day = today_date - timedelta(days=3)
    #     data_log_today = DataLog.objects.get(created=date_for_day)
    # except FourthException:
    #     date_for_day = today_date - timedelta(days=4)
    #     data_log_today = DataLog.objects.get(created=date_for_day)

    if not data_log_today:
        try:
            data_log_today = DataLog.objects.get(created=today_date)
        except:
            pass


    if not data_log_today:
        try:
            date_for_day = today_date - timedelta(days=1)
            data_log_today = DataLog.objects.get(created=date_for_day)
        except:
            pass


    if not data_log_today:
        try:
            date_for_day = today_date - timedelta(days=2)
            data_log_today = DataLog.objects.get(created=date_for_day)
        except:
            pass


    if not data_log_today:
        try:
            date_for_day = today_date - timedelta(days=3)
            data_log_today = DataLog.objects.get(created=date_for_day)
        except:
            pass


    if not data_log_today:
        try:
            date_for_day = today_date - timedelta(days=4)
            data_log_today = DataLog.objects.get(created=date_for_day)
        except:
            pass


    if not data_log_today:
        try:
            date_for_day = today_date - timedelta(days=5)
            data_log_today = DataLog.objects.get(created=date_for_day)
        except:
            pass


    if not data_log_today:
        try:
            date_for_day = today_date - timedelta(days=6)
            data_log_today = DataLog.objects.get(created=date_for_day)
        except:
            pass


    if date_for_day:
        date_before_week = date_for_day - timedelta(days=7)
        date_before_month = date_for_day - timedelta(days=30)
        date_before_six_month = date_for_day - timedelta(days=180)
        context.update({
            "current_date": date_for_day,
            "date_before_week": date_before_week,
            "date_before_month": date_before_month,
            "date_before_six_month": date_before_six_month

        })
    else:
        date_before_week = today_date - timedelta(days=7)
        date_before_month = today_date - timedelta(days=30)
        date_before_six_month = today_date - timedelta(days=180)
        context.update({
            "current_date": today_date,
            "date_before_week": date_before_week,
            "date_before_month": date_before_month,
            "date_before_six_month": date_before_six_month
        })


    data_log_before_week = DataLog.objects.filter(created=date_before_week).first()
    data_log_before_month = DataLog.objects.filter(created=date_before_month).first()
    data_log_before_six_month = DataLog.objects.filter(created=date_before_six_month).first()

    context.update({
        "data_log_today": data_log_today,
        "data_log_before_week": data_log_before_week,
        "data_log_before_month": data_log_before_month,
        "data_log_before_six_month": data_log_before_six_month
    })


    return render(request, "social_data/diagnostic_report.html", context)


    #
    # if data_log_today.active_artist_count and data_log_before_week.active_artist_count:
    #     change_in_active_artist_in_last_week =  ((int(data_log_today.active_artist_count) - int(data_log_before_week.active_artist_count))/int(data_log_today.active_artist_count))*100
    #
    # if data_log_today.active_artist_count and data_log_before_month.active_artist_count:
    #     change_in_active_artist_in_last_month =  ((int(data_log_today.active_artist_count) - int(data_log_before_month.active_artist_count))/int(data_log_today.active_artist_count))*100
    #
    # if data_log_today.active_artist_count and data_log_before_six_month.active_artist_count:
    #     change_in_active_artist_in_six_month =  ((int(data_log_today.active_artist_count) - int(data_log_before_six_month.active_artist_count))/int(data_log_today.active_artist_count))*100
    #
    #
    # if data_log_today.inactive_artist_count and data_log_before_week.inactive_artist_count:
    #     change_in_inactive_artist_in_last_week =  ((int(data_log_today.inactive_artist_count) - int(data_log_before_week.inactive_artist_count))/int(data_log_today.inactive_artist_count))*100
    #
    # if data_log_today.inactive_artist_count and data_log_before_month.inactive_artist_count:
    #     change_in_inactive_artist_in_last_month =  ((int(data_log_today.inactive_artist_count) - int(data_log_before_month.inactive_artist_count))/int(data_log_today.inactive_artist_count))*100
    #
    # if data_log_today.inactive_artist_count and data_log_before_six_month.inactive_artist_count:
    #     change_in_inactive_artist_in_six_month =  ((int(data_log_today.inactive_artist_count) - int(data_log_before_six_month.inactive_artist_count))/int(data_log_today.inactive_artist_count))*100
    #
    #
    # if data_log_today.inactive_artist_from_dbbuilder and data_log_before_week.inactive_artist_from_dbbuilder:
    #     change_in_inactive_artist_from_dbbuilder_in_last_week =  ((int(data_log_today.inactive_artist_from_dbbuilder) - int(data_log_before_week.inactive_artist_from_dbbuilder))/int(data_log_today.inactive_artist_from_dbbuilder))*100
    #
    # if data_log_today.inactive_artist_count and data_log_before_month.inactive_artist_count:
    #     change_in_active_artist_in_last_month =  ((int(data_log_today.active_artist_count) - int(data_log_before_month.active_artist_count))/int(data_log_today.active_artist_count))*100
    #
    # if data_log_today.inactive_artist_count and data_log_before_six_month.inactive_artist_count:
    #     change_in_active_artist_in_six_month =  ((int(data_log_today.active_artist_count) - int(data_log_before_six_month.active_artist_count))/int(data_log_today.active_artist_count))*100
    #
    #
    # if data_log_today.artist_with_gps_cord_count and data_log_before_week.artist_with_gps_cord_count:
    #     change_in_artist_with_gps_cord_count_in_last_week =  ((int(data_log_today.artist_with_gps_cord_count) - int(data_log_before_week.artist_with_gps_cord_count))/int(data_log_today.artist_with_gps_cord_count))*100
    #
    # if data_log_today.artist_with_gps_cord_count and data_log_before_month.artist_with_gps_cord_count:
    #     change_in_artist_with_gps_cord_count_count_in_last_month =  ((int(data_log_today.artist_with_gps_cord_count) - int(data_log_before_month.artist_with_gps_cord_count))/int(data_log_today.artist_with_gps_cord_count))*100
    #
    # if data_log_today.inactive_artist_count and data_log_before_six_month.inactive_artist_count:
    #     change_in_artist_with_gps_cord_count_in_six_month =  ((int(data_log_today.artist_with_gps_cord_count) - int(data_log_before_six_month.artist_with_gps_cord_count))/int(data_log_today.artist_with_gps_cord_count))*100
    #
    #
    # if data_log_today.artist_aggregated and data_log_before_week.artist_aggregated:
    #     change_in_artist_aggregated_count_in_last_week =  ((int(data_log_today.artist_aggregated) - int(data_log_before_week.artist_aggregated))/int(data_log_today.artist_aggregated))*100
    #
    # if data_log_today.artist_aggregated and data_log_before_month.artist_aggregated:
    #     change_in_artist_aggregated_count_in_last_month =  ((int(data_log_today.artist_aggregated) - int(data_log_before_month.artist_aggregated))/int(data_log_today.artist_aggregated))*100
    #
    # if data_log_today.inactive_artist_count and data_log_before_six_month.inactive_artist_count:
    #     change_in_artist_aggregated_count_in_six_month =  ((int(data_log_today.artist_aggregated) - int(data_log_before_six_month.artist_aggregated))/int(data_log_today.artist_aggregated))*100
    #
    #
    # if data_log_today.events_aggregated_overall and data_log_before_week.events_aggregated_overall:
    #     change_in_events_aggregated_overall_count_in_last_week =  ((int(data_log_today.events_aggregated_overall) - int(data_log_before_week.events_aggregated_overall))/int(data_log_today.events_aggregated_overall))*100
    #
    # if data_log_today.events_aggregated_overall and data_log_before_month.events_aggregated_overall:
    #     change_in_events_aggregated_overall_count_in_last_month =  ((int(data_log_today.events_aggregated_overall) - int(data_log_before_month.events_aggregated_overall))/int(data_log_today.events_aggregated_overall))*100
    #
    # if data_log_today.events_aggregated_overall and data_log_before_six_month.events_aggregated_overall:
    #     change_in_events_aggregated_overall_count_in_six_month =  ((int(data_log_today.events_aggregated_overall) - int(data_log_before_six_month.events_aggregated_overall))/int(data_log_today.events_aggregated_overall))*100
    #
    #
    # if data_log_today.events_resident_advisor and data_log_before_week.events_resident_advisor:
    #     change_in_events_resident_advisor_count_in_last_week =  ((int(data_log_today.events_resident_advisor) - int(data_log_before_week.events_resident_advisor))/int(data_log_today.events_resident_advisor))*100
    #
    # if data_log_today.events_resident_advisor and data_log_before_month.events_resident_advisor:
    #     change_in_events_resident_advisor_count_in_last_month =  ((int(data_log_today.events_resident_advisor) - int(data_log_before_month.events_resident_advisor))/int(data_log_today.events_resident_advisor))*100
    #
    # if data_log_today.inactive_artist_count and data_log_before_six_month.inactive_artist_count:
    #     change_in_events_resident_advisor_count_in_six_month =  ((int(data_log_today.events_resident_advisor) - int(data_log_before_six_month.events_resident_advisor))/int(data_log_today.events_resident_advisor))*100
    #
    #
    # if data_log_today.events_bandsin_town and data_log_before_week.events_bandsin_town:
    #     change_in_events_bandsin_town_count_in_last_week =  ((int(data_log_today.events_bandsin_town) - int(data_log_before_week.events_bandsin_town))/int(data_log_today.events_bandsin_town))*100
    #
    # if data_log_today.events_bandsin_town and data_log_before_month.events_bandsin_town:
    #     change_in_events_bandsin_town_count_in_last_month =  ((int(data_log_today.events_bandsin_town) - int(data_log_before_month.events_bandsin_town))/int(data_log_today.events_bandsin_town))*100
    #
    # if data_log_today.events_bandsin_town and data_log_before_six_month.events_bandsin_town:
    #     change_in_events_bandsin_town_count_in_six_month =  ((int(data_log_today.events_resident_advisor) - int(data_log_before_six_month.events_resident_advisor))/int(data_log_today.events_resident_advisor))*100
    #
    #
    # if data_log_today.events_bandsin_town and data_log_before_week.events_bandsin_town:
    #     change_in_events_bandsin_town_count_in_last_week =  ((int(data_log_today.events_bandsin_town) - int(data_log_before_week.events_bandsin_town))/int(data_log_today.events_bandsin_town))*100
    #
    # if data_log_today.artist_with_gps_cord_count and data_log_before_month.artist_with_gps_cord_count:
    #     change_in_events_bandsin_town_count_in_last_month =  ((int(data_log_today.events_bandsin_town) - int(data_log_before_month.events_bandsin_town))/int(data_log_today.events_bandsin_town))*100
    #
    # if data_log_today.inactive_artist_count and data_log_before_six_month.inactive_artist_count:
    #     change_in_events_bandsin_town_count_in_six_month =  ((int(data_log_today.events_bandsin_town) - int(data_log_before_six_month.events_bandsin_town))/int(data_log_today.events_bandsin_town))*100
    #
    #
    # if data_log_today.events_song_kick and data_log_before_week.events_song_kick:
    #     change_in_events_song_kick_count_in_last_week =  ((int(data_log_today.events_song_kick) - int(data_log_before_week.events_song_kick))/int(data_log_today.events_song_kick))*100
    #
    # if data_log_today.events_song_kick and data_log_before_month.events_song_kick:
    #     change_in_events_song_kick_count_in_last_month =  ((int(data_log_today.events_song_kick) - int(data_log_before_month.events_song_kick))/int(data_log_today.events_song_kick))*100
    #
    # if data_log_today.events_song_kick and data_log_before_six_month.events_song_kick:
    #     change_in_events_song_kick_count_in_six_month =  ((int(data_log_today.events_song_kick) - int(data_log_before_six_month.events_song_kick))/int(data_log_today.events_song_kick))*100
    #
    #
    # if data_log_today.events_gigatools and data_log_before_week.events_gigatools:
    #     change_in_events_gigatools_count_in_last_week =  ((int(data_log_today.events_gigatools) - int(data_log_before_week.events_gigatools))/int(data_log_today.events_gigatools))*100
    #
    # if data_log_today.artist_with_gps_cord_count and data_log_before_month.artist_with_gps_cord_count:
    #     change_in_events_gigatools_count_in_last_month =  ((int(data_log_today.events_gigatools) - int(data_log_before_month.events_gigatools))/int(data_log_today.events_gigatools))*100
    #
    # if data_log_today.inactive_artist_count and data_log_before_six_month.inactive_artist_count:
    #     change_in_events_gigatools_count_in_six_month =  ((int(data_log_today.events_gigatools) - int(data_log_before_six_month.events_gigatools))/int(data_log_today.events_gigatools))*100
    #
    #
    # if data_log_today.total_news and data_log_before_week.total_news:
    #     change_in_total_news_count_in_last_week =  ((int(data_log_today.total_news) - int(data_log_before_week.total_news))/int(data_log_today.total_news))*100
    #
    # if data_log_today.total_news and data_log_before_month.total_news:
    #     change_in_total_news_count_in_last_month =  ((int(data_log_today.total_news) - int(data_log_before_month.total_news))/int(data_log_today.total_news))*100
    #
    # if data_log_today.total_news and data_log_before_six_month.total_news:
    #     change_in_total_news_count_in_six_month =  ((int(data_log_today.total_news) - int(data_log_before_six_month.total_news))/int(data_log_today.total_news))*100
    #
    #
    # if data_log_today.news_change_underground and data_log_before_week.news_change_underground:
    #     change_in_news_change_underground_count_in_last_week =  ((int(data_log_today.news_change_underground) - int(data_log_before_week.news_change_underground))/int(data_log_today.news_change_underground))*100
    #
    # if data_log_today.news_change_underground and data_log_before_month.news_change_underground:
    #     change_in_news_change_underground_count_in_last_month =  ((int(data_log_today.news_change_underground) - int(data_log_before_month.news_change_underground))/int(data_log_today.news_change_underground))*100
    #
    # if data_log_today.total_news and data_log_before_six_month.total_news:
    #     change_in_news_change_underground_count_in_six_month =  ((int(data_log_today.news_change_underground) - int(data_log_before_six_month.news_change_underground))/int(data_log_today.news_change_underground))*100
    #
    #
    # if data_log_today.news_billboard and data_log_before_week.news_billboard:
    #     change_in_news_billboard_count_in_last_week =  ((int(data_log_today.news_billboard) - int(data_log_before_week.news_billboard))/int(data_log_today.news_billboard))*100
    #
    # if data_log_today.news_billboard and data_log_before_month.news_billboard:
    #     change_in_news_billboard_count_in_last_month =  ((int(data_log_today.news_billboard) - int(data_log_before_month.news_billboard))/int(data_log_today.news_billboard))*100
    #
    # if data_log_today.news_billboard and data_log_before_six_month.news_billboard:
    #     change_in_news_billboard_count_in_six_month =  ((int(data_log_today.news_billboard) - int(data_log_before_six_month.news_billboard))/int(data_log_today.news_billboard))*100
    #
    #
    # if data_log_today.news_dancing_astronaut and data_log_before_week.news_dancing_astronaut:
    #     change_in_news_dancing_astronaut_count_in_last_week =  ((int(data_log_today.news_dancing_astronaut) - int(data_log_before_week.news_dancing_astronaut))/int(data_log_today.news_dancing_astronaut))*100
    #
    # if data_log_today.news_dancing_astronaut and data_log_before_month.news_dancing_astronaut:
    #     change_in_news_dancing_astronaut_count_in_last_month =  ((int(data_log_today.news_dancing_astronaut) - int(data_log_before_month.news_dancing_astronaut))/int(data_log_today.news_dancing_astronaut))*100
    #
    # if data_log_today.news_dancing_astronaut and data_log_before_six_month.news_dancing_astronaut:
    #     change_in_news_dancing_astronaut_count_in_six_month =  ((int(data_log_today.news_dancing_astronaut) - int(data_log_before_six_month.news_dancing_astronaut))/int(data_log_today.news_dancing_astronaut))*100
    #
    #
    # if data_log_today.news_datatransmission and data_log_before_week.news_datatransmission:
    #     change_in_news_datatransmission_count_in_last_week =  ((int(data_log_today.news_datatransmission) - int(data_log_before_week.news_datatransmission))/int(data_log_today.news_datatransmission))*100
    #
    # if data_log_today.news_datatransmission and data_log_before_month.news_datatransmission:
    #     change_in_news_datatransmission_count_in_last_month =  ((int(data_log_today.news_datatransmission) - int(data_log_before_month.news_datatransmission))/int(data_log_today.news_datatransmission))*100
    #
    # if data_log_today.news_datatransmission and data_log_before_six_month.news_datatransmission:
    #     change_in_news_datatransmission_count_in_six_month =  ((int(data_log_today.news_datatransmission) - int(data_log_before_six_month.news_datatransmission))/int(data_log_today.news_datatransmission))*100
    #
    #
    # if data_log_today.news_djmag and data_log_before_week.news_djmag:
    #     change_in_news_djmag_count_in_last_week =  ((int(data_log_today.news_djmag) - int(data_log_before_week.news_djmag))/int(data_log_today.news_djmag))*100
    #
    # if data_log_today.news_djmag and data_log_before_month.news_djmag:
    #     change_in_news_djmag_count_in_last_month =  ((int(data_log_today.news_djmag) - int(data_log_before_month.news_djmag))/int(data_log_today.news_djmag))*100
    #
    # if data_log_today.news_djmag and data_log_before_six_month.news_djmag:
    #     change_in_news_djmag_count_in_six_month =  ((int(data_log_today.news_djmag) - int(data_log_before_six_month.news_djmag))/int(data_log_today.news_djmag))*100
    #
    #
    # if data_log_today.news_djtimes and data_log_before_week.news_djtimes:
    #     change_in_news_djtimes_count_in_last_week =  ((int(data_log_today.news_djtimes) - int(data_log_before_week.news_djtimes))/int(data_log_today.news_djtimes))*100
    #
    # if data_log_today.news_djtimes and data_log_before_month.news_djtimes:
    #     change_in_news_djtimes_count_in_last_month =  ((int(data_log_today.news_djtimes) - int(data_log_before_month.news_djtimes))/int(data_log_today.news_djtimes))*100
    #
    # if data_log_today.news_djtimes and data_log_before_six_month.news_djtimes:
    #     change_in_news_djtimes_count_in_six_month =  ((int(data_log_today.news_djtimes) - int(data_log_before_six_month.news_djtimes))/int(data_log_today.news_djtimes))*100
    #
    #
    # if data_log_today.news_fabriclondon and data_log_before_week.news_fabriclondon:
    #     change_in_news_fabriclondon_count_in_last_week =  ((int(data_log_today.news_fabriclondon) - int(data_log_before_week.news_fabriclondon))/int(data_log_today.news_fabriclondon))*100
    #
    # if data_log_today.news_djtimes and data_log_before_month.news_djtimes:
    #     change_in_news_fabriclondon_count_in_last_month =  ((int(data_log_today.news_fabriclondon) - int(data_log_before_month.news_fabriclondon))/int(data_log_today.news_fabriclondon))*100
    #
    # if data_log_today.news_djtimes and data_log_before_six_month.news_djtimes:
    #     change_in_news_fabriclondon_count_in_six_month =  ((int(data_log_today.news_fabriclondon) - int(data_log_before_six_month.news_fabriclondon))/int(data_log_today.news_fabriclondon))*100
    #
    #
    # if data_log_today.news_hyperbot and data_log_before_week.news_hyperbot:
    #     change_in_news_hyperbot_count_in_last_week =  ((int(data_log_today.news_hyperbot) - int(data_log_before_week.news_hyperbot))/int(data_log_today.news_hyperbot))*100
    #
    # if data_log_today.news_hyperbot and data_log_before_month.news_hyperbot:
    #     change_in_news_hyperbot_count_in_last_month =  ((int(data_log_today.news_hyperbot) - int(data_log_before_month.news_hyperbot))/int(data_log_today.news_hyperbot))*100
    #
    # if data_log_today.news_hyperbot and data_log_before_six_month.news_hyperbot:
    #     change_in_news_hyperbot_count_in_six_month =  ((int(data_log_today.news_hyperbot) - int(data_log_before_six_month.news_hyperbot))/int(data_log_today.news_hyperbot))*100
    #
    #
    # if data_log_today.news_ibizaspotlight and data_log_before_week.news_ibizaspotlight:
    #     change_in_news_ibizaspotlight_count_in_last_week =  ((int(data_log_today.news_ibizaspotlight) - int(data_log_before_week.news_ibizaspotlight))/int(data_log_today.news_ibizaspotlight))*100
    #
    # if data_log_today.news_hyperbot and data_log_before_month.news_hyperbot:
    #     change_in_news_ibizaspotlight_count_in_last_month =  ((int(data_log_today.news_ibizaspotlight) - int(data_log_before_month.news_ibizaspotlight))/int(data_log_today.news_ibizaspotlight))*100
    #
    # if data_log_today.news_hyperbot and data_log_before_six_month.news_hyperbot:
    #     change_in_news_ibizaspotlight_count_in_six_month =  ((int(data_log_today.news_ibizaspotlight) - int(data_log_before_six_month.news_ibizaspotlight))/int(data_log_today.news_ibizaspotlight))*100
    #
    #
    # if data_log_today.news_magneticmag and data_log_before_week.news_magneticmag:
    #     change_in_news_magneticmag_count_in_last_week =  ((int(data_log_today.news_magneticmag) - int(data_log_before_week.news_magneticmag))/int(data_log_today.news_magneticmag))*100
    #
    # if data_log_today.news_magneticmag and data_log_before_month.news_magneticmag:
    #     change_in_news_magneticmag_count_in_last_month =  ((int(data_log_today.news_magneticmag) - int(data_log_before_month.news_magneticmag))/int(data_log_today.news_magneticmag))*100
    #
    # if data_log_today.news_magneticmag and data_log_before_six_month.news_magneticmag:
    #     change_in_news_magneticmag_count_in_six_month =  ((int(data_log_today.news_magneticmag) - int(data_log_before_six_month.news_magneticmag))/int(data_log_today.news_magneticmag))*100
    #
    #
    # if data_log_today.news_mixmag and data_log_before_week.news_mixmag:
    #     change_in_news_mixmag_count_in_last_week =  ((int(data_log_today.news_mixmag) - int(data_log_before_week.news_mixmag))/int(data_log_today.news_mixmag))*100
    #
    # if data_log_today.news_mixmag and data_log_before_month.news_mixmag:
    #     change_in_news_mixmag_count_in_last_month =  ((int(data_log_today.news_mixmag) - int(data_log_before_month.news_mixmag))/int(data_log_today.news_mixmag))*100
    #
    # if data_log_today.news_mixmag and data_log_before_six_month.news_mixmag:
    #     change_in_news_mixmag_count_in_six_month =  ((int(data_log_today.news_mixmag) - int(data_log_before_six_month.news_mixmag))/int(data_log_today.news_mixmag))*100
    #
    #
    # if data_log_today.news_mtv and data_log_before_week.news_mtv:
    #     change_in_news_mtv_count_in_last_week =  ((int(data_log_today.news_mtv) - int(data_log_before_week.news_mtv))/int(data_log_today.news_mtv))*100
    #
    # if data_log_today.news_mtv and data_log_before_month.news_mtv:
    #     change_in_news_mixmag_count_in_last_month =  ((int(data_log_today.news_mtv) - int(data_log_before_month.news_mtv))/int(data_log_today.news_mtv))*100
    #
    # if data_log_today.news_mtv and data_log_before_six_month.news_mtv:
    #     change_in_news_mixmag_count_in_six_month =  ((int(data_log_today.news_mtv) - int(data_log_before_six_month.news_mtv))/int(data_log_today.news_mtv))*100
    #


# 7253902729 - suman verma
# 9412200846 - vivek verma
# 89 - thapar nagar
#
# 9312849301 - Rajni seth
