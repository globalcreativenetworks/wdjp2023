from django.core.management.base import BaseCommand, CommandError

from social_data.models import FacebookData, TwitterData, YoutubeData, SpotifyData
from artist.models import Artist


class Command(BaseCommand):
    # Show this when the user types help
    help = "migrate Social Data"
        # A command must define handle()

    def handle(self, *args, **options):
        artists_list = Artist.objects.all()

        for artist in artists_list:
            facebook_data_exist = FacebookData.objects.filter(artist=artist).exists()
            if facebook_data_exist:
                facebook_data = FacebookData.objects.filter(artist=artist).order_by('-created')[0]
                fb_fancount = facebook_data.fanCount
                if fb_fancount:
                    artist.facebook_fancount = fb_fancount
                    artist.save()

            twitter_data_exist =TwitterData.objects.filter(artist=artist).exists()
            if twitter_data_exist:
                twitter_data = TwitterData.objects.filter(artist=artist).order_by('-created')[0]
                twitter_followersCount = twitter_data.followersCount
                if twitter_followersCount:
                    artist.twitter_fancount = twitter_followersCount
                    artist.save()

            youtube_data_exist =YoutubeData.objects.filter(artist=artist).exists()
            if youtube_data_exist:
                youtube_data = YoutubeData.objects.filter(artist=artist).order_by('-created')[0]
                youtube_subscriberCount = youtube_data.subscriberCount
                if youtube_subscriberCount:
                    artist.youtube_fancount = youtube_subscriberCount
                    artist.save()

            spotify_data_exist =SpotifyData.objects.filter(artist=artist).exists()
            if spotify_data_exist:
                spotify_data = SpotifyData.objects.filter(artist=artist).order_by('-created')[0]
                spotify_followersCount = spotify_data.followersCount
                if spotify_followersCount:
                    artist.spotify_fancount = spotify_followersCount
                    artist.save()
