from django.core.management.base import BaseCommand, CommandError
from django.db.models.expressions import RawSQL

from social_data.models import SpotifyData
from artist.models import Artist



class Command(BaseCommand):
    # Show this when the user types help
    help = "This command will change the negative to Null in spotify database"
    
    def handle(self, *args, **options):
        spotify_incorrect_data = SpotifyData.objects.filter(followersCount__contains='-').update(followersCount=None)