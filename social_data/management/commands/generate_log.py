import requests
import json

from datetime import timedelta, date
from django.core.management.base import BaseCommand, CommandError

from django.contrib.auth.models import User
from profiles.models import Profile

from social_data.models import DataLog
from artist.models import Artist

from django.conf import settings

# from artist.action_handler import update_social_data

class Command(BaseCommand):
    help = 'This management command helps to weekly update the log for social data, Sources and Artist Added in DataBase'

    def handle(self, *args, **options):
        today_date = date.today()
        data_log = None

        if not data_log:
            try:
                data_log = DataLog.objects.get(created=today_date)
            except:
                pass


        if not data_log:
            try:
                date_for_day = today_date - timedelta(days=1)
                data_log = DataLog.objects.get(created=date_for_day)
            except:
                pass


        if not data_log:
            try:
                date_for_day = today_date - timedelta(days=2)
                data_log = DataLog.objects.get(created=date_for_day)
            except:
                pass


        if not data_log:
            try:
                date_for_day = today_date - timedelta(days=3)
                data_log = DataLog.objects.get(created=date_for_day)
            except:
                pass


        if not data_log:
            try:
                date_for_day = today_date - timedelta(days=4)
                data_log = DataLog.objects.get(created=date_for_day)
            except:
                pass


        if not data_log:
            try:
                date_for_day = today_date - timedelta(days=5)
                data_log = DataLog.objects.get(created=date_for_day)
            except:
                pass


        if not data_log:
            try:
                date_for_day = today_date - timedelta(days=6)
                data_log = DataLog.objects.get(created=date_for_day)
            except:
                pass


        if data_log:

            all_artists = Artist.objects.all()

            len_all_artist = len(all_artists)

            active_artist_count = all_artists.filter(status=1).count()

            inactive_artist_count = all_artists.filter(status=0).count()

            inactive_artist_from_dbbuilder = all_artists.filter(status=0, from_builder=True).count()

            artists_with_gps_coordinates = []

            for artist in all_artists:
                if artist.city_latitude and artist.city_longitude:
                    artists_with_gps_coordinates.append(artists_with_gps_coordinates)

            artists_with_gps_coordinates_count = len(artists_with_gps_coordinates)


            if active_artist_count:
                data_log.active_artist_count = active_artist_count


            if inactive_artist_count:
                data_log.inactive_artist_count = inactive_artist_count
            else:
                data_log.inactive_artist_count = 0


            if inactive_artist_from_dbbuilder:
                data_log.inactive_artist_from_dbbuilder = inactive_artist_from_dbbuilder


            if artists_with_gps_coordinates_count:
                data_log.artist_with_gps_cord_count = artists_with_gps_coordinates_count


            if len_all_artist:
                data_log.artist_aggregated = len_all_artist


            if data_log.events_resident_advisor:
                events_resident_advisor_count = data_log.events_resident_advisor
            else:
                events_resident_advisor_count = 0


            if data_log.events_bandsin_town:
                events_bandsin_town_count = data_log.events_bandsin_town
            else:
                events_bandsin_town_count = 0


            if data_log.events_song_kick:
                events_song_kick_count  = data_log.events_song_kick
            else:
                events_song_kick_count = 0


            if data_log.events_gigatools:
                events_gigatools = data_log.events_gigatools
            else:
                events_gigatools = 0


            data_log.events_aggregated_overall =  int(events_resident_advisor_count) + int(events_bandsin_town_count) + int(events_song_kick_count) + int(events_gigatools)

            db_builder_url = "{}/artist-count/".format(settings.DB_BUILDER_SITE_URL)


            response = requests.post(
                url = db_builder_url,
            )

            data_from_db_builder = response.text

            data_from_db_builder_load = json.loads(data_from_db_builder)

            pending_artist_count = data_from_db_builder_load["pending_artist_count"]

            builder_all_artist = data_from_db_builder_load["all_artist_count"]

            data_log.db_builder_pending_artist_count = pending_artist_count
            data_log.db_builder_artist_count = builder_all_artist

            all_user_count = User.objects.all().count()

            admin_user_count = User.objects.filter(is_superuser=True).count()

            promoter_user_count = Profile.objects.filter(user_types="Promoter").count()

            agent_user_count = Profile.objects.filter(user_types="Agent").count()

            artist_user_count = Profile.objects.filter(user_types="Artists/DJs").count()

            data_log.all_user_count = all_user_count
            data_log.admin_user_count = admin_user_count
            data_log.promoter_user_count = promoter_user_count
            data_log.agent_user_count = agent_user_count
            data_log.artist_user_count = artist_user_count

            data_log.save()

        else:
            print("No Data Log Found !")
