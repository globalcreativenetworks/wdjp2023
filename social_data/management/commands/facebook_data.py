import re
import os
import csv
import json
import codecs
import unicodedata
import pandas as pd

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from social_data.models import FacebookData
from artist.models import Artist

COUNT = 0
MATCH_COUNT = 0
NONE_COUNT = 0
SAVED_COUNT = 0


def print_row(data, update):

    artist_id = None

    try:
        global MATCH_COUNT, NONE_COUNT, COUNT
        try:
            artist_name = unicode(data["Name"]).decode('unicode-escape')
        except:
            artist_name = data["Name"]

        artist = Artist.allartistobjects.filter(artistName=artist_name)

        COUNT += 1

        if artist.first():
            artist_id = artist.first().artistID
            followers = data["followers"]
            likes = data["likes"]

            MATCH_COUNT += 1
            
            if update:
                return update_data(artist_id, followers, likes)
            else:
                return artist_id
                
        else:
            NONE_COUNT += 1
            return None
    except:
        return artist_id


def update_data(artist_id, followers, likes):

    global SAVED_COUNT

    try:
        artist = Artist.allartistobjects.get(artistID=artist_id)

        facebook_data_obj = FacebookData(
            artist=artist,
            likes=likes.replace(',',''),
            fanCount=followers,
            talkingAboutCount=0
        )

        facebook_data_obj.save()

        SAVED_COUNT += 1

        return artist_id
    except Exception as e:
        print("under except", e)
        return None



class Command(BaseCommand):
    help = "This command Is for moving data from csv/xls to database"

    def add_arguments(self, parser):

        parser.add_argument(
            '--update',
            action='store_true',
            help='Updata data to facebook data',
        )

    def handle(self, *args, **options):

        update = options['update']

        df = pd.read_csv(settings.FB_FILE_PATH, encoding='utf-8')

        df.dropna(subset=["Name"], inplace=True)
        df["likes"] = df["likes"].fillna(value='0')
        df["artist_id"] = df.loc[:].apply(
            lambda x: print_row(x, update), axis=1)

        df.to_csv('output.csv', sep='|')

        print("Total Artist In Database: ", Artist.allartistobjects.all().count())
        print("Total Artist In CSV: ", COUNT)
        print("Artists Found: ", MATCH_COUNT)
        print("Artists Not Found: ", NONE_COUNT)
        print("Artists Not Matched: ", Artist.allartistobjects.all().count()-MATCH_COUNT)
        print("Artists Saved: ", SAVED_COUNT)
