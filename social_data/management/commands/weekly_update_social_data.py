from optparse import make_option

from django.core.management.base import BaseCommand, CommandError

from artist.models import Artist
from artist.action_handler import update_social_data,rebuild_db


class Command(BaseCommand):
    help = 'This management command helps to weekly update the social data'
    option_list = BaseCommand.option_list + (
        make_option(
            "-s",
            "--source",
            dest="source",
            help="Enter the Social Source Name",
            metavar="Source"
        ),
    )

    def handle(self, *args, **options):
        social_source = ['facebook', 'twitter', 'spotify', 'youtube', 'instagram']
        if options['source'] is None:
            rebuild_db()
        elif options['source'] not in social_source:
            raise CommandError("Please Select a source from {}".format(", ".join(social_source)))
        else:
            update_social_data.delay(options['source'])
