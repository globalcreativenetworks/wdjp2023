# File name: update_trends.py
# Author: Vantur A.
# Date created: 12/20/2018
# Python Version: 2.7

# Begin code

import numpy as np

from django.core.management.base import BaseCommand, CommandError
from django.db.utils import IntegrityError
from django.db.models import Avg, Count
from datetime import date, timedelta

from social_data.models import FacebookData, TwitterData, YoutubeData, SpotifyData, InstagramData, ArtistTop, Tops,\
    TimeTrends
from artist.models import Artist


class Command(BaseCommand):
    # Show this when the user types help
    help = "This command launches the calculations for Trending Artists tops"
    # A command must define handle()

    today = date.today()
    week_1 = today - timedelta(days=7) # No. for weeks field
    month_1 = today-timedelta(days=31)
    month_3 = today - timedelta(days=31 * 3)
    month_6 = today - timedelta(days=31 * 6)
    year_1 = today - timedelta(days=365)

    def handle(self, *args, **options):
        # get all artists
        artists_list = Artist.objects.all()
        # get the tops from DB
        tops = Tops.objects.all()
        # check every artist in list
        try:
            for artist in artists_list:
                growth_trend = 0
                items = 0
                numbers = 0
                total_followers_count = 0
                
                # get tops existing growth values for different fimeframe
                time_tops = TimeTrends.objects.all()
                # make sure Facebook data exists
                facebook_data_exist = FacebookData.objects.filter(artist=artist).exists()
                if facebook_data_exist:
                    artist_top = ArtistTop()
                    # get the latest social info for the last 2 weeks (social_data is scrapped weekly)
                    facebook_data = FacebookData.objects.filter(artist=artist).order_by('-created')[:2]
                    try:
                        (current_facebook_data, previous_facebook_data) = facebook_data
                    except ValueError:  # no previous_spotify_data existent, try next time
                        (current_facebook_data, previous_facebook_data) = (None, None)
                    if current_facebook_data and previous_facebook_data and current_facebook_data.fanCount is not None and previous_facebook_data.fanCount is not None:
                        # convert fan Count to float from string (they are always integer but float is needed in computation

                        current_fan_count = float(current_facebook_data.fanCount.replace(',',''))
                        previous_fan_count = float(previous_facebook_data.fanCount.replace(',',''))

                        if np.isnan(current_fan_count):
                            current_fan_count = 0
                            previous_fan_count = 0
                        if np.isnan(previous_fan_count):
                            previous_fan_count = 0
    
                        # compute growth trend in percentage up to 6 decimals, negative growth_trend = decreasing popularity

                        # Adding to total followers count
                        total_followers_count += current_fan_count
                        try:
                            facebook_growth_trend = ((current_fan_count - previous_fan_count) / previous_fan_count) * 100
                        except ZeroDivisionError:
                            facebook_growth_trend = 0.0
                        top_id = tops.get(top_name='Facebook')
                        artist_top.top_id = top_id
                        artist_top.artistid = artist
                        artist_top.growth = facebook_growth_trend
                        artist_top.number = (current_fan_count - previous_fan_count)
                        artist_top.save()
                        growth_trend += facebook_growth_trend
                        items += 1
                        numbers += artist_top.number
                        time_trend_fb_exist = time_tops.filter(artist=artist, top=top_id).exists()
                        if time_trend_fb_exist:
                            time_trend_fb = time_tops.get(artist=artist, top=top_id)
                            data = ArtistTop.objects.filter(artistid=artist, top_id=top_id)
                            time_trend_fb.growth_1week, time_trend_fb.number_1week = self.compute_timetrends(data=data, start=self.week_1)
                            time_trend_fb.growth_1mth, time_trend_fb.number_1mth = self.compute_timetrends(data=data, start=self.month_1)
                            time_trend_fb.growth_3mth, time_trend_fb.number_3mth = self.compute_timetrends(data=data, start=self.month_3)
                            time_trend_fb.growth_6mth, time_trend_fb.number_6mth = self.compute_timetrends(data=data, start=self.month_6)
                            time_trend_fb.growth_1yr, time_trend_fb.number_1yr = self.compute_timetrends(data=data, start=self.year_1)

                            # Adding Current Data for Facebook
                            time_trend_fb.current_count = current_fan_count
                        else:
                            time_trend_fb = TimeTrends()
                            time_trend_fb.artist = artist
                            time_trend_fb.growth_1week = 0
                            time_trend_fb.number_1week = 0
                            time_trend_fb.growth_1mth = 0
                            time_trend_fb.number_1mth = 0
                            time_trend_fb.growth_3mth = 0
                            time_trend_fb.number_3mth = 0
                            time_trend_fb.growth_6mth = 0
                            time_trend_fb.number_6mth = 0
                            time_trend_fb.growth_1yr = 0
                            time_trend_fb.number_1yr = 0

                            # Adding Current Data for Facebook
                            time_trend_fb.current_count = current_fan_count
                            time_trend_fb.top = top_id
                        time_trend_fb.modified = self.today
                        time_trend_fb.save()

                # make sure Twitter data exists
                twitter_data_exist = TwitterData.objects.filter(artist=artist).exists()
                if twitter_data_exist:
                    artist_top = ArtistTop()
                    # get the latest social info for the last 2 weeks (social_data is scrapped weekly)
                    twitter_data = TwitterData.objects.filter(artist=artist).order_by('-created')[:2]
                    try:
                        (current_twitter_data, previous_twitter_data) = twitter_data
                    except ValueError:  # no previous_spotify_data existent, try next time
                        (current_twitter_data, previous_twitter_data) = (None, None)
                    if current_twitter_data and previous_twitter_data and current_twitter_data.followersCount is not None and previous_twitter_data.followersCount is not None:
                        # convert fan Count to float from string (they are always integer but float is needed in computation
                        current_fan_count = float(current_twitter_data.followersCount)
                        previous_fan_count = float(previous_twitter_data.followersCount)
                        # compute growth trend in percentage up to 6 decimals, negative growth_trend = decreasing popularity

                        # Adding to total followers count
                        total_followers_count += current_fan_count
                        try:
                            twitter_growth_trend = ((current_fan_count - previous_fan_count) / previous_fan_count) * 100
                        except ZeroDivisionError:
                            twitter_growth_trend = 0.0
                        top_id = tops.get(top_name='Twitter')
                        artist_top.top_id = top_id
                        artist_top.artistid = artist
                        artist_top.growth = twitter_growth_trend
                        artist_top.number = (current_fan_count - previous_fan_count)
                        artist_top.save()
                        growth_trend += twitter_growth_trend
                        items += 1
                        numbers += artist_top.number
                        time_trend_tw_exist = time_tops.filter(artist=artist, top=top_id).exists()

                        if time_trend_tw_exist:
                            time_trend_tw = time_tops.get(artist=artist, top=top_id)
                            data = ArtistTop.objects.filter(artistid=artist, top_id=top_id)
                            time_trend_tw.growth_1week, time_trend_tw.number_1week = self.compute_timetrends(data=data, start=self.week_1)
                            time_trend_tw.growth_1mth, time_trend_tw.number_1mth = self.compute_timetrends(data=data, start=self.month_1)
                            time_trend_tw.growth_3mth, time_trend_tw.number_3mth = self.compute_timetrends(data=data, start=self.month_3)
                            time_trend_tw.growth_6mth, time_trend_tw.number_6mth = self.compute_timetrends(data=data, start=self.month_6)
                            time_trend_tw.growth_1yr, time_trend_tw.number_1yr = self.compute_timetrends(data=data, start=self.year_1)

                            # Adding Current Count for Twitter
                            time_trend_tw.current_count = current_fan_count
                        else:
                            time_trend_tw = TimeTrends()
                            time_trend_tw.artist = artist
                            time_trend_tw.growth_1week = 0
                            time_trend_tw.number_1week = 0
                            time_trend_tw.growth_1mth = 0
                            time_trend_tw.number_1mth = 0
                            time_trend_tw.growth_3mth = 0
                            time_trend_tw.number_3mth = 0
                            time_trend_tw.growth_6mth = 0
                            time_trend_tw.number_3mth = 0
                            time_trend_tw.growth_1yr = 0
                            time_trend_tw.number_1yr = 0
                            time_trend_tw.current_count = current_fan_count
                            time_trend_tw.top = top_id
                        time_trend_tw.modified = self.today
                        time_trend_tw.save()

                # make sure Youtube data exists
                youtube_data_exist = YoutubeData.objects.filter(artist=artist).exists()
                if youtube_data_exist:
                    artist_top = ArtistTop()
                    # get the latest social info for the last 2 weeks (social_data is scrapped weekly)
                    youtube_data = YoutubeData.objects.filter(artist=artist).order_by('-created')[:2]
                    try:
                        (current_youtube_data, previous_youtube_data) = youtube_data
                    except ValueError:  # no previous_spotify_data existent, try next time
                        current_youtube_data, previous_youtube_data = (None, None)

                    # Data for subscriber
                    if current_youtube_data and previous_youtube_data and  current_youtube_data.subscriberCount is not None and previous_youtube_data.subscriberCount is not None:
                        # convert fan Count to float from string (they are always integer but float is needed in computation
                        current_fan_count = float(current_youtube_data.subscriberCount)
                        previous_fan_count = float(previous_youtube_data.subscriberCount)
                        # compute growth trend in percentage up to 6 decimals, negative growth_trend = decreasing popularity

                        # Adding to total followers count
                        total_followers_count += current_fan_count
                        try:
                            youtube_growth_trend = ((current_fan_count - previous_fan_count) / previous_fan_count) * 100
                        except ZeroDivisionError:
                            youtube_growth_trend = 0.0
                        top_id = tops.get(top_name='Youtube')
                        artist_top.top_id = top_id
                        artist_top.artistid = artist
                        artist_top.growth = youtube_growth_trend
                        artist_top.number = (current_fan_count - previous_fan_count)
                        artist_top.save()
                        growth_trend += youtube_growth_trend
                        items += 1
                        numbers += artist_top.number
                        time_trend_yt_exist = time_tops.filter(artist=artist, top=top_id).exists()

                        if time_trend_yt_exist:
                            time_trend_yt = time_tops.get(artist=artist, top=top_id)
                            data = ArtistTop.objects.filter(artistid=artist, top_id=top_id)
                            time_trend_yt.growth_1week, time_trend_yt.number_1week = self.compute_timetrends(data=data, start=self.week_1)
                            time_trend_yt.growth_1mth, time_trend_yt.number_1mth = self.compute_timetrends(data=data, start=self.month_1)
                            time_trend_yt.growth_3mth, time_trend_yt.number_3mth = self.compute_timetrends(data=data, start=self.month_3)
                            time_trend_yt.growth_6mth, time_trend_yt.number_6mth = self.compute_timetrends(data=data, start=self.month_6)
                            time_trend_yt.growth_1yr, time_trend_yt.number_1yr = self.compute_timetrends(data=data, start=self.year_1)

                            # Adding Current Count for YouTube Subscriber
                            time_trend_yt.current_count = current_fan_count
                        else:
                            time_trend_yt = TimeTrends()
                            time_trend_yt.artist = artist
                            time_trend_yt.growth_1week = 0
                            time_trend_yt.number_1week = 0
                            time_trend_yt.growth_1mth = 0
                            time_trend_yt.number_1mth = 0
                            time_trend_yt.growth_3mth = 0
                            time_trend_yt.number_3mth = 0
                            time_trend_yt.growth_6mth = 0
                            time_trend_yt.number_6mth = 0
                            time_trend_yt.growth_1yr = 0
                            time_trend_yt.number_1yr = 0

                            # Adding Current Count for YouTube Subscriber
                            time_trend_yt.current_count = current_fan_count
                            time_trend_yt.top = top_id
                        time_trend_yt.modified = self.today
                        time_trend_yt.save()

                    # Data for Youtube views
                    if current_youtube_data and previous_youtube_data and current_youtube_data.viewCount is not None and previous_youtube_data.viewCount is not None:
                        # convert fan Count to float from string (they are always integer but float is needed in computation
                        current_view_count = float(current_youtube_data.viewCount)
                        previous_view_count = float(previous_youtube_data.viewCount)
                        # compute growth trend in percentage up to 6 decimals, negative growth_trend = decreasing popularity

                        # Adding to total followers count
                        total_followers_count += current_view_count
                        try:
                            youtube_view_growth_trend = ((current_view_count - previous_view_count) / previous_view_count) * 100
                        except ZeroDivisionError:
                            youtube_view_growth_trend = 0.0
                        top_id = tops.get(top_name='Youtube Views') # Top name new or is fine
                        artist_top.top_id = top_id
                        artist_top.artistid = artist
                        artist_top.growth = youtube_view_growth_trend
                        artist_top.number = (current_view_count - previous_view_count)
                        artist_top.save()
                        growth_trend += youtube_view_growth_trend
                        items += 1
                        numbers += artist_top.number
                        time_trend_yt_views_exist = time_tops.filter(artist=artist, top=top_id).exists()

                        if time_trend_yt_views_exist:
                            time_trend_yt_views = time_tops.get(artist=artist, top=top_id)
                            data = ArtistTop.objects.filter(artistid=artist, top_id=top_id)
                            time_trend_yt_views.growth_1week, time_trend_yt_views.number_1week = self.compute_timetrends(data=data, start=self.week_1)
                            time_trend_yt_views.growth_1mth, time_trend_yt_views.number_1mth = self.compute_timetrends(data=data, start=self.month_1)
                            time_trend_yt_views.growth_3mth, time_trend_yt_views.number_3mth = self.compute_timetrends(data=data, start=self.month_3)
                            time_trend_yt_views.growth_6mth, time_trend_yt_views.number_6mth = self.compute_timetrends(data=data, start=self.month_6)
                            time_trend_yt_views.growth_1yr, time_trend_yt_views.number_1yr = self.compute_timetrends(data=data, start=self.year_1)

                            # Adding Current Count for YouTube Views
                            time_trend_yt_views.current_count = current_fan_count
                        else:
                            time_trend_yt_views = TimeTrends()
                            time_trend_yt_views.artist = artist
                            time_trend_yt_views.growth_1week = 0
                            time_trend_yt_views.number_1week = 0
                            time_trend_yt_views.growth_1mth = 0
                            time_trend_yt_views.number_1mth = 0
                            time_trend_yt_views.growth_3mth = 0
                            time_trend_yt_views.number_3mth = 0
                            time_trend_yt_views.growth_6mth = 0
                            time_trend_yt_views.number_6mth = 0
                            time_trend_yt_views.growth_1yr = 0
                            time_trend_yt_views.number_1yr = 0

                            # Adding Current Count for YouTube Views
                            time_trend_yt.current_count = current_view_count
                            time_trend_yt_views.top = top_id
                        time_trend_yt_views.modified = self.today
                        time_trend_yt_views.save()
                    
                # make sure Spotify data exists
                spotify_data_exist = SpotifyData.objects.filter(artist=artist).exists()
                if spotify_data_exist:
                    artist_top = ArtistTop()
                    # get the latest social info for the last 2 weeks (social_data is scrapped weekly)
                    spotify_data = SpotifyData.objects.filter(artist=artist).order_by('-created')[:2]
                    try:
                        (current_spotify_data, previous_spotify_data) = spotify_data
                    except ValueError:  # no previous_spotify_data existent, try next time
                        (current_spotify_data, previous_spotify_data) = (None, None)
                    if current_spotify_data and previous_spotify_data and current_spotify_data.followersCount is not None and previous_spotify_data.followersCount is not None:
                        # convert fan Count to float from string (they are always integer but float is needed in computation
                        current_fan_count = float(current_spotify_data.followersCount)
                        previous_fan_count = float(previous_spotify_data.followersCount)
                        # compute growth trend in percentage up to 6 decimals, negative growth_trend = decreasing popularity

                        # Adding to total followers count
                        total_followers_count += current_fan_count
                        try:
                            spotify_growth_trend = ((current_fan_count - previous_fan_count) / previous_fan_count) * 100
                        except ZeroDivisionError:
                            spotify_growth_trend = 0.0
                        top_id = tops.get(top_name='Spotify')
                        artist_top.top_id = top_id
                        artist_top.artistid = artist
                        artist_top.growth = spotify_growth_trend
                        artist_top.number = (current_fan_count - previous_fan_count)
                        artist_top.save()
                        growth_trend += spotify_growth_trend
                        items += 1
                        numbers += artist_top.number
                        time_trend_sp_exist = time_tops.filter(artist=artist, top=top_id).exists()

                        if time_trend_sp_exist:
                            time_trend_sp = time_tops.get(artist=artist, top=top_id)
                            data = ArtistTop.objects.filter(artistid=artist, top_id=top_id)
                            time_trend_sp.growth_1week, time_trend_sp.number_1week = self.compute_timetrends(data=data, start=self.week_1)
                            time_trend_sp.growth_1mth, time_trend_sp.number_1mth = self.compute_timetrends(data=data, start=self.month_1)
                            time_trend_sp.growth_3mth, time_trend_sp.number_3mth = self.compute_timetrends(data=data, start=self.month_3)
                            time_trend_sp.growth_6mth, time_trend_sp.number_6mth = self.compute_timetrends(data=data, start=self.month_6)
                            time_trend_sp.growth_1yr, time_trend_sp.number_1yr = self.compute_timetrends(data=data, start=self.year_1)

                            # Adding Current Count for Spotify
                            time_trend_sp.current_count = current_fan_count
                        else:
                            time_trend_sp = TimeTrends()
                            time_trend_sp.artist = artist
                            time_trend_sp.growth_1week = 0
                            time_trend_sp.number_1week = 0
                            time_trend_sp.growth_1mth = 0
                            time_trend_sp.number_1mth = 0
                            time_trend_sp.growth_3mth = 0
                            time_trend_sp.number_3mth = 0
                            time_trend_sp.growth_6mth = 0
                            time_trend_sp.number_6mth = 0
                            time_trend_sp.growth_1yr = 0
                            time_trend_sp.number_1yr = 0

                            # Adding Current Count for Spotify
                            time_trend_sp.current_count = current_fan_count
                            time_trend_sp.top = top_id
                        time_trend_sp.modified = self.today
                        time_trend_sp.save()

                # make sure Instagram data exists
                instagram_data_exist = InstagramData.objects.filter(artist=artist).exists()
                if instagram_data_exist:
                    artist_top = ArtistTop()
                    # get the latest social info for the last 2 weeks (social_data is scrapped weekly)
                    instagram_data = InstagramData.objects.filter(artist=artist).order_by('-created')[:2]
                    try:
                        (current_instagram_data, previous_instagram_data) = instagram_data
                    except ValueError:  # no previous_instagram_data existent, try next time
                        (current_instagram_data, previous_instagram_data) = (None, None)
                    if current_instagram_data and previous_instagram_data and current_instagram_data.followed_by is not None and previous_instagram_data.followed_by is not None:
                        # convert fan Count to float from string (they are always integer but float is needed in computation
                        current_fan_count = float(current_instagram_data.followed_by)
                        previous_fan_count = float(previous_instagram_data.followed_by)
                        # compute growth trend in percentage up to 6 decimals, negative growth_trend = decreasing popularity

                        # Adding to total followers count
                        total_followers_count += current_fan_count
                        try:
                            instagram_growth_trend = ((current_fan_count - previous_fan_count) / previous_fan_count) * 100
                        except ZeroDivisionError:
                            instagram_growth_trend = 0.0
                        top_id = tops.get(top_name='Instagram')
                        artist_top.top_id = top_id
                        artist_top.artistid = artist
                        artist_top.growth = instagram_growth_trend
                        artist_top.number = (current_fan_count - previous_fan_count)
                        artist_top.save()
                        growth_trend += instagram_growth_trend
                        items += 1
                        numbers += artist_top.number
                        time_trend_insta_exist = time_tops.filter(artist=artist, top=top_id).exists()

                        if time_trend_insta_exist:
                            time_trend_insta = time_tops.get(artist=artist, top=top_id)
                            data = ArtistTop.objects.filter(artistid=artist, top_id=top_id)
                            time_trend_insta.growth_1week, time_trend_insta.number_1week = self.compute_timetrends(data=data, start=self.week_1)
                            time_trend_insta.growth_1mth, time_trend_insta.number_1mth = self.compute_timetrends(data=data, start=self.month_1)
                            time_trend_insta.growth_3mth, time_trend_insta.number_3mth = self.compute_timetrends(data=data, start=self.month_3)
                            time_trend_insta.growth_6mth, time_trend_insta.number_6mth = self.compute_timetrends(data=data, start=self.month_6)
                            time_trend_insta.growth_1yr, time_trend_insta.number_1yr = self.compute_timetrends(data=data, start=self.year_1)

                            # Adding Current Count for Instagram
                            time_trend_insta.current_count = current_fan_count
                        else:
                            time_trend_insta = TimeTrends()
                            time_trend_insta.artist = artist
                            time_trend_insta.growth_1week = 0
                            time_trend_insta.number_1week = 0
                            time_trend_insta.growth_1mth = 0
                            time_trend_insta.number_1mth = 0
                            time_trend_insta.growth_3mth = 0
                            time_trend_insta.number_3mth = 0
                            time_trend_insta.growth_6mth = 0
                            time_trend_insta.number_6mth = 0
                            time_trend_insta.growth_1yr = 0
                            time_trend_insta.number_1yr = 0

                            # Adding Current Count for Instagram
                            time_trend_insta.current_count = current_fan_count
                            time_trend_insta.top = top_id
                        time_trend_insta.modified = self.today
                        time_trend_insta.save()

                # make the wheredjsplay Growth Trend
                if items > 0:
                    top_id = tops.get(top_name='WDJP')
                    growth_trend = growth_trend / items
                    artist_top = ArtistTop()
                    artist_top.top_id = top_id
                    artist_top.artistid = artist
                    artist_top.growth = growth_trend
                    artist_top.number = numbers / items
                    artist_top.save()

                    time_trend_wdjp_exist = time_tops.filter(artist=artist, top=top_id).exists()
                    if time_trend_wdjp_exist:
                        time_trend_wdjp = time_tops.get(artist=artist, top=top_id)
                        data = ArtistTop.objects.filter(artistid=artist, top_id=top_id)
                        time_trend_wdjp.growth_1week, time_trend_wdjp.number_1week = self.compute_timetrends(data=data, start=self.week_1)
                        time_trend_wdjp.growth_1mth, time_trend_wdjp.number_1mth = self.compute_timetrends(data=data, start=self.month_1)
                        time_trend_wdjp.growth_3mth, time_trend_wdjp.number_3mth = self.compute_timetrends(data=data, start=self.month_3)
                        time_trend_wdjp.growth_6mth, time_trend_wdjp.number_6mth = self.compute_timetrends(data=data, start=self.month_6)
                        time_trend_wdjp.growth_1yr, time_trend_wdjp.number_1yr = self.compute_timetrends(data=data, start=self.year_1)

                        # Adding Aggregated Current count
                        time_trend_wdjp.current_count = total_followers_count
                    else:
                        time_trend_wdjp = TimeTrends()
                        time_trend_wdjp.artist = artist
                        time_trend_wdjp.growth_1week = 0
                        time_trend_wdjp.number_1week = 0
                        time_trend_wdjp.growth_1mth = 0
                        time_trend_wdjp.number_1mth = 0
                        time_trend_wdjp.growth_3mth = 0
                        time_trend_wdjp.number_3mth = 0
                        time_trend_wdjp.growth_6mth = 0
                        time_trend_wdjp.number_6mth = 0
                        time_trend_wdjp.growth_1yr = 0
                        time_trend_wdjp.number_1yr = 0

                        # Adding Aggregated Current count
                        time_trend_wdjp.current_count = total_followers_count
                        time_trend_wdjp.top = top_id
                    time_trend_wdjp.modified = self.today
                    time_trend_wdjp.save()
        except IntegrityError as e:
            raise e
            print("Update cannot be performed because only one is allowed per day!")

    def compute_timetrends(self, data, start):
        if data is not None and start is not None:
            growth = data.filter(created__range=[start, self.today]).aggregate(avg_growth=Avg('growth'))
            number = data.filter(created__range=[start, self.today]).aggregate(avg_number=Avg('number'))
            return growth['avg_growth'], number['avg_number']
        return 0
