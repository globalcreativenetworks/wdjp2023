from django.contrib import admin

from social_data.models import DataLog, YoutubeData, TwitterData, FacebookData, SpotifyData, InstagramData, SpotifyArtistImages, SpotifyArtistGenre, SpotifyArtistTopTracks, TimeTrends, Tops


def artist_name(obj):
    return '%s - Week %s' % (obj.artist.artistName, obj.created.isocalendar()[1] if obj.created else '')
artist_name.short_description = 'Artist'


class LogDataAdmin(admin.ModelAdmin):
    list_display = ("created",)


class YoutubeDataAdmin(admin.ModelAdmin):
    list_display = (artist_name, "publishedAt", "commentCount", "videoCount", "viewCount", "subscriberCount", "created")
    readonly_fields = ['created', 'modified']
    search_fields = ("artist__artistName", "publishedAt", "commentCount", "videoCount", "viewCount", "subscriberCount")
    ordering = ('artist__artistName',)


class TwitterDataAdmin(admin.ModelAdmin):
    list_display = (artist_name, "followersCount", "friendsCount", "favouritesCount", "statusesCount", "listedCount", "retweetCount", "createdAt", "created")
    readonly_fields = ['created', 'modified']
    search_fields = ("artist__artistName", "followersCount", "friendsCount", "favouritesCount", "statusesCount", "listedCount", "retweetCount", "createdAt")
    ordering = ('artist__artistName',)


class FacebookDataAdmin(admin.ModelAdmin):
    list_display = (artist_name, "fanCount", "talkingAboutCount", 'created')
    readonly_fields = ['created', 'modified']
    search_fields = ("artist__artistName",)
    ordering = ('artist__artistName',)


class SpotifyDataAdmin(admin.ModelAdmin):
    list_display = (artist_name, "followersCount", 'popularityIndex', 'created')
    readonly_fields = ['created', 'modified']
    search_fields = ("artist__artistName",)
    ordering = ('artist__artistName',)


class InstagramDataAdmin(admin.ModelAdmin):
    list_display = (artist_name, "followed_by", "num_posts", 'created')
    readonly_fields = ['created', 'modified']
    search_fields = ("artist__artistName",)
    ordering = ('artist__artistName',)


class SpotifyArtistImagesAdmin(admin.ModelAdmin):
    list_display = ("spotify_data",)
    raw_id_fields = ('spotify_data',)


class SpotifyArtistGenreAdmin(admin.ModelAdmin):
    list_display = ("genre_name",)


class SpotifyArtistTopTracksAdmin(admin.ModelAdmin):
    list_display = ("track_name",)
    raw_id_fields = ('spotify_data',)

admin.site.register(DataLog, LogDataAdmin)
admin.site.register(YoutubeData, YoutubeDataAdmin)
admin.site.register(TwitterData, TwitterDataAdmin)
admin.site.register(FacebookData, FacebookDataAdmin)
admin.site.register(SpotifyData, SpotifyDataAdmin)
admin.site.register(InstagramData, InstagramDataAdmin)
admin.site.register(SpotifyArtistImages, SpotifyArtistImagesAdmin)
admin.site.register(SpotifyArtistGenre, SpotifyArtistGenreAdmin)
admin.site.register(SpotifyArtistTopTracks, SpotifyArtistTopTracksAdmin)

# Registered Time Trends
admin.site.register(TimeTrends)
admin.site.register(Tops)