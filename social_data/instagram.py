import os
import urllib
import requests
import time
from bs4 import BeautifulSoup
from unidecode import unidecode
import logging
import json
from datetime import date

from django.conf import settings
from report.utils import *

from report.models import ReportInstagram
from social_data.utils import _get_url_for_response
from social_data.models import InstagramData
from artist.models import Source, Artist, ArtistWebsite
from requests.exceptions import ConnectionError

from social_data.models import DataLog

class InstagramApp():

    def __init__(self):
        self.logger = logging.getLogger("InstagramApp")
        self.today_date = date.today()

    def get_insta_data(self,instaurl):
        try:

            r = requests.get(instaurl)

            self.logger.info('Processing: '+str(instaurl))
            soup = BeautifulSoup(r.text, 'html.parser')
            body = soup.find('body')
            script_tag = body.find('script')
            text = script_tag.text.strip().replace('window._sharedData =', '').replace(';', '')

            # text = soup.find_all('script')[2].text.strip('window._sharedData = ').strip(';')
            data = json.loads(text)

            # user = data['entry_data']['ProfilePage'][0]['user']
            user = data['entry_data']['ProfilePage'][0]['graphql']['user']

            return user

        except Exception as e:
            print('Error in ' + str(instaurl))
            return None

    def create_instance(self, artist, data):
        """
        This method is use to create InstaGram Data instance of a artist.
        """

        if data.has_key('error') :
            pass
        else:
            instance = InstagramData.objects.create(artist=artist)

            if data.has_key('edge_followed_by') and data['edge_followed_by'].has_key('count'):
                instance.followed_by = data['edge_followed_by']['count']
                artist.instagram_fancount = data['edge_followed_by']['count']
                artist.save()
            else :
                raise Exception('Followers not scraped')
            if data.has_key('edge_follow') and data['edge_follow'].has_key('count'):
                instance.follows = data['edge_follow']['count']
            else :
                raise Exception('Follows not scraped')
            if data.get('external_url',None):
                instance.external_url = data.get('external_url',None)

            if data.has_key('biography'):
                instance.bio = data['biography']

            if data.has_key('edge_owner_to_timeline_media') and data['edge_owner_to_timeline_media'].has_key('count'):
                instance.num_posts = data['edge_owner_to_timeline_media']['count']

            instance.save()

            return instance

    def create_photo_instance(self, artist, data):
        """
        This method is use to create InstaGram Data instance of a artist.
        """

        if data.has_key('error') :
            pass
        else:
            instance = InstagramData.objects.create(artist=artist)

            if data.has_key('edge_followed_by') and data['edge_followed_by'].has_key('count'):
                instance.followed_by = data['edge_followed_by']['count']
                artist.instagram_fancount = data['edge_followed_by']['count']
                artist.save()
            else :
                pass

            if data.has_key('edge_follow') and data['edge_follow'].has_key('count'):
                instance.follows = data['edge_follow']['count']
            else :
                raise Exception('Follows not scraped')
            if data.get('external_url',None):
                instance.external_url = data.get('external_url',None)


            if data.has_key('profile_pic_url'):
                try:
                    image_url = data['profile_pic_url']
                    response = requests.get(image_url)
                    image_name = artist.artistName.replace(" ", "_").decode('unicode_escape').encode('ascii','ignore') + ".jpg"
                    full_image_path = os.path.join(settings.MEDIA_ROOT, 'artistphotos', image_name)

                    artist_image_file = open(full_image_path, 'wb+')
                    artist_image_file.write(response.content)
                    artist_image_file.close()

                    artist_image_path = full_image_path.split("media/")[1]

                    instance.profile_pic = artist_image_path
                except:
                    pass

            instance.save()

            return instance


def scrape_artist_photo(artists=[]):
    today_date = date.today()

    count = 0

    for artist in artists:
        artist = Artist.objects.get(artistID=artist)
        source, created = Source.objects.get_or_create(sourceName="instagram")

        try:
            artist_website = ArtistWebsite.objects.get(sourceID=source.sourceID, artistID=artist)
        except ArtistWebsite.DoesNotExist:
            artist_website = None
        except ArtistWebsite.MultipleObjectsReturned:
            artist_website = ArtistWebsite.objects.filter(sourceID=source.sourceID, artistID=artist)[0]

        if artist_website:
            artist_insta_data = InstagramApp()
            try:
                data = artist_insta_data.get_insta_data(artist_website.url)

                instance = artist_insta_data.create_photo_instance(artist, data)
                if data and instance:
                    count += 1
                else:
                    raise Exception("Data was not scraped")
            except Exception as e:

                continue

        else:
            pass





def scrap_instagram_data(artists=[]):
    today_date = date.today()

    count = 0

    for artist in artists:
        artist = Artist.objects.get(artistID=artist)
        source, created = Source.objects.get_or_create(sourceName="instagram")

        try:
            artist_website = ArtistWebsite.objects.get(sourceID=source.sourceID, artistID=artist)
        except ArtistWebsite.DoesNotExist:
            artist_website = None
        except ArtistWebsite.MultipleObjectsReturned:
            artist_website = ArtistWebsite.objects.filter(sourceID=source.sourceID, artistID=artist)[0]

        if artist_website:
            artist_insta_data = InstagramApp()
            try:
                data = artist_insta_data.get_insta_data(artist_website.url)

                instance = artist_insta_data.create_instance(artist, data)
                if data and instance:
                    count += 1
                else:
                    raise Exception("Data was not scraped")
            except Exception as e:
                report = ReportInstagram(created=today_date,
                                       artist=artist)
                report.title = "Artist: %s was not scrapped" % artist.artistName
                report.description = "Internal error occured during scrapping process: " + str(e.message)+'\n' + "Url: " + artist_website.url
                report.event = ERROR
                report.resolution = INTERNAL_SERVER_ERROR
                report.save()
                continue
            report = ReportInstagram(created=today_date,
                                   artist=artist)
            report.title = "Artist: %s was scrapped" % artist.artistName
            report.description = "Was scrapped from url: "+ artist_website.url
            report.event = INFO
            report.resolution = NA
            report.save()
        else:
            report = ReportInstagram(created=today_date,
                                   artist=artist)
            report.title = "Artist: %s was not scrapped" % artist.artistName
            report.description = "Artist does not have an URL"
            report.event = UNKNOWN
            report.resolution = CKECK_URL
            report.save()
            count += 1

    instagram_log, created = DataLog.objects.get_or_create(created=today_date)

    if instagram_log.instagram_count:
        instagram_count = int(instagram_log.instagram_count)
        instagram_count += count
        instagram_log.instagram_count = instagram_count
    else:
        instagram_log.instagram_count = count

    instagram_log.save()
