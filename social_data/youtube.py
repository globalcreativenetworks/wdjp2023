from django.conf import settings

from report.models import ReportYoutube
from report.utils import *

from social_data.models import YoutubeData, DataLog
from social_data.utils import _get_url_for_response
from artist.models import Source, Artist, ArtistWebsite
from datetime import date

class YoutubeAPI(object):
    today_date = date.today()
    forUsername = None
    id = None

    def __init__(self, forUsername):
        self.base_url = "https://www.googleapis.com/youtube/v3/channels"
        self.key = settings.YOUTUBE_API_KEY



        if len(forUsername.split("/")) > 1:
            # Check if channel
            if "channel" in forUsername.split("/")[-2]:
                self.id = forUsername.split("/")[-1]
            else:
                self.forUsername = forUsername.split("/")[-1]
        else:
            self.forUsername = forUsername


    def set_pageid_from_artist(self, artistID):
        try:
            artist = Artist.allartistobjects.get(artistID=artistID)
            source, created = Source.objects.get_or_create(sourceName="Youtube")
        except ArtistWebsite.DoesNotExist:
            artist_website = None
        try:
            artist_website = ArtistWebsite.objects.get(sourceID=source.sourceID, artistID=artist)
        except ArtistWebsite.DoesNotExist:
            artist_website = None
        except ArtistWebsite.MultipleObjectsReturned:
            artist_website = ArtistWebsite.objects.filter(sourceID=source.sourceID, artistID=artist)[0]
        if artist_website:
            self.page_id = artist_website.url.split("?")[0]
        else:
            self.page_id = None


    def page_fans_country(self):
        if not self.page_id:
            return {'error':'Page Url is not set for the artist'}
        tznow = datetime.datetime.utcnow().strftime('%Y-%m-%d')
        page_id  = self.page_id.replace("https://www.facebook.com/","")
        if not page_id.endswith('/'):
            page_id = "%s/"%page_id
        url = self.base_url + page_id + "insights/page_fans_country/lifetime?since="+tznow+"&until="+tznow+"&access_token=" + self.access_token['access_token']
        for i in range(0,2):
            while True:
                try:
                    time.sleep(5)
                    response = _get_url_for_response(url)
                except ConnectionError as e:
                    print "Retry",e
                    continue
                return response.json()
        return {'error':'Page Url is not set for the artist'}



    def get_statistics(self):
        """
        This function return the statistics of the channel i.e
        commentCount, viewCount, videoCount, subscriberCount
        """
        url = ""
        if self.forUsername is None:
            url = self.base_url + "?part=statistics&id=" + self.id + "&key=" + self.key
        elif self.id is None:
            url = self.base_url + "?part=statistics&forUsername=" + self.forUsername + "&key=" + self.key
        response = _get_url_for_response(url)
        statistics = response.json()

        if statistics['items']:
            return statistics['items'][0]
        else:
            return {'errors': [{'message': 'Sorry, that channel does not exist.',}]}


    def get_snippet(self):
        """
        This function return the snippet of the channel i.e
        thumbnails, country, title, publishedAt, description

        """
        if self.id is None:
            url = self.base_url + "?part=snippet&forUsername=" + self.forUsername + "&key=" + self.key
        elif self.forUsername is None:
            url = self.base_url + "?part=snippet&id=" + self.id + "&key=" + self.key
        response = _get_url_for_response(url)
        snippet = response.json()

        if snippet['items']:
            return snippet['items'][0]
        else:
            return {'errors': [{'message': 'Sorry, that channel does not exist.',}]}


    def create_instance(self, artist, data):
        """
        This method is use to create or update the YoutubeData instance on a artist.
        """
        if data.has_key('errors') or data.has_key('error'):
            pass
        else:
            instance = YoutubeData.objects.create(artist=artist)
            instance.publishedAt = data['publishedAt']
            instance.commentCount = data['commentCount']
            instance.videoCount = data['videoCount']
            instance.viewCount = data['viewCount']
            instance.subscriberCount = data['subscriberCount']
            instance.description = data['description']
            instance.save()

            if data.get("subscriberCount"):
                artist.youtube_fancount = data.get("subscriberCount","")
                artist.save()

            return instance


    def get_clean_data(self):
        data = {}
        statistics = self.get_statistics()

        snippet = self.get_snippet()

        if statistics.has_key('errors') or snippet.has_key('errors'):
            data['error'] = 'Sorry, that channel does not exist.'
        else:
            statistics = statistics['statistics']
            snippet = snippet['snippet']
            data['commentCount'] = statistics['commentCount']
            data['videoCount'] = statistics['videoCount']
            data['viewCount'] = statistics['viewCount']
            data['subscriberCount'] = statistics['subscriberCount']
            data['publishedAt'] = snippet['publishedAt']
            data['description'] = snippet['description']
        return data


def scrap_youtube_api(artists=[]):
    today_date = date.today()
    count = 0
    for artist in artists:
        artist = Artist.objects.get(artistID=artist)
        source, created = Source.objects.get_or_create(sourceName="Youtube")


        try:
            artist_website = ArtistWebsite.objects.get(sourceID=source.sourceID, artistID=artist)
        except ArtistWebsite.DoesNotExist:
            artist_website = None
        except ArtistWebsite.MultipleObjectsReturned:
            artist_website = ArtistWebsite.objects.filter(sourceID=source.sourceID, artistID=artist)[0]

        if artist_website:
            instance = None
            try:
                artist_youtubeapi = YoutubeAPI(artist_website.url)
                data = artist_youtubeapi.get_clean_data()
                instance = artist_youtubeapi.create_instance(artist, data)
                if instance is None :
                    count += 1
                    if 'error' in data:
                        raise Exception("Data was not scraped with error " + data['error'] )
                    raise Exception("Data was not scraped")
                    pass
            except Exception as e:
                report = ReportYoutube(created=today_date,
                                       artist=artist)
                report.title = "Artist: %s was not scrapped" % artist.artistName
                report.description = "Internal error occured during scrapping process: " + str(e.message)+'\n' + "Url: " + artist_website.url
                report.event = ERROR
                report.resolution = INTERNAL_SERVER_ERROR
                report.save()
                # continue if error
                continue
            report = ReportYoutube(created=today_date,
                                   artist=artist)
            report.title = "Artist: %s was scrapped" % artist.artistName
            report.description = "FanCount " + str(instance.subscriberCount) + " was scrapped from url: "+ artist_website.url
            report.event = INFO
            report.resolution = NA
            report.save()
        else:
            report = ReportYoutube(created=today_date,
                                   artist=artist)
            report.title = "Artist: %s was not scrapped" % artist.artistName
            report.description = "Artist does not have an URL"
            report.event = UNKNOWN
            report.resolution = CKECK_URL
            report.save()
            count += 1

    youtube_log, created = DataLog.objects.get_or_create(created=today_date)
    if youtube_log.youtube_count:
        youtube_log_count = int(youtube_log.youtube_count)
        youtube_log_count += count
        youtube_log.youtube_count = youtube_log_count
    else:
        youtube_log.youtube_count = count
    youtube_log.save()
