from __future__ import unicode_literals

from django.db import models

from artist.models import Artist,Source


class YoutubeData(models.Model):
    artist = models.ForeignKey(Artist)
    publishedAt = models.DateTimeField(null=True, blank=True)
    commentCount = models.CharField(max_length=100, null=True, blank=True)
    videoCount = models.CharField(max_length=100, null=True, blank=True)
    viewCount = models.CharField(max_length=100, null=True, blank=True)
    subscriberCount = models.CharField(max_length=100, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return u'%s' % self.artist


class TwitterData(models.Model):
    artist = models.ForeignKey(Artist)
    followersCount = models.CharField(max_length=100, null=True, blank=True)
    friendsCount = models.CharField(max_length=100, null=True, blank=True)
    favouritesCount = models.CharField(max_length=100, null=True, blank=True)
    statusesCount = models.CharField(max_length=100, null=True, blank=True)
    listedCount = models.CharField(max_length=100, null=True, blank=True)
    retweetCount = models.CharField(max_length=100, null=True, blank=True)
    createdAt = models.DateTimeField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return u'%s' % self.artist


class FacebookData(models.Model):
    artist = models.ForeignKey(Artist)
    fanCount = models.CharField(max_length=100, null=True, blank=True)
    talkingAboutCount = models.CharField(max_length=100, null=True, blank=True)
    bio = models.TextField(null=True, blank=True)
    press_contact = models.TextField(null=True, blank=True)
    general_manager = models.TextField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified = models.DateTimeField(auto_now=True, null=True, blank=True)
    likes = models.PositiveIntegerField(default=0)

    def __str__(self):
        return u'%s' % self.artist


class SpotifyArtistGenre(models.Model):
    genre_name = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return u'%s' % self.genre_name


class SpotifyData(models.Model):
    artist = models.ForeignKey(Artist)
    followersCount = models.CharField(max_length=100, null=True, blank=True)
    popularityIndex = models.CharField(max_length=5, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified = models.DateTimeField(auto_now=True, null=True, blank=True)
    genre = models.ManyToManyField(SpotifyArtistGenre)

    def __str__(self):
        return u'%s' % self.artist


class SpotifyArtistImages(models.Model):
    spotify_data = models.ForeignKey(SpotifyData)
    artistPhoto = models.ImageField(upload_to='artistphotos/', blank=True, null=True)

    def __str__(self):
        return u'%s' % self.spotify_data.artist.artistName


class SpotifyArtistTopTracks(models.Model):
    spotify_data = models.ForeignKey(SpotifyData)
    track_name = models.TextField(null=True, blank=True)

    def __str__(self):
        return u'%s' % self.track_name


class FacebookLike(models.Model):
    artist = models.ForeignKey(Artist)
    country_name = models.CharField(max_length=100)
    country_code = models.CharField(max_length=10)
    like_count = models.IntegerField()
    created_at = models.DateField()

    def __str__(self):
        return u'%s' % self.artist


class YoutubeLike(models.Model):
    artist = models.ForeignKey(Artist)
    country_name = models.CharField(max_length=100)
    country_code = models.CharField(max_length=10)
    subscriber_count = models.IntegerField()
    created_at = models.DateField()

    def __str__(self):
        return u'%s' % self.artist


class InstagramData(models.Model):
    artist = models.ForeignKey(Artist)
    followed_by = models.CharField(max_length=100, null=True, blank=True)
    follows = models.CharField(max_length=100, null=True, blank=True)
    external_url = models.CharField(max_length=500)
    bio = models.TextField(null=True, blank=True)
    profile_pic = models.ImageField(upload_to='artistphotos/', blank=True, null=True)
    num_posts = models.CharField(max_length=100, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return u'%s' % self.artist


class DataLog(models.Model):
    active_artist_count = models.CharField(max_length=100, null=True, blank=True)
    inactive_artist_count = models.CharField(max_length=100, null=True, blank=True)
    inactive_artist_from_dbbuilder = models.CharField(max_length=100, null=True, blank=True)
    artist_with_gps_cord_count = models.CharField(max_length=100, null=True, blank=True)
    artist_aggregated = models.CharField(max_length=100, null=True, blank=True)

    events_aggregated_overall = models.CharField(max_length=100, null=True, blank=True)
    events_resident_advisor = models.CharField(max_length=100, null=True, blank=True)
    events_bandsin_town = models.CharField(max_length=100, null=True, blank=True)
    events_song_kick = models.CharField(max_length=100, null=True, blank=True)
    events_gigatools = models.CharField(max_length=100, null=True, blank=True)

    # sources_count = models.CharField(max_length=50, null=True, blank=True)
    # google_count = models.CharField(max_length=50, null=True, blank=True)
    total_news = models.CharField(max_length=100, null=True, blank=True)
    news_change_underground = models.CharField(max_length=100, null=True, blank=True)
    news_billboard= models.CharField(max_length=100, null=True, blank=True)
    news_dancing_astronaut = models.CharField(max_length=100, null=True, blank=True)
    news_datatransmission = models.CharField(max_length=100, null=True, blank=True)
    news_djmag = models.CharField(max_length=100, null=True, blank=True)
    news_djtimes = models.CharField(max_length=100, null=True, blank=True)
    news_fabriclondon = models.CharField(max_length=100, null=True, blank=True)
    news_hyperbot = models.CharField(max_length=100, null=True, blank=True)
    news_ibizaspotlight = models.CharField(max_length=100, null=True, blank=True)
    news_magneticmag = models.CharField(max_length=100, null=True, blank=True)
    news_mixmag = models.CharField(max_length=100, null=True, blank=True)
    news_mtv = models.CharField(max_length=100, null=True, blank=True)
    news_prnewsire = models.CharField(max_length=100, null=True, blank=True)
    news_pulse_radio = models.CharField(max_length=100, null=True, blank=True)
    news_record_of_the_day = models.CharField(max_length=100, null=True, blank=True)
    news_residentadvisor = models.CharField(max_length=100, null=True, blank=True)
    news_run_the_trap = models.CharField(max_length=100, null=True, blank=True)
    news_the_guardian = models.CharField(max_length=100, null=True, blank=True)
    news_the_times = models.CharField(max_length=100, null=True, blank=True)
    news_vice = models.CharField(max_length=100, null=True, blank=True)
    news_xlr8r = models.CharField(max_length=100, null=True, blank=True)
    news_youredm = models.CharField(max_length=100, null=True, blank=True)
    news_zerohedge = models.CharField(max_length=100, null=True, blank=True)

    spotify_count = models.CharField(max_length=100, null=True, blank=True)
    twitter_count = models.CharField(max_length=100, null=True, blank=True)
    facebook_count = models.CharField(max_length=100, null=True, blank=True)
    instagram_count = models.CharField(max_length=100, null=True, blank=True)
    youtube_count = models.CharField(max_length=100, null=True, blank=True)

    all_user_count = models.CharField(max_length=100, null=True, blank=True)
    admin_user_count = models.CharField(max_length=100, null=True, blank=True)
    promoter_user_count = models.CharField(max_length=100, null=True, blank=True)
    agent_user_count = models.CharField(max_length=100, null=True, blank=True)
    artist_user_count = models.CharField(max_length=100, null=True, blank=True)
    most_loged_in_user = models.CharField(max_length=100, null=True, blank=True)

    db_builder_artist_count = models.CharField(max_length=100, null=True, blank=True)
    db_builder_pending_artist_count =  models.CharField(max_length=100, null=True, blank=True)
    db_builder_development_artist_count = models.CharField(max_length=100, null=True, blank=True)

    created = models.DateField(null=True, blank=True)

    def __str__(self):
        return u'%s' % self.created


class Tops(models.Model):
    id = models.AutoField(primary_key=True, db_column='id')
    top_name = models.CharField(max_length=250)

    class Meta:
        db_table = 'tops'

    def __str__(self):
        return u'%s' % self.top_name


class ArtistTop(models.Model):
    id = models.AutoField(primary_key=True, db_column='id')
    top_id = models.ForeignKey(Tops, db_column='top_id')
    artistid = models.ForeignKey(Artist, db_column='artistID')  # Field name made lowercase.
    created = models.DateField(auto_now_add=True, blank=True, null=True)
    growth = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    number = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'artist_top'

    def __str__(self):
        return '%s - %s' % (self.artistid.artistName, self.top_id.top_name)


class TimeTrends(models.Model):
    id = models.AutoField(primary_key=True)
    top = models.ForeignKey(Tops)
    artist = models.ForeignKey(Artist)  # Field name made lowercase.
    created = models.DateField(auto_now_add=True, blank=True, null=True)
    modified = models.DateField(auto_now_add=True, blank=True, null=True)
    growth_1mth = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    number_1mth = models.IntegerField(blank=True, null=True)
    growth_3mth = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    number_3mth = models.IntegerField(blank=True, null=True)
    growth_6mth = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    number_6mth = models.IntegerField(blank=True, null=True)
    growth_1yr = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    number_1yr = models.IntegerField(blank=True, null=True)
    growth_1week = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    number_1week = models.IntegerField(blank=True, null=True)

    # Field for Artist Current Count
    current_count = models.BigIntegerField(default=0, blank=True, null=True)

    class Meta:
        db_table = 'artist_top_time'

    def __str__(self):
        return '%s - %s' % (self.artist.artistName, self.top.top_name)
