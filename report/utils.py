# Define some event types to be used on reporting

# UNKNOWN is used when not sure about the event
UNKNOWN = 0

# INFO is used when event has no impact on data integrity
# E.g: The artist has no upcoming events when try to scrape its profile
INFO = 1

# WARNING is used when event has no big impact on data validity
# E.g: The venue address could not be geocoded
WARNING = 2

# Error is used when event has big impact on data validity/integrity
# E.g: The artist event could not be retrieved or an error was raised
ERROR = 3


EVENT_TYPE = ((UNKNOWN, 'UNKNOWN'), (INFO, 'INFO'), (WARNING, 'WARNING'), (ERROR, 'ERROR'))

NA = 'NA'
CKECK_URL = 'Check URL if is valid and update\n'
INEXISTENT_USER = 'Artist is not existent on source\n'
DEBUG = 'Make some in details debugging\n'
NO_ACCESS = 'Access to URL is denied. Authentication might be necessary\n'
INTERNAL_SERVER_ERROR = "Internal server error\n"