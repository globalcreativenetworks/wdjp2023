from __future__ import unicode_literals

from django.db import models

from artist.models import (Artist, Source)
from utils import EVENT_TYPE


class ReportBandsintown(models.Model):
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE)
    source = models.ForeignKey(Source, on_delete=models.CASCADE)
    title = models.CharField(max_length=100, default='')
    event = models.IntegerField(default=-1, choices=EVENT_TYPE)
    description = models.CharField(max_length=255, default='')
    created = models.DateField(auto_now_add=True, blank=True, null=True)
    resolution = models.CharField(max_length=255, default='')

    def __str__(self):
        return self.description


class ReportResident(models.Model):
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE)
    title = models.CharField(max_length=100, default='')
    event = models.IntegerField(default=-1, choices=EVENT_TYPE)
    description = models.CharField(max_length=700, default='')
    created = models.DateField(auto_now_add=True, blank=True, null=True)
    resolution = models.CharField(max_length=255, default='')

    def __str__(self):
        return self.description


class ReportSongkick(models.Model):
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE)
    title = models.CharField(max_length=100, default='')
    event = models.IntegerField(default=-1, choices=EVENT_TYPE)
    description = models.CharField(max_length=700, default='')
    created = models.DateField(auto_now_add=True, blank=True, null=True)
    resolution = models.CharField(max_length=255, default='')

    def __str__(self):
        return self.description


class ReportGigatools(models.Model):
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE)
    title = models.CharField(max_length=100, default='')
    event = models.IntegerField(default=-1, choices=EVENT_TYPE)
    description = models.CharField(max_length=700, default='')
    created = models.DateField(auto_now_add=True, blank=True, null=True)
    resolution = models.CharField(max_length=255, default='')

    def __str__(self):
        return self.description


class ReportElectronicFestivals(models.Model):
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE)
    title = models.CharField(max_length=100, default='')
    event = models.IntegerField(default=-1, choices=EVENT_TYPE)
    description = models.CharField(max_length=700, default='')
    created = models.DateField(auto_now_add=True, blank=True, null=True)
    resolution = models.CharField(max_length=255, default='')

    def __str__(self):
        return self.description


class ReportSpotify(models.Model):
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE)
    title = models.CharField(max_length=100, default='')
    event = models.IntegerField(default=-1, choices=EVENT_TYPE)
    description = models.CharField(max_length=700, default='')
    created = models.DateField(auto_now_add=True, blank=True, null=True)
    resolution = models.CharField(max_length=255, default='')

    def __str__(self):
        return self.description


class ReportYoutube(models.Model):
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE)
    title = models.CharField(max_length=100, default='')
    event = models.IntegerField(default=-1, choices=EVENT_TYPE)
    description = models.CharField(max_length=700, default='')
    created = models.DateField(auto_now_add=True, blank=True, null=True)
    resolution = models.CharField(max_length=255, default='')

    def __str__(self):
        return self.description


class ReportTwitter(models.Model):
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE)
    title = models.CharField(max_length=100, default='')
    event = models.IntegerField(default=-1, choices=EVENT_TYPE)
    description = models.CharField(max_length=700, default='')
    created = models.DateField(auto_now_add=True, blank=True, null=True)
    resolution = models.CharField(max_length=255, default='')

    def __str__(self):
        return self.description


class ReportInstagram(models.Model):
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE)
    title = models.CharField(max_length=100, default='')
    event = models.IntegerField(default=-1, choices=EVENT_TYPE)
    description = models.CharField(max_length=700, default='')
    created = models.DateField(auto_now_add=True, blank=True, null=True)
    resolution = models.CharField(max_length=255, default='')

    def __str__(self):
        return self.description


class ReportAgregation(models.Model):
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE)
    source = models.ForeignKey(Source, on_delete=models.CASCADE)
    title = models.CharField(max_length=100, default='')
    event = models.IntegerField(default=-1, choices=EVENT_TYPE)
    description = models.CharField(max_length=255, default='')
    created = models.DateField(auto_now_add=True, blank=True, null=True)
    resolution = models.CharField(max_length=255, default='')

    def __str__(self):
        return self.description
