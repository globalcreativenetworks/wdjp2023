from django.template import Library

register = Library()


@register.filter
def as_percentage_of(part, whole):
    try:
        return "%.2f%%" % (float(part) / whole * 100)
    except (ValueError, ZeroDivisionError):
        return ""
