from django.conf.urls import url
from report import views

urlpatterns = [
   url(r'^$', views.reports, name="reports"),

   url(r'^(?P<artist_name>\s)/$', views.artist_report, name="artist_report"),

   url(r'^bandsintown/$', views.bandsintown, name="scrape_report_bands"),

   url(r'^resident/$', views.resident, name="scrape_report_resident"),

   url(r'^gigatools/$', views.gigatools, name="scrape_report_gigatools"),

   url(r'^spotify/$', views.spotify, name="scrape_report_spotify"),

   url(r'^songkick/$', views.songkick, name="scrape_report_songkick"),

   url(r'^instagram/$', views.instagram, name="scrape_report_instagram"),

   url(r'^facebook/$', views.facebook, name="scrape_report_facebook"),

   url(r'^twitter/$', views.twitter, name="scrape_report_twitter"),

   url(r'^youtube/$', views.youtube, name="scrape_report_youtube"),

   url(r'^soundcloud/$', views.soundcloud, name="scrape_report_soundcloud"),
]
