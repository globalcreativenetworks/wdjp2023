from django.shortcuts import render
import re
from django.contrib.admin.views.decorators import staff_member_required
from django.db.models import Q

from .utils import UNKNOWN, INFO, WARNING, ERROR
from .models import ReportBandsintown, ReportSpotify, ReportYoutube, ReportTwitter, ReportInstagram, ReportResident, \
    ReportSongkick, ReportGigatools
from events.models import DjParsedData
from artist.models import Source, Artist, ArtistWebsite
from scrapper.parsers.source import SourceType
# from .filters import ReportFilter
from datetime import date, datetime, timedelta
# from django.core.paginator import PageNotAnInteger, EmptyPage #, Paginator
from pure_pagination import Paginator, EmptyPage, PageNotAnInteger

today = date.today()
month_4 = today - timedelta(days=31 * 4)


def get_source_model(source=None):
    if source is not None:
        source = Source.objects.filter(sourceName=source)
    return source


def process_single_artist_report(request):
    context = dict()
    an = request.GET['artist_name']
    if an.endswith(" / Artist"):
        an = an[:-9]
    context['name'] = an
    artist = Artist.objects.filter(artistName=an)

    try:
        bit = ReportBandsintown.objects.filter(artist=artist)
        last_scrap_bit = bit.latest('created')

        source, created = Source.objects.get_or_create(sourceName="Bandsintown")
        artist_website = False
        try:
            artist_website = ArtistWebsite.objects.get(sourceID=source.sourceID, artistID=artist)
        except ArtistWebsite.DoesNotExist:
            artist_website = None
        except ArtistWebsite.MultipleObjectsReturned:
            artist_website = ArtistWebsite.objects.filter(sourceID=source.sourceID, artistID=artist.artistID)[0]
        if artist_website != False:
            context["bands_source_exist"] = True
            context["bands_created"] = last_scrap_bit.created
            if last_scrap_bit.event == INFO or last_scrap_bit.event == WARNING:
                context["bands_successful"] = True
            else:
                context["bands_successful"] = False
            res_cnt = ReportBandsintown.objects.filter(artist=artist, created=last_scrap_bit.created).count()
            context["bands_count"] = res_cnt
        else:
            context["bands_source_exist"] = False
            context["bands_created"] = False
            context["bands_successful"] = False
            context["bands_count"] = False
    except :
        context["bands_source_exist"] = False
        context["bands_created"] = False
        context["bands_successful"] = False
        context["bands_count"] = False
    # Resident
    try:
        res = ReportResident.objects.filter(artist=artist)
        if res.count() != 0:
            last_scrap_bit = res.latest('created')

            source, created = Source.objects.get_or_create(sourceName="Resident Adviser")
            artist_website = False
            try:
                artist_website = ArtistWebsite.objects.get(sourceID=source.sourceID, artistID=artist)
            except ArtistWebsite.DoesNotExist:
                artist_website = None
            except ArtistWebsite.MultipleObjectsReturned:
                artist_website = ArtistWebsite.objects.filter(sourceID=source.sourceID, artistID=artist.artistID)[0]
            if artist_website != False:
                context["resident_source_exist"] = True
                context["resident_created"] = last_scrap_bit.created
                if last_scrap_bit.event == INFO or last_scrap_bit.event == WARNING:
                    context["resident_successful"] = True
                    if last_scrap_bit.event == INFO:
                        res_cnt = last_scrap_bit.description
                        m = re.search("\d+", res_cnt)
                        res_cnt = m.group(0)
                        context["resident_count"] = res_cnt
                else:
                    context["resident_successful"] = False
                    context["resident_count"] = False
            else:
                context["resident_source_exist"] = False
                context["resident_created"] = False
                context["resident_successful"] = False
                context["resident_count"] = False
        else:
            context["resident_source_exist"] = False
            context["resident_created"] = False
            context["resident_successful"] = False
            context["resident_count"] = False
    except:
        context["resident_source_exist"] = False
        context["resident_created"] = False
        context["resident_successful"] = False
        context["resident_count"] = False

    # Songkick
    try:
        skick = ReportSongkick.objects.filter(artist=artist)
        if skick.count() != 0:
            last_scrap_bit = skick.latest('created')

            source, created = Source.objects.get_or_create(sourceName="Songkick")
            artist_website = False
            try:
                artist_website = ArtistWebsite.objects.get(sourceID=source.sourceID, artistID=artist)
            except ArtistWebsite.DoesNotExist:
                artist_website = None
            except ArtistWebsite.MultipleObjectsReturned:
                artist_website = ArtistWebsite.objects.filter(sourceID=source.sourceID, artistID=artist.artistID)[0]
            if artist_website != False:
                context["songkick_source_exist"] = True
                context["songkick_created"] = last_scrap_bit.created
                if last_scrap_bit.event == INFO or last_scrap_bit.event == WARNING:
                    context["songkick_successful"] = True
                    if last_scrap_bit.event == INFO:
                        res_cnt = last_scrap_bit.description
                        m = re.search("\d+", res_cnt)
                        res_cnt = m.group(0)
                        context["songkick_count"] = res_cnt
                else:
                    context["songkick_successful"] = False
                    context["songkick_count"] = False
            else:
                context["songkick_source_exist"] = False
                context["songkick_created"] = False
                context["songkick_successful"] = False
                context["songkick_count"] = False
        else:
            context["songkick_source_exist"] = False
            context["songkick_created"] = False
            context["songkick_successful"] = False
            context["songkick_count"] = False
    except:
        context["songkick_source_exist"] = False
        context["songkick_created"] = False
        context["songkick_successful"] = False
        context["songkick_count"] = False


    #gigatools
    try:
        giga = ReportGigatools.objects.filter(artist=artist)
        if giga.count() != 0:
            last_scrap_bit = giga.latest('created')

            source, created = Source.objects.get_or_create(sourceName="Gigatools")
            artist_website = False
            try:
                artist_website = ArtistWebsite.objects.get(sourceID=source.sourceID, artistID=artist)
            except ArtistWebsite.DoesNotExist:
                artist_website = None
            except ArtistWebsite.MultipleObjectsReturned:
                artist_website = ArtistWebsite.objects.filter(sourceID=source.sourceID, artistID=artist.artistID)[0]
            if artist_website != False:
                context["gigatools_source_exist"] = True
                context["gigatools_created"] = last_scrap_bit.created
                if last_scrap_bit.event == INFO or last_scrap_bit.event == WARNING:
                    context["gigatools_successful"] = True
                    if last_scrap_bit.event == INFO:
                        res_cnt = last_scrap_bit.description
                        m = re.search("\d+", res_cnt)
                        res_cnt = m.group(0)
                        context["gigatools_count"] = res_cnt
                else:
                    context["gigatools_successful"] = False
                    context["gigatools_count"] = False
            else:
                context["gigatools_source_exist"] = False
                context["gigatools_created"] = False
                context["gigatools_successful"] = False
                context["gigatools_count"] = False
        else:
            context["gigatools_source_exist"] = False
            context["gigatools_created"] = False
            context["gigatools_successful"] = False
            context["gigatools_count"] = False
    except:
        context["gigatools_source_exist"] = False
        context["gigatools_created"] = False
        context["gigatools_successful"] = False
        context["gigatools_count"] = False


    # Spotify
    try:
        skick = ReportSpotify.objects.filter(artist=artist)
        if skick.count() != 0:
            last_scrap_bit = skick.latest('created')

            source, created = Source.objects.get_or_create(sourceName="Spotify")
            artist_website = False
            try:
                artist_website = ArtistWebsite.objects.get(sourceID=source.sourceID, artistID=artist)
            except ArtistWebsite.DoesNotExist:
                artist_website = None
            except ArtistWebsite.MultipleObjectsReturned:
                artist_website = ArtistWebsite.objects.filter(sourceID=source.sourceID, artistID=artist.artistID)[0]
            if artist_website != False:
                context["spotify_source_exist"] = True
                context["spotify_created"] = last_scrap_bit.created
                if last_scrap_bit.event == INFO or last_scrap_bit.event == WARNING:
                    context["spotify_successful"] = True
                    if last_scrap_bit.event == INFO:
                        res_cnt = last_scrap_bit.description
                        m = re.search("\d+", res_cnt)
                        res_cnt = m.group(0)
                        context["spotify_count"] = res_cnt
                else:
                    context["spotify_successful"] = False
                    context["spotify_count"] = False
            else:
                context["spotify_source_exist"] = False
                context["spotify_created"] = False
                context["spotify_successful"] = False
                context["spotify_count"] = False
        else:
            context["spotify_source_exist"] = False
            context["spotify_created"] = False
            context["spotify_successful"] = False
            context["spotify_count"] = False
    except:
        context["spotify_source_exist"] = False
        context["spotify_created"] = False
        context["spotify_successful"] = False
        context["spotify_count"] = False

    # instagram
    try:
        skick = ReportInstagram.objects.filter(artist=artist)
        if skick.count() != 0:
            last_scrap_bit = skick.latest('created')

            source, created = Source.objects.get_or_create(sourceName="Instagram")
            artist_website = False
            try:
                artist_website = ArtistWebsite.objects.get(sourceID=source.sourceID, artistID=artist)
            except ArtistWebsite.DoesNotExist:
                artist_website = None
            except ArtistWebsite.MultipleObjectsReturned:
                artist_website = ArtistWebsite.objects.filter(sourceID=source.sourceID, artistID=artist.artistID)[0]
            if artist_website != False:
                context["instagram_source_exist"] = True
                context["instagram_created"] = last_scrap_bit.created
                if last_scrap_bit.event == INFO or last_scrap_bit.event == WARNING:
                    context["instagram_successful"] = True
                    if last_scrap_bit.event == INFO:
                        pass
                        # res_cnt = last_scrap_bit.description
                        # m = re.search("\d+", res_cnt)
                        # res_cnt = m.group(0)
                        # context["spotify_count"] = res_cnt

                else:
                    context["spotify_successful"] = False
                    # context["spotify_count"] = False
            else:
                context["instagram_source_exist"] = False
                context["instagram_created"] = False
                context["instagram_successful"] = False
                # context["spotify_count"] = False
        else:
            context["instagram_source_exist"] = False
            context["instagram_created"] = False
            context["instagram_successful"] = False
            context["instagram_count"] = False
    except:
        context["instagram_source_exist"] = False
        context["instagram_created"] = False
        context["instagram_successful"] = False
        context["instagram_count"] = False

    # twitter
    try:
        skick = ReportTwitter.objects.filter(artist=artist)
        if skick.count() != 0:
            last_scrap_bit = skick.latest('created')

            source, created = Source.objects.get_or_create(sourceName="Twitter")
            artist_website = False
            try:
                artist_website = ArtistWebsite.objects.get(sourceID=source.sourceID, artistID=artist)
            except ArtistWebsite.DoesNotExist:
                artist_website = None
            except ArtistWebsite.MultipleObjectsReturned:
                artist_website = ArtistWebsite.objects.filter(sourceID=source.sourceID, artistID=artist.artistID)[0]
            if artist_website != False:
                context["twitter_source_exist"] = True
                context["twitter_created"] = last_scrap_bit.created
                if last_scrap_bit.event == INFO or last_scrap_bit.event == WARNING:
                    context["twitter_successful"] = True
                    if last_scrap_bit.event == INFO:
                        pass
                        # res_cnt = last_scrap_bit.description
                        # m = re.search("\d+", res_cnt)
                        # res_cnt = m.group(0)
                        # context["spotify_count"] = res_cnt

                else:
                    context["twitter_successful"] = False
                    # context["spotify_count"] = False
            else:
                context["twitter_source_exist"] = False
                context["twitter_created"] = False
                context["twitter_successful"] = False
                # context["spotify_count"] = False
        else:
            context["twitter_source_exist"] = False
            context["twitter_created"] = False
            context["twitter_successful"] = False
            context["twitter_count"] = False
    except:
        context["twitter_source_exist"] = False
        context["twitter_created"] = False
        context["twitter_successful"] = False
        context["twitter_count"] = False

    try:
        skick = ReportYoutube.objects.filter(artist=artist)
        if skick.count() != 0:
            last_scrap_bit = skick.latest('created')

            source, created = Source.objects.get_or_create(sourceName="Youtube")
            artist_website = False
            try:
                artist_website = ArtistWebsite.objects.get(sourceID=source.sourceID, artistID=artist)
            except ArtistWebsite.DoesNotExist:
                artist_website = None
            except ArtistWebsite.MultipleObjectsReturned:
                artist_website = ArtistWebsite.objects.filter(sourceID=source.sourceID, artistID=artist.artistID)[0]
            if artist_website != False:
                context["youtube_source_exist"] = True
                context["youtube_created"] = last_scrap_bit.created
                if last_scrap_bit.event == INFO or last_scrap_bit.event == WARNING:
                    context["youtube_successful"] = True
                    if last_scrap_bit.event == INFO:
                        pass
                        # res_cnt = last_scrap_bit.description
                        # m = re.search("\d+", res_cnt)
                        # res_cnt = m.group(0)
                        # context["spotify_count"] = res_cnt

                else:
                    context["youtube_successful"] = False
                    # context["spotify_count"] = False
            else:
                context["youtube_source_exist"] = False
                context["youtube_created"] = False
                context["youtube_successful"] = False
                # context["spotify_count"] = False
        else:
            context["youtube_source_exist"] = False
            context["youtube_created"] = False
            context["youtube_successful"] = False
            context["youtube_count"] = False
    except:
        context["youtube_source_exist"] = False
        context["youtube_created"] = False
        context["youtube_successful"] = False
        context["youtube_count"] = False

    return render(request, "report/reports_artist.html", context)


@staff_member_required(login_url='login_user')
def reports(request):
    if not ("artist_name" in request.GET):
        events = DjParsedData.objects.all()
        context = dict()
        ev_no = list()
        source = get_source_model(SourceType.Bandsintown)
        ev_no.append(events.filter(sourceid=source).count())
        ev_no.append(events.filter(sourceid=get_source_model(SourceType.ResidentAdviser)).count())
        ev_no.append(events.filter(sourceid=get_source_model(SourceType.Gigatools)).count())
        ev_no.append(events.filter(sourceid=get_source_model(SourceType.Songkick)).count())

        # Scrape Artists
        scraped_artists = []

        report = ReportTwitter.objects.all()
        good = report.values_list('artist_id').order_by('created').distinct().filter(
            Q(event=INFO) | Q(event=WARNING)).count()
        scraped_artists.append(good)
        report = ReportYoutube.objects.all()
        good = report.values_list('artist_id').order_by('created').distinct().filter(
            Q(event=INFO) | Q(event=WARNING)).count()
        scraped_artists.append(good)
        report = ReportInstagram.objects.all()
        good = report.values_list('artist_id').order_by('created').distinct().filter(
            Q(event=INFO) | Q(event=WARNING)).count()
        scraped_artists.append(good)
        report = ReportSpotify.objects.all()
        good = report.values_list('artist_id').order_by('created').distinct().filter(
            Q(event=INFO) | Q(event=WARNING)).count()
        scraped_artists.append(good)
        # report = ReportSpotify.objects.all()
        # good = report.values_list('artist_id').order_by('created').distinct().filter(Q(event=INFO) | Q(event=WARNING)).count()
        # scraped_artists.append(good)

        # Add events time
        ev_time = []
        timestamp = None
        try:
            timestamp = ReportBandsintown.objects.latest('created').created
        except:
            timestamp = "None"
            pass
        ev_time.append(timestamp)

        timestamp = None
        try:
            timestamp = ReportResident.objects.latest('created').created
        except:
            timestamp = "None"
            pass
        ev_time.append(timestamp)

        timestamp = None
        try:
            timestamp = ReportBandsintown.objects.latest('created').created
        except:
            timestamp = "None"
            pass
        ev_time.append(timestamp)

        timestamp = None
        try:
            timestamp = ReportSongkick.objects.latest('created').created
        except:
            timestamp = "None"
            pass
        ev_time.append(timestamp)

        # Add events time
        ev_time_socials = []
        timestamp = None
        try:
            timestamp = ReportTwitter.objects.latest('created').created
        except:
            timestamp = "None"
            pass
        ev_time_socials.append(timestamp)

        timestamp = None
        try:
            timestamp = ReportYoutube.objects.latest('created').created
        except:
            timestamp = "None"
            pass
        ev_time_socials.append(timestamp)

        timestamp = None
        try:
            timestamp = ReportInstagram.objects.latest('created').created
        except:
            timestamp = "None"
            pass
        ev_time_socials.append(timestamp)

        timestamp = None
        try:
            timestamp = ReportSpotify.objects.latest('created').created
        except:
            timestamp = "None"
            pass
        ev_time_socials.append(timestamp)

        context['events'] = ev_no
        context['events_time'] = ev_time
        context['events_time_socials'] = ev_time_socials
        context['social_scraped'] = scraped_artists

        return render(request, "report/reports.html", context)

    else:
        return process_single_artist_report(request)


# @staff_member_required(login_url='login_user')
# def bandsintown(request):
#     report = ReportFilter(request.GET,
#                           queryset=ReportBandsintown.objects.all())
#
#     # paginator = Paginator(report, 5)
#     #
#     # page = request.GET.get('page')
#     # try:
#     #     reports = paginator.page(page)
#     # except PageNotAnInteger:
#     #     # If page is not an integer, deliver first page.
#     #     reports = paginator.page(1)
#     # except EmptyPage:
#     #     # If page is out of range (e.g. 9999), deliver last page of results.
#     #     reports = paginator.page(paginator.num_pages)
#     context = {
#         'reports': report,
#     }
#     return render(request, "report/bandsintown.html", context)


@staff_member_required(login_url='login_user')
def bandsintown(request):
    # get sourceID
    source = get_source_model(SourceType.Bandsintown)
    # get all artists count number
    all_artists = Artist.objects.all().count()
    # get all bandsintown artists
    bandsintown_artists = ArtistWebsite.objects.filter(sourceID=source)
    bandsintown_artists_total = bandsintown_artists.count()

    report = ReportBandsintown.objects.all()
    # last scrapping date
    last_scrap = report.latest('created')

    # Get all scrapping dates from the last 4 months
    enableddates = report.filter(created__gte=month_4).values('created').distinct()

    if request.GET.get('date'):
        date_current = datetime.strptime(request.GET.get('date'), '%d-%m-%Y')
        date_to_filter = date_current.strftime('%Y-%m-%d')
        report = report.filter(created=date_to_filter)
    else:
        date_current = date_to_filter = last_scrap.created
        report = report.filter(created=date_to_filter)
    # get artists successfully scrapped
    good = report.filter(Q(event=INFO) | Q(event=WARNING)).values_list('artist_id').distinct().count()
    # get artists not scrapped
    bad = report.filter(Q(event=ERROR) | Q(event=UNKNOWN)).values_list('artist_id').distinct().count()
    # artists with no account
    no_account = all_artists - bandsintown_artists_total

    # total events
    events = report.filter(Q(event=INFO) | Q(event=WARNING)).count()
    # geocoded events
    geocoded = report.filter(event=INFO).count()
    # not geocoded
    not_coded = report.filter(event=WARNING).count()
    # geocoded events
    unknown = report.filter(event=UNKNOWN).count()
    # not geocoded
    errors = report.filter(event=ERROR).count()

    logs = geocoded + not_coded + unknown + errors

    filter_type = int(request.GET.get('type', -1))
    if filter_type != -1:
        report = report.filter(event=filter_type)
    paginator = Paginator(report, 10, request=request)

    page = request.GET.get('page')
    try:
        reports = paginator.page(page)
    except (PageNotAnInteger, TypeError):
        # If page is not an integer, deliver first page.
        reports = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        reports = paginator.page(paginator.num_pages)

    context = {
        'reports': reports,
        'total': all_artists,
        'type_filter': filter_type,
        'artists': bandsintown_artists_total,
        'good': good,
        'bad': bad,
        'no_account': no_account,
        'events': events,
        'geocoded': geocoded,
        'not_coded': not_coded,
        'unknown': unknown,
        'errors': errors,
        'last_scrap': last_scrap,
        'source': source.get().pk,
        'enabledDates': enableddates,
        'logs': logs,
        'date_current': date_current,
    }
    return render(request, "report/bandsintown.html", context)


@staff_member_required(login_url='login_user')
def resident(request):
    # get sourceID
    source = get_source_model(SourceType.ResidentAdviser)
    # get all artists count number
    all_artists = Artist.objects.all().count()
    # get all bandsintown artists
    bandsintown_artists = ArtistWebsite.objects.filter(sourceID=source)
    bandsintown_artists_total = bandsintown_artists.count()

    report = ReportResident.objects.all()
    # last scrapping date
    last_scrap = 0
    if len(report) > 0:
        last_scrap = report.latest('created')

    # Get all scrapping dates from the last 4 months
    enableddates = report.filter(created__gte=month_4).values('created').distinct()

    if request.GET.get('date'):
        date_current = datetime.strptime(request.GET.get('date'), '%d-%m-%Y')
        date_to_filter = date_current.strftime('%Y-%m-%d')
        report = report.filter(created=date_to_filter)
    else:
        if len(report) > 0:
            date_current = date_to_filter = last_scrap.created
        else:
            date_current = date_to_filter = datetime.now()
        report = report.filter(created=date_to_filter)
    # get artists successfully scrapped
    good = report.filter(Q(event=INFO) | Q(event=WARNING)).values_list('artist_id').distinct().count()
    # get artists not scrapped
    bad = report.filter(Q(event=ERROR) | Q(event=UNKNOWN)).values_list('artist_id').distinct().count()
    # artists with no account
    no_account = all_artists - bandsintown_artists_total

    # total events
    events = report.filter(Q(event=INFO) | Q(event=WARNING)).count()
    # geocoded events
    geocoded = report.filter(event=INFO).count()
    # not geocoded
    not_coded = report.filter(event=WARNING).count()
    # geocoded events
    unknown = report.filter(event=UNKNOWN).count()
    # not geocoded
    errors = report.filter(event=ERROR).count()

    logs = geocoded + not_coded + unknown + errors

    filter_type = int(request.GET.get('type', -1))
    if filter_type != -1:
        report = report.filter(event=filter_type)
    paginator = Paginator(report, 10, request=request)

    page = request.GET.get('page')
    try:
        reports = paginator.page(page)
    except (PageNotAnInteger, TypeError):
        # If page is not an integer, deliver first page.
        reports = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        reports = paginator.page(paginator.num_pages)

    context = {
        'reports': reports,
        'total': all_artists,
        'type_filter': filter_type,
        'artists': bandsintown_artists_total,
        'good': good,
        'bad': bad,
        'no_account': no_account,
        'events': events,
        'geocoded': geocoded,
        'not_coded': not_coded,
        'unknown': unknown,
        'errors': errors,
        'last_scrap': last_scrap,
        'source': source.get().pk,
        'enabledDates': enableddates,
        'logs': logs,
        'date_current': date_current,
    }
    return render(request, "report/resident.html", context)


@staff_member_required(login_url='login_user')
def gigatools(request):
    # get sourceID
    source = get_source_model(SourceType.Gigatools)
    # get all artists count number
    all_artists = Artist.objects.all().count()
    # get all bandsintown artists
    bandsintown_artists = ArtistWebsite.objects.filter(sourceID=source)
    bandsintown_artists_total = bandsintown_artists.count()

    report = ReportGigatools.objects.all()
    # last scrapping date
    last_scrap = 0
    if len(report) > 0:
        last_scrap = report.latest('created')

    # Get all scrapping dates from the last 4 months
    enableddates = report.filter(created__gte=month_4).values('created').distinct()

    if request.GET.get('date'):
        date_current = datetime.strptime(request.GET.get('date'), '%d-%m-%Y')
        date_to_filter = date_current.strftime('%Y-%m-%d')
        report = report.filter(created=date_to_filter)
    else:
        if len(report) > 0:
            date_current = date_to_filter = last_scrap.created
        else:
            date_current = date_to_filter = datetime.now()
        report = report.filter(created=date_to_filter)
    # get artists successfully scrapped
    good = report.filter(Q(event=INFO) | Q(event=WARNING)).values_list('artist_id').distinct().count()
    # get artists not scrapped
    bad = report.filter(Q(event=ERROR) | Q(event=UNKNOWN)).values_list('artist_id').distinct().count()
    # artists with no account
    no_account = all_artists - bandsintown_artists_total

    # total events
    events = report.filter(Q(event=INFO) | Q(event=WARNING)).count()
    # geocoded events
    geocoded = report.filter(event=INFO).count()
    # not geocoded
    not_coded = report.filter(event=WARNING).count()
    # geocoded events
    unknown = report.filter(event=UNKNOWN).count()
    # not geocoded
    errors = report.filter(event=ERROR).count()

    logs = geocoded + not_coded + unknown + errors

    filter_type = int(request.GET.get('type', -1))
    if filter_type != -1:
        report = report.filter(event=filter_type)
    paginator = Paginator(report, 10, request=request)

    page = request.GET.get('page')
    try:
        reports = paginator.page(page)
    except (PageNotAnInteger, TypeError):
        # If page is not an integer, deliver first page.
        reports = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        reports = paginator.page(paginator.num_pages)

    context = {
        'reports': reports,
        'total': all_artists,
        'type_filter': filter_type,
        'artists': bandsintown_artists_total,
        'good': good,
        'bad': bad,
        'no_account': no_account,
        'events': events,
        'geocoded': geocoded,
        'not_coded': not_coded,
        'unknown': unknown,
        'errors': errors,
        'last_scrap': last_scrap,
        'source': source.get().pk,
        'enabledDates': enableddates,
        'logs': logs,
        'date_current': date_current,
    }
    return render(request, "report/gigatools.html", context)


# @staff_member_required(login_url='login_user')
# def spotify(request):
#     reports = ReportSpotify.objects.all()
#     # get all artists count number
#     all_artists = Artist.objects.all().count()
#     spotify_artists = ArtistWebsite.objects.filter(sourceID=get_source_model(SourceType.Spotify)).count()
#     good = reports.filter(Q(event=INFO) | Q(event=WARNING)).values_list('artist_id').distinct().count()
#     # get artists not scrapped
#     bad = reports.filter(Q(event=ERROR) | Q(event=UNKNOWN)).values_list('artist_id').distinct().count()
#     # artists with no account
#     no_account = all_artists-spotify_artists
#     last_scrap = reports.order_by('-created').values('created')[0]
#
#     filter_type = int(request.GET.get('type', -1))
#     if filter_type != -1:
#         reports = reports.filter(event=filter_type)
#     paginator = Paginator(reports, 10, request=request)
#
#     page = request.GET.get('page')
#     try:
#         reports = paginator.page(page)
#     except (PageNotAnInteger, TypeError):
#         # If page is not an integer, deliver first page.
#         reports = paginator.page(1)
#     except EmptyPage:
#         # If page is out of range (e.g. 9999), deliver last page of results.
#         reports = paginator.page(paginator.num_pages)
#     context = {
#         'reports': reports,
#         'type_filter': filter_type,
#
#     }
#     return render(request, "report/spotify.html", context)


@staff_member_required(login_url='login_user')
def spotify(request):
    # get all artists count number
    all_artists = Artist.objects.all().count()
    # get all bandsintown artists
    spotify_artists = ArtistWebsite.objects.filter(sourceID=get_source_model(SourceType.Spotify)).count()
    # last scrapping date

    report = ReportSpotify.objects.all()
    last_scrap = 0
    if len(report) > 0:
        last_scrap = report.latest('created')

    if request.GET.get('date'):
        date_current = datetime.strptime(request.GET.get('date'), '%d-%m-%Y')
        date_to_filter = date_current.strftime('%Y-%m-%d')
        report = report.filter(created=date_to_filter)
    else:
        if len(report) > 0:
            date_current = date_to_filter = last_scrap.created
        else:
            date_current = date_to_filter = datetime.now()
        report = report.filter(created=date_to_filter)

    # get artists successfully scrapped
    good = report.values_list('artist_id').order_by('created').distinct().filter(
        Q(event=INFO) | Q(event=WARNING)).count()

    # get artists not scrapped
    bad = report.filter(Q(event=ERROR) | Q(event=UNKNOWN)).values_list('artist_id').distinct().count()
    # artists with no account

    # total events
    events = report.filter(Q(event=INFO) | Q(event=WARNING)).count()
    # geocoded events
    info = report.filter(event=INFO).count()
    # not geocoded
    not_coded = report.filter(event=WARNING).count()
    # geocoded events
    unknown = report.filter(event=UNKNOWN).count()
    # not geocoded
    errors = report.filter(event=ERROR).count()

    no_account = unknown
    filter_type = int(request.GET.get('type', -1))
    if filter_type != -1:
        report = report.filter(event=filter_type)
    paginator = Paginator(report, 10, request=request)

    page = request.GET.get('page')
    try:
        reports = paginator.page(page)
    except (PageNotAnInteger, TypeError):
        # If page is not an integer, deliver first page.
        reports = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        reports = paginator.page(paginator.num_pages)

    context = {
        'reports': reports,
        'total': all_artists,
        'type_filter': filter_type,
        'artists': spotify_artists,
        'good': good,
        'bad': bad,
        'no_account': no_account,
        'events': events,
        'info': info,
        'not_coded': not_coded,
        'unknown': unknown,
        'errors': errors,
        'last_scrap': last_scrap,
        'date_current': date_current,
    }
    return render(request, "report/spotify.html", context)


@staff_member_required(login_url='login_user')
def songkick(request):
    # get sourceID
    source = get_source_model(SourceType.Songkick)
    # get all artists count number
    all_artists = Artist.objects.all().count()
    # get all bandsintown artists
    bandsintown_artists = ArtistWebsite.objects.filter(sourceID=source)
    bandsintown_artists_total = bandsintown_artists.count()

    report = ReportSongkick.objects.all()
    # last scrapping date
    last_scrap = 0
    if len(report) > 0:
        last_scrap = report.latest('created')

    # Get all scrapping dates from the last 4 months
    enableddates = report.filter(created__gte=month_4).values('created').distinct()

    if request.GET.get('date'):
        date_current = datetime.strptime(request.GET.get('date'), '%d-%m-%Y')
        date_to_filter = date_current.strftime('%Y-%m-%d')
        report = report.filter(created=date_to_filter)
    else:
        if len(report) > 0:
            date_current = date_to_filter = last_scrap.created
        else:
            date_current = date_to_filter = datetime.now()
        report = report.filter(created=date_to_filter)
    # get artists successfully scrapped
    good = report.filter(Q(event=INFO) | Q(event=WARNING)).values_list('artist_id').distinct().count()
    # get artists not scrapped
    bad = report.filter(Q(event=ERROR) | Q(event=UNKNOWN)).values_list('artist_id').distinct().count()
    # artists with no account
    no_account = all_artists - bandsintown_artists_total

    # total events
    events = report.filter(Q(event=INFO) | Q(event=WARNING)).count()
    # geocoded events
    geocoded = report.filter(event=INFO).count()
    # not geocoded
    not_coded = report.filter(event=WARNING).count()
    # geocoded events
    unknown = report.filter(event=UNKNOWN).count()
    # not geocoded
    errors = report.filter(event=ERROR).count()

    logs = geocoded + not_coded + unknown + errors

    filter_type = int(request.GET.get('type', -1))
    if filter_type != -1:
        report = report.filter(event=filter_type)
    paginator = Paginator(report, 10, request=request)

    page = request.GET.get('page')
    try:
        reports = paginator.page(page)
    except (PageNotAnInteger, TypeError):
        # If page is not an integer, deliver first page.
        reports = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        reports = paginator.page(paginator.num_pages)

    context = {
        'reports': reports,
        'total': all_artists,
        'type_filter': filter_type,
        'artists': bandsintown_artists_total,
        'good': good,
        'bad': bad,
        'no_account': no_account,
        'events': events,
        'geocoded': geocoded,
        'not_coded': not_coded,
        'unknown': unknown,
        'errors': errors,
        'last_scrap': last_scrap,
        'source': source.get().pk,
        'enabledDates': enableddates,
        'logs': logs,
        'date_current': date_current,
    }
    return render(request, "report/songkick.html", context)


@staff_member_required(login_url='login_user')
def instagram(request):
    # get sourceID
    source = get_source_model(SourceType.Instagram)
    # get all artists count number
    all_artists = Artist.objects.all().count()
    # get all bandsintown artists
    bandsintown_artists = ArtistWebsite.objects.filter(sourceID=source)
    bandsintown_artists_total = bandsintown_artists.count()

    report = ReportInstagram.objects.all()
    # last scrapping date
    last_scrap = 0
    if len(report) > 0:
        last_scrap = report.latest('created')

    # Get all scrapping dates from the last 4 months
    enableddates = report.filter(created__gte=month_4).values('created').distinct()

    if request.GET.get('date'):
        date_current = datetime.strptime(request.GET.get('date'), '%d-%m-%Y')
        date_to_filter = date_current.strftime('%Y-%m-%d')
        report = report.filter(created=date_to_filter)
    else:
        if len(report) > 0:
            date_current = date_to_filter = last_scrap.created
        else:
            date_current = date_to_filter = datetime.now()
        report = report.filter(created=date_to_filter)
    # get artists successfully scrapped
    good = report.filter(Q(event=INFO) | Q(event=WARNING)).values_list('artist_id').distinct().count()
    # get artists not scrapped
    bad = report.filter(Q(event=ERROR) | Q(event=UNKNOWN)).values_list('artist_id').distinct().count()
    # artists with no account
    no_account = all_artists - bandsintown_artists_total

    # total events
    events = report.filter(Q(event=INFO) | Q(event=WARNING)).count()
    # geocoded events
    geocoded = report.filter(event=INFO).count()
    # not geocoded
    not_coded = report.filter(event=WARNING).count()
    # geocoded events
    unknown = report.filter(event=UNKNOWN).count()
    # not geocoded
    errors = report.filter(event=ERROR).count()

    logs = geocoded + not_coded + unknown + errors

    filter_type = int(request.GET.get('type', -1))
    if filter_type != -1:
        report = report.filter(event=filter_type)
    paginator = Paginator(report, 10, request=request)

    page = request.GET.get('page')
    try:
        reports = paginator.page(page)
    except (PageNotAnInteger, TypeError):
        # If page is not an integer, deliver first page.
        reports = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        reports = paginator.page(paginator.num_pages)

    context = {
        'reports': reports,
        'total': all_artists,
        'type_filter': filter_type,
        'artists': bandsintown_artists_total,
        'good': good,
        'bad': bad,
        'no_account': no_account,
        'events': events,
        'geocoded': geocoded,
        'not_coded': not_coded,
        'unknown': unknown,
        'errors': errors,
        'last_scrap': last_scrap,
        'source': source.get().pk,
        'enabledDates': enableddates,
        'logs': logs,
        'date_current': date_current,
    }
    return render(request, "report/instagram.html", context)


@staff_member_required(login_url='login_user')
def facebook(request):
    context = dict()
    return render(request, "report/facebook.html", context)


@staff_member_required(login_url='login_user')
def twitter(request):
    # get sourceID
    source = get_source_model(SourceType.Twitter)
    # get all artists count number
    all_artists = Artist.objects.all().count()
    # get all bandsintown artists
    bandsintown_artists = ArtistWebsite.objects.filter(sourceID=source)
    bandsintown_artists_total = bandsintown_artists.count()

    report = ReportTwitter.objects.all()
    # last scrapping date
    last_scrap = 0
    if len(report) > 0:
        last_scrap = report.latest('created')

    # Get all scrapping dates from the last 4 months
    enableddates = report.filter(created__gte=month_4).values('created').distinct()

    if request.GET.get('date'):
        date_current = datetime.strptime(request.GET.get('date'), '%d-%m-%Y')
        date_to_filter = date_current.strftime('%Y-%m-%d')
        report = report.filter(created=date_to_filter)
    else:
        if len(report) > 0:
            date_current = date_to_filter = last_scrap.created
        else:
            date_current = date_to_filter = datetime.now()
        report = report.filter(created=date_to_filter)
    # get artists successfully scrapped
    good = report.filter(Q(event=INFO) | Q(event=WARNING)).values_list('artist_id').distinct().count()
    # get artists not scrapped
    bad = report.filter(Q(event=ERROR) | Q(event=UNKNOWN)).values_list('artist_id').distinct().count()
    # artists with no account
    no_account = all_artists - bandsintown_artists_total

    # total events
    events = report.filter(Q(event=INFO) | Q(event=WARNING)).count()
    # geocoded events
    geocoded = report.filter(event=INFO).count()
    # not geocoded
    not_coded = report.filter(event=WARNING).count()
    # geocoded events
    unknown = report.filter(event=UNKNOWN).count()
    # not geocoded
    errors = report.filter(event=ERROR).count()

    logs = geocoded + not_coded + unknown + errors

    filter_type = int(request.GET.get('type', -1))
    if filter_type != -1:
        report = report.filter(event=filter_type)
    paginator = Paginator(report, 10, request=request)

    page = request.GET.get('page')
    try:
        reports = paginator.page(page)
    except (PageNotAnInteger, TypeError):
        # If page is not an integer, deliver first page.
        reports = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        reports = paginator.page(paginator.num_pages)

    context = {
        'reports': reports,
        'total': all_artists,
        'type_filter': filter_type,
        'artists': bandsintown_artists_total,
        'good': good,
        'bad': bad,
        'no_account': no_account,
        'events': events,
        'geocoded': geocoded,
        'not_coded': not_coded,
        'unknown': unknown,
        'errors': errors,
        'last_scrap': last_scrap,
        'source': source.get().pk,
        'enabledDates': enableddates,
        'logs': logs,
        'date_current': date_current,
    }
    return render(request, "report/twitter.html", context)


@staff_member_required(login_url='login_user')
def youtube(request):
    # get sourceID
    source = get_source_model(SourceType.Youtube)
    # get all artists count number
    all_artists = Artist.objects.all().count()
    # get all bandsintown artists
    bandsintown_artists = ArtistWebsite.objects.filter(sourceID=source)
    bandsintown_artists_total = bandsintown_artists.count()

    report = ReportYoutube.objects.all()
    # last scrapping date
    last_scrap = 0
    if len(report) > 0:
        last_scrap = report.latest('created')

    # Get all scrapping dates from the last 4 months
    enableddates = report.filter(created__gte=month_4).values('created').distinct()

    if request.GET.get('date'):
        date_current = datetime.strptime(request.GET.get('date'), '%d-%m-%Y')
        date_to_filter = date_current.strftime('%Y-%m-%d')
        report = report.filter(created=date_to_filter)
    else:
        if len(report) > 0:
            date_current = date_to_filter = last_scrap.created
        else:
            date_current = date_to_filter = datetime.now()
        report = report.filter(created=date_to_filter)
    # get artists successfully scrapped
    good = report.filter(Q(event=INFO) | Q(event=WARNING)).values_list('artist_id').distinct().count()
    # get artists not scrapped
    bad = report.filter(Q(event=ERROR) | Q(event=UNKNOWN)).values_list('artist_id').distinct().count()
    # artists with no account
    no_account = all_artists - bandsintown_artists_total

    # total events
    events = report.filter(Q(event=INFO) | Q(event=WARNING)).count()
    # geocoded events
    geocoded = report.filter(event=INFO).count()
    # not geocoded
    not_coded = report.filter(event=WARNING).count()
    # geocoded events
    unknown = report.filter(event=UNKNOWN).count()
    # not geocoded
    errors = report.filter(event=ERROR).count()

    logs = geocoded + not_coded + unknown + errors

    filter_type = int(request.GET.get('type', -1))
    if filter_type != -1:
        report = report.filter(event=filter_type)
    paginator = Paginator(report, 10, request=request)

    page = request.GET.get('page')
    try:
        reports = paginator.page(page)
    except (PageNotAnInteger, TypeError):
        # If page is not an integer, deliver first page.
        reports = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        reports = paginator.page(paginator.num_pages)

    context = {
        'reports': reports,
        'total': all_artists,
        'type_filter': filter_type,
        'artists': bandsintown_artists_total,
        'good': good,
        'bad': bad,
        'no_account': no_account,
        'events': events,
        'geocoded': geocoded,
        'not_coded': not_coded,
        'unknown': unknown,
        'errors': errors,
        'last_scrap': last_scrap,
        'source': source.get().pk,
        'enabledDates': enableddates,
        'logs': logs,
        'date_current': date_current,
    }
    return render(request, "report/youtube.html", context)


@staff_member_required(login_url='login_user')
def soundcloud(request):
    context = dict()
    return render(request, "report/soundcloud.html", context)


def artist_report(request, artist_name):
    print (112)
    return None
