import django_filters
from .models import ReportBandsintown
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage


class ReportFilter(django_filters.FilterSet):
    description = django_filters.CharFilter(lookup_expr='icontains')
    created = django_filters.NumberFilter(lookup_expr='year')
    resolution = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = ReportBandsintown
        fields = {
            'artist',
            'source',
            'event',
            'title',
        }

