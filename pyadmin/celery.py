from __future__ import absolute_import

import os
from celery import Celery
from django.conf import settings

#: Set default configuration module name
os.environ.setdefault('CELERY_CONFIG_MODULE', 'pyadmin.settings')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'pyadmin.settings')
app = Celery('pyadmin_celery')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))

