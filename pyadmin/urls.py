"""pyadmin URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from rest_framework import routers

from artist.views import ArtistViewSet, GenreViewSet, source_list

router = routers.DefaultRouter()
router.register(r'genre', GenreViewSet)
router.register(r'artist', ArtistViewSet)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'',  include('home.urls')),  # home page index
    url(r'^artists/', include('artist.urls')),
    url(r'^accounts/', include('accounts.urls')),
    url(r'^sources/$', source_list),
    # url(r'^api/', include(router.urls)),
    # url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^events/', include('events.urls')),
    url(r'^availabilty-search/', include('dates.urls')),
    url(r'^territory/', include('geography.urls')),
    url(r'^profiles/', include('profiles.urls')),
    url(r'^news/', include('news.urls')),
    url(r'^dashboard/', include('dashboard.urls')),
    url(r'^social-data/', include('social_data.urls')),
    url(r'', include('marketing_pages.urls')),
    url(r'^music-river/', include('music_river.urls')),
    url(r'^reports/', include('report.urls')),

    # url(r'^social_auth_login/([a-z]+)$',  social_auth_login, name='users-social-auth-login'),

    # url('', include('social.apps.django_app.urls', namespace='social')),
    # url('', include('django.contrib.auth.urls', namespace='auth')),

    url('', include('social_django.urls', namespace='social')),
    url(r'^redactor/', include('redactor.urls')),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^hijack/', include('hijack.urls', namespace='hijack')),
]

#handler404 = "accounts.views.custom_page_not_found"

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT, show_indexes=True)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATICFILES_DIRS, show_indexes=True)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT, show_indexes=True)


admin.site.site_header = 'Wheredjsplay Administration Board'
