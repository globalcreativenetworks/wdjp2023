"""
Django settings for pyadmin project.

Generated by 'django-admin startproject' using Django 1.9.6.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.9/ref/settings/
"""

import os
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.9/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'oqz3w@43(jx8o(09do!s!_nf1e8m%ot(h-3u=ec*ie@wr$ocy0'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

SITE_URL = 'https://www.wheredjsplay.com'

DB_BUILDER_SITE_URL = 'https://www.wheredjsplay.com:8888'

# DB_BUILDER_SITE_URL = 'http://ec2-34-211-72-14.us-west-2.compute.amazonaws.com'

LOGIN_REDIRECT_URL='/'

ALLOWED_HOSTS = ['*']


DEFAULT_FROM_EMAIL_NAME = 'djs@wheredjsplay.com'
DEFAULT_FROM_EMAIL = 'Wheredjsplay <djs@wheredjsplay.com>'


TONI_EMAIL = "Toni <toni@wheredjsplay.com>"

EMAIL_USE_TLS = True
EMAIL_HOST = 'mail.wheredjsplay.com'
EMAIL_HOST_USER = 'admin@wheredjsplay.com'
EMAIL_HOST_PASSWORD = '39T46-FG.'
EMAIL_PORT = 25

# Application definition


DEFAULT_ADMIN_EMAILS = [
   "site-emails@wheredjsplay.com",
]

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.gis',
    'django.contrib.sites',
    'social_data',
    'djcelery',
    'django.contrib.humanize',
    'raven.contrib.django.raven_compat',

    # front-end applications
    'home',
    'events',
    'accounts',
    'dates',
    'rest_framework',
    'artist',
    'profiles',
    'dashboard',
    'geography',
    'scrape',
    'news',
    'marketing_pages',
    'music_river',
    'report',
    # 3rd Party packages
    'mailchimp',
    'django_user_agents',
    'social_django',
    'watson',
    'redactor',
    'ckeditor',
    'pdfkit',
    'rest_framework.authtoken',
    'hijack',
    'compat',
    'hijack_admin',
    'django_filters',
    'pure_pagination',

    # SSL Server for dev
    # 'sslserver',

    ]

PAGINATION_SETTINGS = {
    'PAGE_RANGE_DISPLAYED': 6,
    'MARGIN_PAGES_DISPLAYED': 2,

    'SHOW_FIRST_PAGE_WHEN_INVALID': True,
}

SITE_ID = 1

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': ('rest_framework.permissions.IsAdminUser',),
    'PAGE_SIZE': 10,
    'DEFAULT_AUTHENTICATION_CLASSES': ('rest_framework.authentication.TokenAuthentication',),
    # 'DEFAULT_RENDERER_CLASSES': ('rest_framework.renderers.JSONRenderer',),
    # 'DEFAULT_PARSER_CLASSES': ('rest_framework.parsers.JSONParser',),


}

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django_user_agents.middleware.UserAgentMiddleware',
    'profiles.middleware.UserLocationCookieMiddleware',
    'profiles.middleware.ErrorFormatterMiddleware',

    # 'profiles.middleware.CheckLoggedInUser',
    # 'social_auth.middleware.SocialAuthExceptionMiddleware',
    # 'accounts.views.social_auth_login'
]

ROOT_URLCONF = 'pyadmin.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'events.context_processors.header_events',
                # 'events.context_processors.is_premium',
                'events.context_processors.user_location',
                'events.context_processors.artist_of_the_day',
                'events.context_processors.get_footer_artist',
                'events.context_processors.return_settings',
                'events.context_processors.get_header_news',
                'events.context_processors.user_type',
                'events.context_processors.wdjp_news',
                'events.context_processors.my_favorite_venue',
                'events.context_processors.my_favorite_event',
                'events.context_processors.my_favorite_artist',
                'events.context_processors.my_account',
                'events.context_processors.return_env',
                'events.context_processors.my_managed_artists',
                'events.context_processors.my_artist_notes',
                'events.context_processors.my_event_notes',

                'social_django.context_processors.backends',
                'social_django.context_processors.login_redirect',
            ],
        },
    },
]

WSGI_APPLICATION = 'pyadmin.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases


DATABASES = {
    # 'default': {
    #            'ENGINE': 'django.db.backends.mysql',
    #            'NAME': 'wdjp_pro',
    #            'USER': 'root',
    #            'PASSWORD': 'lakshay',
    #            'HOST': 'localhost',
    #             # Or an IP Address that your DB is hosted on
    #             'PORT': '3306',
    #     }
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'wdjpdbprod_aws',
        'USER': 'app',
        'PASSWORD': 'wD5X@D8zkK4nHe',
        'HOST': 'dbi.globalcreativenetworks.com',
        # Or an IP Address that your DB is hosted on
        'PORT': '3306',
    }
    }

# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/


AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'social_core.backends.google.GoogleOAuth2',
    'accounts.backends.facebook.FacebookOAuth2',
)


SOCIAL_AUTH_URL_NAMESPACE = 'social'


SOCIAL_AUTH_PIPELINE = (
    'social_core.pipeline.social_auth.social_details',
    'social_core.pipeline.social_auth.social_uid',
    'accounts.views.account_already_in_use',
    'social_core.pipeline.social_auth.social_user',
    'social_core.pipeline.user.get_username',
    'social_core.pipeline.social_auth.associate_by_email',
    # 'social_core.pipeline.user.create_user',
    'social_core.pipeline.social_auth.associate_user',
    'accounts.views.check_user_from',
    'social_core.pipeline.social_auth.load_extra_data',
    'social_core.pipeline.user.user_details',
    'accounts.views.registered_users_only',

    # 'social_core.pipeline.social_auth.associate_by_email',
    # 'accounts.views.user_details_after',
    # 'accounts.views.social_auth_login'
)


SOCIAL_AUTH_LOGIN_REDIRECT_URL = 'social_auth_login_redirect'

# SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = "1001019792489-218nu9tcob05ienmj3k44un3bciu59ib.apps.googleusercontent.com"
# SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = "YGuN5JtbwcH0sMt8_pnM15S5"

SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = "802756896893-o7l1td4gdjp6fmunnl0kskl81luhopcr.apps.googleusercontent.com"
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = "hqI0m0LS1vHIc9UlM_gVIdEw"


# KEYS FOR RE-CAPCHA
RECAPTCHA_SITE_KEY = "6LeT_bIUAAAAAFsfEV-j04uXDriKcB3G7aF9n_tZ"
RECAPTCHA_SECRET_KEY = "6LeT_bIUAAAAAFj0P0FZ2zT9EoYJYJ07nnO6iG3l"


# SOCIAL_AUTH_FACEBOOK_KEY = "566523920192228"
# SOCIAL_AUTH_FACEBOOK_SECRET = "016f2636f5ba569fa5f2b83a58053e69"

# SOCIAL_AUTH_FACEBOOK_KEY = "308625749620608"
# SOCIAL_AUTH_FACEBOOK_SECRET = "aa9000179cce52b045b8cfcb57130a68"

SOCIAL_AUTH_FACEBOOK_KEY = "167732017487050"
SOCIAL_AUTH_FACEBOOK_SECRET = "144aa115f712cbc8d72da90881efd165"



LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True
LOGIN_URL = '/accounts/login-user/'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/

# STATIC_URL = '/static/'
# if not DEBUG:
#     STATIC_ROOT = os.path.join(BASE_DIR, 'static')
# else:
#     #C:\Users\vantu\Desktop\WDJP-final\pyadmin\pyadmin\static
#     #STATICFILES_DIRS = ('D:/TONY/ECLIPSEworkspace/pyadmin/pyadmin/static',)
#     STATICFILES_DIRS = [
#         os.path.join(BASE_DIR, "static"),
#     ]

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

MEDIA_URL = '/media/'  #put whatever you want that when url is rendered it will be /media/imagename.jpg

BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'
CELERY_SEND_EVENTS = True
CELERYBEAT_SCHEDULER = "djcelery.schedulers.DatabaseScheduler"

CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

import djcelery
djcelery.setup_loader()

CELERYBEAT_SCHEDULE = {
    'example-task': {
        'task': 'apps.users.tasks.just_print',
        'schedule': 10,  # in seconds, or timedelta(seconds=10)
    },
}

GEOIP_PATH = os.path.join(BASE_DIR, 'pyadmin/geo')
# social_data application keys

# YOUTUBE_API_KEY = "AIzaSyD4JhkWI5oWNIq9nJedyo113PlaWwgZtxo"

# YOUTUBE_API_KEY = "AIzaSyCnxHS44N53Qlnsd2f_xJcHv5X3OphBDmE"

YOUTUBE_API_KEY = "AIzaSyBAiBTV_mi_GJIHomb65Br6lwMJ1K4xIuk"


#test_app facebook api key
FACEBOOK_API_KEY = "211598386094027"
FACEBOOK_API_SECRET = "5a0242e26fb6af0ddf4e10198c8e5fb2"

#staging key
# FACEBOOK_API_KEY = "308625749620608"
# FACEBOOK_API_SECRET = "6b84f007988827d3e4723e7a84662fe0"

#old one
# FACEBOOK_API_KEY = "566523920192228"
# FACEBOOK_API_SECRET = "016f2636f5ba569fa5f2b83a58053e69"

TWITTER_CONSUMER_KEY = "mwnUFsOKtpcAWELAYhmhLzZZc"
TWITTER_CONSUMER_SECRET = "1MD4a5VMXVqLwehvtjePGlvg3gjGSof5jQlkhisD2FBqpLnT7h"
TWITTER_ACCESS_KEY = "1874761932-X16GeRd8dkBVs1oaSCZwy1eriycbOj7mH6JE7tF"
TWITTER_ACCESS_SECRET = "TfDRSpW5DRKhXa3oscelmsAuubxN8Hfm0480Ow8DKlf6T"

SPOTIFY_CONSUMER_KEY = "b18a61758d6a4401aaaaf12420daa9ad"
SPOTIFY_CONSUMER_SECRET = "306f2855ea51446987ef0e8091e7cbb8"

# Mailchimp API
MAILCHIMP_API_KEY = "6a450a6f4a104135e65cb60901957fd6-us14"

MAILCHIMP_PREMIUM_LIST = "0ceb04f8fc"
MAILCHIMP_BASIC_LIST = "79c9853f9c"
MAILCHIMP_FREE_LIST = "6aa000437c"
MAILCHIMP_BETA_LIST = "5fd9fe0822"

GOOGLE_CUSTOM_SEARCH_API_KEY = "AIzaSyCmEPHe8UrFGQYqRnaLG9c7mKPIpBSb7sQ"
GOOGLE_CUSTOM_SEARCH_API_ID = "012462734859606010397:9ojkbm0gb6o"
GOOGLE_PLACES_GEOCODE_KEY = 'AIzaSyA95184Inn61QQv0kLpPkDW1Fhi5SlcOJE'

STRIPE_SECRET_KEY = "sk_test_dds3jqmJXPAqw81G5kVw2wni"
STRIPE_PUBLISHABEL_KEY = "pk_test_M6BUt4ew8w98s2ymoUDtVFX8"


try:
    from WDJP_ENV import *
    if ENV_NAME == "PRODUCTION":
        from settings_prod import *
    elif ENV_NAME == "STAGING":
        from settings_staging import *
    elif ENV_NAME == "LOCAL":
        from settings_local import *
except:
    pass


STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfile')
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')


MEDIA_URL = '/media/'
AUTH_TOKENS = ['cjbhfbvjrbfhejfb']

# EMAIL_BACKEND='django.core.mail.backends.console.EmailBackend'
EMAIL_BACKEND= 'django.core.mail.backends.smtp.EmailBackend'

# Custom allauth settings
# Use email as the primary identifier
ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_EMAIL_REQUIRED = True
# Make email verification mandatory to avoid junk email accounts
ACCOUNT_EMAIL_VERIFICATION = 'none'
# Eliminate need to provide username, as it's a very old practice
ACCOUNT_USERNAME_REQUIRED = False


EMAIL_HOST = 'smtp.sendgrid.net'
EMAIL_HOST_USER = 'apikey'
EMAIL_HOST_PASSWORD = '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'
EMAIL_PORT = 587
EMAIL_USE_TLS = True

REDACTOR_UPLOAD = 'uploads/'

REDACTOR_OPTIONS = {
    'lang': 'en',
    'plugins': ['video', 'fontfamily', 'fontsize'],
}

CKEDITOR_JQUERY_URL = 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js'

CKEDITOR_UPLOAD_PATH = 'uploads/'
CKEDITOR_IMAGE_BACKEND = "pillow"

# CKEDITOR_CONFIGS = {
#     'default': {
#          'toolbar': [["Format", "Bold", "Italic", "Underline", "Strike", "SpellChecker"],
#                 ],
#     },
# }

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'Custom',
        'toolbar_Custom': [
            ['Bold', 'Italic', 'Underline'],
            ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['Link', 'Unlink'],
            ['RemoveFormat', 'Source']
        ]
    }
}


# HIJACK_LOGIN_REDIRECT_URL = '/profile/'  # Where admins are redirected to after hijacking a user
# HIJACK_LOGOUT_REDIRECT_URL = '/admin/auth/user/'  # Where admins are redirected to after releasing a user


HIJACK_LOGIN_REDIRECT_URL = '/'  # Where admins are redirected to after hijacking a user
HIJACK_LOGOUT_REDIRECT_URL = '/'  # Where admins are redirected to after releasing a user


# AUTH_USER_MODEL = 'accounts.User'


HIJACK_USE_BOOTSTRAP = True

HIJACK_BUTTON_TEMPLATE = 'hijack_admin/admin_button.html'

HIJACK_REGISTER_ADMIN = True

HIJACK_ALLOW_GET_REQUESTS = True

#if DEBUG is False:
#    SERVERNAME = "wdjpdbstaging2.czroneq4sqg9.us-west-2.rds.amazonaws.com"
#    DATABASENAME = "wdjpdbprod"
#    DATABASEUSERNAME = "wdjpuser"
#    DATABASEPASSWORD = "wdjp2017mainline"
#else:
SERVERNAME = DATABASES['default']['HOST']
DATABASENAME = DATABASES['default']['NAME']
DATABASEUSERNAME = DATABASES['default']['USER']
DATABASEPASSWORD = DATABASES['default']['PASSWORD']

FB_FILE_PATH = os.path.join(BASE_DIR, 'static/csv/fb-data.csv')