from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.

LOGIN_TYPE = (
    ('Normal Login', 'Normal Login'),
    ('Facebook Login', 'Facebook Login'),
    ('Gmail Login', "Gmail Login")
)


class MusicRiverLogin(models.Model):
	user = models.ForeignKey(User, null=True, blank=True)
	login_type = models.CharField(max_length=50, choices=LOGIN_TYPE)
	device_token = models.CharField(max_length=50, null=True, blank=True)
	device_type = models.CharField(max_length=50, null=True, blank=True)



class MusicRiverProfile(models.Model):
    user = models.ForeignKey(User, null=True, blank=True)
    is_email_validated = models.BooleanField(default=False)

    def __unicode__(self):
        return u'%s' % (self.user)


# class ForgotPasswordOtp(models.Model):
# 	user = models.ForeignKey(User, null=True, blank=True)
#     is_expired = models.BooleanField(default=False)
#     otp = models.CharField(max_length=100)
    




