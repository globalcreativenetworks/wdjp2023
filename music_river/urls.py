from django.conf.urls import url, include
from rest_framework import routers
from artist.views import GenreViewSet, source_list, ArtistViewSet
# from rest_framework.documentation import include_docs_urls

from .views import UserLoginAPIView, UserRegistrationAPIView, EmailValidateOTPView, ResendOtpView,\
				 ForgotPasswordOtpView, ChangePasswordView, ResetPasswordView, ArtistDetailAPIView,\
				 ArtistNearYouView, SocialLoginApiView, SocialRegistraionApiView, ChangePasswordOtpView


router = routers.DefaultRouter()


# router.register(r'genre', GenreViewSet)
# router.register(r'artist', ArtistViewSet)
# router.register(r'user', UserLoginAPIView,)



urlpatterns = [
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^login-api/$', UserLoginAPIView.as_view(), name='login'),
    url(r'^registration-api/$', UserRegistrationAPIView.as_view(), name="registration"),
    url(r'^email-validation-otp/$', EmailValidateOTPView.as_view(), name="email_validation"), #
    url(r'^resend-otp/$', ResendOtpView.as_view(), name="resend_otp"), #
    url(r'^forgot-password-otp/$', ForgotPasswordOtpView.as_view(), name="forgot_password_otp"), #
    url(r'^change-password-otp/$', ChangePasswordOtpView.as_view(), name="change_password_otp"), #
    url(r'^change-password/$', ChangePasswordView.as_view(), name="change_password_otp"), #
    url(r'^reset-password-api/$', ResetPasswordView.as_view(), name="reset_password"), #
    url(r'^artist/(?P<artistID>\d+)/$', ArtistDetailAPIView.as_view(), name='artist_detail_api'), #
    url(r'^artist-near-you/$', ArtistNearYouView.as_view(), name='artist_near_you'), #
    url(r'^social-login-api/$', SocialLoginApiView.as_view(), name='social_login_api'), #
    url(r'^social-registration-api/$', SocialRegistraionApiView.as_view(), name='social_registration_api'), #

    
    # url(r'^docs/', include_docs_urls(title='Music River API', permission_classes=[])),
    # url(r'^v1/register-api/', view=views.register_api.as_view() , name='register_api')),
    # url(r'^v1/transaksi/(?P<transaksi_id>\d+)/$', view=views.register_api.as_view() , name='register_api')),
]
