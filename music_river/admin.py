from django.contrib import admin
from music_river.models import MusicRiverProfile


class MusicProfileAdmin(admin.ModelAdmin):
	list_display = ['user']
	search_fields = ['user__email']	# search_fields = ['search_type']

admin.site.register(MusicRiverProfile, MusicProfileAdmin)

