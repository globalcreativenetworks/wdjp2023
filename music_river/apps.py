from django.apps import AppConfig


class MusicRiverConfig(AppConfig):
    name = 'music_river'
