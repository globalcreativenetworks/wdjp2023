from django.http import HttpResponse
from rest_framework.views import APIView
from .serializers import UserLoginSerializer
from rest_framework.generics import GenericAPIView, CreateAPIView, RetrieveAPIView, ListAPIView
from rest_framework import status
from rest_framework import viewsets, mixins
from django.contrib.auth.models import User

from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from django.utils.crypto import get_random_string
from social_django.models import UserSocialAuth


from accounts.utils import SendEmail

from profiles.models import Profile
from music_river.models import MusicRiverProfile
from accounts.models import EmailConfirmation, ForgetPassword, Applications, UserFrom
from artist.models import Artist


from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
    )

from .serializers import (
    UserLoginSerializer,
    UserRegistrationSerializer,
    EmailValidateOTPSerializer,
    ForgotPasswordOtpSerializer,
    ChangePasswordOtpSerializer,
    ResetPasswordOtpSerializer,
    ArtistDetailSerializer,
    ArtistNearYouSerializer,
    SocialLoginApiSerializer,
    SocialRegistrationSerializer,
    ResendOtpSerializer,
    ChangePasswordSerializer
    )

class UserLoginAPIView(GenericAPIView):
    authentication_classes = ()
    permission_classes = ()
    serializer_class = UserLoginSerializer

    def post(self, request, *args, **kwargs):
        response = {}
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            user = serializer.user

            token, created = Token.objects.get_or_create(user=user)

            response.update({
                'code': 'SUCCESS', 
                "message" : "Login Successfull",
            })

            response['data'] = {
                'token': token.key,
                'user_id': user.id,
                'first_name': user.first_name,
                'last_name': user.last_name,
                'username': user.username,
                'email': user.email
            }

            return Response(
                data=response,
                status=status.HTTP_200_OK,
            )
        else:
            response['status'] = {
                'code': "ERROR",
                'message': serializer.errors
            }

            return Response(
                data=response,
                status=status.HTTP_400_BAD_REQUEST,
            )


class UserRegistrationAPIView(CreateAPIView):
    authentication_classes = ()
    permission_classes = ()
    serializer_class = UserRegistrationSerializer

    def create(self, request, *args, **kwargs):
        response = {}
        data = request.data
        serializer = self.get_serializer(data=data)
        if serializer.is_valid():
            self.perform_create(serializer)
            user = serializer.instance

            token, created = Token.objects.get_or_create(user=user)
            profile, created = MusicRiverProfile.objects.get_or_create(user=user)
            email_confirmation = EmailConfirmation.objects.create(email=user.email.lower(), token=get_random_string(length=6), is_latest=True)

            msg = SendEmail(request)

            msg.send_by_template([user.email], "accounts/email_validated_otp.html",

            context={
                 'otp': email_confirmation.token
              },

              subject = "Email Validation OTP !"
            )


            response.update({
                'code': 'SUCCESS', 
                "message" : "Registration Successfull",
            })

            response['data'] = {
                'token': token.key,
                'porfile_id': profile.id,
                'user_id': user.id,
                'otp': email_confirmation.token
            }


            return Response(response, status=status.HTTP_201_CREATED)
        else:
            response['status'] = {
                'code': "ERROR",
                'message': serializer.errors
            }

            return Response(
                data=response,
                status=status.HTTP_400_BAD_REQUEST,
            )


class EmailValidateOTPView(GenericAPIView):
    authentication_classes = ( )
    permission_classes = ( )
    serializer_class = EmailValidateOTPSerializer


    def post(self, request, *args, **kwargs):
        response = {}
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():

            response.update({
                "code": "SUCCESS",
                "message": "Email Validated Successfully"
            })


            response['data'] = {
                'otp': serializer.otp
            }

            return Response(response, status=status.HTTP_201_CREATED)

        else:

            response['status'] = {
                'code': "ERROR",
                'message': serializer.errors
            }

            return Response(
                data=response,
                status=status.HTTP_400_BAD_REQUEST,
            )



class SocialLoginApiView(GenericAPIView):
    authentication_classes = ( )
    permission_classes = ( )
    serializer_class = SocialLoginApiSerializer


    def post(self, request, *args, **kwargs):
        response = {}
        serializer = self.get_serializer(data=request.data)


        if serializer.is_valid():

            social_id = serializer.social_id

            social_user_obj = UserSocialAuth.objects.filter(uid=social_id).first()

            if social_user_obj:

                user =  social_user_obj.user

                if user:
                    music_river_profile = MusicRiverProfile.objects.filter(user=user).first()

                    if music_river_profile:
                        if music_river_profile.is_email_validated :
                            token = Token.objects.get(user=user)

                            response.update({
                                    "token": token.key,
                                    "Authenticated": True
                                })

                        else:
                            response.update({
                                    "Authenticated": False
                                })

                # if user:
                #     token = Token.objects.get(user=user)

                #     response.update({
                #             "token": token.key
                #         })

                response.update({
                    "code":  "SUCCESS",
                    "message": "User Exist",
                })

            # social_id = serializer.social_id

            # social_user_obj = UserSocialAuth.objects.filter(uid=social_id).first()

            # user =  social_user_obj.user

            # if user:
            #     token = Token.objects.get(user=user)

            response["data"] = {
                'social_id': serializer.social_id,
            }

            return Response(response, status=status.HTTP_201_CREATED)

        else:

            # response.update({
            #         "Authenticated": False,
            #     })

            response['status'] = {
                'code': "ERROR",
                'message': serializer.errors,

            }

            return Response(
                data=response,
                status=status.HTTP_400_BAD_REQUEST,
            )


class SocialRegistraionApiView(GenericAPIView):
    authentication_classes = ( )
    permission_classes = ( )
    serializer_class = SocialRegistrationSerializer


    def post(self, request, *args, **kwargs):
        response = {}
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():

            # changes from here  -----------------------

            email = request.data["email"]

            user = User.objects.filter(email__iexact=email).first() 

            token, created = Token.objects.get_or_create(user=user)

            profile, created = MusicRiverProfile.objects.get_or_create(user=user)

            email_confirmation = EmailConfirmation.objects.create(email=user.email.lower(), token=get_random_string(length=6), is_latest=True)

            msg = SendEmail(request)

            msg.send_by_template([email], "accounts/email_validated_otp.html",

            context={
                 'otp': email_confirmation.token
              },

              subject = "Email Validation OTP !"
            )

            wdjp_profile = Profile.objects.filter(user=user)
            wdjp_profile.delete()

            # music_river_profile, create =  MusicRiverProfile.objects.get_or_create(user=user)

            application_obj = Applications.objects.filter(application_name="MusicRiver").first()

            if application_obj:
                user_from, created = UserFrom.objects.get_or_create(user=user)
                user_from.source = "MusicRiver"
                userfrom_all = user_from.user_from.all()

            for userfr in userfrom_all:
                userfr.delete()

            user_from.user_from.add(application_obj)
            user_from.save()

            response.update({
                "code":  "SUCCESS",
                "message": "Registration Successfull",
            })

            response["data"] = {
                'user_id': user.id,
                'token': token.key,
                'porfile_id': profile.id,
                'otp': email_confirmation.token
            }


            return Response(response, status=status.HTTP_201_CREATED)

        else:

            response['status'] = {
                'code': "ERROR",
                'message': serializer.errors
            }

            return Response(
                data=response,
                status=status.HTTP_400_BAD_REQUEST,
            )



class ResendOtpView(CreateAPIView):
    authentication_classes = ( )
    permission_classes = ( )
    serializer_class = ResendOtpSerializer



    def create(self, request, *args, **kwargs):

        response = {}
        data = request.data
        serializer = self.get_serializer(data=data)

        if serializer.is_valid():
            self.perform_create(serializer)

            email_confirmation_obj = serializer.instance

            msg = SendEmail(request)

            msg.send_by_template([email_confirmation_obj.email,], "accounts/resend_otp.html",

            context={
             'otp': email_confirmation_obj.token,
                 },

              subject = "Email Validation OTP !"
            )

            response.update({
                "code":  "SUCCESS",
                "message": "OTP Resend ",
            })

            response["data"] = {
                'email': email_confirmation_obj.email,
                'otp': email_confirmation_obj.token,
            }


        else:

            response['status'] = {
                'code': "ERROR",
                'message': serializer.errors
            }

            return Response(
                data = response,
                status=status.HTTP_400_BAD_REQUEST,
            )



        return Response(response, status=status.HTTP_201_CREATED)


class ForgotPasswordOtpView(CreateAPIView):
    authentication_classes = ( )
    permission_classes = ( )
    serializer_class = ForgotPasswordOtpSerializer

    def create(self, request, *args, **kwargs):

        response = {}
        data = request.data

        serializer = self.get_serializer(data=data)

        if serializer.is_valid():
            self.perform_create(serializer)
            user = serializer.instance

            if user.email:

                forgot_pass_obj = ForgetPassword.objects.filter(email__iexact=user.email, is_used=False)

                msg = SendEmail(request)

                msg.send_by_template([user.email], "accounts/forget_password_otp_email.html",

                context={
                     'otp': user.token
                  },
                  subject = "Reset Password!"
                )

            response.update({
                "code":  "SUCCESS",
                "message": "OTP Sent ",
            })

            response["data"] = {
                'otp': user.token
            }


            return Response(response, status=status.HTTP_201_CREATED)

        else:

            response['status'] = {
                'code': "ERROR",
                'message': serializer.errors
            }

            return Response(
                data=response,
                status=status.HTTP_400_BAD_REQUEST,
            )

class ChangePasswordOtpView(CreateAPIView):
    permission_classes = ()
    serializer_class = ChangePasswordOtpSerializer


    def post(self, request, *args, **kwargs):
        response = {}
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():

            response.update({
                "code":  "SUCCESS",
                "message": "Valid OTP ",
            })

            response["data"] = {
                'otp': request.data["otp"],
                'email': request.data["email"]
            }


            return Response(response, status=status.HTTP_201_CREATED)

        else:

            response['status'] = {
                'code': "ERROR",
                'message': serializer.errors
            }

            return Response(
                data=response,
                status=status.HTTP_400_BAD_REQUEST,
            )


    def create(self, request, *args, **kwargs):

        response = {}
        data = request.data

        serializer = self.get_serializer(data=data)

        if serializer.is_valid():
            # self.perform_create(serializer)
            user = serializer


            return Response(response, status=status.HTTP_201_CREATED)

        else:
            response['status'] = {
                'code': "ERROR",
                'message': serializer.errors
            }

            return Response(
                data=response,
                status=status.HTTP_400_BAD_REQUEST,
            )


class ChangePasswordView(CreateAPIView):

    permission_classes = ()
    serializer_class = ChangePasswordSerializer

    def create(self, request, *args, **kwargs):

        response = {}
        data = request.data

        serializer = self.get_serializer(data=data)

        if serializer.is_valid():
            self.perform_create(serializer)
            user = serializer.instance

            response.update({
                "code":  "SUCCESS",
                "message": "Password changed Successfully ",
            })

            return Response(response, status=status.HTTP_201_CREATED)

        else:
            response['status'] = {
                'code': "ERROR",
                'message': serializer.errors
            }

            return Response(
                data=response,
                status=status.HTTP_400_BAD_REQUEST,
            )


class ResetPasswordView(CreateAPIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = ResetPasswordOtpSerializer

    def create(self, request, *args, **Kwargs):
        response = {}
        data = request.data
        serializer = self.get_serializer(data=data)

        if serializer.is_valid():

            self.perform_create(serializer)

            user = serializer.instance

            response.update({
                "code":  "SUCCESS",
                "message": "Password Reset Successfully ",
            })

            return Response(response, status=status.HTTP_201_CREATED)

        else:

            response['status'] = {
                'code': "ERROR",
                'message': serializer.errors
            }
            
            return Response(
                data=response,
                status=status.HTTP_400_BAD_REQUEST,
            )


class ArtistDetailAPIView(RetrieveAPIView):

    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)
    queryset = Artist.objects.all()
    serializer_class = ArtistDetailSerializer
    lookup_field = 'artistID'

    def get(self, request, *args, **kwargs):
        if 'artistID' in kwargs and not Artist.objects.filter(artistID=kwargs.get('artistID')).exists():
            response = {
                'code': 404,
                'message': "not found"
            }
            return Response(response, status=404)
        else:
            return self.retrieve(request, *args, **kwargs)


class ArtistNearYouView(ListAPIView):
    permission_classes = (IsAuthenticated, )
    authentication_classes = (TokenAuthentication, )
    serializer_class = ArtistNearYouSerializer

    def get_queryset(self, *args, **kwargs):
        #queryset_list = super(PostListAPIView, self).get_queryset(*args, **kwargs)
        queryset_list = Artist.objects.all() #filter(user=self.request.user)
        latitude = self.request.GET.get("latitude")
        longitude = self.request.GET.get("longitude")

        if latitude and longitude :
            # queryset_list = queryset_list.filter(
            #         Q(title__icontains=query)|
            #         Q(content__icontains=query)|
            #         Q(user__first_name__icontains=query) |
            #         Q(user__last_name__icontains=query)
            #         ).distinct()

            queryset_list = queryset_list.filter(city_latitude=latitude, city_longitude=longitude)


        return queryset_list

