# from django.contrib.contentypes.models import ContentType
from django.contrib.auth import get_user_model
from django.db.models import Q
from rest_framework import serializers
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.utils.crypto import get_random_string


from accounts.utils import SendEmail, create_username

from accounts.models import EmailConfirmation, ForgetPassword, UserFrom, Applications
from music_river.models import MusicRiverLogin, MusicRiverProfile
from artist.models import Artist
from profiles.models import Profile


from django.utils.translation import ugettext_lazy as _


artist_detail_url = serializers.HyperlinkedIdentityField(
        view_name='artist_detail_api',
        lookup_field='artistID'
        )


from social_django.models import UserSocialAuth

User = get_user_model()


class UserLoginSerializer(serializers.Serializer):
    email = serializers.CharField(required=True)
    password = serializers.CharField(required=True)

    default_error_messages = {
        'inactive_account': _('User account is disabled.'),
        'invalid_credentials': _('Unable to login with provided credentials.'),
        'email_not_validated': _('Your Email is not validated'),
        'profile_not_found': _('You don\'t have an account.')
    }

    def __init__(self, *args, **kwargs):
        super(UserLoginSerializer, self).__init__(*args, **kwargs)
        self.user = None

    def validate(self, attrs):

        user_obj = User.objects.filter(email__iexact=attrs.get("email")).first()

        if user_obj:
            self.user = authenticate(username=user_obj.username, password=attrs.get('password'))

            if self.user:
                profile = MusicRiverProfile.objects.filter(user=user_obj).first()

                if not profile:
                    raise serializers.ValidationError(self.error_messages['profile_not_found'])

                if not profile.is_email_validated:
                    raise serializers.ValidationError(self.error_messages['email_not_validated']) 

                if not self.user.is_active:
                    raise serializers.ValidationError(self.error_messages['inactive_account'])

            else:
                raise serializers.ValidationError(self.error_messages['invalid_credentials'])

        else:
            raise serializers.ValidationError(self.error_messages['invalid_credentials'])

        return attrs



class ChangePasswordOtpSerializer(serializers.ModelSerializer):
    email = serializers.CharField(required=True)
    otp = serializers.CharField(required=True)


    class Meta:
        model = ForgetPassword
        fields = ('email', 'otp',)


    default_error_messages = {
        'invalid_otp': _('This is not the valid otp'),
        'expired_otp': _('This OTP is Expired'),
    }

    def validate(self, attrs):
        email = attrs.get("email")
        otp = attrs.get("otp")

        forget_password_obj = ForgetPassword.objects.filter(token=otp).first()

        # if password != confirm_password :
        #     raise serializers.ValidationError(self.error_messages['invalid_password']) 

        if otp and email:
            forget_password_obj = ForgetPassword.objects.filter(token=otp).first()

            if forget_password_obj and forget_password_obj.email.lower() == email.lower() :
                if forget_password_obj.is_used == False :
                    raise serializers.ValidationError(self.error_messages['expired_otp'])
                # else:
                #     return attrs

            else:
                raise serializers.ValidationError(self.error_messages['invalid_otp'])
                
        return attrs


class ChangePasswordSerializer(serializers.ModelSerializer):
    otp = serializers.CharField(required=True)
    password = serializers.CharField(required=True)
    confirm_password = serializers.CharField(required=True)


    class Meta:
        model = User
        fields = ('otp', 'password', 'confirm_password')


    def __init__(self, *args, **kwargs):
        super(ChangePasswordSerializer, self).__init__(*args, **kwargs)
        self.user = None

    default_error_messages = {
        'invalid_otp': _('This is not the valid otp'),
        'invalid_password': _('Password and Confirm Password are not matching'),
        'expired_otp': _('This OTP is Expired'),
    }

    def validate(self, attrs):
        password = attrs.get("password")
        confirm_password = attrs.get("confirm_password")
        otp = attrs.get("otp")

        forget_password_obj = ForgetPassword.objects.filter(token=otp).first()

        if password != confirm_password :
            raise serializers.ValidationError(self.error_messages['invalid_password']) 

        if otp:
            # forget_password_obj = ForgetPassword.objects.filter(token=otp)

            if forget_password_obj :
                if forget_password_obj.is_used == False :
                    raise serializers.ValidationError(self.error_messages['expired_otp'])
                # else:
                #     return attrs

            else:
                raise serializers.ValidationError(self.error_messages['invalid_otp'])

        return attrs

            
    def create(self, validated_data):
        forget_password_obj = ForgetPassword.objects.filter(token=validated_data["otp"]).first()

        if forget_password_obj:
            user = User.objects.filter(email__iexact=forget_password_obj.email).first()
            # user.password = validated_data["confirm_password"]
            user.set_password(validated_data["confirm_password"])
            user.save()

            forget_password_obj.is_used = False
            forget_password_obj.save()

            return user


class UserRegistrationSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=True)
    email = serializers.EmailField(required=True, min_length=8)
    password = serializers.CharField(required=True, max_length=15)
    # full_name = serializers.CharField(required=True)

    class Meta:
        model = User
        fields = ("id", "first_name", "last_name", "email", "password")

    def validate_email(self, email):

        if User.objects.filter(email__iexact=email).exists():
            raise serializers.ValidationError('User already exists')
        return email.lower()

    def create(self, validated_data):
        # user = User.objects.create_user(validated_data['email'], validated_data['password'],
        # first_name=validated_data['first_name'], last_name=validated_data['last_name'])

        user = User.objects.create_user(
            username=create_username('%s' % (validated_data["first_name"])),
            email=validated_data['email'],
            password=validated_data['password'],
            is_active = True,
        )

        profile = Profile.objects.filter(user=user)
        profile.delete()

        # music_river_profile, create =  MusicRiverProfile.objects.get_or_create(user=user)

        application_obj = Applications.objects.filter(application_name="MusicRiver").first()

        if application_obj:
            user_from, created = UserFrom.objects.get_or_create(user=user)
            user_from.source = "MusicRiver"
            userfrom_all = user_from.user_from.all()

            for userfr in userfrom_all:
                userfr.delete()

            user_from.user_from.add(application_obj)
            user_from.save()

        return user


class ForgotPasswordOtpSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required=True)

    class Meta:
        model = User
        fields = ('email',)

    default_error_messages = {
        'invalid_email': _('Please enter the valid email address.'),
    }


    def validate_email(self, email):
        user_obj = User.objects.filter(email__iexact=email).exists()
        
        if user_obj:
            return email
        else:
            raise serializers.ValidationError(self.error_messages['invalid_email'])

            
    def create(self, validated_data):

        forget_password_filter =  ForgetPassword.objects.filter(email__iexact=validated_data["email"])

        for pass_obj  in forget_password_filter:
            pass_obj.is_used =  False
            pass_obj.save()

        forgot_password_token_obj = ForgetPassword.objects.create(email=validated_data["email"], token=get_random_string(length=6))

        # send_email  

        return forgot_password_token_obj


class ResendOtpSerializer(serializers.ModelSerializer):

    email = serializers.CharField(required=True)

    class Meta:
        model = User
        fields = ('email',)


    default_error_messages = {
        'invalid_email':_('This email  does not exist'),
    }


    def validate_email(self, email):

        if email:
            user_obj = User.objects.filter(email__iexact=email).first()
    
            if not user_obj:
                raise serializers.ValidationError(self.error_messages['invalid_email'])

            return email


    def create(self, validated_data):

        email_confirmation_obj_filter = EmailConfirmation.objects.filter(email__iexact=validated_data["email"])

        for conf in email_confirmation_obj_filter:
            conf.is_latest = False
            conf.save()

        email_confirmation_obj = EmailConfirmation.objects.create(email=validated_data["email"].lower(), token=get_random_string(length=6), is_latest=True)


        return email_confirmation_obj



class ResetPasswordOtpSerializer(serializers.ModelSerializer):
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)
    confirm_password = serializers.CharField(required=True)

    class Meta:
        model = User
        fields = ('old_password', 'new_password', 'confirm_password')


    default_error_messages = {
        'invalid_password': _('New Password and Confirm Password should be same.'),
        'old_password_required':_('Please enter the old Password'),
        'old_password_incorrect':_('Old Password is Incorrect'),
    }

    def __init__(self, *args, **kwargs):
        super(ResetPasswordOtpSerializer, self).__init__(*args, **kwargs)

        try:
            self.user = kwargs["context"]["request"].user
        except:
            pass

    def validate(self, attrs):
        old_password = attrs.get("old_password")
        new_password = attrs.get("new_password")
        confirm_password = attrs.get("confirm_password")

        if self.user:

            check_password = self.user.check_password(old_password)

            if not check_password:
                raise serializers.ValidationError(self.error_messages['old_password_incorrect'])

            if new_password != confirm_password:
                raise serializers.ValidationError(self.error_messages['invalid_password'])

            if old_password == "":
                raise serializers.ValidationError(self.error_messages['old_password_required'])

            else:
                return attrs
            
    def create(self, validated_data):
        self.user.set_password(validated_data["confirm_password"])
        self.user.save()

        return self.user



class EmailValidateOTPSerializer(serializers.ModelSerializer):
    otp = serializers.CharField(required=True)
    email = serializers.CharField(required=True)

    class Meta:
        model = EmailConfirmation
        fields = ('otp', 'email')

    default_error_messages = {
        'expired_coupon': _('This Coupon is Expired.'),
        'invalid_token': _('This Token is not valid.')
    }

    def validate(self, attrs):

        self.otp = attrs.get("otp")
        self.email = attrs.get("email")

        email_confirmation_obj = EmailConfirmation.objects.filter(token=self.otp).first()

        if email_confirmation_obj :
            # if email_confirmation_obj.is_latest  and self.user.email == email_confirmation_obj.email:
            if email_confirmation_obj.is_latest and self.email.lower() == email_confirmation_obj.email.lower():
                email_confirmation_obj.is_latest = False
                email_confirmation_obj.save()

                user = User.objects.filter(email__iexact=email_confirmation_obj.email).first()

                music_river_profile = MusicRiverProfile.objects.filter(user=user).first()

                if music_river_profile:
                    music_river_profile.is_email_validated = True 
                    music_river_profile.save()

                return attrs

            else:
                raise serializers.ValidationError(self.error_messages['invalid_token'])
        else:
            raise serializers.ValidationError(self.error_messages['invalid_token'])



class SocialLoginApiSerializer(serializers.ModelSerializer):
    social_id = serializers.CharField(required=True, max_length=50)

    class Meta:
        model = UserSocialAuth
        fields = ('social_id',)

    default_error_messages = {
        'user_not_exist': _('This User not exist.'),
        'invalid_token': _('This Token is not valid.'),
        'social_id': _('This Social ID not exist.'),
    }


    def validate(self, attrs):

        self.social_id = attrs.get("social_id")

        social_user_obj = UserSocialAuth.objects.filter(uid=self.social_id).first()

        if social_user_obj:
            user_from = UserFrom.objects.filter(user=social_user_obj.user).first()

            application = Applications.objects.filter(application_name="MusicRiver").first()

            if application not in user_from.user_from.all():
                raise serializers.ValidationError(self.error_messages['user_not_exist'])

        else:
            raise serializers.ValidationError(self.error_messages['social_id'])


        return attrs


class SocialRegistrationSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=True)
    email = serializers.EmailField(required=True, min_length=8)
    password = serializers.CharField(required=True, max_length=15)
    social_id = serializers.CharField(required=False, max_length=50)

    class Meta:
        model = User
        fields = ("first_name", "last_name", "email", "password", "social_id", )

    default_error_messages = {
        'social_id_exist': _('This Social id already Exist.'),
        'email_id_exist': _('This Email id already Exist.'),
    }


    def validate(self, attrs):
        user_obj = User.objects.filter(email__iexact=attrs.get("email")).first()

        if not attrs.get("social_id") and user_obj:
            raise serializers.ValidationError(self.error_messages['email_id_exist'])

        if not user_obj:
            user_obj = User.objects.create_user(
                username=create_username('%s' % (attrs.get("first_name"))),
                email=attrs.get("email").lower(),
                password=attrs.get("password"),
                is_active = True,
                first_name = attrs.get("first_name"),
                last_name = attrs.get("last_name"),
            )

            music_river_profile =  MusicRiverProfile.objects.get_or_create(user=user_obj)

        if attrs.get("social_id"):
            user_social_auth_obj, created = UserSocialAuth.objects.get_or_create(user=user_obj, uid=attrs.get("social_id"))

            if not created:
                raise serializers.ValidationError(self.error_messages['social_id_exist'])

        return attrs


class ArtistDetailSerializer(serializers.ModelSerializer):
    url = artist_detail_url
    image = serializers.SerializerMethodField()
    response_status = serializers.SerializerMethodField()
    message = serializers.SerializerMethodField()
    genre = serializers.SerializerMethodField()


    # def __init__(self, *args, **kwargs):
    #     super(ArtistDetailSerializer, self).__init__(*args, **kwargs)

    #     try:
    #         self.user = kwargs["context"]["request"].user
    #     except:
    #         pass

    class Meta:
        model = Artist
        fields = [
            'url',
            'image',
            'artistID',
            'artistName',
            'artistWebsite',
            'artistPhoto',
            'contact_no',
            'artistAgent',
            'artistBudget',
            'status',
            'genre',
            'biography',
            'remarks',
            'registered',
            'city',
            'country',
            'from_builder',
            'builder_genre',
            'builder_keywords',
            'press_contact',
            'general_manager',
            'gender',
            'facebook_fancount',
            'twitter_fancount',
            'youtube_fancount',
            'spotify_fancount',
            'instagram_fancount',
            'address',
            'type_of_performance',
            'city_latitude',
            'city_longitude',
            'response_status',
            # 'code',
            # 'status'
            "message",
        ]

    def get_image(self, obj):
        try:
            image = obj.artistPhoto.url
        except:
            image = None
        return image


    def get_response_status(self, obj):
        return "200"

    def get_message(self, obj):
        return "Artist details Found"

    def get_genre(self, obj):
        try:
            genre = obj.genre.values_list('genreName', flat=True)

        except:
            genre = " "

        return genre




class ArtistNearYouSerializer(serializers.ModelSerializer):
    url = artist_detail_url
    image = serializers.SerializerMethodField()    # user = UserDetailSerializer(read_only=True)
    genre = serializers.SerializerMethodField()

    class Meta:
        model = Artist
        fields = [
            'url',
            'image',
            'artistName',
            'artistWebsite',
            'artistPhoto',
            'contact_no',
            'artistAgent',
            'artistBudget',
            'status',
            'genre',
            'biography',
            'remarks',
            'registered',
            'city',
            'country',
            'from_builder',
            'builder_genre',
            'builder_keywords',
            'press_contact',
            'general_manager',
            'gender',
            'facebook_fancount',
            'twitter_fancount',
            'youtube_fancount',
            'spotify_fancount',
            'instagram_fancount',
            'address',
            'type_of_performance',
            'city_latitude',
            'city_longitude',
        ]

    def get_image(self, obj):
        try:
            image = obj.artistPhoto.url
        except:
            image = None
        return image


    def get_genre(self, obj):
        try:
            genre = obj.genre.values_list('genreName', flat=True)

        except:
            genre = " "

        return genre





