from django.core.checks import messages
from django.forms import TextInput, Textarea
from django.contrib import admin
from django.db import models

# Register your models here.
from django.forms import forms

from .models import (Genre, Source, Artist, ArtistWebsite, ArtistGenre,
                    ArtistRequest, ArtistStatus, ArtistWeekendStatus,UserNote,
                    Comment,AddArtistRequest, GeneralArtist, ArtistPresskit, 
                    ArtistMedia, ArtistTravelDoc, ArtistCreativeInfo, ArtistPhoto, 
                    Offersheet, Offer, OffersheetSubCategories)
from .action_handler import (scrape_artist_handler, ml_artist_handler, rebuild_db_handler,
                            run_photo_scrapper, run_scrap_sources_url)


from django.contrib.admin.widgets import AdminFileWidget
from django.utils.translation import ugettext as _
from django.utils.safestring import mark_safe
from django.contrib import admin


class WDJPBaseAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        """
        Returns a QuerySet of all model instances that can be edited by the
        admin site. This is used by changelist_view.
        """
        qs = self.model.allartistobjects.get_queryset()
        ordering = self.get_ordering(request)
        if ordering:
            qs = qs.order_by(*ordering)
        return qs


class AdminImageWidget(AdminFileWidget):
    def render(self, name, value, attrs=None):
        output = []
        if value and getattr(value, "url", None):
            image_url = value.url.replace('artistphotos', 'artistphotos')
            file_name = str(value)
            output.append(u' <a href="%s" target="_blank"><img src="%s" alt="%s" /></a>' % \
                          (value.url, image_url, file_name))
        output.append(super(AdminFileWidget, self).render(name, value, attrs))
        return mark_safe(u''.join(output))


class ImageWidgetAdmin(admin.ModelAdmin):
    image_fields = []

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name in self.image_fields:
            request = kwargs.pop("request", None)
            kwargs['widget'] = AdminImageWidget
            return db_field.formfield(**kwargs)
        return super(ImageWidgetAdmin, self).formfield_for_dbfield(db_field, **kwargs)

class GenreAdmin(admin.ModelAdmin):
    list_display = ("genreID", "genreName")
    search_fields = ['genreName']
    ordering = ('genreName',)


class SourceAdmin(admin.ModelAdmin):
    list_display = ("sourceID", "sourceName")
    search_fields = ['sourceName']
    ordering = ('sourceName',)


class ArtistWeekendStatusAdmin(admin.ModelAdmin):
    list_display = ("artist", "booked_percentage")


class ArtistWebsiteAdmin(admin.ModelAdmin):
    list_display = ("ArtistName", "SourceName", "url", "is_verified_link")
    search_fields = ['artistID__artistName', 'sourceID__sourceName']
    list_filter = ['sourceID', 'is_verified_link']
    ordering = ('artistID',)
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '80'})},
    }


    actions = ['bulk_url_verify', 'bulk_url_unverify']

    def bulk_url_verify(self, request, queryset):
        rows_updated = queryset.update(is_verified_link=True)
        if rows_updated == 1:
            message_bit = "1 link was"
        else:
            message_bit = "%s links were" % rows_updated
            self.message_user(request, "%s successfully marked as verified." % message_bit)
    bulk_url_verify.short_description = "Check selected as verified"


    def bulk_url_unverify(self, request, queryset):
        rows_updated = queryset.update(is_verified_link=False)
        if rows_updated == 1:
            message_bit = "1 link was"
        else:
            message_bit = "%s links were" % rows_updated
        self.message_user(request, "%s successfully marked as not verified." % message_bit)
    bulk_url_unverify.short_description = "Check selected as not verified"

def scrape_artist(modeladmin, request, queryset):
    username = request.user.username
    artist_ids = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
    if type(modeladmin) == ArtistStatusAdmin:
        artist_ids = [str(queryset.get(pk=item).artistID.artistID) for item in
                      request.POST.getlist(admin.ACTION_CHECKBOX_NAME)]
    scrape_artist_handler(artist_ids, username)
scrape_artist.short_description = "Scrape Selected Artist"


def ml_artist(modeladmin, request, queryset):
    username = request.user.username
    artist_ids = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
    if type(modeladmin) == ArtistStatusAdmin:
        artist_ids = [str(queryset.get(pk=item).artistID.artistID) for item in
                      request.POST.getlist(admin.ACTION_CHECKBOX_NAME)]
    ml_artist_handler(artist_ids, username)
ml_artist.short_description = "ML Selected Artist"


def rebuild_db(modeladmin, request, queryset):
    username = request.user.username
    artist_ids = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
    if type(modeladmin) == ArtistStatusAdmin:
        artist_ids = [str(queryset.get(pk=item).artistID.artistID) for item in
                      request.POST.getlist(admin.ACTION_CHECKBOX_NAME)]

    rebuild_db_handler(artist_ids, username)
rebuild_db.short_description = "Rebuild Selected Artist's Database"


def scrap_photo(modeladmin, request, queryset):
    username = request.user.username
    artist_ids = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
    run_photo_scrapper.delay(artists= artist_ids, username= username)


scrap_photo.short_description = "Scrape Selected Artist's Photo"

def scrap_sources_url(modeladmin, request, queryset):
    artists_id = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
    run_scrap_sources_url.delay(artists_id)

scrap_sources_url.short_description = "Scrape Artist Source Urls"

def make_artist_active(modeladmin, request, queryset):
    queryset.update(status="1")

make_artist_active.short_description = "Make Selected Artist Active"

def make_artist_inactive(modeladmin, request, queryset):
    queryset.update(status="0")

make_artist_inactive.short_description = "Make Selected Artist InActive"

class ArtistGenreInline(admin.TabularInline):
    model = ArtistGenre
    extra = 0 # how many rows to show


class ArtistWebsiteInline(admin.TabularInline):
    model = ArtistWebsite
    extra = 0
    max_num = 8
    readonly_fields = ('social_links',)
    def social_links(self, instance):
        """
            Returns list of steps which are linked with the module
        """

        if instance.url and instance.url != "NULL":
            return "<a target='_blank' href='{}'>Click Here</a>".format(instance.url)
        else:
            return ""

    social_links.allow_tags = True
    social_links.short_description = 'Social Links'
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': 70})},
    }


class ArtistAdmin(ImageWidgetAdmin, WDJPBaseAdmin):
    list_display = ("artistName", "thumbnail", "artistAgent", "artistBudget", "artist_remarks", "registered")
    model = Artist
    filter_horizontal  = ("genre",)
    actions = [scrape_artist, ml_artist, rebuild_db, scrap_photo, scrap_sources_url, make_artist_active, make_artist_inactive]
    inlines = (ArtistGenreInline, ArtistWebsiteInline,)
    search_fields = ['artistName', 'artistAgent']
    list_filter = ['status', 'artistBudget', "genre", "from_builder"]
    ordering = ('-remarks', 'artistName',)
    readonly_fields = ('remarks',)
    image_fields = ['artistPhoto']
   # form = ArtistForm
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': 80})},
        models.TextField: {'widget': Textarea(attrs={'rows': 4, 'cols': 80})},
    }

def reject(modeladmin, request, queryset):
    queryset.update(status=2)
    queryset.update
reject.short_description = "Reject"


class ArtistRequestAdmin(admin.ModelAdmin):
    list_display = ('artistName', 'artistWebsite', 'artistAgent', 'artistBudget', 'biography','genre', 'requstedBy', 'lastModifiedTime', 'status')
    list_filter = ['requstedBy']
    search_fields = ['artistName', 'artistAgent']
    ordering = ('-lastModifiedTime','status')
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '80'})},
        models.TextField: {'widget': Textarea(attrs={'rows': 4, 'cols': 80})},
    }

    def get_queryset(self, request):
        qs = super(ArtistRequestAdmin, self).get_queryset(request)
        return qs.exclude(status=1)

    def approve(self, request, queryset):
        selected = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        selected_models = self.model.objects.filter(artistID__in=selected)
        exist_list = []
        for selected_model in selected_models:
            ad = ArtistAdmin(Artist,self.admin_site)
            ad_artist = ad.model.objects.filter(artistName=str(selected_model.artistName))
            if ad_artist:
                exist_list.append(str(selected_model.artistName))
        if exist_list:
            self.message_user(request, "Artist Name: %s already Exist." % ", ".join(exist_list), level=messages.ERROR)
            return
        for selected_model in selected_models:
            approved_artist = Artist()
            approved_artist.artistName = selected_model.artistName
            approved_artist.artistWebsite = selected_model.artistWebsite
            approved_artist.artistAgent = selected_model.artistAgent
            approved_artist.artistBudget = selected_model.artistBudget
            approved_artist.biography = selected_model.biography
            approved_artist.requstedBy = selected_model.requstedBy

            selected_genre = str(selected_model.genre).split(',')
            approved_artist.save()
            approved_artist_genre_list = []
            selected_genre = [1,2]
            for genre in selected_genre:
                artist_genre = Genre()
                artist_genre.genreID = genre
                approved_artist_genre = ArtistGenre()
                approved_artist_genre.artistID = approved_artist
                approved_artist_genre.genreID = artist_genre
                approved_artist_genre.save()

        queryset.update(status=1)

    approve.short_description = "Approve"

    actions = [approve, reject]
    model = ArtistRequest


class ArtistStatusAdmin(admin.ModelAdmin):
    list_display = ("ArtistName", "ScrapStatus", "LastScrappedTime",
                    "LastMLTime", "LastSuccessfulScrappedTime", "LastSuccessfulMLTime", "comment")
    search_fields = ['artistID','scrapStatus']
    ordering = ('artistID',)
    list_filter = ['scrapStatus']
    actions = [scrape_artist, ml_artist, rebuild_db]

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class GeneralArtistAdmin(admin.ModelAdmin):
    list_display = ('artistID','user','status')
    search_fields = ('artistID','user','status')
    exclude = ["is_shop_window", "is_tour", "is_booking", "is_private", "tour_text", "booking_text", "biography", "artistPhoto", "is_artist_info_completed", "locations", "places"]

class ArtistTravelDocAdmin(admin.ModelAdmin):
    list_display = ('general_artist', 'travel_doc', 'email_sent')

class OffersheetAdmin(admin.ModelAdmin):
    list_display = ('artist', 'agent', 'stream', 'live_events', 'virtual_events', 'private_music', 'interview')

class OfferAdmin(admin.ModelAdmin):
    list_display = ('artist', 'name', 'email', 'date', 'request_name', 'request_type', 'timezone', 'start_time', 'finish_time', 'paid')

class OffersheetSubCategoriesAdmin(admin.ModelAdmin):
    list_display = ('category', 'name')

admin.site.register(Genre, GenreAdmin)
admin.site.register(Source, SourceAdmin)
admin.site.register(ArtistWebsite, ArtistWebsiteAdmin)
admin.site.register(Artist, ArtistAdmin)
admin.site.register(ArtistRequest, ArtistRequestAdmin)
admin.site.register(ArtistStatus, ArtistStatusAdmin)
admin.site.register(ArtistWeekendStatus, ArtistWeekendStatusAdmin)
admin.site.register(UserNote)
admin.site.register(Comment)
admin.site.register(AddArtistRequest)
admin.site.register(GeneralArtist, GeneralArtistAdmin)
admin.site.register(ArtistPresskit)
admin.site.register(ArtistMedia)
admin.site.register(ArtistTravelDoc, ArtistTravelDocAdmin)
admin.site.register(ArtistCreativeInfo)
admin.site.register(ArtistPhoto)
admin.site.register(Offersheet, OffersheetAdmin)
admin.site.register(Offer, OfferAdmin)
admin.site.register(OffersheetSubCategories, OffersheetSubCategoriesAdmin)