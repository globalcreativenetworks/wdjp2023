from StringIO import StringIO

from ckeditor.widgets import CKEditorWidget
from django import forms
from django.contrib.auth.models import User
from django.core.files import File
from django.core.files.base import ContentFile
from django.forms import FileInput
from PIL import Image

from artist.models import (AddArtistRequest, Artist, ArtistWebsite, Comment,
                           UserNote, Offer)

FAME_LEVEL = (
    (0, "DON'T KNOW"),
    (1, 'UNDERGROUND STAR'),
    (2, 'POPULAR ACT'),
    (3, 'GLOBAL STAR')
)

class DjArtistForm(forms.ModelForm):

    class Meta:
        model = Artist
        fields = ('artistName', 'city', 'country', 'gender', 'artistBudget')
        # widgets = {
        #     'artistPhoto': FileInput(),
        # }

    def __init__(self, *args, **kwargs):
        super(DjArtistForm, self).__init__(*args, **kwargs)
        self.fields['artistName'].widget.attrs.update({'class': 'py-form-control', 'required': 'required'})
        self.fields['city'].widget.attrs.update({'class': 'py-form-control'})
        self.fields['country'].widget.attrs.update({'class': 'py-form-control'})
        self.fields['gender'].widget.attrs.update({'class': 'py-form-control'})
        self.fields['artistBudget'].widget.attrs.update({'class': 'py-form-control'})
        self.fields['artistBudget'].choices = FAME_LEVEL


class DjArtistForm2(forms.ModelForm):

    class Meta:
        model = Artist
        fields = ('artistWebsite', 'artistAgent', 'press_contact', 'general_manager', 'contact_no')

    def __init__(self, *args, **kwargs):
        super(DjArtistForm2, self).__init__(*args, **kwargs)
        self.fields['artistWebsite'].widget.attrs.update({'class': 'py-form-control'})
        self.fields['artistAgent'].widget.attrs.update({'class': 'py-form-control',  'cols': '100', 'rows': '5'})
        self.fields['press_contact'].widget.attrs.update({'class': 'py-form-control'})
        self.fields['general_manager'].widget.attrs.update({'class': 'py-form-control'})
        self.fields['contact_no'].widget.attrs.update({'class': 'py-form-control'})


class DjArtistForm3(forms.ModelForm):
    x = forms.FloatField(widget=forms.HiddenInput())
    y = forms.FloatField(widget=forms.HiddenInput())
    width = forms.FloatField(widget=forms.HiddenInput())
    height = forms.FloatField(widget=forms.HiddenInput())

    class Meta:
        model = Artist
        fields = ('artistPhoto', 'x', 'y', 'width', 'height', )
        widgets = {
            'artistPhoto': FileInput(),
        }

    def save(self):
        photo = super(DjArtistForm3, self).save(commit=False)
        img_io = StringIO()

        x = self.cleaned_data.get('x')
        y = self.cleaned_data.get('y')
        w = self.cleaned_data.get('width')
        h = self.cleaned_data.get('height')
        image = Image.open(photo.artistPhoto)
        cropped_image = image.crop((x, y, w+x, h+y))
        cropped_image.save(img_io, format='JPEG')
        img_content = ContentFile(img_io.getvalue(),photo.artistPhoto.path)
        photo.artistPhoto = img_content
        return photo

    def __init__(self, *args, **kwargs):
        super(DjArtistForm3, self).__init__(*args, **kwargs)
        self.fields['artistPhoto'].widget.attrs.update({'class': 'py-form-control'})


class UserNoteForm(forms.ModelForm):

    class Meta:
        model = UserNote
        fields = ('artist','note','user')
        widgets = {'artist': forms.HiddenInput(), 'user': forms.HiddenInput()}

    def __init__(self, *args, **kwargs):
        super(UserNoteForm, self).__init__(*args, **kwargs)
        self.fields['note'].widget.attrs.update({'class': 'form-control', 'rows':5})

class CommentForm(forms.ModelForm):
    content = forms.CharField(label="", widget=forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Enter your comment here.', 'rows':'4', 'cols':'50'}))
    
    class Meta:
        model = Comment
        fields = ('content',)

class AddArtistRequestForm(forms.ModelForm):
    approved = forms.CharField(widget=forms.HiddenInput())

    class Meta:
        model = AddArtistRequest
        fields = ('comments','approved',)
    
    def __init__(self, *args, **kwargs):
        super(AddArtistRequestForm, self).__init__(*args, **kwargs)
        self.fields['comments'].widget.attrs.update({'class': 'py-form-control','cols': '100', 'rows': '5'})
        self.fields['comments'].label = ''

class ArtistWebsiteForm(forms.ModelForm):
    has_official_account = forms.BooleanField(label='has_official_account')

    def __init__(self, *args, **kwargs):
        super(ArtistWebsiteForm, self).__init__(*args, **kwargs)
        self.fields['url'].widget.attrs.update({'class': 'py-form-control'})
        self.fields['has_official_account'].required = False
        self.fields['url'].required = False

    class Meta:
        model = ArtistWebsite
        fields = ('url', 'is_verified_link', 'has_official_account')
        widgets = ({'is_verified_link': forms.CheckboxInput(attrs={'style':'display:block;'}),
                    'has_official_account': forms.CheckboxInput(attrs={'style':'display:block;'})})

    def save(self, commit=True):
        if self.cleaned_data.get('has_official_account', False):
            self.instance.url=""
            self.instance.is_verified_link=False
        return super(ArtistWebsiteForm, self).save(commit=commit)

class ArtistProfileForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ArtistProfileForm, self).__init__(*args, **kwargs)
        self.fields['about'].widget.attrs.update({'class': 'py-form-control', 'placeholder': 'Enter About Info from preferably Facebook or Website, if facebook info is not available.'})
        self.fields['notes'].widget.attrs.update({'class': 'py-form-control', 'placeholder': 'Write Any Note on %s that you want to add.' %(self.instance.artistName)})
        self.fields['about'].widget = CKEditorWidget()
        self.fields['notes'].widget = CKEditorWidget()
        
    class Meta:
        model = Artist
        fields = ('about', 'notes')

class OfferStageOneForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(OfferStageOneForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({
            'class': 'py-form-control', 'required': 'required'})
        self.fields['request_type'].widget.attrs.update({
            'class': 'py-form-control', 'required': 'required'})

    class Meta:
        model  = Offer
        fields = ('name', 'request_type')

class OfferStageTwoForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(OfferStageTwoForm, self).__init__(*args, **kwargs)
        # self.fields['email'].widget.attrs.update({
        #     'class': 'py-form-control', 'required': 'required'})
        self.fields['date'].widget.attrs.update({
            'class': 'py-form-control datepicker',
            'required': 'required',
            'readonly': 'true',
            'autocomplete': 'off'
        })
        self.fields['request_name'].widget.attrs.update({
            'class': 'py-form-control', 'required': 'required'})
        # self.fields['phone_code'].widget.attrs.update({
        #     'class': 'py-form-control', 'required': 'required'})
        # self.fields['phone_number'].widget.attrs.update({
        #     'class': 'py-form-control', 'required': 'required'})

    class Meta:
        model  = Offer
        fields = ('date', 'request_name')

class OfferStageThreeForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(OfferStageThreeForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs.update({
            'class': 'py-form-control', 'required': 'required'})
        self.fields['phone_code'].widget.attrs.update({
            'class': 'py-form-control', 'required': 'required'})
        self.fields['phone_number'].widget.attrs.update({
            'class': 'py-form-control', 'required': 'required'})

    class Meta:
        model  = Offer
        fields = ('email', 'phone_code', 'phone_number')