from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import Artist, ArtistStatus


@receiver(post_save, sender=Artist, dispatch_uid='add_artist')
def add_artist_scrap_status(sender, instance=None, created=False, **kwargs):
    if instance.pk and created:
        artist_status = ArtistStatus()
        artist_status.artistID = instance
        artist_status.scrapStatus = 0
        artist_status.save()