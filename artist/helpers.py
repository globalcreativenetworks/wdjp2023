from django.conf import settings

from profiles.models import Profile


def user_is_agency(request):
    if not request.user.is_authenticated():
        return False
    agency_exists = Profile.objects.filter(user=request.user,user_types='AGENCY').first()
    if agency_exists:
        return True
    else:
        return False

def get_agency_name(request):
    url = request.META.get('HTTP_HOST')
    if url in settings.BASE_URLS:
        return ""
    if url in Profile.objects.filter(company_website__iexact = url,user_types='AGENCY').values_list("company_website",flat=True):
        return ""
    agency_name = url.split(".",1)[0]
    return agency_name

def profile_agency(request):
    url = request.META.get('HTTP_HOST')
    agency_name = get_agency_name(request)
    if agency_name == "":
        agency_profile = Profile.objects.filter(company_website__iexact = url, user_types='AGENCY').first()
    else:
        agency_profile = Profile.objects.filter(agency_url_prefix=agency_name, user_types='AGENCY').first()
    return agency_profile

def all_users_under_agency(request):
    profile_obj = profile_agency(request)
    users = []
    if profile_obj:
        profiles = Profile.objects.filter(under_agency=profile_obj.user).order_by('user__first_name')
        users= [profile.user for profile in profiles]
    return users

def user_ids_under_agency(request):
    users = all_users_under_agency(request)
    user_ids = [user.id for user in users]
    return user_ids