from __future__ import absolute_import
from celery import shared_task
from celery.task import periodic_task


@periodic_task(ignore_result=True, run_every=10)  # 10 seconds, or timedelta(seconds=10)
def just_print():
    print "Print from celery task"


@shared_task
def test(param):
    return "The test task executed with '%s' argument." % param