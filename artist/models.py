from __future__ import unicode_literals
from django import forms
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

from accounts.constants import TYPE_OF_PERFORMANCE
from multiselectfield import MultiSelectField
#cyclic dependency
# from accounts.models import EmailConfirmation

GENDER_CHOICES = [('MALE', 'MALE'), ('FEMALE', 'FEMALE'), ('GROUP', 'A GROUP OF BOTH MALE AND FEMALE')]

CATEGORIES = [
    ('live_event', 'Live Events / Bookings'),
    ('virtual_event', 'Virtual Events / Parties'),
    ('streams', 'Streams'),
    ('private_music', 'Teach / Share'),
    ('interview', 'Press / Promo')
]

# Create your models here.

class Genre(models.Model):
    genreID = models.AutoField(primary_key=True)
    genreName = models.CharField(max_length=20)

    def __str__(self):
        return self.genreName

    @classmethod
    def get_classname(cls):
        return cls.__name__


    class Meta:
        db_table = "dj_Genre"
        verbose_name_plural = "Genre"


class Source(models.Model):
    sourceID = models.AutoField(primary_key=True)
    sourceName = models.CharField(max_length=20)

    def __str__(self):
        return self.sourceName

    class Meta:
        db_table = "dj_Source"


class ArtistRequest(models.Model):
    artistID = models.AutoField(primary_key=True, db_column='artistID')
    artistName = models.CharField(max_length=100)
    artistWebsite = models.CharField(max_length=500, null=True, blank=True)
    artistAgent = models.CharField(max_length=50, null=True, blank=True)
    BudgetChoice = ((0, 'None'), (1, 'Low'), (2, 'Medium'), (3, 'High'))
    artistBudget = models.IntegerField(choices=BudgetChoice, default=1)
    StatusChoice = ((0, 'Pending'),(1, 'Approved'),(2, 'Rejected'))
    status = models.IntegerField(default=0, choices=StatusChoice)
    genre = models.CharField(max_length=500, null=True, blank=True)
    biography = models.TextField(blank=True, null=True)
    lastModifiedTime = models.DateTimeField(editable=False)
    requstedBy =  models.CharField(max_length=100)

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        self.lastModifiedTime = timezone.now()
        if not self.artistID:
            self.status = 0

        return super(ArtistRequest, self).save(*args, **kwargs)

    def __str__(self):
        return self.artistName

    class Meta:
        db_table = "dj_ArtistsRequested"


class BaseArtistManager(models.Manager):

    def get_queryset(self):
        """
        Returns a new QuerySet object.  Subclasses can override this method to
        easily customize the behavior of the Manager.
        """
        return self._queryset_class(model=self.model, using=self._db, hints=self._hints).exclude(status="0").exclude(status="")


class Artist(models.Model):
    artistID = models.AutoField(primary_key=True, db_column='artistID')
    artistName = models.CharField(max_length=100)
    artistWebsite = models.CharField(max_length=500, null=True, blank=True)
    artistPhoto = models.ImageField(upload_to='artistphotos/', blank=True, null=True)
    contact_no = models.CharField(max_length=20, null=True, blank=True)
    artistAgent = models.CharField(max_length=500, blank=True, null=True)
    BudgetChoice = ((0, 'None'), (1, 'UNDERGROUND STAR'), (2, 'POPULAR ACT'), (3, 'GLOBAL STAR'))
    artistBudget = models.IntegerField(choices=BudgetChoice, default=1)
    StatusChoice = (("0", 'InActive'),("1", 'Active'))
    status = models.CharField(max_length=2, default="1", choices=StatusChoice)
    genre = models.ManyToManyField(Genre, through="ArtistGenre")
    biography = models.TextField(blank=True, null=True)
    remarks = models.CharField(max_length=200, blank=True, default='')
    registered = models.DateTimeField(auto_now=True, null=True, blank=True)
    city = models.CharField(max_length=100, null=True, blank=True)
    country = models.CharField(max_length=50, null=True, blank=True)
    from_builder = models.BooleanField(default=False)
    builder_genre = models.TextField(null=True, blank=True)
    builder_keywords = models.TextField(null=True, blank=True)
    press_contact = models.TextField(null=True, blank=True)
    general_manager = models.TextField(null=True, blank=True)
    gender = models.CharField(max_length=10, null=True, blank=True, choices=GENDER_CHOICES)
    facebook_fancount = models.BigIntegerField(default=0)
    twitter_fancount = models.BigIntegerField(default=0)
    youtube_fancount = models.BigIntegerField(default=0)
    spotify_fancount = models.BigIntegerField(default=0)
    instagram_fancount = models.BigIntegerField(default=0)
    address = models.TextField(null=True, blank=True)
    type_of_performance = MultiSelectField(max_length=1000, choices=TYPE_OF_PERFORMANCE, null=True, blank=True)
    user_id = models.IntegerField()
    objects = BaseArtistManager()
    allartistobjects = models.Manager()
    city_latitude = models.DecimalField(max_digits=12, decimal_places=9, blank=True, null=True)
    city_longitude = models.DecimalField(max_digits=13, decimal_places=9, blank=True, null=True)

    # New Fields for Add an Artist Flow
    about = models.TextField(null=True,blank=True)
    notes = models.TextField(null=True,blank=True)

    # Add an Artist Flow
    is_artist_info_completed = models.BooleanField(default=True)
    completed_info_url = models.CharField(max_length=500, null=True, blank=True)


    def __str__(self):
        return self.artistName

    def thumbnail(self):
        if self.artistPhoto:
            image = self.artistPhoto.name.replace('artistphotos', 'artistphotos/128')
            return u'<img src="/media/%s" />' % (image)
        return ""

    thumbnail.allow_tags = True
    thumbnail.short_description = "Thumbnail"

    def artist_remarks(self):
        try:
            if self.remarks:
                return u'<a href="/admin/scrape/scrapephoto/?q=%s">Approval Pending</a>' % self.artistName.replace(' ', '+')
            return ''
        except Exception, ex:
            return ''

    artist_remarks.allow_tags = True
    artist_remarks.short_description = "Remarks"

    @classmethod
    def get_classname(cls):
        return cls.__name__

    class Meta:
        db_table = "dj_Artists"


class ArtistWeekendStatus(models.Model):
    artist = models.ForeignKey(Artist)
    booked_percentage = models.IntegerField(null=True, blank=True)
    weekend = models.CharField(max_length=10, null=True, blank=True)
    year = models.CharField(max_length=20, null=True, blank=True)

    def __str__(self):
        return self.artist.artistName



class ArtistGenre(models.Model):
    artistID = models.ForeignKey(Artist, db_column='artistID', on_delete=models.CASCADE)
    genreID = models.ForeignKey(Genre, db_column='genreID',  on_delete=models.CASCADE)

    class Meta:
        db_table = 'dj_ArtistGenre'


# class ArtistGenre(models.Model):
#     artistID = models.ForeignKey(Artist)
#     genreID = models.ManyToManyField(Genre, related_name='Artist')
#
#     class Meta:
#         db_table = "dj_ArtistGenre"


class ArtistWebsite(models.Model):
    id = models.AutoField(primary_key=True, db_column='id')
    sourceID = models.ForeignKey(Source, db_column="sourceID")
    artistID = models.ForeignKey(Artist, db_column="artistID",  on_delete=models.CASCADE)
    url = models.CharField(max_length=500, default="")
    is_verified_link = models.BooleanField(default=False)

    def ArtistName(self):
        return self.artistID
    ArtistName.short_description = "Artist Name"

    def SourceName(self):
        return self.sourceID

    SourceName.short_description = "Source Name"
    url.short_description = "Source URL"

    class Meta:
        db_table = "dj_artist_website"


class ArtistStatus(models.Model):
    id = models.AutoField(primary_key=True, db_column='id')
    #ScrapStatusChoices = ((0, 'Pending'), (1, 'In-Progress'), (2, 'Done'), (3, 'Failed'))
    ScrapStatusChoices = {0 : 'Pending', 1 : 'In-Progress', 2 : 'Done', 3 : 'Failed'}
    ScrapStatusColor = {0: 'Orange', 1: '#79aec8', 2: 'green', 3: 'red'}
    artistID = models.ForeignKey(Artist, db_column='artistID', unique=True)
    scrapStatus = models.IntegerField(choices=[(key,value) for key, value in ScrapStatusChoices.iteritems()], default=0)
    lastScrappedTime = models.DateTimeField(auto_now=True)
    lastSuccessfulScrappedTime = models.DateTimeField(auto_now=True)
    lastMLTime = models.DateTimeField(auto_now=True)
    lastSuccessfulMLTime = models.DateTimeField(auto_now=True)
    nextScheduleTime = models.DateTimeField(auto_now=True)
    comment = models.CharField(max_length=200)

    def ArtistName(self):
        return self.artistID

    ArtistName.short_description = "Artist Name"

    def LastScrappedTime(self):
        return self.lastScrappedTime

    LastScrappedTime.short_description = "Last Scrap"

    def LastMLTime(self):
        return self.lastMLTime

    LastMLTime.short_description = "Last ML"

    def LastSuccessfulScrappedTime(self):
        return self.lastSuccessfulScrappedTime

    LastSuccessfulScrappedTime.short_description = "Last Successful Scrap"

    def LastSuccessfulMLTime(self):
        return self.lastSuccessfulMLTime

    LastSuccessfulMLTime.short_description = "Last Successful ML"

    def ScrapStatus(self):
        #return self.ScrapStatusChoices.get(self.scrapStatus)
        return '<div style="width:100%%; height:100%%; color:%s;font-weight:900">%s</div>' % (self.ScrapStatusColor.get(self.scrapStatus), self.ScrapStatusChoices.get(self.scrapStatus))

    ScrapStatus.short_description = "Last Scrap Status"
    verbose_name_plural = "Artist Scrape Status"
    ScrapStatus.allow_tags = True

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        # if not self.id:
            # self.lastScrappedTime = timezone.now()
            # self.lastSuccessfulScrappedTime = timezone.now()
            # self.lastMLTime = timezone.now()
            # self.lastSuccessfulMLTime = timezone.now()
            # self.nextScheduleTime = timezone.now()

        return super(ArtistStatus, self).save(*args, **kwargs)

    class Meta:
        db_table = "dj_artist_scrapstatus"
        verbose_name_plural = "Artist Scrapping Status"

class DjSimilarArtists(models.Model):
    similarartistid = models.AutoField(db_column='similarArtistID', primary_key=True)  # Field name made lowercase.
    artistid = models.IntegerField(db_column='artistID')  # Field name made lowercase.
    sourceid = models.IntegerField(db_column='sourceID')  # Field name made lowercase.
    artistname = models.CharField(db_column='artistName', max_length=100)  # Field name made lowercase.
    linkedartistid = models.IntegerField(db_column='linkedArtistID')  # Field name made lowercase.
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'dj_similar_artists'

class UserNote(models.Model):
    artist = models.ForeignKey(Artist)
    user = models.ForeignKey(User)
    note = models.TextField(blank=True, null=True)
    modified_date = models.DateTimeField(null=True,blank=True)

    def __str__(self):
        return self.note


class Comment(models.Model):
    artist = models.ForeignKey(Artist)
    user = models.ForeignKey(User)
    reply = models.ForeignKey('Comment', null=True,blank=True, related_name="replies")
    content = models.TextField(max_length=250)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{}-{}-{}'.format(self.id, self.artist.artistName, str(self.user.username))

class AddArtistRequest(models.Model):
    artist = models.ForeignKey(Artist)
    requested_by = models.ForeignKey(User)
    handled_by = models.ForeignKey(User, null=True, blank=True, related_name="handled_by")
    StatusChoice = ((0, 'Pending'),(1, 'Approved'),(2, 'Rejected'))
    status = models.IntegerField(default=0, choices=StatusChoice)
    created_at  = models.DateTimeField(editable=False)
    modified_at = models.DateTimeField()
    comments = models.CharField(max_length=500, null=True, blank=True)
    EmailStatus = ((0, 'Not Sent'),(1, 'Sent'))
    email_status = models.IntegerField(default=0, choices=EmailStatus)

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.status = 0
            self.email_status = 0
            self.created_at = timezone.now()

        self.modified_at = timezone.now()

        return super(AddArtistRequest, self).save(*args, **kwargs)

    def __str__(self):
        return self.artist.artistName

# General Artist Part Added

class ArtistInfo(models.Model):
    artistID = models.OneToOneField(Artist, db_column='artistID', on_delete=models.CASCADE)
    title =  models.CharField(max_length=250, null=True, blank=True)
    web_title = models.CharField(max_length=250, null=True, blank=True)
    legal_title = models.CharField(max_length=250, null=True, blank=True)
    email = models.EmailField(blank=True, null=True)
    phone = models.CharField(max_length=20, null=True, blank=True)
    city = models.CharField(max_length=120, blank=True, null=True)
    country = models.CharField(max_length=80, blank=True, null=True)
    hashtag = models.CharField(max_length=250, null=True, blank=True)
    travel_conditions = models.TextField(null=True,blank=True)
    rider_conditions = models.TextField(null=True,blank=True)
    artwork_conditions = models.TextField(null=True,blank=True)


class GeneralArtist(models.Model):
    artistID = models.ForeignKey(Artist, db_column='artistID', on_delete=models.CASCADE)
    user = models.ForeignKey(User,  on_delete=models.CASCADE, related_name='user')
    StatusChoice = (("0", 'InActive'),("1", 'Active'))
    status = models.CharField(max_length=2, default="1", choices=StatusChoice)
    is_shop_window = models.BooleanField(default=False)
    is_tour = models.BooleanField(default=False)
    is_booking = models.BooleanField(default=False)
    is_private = models.BooleanField(default=False)
    tour_text = models.CharField(max_length=250, null=True, blank=True)
    booking_text = models.CharField(max_length=250, null=True, blank=True)
    biography = models.TextField(null=True, blank=True)
    # Artist Fields
    artistPhoto = models.ImageField(upload_to='artistphotos/', blank=True, null=True, help_text="Upload image size of 200x200 px")
    is_artist_info_completed = models.BooleanField(default=False)

    # For Places and locations Tag
    locations = models.TextField(null=True, blank=True)
    places = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.artistID.artistName


class ArtistCreativeInfo(models.Model):
    general_artist = models.ForeignKey(GeneralArtist, on_delete=models.CASCADE)
    labels = models.CharField(max_length=500, null=True, blank=True)
    affiliations = models.CharField(max_length=500, null=True, blank=True)
    discography_url = models.CharField(max_length=250, null=True, blank=True)
    press_kit_url = models.CharField(max_length=250, null=True, blank=True)
    press_kit_titlename = models.CharField(max_length=250, null=True, blank=True)
    email_sent = models.BooleanField(default = False)


class ArtistPresskit(models.Model):
    general_artist = models.ForeignKey(GeneralArtist, on_delete=models.CASCADE)
    press_kit = models.FileField(upload_to='artist_docs/', null=True, blank=True)
    press_kit_filename = models.CharField(max_length=250, null=True, blank=True)
    email_sent = models.BooleanField(default = False)

    def file_name(self):
        if self.press_kit:
            return self.press_kit.name.split("/")[-1]
        return ""

MEDIA_TYPE=(
        ('Facebook','Facebook'),
        ('Instagram','Instagram'),
        ('Soundcloud','Soundcloud'),
        ('Twitter','Twitter'),
        ('Beatport','Beatport'),
        ('YouTube','YouTube'),
        ('Mixcloud','Mixcloud'),
        ('Bandsintown','Bandsintown'),
        ('Dropbox', 'Dropbox'),
    )

class ArtistMedia(models.Model):
    artistID = models.ForeignKey(Artist, db_column='artistID', on_delete=models.CASCADE)
    media_type = models.CharField(max_length=255, choices=MEDIA_TYPE)
    media_title = models.CharField(max_length=255)
    media_url = models.URLField()

    def __str__(self):
        return self.media_type

class ArtistTechRider(models.Model):
    general_artist = models.ForeignKey(GeneralArtist, on_delete=models.CASCADE)
    tech_rider = models.FileField(upload_to='artist_docs/', null = True)
    tech_rider_filename = models.CharField(max_length=255,blank=True,null=True)
    email_sent = models.BooleanField(default = False)

    def file_name(self):
        if self.tech_rider:
            return self.tech_rider.name.split("/")[-1]
        return ""

class ArtistHospitalityRider(models.Model):
    general_artist = models.ForeignKey(GeneralArtist, on_delete=models.CASCADE)
    hospitality_rider = models.FileField(upload_to='artist_docs/', null = True)
    email_sent = models.BooleanField(default = False)

    def file_name(self):
        if self.tech_rider:
            return self.tech_rider.name.split("/")[-1]
        return ""

class ArtistTravelDoc(models.Model):
    general_artist = models.ForeignKey(GeneralArtist, on_delete=models.CASCADE)
    travel_doc = models.FileField(upload_to='artist_docs/', null = True)
    email_sent = models.BooleanField(default = False)

    def file_name(self):
        if self.travel_doc:
            return self.travel_doc.name.split("/")[-1]
        return ""

class ArtistPhoto(models.Model):
    artistID = models.ForeignKey(Artist, db_column='artistID', on_delete=models.CASCADE)
    image_url = models.CharField(max_length=255)


class OffersheetSubCategories(models.Model):
    category  = models.CharField(max_length=255, choices=CATEGORIES)
    name      = models.CharField(max_length=255)
    fields    = models.TextField(null=False, blank=False)
    icon      = models.ImageField(upload_to='offersheetsubcategories/', blank=True, null=True)
    demo_text = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class Offersheet(models.Model):
    artist           = models.ForeignKey(Artist, db_column='artistID', on_delete=models.CASCADE)
    agent            = models.ForeignKey(User, blank=True, null=True)
    include_presskit = models.BooleanField(default=False)
    include_rider    = models.BooleanField(default=False)
    live_events      = models.BooleanField(default=False)
    stream           = models.BooleanField(default=False)
    virtual_events   = models.BooleanField(default=False)
    private_music    = models.BooleanField(default=False)
    interview        = models.BooleanField(default=False)
    #allowed subcategories
    subcategories = models.ManyToManyField(OffersheetSubCategories)

class EmailConfirmation(models.Model):
    email = models.EmailField(max_length=320)
    token = models.CharField(max_length=20,null=True)
    # is_latest = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return u'%s' % (self.email)

class Offer(models.Model):
    artist         = models.ForeignKey(Artist, db_column='artistID', on_delete=models.CASCADE)
    name           = models.CharField(max_length=255)
    email          = models.EmailField(null=True)
    date           = models.DateField(null=True)
    phone_code     = models.CharField(max_length=10, null=True)
    phone_number   = models.CharField(max_length=15, null=True)
    request_name   = models.CharField(max_length=255, null=True)
    request_type   = models.ForeignKey(OffersheetSubCategories, on_delete=models.CASCADE, null=True)
    timezone       = models.CharField(max_length=255, null=True)
    start_time     = models.TimeField(null=True)
    finish_time    = models.TimeField(null=True)
    paid           = models.BooleanField(default=False)
    extra_data     = models.TextField(null=True)
    event_detail   = models.TextField(null=True)
    gdpr_compliant = models.BooleanField(default=False)
    # check for validation
    email_validation = models.ForeignKey(EmailConfirmation,on_delete=models.DO_NOTHING,null=True)
    is_validated = models.BooleanField(default=False)
#    offer status 0-new 1-hold 2-accept 3-delete
    status = models.PositiveSmallIntegerField(default=0)
    create_date = models.DateField(null=True)
    sent_message = models.TextField(null=True)
