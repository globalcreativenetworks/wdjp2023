import tempfile
import requests
import json
import urllib2
import os


from googleapiclient.discovery import build
from bs4 import BeautifulSoup


from django.core.mail import EmailMultiAlternatives
from django.conf import settings
from xml.etree import ElementTree as ET
from django.template.loader import render_to_string
from django.template import RequestContext
from django.shortcuts import redirect
from django.http import Http404
from django.utils.encoding import smart_str

from artist.models import Artist, ArtistWebsite, Source
from events.models import WpMyEvents

from .helpers import user_is_agency, user_ids_under_agency

class SendEmail(object):

    def __init__(self, request=None, headers=None, sender=None, backend=None , file=[]):
        self.request = request
        self.headers = headers
        self.from_name = settings.DEFAULT_FROM_EMAIL_NAME
        self.file = file

        if sender:
            self.sender = sender
        else:
            self.sender = settings.DEFAULT_FROM_EMAIL

    def send_by_template(self, recipient, template_path, context, subject, bcc_email=[], cc_email=[]):
        """
        send email function with template_path. will do both rendering & send in this function.
        should not change the interface.
        """
        body = self.email_render(template_path, context)
        self.send_email(recipient, subject, body, bcc_email, cc_email)

    def send_by_body(self, recipient, subject, body, bcc_email=[], cc_email=[]):
        """
        send email function with text. will do both rendering & send in this function.
        should not change the interface.
        """
        try:
            self.send_email(recipient, subject, body, bcc_email, cc_email)
        except Exception:
            pass

    def send_email(self, recipient, subject, body, bcc_email, cc_email):
        """
        send email with rendered subject and body
        """
        msg = EmailMultiAlternatives(subject, subject, self.sender, recipient, bcc=bcc_email, cc=cc_email)

        msg.attach_alternative(body, "text/html")

        if self.file:
            for file in self.file:
               tmp_file = tempfile.NamedTemporaryFile(mode='wb', delete="True")
               tmp_file.close()
               tmp_file = open(file.name, 'w')
               tmp_file.write(file.read())
               tmp_file.close()
               msg.attach_file(tmp_file.name)
               tmp_file.close()

        msg.send()

    def email_render(self, template_path, context):
        """
        wrapper to generate email subject and body
        """
        if self.request is None:
            body = render_to_string(template_path, context)
        else:
            body = render_to_string(template_path, context, RequestContext(self.request))
        return body


def google_search(search_term, api_key, cse_id, **kwargs):
    service = build("customsearch", "v1", developerKey=api_key)
    res = service.cse().list(q=search_term, cx=cse_id, **kwargs).execute()
    return res['items']


def return_artist_data(artist, artist_unverified_not_avilable_sources):
    API_KEY = settings.GOOGLE_CUSTOM_SEARCH_API_KEY
    API_ID = settings.GOOGLE_CUSTOM_SEARCH_API_ID

    key_word_dict = {
        'Website': 'website',
        'Bandsintown': 'Bandsintown',
        'Resident Adviser': 'Resident Advisor dj profile',
        'Gigatools': 'Gigatools',
        'Facebook': 'Facebook',
        'Soundcloud': 'Soundcloud',
        'Songkick': 'Songkick',
        'Youtube': 'Youtube channel',
        'Twitter': 'Instagram',
        'Spotify': 'Spotify',
        'Instagram': 'Instagram',
    }

    # links = ['website','Bandsintown', 'Resident Advisor dj profile', \
    #          'Gigatools', 'Facebook','Soundcloud','Songkick',\
    #          'Twitter','Instagram','Youtube channel','Spotify'];

    constraints_dict = {
        'Website': 'www',
        'Bandsintown': 'bandsintown',
        'Resident Adviser': 'residentadvisor',
        'Gigatools': 'gigatools',
        'Facebook': 'facebook',
        'Soundcloud': 'soundcloud',
        'Songkick': 'songkick',
        'Youtube': 'youtube',
        'Twitter': 'twitter',
        'Spotify': 'spotify',
        'Instagram': 'instagram',
    }

    artist_last_name = artist.artistName.split()[0]

    result_dict = {}

    for website in artist_unverified_not_avilable_sources:
        try:
            if key_word_dict.has_key(website):
                key_word = key_word_dict[website]
            else:
                key_word = website
            print key_word
            results = google_search(key_word + ' ' + artist.artistName, API_KEY, API_ID, num=1)
            openArray = results[0]
            result_dict[website] = openArray.get('formattedUrl')
        except Exception, e:
            print('error : '+ str(e))

    print result_dict
    for key,value in result_dict.items():
        if constraints_dict.has_key(key):
            constrant = constraints_dict[key]
        else:
            constrant = result_dict[key].lower()
        if constrant in value:
            if artist_last_name in value:
                pass
            if artist_last_name.lower() in value:
                pass
            else:
                if key == "Spotify":
                    if "open.spotify.com" not in value:
                        result_dict[key] = None
                else:
                    result_dict[key] = None
        else:
            result_dict[key] = None

    return result_dict


def scrap_artist_sources(artists_id):
    artists_id = [int(id) for id in artists_id]
    all_artist = Artist.objects.filter(artistID__in=artists_id)

    all_sources = Source.objects.all().values_list('sourceName', flat=True)
    all_sources = list(all_sources)

    for artist in all_artist:
        artist_verified_sources = ArtistWebsite.objects.filter(artistID=artist, is_verified_link=True).values_list('sourceID__sourceName', flat=True)
        artist_verified_sources = list(artist_verified_sources)

        artist_unverified_not_available_sources = list(set(all_sources) - set(artist_verified_sources))

        artist_data = return_artist_data(artist, artist_unverified_not_available_sources)
        for key, value in artist_data.items():
            if artist_data[key]:
                try:
                    source = Source.objects.get(sourceName=key)
                    artist_web = ArtistWebsite.objects.get_or_create(sourceID=source, artistID=artist)
                    artist_web[0].url = artist_data[key]
                    artist_web[0].save()
                except:
                    pass

temp_artists_list = []
my_api_key = "AIzaSyAx5MvNAfiC4mzCoWM_V6SSym-7aJa-DwM"
my_cse_id = "012462734859606010397:9ojkbm0gb6o"
genres_Array = []
client_id = '36525c08941b490d90ba9dbfda6ac99e'
client_secret = 'b591c227fa124afca0dba40e41f824d2'
grant_type = 'client_credentials'
body_params = {'grant_type': grant_type}
token_url = 'https://accounts.spotify.com/api/token'

def accessToken():
    token_url = 'https://accounts.spotify.com/api/token'
    response = requests.post(token_url, data=body_params, auth=(client_id, client_secret))
    json_obj = json.loads(response.text)
    # print json_obj['access_token']
    return json_obj['access_token']


def debug(variable_name, variable):
    if settings.DEBUG:
        print("DEBUG" + "-"*20)
        print ("Variable '{}' value is: {}".format( variable_name, variable ))
        print("END DEBUG" + "-" * 16)


def artist_live(name):
    link_array = []
    image = 'Null'
    links = [' on wikipedia']
    deadOrLive = 'Live'
    try:
        for link in links:
            try:
                results = google_search(name + link, num=1)
                openArray = results[0]
                link_array.append(openArray.get('formattedUrl'))
            except Exception as e:
                print("Exception in 'artist_live' inner try", str(e))
        if (smart_str((link_array[0])).strip().startswith("https://en.wikipedia.org")):
            urlString = smart_str(link_array[0]).strip()
            hdr = {'User-Agent': 'Mozilla/5.0'}
            req = urllib2.Request(urlString, headers=hdr)
            html_page = urllib2.urlopen(req)
            soup = BeautifulSoup(html_page)
            for table in soup.findAll('table', {'class': 'infobox vcard'}):
                for tr in table.findAll('tr'):
                    for th in tr.findAll('th'):
                        if smart_str((th.text)).strip() == "Died":
                            deadOrLive = smart_str((th.text)).strip()
                    for td in tr.findAll('td'):
                        for link in td.findAll('a', {'class': 'image'}):
                            for ima in link.findAll('img'):
                                image = smart_str(ima.get('src')).strip()
                                image = 'https:' + image

            for table in soup.findAll('table', {'class': 'infobox biography vcard'}):
                for tr in table.findAll('tr'):
                    for th in tr.findAll('th'):
                        if smart_str((th.text)).strip() == "Died":
                            deadOrLive = smart_str((th.text))
                    for td in tr.findAll('td'):
                        for link in td.findAll('a', {'class': 'image'}):
                            for ima in link.findAll('img'):
                                image = smart_str(ima.get('src')).strip()
                                image = 'https:' + image

            for table in soup.findAll('table', {'class': 'infobox vcard plainlist'}):
                for tr in table.findAll('tr'):
                    for th in tr.findAll('th'):
                        if smart_str((th.text)).strip() == "Died":
                            deadOrLive = smart_str((th.text)).strip()
                    for td in tr.findAll('td'):
                        for link in td.findAll('a', {'class': 'image'}):
                            for ima in link.findAll('img'):
                                image = smart_str(ima.get('src')).strip()
                                image = 'https:' + image

    except Exception as e:
        print("Exception in 'artist_live' outer try", str(e))
        image = "Null"
    debug('deadOrLive', deadOrLive)
    debug('image', image)
    return deadOrLive, image

def artist_Img_FromSpotify(artist_Id):
    image_Array = []
    token = accessToken()
    token = smart_str(token).strip()
    r = requests.get('https://api.spotify.com/v1/artists/' + artist_Id +
                     '?access_token=' + token)
    abc = r.json()
    images = abc['images']
    for im in images:
        image_from_json = smart_str(im['url']).strip()
        image_Array.append(image_from_json)
    return image_Array[2]


def popular_tracks(artist_Id):
    populartrack = []
    params = {'country': 'SE'}
    token = accessToken()
    token = smart_str(token)
    # r = requests.get('https://api.spotify.com/v1/artists/' + artist_Id +
    #                  '/top-tracks?access_token=' + token, params)

    r = requests.get('https://api.spotify.com/v1/artists/' + artist_Id +
                     '/top-tracks?access_token=' + token, params)


    abc = r.json()

    for tracks in abc['tracks']:
        track = smart_str(tracks['name']).strip()

        if "'" in track:
            continue
        else:
            populartrack.append(track)
    debug('popularTrack', populartrack)


    return populartrack


def artist_Rel_KeyWord(artist_name):
    keywords = []
    link = 'http://google.com/complete/search?output=toolbar&q=%s' % (artist_name)
    # try:
    html_pagee = requests.get(link)
    html_pagee.raw.decode_content = True
    root = ET.fromstring(html_pagee.content)
    for child in root:
        for sugg in child:
            key_str = smart_str((sugg.attrib.get('data'))).strip()
            if "'" in key_str:
                continue
            elif "'" not in key_str:
                keywords.append(smart_str(key_str).strip())
    key = []
    len_Key = len(keywords) / 2
    for i in range(len_Key):
        key.append(str(keywords[i]).replace('  ', ''))
    # except Exception as e:
    #     print("Exception in 'artist_Rel_KeyWord' try", str(e))
    key.append(None)
    debug('key', key)
    return key


def followers(artist_Id):
    genres_Array = []
    token = accessToken()
    token = smart_str(token)
    r = requests.get('https://api.spotify.com/v1/artists/' + artist_Id +
                     '?access_token=' + token)
    abc = r.json()
    followrs = smart_str(abc['followers']['total']).strip()
    genres = abc['genres']
    for gen in genres:
        gen = smart_str(gen).strip()
        if "'" in gen:
            continue
        else:
            genres_Array.append(gen)
    return followrs, genres_Array


def find_ArtistDetails(name, artist_obj):
    artist_Info_Array = []
    try:
        artist_Id = find_SpotifyId(name)

        if (artist_Id == "null"):
            print name + ' does not exist on Spotify'
            message = name + ' does not exist on Spotify'

        else:
            # tracks = popular_tracks(artist_Id)
            deadAlive, image_Artist = artist_live(name)
            if image_Artist == "Null":
                image_Artist = artist_Img_FromSpotify(artist_Id)
            # tracks_String = ','.join(tracks)
            # keywords = artist_Rel_KeyWord(name)
            # if keywords[0] != None:
            #     if keywords[-1] == None:
            #         keywords.pop(-1)
            #     keywords_String = ','.join(keywords)
            # elif keywords[0] == None:
            #     keywords_String = None
            followerss, genres_Array = followers(artist_Id)
            if len(genres_Array) != 0:
                gen_String = ','.join(genres_Array)
            else:
                gen_String = ','.join(genres_Array)
                gen_String = gen_String + "None"
            link_array = find_Artist_Links(name)

            artist_Info_Array.append(name)
            artist_Info_Array.append(followerss)
            artist_Info_Array.append(smart_str(link_array[0]).strip())
            artist_Info_Array.append(smart_str(link_array[1]).strip())
            artist_Info_Array.append(smart_str(link_array[2]).strip())
            artist_Info_Array.append(smart_str(link_array[3]).strip())
            artist_Info_Array.append(smart_str(link_array[4]).strip())
            artist_Info_Array.append(smart_str(link_array[5]).strip())
            artist_Info_Array.append(smart_str(link_array[6]).strip())
            artist_Info_Array.append(smart_str(link_array[7]).strip())
            artist_Info_Array.append(smart_str(link_array[8]).strip())
            artist_Info_Array.append(smart_str(link_array[9]).strip())
            artist_Info_Array.append(smart_str(link_array[10]).strip())
            artist_Info_Array.append(smart_str(link_array[11]).strip())

            try:
                artist_Info_Array.append(nationality(name))
            except Exception as e:
                print("Exception in 'find_ArtistDetails' inner try", str(e))
                artist_Info_Array.append(None)
            # artist_Info_Array.append(keywords_String)
            # artist_Info_Array.append(tracks_String)
            artist_Info_Array.append(image_Artist)
            artist_Info_Array.append(gen_String)
        debug('artist_Info_Array', artist_Info_Array)

        if artist_obj:
            sources = ["bandsintown", "resident adviser", "gigatools", "facebook", "soundcloud", "songkick", "twitter", "instagram", "youtube", "spotify", "lastfm"]
            i = 3
            for source in sources:
                if i > 13:
                    break
                sou = Source.objects.get_or_create(sourceName=source)
                artist_website = ArtistWebsite.objects.get_or_create(artistID=artist_obj, sourceID=sou[0], url=artist_Info_Array[i], is_verified_link = True)
                i += 1

            artist_obj.artistWebsite = artist_Info_Array[2]

            image_url = artist_Info_Array[15]
            response = requests.get(image_url)
            image_name = artist_obj.artistName.replace(" ", "_").decode('unicode_escape').encode('ascii','ignore') + ".jpg"

            full_image_path = os.path.join(settings.MEDIA_ROOT, 'artistphotos', image_name)
            artist_image_file = open(full_image_path, 'wb+')
            artist_image_file.write(response.content)
            artist_image_file.close()

            artist_image_path = full_image_path.split("media/")[1]
            artist_obj.artistPhoto = artist_image_path
            artist_obj.save()
        return artist_Info_Array
    except Exception as e:
        print("Exception in 'find_ArtistDetails' outer try", str(e))
        return artist_Info_Array

def find_Artist_Links(name):
    link_array = []
    links = ['Official website of', 'Bandsintown', 'Resident Advisor dj profile',
             'Gigatools', 'Facebook', 'Soundcloud', 'Songkick',
             'Twitter', 'Instagram', 'Youtube channel', 'Spotify', 'last.fm'];
    for link in links:
        try:
            results = google_search(link + ' ' + name, num=1)
            openArray = results[0]
            link_String = openArray.get('formattedUrl')
            if "'" in link_String:
                link_array.append("None")
            else:
                link_array.append(link_String)
        except Exception as e:
            print("Exception in 'find_Artist_Links' try", str(e))
            link_array.append("None")
    debug('link_array', link_array)
    return link_array


def find_SpotifyId(name):
    print "name +++++++++++++++++++++++++++"
    print name
    link_array = []
    artist_Id = 'null'
    site = ' on spotify'
    try:
        results = google_search( str(name) + site, num=1)
        openArray = results[0]
        link_array.append(openArray.get('formattedUrl'))
    except Exception as e:
        print('Exception in "find_SpotifyId":', str(e))
    if (smart_str(link_array[0]).strip().startswith("https://open.spotify.com/artist")):
        if len(link_array[0]) != 0:
            myString = smart_str((link_array[0])).strip()
            n, artist_Id = myString.split('artist/')
            if (str(artist_Id) == None):
                artist_Id = 'null'
        else:
            artist_Id = 'null'
        debug('artist_Id', artist_Id)
        return artist_Id
    else:
        return artist_Id


def similar_artists(artist_Id):
    final_art = []
    art = []
    artist_name_and_followers = {}
    token = accessToken()
    token = smart_str(token)
    r = requests.get('https://api.spotify.com/v1/artists/' + artist_Id +
                     '/related-artists?access_token=' + token)
    try:
        abc = r.json()

        artists = abc['artists']

        for i in artists:
            try:
                artists_name = smart_str(i['name']).strip() # Clean Artist Name
                artist_follower = i['followers']['total'] # Get followers
                if artist_follower > 4999: # Artist threshold
                    if not "xC" in artists_name:
                        artist_name_and_followers[artists_name] = artist_follower
                        art.append(artists_name) # Make Artist List

            except Exception as e:
                print("Exception in 'similar_artists' inner try", str(e))
                continue
    except Exception as e:
        print("Exception in 'similar_artists' outer try", str(e))

    return art, artist_name_and_followers


def google_search(search_term, **kwargs):
    try:
        service = build("customsearch", "v1", developerKey=my_api_key)
        res = service.cse().list(q=search_term, cx=my_cse_id, **kwargs).execute()
    except Exception as e:
        return False
    return res['items']

def get_artist_calendar_events(request,artist_id,calendar_year,calendar_month):
    if request.user.is_superuser:
        events = WpMyEvents.objects.filter(event_date__year=calendar_year,event_date__month=calendar_month)
    elif user_is_agency(request):
        events = WpMyEvents.objects.filter(user_id__in=user_ids_under_agency(request), event_date__year=calendar_year,event_date__month=calendar_month)
    else:
        events = WpMyEvents.objects.filter(user_id = request.user.id,event_date__year=calendar_year,event_date__month=calendar_month)
    events_list = []
    for event in events:
        # if event.general_artist:
        #     if event.general_artist.artistID.artistID == int(artist_id):
        #         events_list.append(event)
        events_list.append(event)
    return events_list