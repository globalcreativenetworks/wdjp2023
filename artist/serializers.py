from rest_framework import serializers
from artist.models import ArtistRequest, Genre, Source


class GenreSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Genre
        fields = ('genreID', 'genreName')


class SourceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Source
        fields = ('sourceID', 'sourceName')


class ArtistSerializer(serializers.ModelSerializer):

    class Meta:
        model = ArtistRequest
        fields = ('artistID', 'artistName', 'artistWebsite', 'artistAgent', 'artistBudget',
                  'biography','genre', 'requstedBy')
