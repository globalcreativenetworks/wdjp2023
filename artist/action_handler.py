from scrapper import Scrapper, ScrapMode
from celery import Celery
from social_data.facebook import FacebookAPI, scrap_facebook_data
from social_data.twitter import scrap_twitter_api
from social_data.youtube import scrap_youtube_api, YoutubeAPI
from social_data.spotify import scrap_spotify_api
from social_data.instagram import scrap_instagram_data
from artist.models import Artist, ArtistWebsite
from artist.utils import scrap_artist_sources
from social_data.models import FacebookLike
from social_data.management.commands import update_trends
from geography.models import Country
from django.db.models import Q
import datetime

app = Celery('pyadmin_celery', broker='redis://localhost:6379/0')


@app.task
def run_scrapper(scrape_mode = ScrapMode.All, artists=[], username=""):
    scrapper_instance = Scrapper()
    scrapper_instance.process(scrape_mode=scrape_mode, selected_artist=artists, username=username)


@app.task
def rebuild_db():
    scrapper_instance = Scrapper()
    scrapper_instance.process(scrape_mode=ScrapMode.All, selected_artist=[], username="Scheduler")


@app.task
def run_photo_scrapper(artists=[], username="", new_source=False):
    scrapper_instance = Scrapper()
    scrapper_instance.scrape_photo(selected_artist=artists, username=username, new_source=new_source)


def scrape_artist_handler(artists, username):
    run_scrapper.delay(scrape_mode=ScrapMode.Scrape, artists=artists, username=username)


def ml_artist_handler(artists, username):
    run_scrapper.delay(scrape_mode=ScrapMode.MachineLearning, artists=artists, username=username)


def rebuild_db_handler(artists, username):
    run_scrapper.delay(scrape_mode=ScrapMode.All, artists=artists, username=username)


@app.task
def update_social_data(source=None):
    all_artist = Artist.objects.all().values_list('artistID', flat=True)
    if source == 'twitter':
        scrap_twitter_api(all_artist) #scrap twitter data
    elif source == 'youtube':
        scrap_youtube_api(all_artist) #scrap youtube data
    elif source == "spotify":
        scrap_spotify_api(all_artist) #scrap spotify data
    elif source == "facebook":
        scrap_facebook_api(all_artist)
    elif source == "instagram":
        scrap_instagram_data(all_artist)

    else:
        scrap_twitter_api(all_artist)
        scrap_youtube_api(all_artist)
        scrap_spotify_api(all_artist)
        scrap_facebook_api(all_artist)
    trend = update_trends.Command()
    trend.handle()


@app.task
def run_scrap_sources_url(artists_id):
    scrap_artist_sources(artists_id)


def scrap_facebook_api(all_artist):
    for artist in all_artist:
        facebookapi = FacebookAPI()
        facebookapi.set_pageid_from_artist(artist)
        api_data = facebookapi.get_details()
        if api_data.has_key('error'):
            continue
        scrap_facebook_data(api_data, artists=[artist])

@app.task
def run_facebook_like_count(artists= []):
    countries = Country.objects.all().order_by("name")
    if not artists:
        artists = Artist.objects.all().values_list('artistID',flat=True)
    for artist in artists:
        print ("artist",artist)
        facebookapi = FacebookAPI()
        page_id = facebookapi.set_pageid_from_artist(artist)

        api_data = facebookapi.page_fans_country()

        if api_data.has_key('error'):
            continue
        if not api_data.has_key('data'):
            continue
        data = api_data['data'][0]['values'][0]
        date = datetime.datetime.strptime(data['end_time'].split("T")[0], "%Y-%m-%d")
        facebook_likes = FacebookLike.objects.filter(artist_id=artist,created_at=date)
        if facebook_likes:
            print ("Aleady Exist.")
            continue
        if data.has_key('value'):
            countries_like_count = data['value']
            for country_code,like_count in countries_like_count.items():
                try:
                    country_name = countries.filter(Q(code = country_code)).get().name
                except Exception as e:
                    print ("Country Not Exist.")
                    continue
                facebook_like = FacebookLike()
                facebook_like.artist_id = artist
                facebook_like.country_name = country_name
                facebook_like.country_code = country_code
                facebook_like.like_count = like_count
                facebook_like.created_at = date
                facebook_like.save()
                print ("Saved....")
