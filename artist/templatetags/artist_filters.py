import re
from datetime import datetime

from dateutil import parser
from django.db.models import Q
from django.template import Library
from django.template.defaultfilters import stringfilter
from django.utils.safestring import mark_safe

from artist.views import get_artist_social_url, get_upcoming_events
from events.models import DjMaster, Event
from geography.models import City, Country
from news.models import News
from profiles.models import Profile, ProfilesProfileFavoriteVenues

register = Library()


@register.filter
def get_venue(artist):
    upcoming_events = get_upcoming_events(artist, start=0, map_event=None)
    if upcoming_events:
        country = upcoming_events[0].country
        if country == "United States of America":
            country = "United States"

        return country

    return " "


@register.filter(name='split')
@stringfilter
def split(value, key):
    """
        Returns the value turned into a list.
    """
    return value.split(key)


@register.filter
def get_venue_city(artist):
    upcoming_events = get_upcoming_events(artist, start=0, map_event=None)

    if upcoming_events:
        country = upcoming_events[0].city

        return country

    return " "

@register.filter
def get_country_code_upper(country):
    country_code = Country.objects.filter(
        Q(full_name__iexact=country) | Q(full_name__icontains=country) | Q(
            code__iexact=country) | Q(name__iexact=country)
    ).first()
    if country_code:
        return country_code.code
    else:
        return ""


@register.filter
def get_city_key_and_code(name, both=None):
    name = name.replace('\n','').strip()
    city_code = City.objects.filter(
        Q(city__iexact=name) | Q(city__icontains=name)
    ).first()
    if city_code and both:
        return "country=%s&city=%s" %(city_code.iso2, city_code.pk_city)
    elif city_code:
        return city_code.pk_city
    else:
        return ""


@register.filter
def get_genre(artists):
    artist_genre = []
    if artists != None:
        for artist in artists:
            for genre in artist.genre.all():
                if genre.genreName not in artist_genre:
                    artist_genre.append(genre.genreName)
    return artist_genre


@register.filter
@stringfilter
def get_budget(count):
    count = int(count)
    return '$'*count


@register.filter
def artist_news(artist_obj):
    news = News.objects.filter(
        artists=artist_obj).order_by('-publish_date')[:5]
    return news


@register.filter
@stringfilter
def get_source(artist):
    urls = get_artist_social_url(artist)
    urls.pop('youtube_username', '')
    return urls.items()


@register.filter
@stringfilter
def format_date(date, format):
    if date != '':
        dt = parser.parse(date)
        formated_date = dt.strftime(format)
        return formated_date
    return ''


@register.filter
def add_digit(value, digit2):
    return int(value) + int(digit2)


@register.filter
def get_value_from_dict(dict_obj, key):
    return dict_obj.get(key)


@register.filter
def get_length(venue, profile):
    profile = ProfilesProfileFavoriteVenues.objects.filter(
        profile=profile, djvenues=venue)
    return profile[0].uservenuecontact_set.all().count()


@register.filter
def get_percentage_change(value1, value2):

    value1 = int(value1)
    value2 = int(value2)

    try:
        percentage = ((float(value1) - float(value2))/float(value1))*100
        if percentage < 0:
            percentage = -(percentage)
        return round(percentage, 2)
    except:
        return 0


@register.filter
def get_country_code(country):

    country_code = Country.objects.filter(
        Q(full_name__iexact=country) | Q(full_name__contains=country) | Q(
            code__iexact=country) | Q(name__iexact=country)
    ).first()

    if country_code:
        return country_code.code.lower()
    else:
        return ""


@register.filter
def get_country_flag_url(country):
    try:
        country_obj = Country.objects.filter(
            name__iexact=country.strip()).first()
        country_code = str(country_obj.code).lower()
        return '/media/country_flags/' + country_code + '.gif'
    except:
        return False


@register.filter
def name_from_email(email):
    name = email.split("@")[0].title()
    return name


@register.filter
def get_artist_image_or_none(artist):
    if artist:
        if artist.artistPhoto:
            return artist.artistPhoto.url
        else:
            return None
    else:
        return None


@register.filter
def content_placeholder(value, token):
    value.fields['content'].widget.attrs["placeholder"] = token
    return value


@register.filter
def add_class(value, css_class):
    class_re = re.compile(r'(?<=class=["\'])(.*)(?=["\'])')
    string = str(value)
    match = class_re.search(string)
    if match:
        m = re.search(r'^%s$|^%s\s|\s%s\s|\s%s$' % (css_class, css_class,
                                                    css_class, css_class), match.group(1))
        print (match.group(1))
        if not m:
            return mark_safe(class_re.sub(match.group(1) + " " + css_class,
                                          string))
    else:
        return mark_safe(string.replace('>', ' class="%s">' % css_class))
    return value

@register.filter
def get_text_or_none(txt):
    if txt or txt != "":
        return txt
    else:
        return "NONE"