from __future__ import unicode_literals

from django.apps import AppConfig
from watson import search as watson

class ArtistConfig(AppConfig):
    name = 'artist'
    verbose_name = "Artist Management"

    def ready(self):
        Artist = self.get_model("Artist",)
        Genre = self.get_model("Genre",)
        watson.register(Artist)
        watson.register(Genre)
        from . import signals
