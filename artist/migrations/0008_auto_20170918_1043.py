# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-09-18 10:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('artist', '0007_auto_20170918_0951'),
    ]

    operations = [
        migrations.AlterField(
            model_name='artist',
            name='artistAgent',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
    ]
