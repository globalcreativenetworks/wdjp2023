# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2020-05-01 09:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('artist', '0035_auto_20200501_0524'),
    ]

    operations = [
        migrations.AddField(
            model_name='offer',
            name='phone_code',
            field=models.CharField(max_length=10, null=True),
        ),
        migrations.AddField(
            model_name='offer',
            name='phone_number',
            field=models.CharField(max_length=15, null=True),
        ),
    ]
