# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2020-05-06 09:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('artist', '0040_auto_20200505_0603'),
    ]

    operations = [
        migrations.AddField(
            model_name='offersheetsubcategories',
            name='demo_text',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='offersheetsubcategories',
            name='icon',
            field=models.ImageField(blank=True, null=True, upload_to='offersheetsubcategories/'),
        ),
    ]
