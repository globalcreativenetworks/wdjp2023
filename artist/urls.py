from django.conf.urls import url

from artist import views

urlpatterns = [
    url(r'^start_add_artist/$', views.start_add_artist, name="start_add_artist"),

    url(r'^artist_step_1_0/$', views.artist_step_1_0, name="artist_step_1_0"),
    url(r'^artist_step_1_1/$', views.artist_step_1_1, name="artist_step_1_1"),
    url(r'^artist_step_1/$', views.artist_step_1, name="artist_step_1"),
    url(r'^validate-artist-name/$', views.validate_artist_name, name='validate_artist_name'),
    
    url(r'^artist_step_2_edit/(?P<artist_id>\d+)$', views.artist_step_2_edit, name="artist_step_2_edit"),
    url(r'^artist_step_2/$', views.artist_step_2, name="artist_step_2"),

    url(r'^artist_step_3_16/(?P<artist_id>\d+)$', views.artist_step_3_16, name="artist_step_3_16"),
    url(r'^artist_step_3_1/(?P<artist_id>\d+)$', views.artist_step_3_1, name="artist_step_3_1"),
    url(r'^artist_step_3_2/(?P<artist_id>\d+)$', views.artist_step_3_2, name="artist_step_3_2"),
    url(r'^artist_step_3_3/(?P<artist_id>\d+)$', views.artist_step_3_3, name="artist_step_3_3"),
    url(r'^artist_step_3_4/(?P<artist_id>\d+)$', views.artist_step_3_4, name="artist_step_3_4"),
    url(r'^artist_step_3_5/(?P<artist_id>\d+)$', views.artist_step_3_5, name="artist_step_3_5"),
    url(r'^artist_step_3_6/(?P<artist_id>\d+)$', views.artist_step_3_6, name="artist_step_3_6"),
    url(r'^artist_step_3_7/(?P<artist_id>\d+)$', views.artist_step_3_7, name="artist_step_3_7"),
    url(r'^artist_step_3_8/(?P<artist_id>\d+)$', views.artist_step_3_8, name="artist_step_3_8"),
    url(r'^artist_step_3_9/(?P<artist_id>\d+)$', views.artist_step_3_9, name="artist_step_3_9"),
    url(r'^artist_step_3_10/(?P<artist_id>\d+)$', views.artist_step_3_10, name="artist_step_3_10"),
    url(r'^artist_step_3_11/(?P<artist_id>\d+)$', views.artist_step_3_11, name="artist_step_3_11"),
    url(r'^artist_step_3_12/(?P<artist_id>\d+)$', views.artist_step_3_12, name="artist_step_3_12"),
    url(r'^artist_step_3_13/(?P<artist_id>\d+)$', views.artist_step_3_13, name="artist_step_3_13"),
    url(r'^artist_step_3_14/(?P<artist_id>\d+)$', views.artist_step_3_14, name="artist_step_3_14"),
    url(r'^artist_step_3_15/(?P<artist_id>\d+)$', views.artist_step_3_15, name="artist_step_3_15"),

    url(r'^artist_step_4/(?P<artist_id>\d+)$', views.artist_step_4, name="artist_step_4"),

    url(r'^artist_step_5/(?P<artist_id>\d+)$', views.artist_step_5, name="artist_step_5"),
    url(r'^artist_step_5_confirm/(?P<artist_id>\d+)$', views.artist_step_5_confirm, name="artist_step_5_confirm"),

    url(r'^artist_step_profile/(?P<artist_id>\d+)$', views.artist_step_profile, name="artist_step_profile"),

    url(r'^artist_step_similar_artists/(?P<artist_id>\d+)$', views.artist_step_similar_artists, name="artist_step_similar_artists"),

    url(r'^artist_step_6_preview/(?P<artist_id>\d+)$', views.artist_step_6_preview, name="artist_step_6_preview"),
    url(r'^artist_step_6/(?P<artist_id>\d+)$', views.artist_step_6, name="artist_step_6"),
    
    url(r'^artist_submit_details/(?P<artist_id>\d+)$', views.artist_submit_details, name="artist_submit_details"),

    url(r'^new-artist-to-complete/$', views.new_artist_to_complete, name='new_artist_to_complete'),

    url(r'^artist/(?P<artist_id>\d+)/$', views.artist_detail, name='artist_detail'),
    url(r'^artist/(?P<artist_id>\d+)/load_events/$', views.load_events, name='load_events'),
    url(r'^artist/add-or-remove-favorite/$', views.add_or_remove_favorite, name='add_or_remove_favorite'),
    url(r'^artist/add-or-remove-shortlist/$', views.add_or_remove_shortlist, name='add_or_remove_shortlist'),
    url(r'^artist_map/(?P<artist_id>\d+)/$', views.artist_map_frame, name='artist_map_frame'),
    url(r'^maps/$', views.artist_map, name="artist_map"),
    url(r'^search/$', views.artist_search, name="artist_search"),
    url(r'^load-search-artist/$', views.load_search_artist, name="load_search_artist"),
    url(r'^master-search/$', views.master_search, name="master_search"),
    # url(r'^detail-mobile/$', views.detail_mobile, name="detail_mobile"),
    url(r'^music-you-love/$', views.someone_music_you_love, name="someone_music_you_love"),
    url(r'filter-name/$', views.filter_artist_name, name="filter_artist_name"),
    url(r'filter-artist/$', views.filter_name_artist, name="filter_name_artist"),
    url(r'filter-city/$', views.filter_name_city, name="filter_name_city"),
    url(r'filter-all/$', views.filter_all, name="filter_all"),
    url(r'filter-all-art/$', views.filter_all_art, name="filter_all_art"),

    url(r'^flight-share/$', views.flight_share, name="flight_share"),
    url(r'^email-listings/(?P<artist_id>\d+)/$', views.email_listings, name="email_listings"),
    url(r'^free-dates/(?P<artist_id>\d+)/$', views.free_dates, name="free_dates"),
    url(r'^email-artists/$', views.email_artist, name="email_artist"),
    url(r'^my-favourite/$', views.favorite, name="favorite"),
    url(r'^compare_dj/$', views.artist_compare_dj, name="artist_compare_dj"),
    url(r'^compare/$', views.compare_artists, name="compare_artists"),
    url(r'^filter-all-submit/$', views.filter_all_submit, name="filter_all_submit"),
    url(r'^filter-artist-report/$', views.filter_artist_report, name="filter_artist_report"),
    url(r'^$', views.artists, name='artists'),
    # url(r'^find-similar/(?P<artist_name>.*)/$', views.find_similar, name='find_similar'),
    url(r'^find-similar/$', views.find_similar, name='find_similar'),
    url(r'^artist-exist-or-not/$', views.artist_exist_or_not, name='artist_exist_or_not'),
    url(r'^get-data/$', views.get_data, name='get_data'),
    # url(r'^dj-artist/$', views.dj_artist, name='dj_artist'),
    url(r'^edit-artist-detail/$', views.edit_artist_detail, name='edit_artist_detail'),
    url(r'^add-djartist-1/$', views.add_djartist_1, name='add_djartist_1'),
    url(r'^add-djartist-2/$', views.add_djartist_2, name='add_djartist_2'),
    url(r'^add-djartist-3/$', views.add_djartist_3, name='add_djartist_3'),
    url(r'^add-djartist-4/$', views.add_djartist_4, name='add_djartist_4'),
    url(r'^add-djartist-5/(?P<artist_id>\d+)/$', views.add_djartist_5, name='add_djartist_5'),
    url(r'^add-artist-5/(?P<artist_id>\d+)/$', views.add_artist_5, name='add_artist_5'),

    url(r'^edit-djartist-1/(?P<artist_id>\d+)/$', views.edit_djartist_1, name='edit_djartist_1'),
    url(r'^edit-djartist-2/(?P<artist_id>\d+)/$', views.edit_djartist_2, name='edit_djartist_2'),
    url(r'^edit-djartist-3/(?P<artist_id>\d+)/$', views.edit_djartist_3, name='edit_djartist_3'),
    url(r'^edit-djartist-4/(?P<artist_id>\d+)/$', views.edit_djartist_4, name='edit_djartist_4'),

    url(r'^add-managed-djs/$', views.add_managed_djs, name="add_managed_djs"),
    url(r'^add-added-djs/$', views.add_added_djs, name="add_added_djs"),

    url(r'^edit-added-artist-1/(?P<artist_id>\d+)/$', views.edit_added_artist_1, name='edit_added_artist_1'),
    url(r'^edit-added-artist-2/(?P<artist_id>\d+)/$', views.edit_added_artist_2, name='edit_added_artist_2'),
    url(r'^edit-added-artist-3/(?P<artist_id>\d+)/$', views.edit_added_artist_3, name='edit_added_artist_3'),
    url(r'^edit-added-artist-4/(?P<artist_id>\d+)/$', views.edit_added_artist_4, name='edit_added_artist_4'),

    url(r'^get-artist-data/$', views.get_artist_data, name="get_artist_data"),
    url(r'^admin-approve/(?P<artist_id>\d+)/$', views.admin_approve, name='admin_approve'),
    url(r'^email/$', views.logo, name='logo'),
    url(r'^social-data-xls/$', views.social_data_xls, name='social_data_xls'),

    url(r'trends', views.trending_top, name='trends'),
    url(r'user_note/(?P<note_id>\d+)/$', views.user_note, name='user_note'),
    url(r'delete_note/(?P<note_id>\d+)/$', views.delete_note, name='delete_note'),
    url(r'user_comment/(?P<artist_id>\d+)/$', views.user_comment, name='user_comment'),
    url(r'delete_comment/(?P<comment_id>\d+)/$', views.delete_comment, name='delete_comment'),

    url(r'^save_artist_info/(?P<artist_id>\d+)/$', views.save_artist_info, name="save_artist_info"),
    url(r'^delete-artist-file/(?P<artist_id>\d+)/$', views.delete_artist_file, name="delete_artist_file"),
    url(r'^save_artist_creative_info/(?P<artist_id>\d+)/$', views.save_creative_artist_info, name="save_artist_creative_info"),
    
    url(r'^artist/(?P<artist_id>\d+)/load_shortlist_venues/$', views.load_shortlist_venues, name='load_shortlist_venues'),
    url(r'^artist/(?P<artist_id>\d+)/load_shortlist_events/$', views.load_shortlist_events, name='load_shortlist_events'),

    # Getting all events for particular artist
    url(r'^get-artist-events-json/$', views.get_artist_events_json, name='get_artist_events_json'),

    # All artists notes
    url(r'^notes/$', views.artists_notes, name='artists_notes'),

    # Artist Promo Urls
    url(r'^save_artist_photos/(?P<artist_id>\d+)/$', views.save_artist_photos, name="save_artist_photos"),
    url(r'^save_artist_media/(?P<artist_id>\d+)/$', views.save_artist_media, name="save_artist_media"),
    url(r'^artist_media_delete/(?P<media_id>\d+)/(?P<artist_id>\d+)$', views.artist_media_delete, name="artist_media_delete"),
    url(r'^artist_photo_delete/(?P<photo_id>\d+)/(?P<artist_id>\d+)$', views.artist_photo_delete, name="artist_photo_delete"),

    # Mail Views
    url(r'^email-travel-docs/(?P<artist_id>\d+)/$', views.email_travel_docs, name="email_travel_docs"),

    url(r'^search-all-artists/$', views.search_all_artists, name="search_all_artists"),

    # Offersheet
    url(r'^create-offersheet/$', views.create_artist_offersheet, name="create_artist_offersheet"),
    url(r'^offersheet-more-options/(?P<artist_id>\d+)/$', views.offersheet_final_step, name="offersheet_final_step"),
    url(r'^offersheet-created/(?P<artist_id>\d+)/$', views.offersheet_created, name="offersheet_created"),
    url(r'^offersheet/(?P<artist_id>\d+)/edit/$', views.edit_offersheet, name="edit_offersheet"),

    # Offer creation
    url(r'^offersheet/(?P<artist_id>\d+)/$', views.create_offer, name="offer"),
    url(r'^offersheet/(?P<artist_id>\d+)/details$', views.create_offer_details, name="offer_create_details"),
    url(r'^offersheet/(?P<artist_id>\d+)/final$', views.create_offer_final, name="offer_create_final"),
    url(r'^offersheet/(?P<artist_id>\d+)/preview$', views.offer_preview, name="offer_preview"),
    url(r'^offersheet/success$', views.offer_success, name="offer_success"),
    url(r'^offersheet/validate-email/(?P<email>.+)/(?P<token>[a-zA-Z0-9]+)/(?P<offer_id>\d+)$', views.validate_email, name='offer_validate_email'),
    url(r'^offers/(?P<offer_id>\d+)/$', views.offer_detail, name="view_offer"),

    # Offer HQ
    url(r'^offerhq/$', views.offerhq, name="offerhq"),
   ]