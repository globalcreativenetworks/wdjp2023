from __future__ import division

import json
import math
import os
import time
from datetime import datetime, timedelta

import dateutil.relativedelta
import pytz
import requests
import xlwt
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.gis.geoip2 import GeoIP2 as GeoIP
# from django.contrib.gis.geoip import GeoIP
from django.core.mail import EmailMessage
from django.db.models import Count
from django.http import (HttpResponseRedirect,
                         JsonResponse)
from django.shortcuts import render
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView, View
from rest_framework import status, viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from watson import search as watson
from xlwt import Workbook

from accounts.decorators import *
from accounts.utils import SendEmail
from artist.forms import (AddArtistRequestForm, ArtistProfileForm,
                          ArtistWebsiteForm, CommentForm, DjArtistForm,
                          DjArtistForm2, DjArtistForm3, UserNoteForm, OfferStageOneForm,
                          OfferStageTwoForm, OfferStageThreeForm)
from artist.models import (AddArtistRequest, Artist, ArtistCreativeInfo,
                           ArtistGenre, ArtistInfo, ArtistMedia,
                           ArtistPresskit, ArtistRequest, ArtistTechRider,
                           ArtistTravelDoc, ArtistWebsite, ArtistWeekendStatus,
                           Comment, DjSimilarArtists, GeneralArtist, Genre,
                           Source, UserNote, ArtistPhoto, Offersheet, OffersheetSubCategories,
                           Offer, EmailConfirmation)
from artist.serializers import (ArtistSerializer, GenreSerializer,
                                SourceSerializer)
from events.models import DjMaster, DjVenues, Event, WpMyEvents
from geography.models import City, Country, Territories
from marketing_pages.models import MarketingEmails
from django.utils.crypto import get_random_string
from news.models import News
from profiles.models import Profile, ProfileConnections
from profiles.views import save_recent_search
from scrape.models import ScrapePhoto
from scrapper.database.database_interface import DBInterface
from social_data.models import (FacebookData, FacebookLike, InstagramData,
                                SpotifyData, TimeTrends, Tops, TwitterData,
                                YoutubeData)
from .action_handler import rebuild_db_handler
from .utils import find_ArtistDetails, get_artist_calendar_events
from django.utils.html import escape

limit = 20


class GenreViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Genre.objects.all()
    serializer_class = GenreSerializer


class ArtistViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = ArtistRequest.objects.all()
    serializer_class = ArtistSerializer


@api_view(['GET'])
def source_list(request):
    """
    Retrieve, update or delete a snippet instance.
    """
    try:
        source = Source.objects.all()

    except Source.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = SourceSerializer(source, many=True)
        return Response(serializer.data)


@api_view(['POST'])
def add_artist(request):
    """
    Retrieve, update or delete a snippet instance.
    """
    try:
        source = Source.objects.all()

    except Source.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'POST':
        serializer = ArtistSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def filters():
    genres = Genre.objects.all()
    budget = Artist.BudgetChoice
    # popularity = Source.objects.all().exclude(
    #     sourceName__in=['Bandsintown', 'Gigatools', 'Resident Adviser', 'Songkick'])
    popularity = Source.objects.all().filter(
        sourceName__in=['Facebook', 'Youtube', 'Twitter', 'Instagram', 'Spotify'])
    countries = Country.objects.all()
    return (genres, budget, popularity, countries)


def sort_by_popularity(all_artists, source):
    if source == "facebook":
        all_artists = all_artists.order_by("-facebook_fancount")
    elif source == "twitter":
        all_artists = all_artists.order_by("-twitter_fancount")
    elif source == "spotify":
        all_artists = all_artists.order_by("-spotify_fancount")
    elif source == "youtube":
        all_artists = all_artists.order_by("-youtube_fancount")
    elif source == "instagram":
        all_artists = all_artists.order_by("-instagram_fancount")

    return all_artists


@login_required
@check_email_validated
# @check_plan('Trial','Basic','Premium')
@check_plan('Trial', 'Artist/DJ', 'Promoter', 'Agent')
def artists(request):
    prioritize_my_fav = request.GET.get('prioritize_my_fav', '')
    artist_limit = 30
    msg = 'YOUR SEARCH RESULTS FOR ARTISTS'
    artist_request_message = ""
    if (request.GET.get('entries')):
        entries = int(request.GET.get('entries'))
        if ((0 < entries < 30) or (30 < entries < 60) or (60 < entries < 120) or (120 < entries)):
            entries = 30
        artist_limit = entries
    else:
        entries = 30
    if (request.GET.get('genre')):
        genre = int(request.GET.get('genre'))
        name = Genre.objects.filter(genreID=genre).get()
        msg += ' FROM ' + name.genreName + ' GENRE'
    else:
        genre = 0
    if (request.GET.get('budget')):
        budget = int(request.GET.get('budget'))
        name = "$" * budget
        if budget == 0:
            name = 'NOT SPECIFIED'
        msg += ' AT -' + name + '- BUDGET'
    else:
        budget = 0
    if (request.GET.get('popularity')):
        popularity = int(request.GET.get('popularity'))
        current_time = time.time()
        name = Source.objects.filter(sourceID=popularity).get()
        msg += ' MOST POPULAR ON > ' + name.sourceName
        source = 'social_data_' + name.sourceName.lower() + 'data__artist_id'
    else:
        popularity = 0
    if (request.GET.get('shuffle')):
        shuffle = int(request.GET.get('shuffle'))
        if not (request.GET.get('genre')):
            if shuffle == 0 and not popularity:
                msg += ' > DISPLAYED RANDOMLY'
    else:
        shuffle = 0

    # Search For a artist Name Logic
    artist_id_get = request.GET.get('id', '')
    artist_name = request.GET.get('artist_name', '')
    if artist_name:
        if artist_id_get:
            try:
                artist = Artist.objects.get(artistID=artist_id_get)
                return redirect(reverse('artist_detail', kwargs={'artist_id': artist_id_get}))
            except:
                msg = "Sorry We aren't sure who the artist '{}' is ".format(artist_name)
                artist_request_message = True

        else:
            artists_search = Artist.objects.filter(artistName__icontains=artist_name)
            if len(artists_search) == 1:
                return redirect(reverse('artist_detail', kwargs={'artist_id': artists_search[0].artistID}))
            elif len(artists_search) > 1:
                allartists = artists_search
                msg = "Following Are The Artists With Name '{}'".format(artist_name)
            else:
                msg = "Sorry  We aren't sure who the artist '{}' is.".format(artist_name)
                artist_request_message = True
    try:
        if allartists:
            pass
    except:
        allartists = Artist.objects.all().order_by('artistName').distinct()

    if allartists:
        if budget:
            allartists = allartists.filter(artistBudget=budget)
        if genre:
            allartists = allartists.filter(genre__genreID=genre)
        if shuffle == 0:
            allartists = allartists.order_by('?')
        if popularity:
            source = Source.objects.filter(sourceID=popularity).get()
            allartists = sort_by_popularity(allartists, source.sourceName.lower())

    # this block is for prioritizing my fav
    if allartists and prioritize_my_fav == "Yes":
        allartists = prioritize_favourtie_artist(request, allartists)

    if request.is_ajax():
        start = request.GET.get('start', '')
        if start:
            start = int(start)
            allartists = allartists[start:(start + artist_limit)]

            context = {
                "allartists": allartists,
                'filter_popularity': popularity,
                'start': start
            }

            template = render_to_string("artist/get_artist.html", context, context_instance=RequestContext(request))

            start = int(start) + artist_limit
            if allartists:
                status = "True"  # More events
            else:
                status = "False"  # No More events
            response = {
                "status": status,
                "start": start,
                "data": template,
            }

            return HttpResponse(json.dumps(response), content_type="application/json")
    allartists = allartists[0: entries]
    context = {
        "allartists": allartists,
        'artist_start': artist_limit,
        'message': msg,
        'genres': filters()[0],
        'budget': filters()[1],
        'popularity': filters()[2],
        'filter_budget': budget,
        'filter_popularity': popularity,
        'prioritize_my_fav': prioritize_my_fav,
        'genre': genre,
        'entries': entries,
        'shuffle': shuffle,
        'artist_request_message': artist_request_message,
        'artist_name': artist_name,
        'show_filters': True,
    }
    return render(request, 'artist/artists.html', context)


@login_required
@check_email_validated
# @check_plan('Trial','Basic','Premium')
@check_plan('Trial', 'Artist/DJ', 'Promoter', 'Agent')
def compare_artists(request):
    prioritize_my_fav = request.GET.get('prioritize_my_fav', '')
    artist_limit = 30
    msg = 'YOUR SEARCH RESULTS FOR ARTISTS'
    artist_request_message = ""
    if (request.GET.get('entries')):
        entries = int(request.GET.get('entries'))
        if ((0 < entries < 30) or (30 < entries < 60) or (60 < entries < 120) or (120 < entries)):
            entries = 30
        artist_limit = entries
    else:
        entries = 30
    if (request.GET.get('genre')):
        genre = int(request.GET.get('genre'))
        name = Genre.objects.filter(genreID=genre).get()
        msg += ' FROM ' + name.genreName + ' GENRE'
    else:
        genre = 0
    if (request.GET.get('budget')):
        budget = int(request.GET.get('budget'))
        name = "$" * budget
        if budget == 0:
            name = 'NOT SPECIFIED'
        msg += ' AT -' + name + '- BUDGET'
    else:
        budget = 0
    if (request.GET.get('popularity')):
        popularity = int(request.GET.get('popularity'))
        name = Source.objects.filter(sourceID=popularity).get()
        msg += ' MOST POPULAR ON > ' + name.sourceName
        source = 'social_data_' + name.sourceName.lower() + 'data__artist_id'
    else:
        popularity = 0
    if (request.GET.get('shuffle')):
        shuffle = int(request.GET.get('shuffle'))
        if shuffle == 0:
            msg += ' > DISPLAYED RANDOMLY'
    else:
        shuffle = 0

    # Search For a artist Name Logic
    artist_id_get = request.GET.get('id', '')
    artist_name = request.GET.get('artist_name', '')
    if artist_name:
        if artist_id_get:
            try:
                artist = Artist.objects.get(artistID=artist_id_get)
                return redirect(reverse('artist_detail', kwargs={'artist_id': artist_id_get}))
            except:
                msg = "Sorry We aren't sure who the artist '{}' is ".format(artist_name)
                artist_request_message = True

        else:
            artists_search = Artist.objects.filter(artistName__icontains=artist_name)
            if len(artists_search) == 1:
                return redirect(reverse('artist_detail', kwargs={'artist_id': artists_search[0].artistID}))
            elif len(artists_search) > 1:
                allartists = artists_search
                msg = "Following Are The Artists With Name '{}'".format(artist_name)
            else:
                msg = "Sorry  We aren't sure who the artist '{}' is.".format(artist_name)
                artist_request_message = True
    try:
        if allartists:
            pass
    except:
        allartists = Artist.objects.order_by('artistName').distinct()

    if allartists:
        if budget:
            allartists = allartists.filter(artistBudget=budget)
        if genre:
            allartists = allartists.filter(genre__genreID=genre)
        if popularity:
            source = Source.objects.filter(sourceID=popularity).get()
            allartists = sort_by_popularity(allartists, source.sourceName.lower())
        if shuffle == 0:
            allartists = allartists.order_by('?')

    # this block is for prioritizing my fav
    if allartists and prioritize_my_fav == "Yes":
        allartists = prioritize_favourtie_artist(request, allartists)

    if request.is_ajax():
        start = request.GET.get('start', '')
        if start:
            start = int(start)
            allartists = allartists[start:(start + artist_limit)]

            context = {
                "allartists": allartists,
                'filter_popularity': popularity,
                'start': start
            }

            template = render_to_string("artist/get_artist.html", context, context_instance=RequestContext(request))

            start = int(start) + artist_limit
            if allartists:
                status = "True"  # More events
            else:
                status = "False"  # No More events
            response = {
                "status": status,
                "start": start,
                "data": template,
            }

            return HttpResponse(json.dumps(response), content_type="application/json")
    allartists = allartists[0: entries]
    context = {
        "allartists": allartists,
        'artist_start': artist_limit,
        'message': msg,
        'genres': filters()[0],
        'budget': filters()[1],
        'popularity': filters()[2],
        'filter_budget': budget,
        'filter_popularity': popularity,
        'prioritize_my_fav': prioritize_my_fav,
        'genre': genre,
        'entries': entries,
        'shuffle': shuffle,
        'artist_request_message': artist_request_message,
        'artist_name': artist_name,
    }
    return render(request, 'artist/compare_artists.html', context)


def get_artist_social_url(artist, for_icons=False):
    if for_icons:
        sources = Source.objects.all().exclude(
        sourceName__in=['Bandsintown', 'Gigatools', 'ResidentAdviser', 'Songkick', 'lastfm', 'resident adviser', 'BeatPort'
                        'ElectronicFestivals', 'EventBrite', 'IflyerJP', 'TicketMaster', 'ITunes', 'VK'])
    else:
        sources = Source.objects.all().exclude(
            sourceName__in=['Spotify', 'Bandsintown', 'Gigatools', 'Resident Adviser', 'Songkick'])

    social_url = {}
    for source in sources:
        artistwebsite = ArtistWebsite.objects.filter(sourceID=source, artistID=artist)
        if artistwebsite:
            social_url[source.sourceName.lower()] = artistwebsite[0].url
            if source.sourceName.lower() == "youtube":
                if len(artistwebsite[0].url.split("/")) > 1:
                    social_url[source.sourceName.lower() + "_username"] = artistwebsite[0].url.split("/")[-1]

            if source.sourceName.lower() == "soundcloud":
                """
                This condition remove the www. from the url
                """
                social_url[source.sourceName.lower()] = artistwebsite[0].url.replace("www.", "")

    return social_url


def get_growth(old_likes, new_likes):
    old_likes = float(old_likes)
    new_likes = float(new_likes)
    if new_likes == 0.0 and old_likes == 0.0:
        return str(0)
    growth = (new_likes - old_likes) / old_likes * 100
    return str(growth)[:3]


def get_upcoming_events_dates(artist, single_date):
    event_ids = DjMaster.objects.filter(artistid=artist.artistID).values_list('eventid', flat=True).distinct()
    artist_upcoming_event_ids = Event.objects.filter(eventID__in=event_ids, date=single_date).values_list('eventID',
                                                                                                          flat=True).order_by(
        'date')

    if artist_upcoming_event_ids:
        artist_upcoming_event_ids = [str(x) for x in artist_upcoming_event_ids]
        event_id_string = '('
        event_id_string += ','.join(artist_upcoming_event_ids) + ')'
        upcoming_events = DjMaster.objects.raw(
            "SELECT * FROM dj_Master JOIN dj_Events ON dj_Events.eventID = dj_Master.eventID JOIN dj_Venues ON dj_Venues.venueID = dj_Master.venueID WHERE dj_Master.artistID = {} AND dj_Events.eventID IN {} GROUP BY dj_Events.date,dj_Venues.country ORDER BY dj_Events.date".format(
                artist.artistID, event_id_string))

    else:
        upcoming_events = []

    return upcoming_events


def get_upcoming_events(artist, start=0, map_event=None, upcoming_events_length=False):
    global limit
    event_ids = DjMaster.objects.filter(artistid=artist.artistID).values_list('eventid', flat=True).distinct()

    if map_event:
        artist_upcoming_event_ids = Event.objects.filter(eventID__in=event_ids).filter(
            date__gte=datetime.now().date()).values_list('eventID', flat=True).order_by('date')
    else:
        if not upcoming_events_length:
            artist_upcoming_event_ids = Event.objects.filter(eventID__in=event_ids).filter(
                date__gte=datetime.now().date()).values_list('eventID', flat=True).order_by('date')[
                                        start: start + limit]
        else:
            artist_upcoming_event_ids = Event.objects.filter(eventID__in=event_ids).filter(
                date__gte=datetime.now().date()).values_list('eventID', flat=True).order_by('date')
    if artist_upcoming_event_ids:
        artist_upcoming_event_ids = [str(x) for x in artist_upcoming_event_ids]
        event_id_string = '('
        event_id_string += ','.join(artist_upcoming_event_ids) + ')'

        upcoming_events = DjMaster.objects.raw(
            "SELECT * FROM dj_Master JOIN dj_Events ON dj_Events.eventID = dj_Master.eventID JOIN dj_Venues ON dj_Venues.venueID = dj_Master.venueID WHERE dj_Master.artistID = {} AND dj_Events.eventID IN {} GROUP BY dj_Events.date,dj_Venues.country ORDER BY dj_Events.date".format(
                artist.artistID, event_id_string))

    else:
        upcoming_events = []

    if not upcoming_events_length:
        return upcoming_events
    else:
        return list(upcoming_events)[:20], len(list(upcoming_events))


def get_past_events(artist, start=0, map_event=None, calendar_year=None, calendar_month=None, past_events_length=False):
    global limit
    event_ids = DjMaster.objects.filter(artistid=artist.artistID).values_list('eventid', flat=True).distinct()
    if map_event:
        artist_past_event_ids = Event.objects.filter(eventID__in=event_ids).filter(
            date__lt=datetime.now().date()).values_list('eventID', flat=True).order_by('-date')[0:30]
    elif calendar_year and calendar_month:
        artist_past_event_ids = Event.objects.filter(eventID__in=event_ids).filter(
            date__year=calendar_year, date__month=calendar_month).values_list('eventID', flat=True).order_by('-date')
    else:
        if not past_events_length:
            artist_past_event_ids = Event.objects.filter(eventID__in=event_ids).filter(
                date__lt=datetime.now().date()).values_list('eventID', flat=True).order_by('-date')[
                                    start: start + limit]
        else:
            artist_past_event_ids = Event.objects.filter(eventID__in=event_ids).filter(
                date__lt=datetime.now().date()).values_list('eventID', flat=True).order_by('-date')
    if artist_past_event_ids:
        artist_past_event_ids = [str(x) for x in artist_past_event_ids]
        event_id_string = '('
        event_id_string += ','.join(artist_past_event_ids) + ')'

        past_events = DjMaster.objects.raw(
            "SELECT * FROM dj_Master JOIN dj_Events ON dj_Events.eventID = dj_Master.eventID JOIN dj_Venues ON dj_Venues.venueID = dj_Master.venueID WHERE dj_Master.artistID = {} AND dj_Events.eventID IN {} GROUP BY dj_Events.date,dj_Venues.country ORDER BY dj_Events.date desc".format(
                artist.artistID, event_id_string))
    else:
        past_events = []

    if not past_events_length:
        return past_events
    else:
        return list(past_events)[:20], len(list(past_events))


def get_similar_artist(artist, limit=None):
    similar_artist_id = DjSimilarArtists.objects.filter(artistid=artist.artistID).exclude(linkedartistid=0).values_list(
        'linkedartistid', flat=True)
    similar_artist = Artist.objects.get_queryset().filter(artistID__in=similar_artist_id).distinct()
    if not similar_artist:
        # artist_genre = artist.genre.all()
        # similar_artist = Artist.objects.filter(genre__in=artist_genre).exclude(
        #     artistID=artist.artistID).distinct().order_by('-artistBudget')
        return []

    similar_artist = similar_artist.exclude(artistID=artist.artistID)[:40]
    if limit:
        return similar_artist[:limit]
    else:
        return similar_artist


def get_multiple_similiar_artist(artists):
    similar_artist_id = DjSimilarArtists.objects.filter(artistid__in=[artist.artistID for artist in artists]).exclude(
        linkedartistid=0).values_list('linkedartistid', flat=True)
    similar_artist_id = list(similar_artist_id)

    similar_artist_id.extend([artist.artistID for artist in artists])

    similar_artist = Artist.objects.filter(artistID__in=similar_artist_id).distinct()

    return similar_artist


def get_artist_statistics(artist):
    statistics = {}
    year_before = timezone.now() - timedelta(days=365)

    # facebookdata = FacebookData.objects.filter(artist=artist,
    #                                            fanCount__isnull=False).order_by('created')

    # if facebookdata.exists():
    #     latest_facebookdata = facebookdata

    #     likes = latest_facebookdata.last().fanCount
    #     statistics['facebook'] = {}
    #     statistics['facebook']['likes'] = likes
    #     if latest_facebookdata[0].bio:
    #         statistics['facebook']['biography'] = latest_facebookdata[0].bio
    #     if len(latest_facebookdata) > 1:
    #         old_likes = latest_facebookdata[1].fanCount
    #         if old_likes and likes:
    #             statistics['facebook']['growth'] = get_growth(old_likes, likes)
    #         else:
    #             statistics['facebook']['growth'] = None
    #     else:
    #         statistics['facebook']['growth'] = None

    #     map_data = []
    #     for fb_data in facebookdata:
    #         fb_data.created = fb_data.created + dateutil.relativedelta.relativedelta(months=-1)
    #         map_data.append({'fans': fb_data.fanCount, 'talkingAboutCount': fb_data.talkingAboutCount,'date': fb_data.created.strftime('%Y,%m,%d')})

    #     statistics['facebook']['map_data'] = map_data

    twitterdata = TwitterData.objects.filter(
        artist=artist, followersCount__isnull=False).order_by('created')

    if twitterdata.exists():
        latest_twitterdata = twitterdata
        statistics['twitter'] = {}
        likes = latest_twitterdata.last().followersCount

        statistics['twitter']['likes'] = likes
        if len(latest_twitterdata) > 1:
            old_likes = latest_twitterdata.filter(created__gt=year_before)
            if old_likes:
                old_likes = old_likes[0].followersCount
            if old_likes and likes:
                statistics['twitter']['growth'] = get_growth(old_likes, likes)
            else:
                statistics['twitter']['growth'] = None
        else:
            statistics['twitter']['growth'] = None
        map_data = []
        for tw_data in twitterdata:
            tw_data.created = tw_data.created + dateutil.relativedelta.relativedelta(months=-1)
            map_data.append({'fans': tw_data.followersCount, 'date': tw_data.created.strftime('%Y,%m,%d')})

        statistics['twitter']['map_data'] = map_data

    spotifydata = SpotifyData.objects.filter(
        artist=artist, followersCount__isnull=False).order_by('created')
    if spotifydata.exists():
        latest_spotifydata = spotifydata
        statistics['spotify'] = {}
        likes = latest_spotifydata.last().followersCount
        statistics['spotify']['likes'] = likes
        if len(latest_spotifydata) > 1:
            old_likes = latest_spotifydata.filter(created__gt=year_before)
            if old_likes:
                old_likes = old_likes[0].followersCount
            if old_likes and likes:
                statistics['spotify']['growth'] = get_growth(old_likes, likes)
            else:
                statistics['spotify']['growth'] = None
        else:
            statistics['spotify']['growth'] = None

        map_data = []
        for sp_data in spotifydata:
            sp_data.created = sp_data.created + dateutil.relativedelta.relativedelta(months=-1)
            map_data.append({'fans': sp_data.followersCount, 'date': sp_data.created.strftime('%Y,%m,%d')})

        statistics['spotify']['map_data'] = map_data

    youtubedata = YoutubeData.objects.filter(
        artist=artist, subscriberCount__isnull=False).order_by('created')
    if youtubedata.exists():
        latest_youtubedata = youtubedata
        likes = latest_youtubedata.last().subscriberCount
        if likes:
            statistics['youtube'] = {}
            statistics['youtube']['likes'] = likes
            if len(latest_youtubedata) > 1:
                old_likes = latest_youtubedata.filter(created__gt=year_before)
                if old_likes:
                    old_likes = old_likes[0].subscriberCount
                if old_likes and likes:
                    statistics['youtube']['growth'] = get_growth(old_likes, likes)
                else:
                    statistics['youtube']['growth'] = None
            else:
                statistics['youtube']['growth'] = None

            map_data = []
            for yt_data in youtubedata:
                yt_data.created = yt_data.created + dateutil.relativedelta.relativedelta(months=-1)
                map_data.append({'fans': yt_data.subscriberCount, 'date': yt_data.created.strftime('%Y,%m,%d')})

            statistics['youtube']['map_data'] = map_data
    

    # YOUTUBE VIEWS
    youtubeviews = YoutubeData.objects.filter(
        artist=artist, viewCount__isnull=False).order_by('created')
    if youtubeviews.exists():
        latest_youtubeviews = youtubeviews
        likes = latest_youtubeviews.last().viewCount
        if likes:
            statistics['ytviews'] = {}
            statistics['ytviews']['likes'] = likes
            if len(latest_youtubeviews) > 1:
                old_likes = latest_youtubeviews.filter(created__gt=year_before)
                if old_likes:
                    old_likes = old_likes[0].viewCount
                if old_likes and likes:
                    statistics['ytviews']['growth'] = get_growth(old_likes, likes)
                else:
                    statistics['ytviews']['growth'] = None
            else:
                statistics['ytviews']['growth'] = None

            map_data = []
            for yt_view in youtubeviews:
                yt_view.created = yt_view.created + dateutil.relativedelta.relativedelta(months=-1)
                map_data.append({'views': yt_view.viewCount, 'date': yt_view.created.strftime('%Y,%m,%d')})

            statistics['ytviews']['map_data'] = map_data

    instagramdata = InstagramData.objects.filter(
        artist=artist, followed_by__isnull=False).order_by('created')

    if instagramdata.exists():
        latest_instagramdata = instagramdata
        likes = latest_instagramdata.last().followed_by
        if likes:
            statistics['instagram'] = {}
            statistics['instagram']['likes'] = likes
            if len(latest_instagramdata) > 1:
                old_likes = latest_instagramdata.filter(created__gt=year_before)
                if old_likes:
                    old_likes = old_likes[0].followed_by
                if old_likes and likes:
                    statistics['instagram']['growth'] = get_growth(old_likes, likes)
                else:
                    statistics['instagram']['growth'] = None
            else:
                statistics['instagram']['growth'] = None

            map_data = []
            for insta_data in instagramdata:
                insta_data.created = insta_data.created + dateutil.relativedelta.relativedelta(months=-1)
                map_data.append({'fans': insta_data.followed_by, 'date': insta_data.created.strftime('%Y,%m,%d')})

            statistics['instagram']['map_data'] = map_data

    return statistics


def get_myevents_artist_invite(artist, request):
    all_user_events = WpMyEvents.objects.filter(user_id=request.user.id)
    artist_requested_event_list = all_user_events.filter(invited_users=artist)
    return artist_requested_event_list


def get_artist_news(artist, news_count=5):
    news = News.objects.filter(artists=artist).order_by('-publish_date')[:news_count]
    return news


def get_artist_last_event_near_to_user(artist, request):
    withinmiles = 100
    try:
        user_lat = request.COOKIES['user_session_lat']
        user_long = request.COOKIES['user_session_long']
    except:
        return [], []
    user_lat = float(user_lat)
    user_long = float(user_long)
    past_happend_events = []
    future_happening_events = []
    artist_id = "(" + str(artist.artistID) + ")"
    all_artist_events = DjMaster.objects.raw(
        "SELECT * FROM dj_Master JOIN dj_Events ON dj_Events.eventID = dj_Master.eventID JOIN dj_Venues ON dj_Venues.venueID = dj_Master.venueID WHERE dj_Master.artistID IN {} ORDER BY dj_Events.date DESC".format(
            artist_id))
    for event in all_artist_events:
        if event.venueLatitude != 0E-9 and event.venueLongitude != 0E-9 and event.venueLatitude != None and event.venueLongitude != None:
            venue_latitude = event.venueLatitude
            venue_longitude = event.venueLongitude

            distance = distance_using_lati_long(user_lat, user_long, float(venue_latitude), float(venue_longitude))

            if distance <= withinmiles:
                if datetime.now().date() < event.date and event.date:
                    future_happening_events.append(event)
                else:
                    past_happend_events.append(event)

    if past_happend_events:
        past_happend_events = [past_happend_events[0]]
    return past_happend_events, future_happening_events


@login_required
@check_plan('Trial', 'Promoter', 'Artist/DJ', 'Agent')
@check_email_validated
def artist_detail(request, artist_id):
    start_time = time.time()
    today_date = datetime.now()
    free_online_dates = []

    if request.user.is_authenticated():
        pro_obj = Profile.objects.filter(user=request.user).first()
    else:
        pro_obj = None

    global limit
    context = {}
    all_events = WpMyEvents.objects.filter(user_id=request.user.id)
    try:
        artist = Artist.objects.get_queryset().get(artistID=artist_id)
    except Artist.DoesNotExist:
        raise Http404()
    
    # Checking if the artist is managed by the user
    if pro_obj:
        user_managed_artists = pro_obj.profile_managed_djs.all()
        if artist in user_managed_artists:
            context.update({
                'managed_by_user': True
            })
        else:
            context.update({
                'managed_by_user': False
            })

        # Getting General Artist
        general_artist = GeneralArtist.objects.filter(artistID=artist, user=request.user).first()

        # Getting Artist PressKit for promo
        if general_artist:
            artist_presskits = ArtistPresskit.objects.filter(general_artist=general_artist)
            context.update({
                'press_kit': artist_presskits,
            })

            artist_creative_info = ArtistCreativeInfo.objects.filter(general_artist=general_artist).first()
            if artist_creative_info:
                context.update({
                    'press_kit_titlename': artist_creative_info.press_kit_titlename,
                    'press_kit_url': artist_creative_info.press_kit_url
                })

        # Getting Artist media
        artist_media = ArtistMedia.objects.filter(artistID=artist)

        context.update({
            'dropbox_media_count': artist_media.filter(media_type="Dropbox").count(),
            'soundcloud_media_count': artist_media.filter(media_type="Soundcloud").count(),
            'youtube_media_count': artist_media.filter(media_type="Youtube").count(),
        })

        if artist_media:
            context.update({
                'artist_media': artist_media,
                'all_artist_media': len(artist_media)
            })

        # Getting Artist Photos
        artist_photos = ArtistPhoto.objects.filter(artistID=artist)
        if artist_photos:
            context.update({
                'artist_photos': artist_photos
            })

        # offersheet
        offersheet = Offersheet.objects.filter(artist=artist)
        if offersheet:
            context.update({
                'offersheet': offersheet.first()
            })


        
    # Comments part - Start

    comments = Comment.objects.filter(artist=artist, reply=None).order_by('-id')
    comment_form= CommentForm
    context.update({
        'comments':comments,
        'comment_form':comment_form})

    # Comments part -End

    # Connections part Start

    artist_connections = ProfileConnections.objects.filter(artist=artist)
    artist_in_connections = ProfileConnections.objects.filter(artist=artist, requested_by=request.user).first()
    
    if artist_in_connections and (artist_in_connections in artist_connections):
        context.update({
            'register_artist': False,
        })  
    else:
        context.update({
            'register_artist': True,
        })  

    context.update({
        'artist_connections': artist_connections.filter(status=1),
    })
    # Connections part End
    try:
        artistweekendstatus = ArtistWeekendStatus.objects.filter(artist=artist).values_list("booked_percentage",
                                                                                            flat=True)

        if len(artistweekendstatus) == 53:
            artist_weekend_total = 0

            for percentage in artistweekendstatus:
                artist_weekend_total += percentage

            artist_weekend_total_percentage = artist_weekend_total / len(artistweekendstatus)

            context.update({"artist_weekend_total_percentage": round(artist_weekend_total_percentage, 1)})

            winter_season_weekends = artistweekendstatus[0:9]
            winter_season_weekends_total = 0
            for percentage in winter_season_weekends:
                winter_season_weekends_total += percentage

            spring_season_weekends = artistweekendstatus[9:22]
            spring_season_weekends_total = 0
            for percentage in spring_season_weekends:
                spring_season_weekends_total += percentage

            spring_season_weekends_total_percentage = spring_season_weekends_total / len(spring_season_weekends)

            summer_season_weekends = artistweekendstatus[22:35]
            summer_season_weekends_total = 0
            for percentage in summer_season_weekends:
                summer_season_weekends_total += percentage

            summer_season_weekends_total_percentage = summer_season_weekends_total / len(summer_season_weekends)

            autumn_season_weekends = artistweekendstatus[35:48]
            autumn_season_weekends_total = 0

            for percentage in autumn_season_weekends:
                autumn_season_weekends_total += percentage

            autumn_season_weekends_total_percentage = autumn_season_weekends_total / len(autumn_season_weekends)

            dec_winter_season_weekend = artistweekendstatus[48:53]
            for percentage in dec_winter_season_weekend:
                winter_season_weekends_total += percentage

            winter_season_weekends_total_percentage = winter_season_weekends_total / (
                        len(winter_season_weekends) + len(dec_winter_season_weekend))

            context.update({
                'winter_season_weekends_total_percentage': round(winter_season_weekends_total_percentage, 1),
                'spring_season_weekends_total_percentage': round(spring_season_weekends_total_percentage, 1),
                'summer_season_weekends_total_percentage': round(summer_season_weekends_total_percentage, 1),
                'autumn_season_weekends_total_percentage': round(autumn_season_weekends_total_percentage, 1)
            })

    except Artist.DoesNotExist:
        raise Http404()

    general_manager = artist.general_manager
    if general_manager:
        general_manager = general_manager.replace(" ", "")
        general_manager_list = general_manager.split(",")

        context.update({"general_manager_list": general_manager_list})

    message_str = request.GET.get('message_str', '')

    if message_str:
        messages_list = message_str.split(" > ")
        for message in messages_list:
            try:
                if "ON DATE" in message:
                    date = message.split("ON DATE ")[1]
                    if "MOST POPULAR ON" in date:
                        date = date.replace(" MOST POPULAR ON", "")
                    single_date = datetime.strptime(date, "%a %d %b %Y")
                    upcoming_events = get_upcoming_events_dates(artist, single_date)
                    if upcoming_events:
                        for event in upcoming_events:
                            event_country = event.country
                            free_online_dates.append(
                                {"date": "%s" % single_date.strftime("%b %d, %Y"), "status": "Booked",
                                 "country": event_country})
                    else:
                        free_online_dates.append(
                            {"date": "%s" % single_date.strftime("%b %d, %Y"), "status": "Free Online"})

                    break
                if "BETWEEN DATE" in message:
                    dates = message.replace("BETWEEN DATE", "").split("AND")
                    search_date1 = datetime.strptime(dates[0].strip(), "%a %d %b %Y")
                    search_date2 = dates[1]
                    if "MOST POPULAR ON" in search_date2:
                        search_date2 = search_date2.replace(" MOST POPULAR ON", "")
                    search_date2 = datetime.strptime(search_date2.strip(), "%a %d %b %Y")

                    for single_date in (search_date1 + timedelta(n) for n in
                                        range(int((search_date2 - search_date1).days) + 1)):
                        upcoming_events = get_upcoming_events_dates(artist, single_date)
                        if upcoming_events:
                            for event in upcoming_events:
                                event_country = event.country
                                free_online_dates.append(
                                    {"date": "%s" % single_date.strftime("%b %d, %Y"), "status": "Booked",
                                     "country": event_country})
                        else:
                            free_online_dates.append(
                                {"date": "%s" % single_date.strftime("%b %d, %Y"), "status": "Free Online"})

                    break
            except:
                pass

    if message_str:
        message_string = message_str
        message_string = message_str.replace("YOUR SEARCH RESULTS FOR > ", "")
    else:
        message_string = None

    try:
        # artist = Artist.allartistobjects.get(artistID=artist_id)
        artist_genres = ArtistGenre.objects.filter(artistID=artist)
    except:
        raise Http404()

    social_urls = get_artist_social_url(artist, True)

    statistics = get_artist_statistics(artist)
    similar_artists_id = DjSimilarArtists.objects.filter(artistid=artist.artistID).values_list("linkedartistid",
                                                                                               flat=True)
    similar_artists = Artist.objects.filter(artistID__in=similar_artists_id)
    if len(similar_artists) == 0:
        similar_artists = get_similar_artist(artist)
    upcoming_events_data = get_upcoming_events(artist, upcoming_events_length=True)
    upcoming_events = upcoming_events_data[0]
    upcoming_events_length = upcoming_events_data[1]
    past_events_data = get_past_events(artist, past_events_length=True)
    past_events = past_events_data[0]
    old_events_length = past_events_data[1]
    latest_news = get_artist_news(artist)
    # artist_top_ten_country = get_user_top_country_an_likes(artist)

    past_happend_events, future_happening_events = get_artist_last_event_near_to_user(artist, request)
    artist_requested_event_list = get_myevents_artist_invite(artist, request)

    fav_venues = pro_obj.favorite_venues.all()
    fav_venues_list = []
    for venue in fav_venues:
        fav_venues_list.append(venue.venuename)
    fav_events = pro_obj.favorite_events.all()
    fav_events_list = []
    for ev in fav_events:
        fav_events_list.append(ev.eventID)

    youtubedata = YoutubeData.objects.filter(
        artist=artist, subscriberCount__isnull=False).order_by('-created')
    
    context.update({
        'artist': artist,
        'pro_obj': pro_obj,
        'artist_genres': artist_genres,
        'statistics': statistics,
        'social_urls': social_urls,
        'upcoming_events': upcoming_events,
        'upcoming_events_length': upcoming_events_length,
        'old_events': past_events,
        'old_events_length': old_events_length,
        'start': limit,
        'similar_artists': similar_artists,
        'message_string': message_string,
        'all_events': all_events,
        'past_happend_events': past_happend_events,
        'future_happening_events': future_happening_events,
        'artist_requested_event_list': artist_requested_event_list,
        'free_online_dates': free_online_dates,
        'latest_news': latest_news,
        'fav_venues_list': fav_venues_list,
        'fav_events_list': fav_events_list,
        # 'artist_top_ten_country': artist_top_ten_country,
        "site_url": settings.SITE_URL,
        'show_loader': True,
        'youtubedata':youtubedata,
    })

    if request.user.is_authenticated():
        data = {'artist':artist, 'user':request.user}
        form_note = UserNoteForm(initial=data)
        notes = UserNote.objects.filter(artist=artist,user=request.user).order_by('-modified_date')
        context.update({
            'notes':notes,
            'form_note':form_note,
        })

    if request.user.is_superuser:
        week_before_today = today_date - timedelta(7)
        events_id_list = DjMaster.objects.filter(artistid=artist.artistID).values_list('eventid', flat=True)
        events_list = Event.objects.filter(eventID__in=events_id_list)

        if events_list:
            bandsintown_events_count = events_list.filter(bandsintownID__gt=0).values_list("date",
                                                                                           flat=True).distinct().count()
            bandsintown_events_future_count = events_list.filter(bandsintownID__gt=0, date__gt=today_date).values_list(
                "date", flat=True).distinct().count()
            bandsin_town_added_this_week = events_list.filter(bandsintownID__gt=0, created__gt=week_before_today,
                                                              created__lte=today_date).values_list("date",
                                                                                                   flat=True).distinct().count()

            resident_events_count = events_list.filter(residentID__gt=0).values_list("date",
                                                                                     flat=True).distinct().count()
            resident_events_future_count = events_list.filter(residentID__gt=0, date__gt=today_date).values_list("date",
                                                                                                                 flat=True).distinct().count()
            resident_town_added_this_week = events_list.filter(residentID__gt=0, created__gt=week_before_today,
                                                               created__lte=today_date).values_list("date",
                                                                                                    flat=True).distinct().count()

            gigatools_events_count = events_list.filter(gigatoolsID__gt=0).values_list("date",
                                                                                       flat=True).distinct().count()
            gigatools_events_future_count = events_list.filter(gigatoolsID__gt=0, date__gt=today_date).values_list(
                "date", flat=True).distinct().count()
            gigatools_town_added_this_week = events_list.filter(gigatoolsID__gt=0, created__gt=week_before_today,
                                                                created__lte=today_date).values_list("date",
                                                                                                     flat=True).distinct().count()

            songkick_events_count = events_list.filter(songkickID__gt=0).values_list("date",
                                                                                     flat=True).distinct().count()
            songkick_events_future_count = events_list.filter(songkickID__gt=0, date__gt=today_date).values_list("date",
                                                                                                                 flat=True).distinct().count()
            songkick_town_added_this_week = events_list.filter(songkickID__gt=0, created__gt=week_before_today,
                                                               created__lte=today_date).values_list("date",
                                                                                                    flat=True).distinct().count()

            facebook_events_count = events_list.filter(facebookID__gt=0).values_list("date",
                                                                                     flat=True).distinct().count()
            facebook_events_future_count = events_list.filter(facebookID__gt=0, date__gt=today_date).values_list("date",
                                                                                                                 flat=True).distinct().count()
            facebook_town_added_this_week = events_list.filter(facebookID__gt=0, created__gt=week_before_today,
                                                               created__lte=today_date).values_list("date",
                                                                                                    flat=True).distinct().count()

            context.update({
                "bandsintown_events_count": bandsintown_events_count,
                "bandsintown_events_future_count": bandsintown_events_future_count,
                "bandsin_town_added_this_week": bandsin_town_added_this_week,

                "resident_events_count": resident_events_count,
                "resident_events_future_count": resident_events_future_count,
                "resident_town_added_this_week": resident_town_added_this_week,

                "gigatools_events_count": gigatools_events_count,
                "gigatools_events_future_count": gigatools_events_future_count,
                "gigatools_town_added_this_week": gigatools_town_added_this_week,

                "songkick_events_count": songkick_events_count,
                "songkick_events_future_count": songkick_events_future_count,
                "songkick_town_added_this_week": songkick_town_added_this_week,

                "facebook_events_count": facebook_events_count,
                "facebook_events_future_count": facebook_events_future_count,
                "facebook_town_added_this_week": facebook_town_added_this_week,
            })
    
    # Adding General Artist Part

    general_artist = GeneralArtist.objects.filter(artistID=artist,user=request.user,status=1).first()
    artist_media = ArtistMedia.objects.filter(artistID=artist)
    offers = list(Offer.objects.filter(artist=artist, is_validated=True ))
    offer_categories = Offersheet.objects.filter(artist = artist).first()
    allowed_categories = []
    half_subjects = 0
    if offer_categories is not None:
        allowed_categories = list(offer_categories.subcategories.all())
        half_subjects = (len(list(offer_categories.subcategories.all()))/2)
    new_offers_numbers = len(list(Offer.objects.filter(artist=artist, is_validated=True,status=0 )))
    art_travelinfo = []
    art_tech_rider_info = []
    if general_artist:
        art_travelinfo = ArtistTravelDoc.objects.filter(general_artist=general_artist)
    artist_travel_doc_media = None
    if artist_media.filter(media_type="TravelDoc").exists():
        artist_travel_doc_media = artist_media.filter(media_type="TravelDoc")[0]
    art_tech_rider_info  = ArtistTechRider.objects.filter(general_artist=general_artist)

    context.update({
        'general_artist':general_artist,
        'art_travelinfo':art_travelinfo,
        'all_art_travelinfo': len(art_travelinfo),
        'artist_travel_doc_media':artist_travel_doc_media,
        'art_tech_rider_info':art_tech_rider_info,
        'all_art_tech_rider_info': len(art_tech_rider_info),
        'submitted_offers': offers,
        'new_offers_number': new_offers_numbers,
        'allowed_categories': allowed_categories,
        'half_subjects' : half_subjects
    })

    # Shortlist venues:
    all_venues = []
    all_shortlisted_events = []
    if artist.shortlistedvenue_set.filter(user=request.user).first():
        all_venues = artist.shortlistedvenue_set.filter(user=request.user).first().venueids.all()
    if artist.shortlistedevent_set.filter(user=request.user).first():
        all_shortlisted_events = artist.shortlistedevent_set.filter(user=request.user).first().eventids.all()
    context.update({
        'all_venues':len(all_venues),
        'all_shortlisted_events':len(all_shortlisted_events),
    })

    end_time = time.time()

    print("time for execution ++++++++++++++++++++++++++++++++++++")

    print(end_time - start_time)

    return render(request, 'artist/detail.html', context)


@login_required
@check_email_validated
# @check_plan('Trial','Basic','Premium')
@check_plan('Trial', 'Promoter', 'Artist/DJ', 'Agent')
def load_events(request, artist_id):
    artist = Artist.objects.get(artistID=artist_id)
    start = request.GET.get('start')
    action = request.GET.get('action', '')
    dj_artist = request.GET.get('dj_artist', '')
    if action == "upcoming":
        events = get_upcoming_events(artist, start=int(start))
    else:
        events = get_past_events(artist, start=int(start))

    pro_obj = Profile.objects.get(user=request.user)
    fav_venues = pro_obj.favorite_venues.all()
    fav_venues_list = []
    for venue in fav_venues:
        fav_venues_list.append(venue.venuename)
    fav_events = pro_obj.favorite_events.all()
    fav_events_list = []
    for ev in fav_events:
        fav_events_list.append(ev.eventID)
    
    # Shortlist venues:
    global limit
    all_venues = []
    if artist.shortlistedvenue_set.all().first():
        all_venues = artist.shortlistedvenue_set.all().first().venueids.all()

    context = { "events": events,
                "artist": artist, 
                "fav_venues_list": fav_venues_list,
                "fav_events_list": fav_events_list,
                "all_venues": all_venues
                }

    if dj_artist:
        template = render_to_string("dashboard/dj_dashboard_get_events.html", context,
                                    context_instance=RequestContext(request))
    else:
        template = render_to_string("artist/get_events.html", context, request= request)

    start = int(start) + limit
    if events:
        status = "True"  # More events
    else:
        status = "False"  # No More events

    response = {
        "status": status,
        "start": start,
        "data": template,
    }

    return HttpResponse(json.dumps(response), content_type="applicaton/json")


@login_required
@check_email_validated
# @check_plan('Trial','Basic','Premium')
@check_plan('Trial', 'Artist/DJ', 'Promoter', 'Agent')
def artist_map(request):
    artist = request.GET.get('artist')
    if artist:
        artist = Artist.objects.get(artistID=artist)
        events = get_upcoming_events(artist, map_event=True)
        events = list(events)
        past_events = get_past_events(artist, map_event=True)
        for event in past_events:
            events.append(event)
        data_list = []

        artist_info = {}
        artist_info['id'] = artist.artistID
        artist_info['artist'] = artist.artistName

        try:
            sourceImage = Artist.objects.get(artistID=artist.artistID).artistPhoto.url
        except Exception as e:
            sourceImage = None
        if sourceImage == None:
            try:
                sourceImage = ScrapePhoto.objects.get(artistID=artist.artistID).flipImage
                sourceImage = "/media%s" % (sourceImage)
            except Exception as e:
                sourceImage = ""
        artist_info['artist_image'] = sourceImage

        all_events = []
        for event in events:
            e = {}
            e['eventID'] = event.eventid.eventID
            e['eventName'] = event.eventName
            e['date'] = str(event.date)
            e['startTime'] = event.startTime
            e['endTime'] = event.endTime
            if event.date < datetime.now().date():
                e['eventType'] = "past"
            else:
                e['eventType'] = "future"
            v = {}
            v['venueID'] = str(event.venueid)
            v['venueName'] = event.venueName
            v['venueAddress'] = event.venueAddress
            if event.venueLatitude == 0E-9:
                v['venueLatitude'] = "0.000000000"
            else:
                v['venueLatitude'] = str(event.venueLatitude)
            if event.venueLongitude == 0E-9:
                v['venueLongitude'] = "0.000000000"
            else:
                v['venueLongitude'] = str(event.venueLongitude)
            v['city'] = event.city
            v['country'] = event.country
            event_dict = {}
            event_dict['eventDetail'] = e
            event_dict['venueDetail'] = v
            all_events.append(event_dict)
        data = {}
        data['artistDetail'] = artist_info
        data['events'] = all_events
        data_list = [data]
    else:
        data_list = []
    return HttpResponse(json.dumps(data_list))


@login_required
@check_email_validated
# @check_plan('Trial','Basic','Premium')
@check_plan('Trial', 'Artist/DJ', 'Promoter', 'Agent')
def add_or_remove_favorite(request):
    response = {"status": False, "errors": []}

    if request.is_ajax() and request.POST['action'] == 'create_favorite':
        artist_id = request.POST['id']
        artist = Artist.objects.get(artistID=artist_id)
        profile = Profile.objects.get(user=request.user)
        profile.favorite_artist.add(artist)
        profile.save()
        response["message"] = "%s added to favorites" % artist.artistName

    if request.is_ajax() and request.POST['action'] == 'remove_favorite':
        artist_id = request.POST['id']
        artist = Artist.objects.get(artistID=artist_id)
        profile = Profile.objects.get(user=request.user)
        profile.favorite_artist.remove(artist)
        profile.save()
        response["message"] = "%s removed from favorites" % artist.artistName

    response["status"] = True
    response["id"] = artist_id
    return HttpResponse(json.dumps(response))


@login_required
@check_email_validated
# @check_plan('Trial','Basic','Premium')
@check_plan('Trial', 'Artist/DJ', 'Promoter', 'Agent')
def add_or_remove_shortlist(request):
    response = {"status": False, "errors": []}

    if request.is_ajax() and request.POST['action'] == 'add_to_shortlist':
        artist_id = request.POST['id']
        event_id = request.POST["event_id"]
        if event_id:
            event = WpMyEvents.objects.get(event_id=event_id)
            artist = Artist.objects.get(artistID=artist_id)
            event.shortlisted_artists.add(artist)

    # if request.is_ajax() and request.POST['action'] == 'remove_from_shortlist':
    #     artist_id = request.POST['id']
    #     artist = Artist.objects.get(artistID=artist_id)
    #     profile = Profile.objects.get(user=request.user)
    #     profile.shortlist_artist.remove(artist)
    #     profile.save()

    response["status"] = True
    response["id"] = artist_id
    return HttpResponse(json.dumps(response))


def prioritize_favourtie_artist(request, allartists):
    if request.user.is_authenticated():
        profile_obj = Profile.objects.get(user=request.user)
        if profile_obj.favorite_artist:
            allartists_id = list(allartists.values_list('artistID', flat=True))
            favorites_artist_ids = list(profile_obj.favorite_artist.values_list('artistID', flat=True))
            for fav_id in favorites_artist_ids:
                if fav_id in allartists_id:
                    allartists_id.remove(fav_id)
                    allartists_id.insert(0, fav_id)
            allartists_id = [int(artist_id) for artist_id in allartists_id]
            clauses = ' '.join(['WHEN artistID=%s THEN %s' % (pk, i) for i, pk in enumerate(allartists_id)])
            ordering = 'CASE %s END' % clauses
            allartists = Artist.objects.filter(artistID__in=allartists_id).extra(select={'ordering': ordering},
                                                                                 order_by=('ordering',))

    return allartists


@login_required
@check_email_validated
# @check_plan('Trial','Premium')
@check_plan('Trial', 'Artist/DJ', 'Promoter', 'Agent')
def load_search_artist(request):
    response = {'status': "FAILURE"}
    if request.is_ajax():
        all_artist_id = request.GET.get('all_artist_id', '')
        message_str = request.GET.get('message_str', '')
        all_artist = []
        if request.user.is_authenticated():
            pro_obj = Profile.objects.get(user=request.user)
        else:
            pro_obj = None

        all_artist_id = all_artist_id.replace("[", "").replace("]", "")
        all_artist_id = all_artist_id.split(",")
        all_artist_id = [int(id) for id in all_artist_id]

        for id in all_artist_id:
            artist_dict = {}
            try:
                artist = Artist.objects.get(artistID=int(id))
            except:
                continue
            artist_dict['upcoming_events'] = get_upcoming_events(artist)
            # artist_dict['statistics'] = get_artist_statistics(artist)
            artist_dict['social_urls'] = get_artist_social_url(artist)
            artist_dict['similar_artists'] = get_similar_artist(artist, limit=12)
            artist_dict['artist_requested_event_list'] = get_myevents_artist_invite(artist, request)
            artist_dict['artist_obj'] = artist
            artist_dict['free_dates'] = []

            if message_str:
                messages_list = message_str.split(" &gt; ")
                for message in messages_list:
                    if "ON DATE" in message:
                        date = message.split("ON DATE ")[1]
                        if "MOST POPULAR ON" in date:
                            date = date.replace(" MOST POPULAR ON", "")
                        single_date = datetime.strptime(date, "%a %d %b %Y")
                        event_ids = DjMaster.objects.filter(artistid=int(id)).values_list('eventid', flat=True)
                        artist_event = Event.objects.filter(eventID__in=event_ids, date=single_date)
                        if not artist_event:
                            artist_dict['free_dates'].append(
                                {"date": "%s" % single_date.strftime("%b %d, %Y"), "status": "Free Online"})
                        else:
                            artist_dict['free_dates'].append(
                                {"date": "%s" % single_date.strftime("%b %d, %Y"), "status": "Booked"})
                        break
                    if "BETWEEN DATE" in message:
                        dates = message.replace("BETWEEN DATE", "").split("AND")
                        search_date1 = datetime.strptime(dates[0].strip(), "%a %d %b %Y")
                        search_date2 = dates[1]
                        if "MOST POPULAR ON" in search_date2:
                            search_date2 = search_date2.replace(" MOST POPULAR ON", "")
                        search_date2 = datetime.strptime(search_date2.strip(), "%a %d %b %Y")
                        for single_date in (search_date1 + timedelta(n) for n in
                                            range(int((search_date2 - search_date1).days))):
                            event_ids = DjMaster.objects.filter(artistid=int(id)).values_list('eventid', flat=True)
                            artist_event = Event.objects.filter(eventID__in=event_ids, date=single_date)
                            if not artist_event:
                                artist_dict['free_dates'].append(
                                    {"date": "%s" % single_date.strftime("%b %d, %Y"), "status": "Free Online"})
                            else:
                                artist_dict['free_dates'].append(
                                    {"date": "%s" % single_date.strftime("%b %d, %Y"), "status": "Booked"})
                        break

            all_artist.append(artist_dict)

        context = {
            "all_artist": all_artist,
            "pro_obj": pro_obj,
        }
        template = render_to_string("artist/search_result_ajax.html", context, context_instance=RequestContext(request))

        map_template = render_to_string("artist/search_result_ajax_map.html", context,
                                        context_instance=RequestContext(request))

        response['template'] = template
        response['map_template'] = map_template
        response['status'] = "SUCCESS"
        return HttpResponse(json.dumps(response), content_type="applicaton/json")

    return HttpResponse(json.dumps(response), content_type="applicaton/json")


@login_required
@check_email_validated
# @check_plan('Trial','Premium')
@check_plan('Trial', 'Promoter', 'Artist/DJ', 'Agent')
def artist_search(request):
    all_events = WpMyEvents.objects.filter(user_id=request.user.id)
    if request.user.is_authenticated():
        pro_obj = Profile.objects.get(user=request.user)
    else:
        pro_obj = None

    artist_id = request.GET.getlist('artists', '')
    message_str = request.GET.get('message_str', '')
    if message_str:
        message_string = message_str
    else:
        message_string = ""

    all_artist_id = [int(id) for id in artist_id]

    all_artist_name = Artist.objects.filter(artistID__in=all_artist_id).values_list('artistName', flat=True)

    all_artist_name = (", ").join(all_artist_name)
    context = {
        'all_artist_name': all_artist_name,
        'pro_obj': pro_obj,
        'message_string': message_string,
        'all_events': all_events,
        'all_artist_id': all_artist_id,
        'all_artist_name': all_artist_name,
    }

    return render(request, 'artist/search_result.html', context)


@login_required
# @check_plan('Trial','Basic','Premium')
@check_email_validated
@check_plan('Trial', 'Promoter', 'Artist/DJ', 'Agent')
def artist_compare_dj(request):
    if request.GET.get('artist1', False) and request.GET.get('artist2', False):
        artist_ids = request.GET.getlist('artist1', '') + request.GET.getlist('artist2', '')
    elif request.GET.get('artists', False):
        artist_ids = request.GET.getlist('artists', '')
    else:
        return HttpResponseRedirect("/artists/compare/")
    artist_heading = []
    onlyartist_names = []
    compare_artist = []
    fanbase_charts = []
    countries_charts = []
    bar_charts = {}
    facebook_like_graph = []

    bar_charts['Facebook'] = {
        "text": "Facebook Fan Count",
        "msg": "",
        "msg_list": [],
        "values": [],
        "backgroundColor": "#3b5998"
    }
    bar_charts['Twitter'] = {
        "text": "Twitter Followers Count",
        "msg": "",
        "msg_list": [],
        "values": [],
        "backgroundColor": "#55acee"
    }
    bar_charts['Spotify'] = {
        "text": "Spotify Listeners Count",
        "msg": "",
        "msg_list": [],
        "values": [],
        "backgroundColor": "#2ebd59"
    }
    bar_charts['Youtube'] = {
        "text": "Youtube Subscribers Count",
        "msg": "",
        "msg_list": [],
        "values": [],
        "backgroundColor": "#b31217"
    }
    bar_charts['Events'] = {
        "text": "Events Past 12 Months",
        "msg": "",
        "msg_list": [],
        "values": [],
        "backgroundColor": "#85929E"
    }
    bar_charts['Instagram'] = {
        "text": "Instagram Followed by",
        "msg": "",
        "msg_list": [],
        "values": [],
        "backgroundColor": "#85929E"
    }

    for index, artist_id in enumerate(artist_ids):
        artist_object = Artist.objects.get(artistID=artist_id)

        event_ids = DjMaster.objects.filter(artistid=artist_id).values_list('eventid', flat=True)
        venue_ids = DjMaster.objects.filter(artistid=artist_id, eventid__date__lte=datetime.now().date(),
                                            eventid__date__gte=datetime.now().date() - timedelta(days=365)).values_list(
            'venueid', flat=True)
        dj_venues = DjVenues.objects.filter(venueid__in=venue_ids).values('country').annotate(
            e_count=Count('country')).order_by('-e_count')[:10]
        eventsCount = Event.objects.filter(eventID__in=event_ids).filter(date__lte=datetime.now().date(),
                                                                         date__gte=datetime.now().date() - timedelta(
                                                                             days=365)).count()
        bar_charts['Events']['values'].append(int(eventsCount))

        if int(eventsCount) == 0:
            bar_charts['Events']['msg_list'].append(str(artist_object.artistName))

        try:
            sourceImage = Artist.objects.get(artistID=artist_id).artistPhoto.url
        except Exception as e:
            sourceImage = None
        if sourceImage == None:
            try:
                sourceImage = ScrapePhoto.objects.get(artistID=artist_id).flipImage
                sourceImage = "/media%s" % (sourceImage)
            except Exception as e:
                sourceImage = ""

        artist_heading.append({
            "name": artist_object.artistName,
            "profile_image": sourceImage
        })

        onlyartist_names.append({
            "text": artist_object.artistName,
            "font-size": "12",
            "hook": "scale:name=scale-x,index=%s" % (index),
            "offsetX": -80
        })
        youtube_obj = YoutubeData.objects.filter(artist=artist_id).values_list('subscriberCount', flat=True)
        subscriberCount = max(youtube_obj) if youtube_obj and not all(item is None for item in youtube_obj) else 0
        bar_charts['Youtube']['values'].append(int(subscriberCount))
        if int(subscriberCount) == 0:
            bar_charts['Youtube']['msg_list'].append(str(artist_object.artistName))

        twitter_obj = TwitterData.objects.filter(artist=artist_id).values('followersCount', 'friendsCount',
                                                                          'favouritesCount')
        followersCount = [x['followersCount'] for x in twitter_obj]
        followersCount1 = max(followersCount) if followersCount and not all(
            item is None for item in followersCount) else 0
        bar_charts['Twitter']['values'].append(int(followersCount1))
        if int(followersCount1) == 0:
            bar_charts['Twitter']['msg_list'].append(str(artist_object.artistName))

        facebook_obj = FacebookData.objects.filter(artist=artist_id).values('fanCount', 'talkingAboutCount')
        fanCount = [x['fanCount'] for x in facebook_obj]
        fanCount = max(fanCount) if fanCount and not all(item is None for item in fanCount) else 0
        if type(fanCount) != int:
            fanCount = fanCount.replace(',', '')
        print(type(fanCount))
        bar_charts['Facebook']['values'].append(int(fanCount))
        if int(fanCount) == 0:
            bar_charts['Facebook']['msg_list'].append(str(artist_object.artistName))

        spotify_obj = SpotifyData.objects.filter(artist=artist_id).values('followersCount', 'popularityIndex')
        followersCount = [x['followersCount'] for x in spotify_obj]
        followersCount = max(followersCount) if followersCount and not all(
            item is None for item in followersCount) else 0
        bar_charts['Spotify']['values'].append(int(followersCount))
        if int(followersCount) == 0:
            bar_charts['Spotify']['msg_list'].append(str(artist_object.artistName))

        instagram_obj = InstagramData.objects.filter(artist=artist_id).values_list('followed_by', flat=True)
        followed_by = max(instagram_obj) if instagram_obj and not all(item is None for item in instagram_obj) else 0
        bar_charts['Instagram']['values'].append(int(followed_by))
        if int(followed_by) == 0:
            bar_charts['Instagram']['msg_list'].append(str(artist_object.artistName))

        compare_artist.append({
            "text": artist_object.artistName,
            "values": [int(fanCount) + int(followersCount1) + int(subscriberCount) + int(followersCount) + int(
                eventsCount) + int(followed_by)],
        })
        data = []
        for item in dj_venues:
            country = str(item['country'])
            final_country = None
            if country:
                if "\n" in country:
                    refined_country = country.split("\n")
                    final_country = refined_country[1]
                else:
                    final_country = str(item['country'])
            data.append({
                "text": final_country,
                "values": [int(item['e_count'])],
            })

        countries_charts.append({
            "name": str(artist_object.artistName),
            "data": data,
        })

        fanbase_charts.append({
            "name": str(artist_object.artistName),
            "id": str(artist_object.artistID),
            "profile_image": str(sourceImage),
            "data": [
                {
                    "text": "Facebook",
                    "values": int(fanCount),
                },
                {
                    "text": "Twitter",
                    "values": int(followersCount1),
                },
                {
                    "text": "Youtube",
                    "values": int(subscriberCount),
                },
                {
                    "text": "Spotify",
                    "values": int(followersCount),
                },
                {
                    "text": "Events",
                    "values": int(eventsCount),
                },
                {
                    "text": "Instagram",
                    "values": int(followed_by),
                }
            ]})

        data = []
        try:
            created_at = FacebookLike.objects.filter(artist_id=artist_id).order_by('-created_at')[0].created_at
            facebook_likes = FacebookLike.objects.filter(artist_id=artist_id, created_at=created_at).order_by(
                '-like_count')[:10]
            for facebook_like in facebook_likes:
                data.append({
                    "text": str(facebook_like.country_name),
                    "values": [int(facebook_like.like_count)],
                })
            facebook_like_graph_title = "%s - FACEBOOK FANS BY COUNTRY " % (artist_object.artistName)

            facebook_like_graph.append({
                "name": str(facebook_like_graph_title),
                "id": str(artist_object.artistID),
                "data": data,
            })
        except Exception as e:
            facebook_like_graph_title = "%s - FACEBOOK FANS BY COUNTRY " % (artist_object.artistName)
            facebook_like_graph_title_msg = "Sorry currently there is no data available"
            facebook_like_graph.append({
                "name": str(facebook_like_graph_title),
                "msg": str(facebook_like_graph_title_msg),
                "id": str(artist_object.artistID),
                "data": data,
            })

    if bar_charts['Events']['msg_list']:
        bar_charts['Events']['msg'] = ",".join(bar_charts['Events']['msg_list'])
        bar_charts['Events']['msg'] = "%s = Sorry currently there is no data available" % bar_charts['Events']['msg']
    if bar_charts['Youtube']['msg_list']:
        bar_charts['Youtube']['msg'] = ",".join(bar_charts['Youtube']['msg_list'])
        bar_charts['Youtube']['msg'] = "%s = Sorry currently there is no data available" % bar_charts['Youtube']['msg']
    if bar_charts['Twitter']['msg_list']:
        bar_charts['Twitter']['msg'] = ",".join(bar_charts['Twitter']['msg_list'])
        bar_charts['Twitter']['msg'] = "%s = Sorry currently there is no data available" % bar_charts['Twitter']['msg']
    if bar_charts['Facebook']['msg_list']:
        bar_charts['Facebook']['msg'] = ",".join(bar_charts['Facebook']['msg_list'])
        bar_charts['Facebook']['msg'] = "%s = Sorry currently there is no data available" % bar_charts['Facebook'][
            'msg']
    if bar_charts['Spotify']['msg_list']:
        bar_charts['Spotify']['msg'] = ",".join(bar_charts['Spotify']['msg_list'])
        bar_charts['Spotify']['msg'] = "%s = Sorry currently there is no data available" % bar_charts['Spotify']['msg']
    if bar_charts['Instagram']['msg_list']:
        bar_charts['Instagram']['msg'] = ",".join(bar_charts['Instagram']['msg_list'])
        bar_charts['Instagram']['msg'] = "%s = Sorry currently there is no data available" % bar_charts['Instagram'][
            'msg']

    if sum(bar_charts['Events']['values']) <= 0:
        bar_charts.pop('Events', None)
    if sum(bar_charts['Youtube']['values']) <= 0:
        bar_charts.pop('Youtube', None)
    if sum(bar_charts['Twitter']['values']) <= 0:
        bar_charts.pop('Twitter', None)
    if sum(bar_charts['Facebook']['values']) <= 0:
        bar_charts.pop('Facebook', None)
    if sum(bar_charts['Spotify']['values']) <= 0:
        bar_charts.pop('Spotify', None)
    if sum(bar_charts['Instagram']['values']) <= 0:
        bar_charts.pop('Instagram', None)

    context = {
        'onlyartist_names': json.dumps(onlyartist_names),
        'compare_artist': json.dumps(compare_artist),
        'countries_charts': json.dumps(countries_charts),
        'fanbase_charts': json.dumps(fanbase_charts),
        'artist_heading': artist_heading,
        'bar_charts': bar_charts,
        'facebook_like_graph': facebook_like_graph,
        'show_loader': True,
    }

    return render(request, 'artist/artist_compare_dj.html', context)


@login_required
@check_email_validated
# @check_plan('Trial','Basic','Premium')
@check_plan('Trial', 'Promoter', 'Artist/DJ', 'Agent')
def artist_map_frame(request, artist_id):
    artist = Artist.objects.get(artistID=artist_id)
    upcoming_events = get_upcoming_events(artist)
    context = {
        'artist': artist,
        'upcoming_events': upcoming_events,
    }
    return render(request, 'artist/artist_map.html', context)


@login_required
# @check_plan('Trial','Basic','Premium')
@check_email_validated
@check_plan('Trial', 'Artist/DJ', 'Promoter', 'Agent')
def master_search(request):
    user = request.user
    pro_obj = Profile.objects.get(user=request.user)
    if pro_obj.user_types != "Promoter" and not request.user.is_superuser:
        return redirect('/')
    start_time = time.time()
    artist_limit = 18
    allartists = []
    message = False
    message_string = "YOUR SEARCH RESULTS FOR"
    artist_name = request.GET.get('artistname', '')
    whattosearch = request.GET.get('whattosearch', '')
    bookingstatus = request.GET.get('bookingstatus', '')
    date = request.GET.get('date', '')
    days = request.GET.get('days', '')
    genre = request.GET.get('genre', '')
    budget = request.GET.get('budget', '')
    country = request.GET.get('country', '')
    territories = request.GET.get('territories', '')
    prioritize_my_fav = request.GET.get('prioritize_my_fav', '')
    with_in_the_time_duration = request.GET.get('with_in_the_time_duration', '')
    useraddress = request.GET.get('useraddress', '')

    if whattosearch == "one":
        message_string = "{} > {}".format(message_string, artist_name)
    elif whattosearch == "any":
        message_string = "{} > ANY ARTISTS".format(message_string)
    else:
        allartists = []

    if bookingstatus == "free":
        message_string = "{} > FREE".format(message_string)
    elif bookingstatus == "booked":
        message_string = "{} > BOOKED".format(message_string)

    if genre:
        message_string = "{} > {} Genre".format(message_string, genre)

    if budget and budget != "0":

        if budget == "1":
            budget_msg = "UNDERGROUND STAR"
        elif budget == "2":
            budget_msg = "POPULAR ACT"
        elif budget == "3":
            budget_msg = "GLOBAL STAR"
        message_string = "{} > BUDGET {}".format(message_string, budget_msg)

    if date:
        if not days:
            date_obj = datetime.strptime(date, '%m/%d/%Y').date()
            message_string = "{} > ON DATE {}".format(message_string, date_obj.strftime('%a %d %b %Y'))
        else:
            search_dates = [datetime.strptime(date, '%m/%d/%Y').date()]
            for day in xrange(int(days) + 1):
                new_date = search_dates[-1]
                new_date += timedelta(days=1)
                search_dates.append(new_date)
            message_string = "{} > DATE IN BETWEEN {} AND {}".format(message_string,
                                                                     search_dates[0].strftime('%a %d %b %Y'),
                                                                     search_dates[-1].strftime('%a %d %b %Y'))

    if country and bookingstatus == "booked":
        message_string = "{} > IN {} ".format(message_string, country)

    if territories and bookingstatus == "booked":
        message_string = "{},{}".format(message_string, territories)

    if with_in_the_time_duration and useraddress:
        message_string = "{} > NO SHOW WITHIN {} WITHIN {} MONTHS".format(message_string, useraddress,
                                                                          with_in_the_time_duration)

    if whattosearch:
        allartistsBool = True
        if whattosearch == "any":
            allartistsBool = True
            allartists = Artist.objects.all()
        elif whattosearch == "one" and artist_name:
            allartistsBool = False
            allartists = Artist.objects.filter(artistName=artist_name)
            # artist_ids = Artist.objects.filter(artistName=artist_name).values_list('artistID', flat=True)

        if budget:
            allartists = allartists.filter(artistBudget=budget)
        if genre:
            allartists = allartists.filter(genre__genreName__in=[genre])

        if allartists:

            conn = DBInterface()
            conn.connect()

            if bookingstatus == "free":
                free_artists_int_list = map(lambda x: x.artistID, allartists)
                free_date_artists_list = map(lambda x: str(x.artistID), allartists)
                free_artists = ",".join(free_date_artists_list)
                free_artists = "(" + free_artists + ")"

                free_dates = [datetime.strptime(date, '%m/%d/%Y').date()]

                if not days:
                    days = 0

                for day in xrange(int(days)):
                    new_date = free_dates[-1]
                    new_date += timedelta(days=1)
                    free_dates.append(new_date)

                free_dates = map(lambda x: "'" + x.strftime('%Y-%m-%d') + "'", free_dates)
                free_dates = ",".join(free_dates)
                free_dates = "(" + free_dates + ")"

                booked_on_free_dates_query = '''
                    SELECT dj_Master.artistID
                        FROM dj_Master
                        JOIN dj_Events ON dj_Master.eventID = dj_Events.eventID
                        WHERE dj_Master.artistID IN {} AND
                    dj_Events.date IN {};'''.format(free_artists, free_dates)

                free_artist_dates_tuple = conn.execute_query(booked_on_free_dates_query)
                booked_on_free_dates_artists = [int(i[0]) for i in free_artist_dates_tuple]

                available_artists_list = list(set(free_artists_int_list) - set(booked_on_free_dates_artists))
                allartists = Artist.objects.filter(artistID__in=available_artists_list)

            elif bookingstatus == "booked":
                booked_date_artists_list = map(lambda x: str(x.artistID), allartists)
                booked_artists = ",".join(booked_date_artists_list)
                booked_artists = "(" + booked_artists + ")"

                booked_dates = [datetime.strptime(date, '%m/%d/%Y').date()]

                if not days:
                    days = 0

                for day in xrange(int(days)):
                    new_date = booked_dates[-1]
                    new_date += timedelta(days=1)
                    booked_dates.append(new_date)

                booked_dates = map(lambda x: "'" + x.strftime('%Y-%m-%d') + "'", booked_dates)
                booked_dates = ",".join(booked_dates)
                booked_dates = "(" + booked_dates + ")"

                if country or territories:
                    country_list = []
                    if country:
                        country_list = [str(country)]
                    if territories:
                        terr = Territories.objects.get(name=territories)
                        terr_country = Country.objects.filter(territory_code=terr).values_list('name', flat=True)
                        terr_country = [str(c) for c in terr_country]
                        country_list.extend(terr_country)

                    if "Cote d'Ivoire" in country_list:
                        country_list.remove("Cote d'Ivoire")  # this creates issue in the mysql syntax so temp fix add

                    country_list = map(lambda x: "'" + x + "'", country_list)
                    country_list_cou = ",".join(country_list)
                    country_list_cou = "(" + country_list_cou + ")"
                    booked_on_booked_dates_query = '''
                        SELECT dj_Master.artistID
                            FROM dj_Master
                            JOIN dj_Events ON dj_Master.eventID = dj_Events.eventID
                            JOIN dj_Venues ON dj_Master.venueID = dj_Venues.venueID
                            WHERE dj_Master.artistID IN {}
                            AND dj_Venues.country IN {}
                        AND dj_Events.date IN {};'''.format(booked_artists, country_list_cou, booked_dates)
                else:
                    booked_on_booked_dates_query = '''
                        SELECT dj_Master.artistID
                            FROM dj_Master
                            JOIN dj_Events ON dj_Master.eventID = dj_Events.eventID
                            WHERE dj_Master.artistID IN {} AND
                        dj_Events.date IN {};'''.format(booked_artists, booked_dates)

                booked_artist_dates_tuple = conn.execute_query(booked_on_booked_dates_query)
                booked_on_free_dates_artists = [i[0] for i in booked_artist_dates_tuple]

                available_artists_list = booked_on_free_dates_artists
                allartists = Artist.objects.filter(artistID__in=available_artists_list)

            if with_in_the_time_duration and useraddress:
                user_lat = 51.509865  # Default Coordinates
                user_long = -0.118092
                withinmiles = 100
                google_location_api_url = "http://maps.google.com/maps/api/geocode/json?address="
                response = requests.get(google_location_api_url + useraddress.replace(" ", "+"))
                json_response = response.json()
                if json_response['status'] == "OK":
                    try:
                        user_lat = json_response['results'][0]['geometry']['location']['lat']
                        user_long = json_response['results'][0]['geometry']['location']['lng']
                    except:
                        geoip = GeoIP()
                        ip = get_client_ip(request)
                        if ip and ip != "127.0.0.1":
                            user_lat, user_long = geoip.lat_lon(ip)

                user_lat = float(user_lat)
                user_long = float(user_long)
                if allartists:
                    remove_artist_list = []
                    all_artists_list = map(lambda x: str(x.artistID), allartists)
                    all_artists_id = ",".join(all_artists_list)
                    all_artists_id = "(" + all_artists_id + ")"
                    buffer_month = int(with_in_the_time_duration)
                    event_date = datetime.strptime(date, '%m/%d/%Y').date()
                    date_upto = event_date - timedelta(days=buffer_month * 30)
                    all_artist_events = DjMaster.objects.raw(
                        "SELECT * FROM dj_Master JOIN dj_Events ON dj_Events.eventID = dj_Master.eventID JOIN dj_Venues ON dj_Venues.venueID = dj_Master.venueID WHERE dj_Master.artistID IN {} AND dj_Events.date BETWEEN {} and {}".format(
                            all_artists_id, "'" + str(date_upto) + "'", "'" + str(event_date) + "'"))
                    for event in all_artist_events:
                        if event.venueLatitude != 0E-9 and event.venueLongitude != 0E-9 and event.venueLatitude != None and event.venueLongitude != None:
                            venue_latitude = event.venueLatitude
                            venue_longitude = event.venueLongitude

                            distance = distance_using_lati_long(user_lat, user_long, float(venue_latitude),
                                                                float(venue_longitude))

                            if distance <= withinmiles:
                                remove_artist_list.append(event.artistid)

                    remove_artist_list = list(set(remove_artist_list))
                    remove_artist_list_id = [artist.artistID for artist in remove_artist_list]
                    allartists = allartists.exclude(artistID__in=remove_artist_list_id)

    # this block is for prioritizing my fav
    if allartists and prioritize_my_fav == "Yes":
        allartists = prioritize_favourtie_artist(request, allartists)

    if request.is_ajax():
        start = request.GET.get('start', '')
        if start:
            start = int(start)
            allartists = allartists[start:(start + artist_limit)]

            context = {"allartists": allartists}

            template = render_to_string("artist/get_artist.html", context, context_instance=RequestContext(request))

            start = int(start) + limit
            if allartists:
                status = "True"  # More events
            else:
                status = "False"  # No More events
            response = {
                "status": status,
                "start": start,
                "data": template,
            }

            return HttpResponse(json.dumps(response), content_type="applicaton/json")

    all_genre = Genre.objects.all()
    all_country = Country.objects.all().order_by('name')
    all_territories = Territories.objects.all().order_by('name')
    if allartists:
        all_artists_id = list(allartists.values_list('artistID', flat=True))
    else:
        all_artists_id = None

    if whattosearch:  # This block save the user searches
        search_type = 'MasterSearch'
        message = message_string
        recentsearch = save_recent_search(request, search_type, message)

    context = {
        'artist_start': artist_limit,
        'allartists': allartists[0:artist_limit],
        'result_length': len(allartists),
        'all_genre': all_genre,
        'all_country': all_country,
        'all_territories': all_territories,
        'artist_name': artist_name,
        'whattosearch': whattosearch,
        'bookingstatus': bookingstatus,
        'prioritize_my_fav': prioritize_my_fav,
        'date': date,
        'days': days,
        'genre': genre,
        'budget': budget,
        'country': country,
        'territories': territories,
        'message': message,
        'message_string': message_string,
        'all_artists_id': all_artists_id,
        'with_in_the_time_duration': with_in_the_time_duration,
        'useraddress': useraddress,
    }

    total_time_taken = time.time() - start_time

    return render(request, 'artist/master_search.html', context)


@login_required
@check_email_validated
# @check_plan('Trial','Basic','Premium')
#@check_plan('Trial', 'Promoter', 'Artist/DJ', 'Agent')
def filter_name_artist(request):
    all_artist = []
    if request.GET.get('query'):
        results = Artist.objects.filter(artistName__icontains=request.GET.get('query')). \
            values('artistID', 'artistName').distinct()
        for sub in results:
            for key in sub:
                sub[key] = str(sub[key])
    response = list(results)
    return HttpResponse(json.dumps(response), content_type='application/json')


@login_required
@check_email_validated
# @check_plan('Trial','Basic','Premium')
@check_plan('Trial', 'Promoter', 'Artist/DJ', 'Agent')
def filter_name_city(request):
    all_artist = []
    country = request.GET.get("country")

    if country:
        cities = City.objects.filter(iso2=country).order_by("city")

    if request.GET.get('query'):
        if country:
            results = City.objects.filter(iso2=country, city__icontains=request.GET.get('query')).values('pk_city',
                                                                                                         'city').distinct()
        else:
            results = City.objects.filter(city__icontains=request.GET.get('query')).values('pk_city', 'city').distinct()

        for sub in results:
            for key in sub:
                sub[key] = str(sub[key])
    response = list(results)
    return HttpResponse(json.dumps(response), content_type='application/json')


@login_required
# @check_plan('Trial','Basic','Premium')
@check_email_validated
@check_plan('Trial', 'Promoter', 'Artist/DJ', 'Agent')
def filter_all(request):
    search_data = []
    if request.GET.get('query'):
        query = request.GET.get('query')
        artist = Artist.objects.filter(status="1")
        search_result = watson.search(query, models=(artist, Country, City, Genre), ranking=False)

        for result in search_result:
            dict = {}
            dict["id"] = result.object.pk
            dict["name"] = result.title
            dict["class"] = result.object.get_classname()
            search_data.append(dict)

    response = search_data
    return HttpResponse(json.dumps(response), content_type='application/json')


@login_required
# @check_plan('Trial','Basic','Premium')
@check_email_validated
@check_plan('Trial', 'Promoter', 'Artist/DJ', 'Agent')
def filter_all_art(request):
    search_data = []
    if request.GET.get('query'):
        query = request.GET.get('query')
        artist = Artist.objects.filter(status="1")
        search_result = watson.search(query, models=[artist])

        for result in search_result:
            dict = {}
            dict["id"] = result.object.pk
            dict["name"] = result.title
            dict["class"] = result.object.get_classname()
            search_data.append(dict)

    response = search_data
    return HttpResponse(json.dumps(response), content_type='application/json')


def filter_all_submit(request):
    id_type = request.POST.get("id_and_type")
    id, type = id_type.split("_")

    if type == "City":
        what = 1
        city = City.objects.filter(pk_city=id)[0]
        city_code = city.pk
        country_code = city.iso2
        response = redirect('territory_search')
        response['Location'] += '?what={}&country={}&city={}'.format(what, country_code, city_code)
        return response

    if type == "Artist":
        return redirect(reverse("artist_detail", kwargs={'artist_id': id}))

    if type == "Country":
        what = 1
        country = Country.objects.get(pk=id)
        country_code = country.code
        response = redirect('territory_search')
        response['Location'] += '?what={}&country={}'.format(what, country_code)
        return response

    if type == "Genre":
        return redirect(reverse("artists"))

    return redirect("/")


def filter_artist_report(request):
    artist_name = request.POST['artist_name']
    if artist_name.endswith(" / Artist"):
        artist_name = artist_name[:-9]

    return redirect(reverse("artist_report", kwargs={'artist_name': artist_name}))


@login_required
@check_email_validated
# @check_plan('Trial','Basic','Premium')
@check_plan('Trial', 'Promoter', 'Agent', 'Artist/DJ')
def filter_artist_name(request):
    all_artist = []
    if request.GET.get('query'):
        all_artist = list(
            Artist.objects.filter(artistName__icontains=request.GET.get('query')).values_list('artistName', flat=True))
    response = {
        "artists": all_artist,
    }
    return HttpResponse(json.dumps(response), content_type='application/json')


@login_required
# @check_plan('Trial','Basic','Premium')
@check_email_validated
@check_plan('Trial', 'Artist/DJ', 'Promoter', 'Agent')
def flight_share(request):
    user = request.user
    pro_obj = Profile.objects.get(user=request.user)
    if pro_obj.user_types != "Promoter" and not request.user.is_superuser:
        return redirect('/')
    start_time = time.time()
    artist_limit = 18
    message_string = "YOUR SEARCH RESULTS FOR"
    user_lat = 28.6618976  # Default Coordinates
    user_long = 77.22739580000007
    address = request.GET.get('useraddress', '')
    geoLat = request.GET.get('geoLat', '')
    geoLang = request.GET.get('geoLang', '')

    if geoLat and geoLang and not address:
        user_lat = geoLat
        user_long = geoLang
    elif not address:
        geoip = GeoIP()
        ip = get_client_ip(request)
        if ip and ip != "127.0.0.1":
            user_lat, user_long = geoip.lat_lon(ip)
    else:
        google_location_api_url = "https://maps.google.com/maps/api/geocode/json?address="
        response = requests.get(
            google_location_api_url + address.replace(" ", "+") + "&key=AIzaSyA95184Inn61QQv0kLpPkDW1Fhi5SlcOJE")
        json_response = response.json()
        if json_response['status'] == "OK":
            try:
                user_lat = json_response['results'][0]['geometry']['location']['lat']
                user_long = json_response['results'][0]['geometry']['location']['lng']
            except:
                pass

    allartists = []
    artist_name = request.GET.get('artistname', '')
    whattosearch = request.GET.get('whattosearch', '')
    bookedon = request.GET.get('bookedon', '')
    freeon = request.GET.get('freeon', '')
    freeondates = request.GET.get('freeondates', '')
    withinmiles = request.GET.get('withinmiles', '')
    prioritize_my_fav = request.GET.get('prioritize_my_fav', '')

    if whattosearch == "one":
        message_string = "{} > {}".format(message_string, artist_name)
    elif whattosearch == "any":
        message_string = "{} > ANY ARTISTS".format(message_string)
    else:
        allartists = []

    if freeon:
        date_obj = datetime.strptime(freeon, '%m/%d/%Y').date()
        message_string = "{} > FREE ON DATE {}".format(message_string, date_obj.strftime('%d %b'))

    if bookedon:
        date_obj = datetime.strptime(bookedon, '%m/%d/%Y').date()
        message_string = "{} >BOOKED ON DATE {}".format(message_string, date_obj.strftime('%d %b'))

    if withinmiles:
        message_string = "{} > WITH IN {} MILES".format(message_string, withinmiles)

    if bookedon:
        allartistsBool = True
        if whattosearch == "any":
            allartistsBool = True
        elif whattosearch == "one" and artist_name:
            allartistsBool = False
            artist_ids = Artist.objects.filter(artistName=artist_name).values_list('artistID', flat=True)

        conn = DBInterface()
        conn.connect()
        bookeddate = datetime.strptime(bookedon, '%m/%d/%Y').date()

        # 1.58 seconds
        if allartistsBool:

            booked_date_query = '''
                SELECT dj_Master.artistID
                FROM dj_Events
                JOIN dj_Master ON dj_Master.eventID = dj_Events.eventID
                WHERE dj_Events.date = "{}"'''.format(bookeddate.strftime('%Y-%m-%d'))

        else:
            artist_ids = str(artist_ids[0])
            booked_date_query = '''
                SELECT dj_Master.artistID
                FROM dj_Master
                JOIN dj_Events ON dj_Master.eventID = dj_Events.eventID
                WHERE dj_Master.artistID = "{}" AND dj_Events.date = "{}"'''.format(artist_ids,
                                                                                    bookeddate.strftime('%Y-%m-%d'))

        booked_dates_artists = conn.execute_query(booked_date_query)

        booked_date_artists_list = [i[0] for i in booked_dates_artists]

        if booked_date_artists_list:
            booked_date_artists = map(lambda x: str(x), booked_date_artists_list)
            booked_date_artists = ",".join(booked_date_artists)
            booked_date_artists = "(" + booked_date_artists + ")"

            free_dates = [datetime.strptime(freeon, '%m/%d/%Y').date()]

            if not freeondates:
                freeondates = 0
            for day in xrange(int(freeondates)):
                new_date = free_dates[-1]
                new_date += timedelta(days=1)
                free_dates.append(new_date)

            free_dates = map(lambda x: "'" + x.strftime('%Y-%m-%d') + "'", free_dates)
            free_dates = ",".join(free_dates)
            free_dates = "(" + free_dates + ")"

            booked_on_free_dates_query = '''
                SELECT dj_Master.artistID
                FROM dj_Master
                JOIN dj_Events ON dj_Master.eventID = dj_Events.eventID
                WHERE dj_Master.artistID IN {} AND
                    dj_Events.date IN {};'''.format(booked_date_artists, free_dates)

            booked_on_free_dates_tuple = conn.execute_query(booked_on_free_dates_query)
            booked_on_free_dates_artists = [i[0] for i in booked_on_free_dates_tuple]

            start_time_3 = time.time()

            available_artists_list = list(set(booked_date_artists_list) - set(booked_on_free_dates_artists))
            exclude_available_artist = []

            print "with in booked_on_free_dates_query time {}".format(time.time() - start_time_3)

            available_artists = map(lambda x: str(x), available_artists_list)
            available_artists = ",".join(available_artists)
            available_artists = "(" + available_artists + ")"
            if available_artists_list:
                if withinmiles:
                    start_time_4 = time.time()
                    events = DjMaster.objects.raw(
                        "SELECT * FROM dj_Master JOIN dj_Events ON dj_Events.eventID = dj_Master.eventID JOIN dj_Venues ON dj_Venues.venueID = dj_Master.venueID WHERE dj_Master.artistID IN {} AND dj_Events.date = '{}'".format(
                            available_artists, bookeddate.strftime('%Y-%m-%d')))
                    for event in events:
                        if event.venueLatitude != 0E-9 and event.venueLongitude != 0E-9:
                            venue_latitude = event.venueLatitude
                            venue_longitude = event.venueLongitude
                            distance = distance_using_lati_long(user_lat, user_long, float(venue_latitude),
                                                                float(venue_longitude))

                            if not distance <= int(withinmiles):
                                exclude_available_artist.append(event.artistid.artistID)
                        else:
                            exclude_available_artist.append(event.artistid.artistID)
                    print "with in miles time {}".format(time.time() - start_time_4)

            available_artists = list(set(available_artists_list) - set(exclude_available_artist))
            allartists = Artist.objects.filter(artistID__in=available_artists)
        else:
            allartists = []

        if allartists and prioritize_my_fav == "Yes":
            allartists = prioritize_favourtie_artist(request, allartists)

        if request.is_ajax():

            start = request.GET.get('start', '')
            if start:
                start = int(start)
                allartists = allartists[start:(start + artist_limit)]

                context = {"allartists": allartists}

                template = render_to_string("artist/get_artist.html", context, context_instance=RequestContext(request))

                start = int(start) + limit
                if allartists:
                    status = "True"  # More artists
                else:
                    status = "False"  # No More artists
                response = {
                    "status": status,
                    "start": start,
                    "data": template,
                }
                return HttpResponse(json.dumps(response), content_type="applicaton/json")

    if allartists:
        all_artists_id = list(allartists.values_list('artistID', flat=True))
    else:
        all_artists_id = None

    if whattosearch:  # This block save the user searches
        search_type = 'FlightShareSearch'
        message = message_string
        recentsearch = save_recent_search(request, search_type, message)

    context = {
        'artist_start': artist_limit,
        'allartists': allartists[0:artist_limit],
        'artist_name': artist_name,
        'whattosearch': whattosearch,
        'bookedon': bookedon,
        'address': address,
        'result_length': len(allartists),
        'freeon': freeon,
        'freeondates': freeondates,
        'withinmiles': withinmiles,
        'message_string': message_string,
        'all_artists_id': all_artists_id,
        'prioritize_my_fav': prioritize_my_fav,
    }

    return render(request, 'artist/flight_share.html', context)


def distance_using_lati_long(user_latitude, user_longitude, venue_latitude, venue_longitude):
    degrees_to_radians = math.pi / 180.0

    phi1 = (90.0 - user_latitude) * degrees_to_radians
    phi2 = (90.0 - venue_latitude) * degrees_to_radians

    theta1 = user_longitude * degrees_to_radians
    theta2 = venue_longitude * degrees_to_radians

    cos = (math.sin(phi1) * math.sin(phi2) * math.cos(theta1 - theta2) +
           math.cos(phi1) * math.cos(phi2))
    arc = math.acos(cos)
    earth_radius = 3959
    return arc * 3959


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


@login_required
# @check_plan('Trial','Basic','Premium')
@check_email_validated
@check_plan('Trial', 'Artist/DJ', 'Promoter', 'Agent')
def someone_music_you_love(request):
    user = request.user
    pro_obj = Profile.objects.get(user=request.user)
    if pro_obj.user_types != "Promoter" and not request.user.is_superuser:
        return redirect('/')
    free_date = request.GET.get('free_on', '')
    artist_name = request.GET.get('artist_name', '')
    days = request.GET.get('days', '')
    artist_limit = 18
    date_msg = ""
    allartists = []
    artist_status = None
    booked_on = False
    if artist_name:
        try:
            artist = Artist.objects.get(artistName=artist_name)
        except:
            artist = None
    else:
        artist = None
    if artist:
        similar_artist = get_similar_artist(artist)
        # for sm_artist in similar_artist:
        #     allartists.extend(get_similar_artist(sm_artist))

        allartists = get_multiple_similiar_artist(similar_artist)

        # allartists = allartists.exclude(artistName=artist_name)

        allartists = set(allartists)

        allartists = Artist.objects.filter(artistID__in=[x.artistID for x in allartists])

        artist_genre = artist.genre.all()

        allartists = allartists.filter(genre__in=artist_genre).distinct()
        allartists = list(allartists)

        if artist not in allartists:
            allartists.append(artist)

    if allartists:
        conn = DBInterface()
        conn.connect()

        free_artists_int_list = map(lambda x: x.artistID, allartists)

        free_date_artists_list = map(lambda x: str(x.artistID), allartists)
        free_artists = ",".join(free_date_artists_list)
        free_artists = "(" + free_artists + ")"

        free_dates = [datetime.strptime(free_date, '%m/%d/%Y').date()]

        if not days:
            days = 0

        for day in xrange(int(days)):
            new_date = free_dates[-1]
            new_date += timedelta(days=1)
            free_dates.append(new_date)

        if days == 0:
            date_msg = "on {}".format(free_dates[0].strftime('%d %b'))
        else:
            date_msg = "in between {} to {}".format(free_dates[0].strftime('%d %b'), free_dates[-1].strftime('%d %b'))

        free_dates = map(lambda x: "'" + x.strftime('%Y-%m-%d') + "'", free_dates)
        free_dates = ",".join(free_dates)
        free_dates = "(" + free_dates + ")"

        booked_on_free_dates_query = '''
            SELECT dj_Master.artistID
                FROM dj_Master
                JOIN dj_Events ON dj_Master.eventID = dj_Events.eventID
                WHERE dj_Master.artistID IN {} AND
            dj_Events.date IN {};'''.format(free_artists, free_dates)

        booked_artists = '''
                            SELECT dj_Master.artistID FROM dj_Master
                                JOIN dj_Artists on dj_Master.artistID = dj_Artists.artistID
                                JOIN dj_Events ON dj_Master.eventID = dj_Events.eventID
                                WHERE dj_Artists.artistID IN {} AND dj_Events.date IN {} ;'''.format(free_artists,
                                                                                                     free_dates)

        booked_artists = conn.execute_query(booked_artists)

        booked_artists_tuple = [int(i[0]) for i in booked_artists]

        free_artist_dates_tuple = conn.execute_query(booked_on_free_dates_query)

        booked_on_free_dates_artists = [int(i[0]) for i in free_artist_dates_tuple]
        if artist.artistID not in booked_on_free_dates_artists:
            artist_status = "potentially FREE"
        else:
            artist_status = "booked"
            booked_in = get_upcoming_events(artist, start=0, map_event=None)
            if booked_in:
                country = booked_in[0].country
                city = booked_in[0].city
                if city:
                    booked_in = city + ", " + country
                else:
                    booked_in = country

        available_artists_list = list(set(free_artists_int_list) - set(booked_on_free_dates_artists))

        booked_on = Artist.objects.filter(artistID__in=booked_artists_tuple).exclude(artistName=artist_name)

        allartists = Artist.objects.filter(artistID__in=available_artists_list).exclude(artistName=artist_name)

    if request.is_ajax():
        start = request.GET.get('start', '')
        start1 = request.GET.get('start1', '')

        print "start 1 +++++++++++++++++++++++++++++++++++++++"
        print start1

        try:
            booked_artist = request.GET["booked_artist"]
            print "booked _artist ++++++++++++++++++++++++++++++++++++++++"
            print booked_artist
        except:
            booked_artist = False

        try:
            all_artist = request.GET["all_artist"]
        except:
            all_artist = False

        if start and all_artist == "true":
            print "here 1++++++++++++++++++++++++++++++"
            start = int(start)
            allartists = allartists[start:(start + artist_limit)]

            context = {"allartists": allartists}

            start = int(start) + limit
            if allartists:
                status = "True"  # More events
            else:
                status = "False"  # No More events
            response = {
                "status": status,
                "start": start,
            }

        if start1 and booked_artist == "true":
            print "here 2 ++++++++++++++++++++++++++++++++++++++++++++++++++"
            start1 = int(start1)
            booked_on = booked_on[start1:(start1 + artist_limit)]

            context = {"allartists": booked_on}

            start1 = int(start1) + limit

            if booked_on:
                status = "True"  # More events
            else:
                status = "False"  # No More events

            response = {
                "status": status,
                "start": start1,
            }

        template = render_to_string("artist/get_artist.html", context, context_instance=RequestContext(request))

        response.update({
            "data": template
        })

        return HttpResponse(json.dumps(response), content_type="applicaton/json")

    if artist and artist_status == 'potentially FREE':
        message_string = "{} is {} and so are the following artists {}".format(artist.artistName, artist_status,
                                                                               date_msg)
    elif artist and artist_status == 'booked':
        message_string = "{} is {} in {} but the following similar artists are potentially free {}".format(
            artist.artistName, artist_status, booked_in, date_msg)
    else:
        message_string = ""

    if allartists:
        all_artists_id = list(allartists.values_list('artistID', flat=True))
    else:
        all_artists_id = None

    if artist_name:  # This block save the user searches
        search_type = 'SomeOneMusicYouLove'
        message = message_string
        recentsearch = save_recent_search(request, search_type, message)

    context = {
        'artist_start': artist_limit,
        'artist': artist,
        'allartists': allartists[0:artist_limit],
        'artist_status': artist_status,
        'free_date': free_date,
        'artist_name': artist_name,
        'days': days,
        'all_artists_id': all_artists_id,
        'message_string': message_string,
        'booked_on': booked_on,
        "bookedartist_start": artist_limit,
    }
    return render(request, 'artist/music_you_love.html', context)


@login_required
@check_email_validated
# @check_plan('Trial','Basic','Premium')
@check_plan('Trial', 'Artist/DJ', 'Promoter', 'Agent')
def email_listings(request, artist_id):
    artist = Artist.objects.get(artistID=artist_id)
    upcoming_events = get_upcoming_events(artist)

    social_urls = get_artist_social_url(artist)
    statistics = get_artist_statistics(artist)
    request_url = request.META.get('HTTP_REFERER')

    user = User.objects.get(id=request.user.id)
    user_email = user.email
    budget = artist.artistBudget * "$"
    if user_email:
        msg = SendEmail(request)
        msg.send_by_template([user_email], "artist/email_listing.html",
                             context={
                                 "social_urls": social_urls,
                                 "upcoming_events": upcoming_events,
                                 "artist": artist,
                                 "site_url": settings.SITE_URL,
                                 "budget": budget,
                                 "statistics": statistics,
                             },
                             subject="Email Listings"
                             )

    messages.add_message(request, messages.SUCCESS,
                         " Artist Email listing Email has been successfully sent at your Email ")

    if request_url:
        return redirect(request_url)
    else:
        return redirect(reverse('artist_detail', kwargs={'artist_id': artist_id}))


@login_required
@check_email_validated
# @check_plan('Trial','Basic','Premium')
@check_plan('Trial', 'Artist/DJ', 'Promoter', 'Agent')
def free_dates(request, artist_id):
    artist = Artist.objects.get(artistID=artist_id)

    upcoming_events = get_upcoming_events(artist)

    social_urls = get_artist_social_url(artist)

    statistics = get_artist_statistics(artist)

    today = datetime.now().date()
    days = 90
    day_list = [today]
    for i in range(days):
        new_day = day_list[-1] + timedelta(1)
        day_list.append(new_day)

    artist_booked_dates = []
    for event in upcoming_events:
        if event.date:
            artist_booked_dates.append(event.date)

    day_list = set(day_list)
    artist_booked_dates = set(artist_booked_dates)
    free_dates = day_list - artist_booked_dates
    free_dates = list(free_dates)
    free_dates = sorted(free_dates)
    free_dates = filter(lambda x: x.isoweekday() in [5, 6], free_dates)

    free_dates_list = []
    for event in upcoming_events:
        free_dates_dict = {}
        free_dates_dict['country'] = event.country
        free_dates_dict['city'] = event.city
        free_dates_dict['date'] = event.date
        free_dates_dict['evenueName'] = event.evenueName
        free_dates_dict['eventName'] = event.eventName
        free_dates_list.append(free_dates_dict)

    for date in free_dates:
        free_dates_dict = {}
        free_dates_dict['date'] = date
        free_dates_list.append(free_dates_dict)

    free_dates_upcoming_event = sorted(free_dates_list, key=lambda k: k['date'])
    request_url = request.META.get('HTTP_REFERER')
    user_email = request.user.email
    budget = artist.artistBudget * "$"

    if user_email:
        msg = SendEmail(request)
        msg.send_by_template([user_email], "artist/free_dates.html",
                             context={
                                 "social_urls": social_urls,
                                 "free_dates_upcoming_event": free_dates_upcoming_event,
                                 "artist": artist,
                                 "site_url": settings.SITE_URL,
                                 "budget": budget,
                                 "statistics": statistics,
                             },
                             subject="Free Dates"
                             )
    messages.add_message(request, messages.SUCCESS,
                         " Artist Free dates Email has been successfully sent at your Email ")

    if request_url:
        return redirect(request_url)
    else:
        return redirect(reverse('artist_detail', kwargs={'artist_id': artist_id}))


@login_required
@check_email_validated
# @check_plan('Trial','Basic','Premium')
@check_plan('Trial', 'Artist/DJ', 'Promoter', 'Agent')
def email_artist(request):
    response = {'status': False}
    if request.is_ajax():
        action = request.GET.get('action', '')
        message = request.GET.get('message', '')
        message = message.replace("&gt;", "")
        site_url = settings.SITE_URL
        type_of_search = request.GET.get('type_of_search', '')
        if type_of_search:
            subject = "WDJP Search Result For {}".format(type_of_search)
        else:
            subject = message

        if action == "list":
            start = request.GET.get('start', '')
            all_artists_id = request.GET.get('all_artists_id', '')
            all_artists_id = json.loads(all_artists_id)
            all_artists_id = [int(artistid) for artistid in all_artists_id]
            message_data = []
            all_artists = Artist.objects.filter(artistID__in=all_artists_id)
            user_email = request.user.email
            if user_email:
                msg = SendEmail(request)
                msg.send_by_template([user_email], "artist/email/filters_email.html",
                                     context={
                                         "action": action,
                                         "all_artists": all_artists,
                                         "user": request.user,
                                         "message": message.replace('YOUR SEARCH RESULTS FOR ARTISTS FREE',
                                                                    'Here are your search results for artists free'),
                                         "site_url": settings.SITE_URL,
                                     },
                                     subject=subject
                                     )

            response['status'] = True

        # elif action == "surroundings":
        #     start = request.GET.get('start', '')
        #     all_artists_id = request.GET.get('all_artists_id', '')
        #     all_artists_id = all_artists_id[1:-1].split(", ")
        #     all_artists_id = all_artists_id[0:int(start)]
        #     all_artists_id = [int(artist_id.replace("L", "")) for artist_id in all_artists_id]
        #     message_data = []
        #     all_artists = Artist.objects.filter(artistID__in=all_artists_id)
        #     for artist in all_artists:
        #         past_events = get_past_events(artist)
        #         if past_events:
        #             last_event = past_events[0]
        #             before_string = " before-{} ({})".format(last_event.date.strftime("%d %b"), last_event.country)
        #         else:
        #             before_string = ""
        #         upcoming_events = get_upcoming_events(artist)
        #
        #         if upcoming_events:
        #             first_event = upcoming_events[0]
        #             after_string = " after-{} ({})".format(first_event.date.strftime("%d %b"), first_event.country)
        #         else:
        #             after_string = ""
        #         message_str = "<a href='{}/artists/artist/{}/'>{}</a>-".format(settings.SITE_URL, artist.artistID, artist.artistName) + before_string + after_string
        #         message_data.append(message_str)
        #
        #     user_email = request.user.email
        #
        #     if user_email:
        #         msg = SendEmail(request)
        #         msg.send_by_template([user_email], "artist/email/filters_email.html",
        #             context={
        #                 "action": action,
        #                 "user": request.user,
        #                 "message": message,
        #                 "site_url": settings.SITE_URL,
        #                 "message_data": message_data,
        #             },
        #             subject = subject
        #         )
        #
        #     response['status'] = True

        elif action == "selected":
            start = request.GET.get('start', '')
            all_artists_id = request.GET.get('all_artists_id', '')
            all_artists_id = json.loads(all_artists_id)
            all_artists_id = [int(artistid) for artistid in all_artists_id]
            message_data = []
            all_artists = Artist.objects.filter(artistID__in=all_artists_id)
            for artist in all_artists:
                past_events = get_past_events(artist)
                if past_events:
                    last_event = past_events[0]
                    before_string = " before-{} ({})".format(last_event.date.strftime("%d %b"), last_event.country)
                else:
                    before_string = ""
                upcoming_events = get_upcoming_events(artist)
                if upcoming_events:
                    first_event = upcoming_events[0]
                    after_string = " after-{} ({})".format(first_event.date.strftime("%d %b"), first_event.country)
                else:
                    after_string = ""
                message_str = "<a href='{}/artists/artist/{}/'>{}</a>-".format(settings.SITE_URL, artist.artistID,
                                                                               artist.artistName) + before_string + after_string
                message_data.append(message_str)

            user_email = request.user.email
            if user_email:
                msg = SendEmail(request)
                msg.send_by_template([user_email], "artist/email/filters_email.html",
                                     context={
                                         "action": action,
                                         "user": request.user,
                                         "message": message.replace('YOUR SEARCH RESULTS FOR ARTISTS FREE',
                                                                    'Here are your search results for artists free'),
                                         "site_url": settings.SITE_URL,
                                         "message_data": message_data,
                                     },
                                     subject=subject
                                     )

            response['status'] = True

    return HttpResponse(json.dumps(response), content_type="applicaton/json")

    # selected_song = album.song_set.get(pk=request.POST['song'])


@login_required
@check_email_validated
# @check_plan('Trial','Basic','Premium')
@check_plan('Trial', 'Promoter', 'Artist/DJ', 'Agent')
def favorite(request):
    all_events = WpMyEvents.objects.filter(user_id=request.user.id)
    profile = Profile.objects.get(user=request.user)

    email_frequency_value = profile.email_frequency

    favorites_artist = profile.favorite_artist.all().order_by('artistName')

    favorites_artist_ids = favorites_artist.values_list('artistID', flat=True)

    artists = Artist.objects.all().values_list('artistID', 'artistName')

    all_artist = []
    for id in favorites_artist_ids:
        artist_dict = {}
        try:
            artist = Artist.objects.get(artistID=int(id))
        except:
            continue
        artist_dict['upcoming_events'] = get_upcoming_events(artist)
        # artist_dict['statistics'] = get_artist_statistics(artist)
        artist_dict['social_urls'] = get_artist_social_url(artist)
        artist_dict['similar_artists'] = get_similar_artist(artist, limit=12)
        artist_dict['artist_requested_event_list'] = get_myevents_artist_invite(artist, request)
        artist_dict['artist_obj'] = artist
        all_artist.append(artist_dict)

    if request.method == "POST":
        for artist in profile.favorite_artist.all():
            profile.favorite_artist.remove(artist)
        profile.save()

        favorite_artists_id = request.POST.getlist('favorite_artists', '')
        if favorite_artists_id:
            favorite_artists_id = [int(id) for id in favorite_artists_id]
            profile = Profile.objects.get(user=request.user)
            for id in favorite_artists_id:
                profile.favorite_artist.add(Artist.objects.get(artistID=id))
            profile.save()
        return redirect("favorite")

    context = {
        "all_artist": all_artist,
        "favorites_artist": favorites_artist,
        "profile": profile,
        "all_events": all_events,
        "artists": artists,
        "favorites_artist_ids": favorites_artist_ids,
        "email_frequency_value": email_frequency_value
    }

    return render(request, 'artist/favorite.html', context)


def artist_exist_or_not(request):
    response = {'status': True}

    artist_name = request.GET.get("q")

    artist = Artist.allartistobjects.filter(artistName=artist_name)
    if not artist:
        response['status'] = False

    return HttpResponse(json.dumps(response))


def find_similar(request):
    artist_list = []

    artists_name = request.GET.get("q")

    artists_name_list = artists_name.split(",")

    for artist_name in artists_name_list:
        artist = Artist.allartistobjects.filter(artistName=artist_name)

        if not artist:
            artist_list.append(artist_name)

    return HttpResponse(json.dumps(artist_list), content_type="applicaton/json")


@csrf_exempt
def get_data(request):
    response = {
        "status": False
    }

    if request.method != "POST":
        response['message'] = "Get Method Not Allowed"
        return JsonResponse(response)

    artists_data = request.POST.get("artists_data", '')

    token = request.POST.get('token', '')

    scraping_artist_id_list = []

    if artists_data and token in settings.AUTH_TOKENS:
        response['status'] = True
        artists_data = json.loads(artists_data)

        for artist_data in artists_data:
            artistName = artist_data["artistName"]
            followers = artist_data["followers"]
            website = artist_data["website"]
            bandsintown = artist_data["bandsintown"]
            resident = artist_data["resident"]
            gigatools = artist_data["gigatools"]
            facebook = artist_data["facebook"]
            soundcloud = artist_data["soundcloud"]
            songkick = artist_data["songkick"]
            twitter = artist_data["twitter"]
            instagram = artist_data["instagram"]
            youtube = artist_data["youtube"]
            spotify = artist_data["spotify"]
            lastfm = artist_data["lastfm"]
            keywords = artist_data["keywords"]
            tracks = artist_data["tracks"]
            image = artist_data["image"]
            genres = artist_data["genres"]
            # inserted = artist_data["inserted"]
            verified = artist_data["verified"]
            approved_by = artist_data["approved_by"]
            # a_band = artist_data["a_band"]
            # a_dj = artist_data["a_dj"]
            # a_live_performer = artist_data["a_live_performer"]
            # management_contact = artist_data["management_contact"]
            # booking_agent = artist_data["booking_agent"]
            is_alive = artist_data["is_alive"]
            nationality = artist_data["nationality"]
            home_city = artist_data["home_city"]
            country = artist_data["country"]
            city_latitude = artist_data["city_latitude"]
            city_longitude = artist_data["city_longitude"]
            address = artist_data["address"]
            type_of_performance = artist_data["type_of_performance"]
            similar_artists = artist_data['similar_artists']
            budget = artist_data['budget']
            gender = artist_data["gender"]
            builder_genres = artist_data["builder_genres"]

            keywords_list = keywords.split(",")
            genres_list = genres.split(",")
            if builder_genres:
                builder_genres_list = builder_genres.split(",")

            try:
                artist = Artist.allartistobjects.filter(artistName=artistName)
                status = True
            except:
                pass

            if not artist:
                artist = Artist.allartistobjects.create(
                    artistName=artistName,
                    artistWebsite=website,
                    # artistAgent=booking_agent,
                    user_id=0,
                    country=nationality,
                    city_latitude=city_latitude,
                    city_longitude=city_longitude,
                    from_builder=True,
                    status="0",
                    artistBudget=budget,
                    builder_genre=genres,
                    builder_keywords=keywords,
                    city=home_city,
                    gender=gender,
                    type_of_performance=type_of_performance,
                    address=address
                )
                status = False

            if status == False:
                if image:
                    image = requests.get(image)
                    image_name = artist.artistName.replace(" ", "_").decode('unicode_escape').encode('ascii',
                                                                                                     'ignore') + ".jpg"

                    full_image_path = os.path.join(settings.MEDIA_ROOT, 'artistphotos', image_name)
                    artist_image_file = open(full_image_path, 'wb+')
                    artist_image_file.write(image.content)
                    artist_image_file.close()

                    artist_image_path = full_image_path.split("media/")[1]
                    artist.artistPhoto = artist_image_path
                    artist.save()

                if builder_genres:

                    for genre_name in builder_genres_list:

                        if genre_name == "Dubstep" or genre_name == "Trap Future Bass" or genre_name == "Leftfield Bass" or genre_name == "Trap Future Bass":
                            genre_name = "Dubstep Bass Trap"
                        if genre_name == "Future House" or genre_name == "House" or genre_name == "Indie Dance Nu Disco" \
                                or genre_name == "Leftfield House Techno" or genre_name == "Dance" or genre_name == "Deep House" \
                                or genre_name == "Electro House" or genre_name == "Funk Soul Disco" or genre_name == "Funky Groove Jackin House" or genre_name == "Afro House" \
                                or genre_name == "Progressive House" or genre_name == "Tropical House" or genre_name == "Tech House":
                            genre_name = "House"
                        if genre_name == "Garage Bassline Grime":
                            genre_name = "Garage"
                        if genre_name == "Glitch Hop" or genre_name == "Electronica Downtempo":
                            genre_name = "Electronica"
                        if genre_name == "Hard Dance/ Hard Style":
                            genre_name = "Hard Dance/HardStyle"
                        if genre_name == "Hard Dance Hard Techno" or genre_name == "Minimal Deep Tech" or genre_name == "Techno":
                            genre_name = "Techno"
                        if genre_name == "Hip Hop R&B":
                            genre_name = "Hip Hop & R & B"
                        if genre_name == "Reggae":
                            genre_name = "Reggae"
                        if genre_name == "Big Room":
                            genre_name = "EDM"
                        if genre_name == "Drum & Bass":
                            genre_name = "Drum & Bass"
                        if genre_name == "Psy Trance" or genre_name == "Trance":
                            genre_name = "Trance & Psy Trance"
                        if genre_name == "Tropical House":
                            genre_name = "Tropical House"

                        genre, created = Genre.objects.get_or_create(genreName=genre_name)
                        # artist_genre = ArtistGenre(artistID=artist, genreID=genre)
                        # artist_genre.save()
                        artist_genre, created = ArtistGenre.objects.get_or_create(artistID=artist, genreID=genre)

                sources = ["bandsintown", "resident adviser", "gigatools", "facebook", "soundcloud", "songkick",
                           "twitter", "instagram", "youtube", "spotify", "lastfm"]

                sources_url = [bandsintown, resident, gigatools, facebook, soundcloud, songkick, twitter, instagram,
                               youtube, spotify, lastfm]

                i = 0
                for source in sources:
                    sou = Source.objects.get_or_create(sourceName=source)
                    artist_website = ArtistWebsite()
                    artist_website = ArtistWebsite(artistID=artist, sourceID=sou[0], url=sources_url[i])
                    artist_website.is_verified_link = True
                    artist_website.save()
                    i += 1

                if similar_artists:
                    similar_artist_objs = Artist.allartistobjects.filter(artistName__in=similar_artists)
                    for similar_artist_obj in similar_artist_objs:
                        if not DjSimilarArtists.objects.filter(artistid=artist.artistID,
                                                               linkedartistid=similar_artist_obj.artistID):
                            similar_obj = DjSimilarArtists.objects.create(
                                artistid=artist.artistID,
                                linkedartistid=similar_artist_obj.artistID,
                                sourceid=1,
                                artistname=artist.artistName,
                                updated_at=datetime.now()
                            )
                scraping_artist_id_list.append(str(artist.artistID))

    rebuild_db_handler(scraping_artist_id_list, "sanyam")

    return JsonResponse(response, safe=False)


@login_required
@check_email_validated
def add_added_djs(request):
    context = {}
    profile = Profile.objects.get(user=request.user)
    genres = Genre.objects.all().values_list('genreID', 'genreName')
    form = DjArtistForm(request.POST or None, request.FILES)
    countries = Country.objects.all().order_by("name")

    if request.method == "POST":
        if form.is_valid():
            dj_artist = Artist()
            dj_artist.artistName = form.cleaned_data['artistName']
            # dj_artist.city = form.cleaned_data['city']

            city = form.cleaned_data['city']
            city = city.split(',')
            dj_artist.city = city[0]
            dj_artist.country = form.cleaned_data['country']
            dj_artist.gender = form.cleaned_data['gender']
            dj_artist.artistWebsite = ""
            dj_artist.user_id = 0
            dj_artist.status = "0"
            dj_artist.save()

            genres_id_list = request.POST.getlist('genres', '')

            if genres_id_list:
                genres_id = [int(id) for id in genres_id_list]
                for id in genres_id:
                    genre = Genre.objects.get(genreID=id)
                    artist_genre = ArtistGenre(artistID=dj_artist, genreID=genre)
                    artist_genre.save()

            profile.added_djs.add(dj_artist)

            try:
                data = find_ArtistDetails(dj_artist.artistName, dj_artist)
            except:
                pass

            msg = SendEmail()
            artist_obj = dj_artist

            msg.send_by_template(settings.DEFAULT_ADMIN_EMAILS, "artist/dj_artist_email.html",
                                 # msg.send_by_template(("site-debug@wheredjsplay.com",), "artist/dj_artist_email.html",
                                 context={
                                     "user": request.user,
                                     "artist_obj": artist_obj,
                                     "site_url": settings.SITE_URL,
                                 },
                                 subject="Requested Dj Artist"
                                 )

            msg.send_by_template([request.user.email, ], "artist/artist_added_email.html",
                                 context={
                                     "artist_obj": artist_obj,
                                     "site_url": settings.SITE_URL,
                                 },
                                 subject="Dj Artist"
                                 )

            return redirect(reverse('edit_added_artist_2', kwargs={"artist_id": dj_artist.artistID}))

    else:
        form = DjArtistForm()

    context.update({
        "form": form,
        "countries": countries,
        "genres": genres,
        "added_dj": True,
    })

    return render(request, "artist/add_djartist_1.html", context)


@login_required
@check_email_validated
def edit_added_artist_2(request, artist_id):
    profile = Profile.objects.get(user=request.user)
    if artist_id:
        dj_artist = Artist.allartistobjects.get(artistID=artist_id)
    else:
        dj_artist = profile.dj_artist
    genres = dj_artist.genre.all()
    genre_name_list = []
    for genre in genres:
        genre_name_list.append(genre.genreName)
    genre_names = ",".join(genre_name_list)
    artist_website = dj_artist.artistwebsite_set.all()

    if request.method == "POST":
        sources_url = request.POST.getlist("sources")
        sources = ["bandsintown", "resident adviser", "gigatools", "facebook", "soundcloud", "songkick", "twitter",
                   "instagram", "youtube", "spotify", "lastfm"]
        i = 0
        for source in sources:
            sou = Source.objects.get_or_create(sourceName=source)
            artist_web = ArtistWebsite.objects.get_or_create(artistID=dj_artist, sourceID=sou[0], is_verified_link=True)
            artist_web[0].url = sources_url[i]
            artist_web[0].save()
            i += 1

        return redirect(reverse('edit_added_artist_3', kwargs={"artist_id": dj_artist.artistID}))

    context = {"artist_website": artist_website, "dj_artist": dj_artist, "genre_names": genre_names, "added_dj": True}

    return render(request, "artist/add_djartist_2.html", context)


@login_required
@check_email_validated
def edit_added_artist_3(request, artist_id):
    profile = Profile.objects.get(user=request.user)

    if artist_id:
        dj_artist = Artist.allartistobjects.get(artistID=artist_id)
    else:
        dj_artist = profile.dj_artist

    form = DjArtistForm2(request.POST or None, instance=dj_artist)
    if request.method == "POST":
        if form.is_valid():
            obj = form.save(commit=False)
            obj.save()
        return redirect(reverse('edit_added_artist_4', kwargs={"artist_id": dj_artist.artistID}))

    else:
        form = DjArtistForm2(instance=dj_artist)

    context = {"form": form, "added_dj": True, "dj_artist": dj_artist}
    return render(request, "artist/add_djartist_3.html", context)


@login_required
@check_email_validated
def edit_added_artist_4(request, artist_id):
    profile = Profile.objects.get(user=request.user)
    if artist_id:
        dj_artist = Artist.allartistobjects.get(artistID=artist_id)
    else:
        dj_artist = profile.dj_artist

    form = DjArtistForm3(request.POST or None, request.FILES, instance=dj_artist)
    if request.method == "POST":
        if form.is_valid():
            obj = form.save(commit=False)
            obj.save()


    else:
        form = DjArtistForm3()

    context = {"form": form, "dj_artist": dj_artist, "added_dj": True}

    return render(request, "artist/add_djartist_4.html", context)


@login_required
def edit_added_artist_1(request, artist_id):
    profile = Profile.objects.get(user=request.user)
    if artist_id:
        artist = Artist.allartistobjects.get(artistID=artist_id)
    else:
        artist = profile.dj_artist
    pro_user_type = profile.user_types

    selected_genres = artist.genre.all().values_list('genreID', 'genreName')
    genres = Genre.objects.all().values_list('genreID', 'genreName')
    artist_website = artist.artistwebsite_set.all()

    form = DjArtistForm(request.POST or None, instance=artist)
    countries = Country.objects.all().order_by("name")

    if request.method == "POST":
        if form.is_valid():

            obj = form.save(commit=False)
            obj.save()

            city = form.cleaned_data["city"]
            city = city.split(",")
            artist.city = city[0]
            artist.save()

            for i in artist.artistgenre_set.all():
                i.delete()

            genres_id_list = request.POST.getlist('genres')

            if genres_id_list:
                genres_id = [int(id) for id in genres_id_list]
                for id in genres_id:
                    genre = Genre.objects.get(genreID=id)
                    artist_genre = ArtistGenre(artistID=artist, genreID=genre)
                    artist_genre.save()

            return redirect(reverse("edit_added_artist_2", kwargs={"artist_id": artist.artistID}))

    else:
        form = DjArtistForm(instance=artist)

    context = {
        "form": form, "countries": countries,
        "genres": genres, "selected_genres": selected_genres,
        "artist": artist, "edit": "edit"
    }

    return render(request, "artist/add_djartist_1.html", context)


@login_required
@check_email_validated
@check_plan('Trial', 'Artist/DJ', 'Promoter', 'Agent')
def add_managed_djs(request):
    print "in add added djs ++++++++++++++++++++++++++++++"
    context = {}
    profile = Profile.objects.get(user=request.user)
    user_type = profile.user_types
    genres = Genre.objects.all().values_list('genreID', 'genreName')

    if user_type == "Promoter" or user_type == "Agent":
        form = DjArtistForm(request.POST or None, request.FILES)
        countries = Country.objects.all().order_by("name")

        if request.method == "POST":
            if form.is_valid():
                dj_artist = Artist()
                dj_artist.artistName = form.cleaned_data['artistName']
                dj_artist.city = form.cleaned_data['city']
                dj_artist.country = form.cleaned_data['country']
                dj_artist.gender = form.cleaned_data['gender']
                dj_artist.artistWebsite = ""
                dj_artist.user_id = 0
                dj_artist.status = "0"

                dj_artist.save()

                genres_id_list = request.POST.getlist('genres', '')

                if genres_id_list:
                    genres_id = [int(id) for id in genres_id_list]
                    for id in genres_id:
                        genre = Genre.objects.get(genreID=id)
                        artist_genre = ArtistGenre(artistID=dj_artist, genreID=genre)
                        artist_genre.save()

                profile.profile_managed_djs.add(dj_artist)

                try:
                    data = find_ArtistDetails(dj_artist.artistName, dj_artist)
                except:
                    pass

                msg = SendEmail()
                artist_obj = dj_artist

                msg.send_by_template(settings.DEFAULT_ADMIN_EMAILS, "artist/dj_artist_email.html",
                                     # msg.send_by_template(("site-debug@wheredjsplay.com",), "artist/dj_artist_email.html",
                                     context={
                                         "artist_obj": artist_obj,
                                         "site_url": settings.SITE_URL,
                                     },
                                     subject="Requested Dj Artist"
                                     )

                msg.send_by_template([request.user.email, ], "artist/artist_added_email.html",
                                     context={
                                         "artist_obj": artist_obj,
                                         "site_url": settings.SITE_URL,
                                     },
                                     subject="Dj Artist"
                                     )

                return redirect(reverse('edit_djartist_2', kwargs={"artist_id": dj_artist.artistID}))

        else:
            form = DjArtistForm()

        context.update({"form": form, "countries": countries, "genres": genres})
    else:
        return redirect("/")

    return render(request, "artist/add_djartist_1.html", context)


@login_required
@check_email_validated
@check_plan('Trial', 'Artist/DJ', 'Promoter', 'Agent')
def add_djartist_1(request):
    profile = Profile.objects.get(user=request.user)
    pro_user_type = profile.user_types
    artist = profile.dj_artist
    genres = Genre.objects.all().values_list('genreID', 'genreName')

    if pro_user_type == "Artists/DJs" and artist == None:

        form = DjArtistForm(request.POST or None, request.FILES)
        countries = Country.objects.all().order_by("name")

        if request.method == "POST":
            if form.is_valid():
                dj_artist = Artist()
                dj_artist.artistName = form.cleaned_data['artistName']
                city = form.cleaned_data['city']
                city = city.split(',')
                dj_artist.city = city[0]
                dj_artist.country = form.cleaned_data['country']
                dj_artist.gender = form.cleaned_data['gender']
                dj_artist.artistWebsite = ""
                dj_artist.user_id = 0
                dj_artist.status = "0"

                dj_artist.save()

                genres_id_list = request.POST.getlist('genres', '')

                if genres_id_list:
                    genres_id = [int(id) for id in genres_id_list]
                    for id in genres_id:
                        genre = Genre.objects.get(genreID=id)
                        artist_genre = ArtistGenre(artistID=dj_artist, genreID=genre)
                        artist_genre.save()

                profile.dj_artist = dj_artist
                profile.save()

                try:
                    data = find_ArtistDetails(dj_artist.artistName, dj_artist)
                except:
                    pass

                msg = SendEmail()
                artist_obj = dj_artist

                msg.send_by_template(settings.DEFAULT_ADMIN_EMAILS, "artist/dj_artist_email.html",
                                     # msg.send_by_template(("site-debug@wheredjsplay.com",), "artist/dj_artist_email.html",
                                     context={
                                         "user": request.user,
                                         "artist_obj": artist_obj,
                                         "site_url": settings.SITE_URL,
                                     },
                                     subject="Requested Dj Artist"
                                     )

                return redirect(reverse('add_djartist_2'))

        else:
            form = DjArtistForm()

        context = {"form": form, "countries": countries, "genres": genres}

        return render(request, "artist/add_djartist_1.html", context)

    else:
        return redirect("/")


@login_required
@check_email_validated
def add_djartist_2(request):
    profile = Profile.objects.get(user=request.user)
    dj_artist = profile.dj_artist
    artist_website = dj_artist.artistwebsite_set.all()
    genres = dj_artist.genre.all()
    genre_name_list = []
    for genre in genres:
        genre_name_list.append(genre.genreName)
    genre_names = ",".join(genre_name_list)

    if request.method == "POST":
        sources_url = request.POST.getlist("sources")
        sources = ["bandsintown", "resident adviser", "gigatools", "facebook", "soundcloud", "songkick", "twitter",
                   "instagram", "youtube", "spotify", "lastfm"]
        i = 0
        for source in sources:
            sou = Source.objects.get_or_create(sourceName=source)
            artist_web = ArtistWebsite.objects.get_or_create(artistID=dj_artist, sourceID=sou[0], is_verified_link=True)
            artist_web[0].url = sources_url[i]
            artist_web[0].save()
            i += 1

        return redirect(reverse('add_djartist_3'))

    context = {"artist_website": artist_website, "genre_names": genre_names, "dj_artist": dj_artist}

    return render(request, "artist/add_djartist_2.html", context)


@login_required
@check_email_validated
def add_djartist_3(request):
    profile = Profile.objects.get(user=request.user)
    dj_artist = profile.dj_artist

    form = DjArtistForm2(request.POST or None, )
    if request.method == "POST":
        if form.is_valid():
            dj_artist.artistWebsite = form.cleaned_data['artistWebsite']
            dj_artist.artistAgent = form.cleaned_data['artistAgent']
            dj_artist.press_contact = form.cleaned_data['press_contact']
            dj_artist.contact_no = form.cleaned_data['contact_no']
            dj_artist.general_manager = form.cleaned_data['general_manager']
            dj_artist.save()
        return redirect(reverse('add_djartist_4'))

    else:
        form = DjArtistForm2()

    context = {"form": form}
    return render(request, "artist/add_djartist_3.html", context)


@login_required
@check_email_validated
def add_djartist_4(request):
    profile = Profile.objects.get(user=request.user)
    dj_artist = profile.dj_artist
    form = DjArtistForm3(request.POST or None, request.FILES)
    if request.method == "POST":
        if form.is_valid():
            if request.FILES:
                dj_artist.artistPhoto = request.FILES.get("artistPhoto")
                dj_artist.save()

            # return redirect(reverse("dj_dashboard", kwargs={"artist_id":dj_artist.artistID}))
    else:
        form = DjArtistForm3()

    context = {"form": form, "dj_artist": dj_artist}
    return render(request, "artist/add_djartist_4.html", context)


@login_required
@check_email_validated
def edit_djartist_1(request, artist_id):
    profile = Profile.objects.get(user=request.user)
    if artist_id:
        artist = Artist.allartistobjects.get(artistID=artist_id)
    else:
        artist = profile.dj_artist

    pro_user_type = profile.user_types

    selected_genres = artist.genre.all().values_list('genreID', 'genreName')
    genres = Genre.objects.all().values_list('genreID', 'genreName')
    artist_website = artist.artistwebsite_set.all()

    if pro_user_type == "Artists/DJs" and artist == profile.dj_artist:

        # selected_genres = artist.genre.all().values_list('genreID', 'genreName')
        # genres = Genre.objects.all().values_list('genreID', 'genreName')
        # artist_website = artist.artistwebsite_set.all()

        form = DjArtistForm(request.POST or None, instance=artist)
        countries = Country.objects.all().order_by("name")

        if request.method == "POST":
            if form.is_valid():

                obj = form.save(commit=False)
                obj.save()

                city = form.cleaned_data["city"]
                city = city.split(",")
                artist.city = city[0]
                artist.save()

                for i in artist.artistgenre_set.all():
                    i.delete()

                genres_id_list = request.POST.getlist('genres')

                if genres_id_list:
                    genres_id = [int(id) for id in genres_id_list]
                    for id in genres_id:
                        genre = Genre.objects.get(genreID=id)
                        artist_genre = ArtistGenre(artistID=artist, genreID=genre)
                        artist_genre.save()

                return redirect(reverse("edit_djartist_2", kwargs={"artist_id": artist.artistID}))

        else:
            form = DjArtistForm(instance=artist)

        context = {
            "form": form, "countries": countries,
            "genres": genres, "selected_genres": selected_genres,
            "artist": artist, "edit": "edit"
        }

        return render(request, "artist/add_djartist_1.html", context)

    else:
        if pro_user_type == "Promoter" or pro_user_type == "Agent":
            # if artist in profile.profile_managed_djs.all() :
            if artist.artistID in profile.all_managed_artists.all().values_list('artist', flat=True):
                form = DjArtistForm(request.POST or None, instance=artist)
                countries = Country.objects.all().order_by("name")

                if request.method == "POST":
                    if form.is_valid():

                        obj = form.save(commit=False)
                        obj.save()

                        city = form.cleaned_data["city"]
                        city = city.split(",")
                        artist.city = city[0]
                        artist.save()

                        for i in artist.artistgenre_set.all():
                            i.delete()

                        genres_id_list = request.POST.getlist('genres')

                        if genres_id_list:
                            genres_id = [int(id) for id in genres_id_list]
                            for id in genres_id:
                                genre = Genre.objects.get(genreID=id)
                                artist_genre = ArtistGenre(artistID=artist, genreID=genre)
                                artist_genre.save()

                        return redirect(reverse("edit_djartist_2", kwargs={"artist_id": artist.artistID}))

                else:
                    form = DjArtistForm(instance=artist)

                context = {
                    "form": form, "countries": countries,
                    "genres": genres, "selected_genres": selected_genres,
                    "artist": artist, "edit": "edit"
                }

                return render(request, "artist/add_djartist_1.html", context)

            else:
                return redirect("/")
    return redirect("/")


@login_required
@check_email_validated
def edit_djartist_2(request, artist_id):
    profile = Profile.objects.get(user=request.user)
    if artist_id:
        dj_artist = Artist.allartistobjects.get(artistID=artist_id)
    else:
        dj_artist = profile.dj_artist
    genres = dj_artist.genre.all()
    genre_name_list = []
    if profile.user_types == "Artists/DJs" and dj_artist == profile.dj_artist:
        for genre in genres:
            genre_name_list.append(genre.genreName)
        genre_names = ",".join(genre_name_list)
        artist_website = dj_artist.artistwebsite_set.all()

        if request.method == "POST":
            sources_url = request.POST.getlist("sources")
            sources = ["bandsintown", "resident adviser", "gigatools", "facebook", "soundcloud", "songkick", "twitter",
                       "instagram", "youtube", "spotify", "lastfm"]
            i = 0
            for source in sources:
                sou = Source.objects.get_or_create(sourceName=source)

                artist_web = ArtistWebsite.objects.get_or_create(artistID=dj_artist, sourceID=sou[0],
                                                                 is_verified_link=True)
                artist_web[0].url = sources_url[i]
                artist_web[0].save()
                i += 1

            return redirect(reverse('edit_djartist_3', kwargs={"artist_id": dj_artist.artistID}))

        context = {"artist_website": artist_website, "dj_artist": dj_artist, "genre_names": genre_names, "edit": "edit"}

        return render(request, "artist/add_djartist_2.html", context)
    else:
        if profile.user_types == "Promoter" or profile.user_types == "Agent":
            if dj_artist.artistID in profile.all_managed_artists.all().values_list('artist', flat=True):
                for genre in genres:
                    genre_name_list.append(genre.genreName)
                genre_names = ",".join(genre_name_list)
                artist_website = dj_artist.artistwebsite_set.all()

                if request.method == "POST":
                    sources_url = request.POST.getlist("sources")
                    sources = ["bandsintown", "resident adviser", "gigatools", "facebook", "soundcloud", "songkick",
                               "twitter", "instagram", "youtube", "spotify", "lastfm"]
                    i = 0
                    for source in sources:
                        sou = Source.objects.get_or_create(sourceName=source)
                        artist_web = ArtistWebsite.objects.get_or_create(artistID=dj_artist, sourceID=sou[0],
                                                                         is_verified_link=True)
                        artist_web[0].url = sources_url[i]
                        artist_web[0].save()
                        i += 1

                    return redirect(reverse('edit_djartist_3', kwargs={"artist_id": dj_artist.artistID}))

                context = {"artist_website": artist_website, "dj_artist": dj_artist, "genre_names": genre_names,
                           "edit": "edit", "managed_djs": "managed_djs"}

                return render(request, "artist/add_djartist_2.html", context)

            else:
                return redirect("/")
    return redirect("/")


@login_required
@check_email_validated
def edit_djartist_3(request, artist_id):
    profile = Profile.objects.get(user=request.user)

    if artist_id:
        dj_artist = Artist.allartistobjects.get(artistID=artist_id)
    else:
        dj_artist = profile.dj_artist

    if profile.user_types == "Artists/DJs" and dj_artist == profile.dj_artist:

        form = DjArtistForm2(request.POST or None, instance=dj_artist)
        if request.method == "POST":
            if form.is_valid():
                obj = form.save(commit=False)
                obj.save()
            return redirect(reverse('edit_djartist_4', kwargs={"artist_id": dj_artist.artistID}))

        else:
            form = DjArtistForm2(instance=dj_artist)

        context = {"form": form, "edit": "edit", "dj_artist": dj_artist}
        return render(request, "artist/add_djartist_3.html", context)
    else:
        if profile.user_types == "Promoter" or "Agent":
            # if dj_artist in profile.profile_managed_djs.all() :
            if dj_artist.artistID in profile.all_managed_artists.all().values_list('artist', flat=True):
                form = DjArtistForm2(request.POST or None, instance=dj_artist)
                if request.method == "POST":
                    if form.is_valid():
                        obj = form.save(commit=False)
                        obj.save()
                    return redirect(reverse('edit_djartist_4', kwargs={"artist_id": dj_artist.artistID}))

                else:
                    form = DjArtistForm2(instance=dj_artist)

                context = {"form": form, "edit": "edit", "dj_artist": dj_artist, "managed_djs": "managed_djs"}
                return render(request, "artist/add_djartist_3.html", context)
            else:
                return redirect("/")
    return redirect("/")


@login_required
@check_email_validated
def edit_djartist_4(request, artist_id):
    profile = Profile.objects.get(user=request.user)
    if artist_id:
        dj_artist = Artist.allartistobjects.get(artistID=artist_id)
    else:
        dj_artist = profile.dj_artist

    if profile.user_types == "Artists/DJs" and dj_artist == profile.dj_artist:
        form = DjArtistForm3(request.POST or None, request.FILES, instance=dj_artist)
        if request.method == "POST":
            if form.is_valid():
                obj = form.save(commit=False)
                obj.save()
                # return redirect(reverse("dj_dashboard", kwargs={'artist_id':dj_artist.artistID}))

        else:
            form = DjArtistForm3()

        context = {"form": form, "dj_artist": dj_artist, "edit": "edit"}
        return render(request, "artist/add_djartist_4.html", context)

    else:
        if profile.user_types == "Promoter" or profile.user_types == "Agent":
            # if dj_artist in profile.profile_managed_djs.all() :
            if dj_artist.artistID in profile.all_managed_artists.all().values_list('artist', flat=True):
                form = DjArtistForm3(request.POST or None, request.FILES, instance=dj_artist)
                if request.method == "POST":
                    if form.is_valid():
                        obj = form.save(commit=False)
                        obj.save()
                        # return redirect(reverse("dj_dashboard", kwargs={'artist_id':dj_artist.artistID}))

                else:
                    form = DjArtistForm3()

                context = {"form": form, "dj_artist": dj_artist, "edit": "edit", "managed_djs": "managed_djs"}
                return render(request, "artist/add_djartist_4.html", context)
            else:
                return redirect("/")
    return redirect("/")


@login_required
@check_email_validated
def add_djartist_5(request, artist_id):
    context = {}
    func = request.GET.get("dj", None)
    if func:
        if func == "add_dj":
            context.update({"add_dj": True})
        if func == "manage_dj":
            context.update({"manage_dj": True})

    artist = Artist.allartistobjects.filter(artistID=artist_id)
    artist_website = artist[0].artistwebsite_set.all()

    context.update({
        "artist": artist[0],
        "artist_website": artist_website,
        "added_dj": True,
        # "manage_dj":True,
        "add_dj": True
    })

    return render(request, "artist/add_djartist_5.html", context)


@login_required
@check_email_validated
def add_artist_5(request, artist_id):
    context = {}
    artist = Artist.allartistobjects.filter(artistID=artist_id)
    artist_website = artist[0].artistwebsite_set.all()

    context.update({
        "artist": artist[0],
        "artist_website": artist_website,
        "manage_dj": True,
        # "added_dj":True,
    })

    return render(request, "artist/add_djartist_5.html", context)


@login_required
@check_email_validated
def dj_artist(request):
    profile = Profile.objects.get(user=request.user)
    pro_user_type = profile.user_types
    artist = profile.dj_artist
    genres = Genre.objects.all().values_list('genreID', 'genreName')

    if pro_user_type == "Artists/DJs" and artist == None:

        form = DjArtistForm(request.POST or None, request.FILES)
        countries = Country.objects.all().order_by("name")

        if request.method == "POST":
            if form.is_valid():
                dj_artist = Artist()
                dj_artist.artistName = form.cleaned_data['artistName']
                dj_artist.artistWebsite = form.cleaned_data['artistWebsite']
                dj_artist.artistAgent = form.cleaned_data['artistAgent']
                dj_artist.artistBudget = form.cleaned_data['artistBudget']
                dj_artist.biography = form.cleaned_data['biography']
                dj_artist.remarks = form.cleaned_data['remarks']
                # dj_artist.city = form.cleaned_data['city']
                city = form.cleaned_data['city']
                city = city.split(',')
                dj_artist.city = city[0]
                dj_artist.country = form.cleaned_data['country']
                dj_artist.press_contact = form.cleaned_data['press_contact']
                dj_artist.general_manager = form.cleaned_data['general_manager']
                dj_artist.gender = form.cleaned_data['gender']
                dj_artist.user_id = 0
                dj_artist.status = "0"

                if request.FILES:
                    dj_artist.artistPhoto = request.FILES.get("artistPhoto")
                dj_artist.save()

                genres_id_list = request.POST.getlist('genres', '')

                if genres_id_list:
                    genres_id = [int(id) for id in genres_id_list]
                    for id in genres_id:
                        genre = Genre.objects.get(genreID=id)
                        artist_genre = ArtistGenre(artistID=dj_artist, genreID=genre)
                        artist_genre.save()

                profile.dj_artist = dj_artist
                profile.save()

                sources_url = request.POST.getlist("sources")

                sources = ["bandsintown", "resident adviser", "gigatools", "facebook", "soundcloud", "songkick",
                           "twitter", "instagram", "youtube", "spotify", "lastfm"]
                i = 0
                for source in sources:
                    sou = Source.objects.get_or_create(sourceName=source)
                    artist_website = ArtistWebsite.objects.get_or_create(artistID=dj_artist, sourceID=sou[0],
                                                                         url=sources_url[i], is_verified_link=True)
                    i += 1

                msg = SendEmail()
                artist_obj = dj_artist

                msg.send_by_template(settings.DEFAULT_ADMIN_EMAILS, "artist/dj_artist_email.html",
                                     context={
                                         "artist_obj": artist_obj,
                                         "site_url": settings.SITE_URL,
                                     },
                                     subject="Requested Dj Artist"
                                     )

                return redirect(reverse("dj_dashboard", kwargs={'artist_id': dj_artist.artistID}))

                kwargs = {'artist_id': artist_id_get}

        else:
            form = DjArtistForm()

        context = {"form": form, "countries": countries, "genres": genres}

        return render(request, "artist/dj_artist.html", context)

    else:
        return redirect("/")


@login_required
@check_email_validated
def edit_artist_detail(request):
    profile = Profile.objects.get(user=request.user)
    artist = profile.dj_artist
    pro_user_type = profile.user_types

    if pro_user_type == "Artists/DJs" and artist:
        selected_genres = artist.genre.all().values_list('genreID', 'genreName')
        genres = Genre.objects.all().values_list('genreID', 'genreName')
        artist_website = artist.artistwebsite_set.all()

        form = DjArtistForm(request.POST or None, request.FILES, instance=artist)
        countries = Country.objects.all().order_by("name")

        if request.method == "POST":
            if form.is_valid():

                obj = form.save(commit=False)
                obj.save()

                for i in artist.artistgenre_set.all():
                    i.delete()

                genres_id_list = request.POST.getlist('genres')

                if genres_id_list:
                    genres_id = [int(id) for id in genres_id_list]
                    for id in genres_id:
                        genre = Genre.objects.get(genreID=id)
                        artist_genre = ArtistGenre(artistID=artist, genreID=genre)
                        artist_genre.save()

                sources_url = request.POST.getlist("sources")

                sources = ["bandsintown", "resident adviser", "gigatools", "facebook", "soundcloud", "songkick",
                           "twitter", "instagram", "youtube", "spotify", "lastfm"]
                i = 0
                for source in sources:
                    sou = Source.objects.get_or_create(sourceName=source)
                    artist_web = ArtistWebsite.objects.get_or_create(artistID=artist, sourceID=sou[0],
                                                                     is_verified_link=True)
                    artist_web[0].url = sources_url[i]
                    artist_web[0].save()
                    i += 1

                return redirect(reverse("dj_dashboard", kwargs={"artist_id": artist.artistID}))

        else:
            form = DjArtistForm(instance=artist)

        context = {
            "form": form, "countries": countries,
            "genres": genres, "selected_genres": selected_genres,
            "artist_website": artist_website, "artist": artist
        }

        return render(request, "artist/dj_artist.html", context)

    else:
        return redirect("/")


def validate_artist_name(request):
    response = {"status": False, "errors": []}
    if request.is_ajax():
        artist_name = request.GET['artist_name']

        try:
            artist_obj = request.GET['artist_obj']
        except:
            artist_obj = None

        artist = Artist.allartistobjects.filter(artistName=artist_name)

        if artist_obj:
            if artist_obj == artist_name:
                response["artist"] = False
            else:
                if artist:
                    response["artist"] = True

                else:
                    response["artist"] = False
        else:
            if artist:
                response["artist"] = True

            else:
                response["artist"] = False

    response["status"] = True
    return HttpResponse(json.dumps(response))


def get_artist_data(request):
    results = []
    if request.is_ajax():
        artist_name = request.GET.get('term[term]')
        results = Artist.objects.filter(artistName__icontains=artist_name). \
            values('artistID', 'artistName').distinct()
        results = list(results)
    return HttpResponse(json.dumps(results), content_type='application/json')


def admin_approve(request, artist_id):
    if request.user.is_authenticated():
        if request.user.is_superuser:
            dj_artist = Artist.allartistobjects.get(artistID=artist_id)
            dj_artist.status = "1"
            dj_artist.save()

            messages.add_message(request, messages.SUCCESS, " " + dj_artist.artistName + " has been approved ")

            return redirect(reverse("artist_detail", kwargs={"artist_id": artist_id}))
        else:
            return redirect(reverse("artist_detail", kwargs={"artist_id": artist_id}))

    else:
        return redirect(reverse("artist_detail", kwargs={"artist_id": artist_id}))


def logo(request):
    return render(request, "artist/base_email.html", {})


def social_data_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="social_data.xls"'
    all_artist = Artist.objects.all()
    wb = Workbook()
    sheet1 = wb.add_sheet('sheet 1')

    columns = [
        (u"Artist", 8000),
        (u"Youtube Latest Data Date", 8000),
        (u"Spotify Latest Data Date", 8000),
    ]

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    row_num = 0

    for col_num in xrange(len(columns)):
        sheet1.write(row_num, col_num, columns[col_num][0], font_style)
        sheet1.col(col_num).width = columns[col_num][1]

    for artist in all_artist:
        row_num += 1
        row = [
            artist.artistName,
        ]

        youtube_data = YoutubeData.objects.filter(artist=artist).order_by("created").first()
        if youtube_data:
            youtube_data_created = str(youtube_data.created)
            if youtube_data_created:
                row.append(youtube_data_created)
        else:
            row.append("No Youtube Data Available")

        spotify_data = SpotifyData.objects.filter(artist=artist).order_by("created").first()
        if spotify_data:
            spotify_data_created = str(spotify_data.created)
            if spotify_data_created:
                row.append(spotify_data_created)
        else:
            row.append("No Spotify Data Available")

        for col_num in xrange(len(row)):
            sheet1.write(row_num, col_num, row[col_num])

    wb.save(response)

    return response


@check_email_validated
@check_plan('Trial', 'Artist/DJ', 'Promoter', 'Agent')
def trending_top(request):
    context = dict()
    genres = Genre.objects.all()
    # sources = Tops.objects.exclude(id=6)
    sources = Tops.objects.all()
    time_frame = ((0, '1 Year'), (1, '1 Month'), (2, '3 Months'), (3, '6 Months'))
    time_filter = 0
    type_filter = int(request.GET.get('type', 2))
    context.update(
        {'genres': genres,
         'sources': sources,
         'time_frame': time_frame,
         'fame': Artist.BudgetChoice
         })
    artists = TimeTrends.objects.all()
    if request.GET.get('genre'):
        genre = request.GET.get('genre')
        genre_filter = genres.filter(genreID=genre).get().pk
        artists = artists.filter(artist__genre__genreID=genre)
        context.update(
            {'genre_filter': genre_filter,
             })

    if request.GET.get('channel'):
        source = request.GET.get('channel')
        source_filter = sources.filter(id=source).get().pk
        source_name = sources.filter(id=source).get().top_name
        artists = artists.filter(top=source)
        context.update(
            {'source_filter': source_filter,
             'source_name': source_name,
             })
    else:
        source_filter = sources.filter(top_name="Spotify").get().pk
        source_name = sources.filter(top_name="Spotify").get().top_name
        artists = artists.filter(top=source_filter)
        context.update({
            'source_filter': source_filter,
            'source_name': source_name,
        })
        

    if request.GET.get('fame'):
        fame = request.GET.get('fame')
        fame_filter = int(fame)
        artists = artists.filter(artist__artistBudget=fame)
        context.update(
            {'fame_filter': fame_filter,

             })

    if request.GET.get('timeframe'):
        time = request.GET.get('timeframe')
        time_filter = int(time)
        if type_filter == 0:
            if time_filter == 0:
                artists = artists.order_by('-growth_1yr')
            elif time_filter == 1:
                artists = artists.order_by('-growth_1mth')
            elif time_filter == 2:
                artists = artists.order_by('-growth_3mth')
            elif time_filter == 3:
                artists = artists.order_by('-growth_6mth')
            elif time_filter == 4:
                artists = artists.order_by('-growth_1week')
        elif type_filter == 1:
            if time_filter == 0:
                artists = artists.order_by('-number_1yr')
            elif time_filter == 1:
                artists = artists.order_by('-number_1mth')
            elif time_filter == 2:
                artists = artists.order_by('-number_3mth')
            elif time_filter == 3:
                artists = artists.order_by('-number_6mth')
            elif time_filter == 4:
                artists = artists.order_by('-number_1week')
        context.update(
            {'time_filter': time_filter,
             })
    elif type_filter == 0:
        artists = artists.order_by('-growth_1yr')
    elif type_filter == 1:
        artists = artists.order_by('-number_1yr')
    elif type_filter == 2:
        artists = artists.order_by('-current_count')

    context.update({
        'allartists': artists[:50],
        'type_filter': type_filter,
    })
    return render(request, "artist/trending_top.html", context)

class UserNotes(View):

    def post(self, request,note_id):
        notes= None
        if int(note_id) == 0:
            form = UserNoteForm(request.POST or None)
            if form.is_valid():
                form.instance.modified_date = datetime.now()
                form.save()
            notes = UserNote.objects.filter(user=form.data['user'], artist=form.data['artist']).order_by('-modified_date')
        else:
            note_obj = UserNote.objects.filter(pk=int(note_id)).first()
            if note_obj:
                note_obj.note = request.POST.get('note_text','')
                note_obj.modified_date = datetime.now()
                note_obj.save()
                notes = UserNote.objects.filter(user=note_obj.user, artist=note_obj.artist).order_by('-modified_date')

        return render(request,"artist/user_note.html",context={'notes':notes})

user_note = UserNotes.as_view()

class DeleteNote(View):
    
    def get(self, request,note_id):
        notes= None
        note_obj = UserNote.objects.filter(pk=int(note_id)).first()
        if note_obj:
            note_obj.delete()
            notes = UserNote.objects.filter(user=note_obj.user, artist=note_obj.artist).order_by('-modified_date')
        return render(request,"artist/user_note.html",context={'notes':notes})

delete_note = DeleteNote.as_view()

class UserComments(View):
    
    def post(self, request,artist_id):
        comments = None
        form = CommentForm(request.POST or None)
        artist = Artist.objects.filter(artistID=artist_id).first()
        comment_form = CommentForm()
        if form.is_valid():
            form.instance.user = request.user
            form.instance.artist = artist
            reply_id = request.POST.get('comment_id')
            if reply_id:
                comment_qs = Comment.objects.get(id=reply_id)
                form.instance.reply = comment_qs
            form.save()
            comments = Comment.objects.filter(artist=artist,reply=None).order_by('-id')

        return render(request,"artist/comments.html",context={'comments':comments,'comment_form':comment_form, 'artist':artist})

user_comment = login_required(UserComments.as_view())


class DeleteComment(View):
    
    def get(self, request,comment_id):
        comments = None
        artist = None
        comment_form = CommentForm()
        comment_obj = Comment.objects.filter(id=int(comment_id)).first()
        if comment_obj:
            artist = comment_obj.artist
            comment_obj.delete()
            comments = Comment.objects.filter(artist=artist,reply=None).order_by('-id')
        return render(request,"artist/comments.html",context={'comments':comments,'comment_form':comment_form, 'artist':artist})

delete_comment = DeleteComment.as_view()


# New Add Artist Flow Added For Agent

@method_decorator(check_plan('Agent'), name='dispatch')
class StartAddArtist(LoginRequiredMixin, View):
    
    def get(self, request):
        return render(request, "artist/add_djartist_welcome.html")

start_add_artist = StartAddArtist.as_view()


@method_decorator(check_plan('Agent'), name='dispatch')
class ArtistStep1(LoginRequiredMixin, View):
    
    def get(self, request):
        context = {
            "artist_name":" ",
        }
        return render(request, "artist/add_djartist_step_1.html")

artist_step_1 = ArtistStep1.as_view()


@method_decorator(check_plan('Agent'), name='dispatch')
class ArtistStep1_0(LoginRequiredMixin, TemplateView):
    template_name = "artist/add_djartist_step_1_0.html"

    def get_context_data(self, *args, **kwargs):
        context = super(ArtistStep1_0, self).get_context_data(*args, **kwargs)
        context['artist_name'] = self.request.GET.get('artist_name')
        return context

artist_step_1_0 = ArtistStep1_0.as_view() 

class ArtistStep1_1(LoginRequiredMixin, View):
    
    def get(self, request):
        context = {}
        return redirect(reverse('artist_step_1'))

    def post(self, request):
        artist_name = request.POST.get('artist_name')
        return redirect(reverse('artist_step_2')+"?artist_name="+artist_name)

artist_step_1_1 = ArtistStep1_1.as_view() 

@method_decorator(check_plan('Agent'), name='dispatch')
class ArtistStep2(LoginRequiredMixin, View):
    
    def get(self, request):
        context = {}
        profile = Profile.objects.get(user=request.user)
        user_type = profile.user_types
        genres = Genre.objects.all().values_list('genreID', 'genreName')
        form = DjArtistForm(initial={'artistName': request.GET.get("artist_name","")})
        form.fields['artistName'].widget.attrs['readonly'] = "readonly"
        countries = Country.objects.all().order_by("name")
        context.update({"form": form, "countries": countries, "genres": genres})
        return render(request, "artist/add_djartist_step_2.html", context)

    def post(self, request):
        context = {}
        profile = Profile.objects.get(user=request.user)
        user_type = profile.user_types
        genres = Genre.objects.all().values_list('genreID', 'genreName')

        form = DjArtistForm(request.POST or None, request.FILES)
        countries = Country.objects.all().order_by("name")
        if form.is_valid():
            dj_artist = Artist()
            dj_artist.artistName = form.cleaned_data['artistName']
            dj_artist.city = form.cleaned_data['city']
            dj_artist.country = form.cleaned_data['country']
            dj_artist.gender = form.cleaned_data['gender']
            dj_artist.artistWebsite = ""
            dj_artist.user_id = request.user.id
            dj_artist.status = "0"
            dj_artist.artistBudget = form.cleaned_data['artistBudget']
            dj_artist.is_artist_info_completed = False
            dj_artist.save()

            dj_artist.completed_info_url = reverse('artist_step_2_edit', kwargs={"artist_id": dj_artist.artistID})
            dj_artist.save()

            genres_id_list = request.POST.getlist('genres', '')

            if genres_id_list:
                genres_id = [int(id) for id in genres_id_list]
                for id in genres_id:
                    genre = Genre.objects.get(genreID=id)
                    artist_genre = ArtistGenre(artistID=dj_artist, genreID=genre)
                    artist_genre.save()
            
            # profile.profile_managed_djs.add(dj_artist)

            try:
                data = find_ArtistDetails(dj_artist.artistName, dj_artist)
            except:
                pass
            
            return redirect(reverse('artist_step_3_1', kwargs={"artist_id": dj_artist.artistID}))
        else:
            context.update({"form": form, "countries": countries, "genres": genres})
            return render(request, "artist/add_djartist_step_2.html", context)

artist_step_2 = ArtistStep2.as_view()


@method_decorator(check_plan('Agent'), name='dispatch')
class ArtistStep2Edit(LoginRequiredMixin, View):
    
    def get(self, request,artist_id):
        profile = Profile.objects.get(user=request.user)
        try:
            artist = Artist.allartistobjects.get(artistID=artist_id)
        except Artist.DoesNotExist:
            raise Http404()

        pro_user_type = profile.user_types

        selected_genres = artist.genre.all().values_list('genreID', 'genreName')
        genres = Genre.objects.all().values_list('genreID', 'genreName')
        artist_website = artist.artistwebsite_set.all()

        form = DjArtistForm(instance=artist)
        form.fields['artistName'].widget.attrs['readonly'] = "readonly"
        countries = Country.objects.all().order_by("name")

        context = {
                "form": form, "countries": countries,
                "genres": genres, "selected_genres": selected_genres,
                "artist": artist, "edit": "edit"
                }
        return render(request, "artist/add_djartist_step_2.html", context)

    def post(self, request,artist_id):
        profile = Profile.objects.get(user=request.user)
        try:
            artist = Artist.allartistobjects.get(artistID=artist_id)
        except Artist.DoesNotExist:
            raise Http404()

        pro_user_type = profile.user_types

        selected_genres = artist.genre.all().values_list('genreID', 'genreName')
        genres = Genre.objects.all().values_list('genreID', 'genreName')
        artist_website = artist.artistwebsite_set.all()

        form = DjArtistForm(request.POST or None, instance=artist)
        countries = Country.objects.all().order_by("name")

        if form.is_valid():
            obj = form.save(commit=False)
            obj.save()

            city = form.cleaned_data["city"]
            city = city.split(",")
            artist.city = city[0]
            artist.completed_info_url = reverse('artist_step_2_edit', kwargs={"artist_id": artist.artistID})
            artist.save()

            for i in artist.artistgenre_set.all():
                i.delete()

            genres_id_list = request.POST.getlist('genres')

            if genres_id_list:
                genres_id = [int(id) for id in genres_id_list]
                for id in genres_id:
                    genre = Genre.objects.get(genreID=id)
                    artist_genre = ArtistGenre(artistID=artist, genreID=genre)
                    artist_genre.save()

            return redirect(reverse("artist_step_3_1", kwargs={"artist_id": artist.artistID}))
        else:
            context = {
                    "form": form, "countries": countries,
                    "genres": genres, "selected_genres": selected_genres,
                    "artist": artist, "edit": "edit"
                }
            return render(request, "artist/add_djartist_step_2.html", context)

artist_step_2_edit = ArtistStep2Edit.as_view()

# FACEBOOK View
@method_decorator(check_plan('Agent'), name='dispatch')
class ArtistStep3_1(LoginRequiredMixin, FormView):
    template_name = 'artist/add_djartist_step_3_1_FB.html'
    form_class = ArtistWebsiteForm

    def get_form_kwargs(self):
        form_kwargs = super(ArtistStep3_1, self).get_form_kwargs()
        
        try:
            artist = Artist.allartistobjects.get(artistID=self.kwargs['artist_id'])
        except Artist.DoesNotExist:
            raise Http404()
        
        try:
            source = Source.objects.get(sourceName='Facebook')
        except Source.DoesNotExist:
            raise Http404()

        obj, created = ArtistWebsite.objects.get_or_create(
            sourceID=source,
            artistID=artist,
            defaults={'url': ""},
            )

        form_kwargs['instance'] = obj
        return form_kwargs

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save()
        form.instance.artistID.completed_info_url = reverse("artist_step_3_1", kwargs={"artist_id": form.instance.artistID.artistID})
        form.instance.artistID.save()
        return super(ArtistStep3_1, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(ArtistStep3_1, self).get_context_data(**kwargs)
        context['artist_id'] = self.kwargs['artist_id']
        return context

    def get_success_url(self):
        pk = self.kwargs['artist_id']
        return reverse('artist_step_3_2', kwargs={'artist_id': pk})

artist_step_3_1 = ArtistStep3_1.as_view()


# TWITTER View
@method_decorator(check_plan('Agent'), name='dispatch')
class ArtistStep3_2(LoginRequiredMixin, FormView):
    template_name = 'artist/add_djartist_step_3_2_Twitter.html'
    form_class = ArtistWebsiteForm

    def get_form_kwargs(self):
        form_kwargs = super(ArtistStep3_2, self).get_form_kwargs()
        
        try:
            artist = Artist.allartistobjects.get(artistID=self.kwargs['artist_id'])
        except Artist.DoesNotExist:
            raise Http404()

        try:
            source = Source.objects.get(sourceName='Twitter')
        except Source.DoesNotExist:
            raise Http404()
        
        obj, created = ArtistWebsite.objects.get_or_create(
            sourceID=source,
            artistID=artist,
            defaults={'url': ""},
            )

        form_kwargs['instance'] = obj
        return form_kwargs

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save()
        form.instance.artistID.completed_info_url = reverse("artist_step_3_2", kwargs={"artist_id": form.instance.artistID.artistID})
        form.instance.artistID.save()
        return super(ArtistStep3_2, self).form_valid(form)


    def get_context_data(self, **kwargs):
        context = super(ArtistStep3_2, self).get_context_data(**kwargs)
        context['artist_id'] = self.kwargs['artist_id']
        return context

    def get_success_url(self):
        pk = self.kwargs['artist_id']
        return reverse('artist_step_3_3', kwargs={'artist_id': pk})

artist_step_3_2 = ArtistStep3_2.as_view()


# YOUTUBE View
@method_decorator(check_plan('Agent'), name='dispatch')
class ArtistStep3_3(LoginRequiredMixin, FormView):
    template_name = 'artist/add_djartist_step_3_3_YouTube.html'
    form_class = ArtistWebsiteForm

    def get_form_kwargs(self):
        form_kwargs = super(ArtistStep3_3, self).get_form_kwargs()
        
        try:
            artist = Artist.allartistobjects.get(artistID=self.kwargs['artist_id'])
        except Artist.DoesNotExist:
            raise Http404()

        try:
            source = Source.objects.get(sourceName='Youtube')
        except Source.DoesNotExist:
            raise Http404()
        
        obj, created = ArtistWebsite.objects.get_or_create(
            sourceID=source,
            artistID=artist,
            defaults={'url': ""},
            )

        form_kwargs['instance'] = obj
        return form_kwargs

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save()
        form.instance.artistID.completed_info_url = reverse("artist_step_3_3", kwargs={"artist_id": form.instance.artistID.artistID})
        form.instance.artistID.save()
        return super(ArtistStep3_3, self).form_valid(form)


    def get_context_data(self, **kwargs):
        context = super(ArtistStep3_3, self).get_context_data(**kwargs)
        context['artist_id'] = self.kwargs['artist_id']
        return context

    def get_success_url(self):
        pk = self.kwargs['artist_id']
        return reverse('artist_step_3_4', kwargs={'artist_id': pk})

artist_step_3_3 = ArtistStep3_3.as_view()


# SPOTIFY View
@method_decorator(check_plan('Agent'), name='dispatch')
class ArtistStep3_4(LoginRequiredMixin, FormView):
    template_name = 'artist/add_djartist_step_3_4_Spotify.html'
    form_class = ArtistWebsiteForm

    def get_form_kwargs(self):
        form_kwargs = super(ArtistStep3_4, self).get_form_kwargs()
        
        try:
            artist = Artist.allartistobjects.get(artistID=self.kwargs['artist_id'])
        except Artist.DoesNotExist:
            raise Http404()

        try:
            source = Source.objects.get(sourceName='Spotify')
        except Source.DoesNotExist:
            raise Http404()
        
        obj, created = ArtistWebsite.objects.get_or_create(
            sourceID=source,
            artistID=artist,
            defaults={'url': ""},
            )

        form_kwargs['instance'] = obj
        return form_kwargs

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save()
        form.instance.artistID.completed_info_url = reverse("artist_step_3_4", kwargs={"artist_id": form.instance.artistID.artistID})
        form.instance.artistID.save()
        return super(ArtistStep3_4, self).form_valid(form)


    def get_context_data(self, **kwargs):
        context = super(ArtistStep3_4, self).get_context_data(**kwargs)
        context['artist_id'] = self.kwargs['artist_id']
        return context

    def get_success_url(self):
        pk = self.kwargs['artist_id']
        return reverse('artist_step_3_5', kwargs={'artist_id': pk})

artist_step_3_4 = ArtistStep3_4.as_view()


# Sound Cloud View
@method_decorator(check_plan('Agent'), name='dispatch')
class ArtistStep3_5(LoginRequiredMixin, FormView):
    template_name = 'artist/add_djartist_step_3_5_SoundCloud.html'
    form_class = ArtistWebsiteForm

    def get_form_kwargs(self):
        form_kwargs = super(ArtistStep3_5, self).get_form_kwargs()
        
        try:
            artist = Artist.allartistobjects.get(artistID=self.kwargs['artist_id'])
        except Artist.DoesNotExist:
            raise Http404()

        try:
            source = Source.objects.get(sourceName='Soundcloud')
        except Source.DoesNotExist:
            raise Http404()
        
        obj, created = ArtistWebsite.objects.get_or_create(
            sourceID=source,
            artistID=artist,
            defaults={'url': ""},
            )

        form_kwargs['instance'] = obj
        return form_kwargs

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save()
        form.instance.artistID.completed_info_url = reverse("artist_step_3_5", kwargs={"artist_id": form.instance.artistID.artistID})
        form.instance.artistID.save()
        return super(ArtistStep3_5, self).form_valid(form)


    def get_context_data(self, **kwargs):
        context = super(ArtistStep3_5, self).get_context_data(**kwargs)
        context['artist_id'] = self.kwargs['artist_id']
        return context

    def get_success_url(self):
        pk = self.kwargs['artist_id']
        return reverse('artist_step_3_6', kwargs={'artist_id': pk})

artist_step_3_5 = ArtistStep3_5.as_view()


# SongKick View
@method_decorator(check_plan('Agent'), name='dispatch')
class ArtistStep3_6(LoginRequiredMixin, FormView):
    template_name = 'artist/add_djartist_step_3_6_SongKick.html'
    form_class = ArtistWebsiteForm

    def get_form_kwargs(self):
        form_kwargs = super(ArtistStep3_6, self).get_form_kwargs()
        try:
            artist = Artist.allartistobjects.get(artistID=self.kwargs['artist_id'])
        except Artist.DoesNotExist:
            raise Http404()

        try:
            source = Source.objects.get(sourceName='Songkick')
        except Source.DoesNotExist:
            raise Http404()
        
        obj, created = ArtistWebsite.objects.get_or_create(
            sourceID=source,
            artistID=artist,
            defaults={'url': ""},
            )

        form_kwargs['instance'] = obj
        return form_kwargs

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save()
        form.instance.artistID.completed_info_url = reverse("artist_step_3_6", kwargs={"artist_id": form.instance.artistID.artistID})
        form.instance.artistID.save()
        return super(ArtistStep3_6, self).form_valid(form)


    def get_context_data(self, **kwargs):
        context = super(ArtistStep3_6, self).get_context_data(**kwargs)
        context['artist_id'] = self.kwargs['artist_id']
        return context

    def get_success_url(self):
        pk = self.kwargs['artist_id']
        return reverse('artist_step_3_7', kwargs={'artist_id': pk})

artist_step_3_6 = ArtistStep3_6.as_view()


# GigaTools View
@method_decorator(check_plan('Agent'), name='dispatch')
class ArtistStep3_7(LoginRequiredMixin, FormView):
    template_name = 'artist/add_djartist_step_3_7_Gigatools.html'
    form_class = ArtistWebsiteForm

    def get_form_kwargs(self):
        form_kwargs = super(ArtistStep3_7, self).get_form_kwargs()
        
        try:
            artist = Artist.allartistobjects.get(artistID=self.kwargs['artist_id'])
        except Artist.DoesNotExist:
            raise Http404()

        try:
            source = Source.objects.get(sourceName='Gigatools')
        except Source.DoesNotExist:
            raise Http404()
        
        
        obj, created = ArtistWebsite.objects.get_or_create(
            sourceID=source,
            artistID=artist,
            defaults={'url': ""},
            )

        form_kwargs['instance'] = obj
        return form_kwargs

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save()
        form.instance.artistID.completed_info_url = reverse("artist_step_3_7", kwargs={"artist_id": form.instance.artistID.artistID})
        form.instance.artistID.save()
        return super(ArtistStep3_7, self).form_valid(form)


    def get_context_data(self, **kwargs):
        context = super(ArtistStep3_7, self).get_context_data(**kwargs)
        context['artist_id'] = self.kwargs['artist_id']
        return context

    def get_success_url(self):
        pk = self.kwargs['artist_id']
        return reverse('artist_step_3_8', kwargs={'artist_id': pk})

artist_step_3_7 = ArtistStep3_7.as_view()


# BandsInTown View
@method_decorator(check_plan('Agent'), name='dispatch')
class ArtistStep3_8(LoginRequiredMixin, FormView):
    template_name = 'artist/add_djartist_step_3_8_BandsInTown.html'
    form_class = ArtistWebsiteForm

    def get_form_kwargs(self):
        form_kwargs = super(ArtistStep3_8, self).get_form_kwargs()
        
        try:
            artist = Artist.allartistobjects.get(artistID=self.kwargs['artist_id'])
        except Artist.DoesNotExist:
            raise Http404()

        try:
            source = Source.objects.get(sourceName='Bandsintown')
        except Source.DoesNotExist:
            raise Http404()
        
        obj, created = ArtistWebsite.objects.get_or_create(
            sourceID=source,
            artistID=artist,
            defaults={'url': ""},
            )

        form_kwargs['instance'] = obj
        return form_kwargs

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save()
        form.instance.artistID.completed_info_url = reverse("artist_step_3_8", kwargs={"artist_id": form.instance.artistID.artistID})
        form.instance.artistID.save()
        return super(ArtistStep3_8, self).form_valid(form)


    def get_context_data(self, **kwargs):
        context = super(ArtistStep3_8, self).get_context_data(**kwargs)
        context['artist_id'] = self.kwargs['artist_id']
        return context

    def get_success_url(self):
        pk = self.kwargs['artist_id']
        return reverse('artist_step_3_9', kwargs={'artist_id': pk})

artist_step_3_8 = ArtistStep3_8.as_view()


# Resident Adviser View
@method_decorator(check_plan('Agent'), name='dispatch')
class ArtistStep3_9(LoginRequiredMixin, FormView):
    template_name = 'artist/add_djartist_step_3_9_ResidentAdviser.html'
    form_class = ArtistWebsiteForm

    def get_form_kwargs(self):
        form_kwargs = super(ArtistStep3_9, self).get_form_kwargs()
        
        try:
            artist = Artist.allartistobjects.get(artistID=self.kwargs['artist_id'])
        except Artist.DoesNotExist:
            raise Http404()
        
        try:
            source = Source.objects.get(sourceName='ResidentAdviser')
        except Source.DoesNotExist:
            raise Http404()
        
        obj, created = ArtistWebsite.objects.get_or_create(
            sourceID=source,
            artistID=artist,
            defaults={'url': ""},
            )

        form_kwargs['instance'] = obj
        return form_kwargs

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save()
        form.instance.artistID.completed_info_url = reverse("artist_step_3_9", kwargs={"artist_id": form.instance.artistID.artistID})
        form.instance.artistID.save()
        return super(ArtistStep3_9, self).form_valid(form)


    def get_context_data(self, **kwargs):
        context = super(ArtistStep3_9, self).get_context_data(**kwargs)
        context['artist_id'] = self.kwargs['artist_id']
        return context

    def get_success_url(self):
        pk = self.kwargs['artist_id']
        return reverse('artist_step_3_10', kwargs={'artist_id': pk})

artist_step_3_9 = ArtistStep3_9.as_view()


# Instagram View
@method_decorator(check_plan('Agent'), name='dispatch')
class ArtistStep3_10(LoginRequiredMixin, FormView):
    template_name = 'artist/add_djartist_step_3_10_Instagram.html'
    form_class = ArtistWebsiteForm

    def get_form_kwargs(self):
        form_kwargs = super(ArtistStep3_10, self).get_form_kwargs()
        
        try:
            artist = Artist.allartistobjects.get(artistID=self.kwargs['artist_id'])
        except Artist.DoesNotExist:
            raise Http404()
        
        try:
            source = Source.objects.get(sourceName='Instagram')
        except Source.DoesNotExist:
            raise Http404()
        
        obj, created = ArtistWebsite.objects.get_or_create(
            sourceID=source,
            artistID=artist,
            defaults={'url': ""},
            )

        form_kwargs['instance'] = obj
        return form_kwargs

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save()
        form.instance.artistID.completed_info_url = reverse("artist_step_3_10", kwargs={"artist_id": form.instance.artistID.artistID})
        form.instance.artistID.save()
        return super(ArtistStep3_10, self).form_valid(form)


    def get_context_data(self, **kwargs):
        context = super(ArtistStep3_10, self).get_context_data(**kwargs)
        context['artist_id'] = self.kwargs['artist_id']
        return context

    def get_success_url(self):
        pk = self.kwargs['artist_id']
        return reverse('artist_step_3_11', kwargs={'artist_id': pk})

artist_step_3_10 = ArtistStep3_10.as_view()


# Last FM View
@method_decorator(check_plan('Agent'), name='dispatch')
class ArtistStep3_11(LoginRequiredMixin, FormView):
    template_name = 'artist/add_djartist_step_3_11_LastFM.html'
    form_class = ArtistWebsiteForm

    def get_form_kwargs(self):
        form_kwargs = super(ArtistStep3_11, self).get_form_kwargs()
        
        try:
            artist = Artist.allartistobjects.get(artistID=self.kwargs['artist_id'])
        except Artist.DoesNotExist:
            raise Http404()
        
        try:
            source = Source.objects.get(sourceName='lastfm')
        except Source.DoesNotExist:
            raise Http404()
        
        obj, created = ArtistWebsite.objects.get_or_create(
            sourceID=source,
            artistID=artist,
            defaults={'url': ""},
            )

        form_kwargs['instance'] = obj
        return form_kwargs

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save()
        form.instance.artistID.completed_info_url = reverse("artist_step_3_11", kwargs={"artist_id": form.instance.artistID.artistID})
        form.instance.artistID.save()
        return super(ArtistStep3_11, self).form_valid(form)


    def get_context_data(self, **kwargs):
        context = super(ArtistStep3_11, self).get_context_data(**kwargs)
        context['artist_id'] = self.kwargs['artist_id']
        return context

    def get_success_url(self):
        pk = self.kwargs['artist_id']
        return reverse('artist_step_3_12', kwargs={'artist_id': pk})

artist_step_3_11 = ArtistStep3_11.as_view()


# Electronic Festivals View
@method_decorator(check_plan('Agent'), name='dispatch')
class ArtistStep3_12(LoginRequiredMixin, FormView):
    template_name = 'artist/add_djartist_step_3_12_ElectronicFestivals.html'
    form_class = ArtistWebsiteForm

    def get_form_kwargs(self):
        form_kwargs = super(ArtistStep3_12, self).get_form_kwargs()
        
        try:
            artist = Artist.allartistobjects.get(artistID=self.kwargs['artist_id'])
        except Artist.DoesNotExist:
            raise Http404()
        
        try:
            source = Source.objects.get(sourceName='ElectronicFestivals')
        except Source.DoesNotExist:
            raise Http404()
        
        obj, created = ArtistWebsite.objects.get_or_create(
            sourceID=source,
            artistID=artist,
            defaults={'url': ""},
            )

        form_kwargs['instance'] = obj
        return form_kwargs

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save()
        form.instance.artistID.completed_info_url = reverse("artist_step_3_12", kwargs={"artist_id": form.instance.artistID.artistID})
        form.instance.artistID.save()
        return super(ArtistStep3_12, self).form_valid(form)


    def get_context_data(self, **kwargs):
        context = super(ArtistStep3_12, self).get_context_data(**kwargs)
        context['artist_id'] = self.kwargs['artist_id']
        return context

    def get_success_url(self):
        pk = self.kwargs['artist_id']
        return reverse('artist_step_3_13', kwargs={'artist_id': pk})

artist_step_3_12 = ArtistStep3_12.as_view()


# BeatPort View
@method_decorator(check_plan('Agent'), name='dispatch')
class ArtistStep3_13(LoginRequiredMixin, FormView):
    template_name = 'artist/add_djartist_step_3_13_Beatport.html'
    form_class = ArtistWebsiteForm

    def get_form_kwargs(self):
        form_kwargs = super(ArtistStep3_13, self).get_form_kwargs()
        
        try:
            artist = Artist.allartistobjects.get(artistID=self.kwargs['artist_id'])
        except Artist.DoesNotExist:
            raise Http404()
        
        try:
            source = Source.objects.get(sourceName='BeatPort')
        except Source.DoesNotExist:
            raise Http404()
        
        obj, created = ArtistWebsite.objects.get_or_create(
            sourceID=source,
            artistID=artist,
            defaults={'url': ""},
            )

        form_kwargs['instance'] = obj
        return form_kwargs

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save()
        form.instance.artistID.completed_info_url = reverse("artist_step_3_13", kwargs={"artist_id": form.instance.artistID.artistID})
        form.instance.artistID.save()
        return super(ArtistStep3_13, self).form_valid(form)


    def get_context_data(self, **kwargs):
        context = super(ArtistStep3_13, self).get_context_data(**kwargs)
        context['artist_id'] = self.kwargs['artist_id']
        return context

    def get_success_url(self):
        pk = self.kwargs['artist_id']
        return reverse('artist_step_3_14', kwargs={'artist_id': pk})

artist_step_3_13 = ArtistStep3_13.as_view()


# Ticket Master View
@method_decorator(check_plan('Agent'), name='dispatch')
class ArtistStep3_14(LoginRequiredMixin, FormView):
    template_name = 'artist/add_djartist_step_3_14_TicketMaster.html'
    form_class = ArtistWebsiteForm

    def get_form_kwargs(self):
        form_kwargs = super(ArtistStep3_14, self).get_form_kwargs()
        
        try:
            artist = Artist.allartistobjects.get(artistID=self.kwargs['artist_id'])
        except Artist.DoesNotExist:
            raise Http404()
        
        try:
            source = Source.objects.get(sourceName='TicketMaster')
        except Source.DoesNotExist:
            raise Http404()
        
        obj, created = ArtistWebsite.objects.get_or_create(
            sourceID=source,
            artistID=artist,
            defaults={'url': ""},
            )

        form_kwargs['instance'] = obj
        return form_kwargs

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save()
        form.instance.artistID.completed_info_url = reverse("artist_step_3_14", kwargs={"artist_id": form.instance.artistID.artistID})
        form.instance.artistID.save()
        return super(ArtistStep3_14, self).form_valid(form)


    def get_context_data(self, **kwargs):
        context = super(ArtistStep3_14, self).get_context_data(**kwargs)
        context['artist_id'] = self.kwargs['artist_id']
        return context

    def get_success_url(self):
        pk = self.kwargs['artist_id']
        return reverse('artist_step_3_15', kwargs={'artist_id': pk})

artist_step_3_14 = ArtistStep3_14.as_view()


# Event Brite View
@method_decorator(check_plan('Agent'), name='dispatch')
class ArtistStep3_15(LoginRequiredMixin, FormView):
    template_name = 'artist/add_djartist_step_3_15_EventBrite.html'
    form_class = ArtistWebsiteForm

    def get_form_kwargs(self):
        form_kwargs = super(ArtistStep3_15, self).get_form_kwargs()
        
        try:
            artist = Artist.allartistobjects.get(artistID=self.kwargs['artist_id'])
        except Artist.DoesNotExist:
            raise Http404()
        
        try:
            source = Source.objects.get(sourceName='EventBrite')
        except Source.DoesNotExist:
            raise Http404()
        
        obj, created = ArtistWebsite.objects.get_or_create(
            sourceID=source,
            artistID=artist,
            defaults={'url': ""},
            )

        form_kwargs['instance'] = obj
        return form_kwargs

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save()
        form.instance.artistID.completed_info_url = reverse("artist_step_3_15", kwargs={"artist_id": form.instance.artistID.artistID})
        form.instance.artistID.save()
        return super(ArtistStep3_15, self).form_valid(form)


    def get_context_data(self, **kwargs):
        context = super(ArtistStep3_15, self).get_context_data(**kwargs)
        context['artist_id'] = self.kwargs['artist_id']
        return context

    def get_success_url(self):
        pk = self.kwargs['artist_id']
        return reverse('artist_step_3_16', kwargs={'artist_id': pk})

artist_step_3_15 = ArtistStep3_15.as_view()


# IflyerJP View
@method_decorator(check_plan('Agent'), name='dispatch')
class ArtistStep3_16(LoginRequiredMixin, FormView):
    template_name = 'artist/add_djartist_step_3_16_IflyerJP.html'
    form_class = ArtistWebsiteForm

    def get_form_kwargs(self):
        form_kwargs = super(ArtistStep3_16, self).get_form_kwargs()
        
        try:
            artist = Artist.allartistobjects.get(artistID=self.kwargs['artist_id'])
        except Artist.DoesNotExist:
            raise Http404()
        
        try:
            source = Source.objects.get(sourceName='IflyerJP')
        except Source.DoesNotExist:
            raise Http404()
        
        obj, created = ArtistWebsite.objects.get_or_create(
            sourceID=source,
            artistID=artist,
            defaults={'url': ""},
            )

        form_kwargs['instance'] = obj
        return form_kwargs

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save()
        form.instance.artistID.completed_info_url = reverse("artist_step_3_16", kwargs={"artist_id": form.instance.artistID.artistID})
        form.instance.artistID.save()
        return super(ArtistStep3_16, self).form_valid(form)


    def get_context_data(self, **kwargs):
        context = super(ArtistStep3_16, self).get_context_data(**kwargs)
        context['artist_id'] = self.kwargs['artist_id']
        return context

    def get_success_url(self):
        pk = self.kwargs['artist_id']
        return reverse('artist_step_4', kwargs={'artist_id': pk})

artist_step_3_16 = ArtistStep3_16.as_view()


# Previous Step3 Method where we get all URLs on single page.

# @method_decorator(check_plan('Agent'), name='dispatch')
# class ArtistStep3(LoginRequiredMixin, View):
    
#     def get(self, request, artist_id, ):
#         profile = Profile.objects.get(user=request.user)
        
#         try:
#             dj_artist = Artist.allartistobjects.get(artistID=artist_id)
#         except Artist.DoesNotExist:
#             raise Http404()

#         genres = dj_artist.genre.all()
#         genre_name_list = []
        
#         for genre in genres:
#             genre_name_list.append(genre.genreName)

#         genre_names = ",".join(genre_name_list)
#         # get artist website list by sourcename alphabetically
#         artist_website = dj_artist.artistwebsite_set.all().order_by('sourceID__sourceName')
#         artist_website_ordered = artist_website
#         if artist_website and len(artist_website) == 11:
#             # Reordering according to list provided in post while saving so that order remains same.
#             artist_website_ordered = [artist_website[0],artist_website[5],artist_website[2],artist_website[1],artist_website[7],artist_website[6],artist_website[9],artist_website[3],artist_website[10],artist_website[8],artist_website[4]]

#         context = {"artist_website": artist_website_ordered, "dj_artist": dj_artist, "genre_names": genre_names,
#                 "edit": "edit"}

#         return render(request, "artist/add_djartist_step_3.html", context)


#     def post(self, request, artist_id):
#         profile = Profile.objects.get(user=request.user)
        
#         try:
#             dj_artist = Artist.allartistobjects.get(artistID=artist_id)
#         except Artist.DoesNotExist:
#             raise Http404()

#         genres = dj_artist.genre.all()
#         genre_name_list = []

#         for genre in genres:
#             genre_name_list.append(genre.genreName)
#         genre_names = ",".join(genre_name_list)
        
#         # Name defined in html
#         sources_url = ["bandsintown", "resident_adviser", "gigatools", "facebook", "soundcloud", "songkick", "twitter", "instagram", "youtube", "spotify", "lastfm"]
        
#         sources = ["bandsintown", "resident adviser", "gigatools", "facebook", "soundcloud", "songkick",
#                 "twitter", "instagram", "youtube", "spotify", "lastfm"]
#         i = 0
#         for source in sources:
#             sou = Source.objects.get_or_create(sourceName=source)
#             artist_web = ArtistWebsite.objects.get_or_create(artistID=dj_artist, sourceID=sou[0],
#                                                             is_verified_link=True)
#             artist_web[0].url = request.POST.get(sources_url[i])
#             artist_web[0].save()
#             i += 1

#         return redirect(reverse('artist_step_4', kwargs={"artist_id": dj_artist.artistID}))

# artist_step_3 = ArtistStep3.as_view()

@method_decorator(check_plan('Agent'), name='dispatch')
class ArtistStep4(LoginRequiredMixin, View):
    
    def get(self, request, artist_id):
        profile = Profile.objects.get(user=request.user)
        
        try:
            dj_artist = Artist.allartistobjects.get(artistID=artist_id)
        except Artist.DoesNotExist:
            raise Http404()

        form = DjArtistForm2(instance=dj_artist)
        context = {"form": form, "edit": "edit", "dj_artist": dj_artist}
        return render(request, "artist/add_djartist_step_4.html", context)

    def post(self, request, artist_id):
        profile = Profile.objects.get(user=request.user)
        
        try:
            dj_artist = Artist.allartistobjects.get(artistID=artist_id)
        except Artist.DoesNotExist:
            raise Http404()

        form = DjArtistForm2(request.POST or None, instance=dj_artist)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.save()
            form.instance.completed_info_url = reverse("artist_step_4", kwargs={"artist_id": form.instance.artistID})
            form.instance.save()
            return redirect(reverse('artist_step_5', kwargs={"artist_id": dj_artist.artistID}))
        else:
            form = DjArtistForm2(instance=dj_artist)
            context = {"form": form, "edit": "edit", "dj_artist": dj_artist}
            return render(request, "artist/add_djartist_step_4.html", context)

artist_step_4 = ArtistStep4.as_view()

@method_decorator(check_plan('Agent'), name='dispatch')
class ArtistStep5(LoginRequiredMixin, View):
    
    def get(self, request, artist_id):
        profile = Profile.objects.get(user=request.user)
        
        try:
            dj_artist = Artist.allartistobjects.get(artistID=artist_id)
        except Artist.DoesNotExist:
            raise Http404()

        form = DjArtistForm3(instance=dj_artist)
        context = {
            "form": form, 
            "dj_artist": dj_artist, 
            "edit": "edit",
            }
        return render(request, "artist/add_djartist_step_5.html", context)

    def post(self, request, artist_id):
        profile = Profile.objects.get(user=request.user)
        
        try:
            dj_artist = Artist.allartistobjects.get(artistID=artist_id)
        except Artist.DoesNotExist:
            raise Http404()

        form = DjArtistForm3(request.POST or None, request.FILES, instance=dj_artist)
        if form.is_valid():
            obj = form.save()
            obj.save()
            form.instance.completed_info_url = reverse("artist_step_5", kwargs={"artist_id": form.instance.artistID})
            form.instance.save()
        else:
            form.instance.completed_info_url = reverse("artist_step_5", kwargs={"artist_id": form.instance.artistID})
            form.instance.save()
        return redirect(reverse('artist_step_5_confirm', kwargs={"artist_id": dj_artist.artistID}))

artist_step_5 = ArtistStep5.as_view()

@method_decorator(check_plan('Agent'), name='dispatch')
class ArtistStep5Confirm(LoginRequiredMixin, View):
    
    def get(self, request, artist_id):
        profile = Profile.objects.get(user=request.user)
        
        try:
            dj_artist = Artist.allartistobjects.get(artistID=artist_id)
        except Artist.DoesNotExist:
            raise Http404()
        
        context = {"dj_artist":dj_artist}

        return render(request,"artist/add_djartist_image_confirm.html",context)

artist_step_5_confirm = ArtistStep5Confirm.as_view()


@method_decorator(check_plan('Agent'), name='dispatch')
class ArtistStepProfile(LoginRequiredMixin, FormView):
    template_name = 'artist/add_djartist_step_profile.html'
    form_class = ArtistProfileForm

    def get_form_kwargs(self):
        form_kwargs = super(ArtistStepProfile, self).get_form_kwargs()
        
        try:
            artist = Artist.allartistobjects.get(artistID=self.kwargs['artist_id'])
        except Artist.DoesNotExist:
            raise Http404()
        
        form_kwargs['instance'] = artist
        return form_kwargs

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save()
        form.instance.completed_info_url = reverse("artist_step_profile", kwargs={"artist_id": form.instance.artistID})
        form.instance.save()
        return super(ArtistStepProfile, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(ArtistStepProfile, self).get_context_data(**kwargs)
        context['artist_id'] = self.kwargs['artist_id']
        return context

    def get_success_url(self):
        pk = self.kwargs['artist_id']
        return reverse('artist_step_similar_artists', kwargs={'artist_id': pk})

artist_step_profile = ArtistStepProfile.as_view()


@method_decorator(check_plan('Agent'), name='dispatch')
class SimilarArtistsStep(LoginRequiredMixin, View):
    
    def get(self, request, artist_id):
        try:
            artist = Artist.allartistobjects.get(artistID=self.kwargs['artist_id'])
        except Artist.DoesNotExist:
            raise Http404()

        similar_artists = get_similar_artist(artist)
        similar_artists_ids = []
        
        if similar_artists:
            similar_artists_ids = similar_artists.values_list('artistID')

        all_artists = Artist.objects.get_queryset().all().values_list('artistID', 'artistName')
        
        context = {
            "added_artist" : artist,
            "artists": all_artists,
            "artist_similar_artists": similar_artists,
            "similar_artists_ids": similar_artists_ids,
        }

        return render(request, 'artist/add_djartist_step_similar_artists.html', context)

    def post(self, request, artist_id):
        profile = Profile.objects.get(user=request.user)
        artist_id = self.kwargs["artist_id"]
        similar_artists_ids = request.POST.getlist('similar_artists')

        for similar_artist_id in similar_artists_ids:
            artist = Artist.objects.get(artistID=similar_artist_id)
            similar_artist_obj = DjSimilarArtists(artistid=artist_id, sourceid=1, artistname=artist.artistName, linkedartistid=artist.artistID)
            similar_artist_obj.save()

        # return redirect('/artists/artist_step_6/{}'.format(artist_id))
        return redirect(reverse('artist_step_6', kwargs={'artist_id': artist_id}))

artist_step_similar_artists = SimilarArtistsStep.as_view()


@method_decorator(check_plan('Agent'), name='dispatch')
class ArtistStep6(LoginRequiredMixin, View):
    
    def get(self, request, artist_id):
        context = {}
        try:
            artist = Artist.allartistobjects.get(artistID=artist_id)
        except Artist.DoesNotExist:
            raise Http404()

        context["similar_artists"] = get_similar_artist(artist)
        
        # get artist website list by sourcename alphabetically
        artist_website_ordered = []
        sources = ["Facebook", "Twitter", "Youtube", "Spotify", "Soundcloud", "Songkick", "Gigatools", "Bandsintown", "ResidentAdviser", "Instagram", "ElectronicFestivals", "BeatPort", "TicketMaster", "EventBrite", "IflyerJP"]
        for src in sources:
            print(src)
            aw = ArtistWebsite.objects.filter(sourceID__sourceName=src, artistID=artist).first()
            if aw:
                artist_website_ordered.append(aw.url)
            else:
                artist_website_ordered.append("No URL Added for " %src)

        # artist_website = artist.artistwebsite_set.all().order_by('sourceID__sourceName')
        # artist_website_ordered = artist_website
        # if artist_website and len(artist_website) == 11:
        #     # Reordering according to list provided in post while saving so that order remains same.
        #     artist_website_ordered = [artist_website[0],artist_website[5],artist_website[2],artist_website[1],artist_website[7],artist_website[6],artist_website[9],artist_website[3],artist_website[10],artist_website[8],artist_website[4]]
        
        context.update({
            "artist": artist,
            "artist_website": artist_website_ordered,
            })
        return render(request,"artist/add_djartist_step_6.html",context)

artist_step_6 = ArtistStep6.as_view()


class ArtistStep6Preview(LoginRequiredMixin, View):
    
    def get(self, request, artist_id):
        if request.user.is_superuser:
            context = {}
            try:
                artist = Artist.allartistobjects.get(artistID=artist_id)
            except Artist.DoesNotExist:
                raise Http404()

            # get artist website list by sourcename alphabetically
            # artist_website = artist.artistwebsite_set.all().order_by('sourceID__sourceName')
            # artist_website_ordered = artist_website
            # if artist_website and len(artist_website) == 11:
            #     # Reordering according to list provided in post while saving so that order remains same.
            #     artist_website_ordered = [artist_website[0],artist_website[5],artist_website[2],artist_website[1],artist_website[7],artist_website[6],artist_website[9],artist_website[3],artist_website[10],artist_website[8],artist_website[4]]
            artist_website_ordered = []
            sources = ["Facebook", "Twitter", "Youtube", "Spotify", "Soundcloud", "Songkick", "Gigatools", "Bandsintown", "ResidentAdviser", "Instagram", "ElectronicFestivals", "BeatPort", "TicketMaster", "EventBrite", "IflyerJP"]
            for src in sources:
                aw = ArtistWebsite.objects.filter(sourceID__sourceName=src, artistID=artist).first()
                if aw:
                    artist_website_ordered.append(aw.url)
                else:
                    artist_website_ordered.append("")
            form = AddArtistRequestForm()

            context.update({
                "artist": artist,
                "artist_website": artist_website_ordered,
                "preview" : "preview",
                "form":form,
                "artist_status": AddArtistRequest.objects.filter(artist=artist).first()
                })
            return render(request,"artist/add_djartist_step_6.html",context)
        else:
            return redirect(reverse('dashboard'))

    def post(self, request, artist_id):
        
        try:
            dj_artist = Artist.allartistobjects.get(artistID=artist_id)
        except Artist.DoesNotExist:
            raise Http404()
        form = AddArtistRequestForm(request.POST or None)
        status = form.data["approved"]
        subject = "Your Request to Add %s is Rejected"%(dj_artist.artistName)
        artist_request = AddArtistRequest.objects.filter(artist=dj_artist).first()
        if artist_request:
            artist_request.comments = form.data["comments"]
            artist_request.handled_by = request.user
            artist_request.save()
            if status == "approved":
                artist_request.status = 1
                artist_request.save()
                dj_artist.status = 1
                dj_artist.save()
                subject = "Your request to add %s is Approved"%(dj_artist.artistName)

            msg = SendEmail()
            msg.send_by_template([artist_request.requested_by.email, ], "artist/artist_added_email.html",
                                    context={
                                        "artist_obj": dj_artist,
                                        "site_url": settings.SITE_URL,
                                        "artist_status": artist_request,
                                    },
                                    subject=subject
                                    )
            messages.add_message(request, messages.SUCCESS, "%s %s Successfully."%(dj_artist.artistName, status))
            return redirect(reverse('dashboard'))
        else:
            messages.add_message(request, messages.WARNING, "Error in %s %s"%(status,dj_artist.artistName))
            return redirect(reverse('dashboard'))

artist_step_6_preview = ArtistStep6Preview.as_view()

@method_decorator(check_plan('Agent'), name='dispatch')
class ArtistSubmitDetails(LoginRequiredMixin, View):
    
    def get(self, request, artist_id):
        try:
            dj_artist = Artist.allartistobjects.get(artistID=artist_id)
        except Artist.DoesNotExist:
            raise Http404()
        data ={}
        artist_requests = AddArtistRequest.objects.filter(artist=dj_artist).first()
        artist_request = None
        if artist_requests:
            artist_request = artist_requests
        else:
            artist_request = AddArtistRequest()
            artist_request.artist = dj_artist
            artist_request.requested_by = request.user
            artist_request.save()
        
        artist_request.handled_by = None
        artist_request.email_status = 0
        artist_request.status = 0
        artist_request.save()
        
        if request.user.is_superuser:
            dj_artist.status = 1
            dj_artist.save()
            artist_request.status = 1
            artist_request.handled_by = request.user
            artist_request.comments = "Artist Request Done By %s"%(request.user)
            artist_request.save()
            subject = "Artist Successfully Added"
            msg = SendEmail()
            msg.send_by_template([artist_request.requested_by.email, ], "artist/artist_added_email.html",
                                    context={
                                        "artist_obj": dj_artist,
                                        "site_url": settings.SITE_URL,
                                        "artist_status": artist_request,
                                        "user": request.user,
                                    },
                                    subject=subject
                                    )
            data.update({"artist_obj":dj_artist})
        else:
            admins = User.objects.filter(is_superuser=True)
            admin_emails = [admin.email for admin in admins]
            msg = SendEmail()
            
            msg.send_by_template(admin_emails, "artist/add_djartist_approval_email.html",
                                    context={
                                        "user":request.user,
                                        "artist_obj": dj_artist,
                                        "site_url": settings.SITE_URL,
                                    },
                                    subject="Requested Dj Artist"
                                    )
            artist_request.email_status = 1
            artist_request.save()
        dj_artist.is_artist_info_completed = True
        dj_artist.save()
        return render(request, "artist/add_djartist_success.html",data)

artist_submit_details = ArtistSubmitDetails.as_view()


@method_decorator(check_plan('Promoter', 'Agent'), name='dispatch')
class ManagedArtists(LoginRequiredMixin, View):
    
    def get(self, request):
        all_events = WpMyEvents.objects.filter(user_id=request.user.id)
        profile = Profile.objects.get(user=request.user)

        email_frequency_value = profile.email_frequency

        profile_managed_djs = profile.profile_managed_djs.all().order_by('artistName')

        profile_managed_djs_ids = profile_managed_djs.values_list('artistID', flat=True)

        artists = Artist.objects.all().values_list('artistID', 'artistName')

        all_artist = []
        for id in profile_managed_djs_ids:
            artist_dict = {}
            try:
                artist = Artist.objects.get(artistID=int(id))
            except:
                continue
            artist_dict['upcoming_events'] = get_upcoming_events(artist)
            # artist_dict['statistics'] = get_artist_statistics(artist)
            artist_dict['social_urls'] = get_artist_social_url(artist)
            artist_dict['similar_artists'] = get_similar_artist(artist, limit=12)
            artist_dict['artist_requested_event_list'] = get_myevents_artist_invite(artist, request)
            artist_dict['artist_obj'] = artist
            all_artist.append(artist_dict)
        
        context = {
            "all_artist": all_artist,
            "profile_managed_djs": profile_managed_djs,
            "profile": profile,
            "all_events": all_events,
            "artists": artists,
            "profile_managed_djs_ids": profile_managed_djs_ids,
            "email_frequency_value": email_frequency_value
        }

        return render(request, 'profiles/managed_artists.html', context)

    def post(self, request):
        profile = Profile.objects.get(user=request.user)
        
        general_artists = GeneralArtist.objects.filter(user = request.user)
        for general_artist in general_artists:
            general_artist.status = 0
            general_artist.save()

        for artist in profile.profile_managed_djs.all():
            profile.profile_managed_djs.remove(artist)
        profile.save()

        profile_managed_djs_ids = request.POST.getlist('managed_artists', '')
        if profile_managed_djs_ids:
            profile_managed_djs_ids = [int(id) for id in profile_managed_djs_ids]
            profile = Profile.objects.get(user=request.user)
            for id in profile_managed_djs_ids:
                profile.profile_managed_djs.add(Artist.objects.get(artistID=id))
                gen_art, created = GeneralArtist.objects.get_or_create(artistID = Artist.objects.get(artistID=id), user = request.user)
                gen_art.status = 1
                gen_art.save()
            profile.save()
        return redirect(reverse("profile_managed_artists"))

profile_managed_artists = ManagedArtists.as_view()


@method_decorator(check_plan('Artist/DJ'), name='dispatch')
class ManagedArtist(LoginRequiredMixin, View):
    
    def get(self, request):
        all_events = WpMyEvents.objects.filter(user_id=request.user.id)
        profile = Profile.objects.get(user=request.user)
        
        if profile.dj_artist:
            messages.add_message(request, messages.INFO, "You have already assigned yourself as an artist")
            return redirect(reverse("dashboard"))

        email_frequency_value = profile.email_frequency

        profile_managed_djs = profile.profile_managed_djs.all().order_by('artistName')

        profile_managed_djs_ids = profile_managed_djs.values_list('artistID', flat=True)

        artists = Artist.objects.all().values_list('artistID', 'artistName')

        all_artist = []
        for id in profile_managed_djs_ids:
            artist_dict = {}
            try:
                artist = Artist.objects.get(artistID=int(id))
            except:
                continue
            artist_dict['upcoming_events'] = get_upcoming_events(artist)
            # artist_dict['statistics'] = get_artist_statistics(artist)
            artist_dict['social_urls'] = get_artist_social_url(artist)
            artist_dict['similar_artists'] = get_similar_artist(artist, limit=12)
            artist_dict['artist_requested_event_list'] = get_myevents_artist_invite(artist, request)
            artist_dict['artist_obj'] = artist
            all_artist.append(artist_dict)
        
        context = {
            "all_artist": all_artist,
            "profile_managed_djs": profile_managed_djs,
            "profile": profile,
            "all_events": all_events,
            "artists": artists,
            "profile_managed_djs_ids": profile_managed_djs_ids,
            "email_frequency_value": email_frequency_value
        }

        return render(request, 'profiles/managed_artist.html', context)

    def post(self, request):
        profile = Profile.objects.get(user=request.user)
        
        general_artists = GeneralArtist.objects.filter(user = request.user)
        for general_artist in general_artists:
            general_artist.status = 0
            general_artist.save()

        for artist in profile.profile_managed_djs.all():
            profile.profile_managed_djs.remove(artist)
        profile.save()

        profile_managed_djs_ids = request.POST.getlist('managed_artist', '')
        if profile_managed_djs_ids:
            profile_managed_djs_ids = [int(id) for id in profile_managed_djs_ids]
            profile = Profile.objects.get(user=request.user)
            for id in profile_managed_djs_ids:
                profile.profile_managed_djs.add(Artist.objects.get(artistID=id))
                profile.dj_artist = Artist.objects.get(artistID=id)
                gen_art, created = GeneralArtist.objects.get_or_create(artistID = Artist.objects.get(artistID=id), user = request.user)
                gen_art.status = 1
                gen_art.save()
            profile.save()
        return redirect(reverse("dashboard"))

profile_managed_artist = ManagedArtist.as_view()


@method_decorator(check_plan('Agent'), name='dispatch')
class SaveArtistInfoView(LoginRequiredMixin, View):

    def post(self, request, artist_id):
        try:
            artist = Artist.allartistobjects.get(artistID=artist_id)
            general_artist = GeneralArtist.objects.get(artistID=artist,user=request.user, status=1)
        except Artist.DoesNotExist:
            raise Http404()
        except GeneralArtist.DoesNotExist:
            raise Http404()
        
        artist_info, created = ArtistInfo.objects.get_or_create(artistID=artist)
        artist_info.email = request.POST.get('email')
        artist_info.phone = request.POST.get('phone')
        artist_info.city = request.POST.get('city')
        artist_info.country = request.POST.get('country')
        redirect_url_events = request.POST.get('event_artist_info',None)
        artist_info.save()

        travel_doc_url = request.POST.get("travel_doc_link")
        if travel_doc_url:
            artist_media,created = ArtistMedia.objects.get_or_create(artistID=general_artist.artistID,media_type="TravelDoc")
            artist_media.media_title = "TravelDoc"
            artist_media.media_url = travel_doc_url
            artist_media.save()
        
        for doc in request.FILES.getlist('passport'):
            ArtistTravelDoc.objects.create(general_artist=general_artist,travel_doc=doc)
        
        return redirect(reverse('artist_detail', kwargs={'artist_id':artist.artistID}))

save_artist_info = SaveArtistInfoView.as_view()


@method_decorator(check_plan('Agent'), name='dispatch')
class DeleteArtistFile(LoginRequiredMixin, View):

    def get(self, request, artist_id):
        try:
            artist = Artist.allartistobjects.get(artistID=artist_id)
            general_artist = GeneralArtist.objects.get(artistID=artist,user=request.user)
        except Artist.DoesNotExist:
            raise Http404()
        except GeneralArtist.DoesNotExist:
            raise Http404()
        info_type = request.GET.get("info_type")
        document_id = request.GET.get("document_id")
        artist_detail_page = request.GET.get("artist_detail_page")
        
        if info_type == "press_kit":
            try:
                artist_creative_info = ArtistCreativeInfo.objects.filter(general_artist=general_artist).first()
                artist_creative_info.press_kit_url = None
                artist_creative_info.save()
            except:
                pass
            try:
                artist_presskit = ArtistPresskit.objects.get(id=document_id)
                artist_presskit.delete()
            except:
                pass
            messages.success(request,"Artist Promo Doc Deleted Successfully")
        
        elif info_type == "travel_doc":
            artist_travel_doc = ArtistTravelDoc.objects.get(id=document_id)
            artist_travel_doc.delete()
            messages.success(request,"Artist Travel Doc Deleted Successfully")

        elif info_type == "travel_doc_url" or info_type == "rider_doc_url":
            artist_travel_doc_media = ArtistMedia.objects.get(id=document_id)
            artist_travel_doc_media.delete()
            messages.success(request,"Artist Travel Doc Deleted Successfully")
        
        else:
            artist_tech_rider_doc = ArtistTechRider.objects.get(id=document_id)
            artist_tech_rider_doc.delete()
            messages.success(request,"Artist Tech Rider Doc Deleted Successfully")
        
        if artist_detail_page:
            return redirect(reverse('artist_detail',kwargs={'artist_id':artist.artistID}))
        else:
            return HttpResponse()

delete_artist_file = DeleteArtistFile.as_view()


@method_decorator(check_plan('Artist/DJ', 'Promoter', 'Agent'), name='dispatch')
class SaveCreativeArtistInfo(LoginRequiredMixin, View):

    def post(self, request, artist_id):
        try:
            artist = Artist.allartistobjects.get(artistID=artist_id)
            general_artist = GeneralArtist.objects.get(artistID=artist,user=request.user)
        except Artist.DoesNotExist:
            raise Http404()
        except GeneralArtist.DoesNotExist:
            raise Http404()
        
        artist_creative_info = ArtistCreativeInfo.objects.filter(general_artist=general_artist).first()
        artist_info, created = ArtistInfo.objects.get_or_create(artistID=general_artist.artistID)
        if not artist_creative_info:
            artist_creative_info = ArtistCreativeInfo.objects.create(general_artist=general_artist)

        artist_creative_info.labels =  request.POST.get("labels")
        artist_creative_info.affiliations = request.POST.get('affiliations')
        artist_creative_info.discography_url = request.POST.get('discography_url')
        press_kit_url = request.POST.get('press_kit_url', None)
        press_kit_titlename = request.POST.get('press_kit_titlename', None)

        # Adding presskit detail if exist
        if press_kit_url:
            artist_creative_info.press_kit_url = press_kit_url
        if press_kit_titlename:
            artist_creative_info.press_kit_titlename = press_kit_titlename
            messages.success(request, "Saved Artist Press Kit URL")

        # Saving Biography if exist
        biography = request.POST.get('biography', None)
        if biography:
            artist.biography = biography
            artist.save()
            messages.success(request, "Saved Artist Biography")

        artist_creative_info.save()

        artist_rider_doc_url = request.POST.get("artist_rider_doc_url")
        filenames = None
        if request.POST.get("change_file_name_array_press_kit"):
            filenames = request.POST.get("change_file_name_array_press_kit")
        else:
            filenames = request.POST.get("change_file_name_array_tech_rider")
        file_name_list = []

        if filenames:
            file_name_list =  filenames.split(",")
            for index,doc in enumerate(request.FILES.getlist('press_kit')):
                if doc._size < 5242880:
                    ArtistPresskit.objects.create(general_artist=general_artist,press_kit=doc,press_kit_filename=file_name_list[index]) 
            for index,doc in enumerate(request.FILES.getlist('tech_rider')):
                if doc._size < 5242880:
                    ArtistTechRider.objects.create(general_artist=general_artist,tech_rider=doc,tech_rider_filename=file_name_list[index]) 
        else:
            for doc in request.FILES.getlist('press_kit'):
                if doc._size < 5242880:
                    ArtistPresskit.objects.create(general_artist=general_artist,press_kit=doc) 
            for doc in request.FILES.getlist('tech_rider'):
                if doc._size < 5242880:
                    ArtistTechRider.objects.create(general_artist=general_artist,tech_rider=doc) 

        if artist_rider_doc_url:
            artist_media,created = ArtistMedia.objects.get_or_create(artistID=general_artist.artistID,media_type="RiderDoc")
            artist_media.media_title = "RiderDoc"
            artist_media.media_url = artist_rider_doc_url
            artist_media.save()
            messages.success(request, "Saved Artist Rider Doc")
        
        return redirect(reverse('artist_detail',kwargs={'artist_id':artist.artistID}))

save_creative_artist_info = SaveCreativeArtistInfo.as_view()


@method_decorator(check_plan('Artist/DJ', 'Promoter', 'Agent'), name='dispatch')
class LoadShortlistVenues(LoginRequiredMixin, View):

    def get(self, request, artist_id):
        artist = Artist.objects.get(artistID=artist_id)
        start = int(request.GET.get('start'))
        
        # Shortlist venues:
        global limit
        all_venues = []
        if artist.shortlistedvenue_set.filter(user=request.user).first():
            all_venues = artist.shortlistedvenue_set.filter(user=request.user).first().venueids.all()

        venue_count = int(start) + limit 
        status = True
        initial = False
        
        if start == 0:
            initial = True
            context = { 
                    "start": limit,
                    "all_venues": all_venues[:limit]
                }
            template = render_to_string("artist/get_shortlist_venues.html", context, request= request)
        
        elif len(all_venues) - start > 0 :
                context = { 
                    "all_venues": all_venues[start:venue_count]
                }
                template = render_to_string("artist/get_shortlist_venues_table.html", context, request= request)
        else:
            status = False # No More venues
            template = ""
        
        response = {
            "initial": initial,
            "status": status,
            "start": venue_count,
            "data": template,
            "venues_count":len(all_venues),
        }

        return HttpResponse(json.dumps(response), content_type="applicaton/json")
    
load_shortlist_venues = LoadShortlistVenues.as_view()


@method_decorator(check_plan('Artist/DJ', 'Promoter', 'Agent'), name='dispatch')
class LoadShortlistEvents(LoginRequiredMixin, View):

    def get(self, request, artist_id):
        artist = Artist.objects.get(artistID=artist_id)
        start = int(request.GET.get('start'))
        
        # Shortlist events:
        global limit
        all_events = []
        if artist.shortlistedevent_set.filter(user=request.user).first():
            all_events = artist.shortlistedevent_set.filter(user=request.user).first().eventids.all()

        event_count = int(start) + limit 
        status = True
        initial = False
        
        if start == 0:
            initial = True
            context = { 
                    "start": limit,
                    "all_events": all_events[:limit]
                }
            template = render_to_string("artist/get_shortlist_events.html", context, request= request)
        
        elif len(all_events) - start > 0 :
                context = { 
                    "all_events": all_events[start:event_count]
                }
                template = render_to_string("artist/get_shortlist_events_table.html", context, request= request)
        else:
            status = False # No More events
            template = ""
        
        response = {
            "initial": initial,
            "status": status,
            "start": event_count,
            "data": template,
            "events_count":len(all_events),
        }

        return HttpResponse(json.dumps(response), content_type="applicaton/json")
    
load_shortlist_events = LoadShortlistEvents.as_view()


class NewArtistToComplete(LoginRequiredMixin, View):

    def get(self, request):
        profile = Profile.objects.get(user=request.user)
        artists = Artist.allartistobjects.filter(
                user_id=profile.user.id, is_artist_info_completed=False)

        if request.GET.get("remove_all"):
            for general_artist in artists:
                general_artist.delete()

            return HttpResponseRedirect(reverse("new_artist_to_complete"))

        context = {
            "new_artists": artists
        }
        return render(request, "artist/new_artist_to_complete.html", context)


new_artist_to_complete = NewArtistToComplete.as_view()

def get_artist_events_json(request):
    calendar_date = request.GET.get('calendar_date')
    calendar_list = calendar_date.split("-")
    calendar_year = int(calendar_list[0])
    calendar_month = int(calendar_list[-1])
    artist = Artist.objects.get(artistID=request.GET.get("artist_id"))
    artist_system_events = get_artist_calendar_events(
        request, request.GET.get("artist_id"), calendar_year, calendar_month)
    artist_online_past_events = get_past_events(
        artist=artist, calendar_year=calendar_year, calendar_month=calendar_month)
    events_dict = {}
    # for event in artist_system_events:
    #     event_date = event.event_date.strftime("%Y-%m-%d")
    #     event_type = None
    #     value = {
    #         "event_name": event.event_name,
    #         "event_id": event.event_id,
    #         "event_type": "system_event"
    #     }
    #     if event_date in events_dict:
    #         events_dict[event_date].append(value)
    #     else:
    #         events_dict[event_date] = [value]

    for dj_master_obj in artist_online_past_events:
        event_date = dj_master_obj.eventid.date.strftime("%Y-%m-%d")
        event_type = None

        value = {
            "event_id": dj_master_obj.eventid.eventID,
            "number": 1,
            "event_name": dj_master_obj.eventid.eventName,
            "venue_name": dj_master_obj.eventid.evenueName,
            "venue_city": dj_master_obj.venueid.city,
            "venue_country": dj_master_obj.venueid.country,
            "event_type": "online_event"
        }
        if event_date in events_dict:
            events_dict[event_date].append(value)
        else:
            events_dict[event_date] = [value]
        
    return HttpResponse(json.dumps(events_dict), content_type="application/json")

@login_required
@check_email_validated
def artists_notes(request):
    artists_notes_data = UserNote.objects.filter(user=request.user).order_by('-modified_date')
    artists = UserNote.objects.order_by('artist__artistName').filter(user=request.user).values_list('artist','artist__artistName').distinct()


    
    date = request.GET.get('date', '')
    artist = request.GET.get('artist', '')

    context = {}

    if artist != '':
        artists_notes_data = artists_notes_data.filter(artist__artistID=artist)
        context.update({
            "filter_artist": int(artist),
        })

    context.update({
        "option_artists": artists,
        "artists": artists_notes_data,
        "date": date,
    })

    return render(request, "artist/artists_notes.html", context)

@login_required
def save_artist_photos(request, artist_id):
    if request.user.is_authenticated():
        artist = Artist.objects.get(artistID=artist_id)
        artist_photo = ArtistPhoto.objects.create(artistID=artist)
        artist_photo.image_url = request.POST.get('image_url')
        artist_photo.save()
        messages.success(request, "Artist photo URL Added")
        return HttpResponseRedirect("/artists/artist/" + artist_id)

@login_required
def save_artist_media(request, artist_id):
    if request.user.is_authenticated():
        artist = Artist.objects.get(artistID=artist_id)
        if request.POST.get('media_type'):
            artist_media = ArtistMedia.objects.create(artistID=artist)
            artist_media.media_type = request.POST.get('media_type')
            artist_media.media_title = request.POST.get('media_title')
            artist_media.media_url = request.POST.get('media_link')
            artist_media.save()
            msg = "Artist {} URL Added".format(artist_media.media_type)
            messages.success(request, msg)
        return HttpResponseRedirect("/artists/artist/" + artist_id)

def delete_artist_file(request, artist_id):
    general_artist = GeneralArtist.objects.get(artistID=artist_id, user=request.user)
    info_type = request.GET.get("info_type")
    document_id = request.GET.get("document_id")
    artist_detail_page = request.GET.get("artist_detail_page")
    if info_type == "press_kit":
        try:
            artist_creative_info = ArtistCreativeInfo.objects.filter(
                general_artist=general_artist).first()
            artist_creative_info.press_kit_url = None
            artist_creative_info.save()
        except:
            pass
        try:
            artist_presskit = ArtistPresskit.objects.get(id=document_id)
            artist_presskit.delete()
        except:
            pass
        messages.success(request, "Artist Promo Doc Deleted Successfully")

    elif info_type == "travel_doc":
        artist_travel_doc = ArtistTravelDoc.objects.get(id=document_id)
        artist_travel_doc.delete()
        messages.success(request, "Artist Travel Doc Deleted Successfully")
    elif info_type == "travel_doc_url" or info_type == "rider_doc_url":
        artist_travel_doc_media = ArtistMedia.objects.get(id=document_id)
        artist_travel_doc_media.delete()
        messages.success(request, "Artist Travel Doc Deleted Successfully")
    else:
        artist_tech_rider_doc = ArtistTechRider.objects.get(id=document_id)
        artist_tech_rider_doc.delete()
        messages.success(request, "Artist Tech Rider Doc Deleted Successfully")
    if artist_detail_page:
        return HttpResponseRedirect("/artists/artist/" + artist_id)
    else:
        return HttpResponse()

def email_travel_docs(request, artist_id):
    artist = Artist.allartistobjects.filter(artistID=artist_id).first()
    if artist:
        if request.method == "POST":
            recipient = request.POST.get("to_email", None)
            subject   = request.POST.get("subject", None)
            body      = request.POST.get("email_body", None)

            email = EmailMessage(subject, body, settings.DEFAULT_FROM_EMAIL_NAME, [recipient])

            # Getting documents
            general_artist = GeneralArtist.objects.filter(artistID=artist,user=request.user).first()

            if general_artist:
                art_travelinfo = ArtistTravelDoc.objects.filter(general_artist=general_artist)

                for docs in art_travelinfo:
                    email.attach_file(docs.travel_doc.path)

            email.send()

            messages.success(request, "Mail Sent Successfully")

    return HttpResponseRedirect("/artists/artist/" + artist_id)

@login_required
def artist_media_delete(request, media_id, artist_id):
    if request.user.is_authenticated():
        media = ArtistMedia.objects.get(id=media_id)
        media_type = media.media_type
        delete_artwork_ajax = request.GET.get("delete_artwork_ajax")
        media.delete()

        msg = "{} link deleted successfully".format(media_type)
        messages.success(request, msg)

        return HttpResponseRedirect("/artists/artist/" + artist_id)


@login_required
def artist_photo_delete(request, photo_id, artist_id):
    if request.user.is_authenticated():
        photo = ArtistPhoto.objects.get(id=photo_id)
        delete_artwork = request.GET.get("delete_artwork_ajax")
        photo.delete()
        messages.success(request, "Artist Photo Deleted Successfully")
        return HttpResponseRedirect("/artists/artist/" + artist_id)

def search_all_artists(request):
    response = []
    term = request.GET.get("term")

    if term:
        artists = Artist.allartistobjects.filter(artistName__istartswith=term)[:30]
    else:
        artists = Artist.allartistobjects.all()[:30]

    for artist in artists:
        response.append({'id': artist.artistID, 'text': artist.artistName})
    
    return JsonResponse(response, safe=False)

@login_required
def create_artist_offersheet(request):

    if request.method == 'POST':
        context = {}

        artistID = request.POST.get('artistID')
        artist = Artist.allartistobjects.filter(artistID=artistID).first()

        offersheet = Offersheet.objects.filter(artist=artist).first()

        if offersheet:
            messages.success(request, "Offersheet is already created for this artist")
            return HttpResponseRedirect("/artists/artist/" + artistID)

        if artist:
            context.update({'artist': artist})

        return render(request, 'artist/offersheet/create_offersheet.html', context)
    else:
        return redirect('/')

@login_required
def offersheet_final_step(request, artist_id):

    if request.method == 'POST':
        context = {}

        artist = Artist.allartistobjects.filter(artistID=artist_id).first()
        pro_obj = Profile.objects.get(user=request.user)
        subcategories = OffersheetSubCategories.objects.all()

        if artist:



            # Saving offersheet
            offersheet, created = Offersheet.objects.get_or_create(artist=artist)

            offersheet.agent = request.user

            offersheet.include_rider    = request.POST.get('rider', "") == "on"
            offersheet.include_presskit = request.POST.get('press_pack', "") == "on"
            
            offersheet.save()
            live_event = subcategories.filter(category='live_event')
            stream_check = subcategories.filter(category='streams')
            virtual_event_check = subcategories.filter(category='virtual_event')
            music_tution_check = subcategories.filter(category='private_music')
            interview_check = subcategories.filter(category='interview')

            context.update({'artist': artist,
                            'live_events': live_event,
                            'streams' : stream_check,
                            'virtual_events': virtual_event_check,
                            'private_musics': music_tution_check,
                            'interviews' : interview_check
                            })
            return render(request, 'artist/offersheet/offersheet_final_step.html', context)
    else:
        return redirect('/')

@login_required
def offersheet_created(request, artist_id):
    if request.method == "POST":
        context = {}

        artist = Artist.allartistobjects.filter(artistID=artist_id).first()
        subcategories = OffersheetSubCategories.objects.all()
        sc = []
        if artist:
            # Saving offersheet
            offersheet, created = Offersheet.objects.get_or_create(artist=artist)


            for subc in list(subcategories):
                 if request.POST.get(str(subc.id), "") == "on":
                     if not (subc in list(offersheet.subcategories.all())):
                         offersheet.subcategories.add(subc)

            offersheet.live_events      = request.POST.get('live_events', "") == "on"
            offersheet.stream           = request.POST.get('stream', "") == "on"
            offersheet.virtual_events   = request.POST.get('virtual_events', "") == "on"
            offersheet.private_music    = request.POST.get('private_music', "") == "on"
            offersheet.interview        = request.POST.get('interview', "") == "on"

            offersheet.save()
            context.update({'artist': artist, 'subcategories': sc})

            messages.success(request, "Offersheet Successfully Created")
            return HttpResponseRedirect("/artists/artist/" + artist_id)
    else:
        return redirect('/')


@login_required
def edit_offersheet(request, artist_id):
    context = {}

    artist = Artist.allartistobjects.filter(artistID=artist_id).first()
    subcategories = OffersheetSubCategories.objects.all()

    if artist:
        offersheet = Offersheet.objects.filter(artist=artist).first()
        # return HttpResponse(escape(repr(request.POST)))
        if not request.user == offersheet.agent:
            return redirect('/')
        if offersheet:
            if request.method == 'POST':
                offersheet.include_rider    = request.POST.get('riders', "") == "on"
                offersheet.stream           = request.POST.get('stream', "") == "on"
                offersheet.include_presskit = request.POST.get('press_pack', "") == "on"
                offersheet.live_events      = request.POST.get('live_events', "") == "on"
                offersheet.virtual_events   = request.POST.get('virtual_events', "") == "on"
                offersheet.private_music    = request.POST.get('private_music', "") == "on"
                offersheet.interview        = request.POST.get('interview', "") == "on"

                for subc in list(subcategories):
                    if request.POST.get(str(subc.id), "") == "on":
                        if not (subc in list(offersheet.subcategories.all())):
                            offersheet.subcategories.add(subc)
                    else:
                        offersheet.subcategories.remove(subc)

                offersheet.save()

                messages.success(request, "Offersheet Updated Successfully")
                return HttpResponseRedirect("/artists/artist/" + artist_id)



            live_event = subcategories.filter(category='live_event')
            stream_check = subcategories.filter(category='streams')
            virtual_event_check = subcategories.filter(category='virtual_event')
            music_tution_check = subcategories.filter(category='private_music')
            interview_check = subcategories.filter(category='interview')

            context.update({'artist': artist,
                            'live_events': live_event,
                            'streams' : stream_check,
                            'virtual_events': virtual_event_check,
                            'private_musics': music_tution_check,
                            'interviews' : interview_check,
                            'offersheet': offersheet})
            return render(request, 'artist/offersheet/edit_offersheet.html', context)
        else:
            messages.success(request, "Offersheet Not Found")
            return HttpResponseRedirect("/artists/artist/" + artist_id)
    else:
        return redirect('/')

def create_offer(request, artist_id):
    context = {}
    artist = Artist.allartistobjects.filter(artistID=artist_id).first()

    offer_id = None
    try:
        offer_id = request.session['offer-id']
    except:
        pass
    
    offer = None
    if offer_id:
        offer = Offer.objects.filter(id=offer_id).first()

    if request.method == 'POST':
        if offer_id:
            form = OfferStageOneForm(instance=offer, data=request.POST)
            offer_obj = form.save(commit=False)
        else:
            form = OfferStageOneForm(request.POST)
            offer_obj = form.save(commit=False)
        
        offer_obj.artist = artist
        offer_obj.save()
        
        request.session['offer-id'] = offer_obj.id
        
        return redirect(reverse('offer_create_details', 
                                kwargs={'artist_id': artist.artistID}))

    if request.method == 'GET':
        if artist:
            offersheet = Offersheet.objects.filter(artist=artist).first()
            if offersheet:
                if offer:
                    form = OfferStageOneForm(instance=offer)
                    context.update({'selected_request_type': offer.request_type.id})
                else:
                    form = OfferStageOneForm()

                # Making Option List
                request_types = []
                all_request_types = OffersheetSubCategories.objects.all()

                if offersheet.live_events:
                    request_types.extend(all_request_types.filter(category='live_event'))
                if offersheet.stream:
                    request_types.extend(all_request_types.filter(category='streams'))
                if offersheet.virtual_events:
                    request_types.extend(all_request_types.filter(category='virtual_event'))
                if offersheet.private_music:
                    request_types.extend(all_request_types.filter(category='private_music'))
                if offersheet.interview:
                    request_types.extend(all_request_types.filter(category='interview'))

                context.update({
                    'artist': artist,
                    'form': form,
                    'request_types': list(offersheet.subcategories.all()),
                    'offersheet': offersheet
                })
                return render(request, 'artist/offersheet/create_offer.html', context)

    return redirect('/')


def create_offer_details(request, artist_id):
    context = {}

    try:
        offer_id = request.session['offer-id']
    except:
        return redirect('/')
    
    artist = Artist.allartistobjects.filter(artistID=artist_id).first()
    offer = Offer.objects.filter(id=offer_id).first()

    if offer:
        fields = json.loads(offer.request_type.fields)

    if request.method == "POST":
        form = OfferStageTwoForm(instance=offer, data=request.POST)
        form_obj = form.save(commit=False)
        timezone    = request.POST.get('timezone')
        start_time  = request.POST.get('start_time')
        finish_time = request.POST.get('finish_time')
        paid        = request.POST.get('paid')

        if timezone and start_time and finish_time and paid:
            form_obj.timezone     = timezone
            form_obj.start_time   = start_time
            form_obj.finish_time  = finish_time
            form_obj.event_detail = request.POST.get('event_summary')
            if paid == "yes":
                form_obj.paid = True
            elif paid == "no":
                form_obj.paid = False
            
            field_data = []
            for field in fields:
                data = {
                    'label': field["label"],
                    'value': request.POST.get(field["label"]),
                    'type': 'text'
                }
                field_data.append(data)
            form_obj.extra_data = json.dumps(field_data)
            form_obj.save()

            return redirect(reverse('offer_create_final', 
                                kwargs={'artist_id': artist.artistID}))

    if request.method == 'GET':
        if offer:
            if artist:
                offersheet = Offersheet.objects.filter(artist=artist).first()
                if offersheet:
                    timezones = pytz.common_timezones

                    # Getting Extra data
                    extra_field_data = []
                    try:
                        extra_field_data = json.loads(offer.extra_data)
                    except:
                        pass

                    form = OfferStageTwoForm(instance=offer)
                    context.update({
                        'artist': artist,
                        'timezones': timezones,
                        'offer': offer,
                        'fields': fields,
                        'extra_field_data': extra_field_data,
                        'form': form
                    })

                    
                    return render(request, 'artist/offersheet/create_offer_details.html', context)

    return redirect('/')

def create_offer_final(request, artist_id):
    context = {}

    try:
        offer_id = request.session['offer-id']
    except:
        return redirect('/')
    
    artist = Artist.allartistobjects.filter(artistID=artist_id).first()
    offer = Offer.objects.filter(id=offer_id).first()

    if request.method == "POST":
        form = OfferStageThreeForm(instance=offer, data=request.POST)
        form_obj = form.save(commit=False)
        form_obj.gdpr_compliant = request.POST.get('gdpr_compliant', "") == "on"
        form_obj.save()

        return redirect(reverse('offer_preview', 
                                kwargs={'artist_id': artist.artistID}))

    if request.method == 'GET':
        if offer:
            if artist:
                offersheet = Offersheet.objects.filter(artist=artist).first()
                if offersheet:
                    form = OfferStageThreeForm(instance=offer)

                    # Getting extra fields
                    extra_field_data = []
                    try:
                        extra_field_data = json.loads(offer.extra_data)
                    except:
                        pass

                    context.update({
                        'offer': offer,
                        'artist': artist,
                        'form': form,
                        'extra_field_data': extra_field_data
                    })

                    return render(request, 'artist/offersheet/create_offer_final.html', context)

def offer_preview(request, artist_id):
    context = {}

    try:
        offer_id = request.session['offer-id']
    except:
        return redirect('/')

    artist = Artist.allartistobjects.filter(artistID=artist_id).first()
    offersheet = Offersheet.objects.filter(artist=artist).first()
    offer = Offer.objects.filter(id=offer_id).first()
    # email confirmation
    ec = EmailConfirmation.objects.get_or_create(email=offer.email)[0]
    ec.token = get_random_string(length=20)
    ec.save()
    offer.email_validation = ec
    offer.save()


    if request.method == 'POST':

        msg = SendEmail()
        # send validation email
        email_confirmation = ec
        validate_email = MarketingEmails.objects.filter(email_type="email_offer_validation").first()

        site_url = settings.SITE_URL
        url = site_url + "/artists/offersheet/validate-email/" + email_confirmation.email + "/" + email_confirmation.token + \
              "/" + str(offer.id)
        content = validate_email.email_content
        content = content.replace("{username}", offer.name)
        content = content.replace("{artist}", offer.artist.artistName)
        a_url = ("<html><a href=" + url + ">click here</a></html>")
        content = content.replace("{url}", a_url)

        try:
            msg.send_by_template([offer.email], "artist/offersheet/artist_offer_submitted_mail.html",
                                   context={
                                       "offer": offer,
                                       "url": url
                                   },
                                   subject=validate_email.email_subject
                                   )
            messages.add_message(request, messages.SUCCESS,
                                 "New Confirmation Email has been sent to your Registered Email")
        except Exception as e:
            print(e)
            messages.add_message(request, messages.SUCCESS, "Please Try Again")
        context.update({
            'artist': artist,
            'offer': offer,
        })
        return render(request,'artist/offersheet/create_offer_valid_request.html', context)

    if request.method == 'GET':
        if artist:
            if offer:
                extra_field_data = []
                try:
                    extra_field_data = json.loads(offer.extra_data)
                except:
                    pass
                context.update({
                    'artist': artist,
                    'offer': offer,
                    'extra_field_data': extra_field_data
                })
                return render(request, 'artist/offersheet/offer_preview.html', context)

    return redirect('/')

def offer_success(request):
    try:
        offer_success = request.session['offer-success']
    except:
        return redirect('/')

    return render(request, 'artist/offersheet/offer_success.html')




# validate email
def generate_confirmation_email(request):
    request_url = request.META.get('HTTP_REFERER')
    email = SendEmail(request)
    user = request.user
    profile = Profile.objects.filter(user=user).first()
    site_url = settings.SITE_URL

    if not profile.is_email_validated :
        email_confirmation = EmailConfirmation.objects.create(email=user.email, token=get_random_string(length=10), is_latest=True)
        validate_email = MarketingEmails.objects.filter(email_type="validate_email").first()

        if validate_email:
            url = site_url+"/accounts/validate-email/"+email_confirmation.email+"/"+email_confirmation.token
            content = validate_email.email_content
            content = content.replace("{username}", user.first_name)
            # content = content.replace("{user_type}", profile.user_types)
            # content = content.replace('src="', 'src="'+site_url+'')
            # content = content.replce("{token}", email_confirmation.token)
            # content = content.replace("{url}", url)
            a_url = ("<html><a href="+ url +">click here</a></html>")
            content = content.replace("{url}", a_url)

            try:
                email.send_by_template([user.email],"marketing_pages/not_loged_5.html",
                    context={
                        "content": content,
                        "site_url":site_url
                    },
                    subject = validate_email.email_subject
                )
                messages.add_message(request, messages.SUCCESS, "New Confirmation Email has been sent to your Registered Email")
            except:
                messages.add_message(request, messages.SUCCESS, "Please Try Again")

        email_confirmation_list = EmailConfirmation.objects.filter(email=user.email).exclude(token=email_confirmation.token)
        for email_confirm_obj in email_confirmation_list:
            email_confirm_obj.is_latest = False
            email_confirm_obj.save()

        # return redirect(request_url)
        return redirect("/")

    else:
        messages.add_message(request, messages.SUCCESS, "This Email is already Validated")
        return redirect('/')

def validate_email(request, email, token,offer_id):
    email_confirmation = EmailConfirmation.objects.filter(email=email, token=token).first()
    if email_confirmation:
        try:
            offer = Offer.objects.filter(email_validation=email_confirmation)[0]
            offer.is_validated=True
            offer.save()
            msg = SendEmail()
            offersheet = Offersheet.objects.filter(artist=offer.artist).first()
            # Sending mail to agent
            msg.send_by_template([offersheet.agent.email], "artist/offersheet/agent_offer_mail.html",
                                 context={
                                     'agent': offersheet.agent,
                                     'site_url': settings.SITE_URL
                                 },
                                 subject='You have one ' + offer.artist.artistName + ' request pending in your inbox.'
                                 )

            # Sending mail to anyon user
            msg.send_by_template([offer.email], "artist/offersheet/artist_offer_mail.html",
                                 context={
                                     'offer': offer,
                                     'site_url': settings.SITE_URL
                                 },
                                 subject='Your booking request has been submitted.'
                                 )

            messages.add_message(request, messages.SUCCESS, "Your Email has been Validated.")
        except:
            messages.add_message(request, messages.ERROR, "Error in offer db. Please contact support")

    else:
        messages.add_message(request, messages.ERROR, "Error in process. Please contact support")
    return redirect('/')

@login_required
@check_email_validated
@check_plan('Trial', 'Artist/DJ', 'Promoter', 'Agent')
def offerhq(request):

    context = {}
    profile = Profile.objects.get(user=request.user)
    user_type = profile.user_types

    # Returning to home if not an agent
    if user_type != "Agent":
        return redirect('/')

    if request.method == 'GET':
        offersheets = Offersheet.objects.filter(agent=request.user)

        # Getting artist that are managed by current agent user but not have an offersheet
        no_offersheet_artists = []
        managed_artists = profile.profile_managed_djs.all()

        for managed_artist in managed_artists:
            flag = False
            for offersheet in offersheets:
                if managed_artist == offersheet.artist:
                    flag = True
                    break
            
            if flag == False:
                no_offersheet_artists.append(managed_artist)

        # Filtering offers for the current agent user
        offers_list = []
        for artist in managed_artists:
            offers = Offer.objects.filter(artist=artist,is_validated=True)
            offers_list.extend(offers)
        
        
        # Filtering streams offer
        offers_data = {}
        categories = ['live_event', 'virtual_event', 'streams', 'private_music', 'interview']
        for category in categories:
            offers = []
            subcategories = OffersheetSubCategories.objects.filter(category=category)
            for offer in offers_list:
                for subcategory in subcategories:
                    if offer.request_type == subcategory:
                        offers.append(offer)
                        break
            offers_data[str(category)] = offers
        
        context.update({
            'offersheets': offersheets,
            'offersheets_count': offersheets.count(),
            'no_offersheet_artists': no_offersheet_artists,
            'site_url': settings.SITE_URL,

            # Lists
            'streams': offers_data["streams"],
            'virtual_events': offers_data["virtual_event"],
            'live_events': offers_data["live_event"],
            'private_music': offers_data["private_music"],
            'interview': offers_data["interview"],

            # Counts
            'streams_count': len(offers_data["streams"]),
            'virtual_events_count': len(offers_data["virtual_event"]),
            'live_events_count': len(offers_data["live_event"]),
            'private_music_count': len(offers_data["private_music"]),
            'interview_count': len(offers_data["interview"]),
        })
        return render(request, 'artist/offerhq/offerhq.html', context)

@login_required
@check_email_validated
@check_plan('Trial', 'Artist/DJ', 'Promoter', 'Agent')
def offer_detail(request, offer_id):

    context = {}
    offer = Offer.objects.filter(id=offer_id).first()
    context.update({"offer": offer,
                    "artist" : offer.artist
                    })

    if request.method == 'POST':

        try:
            if request.POST["response_type"] in ['1','2','3']:
                email = SendEmail()
                # send validation email

                # offer hold
                if request.POST["response_type"] == '1':
                    offer.status = 1
                    offer.sent_message = request.POST['intern_mail']

                    validate_email = MarketingEmails.objects.filter(email_type="offer_hold_email").first()
                    if validate_email:
                        content = validate_email.email_content
                        content = content.replace("{username}", offer.name)
                        content = content.replace("{artist}", offer.artist.artistName)
                        content = content.replace("{text}", offer.sent_message)
                        try:
                            email.send_by_template([offer.email], "marketing_pages/not_loged_5.html",
                                                   context={
                                                       "content": content,
                                                   },
                                                   subject=validate_email.email_subject
                                                   )
                            messages.add_message(request, messages.SUCCESS,
                                                 "New Confirmation Email has been sent to your Registered Email")
                        except:
                            messages.add_message(request, messages.SUCCESS, "Please Try Again")
                # offer accept
                if request.POST["response_type"] == '2':
                    offer.status = 2
                    offer.sent_message = request.POST['intern_mail']

                    validate_email = MarketingEmails.objects.filter(email_type="offer_accept_email").first()
                    if validate_email:
                        content = validate_email.email_content
                        content = content.replace("{username}", offer.name)
                        content = content.replace("{artist}", offer.artist.artistName)
                        content = content.replace("{text}", offer.sent_message)
                        try:
                            email.send_by_template([offer.email], "marketing_pages/not_loged_5.html",
                                                   context={
                                                       "content": content,
                                                   },
                                                   subject=validate_email.email_subject
                                                   )
                            messages.add_message(request, messages.SUCCESS,
                                                 "New Confirmation Email has been sent to your Registered Email")
                        except Exception as e:
                            messages.add_message(request, messages.SUCCESS, "Please Try Again")
                # offer delete
                if request.POST["response_type"] == '3':
                    offer.status = 3
                    offer.sent_message = request.POST['intern_mail']

                    validate_email = MarketingEmails.objects.filter(email_type="offer_reject_email").first()
                    if validate_email:
                        content = validate_email.email_content
                        content = content.replace("{username}", offer.name)
                        content = content.replace("{artist}", offer.artist.artistName)
                        content = content.replace("{text}", offer.sent_message)
                        try:
                            email.send_by_template([offer.email], "marketing_pages/not_loged_5.html",
                                                   context={
                                                       "content": content,
                                                   },
                                                   subject=validate_email.email_subject
                                                   )
                            messages.add_message(request, messages.SUCCESS,
                                                 "New Confirmation Email has been sent to your Registered Email")
                        except:
                            messages.add_message(request, messages.SUCCESS, "Please Try Again")
                offer.save()
                print(222)
        except:
            pass
    return render(request, 'artist/offersheet/offer_detail.html', context)