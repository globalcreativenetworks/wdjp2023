#!/usr/bin/python
import sys, os

from django.core.management.base import BaseCommand
from artist.models import Artist, Genre, ArtistGenre

class Command(BaseCommand):
    help = "Run Scrapping process"

    def handle(self, *args, **options):
        # artist_all = Artist.objects.filter(artistID=71)
        # artist_all = Artist.objects.all()
        artist_all = Artist.allartistobjects.all()

        unique_genre_list = []

        for artist in artist_all:
            genre_all = artist.genre.all()
            for genre in genre_all:
                genre_name = genre.genreName

                genre = Genre.objects.get(genreName=genre_name)
                artist_genre = ArtistGenre.objects.get(artistID=artist, genreID=genre)

                artist_genre.delete()

        #         if genre_name not in unique_genre_list:
        #             unique_genre_list.append(genre_name)
        # print unique_genre_list

                if genre_name == "Dubstep" or genre_name == "Trap Future Bass" or genre_name == "Leftfield Bass" or genre_name == "Trap Future Bass" or genre_name == "Bass/Trap":
                    genre_name = "Dubstep Bass Trap"
                if genre_name == "Future House" or genre_name == "House" or genre_name == "Indie Dance Nu Disco" or genre_name == "Classic house" \
                 or genre_name == "Leftfield House Techno" or genre_name == "Dance" or genre_name == "Deep House" \
                 or genre_name == "Electro House" or genre_name == "Funk Soul Disco" or genre_name == "Funky Groove Jackin House" or genre_name == "Afro House" \
                 or genre_name == "Progressive House" or genre_name == "Tropical House"  or genre_name == "Tech House" or genre_name == "DESERT TRIBAL BEATS"  or genre_name == "Progressive" :
                    genre_name = "House"
                if genre_name == "Garage Bassline Grime":
                    genre_name = "Garage"
                if genre_name == "Glitch Hop" or genre_name == "Electronica Downtempo" or genre_name == "Live Electronic" or genre_name == "Live Electronics" :
                    genre_name = "Electronica"
                if genre_name == "Hard Dance/ Hard Style" :
                    genre_name = "Hard Dance/HardStyle"
                if genre_name == "Hard Dance Hard Techno" or genre_name == "Minimal Deep Tech" or genre_name == "Techno":
                    genre_name = "Techno"
                if genre_name == "Hip Hop R&B" :
                    genre_name = "Hip Hop & R & B"
                if genre_name == "Reggae":
                    genre_name = "Reggae"
                if genre_name == "Big Room":
                    genre_name = "EDM"
                if genre_name == "Drum & Bass":
                    genre_name = "Drum & Bass"
                if genre_name == "Psy Trance" or genre_name == "Trance" :
                    genre_name = "Trance & Psy Trance"
                if genre_name == "Tropical House":
                    genre_name = "Tropical House"

                genre, created = Genre.objects.get_or_create(genreName=genre_name)
                artist_genre, created = ArtistGenre.objects.get_or_create(artistID=artist, genreID=genre)
