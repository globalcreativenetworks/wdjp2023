#!/usr/bin/python
import sys, os
import requests
import json
import string

from os.path import join, dirname, abspath

from django.core.management.base import BaseCommand
from artist.models import Artist

class Command(BaseCommand):
    help = "Run Scrapping process"

    def handle(self, *args, **options):
        # artist_all = Artist.objects.all()
        artist_all = Artist.allartistobjects.all()
        # artist_all = Artist.allartistobjects.filter(artistID=195)

        for artist in artist_all:
            city =  artist.city
            country = artist.country
            if city and country:
                # AIzaSyCDy3VSIvldOrZSVf6KKSiK4JvGqk0E7t8
                # AIzaSyCDy3VSIvldOrZSVf6KKSiK4JvGqk0E7t8
                # AIzaSyAnX4oUbn6wmqRyrP_Lf0RizM9Bo7W-UdY
                url = "https://maps.googleapis.com/maps/api/geocode/json?address={},{}&key=AIzaSyCJTSEiRDKSHOsV5jbnxrISJXoopUUlhgw".format(city, country)

                response = requests.get(url)

                response_text = response.text

                response_text_object = json.loads(response_text)

                google_address = str(response_text_object['results'][0]['formatted_address'])

                google_address_list = google_address.split(",")

                google_country =  google_address_list[-1]

                google_city = ",".join(google_address_list[0:-1])


                city_decoded = [filter(lambda e:e in string.printable, st) for st in google_city]

                blank  = False
                if '' in city_decoded:
                    blank = True

                if google_city:
                    if not blank:
                        artist.city = "    "
                        artist.save()

                        artist.city = google_city

                if google_country:
                    try:
                        artist.country = google_country
                    except:
                        pass

                artist.city_latitude =  response_text_object['results'][0]['geometry']['location']['lat']
                artist.city_longitude = response_text_object['results'][0]['geometry']['location']['lng']

                artist.save()
