#!/usr/bin/python
import sys, os

from django.core.management.base import BaseCommand
from scrapper import Scrapper, ScrapMode

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)


class Command(BaseCommand):
    help = "Run Scrapping process"

    def add_arguments(self, parser):
        parser.add_argument(
            '--photo',
            action='store_true',
            dest='photo',
            default=False,
            help='Scrap Artist Photo instead of events.',
        )
        parser.add_argument(
            '--source',
            dest='source',
            default=False,
            help='From Which Source Need to Scrap Data.',
        )
        parser.add_argument(
            '--artist',
            dest='artist',
            default=False,
            help='Which Artist Needs to be Scrapped.',
        )

    def handle(self, *args, **options):
        scrapper_instance = Scrapper()
        if options['photo']:
            scrapper_instance.scrape_photo()
        elif options['source']:
            source = options['source']
            if options['artist']:
                scrapper_instance.process(scrape_mode=ScrapMode.All, username="system", selected_artist=[options['artist']], source=source)
            else:
                scrapper_instance.process(scrape_mode=ScrapMode.All, username="system", all=True, source=source)
        else:
            scrapper_instance.process(scrape_mode=ScrapMode.All, username="system", all=True)
        #scrapper_instance.process(scrape_mode=ScrapMode.All, username="system", selected_artist=['82'])
