from django.core.management.base import BaseCommand, CommandError
from artist.action_handler import run_facebook_like_count

class Command(BaseCommand):
    help = 'Scrape Facebook Likes'

    def handle(self, *args, **options):
        run_facebook_like_count()