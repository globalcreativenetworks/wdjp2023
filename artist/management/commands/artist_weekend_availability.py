#!/usr/bin/python
import sys, os
import requests
# import json
import string
import csv
import itertools
import re
import pandas as pd
from os.path import join, dirname, abspath


from django.core.management.base import BaseCommand
from artist.models import Artist, ArtistWeekendStatus

class Command(BaseCommand):
    help = "Run Scrapping process"

    def handle(self, *args, **options):
        file_location = join(dirname(dirname(abspath(__file__))), 'artists_wekends_unavailability.csv')

        df = pd.read_csv(file_location)
        count = 0

        for i in df:
            all_percentage_calculation = 0
            if count > 1:
                no_of_weekends = len(df[i])
                artist = Artist.objects.filter(artistName=i).first()
                if artist:
                    weekend = 1
                    for j in df[i]:
                        print weekend
                        artist_weekend_availability, create = ArtistWeekendStatus.objects.get_or_create(artist=artist, booked_percentage=j, weekend=weekend, year="2016-2017")
                        weekend += 1
            count += 1
