#!/usr/bin/python
import sys, os

from django.core.management.base import BaseCommand
from artist.models import Artist, Genre, ArtistGenre

class Command(BaseCommand):
    help = "Run Scrapping process"

    def handle(self, *args, **options):
        # artist_all = Artist.objects.all()
        artist_all = Artist.allartistobjects.all()

        for artist in artist_all:
            genre_all = artist.genre.all()
            for genre in genre_all:
                genre_name = genre.genreName

            artist_genre = ArtistGenre.objects.filter(artistID=artist)
            distinct_genre =  {x.genreID:1 for x in artist_genre}.keys()

            for genre in artist.genre.all():
                artist_genre_obj = ArtistGenre.objects.filter(artistID=artist, genreID=genre)
                for i in artist_genre_obj:
                    i.delete()

            for genre_obj in distinct_genre:
                genre_obj, created = Genre.objects.get_or_create(genreName=genre_obj)
                artist_genre, created = ArtistGenre.objects.get_or_create(artistID=artist, genreID=genre_obj)
