from datetime import datetime
import datetime as DT
from django.shortcuts import render

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from django.core import mail
from django.core.mail import send_mail
from django.conf import settings


from accounts.utils import SendEmail
from artist.models import Artist


class Command(BaseCommand):
    # Show this when the user types help
    help = "Send Email Command"

        # A command must define handle()

    def handle(self, *args, **options):

        today = datetime.today()
        week_ago = today - DT.timedelta(days=7)
        six_days_ago = today - DT.timedelta(days=6)

        artist = Artist.allartistobjects.filter(status="0", from_builder=True)
        artists = artist.filter(registered__gte=week_ago, registered__lte=six_days_ago)
        msg = SendEmail()

        msg.send_by_template(settings.DEFAULT_ADMIN_EMAILS, "artist/send_email.html",
            context={
                "artists": artists,
                "site_url":settings.SITE_URL,
            },
            subject = "Artist Aggregated"
        )
