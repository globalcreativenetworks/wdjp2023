from django.contrib import admin
from django.core.checks import messages
from .models import (ScrapeStatus, ScrapePhoto)
from artist.admin import ArtistAdmin
from artist.models import Artist
from artist.action_handler import run_photo_scrapper
# Register your models here.


def scrap_other(modeladmin, request, queryset):
    username = request.user.username
    artist_ids = [str(queryset.get(pk=item).artistID.artistID) for item in
                  request.POST.getlist(admin.ACTION_CHECKBOX_NAME)]
    run_photo_scrapper.delay(artists=artist_ids, username=username, new_source=True)


class ScrapePhotoAdmin(admin.ModelAdmin):
    list_display = ("ArtistName", "admin_thumbnail", "SourceName", "original_image",
                    "flip_image", "mono_image", "status")
    search_fields = ['artistID__artistName', 'sourceID__sourceName']
    list_filter = ['sourceID', 'status']
    readonly_fields = ('artistID', 'sourceID', 'sourceImage', 'originalImage',
                       'monoImage', 'flipImage', 'lastUpdateTime')

    def approve(self, request, queryset):
        selected = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        selected_models = self.model.objects.filter(id__in=selected)
        for selected_model in selected_models:
            ad = ArtistAdmin(Artist, self.admin_site)
            try:
                ad.model.objects.filter(artistID=int(selected_model.artistID.artistID)).update(
                    artistPhoto = str(selected_model.originalImage).strip('/'), remarks= ''
                )
                queryset.update(status=1)
            except Exception, ex:
                self.message_user(request, "Failed to Approve Artist Name: %s. Error: %s" %
                                  (str(selected_model.artistID), ex.message), level=messages.ERROR)
                return

    def reject(self, request, queryset):
        queryset.update(status=2)
        queryset.update

    approve.short_description = "Approve Artist Photo(s)"
    reject.short_description = "Reject Artist Photo(s)"
    scrap_other.short_description = "Scrape Different source"
    actions = [approve, reject, scrap_other]

    def has_add_permission(self, request, obj=None):
        return False


class ScrapeStatusAdmin(admin.ModelAdmin):
    list_display = ("artistName", "process", "progress", "status", "startTime", "title", "userName")

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(ScrapeStatus, ScrapeStatusAdmin)
admin.site.register(ScrapePhoto, ScrapePhotoAdmin)
