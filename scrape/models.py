from __future__ import unicode_literals

from django.db import models

# Create your models here.
from django.utils import timezone

from artist.models import (Artist, Source)


class ScrapePhoto(models.Model):
    artistID = models.OneToOneField(Artist, on_delete=models.CASCADE, db_column='artistID')
    sourceID = models.ForeignKey(Source, db_column="sourceID")
    sourceImage = models.CharField(max_length=255)
    originalImage = models.CharField(max_length=255)
    monoImage = models.CharField(max_length=255)
    flipImage = models.CharField(max_length=255)
    lastUpdateTime = models.DateTimeField()
    StatusChoice = ((0, 'Pending'), (1, 'Approved'), (2, 'Rejected'))
    status = models.IntegerField(default=0, choices=StatusChoice)

    def save(self, *args, **kwargs):
        self.lastUpdateTime = timezone.now()
        return super(ScrapePhoto, self).save(*args, **kwargs)
        Artist.objects.filter(artistID=self.artistID.artistID).update(remarks="Approval Pending")

    def ArtistName(self):
        return self.artistID

    ArtistName.short_description = "Artist Name"

    @staticmethod
    def get_link(image_path):
        return u'<a href="/media/%s" target="_blank">View</a> ' % image_path

    def SourceName(self):
        return u'<a href="%s" target="_blank">%s</a>' % (self.sourceImage, self.sourceID)

    SourceName.short_description = "Source Name"
    SourceName.allow_tags = True

    def admin_thumbnail(self):
        image = self.originalImage.replace('artistphotos', 'artistphotos/128')
        return u'<img src="/media%s" />' % (image)

    admin_thumbnail.short_description = 'Thumbnail'
    admin_thumbnail.allow_tags = True

    def original_image(self):
        return ScrapePhoto.get_link(self.originalImage)

    original_image.short_description = 'Original'
    original_image.allow_tags = True

    def flip_image(self):
        return ScrapePhoto.get_link(self.flipImage)

    flip_image.short_description = 'Flipped'
    flip_image.allow_tags = True

    def mono_image(self):
        return ScrapePhoto.get_link(self.monoImage)

    mono_image.short_description = 'B/W Image'
    mono_image.allow_tags = True

    class Meta:
        db_table = "dj_ScrappedPhoto"
        verbose_name_plural = "Artist Scrapped Photo"


class ScrapeStatus(models.Model):
    scrapeID = models.AutoField(primary_key=True, db_column='scrapeID')
    title = models.CharField(max_length=255, default='')
    startTime = models.DateTimeField()
    process = models.CharField(max_length=255, default='')
    progress = models.IntegerField(default=0)
    StatusChoice = ((1, 'Queued'), (2, 'In Progress'), (3, 'Done'), (4, 'Failed'))
    status = models.IntegerField(default=1, choices=StatusChoice)
    artistName = models.CharField(max_length=255)
    userName = models.CharField(max_length=255, default='unknown')

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.scrapeID:
            self.startTime = timezone.now()

        return super(ScrapeStatus, self).save(*args, **kwargs)

    class Meta:
        db_table = "dj_ScrapeStatus"
        verbose_name_plural = "Scrape Status"